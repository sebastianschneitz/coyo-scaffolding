(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.base
   *
   * @description
   * # Base module #
   * The base module includes every 3rd-party library as a dependency.
   *
   * New Coyo modules should
   * 1. Add coyo.base as a dependency and
   * 2. Add 3rd-party libraries to this base module and not to the own modules dependency list
   */
  angular
      .module('coyo.base', [
        'angular-loading-bar',
        'ngAria',
        'angularMoment',
        'ui.tree',
        'ngDraggable',
        'ngAnimate',
        'ngCookies',
        'ngFileUpload',
        'uiCropper',
        'ngMessages',
        'ngPicturefill',
        'ngSanitize',
        'ngStorage',
        'ngError',
        'ui.bootstrap',
        'ui.router',
        'ui.router.upgrade',
        'pascalprecht.translate',
        'monospaced.elastic',
        'rails',
        'ngFileUpload',
        'focus-if',
        'luegg.directives',
        'colorpicker.module',
        'ui.select',
        'ui.bootstrap.datetimepicker',
        'ngclipboard'
      ])
      .config(function ($urlServiceProvider) {
        $urlServiceProvider.deferIntercept();
      })
      .constant('protractorEnv', false)
      .config(configMsdElastic);

  /**
   * Appends whitespace to autosize textareas to fix IE style glitches.
   */
  function configMsdElastic(msdElasticConfig) {
    msdElasticConfig.append = ' ';
  }
})(angular);
