(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.events
   *
   * @description
   * Events module
   *
   * @requires $stateProvider
   * @requires main.event.eventsConfig
   */
  angular
      .module('coyo.events', [
        'coyo.base',
        'commons.ui',
        'commons.resource',
        'commons.target'
      ])
      .config(ModuleConfig)
      .config(registerTarget)
      .constant('eventsConfig', {
        templates: {
          list: 'app/modules/events/views/events.list.html',
          settings: 'app/modules/events/views/events.settings.html',
          event: {
            show: 'app/modules/events/views/events.show.html',
            timeline: 'app/modules/events/views/events.show.timeline.html',
            information: 'app/modules/events/views/events.show.information.html',
            participants: 'app/modules/events/views/events.show.participants.html'
          }
        },
        list: {
          paging: {
            pageSize: 20
          }
        }
      });

  /**
   * Module configuration
   */
  function ModuleConfig($stateProvider, eventsConfig) {
    $stateProvider
        .state('main.event', {
          url: '/events?:term&:from&:to&:status&:participationStatus',
          params: {
            term: {
              dynamic: true
            },
            from: {
              dynamic: true
            },
            to: {
              dynamic: true
            },
            status: {
              dynamic: true
            },
            participationStatus: {
              dynamic: true
            }
          },
          templateUrl: eventsConfig.templates.list,
          controller: 'EventsListController',
          controllerAs: '$ctrl',
          data: {
            globalPermissions: 'ACCESS_EVENTS',
            pageTitle: 'MODULE.EVENTS.PAGE_TITLE',
            guide: 'events'
          },
          resolve: {
            currentUser: function (authService) {
              return authService.getUser();
            }
          }
        })
        .state('main.event.create', {
          url: '/create?:host&:public',
          views: {
            '@main': {
              templateProvider: function ($timeout) {
                return $timeout(function () {
                  return '<coyo-event-create '
                      + '[initial-sender]="{{eventCtrl.host}}"'
                      + '[public]="{{eventCtrl.publicVisibility}}"'
                      + '></coyo-event-create>';
                });
              },
              controller: function (host, publicVisibility) {
                this.host = host;
                this.publicVisibility = publicVisibility;
              },
              controllerAs: 'eventCtrl',
            }
          },
          resolve: {
            host: function ($q, $stateParams, SenderModel, currentUser) {
              if ($stateParams.host) {
                return SenderModel.getWithPermissions($stateParams.host, {}, ['manage']).then(function (sender) {
                  return sender._permissions.manage ? sender : null;
                }).catch(function () {
                  return null;
                });
              }
              return $q.resolve(currentUser);
            },
            publicVisibility: function ($stateParams) {
              return $stateParams.public === 'true';
            }
          }
        })
        .state('main.event.show', {
          url: '/:idOrSlug',
          redirect: 'main.event.show.timeline',
          views: {
            '@main': {
              templateUrl: eventsConfig.templates.event.show,
              controller: 'EventsShowController',
              controllerAs: '$ctrl'
            }
          },
          data: {
            senderParam: 'idOrSlug',
            pageTitle: false
          },
          resolve: {
            event: function (EventModel, $stateParams) {
              return EventModel.getWithPermissions({id: $stateParams.idOrSlug}, {with: 'subscriptionInfo'}, ['manage', 'canParticipate']);
            },
            senderId: function (event) {
              return event.id;
            }
          },
          onEnter: function (event, $injector) {
            $injector.get('ngxPageTitleService').setTitle(event.displayName);
          }
        })
        .state('main.event.show.settings', {
          url: '/settings',
          views: {
            '@main': {
              templateUrl: eventsConfig.templates.settings,
              controller: 'EventsSettingsController',
              controllerAs: '$ctrl'
            }
          },
          resolve: {
            eventOrigin: function (EventModel, $stateParams, $state) {
              return EventModel.getWithPermissions({id: $stateParams.idOrSlug}, {origin: true},
                  ['manage', 'delete', 'manageApps', 'manageSlots', 'createFile']).catch(function () {
                $state.go('main.event');
              });
            },
            adminIds: function (eventOrigin) {
              return eventOrigin.getAdminIds();
            }
          }
        })
        .state('main.event.show.timeline', {
          url: '/timeline',
          views: {
            '@main.event.show': {
              templateUrl: eventsConfig.templates.event.timeline
            }
          }
        })
        .state('main.event.show.information', {
          url: '/information',
          views: {
            '@main.event.show': {
              templateUrl: eventsConfig.templates.event.information
            }
          }
        })
        .state('main.event.show.participants', {
          url: '/participants',
          views: {
            '@main.event.show': {
              templateUrl: eventsConfig.templates.event.participants
            }
          }
        });
  }

  function registerTarget(targetServiceProvider) {
    /* register event links for activity notifications */
    targetServiceProvider.register('event', /*@ngInject*/ function (params, $state) {
      var stateParams = {idOrSlug: params.slug || params.id};
      return params.go
        ? $state.go('main.event.show', stateParams, {reload: 'main.event.show'})
        : $state.href('main.event.show', stateParams, {reload: 'main.event.show'});
    });
    targetServiceProvider.register('event_overview', /*@ngInject*/ function (params, $state) {
      return $state.href('main.event');
    });
  }

})(angular);
