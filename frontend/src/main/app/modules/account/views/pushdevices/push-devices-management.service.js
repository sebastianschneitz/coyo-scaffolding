(function () {
  'use strict';

  angular
      .module('coyo.account')
      .factory('pushDevicesManagementService', pushDevicesManagementService);

  function pushDevicesManagementService(authService, pushDevicesService) {

    return {
      preparePushDevices: preparePushDevices,
      deletePushAppInstallation: deletePushAppInstallation,
      togglePushAppInstallationStatus: togglePushAppInstallationStatus
    };

    /**
     * Prepares the push devices.
     *
     * @param {object} vm the view model of the context
     */
    function preparePushDevices(vm) {
      authService.getUser().then(function (currentUser) {
        pushDevicesService.getPushDevices(currentUser).then(function (pushAppInstallations) {
          vm.pushDevices = _.groupBy(pushAppInstallations, 'name');
          vm.pushDevicesAmount = _.size(vm.pushDevices);
          vm.pushDevicesDetails = {};
          _.forEach(vm.pushDevices, function (appInstallations, deviceName) {
            vm.pushDevicesDetails[deviceName] = {
              created: _.minBy(appInstallations, 'created').created,
              active: !!_.find(appInstallations, {'active': true}),
              appsCollapsed: false
            };
          });
        });
      });
    }

    /**
     * Delete a push app installation.
     *
     * @param {object} vm the view model of the context
     * @param {object} appInstallation the app installation to delete
     */
    function deletePushAppInstallation(vm, appInstallation) {
      authService.getUser().then(function (currentUser) {
        if (currentUser.hasGlobalPermissions('MANAGE_USER')) {
          pushDevicesService.deletePushDevice(currentUser, appInstallation).then(function () {
            _.remove(vm.pushDevices[appInstallation.name], {'id': appInstallation.id});
            if (!vm.pushDevices[appInstallation.name].length) {
              delete vm.pushDevices[appInstallation.name];
            }
            vm.pushDevicesAmount = _.size(vm.pushDevices);
            _updatePushDeviceActiveStatusAndCreated(vm);
          });
        }
      });
    }

    /**
     * Toggle the active state of the given app installation.
     *
     * @param {object} vm the view model of the context
     * @param {object} appInstallation the app installation to toggle
     */
    function togglePushAppInstallationStatus(vm, appInstallation) {
      authService.getUser().then(function (currentUser) {
        if (currentUser.hasGlobalPermissions('MANAGE_USER') && !vm.deviceIsToggling) {
          vm.deviceIsToggling = true;
          pushDevicesService.togglePushDevice(currentUser, appInstallation).then(function () {
            appInstallation.active = !appInstallation.active;
            _updatePushDeviceActiveStatusAndCreated(vm);
          }).finally(function () {
            vm.deviceIsToggling = false;
          });
        }
      });
    }

    function _updatePushDeviceActiveStatusAndCreated(vm) {
      _.forEach(vm.pushDevices, function (appInstallations, deviceName) {
        vm.pushDevicesDetails[deviceName].created = _.minBy(appInstallations, 'created').created;
        vm.pushDevicesDetails[deviceName].active = !!_.find(appInstallations, {'active': true});
      });
    }

  }

})();
