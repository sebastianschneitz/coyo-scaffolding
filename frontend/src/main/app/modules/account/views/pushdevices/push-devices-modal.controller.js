(function (angular) {
  'use strict';

  angular
      .module('coyo.account')
      .controller('PushDevicesModalController', PushDevicesModalController);

  function PushDevicesModalController(pushDevicesManagementService) {
    var vm = this;
    vm.$onInit = onInit;

    vm.pushDevices = {};
    vm.pushDevicesAmount = 0;
    vm.pushDevicesDetails = {};

    vm.togglePushAppInstallationStatus = togglePushAppInstallationStatus;
    vm.removePushAppInstallationDevice = removePushAppInstallationDevice;

    /**
     * Toggle the active state of the given app installation.
     *
     * @param {object} appInstallation The appInstallation to toggle the status
     */
    function togglePushAppInstallationStatus(appInstallation) {
      pushDevicesManagementService.togglePushAppInstallationStatus(vm, appInstallation);
    }

    /**
     * Removes the given push app installation.
     *
     * @param {object} appInstallation the push app installation to remove
     */
    function removePushAppInstallationDevice(appInstallation) {
      pushDevicesManagementService.deletePushAppInstallation(vm, appInstallation);
    }

    function onInit() {
      pushDevicesManagementService.preparePushDevices(vm);
    }

  }
})(angular);
