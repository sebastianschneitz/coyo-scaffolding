(function (angular) {
  'use strict';

  angular
      .module('coyo.account')
      .directive('oyocHashtagSubscriptionButton', hashtagSubscriptionButton)
      .controller('HashtagSubscriptionButtonController', hashtagSubscriptionButtonController);

  function hashtagSubscriptionButton() {
    return {
      templateUrl: 'app/modules/account/components/hashtag-subscribe-button/hashtag-subscribe-button.html',
      controller: 'HashtagSubscriptionButtonController',
      controllerAs: '$ctrl',
      scope: true,
      bindToController: {
        term: '@'
      }
    };
  }

  function hashtagSubscriptionButtonController($injector, hashtagSubscriptionsService, authService, $q) {
    var vm = this;

    vm.isSearchTermAHashtag = isSearchTermAHashtag;
    vm.isSubscribedHashtag = isSubscribedHashtag;
    vm.onHashtagSubscribeButtonClick = onHashtagSubscribeButtonClick;
    vm.$onInit = onInit;

    vm.subscriptionLoading = true;

    function isSearchTermAHashtag() {
      return $injector.get('ngxHashtagService').isConcludedHashtag(vm.term);
    }

    function isSubscribedHashtag() {
      return vm.subscribedHashtags && _isHashtagInArray();
    }

    function onHashtagSubscribeButtonClick() {
      var termToUpperCase = vm.term.toUpperCase();
      if (!$injector.get('ngxHashtagService').isConcludedHashtag(vm.term)) {
        return $q.reject();
      }

      if (isSubscribedHashtag()) {
        vm.subscribedHashtags.splice(vm.subscribedHashtags.indexOf(termToUpperCase), 1);
        return hashtagSubscriptionsService.unsubscribe(authService.getCurrentUserId(), vm.term)
            .then(_checkSubscriptionChangeResponse)
            .catch(function () {
              vm.subscribedHashtags.push(termToUpperCase);
            });
      } else {
        vm.subscribedHashtags.push(termToUpperCase);
        return hashtagSubscriptionsService.subscribe(authService.getCurrentUserId(), vm.term)
            .then(_checkSubscriptionChangeResponse)
            .catch(function () {
              vm.subscribedHashtags.splice(vm.subscribedHashtags.indexOf(termToUpperCase), 1);
            });
      }
    }

    function onInit() {
      if (authService.canUseHashtags() && $injector.get('ngxHashtagService').isConcludedHashtag(vm.term)) {
        hashtagSubscriptionsService.getSubscriptions(authService.getCurrentUserId())
            .then(function (subscribedHashtags) {
              vm.subscribedHashtags = subscribedHashtags;
              vm.subscriptionLoading = false;
            });
      }
    }

    function _isHashtagInArray() {
      return _.findIndex(vm.subscribedHashtags, function (subscribedHashtag) {
        return subscribedHashtag.toUpperCase() === vm.term.toUpperCase();
      }) > -1;
    }

    function _checkSubscriptionChangeResponse(httpResponse) {
      if (httpResponse.status !== 200) {
        return $q.reject();
      }
      return $q.resolve();
    }
  }

})(angular);
