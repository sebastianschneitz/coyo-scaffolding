(function () {
  'use strict';

  angular
      .module('coyo.account')
      .factory('hashtagSubscriptionsService', hashtagSubscriptionsService);

  function hashtagSubscriptionsService($http, coyoEndpoints) {

    return {
      subscribe: subscribe,
      unsubscribe: unsubscribe,
      getSubscriptions: getSubscriptions
    };

    function subscribe(userId, hashtag) {
      return $http.post(_getHashtagSubscriptionsUrl(userId), {hashtag: hashtag});
    }

    function unsubscribe(userId, hashtag) {
      return $http.delete(_getHashtagSubscriptionsUrl(userId) + '/' + encodeURIComponent(hashtag));
    }

    function getSubscriptions(userId) {
      return $http.get(_getHashtagSubscriptionsUrl(userId)).then(function (response) {
        return _.map(response.data, 'hashtag');
      });
    }

    function _getHashtagSubscriptionsUrl(userId) {
      return coyoEndpoints.user.hashtagSubscriptions.replace('{id}', userId);
    }
  }

})();
