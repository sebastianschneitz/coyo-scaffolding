(function () {
  'use strict';

  angular
      .module('coyo.account')
      .component('oyocHashtagSubscriptionsSelect', hashtagSubscriptionsSelect())
      .controller('HashtagSubscriptionsSelectController', HashtagSubscriptionsSelectController);

  function hashtagSubscriptionsSelect() {
    return {
      templateUrl: 'app/modules/account/components/hashtag-subscriptions-select/hashtag-subscriptions-select.html',
      controller: 'HashtagSubscriptionsSelectController',
      controllerAs: '$ctrl'
    };
  }

  function HashtagSubscriptionsSelectController($injector, $q, authService, hashtagSubscriptionsService) {
    var vm = this;

    vm.$onInit = onInit;
    vm.hashtags = [];

    var _currentUser = {};

    vm.onTagAdding = onTagAdding;
    vm.onTagAdded = onTagAdded;
    vm.onTagRemoved = onTagRemoved;
    vm.onInvalidTag = onInvalidTag;
    vm.onChange = onChange;

    function onTagAdding(tag) {
      return $injector.get('ngxHashtagService').isConcludedHashtag(tag.text);
    }

    function onTagAdded(tag) {
      hashtagSubscriptionsService.subscribe(_currentUser.id, tag.text).catch(_processFailedResponse);
    }

    function onTagRemoved(tag) {
      hashtagSubscriptionsService.unsubscribe(_currentUser.id, tag.text).catch(_processFailedResponse);
    }

    function _processFailedResponse() {
      onInit();
    }

    function onChange() {
      vm.hashtagsForm.$setValidity('invalid-syntax', true);
      vm.hashtagsForm.$setValidity('already-added', true);
    }

    function onInvalidTag(tag) {
      var contains = _.some(vm.hashtags, function (t) {
        return t.text.toUpperCase() === tag.text.toUpperCase();
      });
      vm.hashtagsForm.$setValidity(contains ? 'already-added' : 'invalid-syntax', false);
    }

    function onInit() {
      authService.getUser().then(function (user) {
        _currentUser = user;
        return user.hasGlobalPermissions('USE_HASHTAGS')
          ? hashtagSubscriptionsService.getSubscriptions(_currentUser.id)
          : $q.reject();
      }).then(function (results) {
        vm.hashtags = results;
      });
    }
  }
})();
