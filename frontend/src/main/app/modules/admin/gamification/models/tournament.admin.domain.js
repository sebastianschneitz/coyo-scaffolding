(function (angular) {
  'use strict';

  angular
      .module('coyo.admin.gamification')
      .factory('TournamentAdminModel', TournamentAdminModel);

  /**
   * @ngdoc service
   * @name coyo.admin.gamification.TournamentAdminModel
   *
   * @description
   * Domain model representing the tournament administration endpoint.
   *
   * @requires restResourceFactory
   */
  function TournamentAdminModel(restResourceFactory) {
    var Tournament = restResourceFactory({
      url: '/web/tournaments/{{id}}'
    });

    return Tournament;
  }
})(angular);
