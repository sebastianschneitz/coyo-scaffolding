(function (angular) {
  'use strict';

  angular
      .module('coyo.admin.license')
      .controller('AdminLicenseController', AdminLicenseController);

  function AdminLicenseController($state, $injector, coyoConfig, coyoUrls, LicenseModel) {
    var vm = this,
        stored;
    vm.$onInit = _init;
    vm.save = save;
    vm.reset = reset;
    vm.resetable = resetable;

    function save(form) {
      form.$setPristine();
      LicenseModel.updateLicense(vm.licenseKey).then(function (license) {
        _updateLicense(license);
        $injector.get('ngxNotificationService').success('ADMIN.LICENSE.SAVE.SUCCESS');
      }, function () {
        _updateLicense({
          licenseKey: vm.licenseKey,
          status: 'LICENSE_KEY',
          currentUserSize: (vm.user && vm.user.current) || 0,
          maxUserSize: 0,
          orderId: vm.orderId,
          expectedServerId: vm.serverId
        }, true);
      });
    }

    function reset(form) {
      form.$setPristine();
      if (stored) {
        _updateLicense(stored, true);
      }
    }

    function resetable() {
      return stored && stored.licenseKey !== vm.licenseKey;
    }

    function _updateLicense(license, doNotStore) {
      if (!doNotStore) {
        stored = license;
      }

      vm.customerCenterLink = coyoUrls.customerCenter + (license.orderId ? '/order/' + license.orderId : '');

      vm.status = license.status || false;
      vm.validDays = license.validDays;
      vm.licenseKey = license.licenseKey;
      vm.orderId = license.orderId;
      vm.serverId = license.expectedServerId;

      vm.user = vm.user || {};
      vm.user.current = license.currentUserSize;
      vm.user.max = license.maxUserSize;
    }

    function _init() {
      vm.version = coyoConfig.versionString();
      LicenseModel.load().then(function (license) {
        if (license.environment === 'cloud') {
          $state.go('admin.license.subscription');
        }

        _updateLicense(license);
      });
    }
  }
})(angular);
