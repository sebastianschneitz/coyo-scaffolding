(function (angular) {
  'use strict';

  angular.module('coyo.admin.apps-widgets')
      .controller('AdminWidgetListController', AdminWidgetListController);

  function AdminWidgetListController($rootScope, $scope, $q, $timeout, $injector, WidgetConfigurationModel,
                                     widgetConfigs, adminWidgetSettingsModal) {
    var vm = this;

    vm.$onInit = onInit;
    vm.openSettings = openSettings;
    vm.update = update;

    function update(widget) {
      var deferred = $q.defer();

      $timeout(function () { // wait for coyoCheckbox to update model
        WidgetConfigurationModel.save(widget).finally(deferred.resolve);
      });

      return deferred.promise;
    }

    function openSettings(widget) {
      adminWidgetSettingsModal
          .open(widget)
          .then(_saveModalChanges);
    }

    function _saveModalChanges(changedWidget) {
      var widget = _.find(vm.widgets, {key: changedWidget.key});
      if (widget) {
        update(changedWidget);
      }
    }

    function onInit() {
      vm.widgets = [];
      vm.isMobile = $rootScope.screenSize.isXs || $rootScope.screenSize.isSm;

      var unsubscribe = $rootScope.$on('screenSize:changed', function (event, screenSize) {
        vm.isMobile = screenSize.isXs || screenSize.isSm;
      });
      $scope.$on('$destroy', unsubscribe);

      _loadWidgets();
    }

    function _loadWidgets() {
      var ngxWidgetRegistry = $injector.get('ngxWidgetRegistry');
      vm.widgets = _.map(ngxWidgetRegistry.getAll(), function (widget) {
        return angular.extend(widget, {
          enabled: false,
          moderatorsOnly: widget.alwaysRestricted
        }, _.find(widgetConfigs, {key: widget.key}), {
          whitelistExternal: widget.whitelistExternal
        });
      });
    }
  }

})(angular);
