(function (angular) {
  'use strict';

  angular
      .module('coyo.profile')
      .component('oyocUserSuperior', userSuperior())
      .controller('UserSuperiorController', UserSuperiorController);

  /**
   * @ngdoc directive
   * @name coyo.profile.coyoUserSuperior:coyoUserSuperior
   * @restrict 'E'
   * @element OWN
   * @scope
   *
   * @description
   * Displays the superior of the given user.
   *
   * @param {object} user
   * the user to render the superior for
   *
   * @requires coyo.domain.UserModel
   */
  function userSuperior() {
    return {
      templateUrl: 'app/modules/profile/components/user-superior/user-superior.html',
      bindings: {
        user: '<'
      },
      controller: 'UserSuperiorController'
    };
  }

  function UserSuperiorController(UserModel, errorService) {
    var vm = this;

    vm.$onInit = init;

    function init() {
      if (!_.isEmpty(vm.user.manager)) {
        UserModel.get(vm.user.manager).then(function (superior) {
          vm.superior = superior;
        }).catch(function (errorResponse) {
          errorService.suppressNotification(errorResponse);
          vm.user.manager = undefined;
        });
      }
    }
  }

})(angular);
