(function (angular) {
  'use strict';

  angular
      .module('coyo.pages')
      .controller('PagesListController', PagesListController);

  function PagesListController($rootScope, $scope, $q, $sessionStorage, $state, $stateParams, $timeout, PageModel,
                               PageCategoryModel, Pageable, currentUser, categories, appService, authService,
                               modalService, subscriptionsService, pagesConfig, selectionFilterService) {

    var vm = this,
        FILTER_KEY_ALL = selectionFilterService.KEY_ALL;

    vm.currentUser = currentUser;
    vm.categories = categories;
    vm.categoryModelClass = PageCategoryModel;
    vm.subscriptions = [];
    vm.subscriptionsLoaded = false;
    vm.languages = {};
    vm.loading = true;
    vm.filtersResettable = false;
    vm.appLinks = {};

    vm.$onInit = onInit;
    vm.search = search;
    vm.isAutoSubscribe = isAutoSubscribe;
    vm.getTotalCount = getTotalCount;
    vm.setFilterStatus = setFilterStatus;
    vm.setFilterCategories = setFilterCategories;
    vm.resetFilters = resetFilters;
    vm.getAppLink = getAppLink;

    function search(searchTerm) {
      if (!vm.editingCategory) {
        vm.query.term = searchTerm;
        vm.query.filters = {
          categories: [],
          status: _getQueryFilterStatus()
        };
        _loadPages();
      }
    }

    function isAutoSubscribe(page) {
      return _.some(vm.subscriptions, {targetId: page.id, autoSubscribe: true}) || undefined;
    }

    function getTotalCount() {
      return (vm.totalCount || 0);
    }

    function setFilterStatus(status) {
      _setStatus(status);
      _loadPages();
    }

    function setFilterCategories(selected) {
      _.set(vm.query, 'filters.categories', selected);
      _loadPages();
    }

    function resetFilters() {
      _resetFilterStatus();
      _resetFilterCategories();
      _loadPages();
    }

    function getAppLink(app, page) {
      return appService.getAppLinkForCreatedApp(page, app);
    }

    /* ==================== */

    function _loadPages() {
      if (vm.loading && vm.currentPage) {
        return;
      }
      vm.subscriptionsLoaded = false;

      // write params to URL
      $state.transitionTo('main.page', _.omitBy({
        term: _.get(vm.query, 'term', ''),
        'categories[]': _getQueryFilterCategories(),
        'status': _getQueryFilterStatus()
      }, _.isEmpty), {location: 'replace'});

      // perform search
      $sessionStorage.pageQuery = vm.query;
      vm.loading = true;

      var term = vm.query.term;
      var sort = term ? ['_score,DESC', 'displayName.sort'] : 'displayName.sort';
      var pageable = new Pageable(0, pagesConfig.list.paging.pageSize, sort);
      var filters = vm.query.filters;
      var searchFields = ['displayName', 'description'];
      var aggregations = {categories: 0, allCategories: 1};
      PageModel.searchWithFilter(term, pageable, filters, searchFields, aggregations).then(function (page) {
        vm.currentPage = page;
        _.forEach(vm.categories, function (category) {
          var data = _.find(page.aggregations.categories, {key: category.id});
          category.count = _.get(data, 'count', 0);
        });
        vm.totalCount = _.get(page.aggregations.allCategories[0], 'count', 0);
        vm.admin = _.find(page.aggregations.status, {key: 'admin'});
        vm.subscribed = _.find(page.aggregations.status, {key: 'subscribed'});
        var missingData = _.find(page.aggregations.categories, {key: 'N/A'});
        vm.missingCount = _.get(missingData, 'count', null);
        _setFilterStatusCount();

        // fetch subscriptions to check for auto subscribe
        subscriptionsService.getSubscriptions(vm.currentUser.id, _.map(page.content, 'id'))
            .then(function (subscriptions) {
              vm.subscriptions.push.apply(vm.subscriptions, subscriptions);
              vm.subscriptionsLoaded = true;
            });
        vm.filtersResettable = _isFiltersResettable();
      }).finally(function () {
        vm.loading = false;
      });
    }

    function _getQueryFilterStatus() {
      var status = _.get(vm.query, 'filters.status');
      return !_.isUndefined(status) ? status : FILTER_KEY_ALL;
    }

    function _setStatus(status) {
      _.set(vm.query, 'filters.status', status);
    }

    function _resetFilterStatus() {
      vm.statusFilterModel.clearAll();
      _setStatus(FILTER_KEY_ALL);
    }

    function _setFilterStatusCount() {
      var adminCount = 0; // _.get(vm.admin, 'count', 0); // temporary disabled
      var subscribedCount = 0; // _.get(vm.subscribed, 'count', 0); // temporary disabled
      vm.statusFilterModel.setCount(getTotalCount());
      vm.statusFilterModel.getItem('ADMIN').count = adminCount;
      vm.statusFilterModel.getItem('SUBSCRIBED').count = subscribedCount;
    }

    function _getQueryFilterCategories() {
      return _.get(vm.query, 'filters.categories', []);
    }

    function _resetFilterCategories() {
      vm.query.filters.categories = vm.categoryFilter = [];
    }

    function _isFiltersResettable() {
      return vm.statusFilterModel.isSelected() || _getQueryFilterCategories().length > 0;
    }

    function onInit() {

      // extract search from URL / storage
      vm.query = $sessionStorage.pageQuery || {};
      if ($stateParams.term || $stateParams['categories[]']) {
        angular.extend(vm.query, {
          term: $stateParams.term,
          filters: {categories: $stateParams['categories[]']}
        });
      }

      // register permission callback
      authService.onGlobalPermissions('MANAGE_PAGE_CATEGORIES', function (canManage) {
        vm.canManagePageCategories = canManage;
      });

      vm.categoryFilter = _getQueryFilterCategories();

      vm.statusFilterModel =
          selectionFilterService.builder()
              .itemModel(selectionFilterService.itemBuilder().key('SUBSCRIBED').icon('zmdi-notifications').build())
              .itemModel(selectionFilterService.itemBuilder().key('ADMIN').icon('zmdi-settings').build())
              .active(_getQueryFilterStatus()).build();

      $timeout(function () {
        _loadPages();
      });
    }
  }

})(angular);
