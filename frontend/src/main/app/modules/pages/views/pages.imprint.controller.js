(function (angular) {
  'use strict';

  angular
      .module('coyo.pages')
      .controller('PageImprintController', PageImprintController);

  /**
   * Controller for the page imprint view
   */
  function PageImprintController($scope, page, UserModel, widgetLayoutService) {
    var vm = this;

    vm.page = page;
    vm.admins = [];
    vm.editMode = false;
    vm.isNew = true;
    vm.edit = edit;
    vm.save = save;
    vm.cancel = cancel;
    vm.$onInit = _init;

    function cancel() {
      vm.editMode = false;
      widgetLayoutService.cancel($scope);
    }

    function edit() {
      vm.editMode = true;
      vm.isNew = false;
      widgetLayoutService.edit($scope, false, false);

    }

    function save() {
      vm.editMode = false;
      widgetLayoutService.save($scope, false, false);
    }

    function _init() {
      UserModel.getUsers(vm.page.adminIds).then(function (result) {
        vm.admins = result;
      });
    }
  }
})(angular);
