(function (angular) {
  'use strict';

  function pageSubscribe() {
    return {
      restrict: 'E',
      templateUrl: function ($element, $attrs) {
        var template = $attrs.link ? '-link' : '-button';
        return 'app/modules/pages/components/page-subscribe/page-subscribe' + template + '.html';
      },
      replace: true,
      scope: {},
      bindToController: {
        page: '<',
        size: '@',
        darkTheme: '<?',
        link: '<?'
      },
      controller: 'PageSubscribeController',
      controllerAs: '$ctrl'
    };
  }

  /**
   * @ngdoc directive
   * @name coyo.pages.coyoPageSubscribe:coyoPageSubscribe
   * @restrict 'E'
   * @element OWN
   *
   * @description
   * Renders a page's subscribe/unsubscribe button.
   *
   * @requires $timeout
   * @requires commons.auth.authService
   *
   * @param {object} page The page
   * @param {string} size The size
   * @param {boolean=} darkTheme Whether to use a dark theme or not. Default false.
   */
  angular
      .module('coyo.pages')
      .directive('coyoPageSubscribe', pageSubscribe)
      .controller('PageSubscribeController', PageSubscribeController);

  function PageSubscribeController($rootScope, $scope, authService, subscriptionsService) {
    var vm = this;

    vm.loading = false;
    vm.subscribed = false;
    vm.autoSubscribe = false;

    vm.toggle = toggle;
    vm.$onInit = init;

    /* --- INIT --- */

    function init() {
      vm.show = false;
      authService.getUser().then(function (user) {
        vm.currentUser = user;

        vm.loading = true;
        var eventHandler = subscriptionsService.onSubscriptionChange(vm.currentUser.id, vm.page.id, _updateSubscription);
        $scope.$on('$destroy', eventHandler);
      });
    }

    /* --- PUBLIC METHODS --- */

    function toggle(event) {
      if (vm.loading) {
        return;
      }
      vm.loading = true;
      event.stopPropagation();

      if (vm.subscribed) {
        _unsubscribe();
      } else {
        _subscribe();
      }
    }

    /* --- PRIVATE METHODS --- */

    function _subscribe() {
      subscriptionsService.subscribe(vm.currentUser.id, vm.page.id, 'page', vm.page.id).then(function () {
        ++vm.page.userSubscriptionsCount;
        $rootScope.$emit('page:subscribed', vm.page);
      });
    }

    function _unsubscribe() {
      subscriptionsService.unsubscribe(vm.currentUser.id, vm.page.id).then(function () {
        --vm.page.userSubscriptionsCount;
        $rootScope.$emit('page:unSubscribed', vm.page.id);
      });
    }

    function _updateSubscription(subscription) {
      vm.subscribed = angular.isDefined(subscription);
      vm.autoSubscribe = vm.subscribed && _.get(subscription, 'autoSubscribe', false);
      vm.loading = false;
      vm.show = true;
    }
  }

})(angular);
