(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.blog')
      .controller('BlogSettingsController', BlogSettingsController);

  function BlogSettingsController($scope) {
    var vm = this;
    vm.$onInit = onInit;
    vm.setAppFolderPermissions = setAppFolderPermissions;

    vm.app = $scope.model;
    vm.app.settings.authorType = _.chain(vm).get('app.settings.authorType').defaultTo('ADMIN').value();
    vm.app.settings.publisherType = _.chain(vm).get('app.settings.publisherType').defaultTo('ADMIN').value();
    vm.app.settings.commentsAllowed = _.chain(vm).get('app.settings.commentsAllowed').defaultTo(true).value();

    function setAppFolderPermissions() {
      if (vm.app.settings.authorType === 'BLOGAPP_LIST_OF_USERS') {
        setListOfUsers();
      } else {
        _.set(vm.app.settings, 'folderPermissions.modifyRole', vm.app.settings.authorType);
        clearListOfUsers();
      }
    }

    function setListOfUsers() {
      _.set(vm.app.settings, 'folderPermissions.modifyRole', undefined);
      _.set(vm.app.settings, 'folderPermissions.users', vm.app.settings.authorIds);
    }

    function clearListOfUsers() {
      vm.app.settings.folderPermissions.users && delete vm.app.settings.folderPermissions.users;
      vm.app.settings.authorIds && delete vm.app.settings.authorIds;
    }

    function onInit() {
      vm.setAppFolderPermissions();
    }
  }

})(angular);
