(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.apps.form
   *
   * @description
   * # form app module #
   * The form app module contains the form app objects.
   *
   * @requires coyo.apps.api.appRegistryProvider
   */
  angular
      .module('coyo.apps.form', [
        'coyo.base',
        'coyo.apps.api',
        'coyo.apps.commons.fields',
        'commons.config',
        'commons.ui',
        'commons.i18n'
      ])
      .config(registerApp)
      .config(registerTarget);

  function registerApp(appRegistryProvider) {
    appRegistryProvider.register({
      name: 'APP.FORM.NAME',
      description: 'APP.FORM.DESCRIPTION',
      key: 'form',
      icon: 'zmdi-inbox',
      states: [
        {
          abstract: true,
          templateUrl: 'app/apps/form/form-frame.html'
        },
        {
          name: 'form',
          url: '',
          default: true,
          templateUrl: 'app/apps/form/form.html',
          controller: 'FormController',
          controllerAs: '$ctrl',
          resolve: {
            fields: /*@ngInject*/ function (FieldModel, app) {
              var context = {
                senderId: app.senderId,
                appId: app.id,
                appKey: app.key
              };
              return FieldModel.get(context);
            }
          }
        },
        {
          name: 'list',
          url: '/list',
          templateUrl: 'app/apps/form/list/form-entries.html',
          controller: 'FormEntriesController',
          controllerAs: '$ctrl',
          resolve: {
            fields: /*@ngInject*/ function (FieldModel, app) {
              var context = {
                senderId: app.senderId,
                appId: app.id,
                appKey: app.key
              };
              return FieldModel.get(context);
            }
          }
        }, {
          name: 'list.details',
          url: '/details/:id',
          resolve: {
            entry: /*@ngInject*/ function (FormEntryModel, $stateParams, app) {
              var context = {
                senderId: app.senderId,
                appId: app.id,
                id: $stateParams.id
              };
              return FormEntryModel.get(context);
            },
            fields: /*@ngInject*/ function (FieldModel, app) {
              var context = {
                senderId: app.senderId,
                appId: app.id,
                appKey: app.key
              };
              return FieldModel.get(context);
            }
          },
          onEnter: /*@ngInject*/ function ($state, app, entry, fields, modalService, coyoConfig) {
            function leaveState(reason) {
              if (reason !== coyoConfig.rejectReason.transitionStarted) {
                $state.go('^');
              }
            }

            modalService.open({
              size: 'lg',
              templateUrl: 'app/apps/form/list/form-entry-detail-modal.html',
              controller: 'FormEntryDetailModalController',
              controllerAs: '$ctrl',
              resolve: {
                app: function () {
                  return app;
                },
                fields: /*@ngInject*/ function () {
                  return fields;
                },
                entry: function () {
                  return entry;
                }
              }
            }).result.then(leaveState, leaveState);
          }
        },
        {
          name: 'configure',
          url: '/configure',
          params: {
            backState: '^.form'
          },
          views: {
            '@$appRoot': {
              templateUrl: 'app/apps/commons/fields/configure-fields.html',
              controller: 'ConfigureFieldsController',
              controllerAs: '$ctrl'
            }
          },
          resolve: {
            fields: /*@ngInject*/ function (FieldModel, app) {
              var context = {
                senderId: app.senderId,
                appId: app.id,
                appKey: app.key
              };
              return FieldModel.get(context);
            }
          }
        }
      ],
      settings: {
        templateUrl: 'app/apps/form/form-settings.html',
        controller: 'FormSettingsController',
        controllerAs: '$ctrl'
      }
    });
  }

  function registerTarget(targetServiceProvider) {
    targetServiceProvider.register('form-entry', /*@ngInject*/ function (params, $state, appRegistry) {
      return params.go && params.go === true ? $state.go(
          appRegistry.getRootStateName(params.appKey, params.senderType) + '.list.details',
          {idOrSlug: params.senderId, appIdOrSlug: params.appId, id: params.id}) : $state.href(
          appRegistry.getRootStateName(params.appKey, params.senderType) + '.list.details',
          {idOrSlug: params.senderId, appIdOrSlug: params.appId, id: params.id});
    });
  }

})(angular);
