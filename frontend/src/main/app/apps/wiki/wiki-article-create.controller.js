(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.wiki')
      .controller('WikiArticleCreateController', WikiArticleCreateController);

  /**
   * Controller for creating a wiki article.
   *
   * @constructor
   */
  function WikiArticleCreateController($q, $scope, $state, $timeout, widgetLayoutService, currentUser, article, app,
                                       sender,
                                       wikiArticleTranslationService) {
    var vm = this;

    vm.$onInit = init;
    vm.article = article;
    vm.app = app;
    vm.sender = sender;
    vm.originalArticle = angular.copy(vm.article);
    vm.editMode = true;
    vm.loading = true;
    vm.simpleMode = true;

    vm.cancel = cancel;
    vm.save = save;
    vm.onLanguageChange = _onLanguageChange;
    vm.onLanguageDeleted = _onLanguageDeleted;
    vm.updateValidity = _updateValidity;
    vm.isTranslationRequired = _isTranslationRequired;

    function cancel() {
      widgetLayoutService.cancel($scope);
      vm.loading = false;
      vm.editMode = false;

      $state.go('^');
    }

    function save() {
      wikiArticleTranslationService.prepareTranslations(vm);

      var deferred = $q.defer();
      if (vm.loading) {
        deferred.resolve();
      } else {
        vm.loading = true;

        if (article.id === article.parentId) {
          article.parentId = null;
        }

        vm.article.create().then(function () {
          $timeout(function () { // wait to sync id to widget layout and slots
            widgetLayoutService.save($scope).then(function () {
              vm.editMode = false;
              $state.go('^.view', {id: vm.article.id, currentLanguage: vm.defaultLanguage}).then(function () {
                deferred.resolve();
              });
            }).catch(function () {
              widgetLayoutService.edit($scope);
              deferred.reject();
            }).finally(function () {
              vm.loading = false;
            });
          });
        }).catch(function () {
          deferred.reject();
          vm.loading = false;
        });
      }

      return deferred.promise;
    }

    /* ===== PRIVATE METHODS ===== */

    function init() {
      widgetLayoutService.onload($scope).then(function () {
        widgetLayoutService.edit($scope);
      }).finally(function () {
        vm.loading = false;
      });

      wikiArticleTranslationService.initLanguages(vm, currentUser).then(function () {
        vm.languageInitialised = {};
      });
    }

    function _updateValidity(key, value) {
      wikiArticleTranslationService.updateValidity(vm, key, value);
    }

    function _isTranslationRequired(language) {
      return wikiArticleTranslationService.isTranslationRequired(vm, language);
    }

    function _onLanguageDeleted(language) {
      return wikiArticleTranslationService.onLanguageDeleted($scope, vm, language);
    }

    function _onLanguageChange(copyFromDefault) {
      wikiArticleTranslationService.onLanguageChange($scope, vm, copyFromDefault);
    }
  }

})(angular);
