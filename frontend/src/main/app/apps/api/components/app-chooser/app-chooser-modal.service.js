(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.api')
      .factory('appChooserModalService', appChooserModalService);

  /**
   * @ngdoc service
   * @name coyo.apps.api.appChooserModalService
   *
   * @description
   * This service opens a modal in which the user can choose and configure a new app. The modal consists of two
   * pages. One for selecting the type of the new app and one for specifying the settings of the chosen app type.
   *
   * @requires modalService
   * @requires sender
   */
  function appChooserModalService(modalService) {
    return {
      open: open
    };

    /**
     * @ngdoc method
     * @name coyo.apps.api.appChooserModalService#open
     * @methodOf coyo.apps.api.appChooserModalService
     *
     * @description
     * Opens the modal to choose and configure an app from.
     *
     * @param {object} sender
     * The sender context the newly created app should belong to.
     *
     * @returns {object}
     * Returns a promise with the saved app.
     */
    function open(sender) {
      return modalService.open({
        size: 'lg',
        templateUrl: 'app/apps/api/components/app-chooser/app-chooser-modal.html',
        controller: 'AppChooserModalController',
        windowTopClass: 'app-chooser-window',
        resolve: {
          sender: /*@ngInject*/ function (SenderModel) {
            return SenderModel.get(sender.id); // to load the "origin" sender which contains the translations
          },
          apps: /*@ngInject*/ function (SenderModel) {
            return new SenderModel({id: sender.id}).getApps();
          }
        }
      }).result;
    }
  }

})(angular);
