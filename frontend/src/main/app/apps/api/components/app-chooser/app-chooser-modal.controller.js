(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.api')
      .controller('AppChooserModalController', AppChooserModalController);

  function AppChooserModalController($q, $rootScope, $uibModalInstance, appRegistry, sender, apps) {
    var vm = this;
    var senderAppInformation = [];

    vm.apps = apps;
    vm.sender = sender;

    vm.$onInit = onInit;

    vm.senderType = sender.typeName;
    vm.saveCallbacks = {
      onBeforeSave: function () {
        return $q.resolve();
      }
    };

    vm.isSenderTranslated = false;
    vm.defaultLanguage;
    vm.currentLanguage;
    vm.languages = {};

    vm.canCreateApp = canCreateApp;
    vm.getAppTypeInformation = getAppTypeInformation;
    vm.goBack = goBack;
    vm.save = save;

    vm.isTranslationRequired = isTranslationRequired;
    vm.updateValidity = updateValidity;
    vm.onAppSelected = onAppSelected;

    /**
     * Checks whether the app can be created or enough instances of it have already been created.
     *
     * @param {object} app The app
     */
    function canCreateApp(app) {
      var appTypeInformation = _.find(senderAppInformation, {key: app.key});
      return !appTypeInformation || appTypeInformation.allowedInstances <= 0 || appTypeInformation.instances < appTypeInformation.allowedInstances;
    }

    /**
     * Returns global type information of the app.
     *
     * @param {object} app The app
     */
    function getAppTypeInformation(app) {
      return _.find(senderAppInformation, {key: app.key}) || {};
    }

    function goBack() {
      vm.selectedApp = null;
      initTranslations();
    }

    function save() {
      if (canCreateApp(vm.selectedApp)) {
        vm.selectedApp.senderId = sender.id;
        vm.selectedApp.prepareTranslationsForSave(vm);
        return vm.saveCallbacks.onBeforeSave().then(function () {
          return vm.selectedApp.createWithPermissions(['manage']).then(function (app) {
            $rootScope.$emit('app:created', app);
            $uibModalInstance.close(app);
          });
        });
      }
      return $q.reject();
    }

    function isTranslationRequired(language) {
      return vm.sender.isTranslationRequired(vm.languages, vm.currentLanguage, language);
    }

    function updateValidity(key, valid) {
      vm.languages[key].valid = valid;
    }

    function onAppSelected() {
      vm.languages[vm.defaultLanguage].translations.name = vm.selectedApp.name;
    }

    function onInit() {
      senderAppInformation = _.map(_.countBy(apps, 'key'), function (count, appKey) {
        return {
          key: appKey,
          instances: count,
          allowedInstances: appRegistry.get(appKey).allowedInstances,
          allowedInstancesErrorMessage: appRegistry.get(appKey).allowedInstancesErrorMessage
        };
      });
      initTranslations();
    }

    function initTranslations() {
      vm.sender.initTranslations(vm);
      vm.currentLanguage = vm.defaultLanguage;
    }
  }
})(angular);
