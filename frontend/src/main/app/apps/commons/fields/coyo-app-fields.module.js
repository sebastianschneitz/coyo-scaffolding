(function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name coyo.apps.commons.fields
   *
   * @description
   * Defines and registers field types for the list app.
   */
  angular
      .module('coyo.apps.commons.fields', [
        'coyo.base',
        'commons.i18n',
        'commons.ui'
      ])
      .config(registerCheckboxField)
      .config(registerDateField)
      .config(registerFileField)
      .config(registerLinkField)
      .config(registerNumberField)
      .config(registerOptionsField)
      .config(registerTextField)
      .config(registerUserField);

  function registerCheckboxField(fieldTypeRegistryProvider) {
    fieldTypeRegistryProvider.register({
      key: 'checkbox',
      title: 'APP.COMMONS.FIELDS.CHECKBOX.TITLE',
      description: 'APP.COMMONS.FIELDS.CHECKBOX.DESCRIPTION',
      icon: 'zmdi-check-square',
      sortOn: '',
      defaultValue: false,
      settings: {
        templateUrl: 'app/apps/commons/fields/settings/checkbox-field-settings.html'
      },
      form: {
        templateUrl: 'app/apps/commons/fields/form/checkbox-field.html'
      },
      render: {
        templateUrl: 'app/apps/commons/fields/render/checkbox.html',
        controller: 'CheckboxRenderController'
      },
      inlineEdit: {
        templateUrl: 'app/apps/commons/fields/inline-edit/checkbox.html'
      }
    });
  }

  function registerDateField(fieldTypeRegistryProvider) {
    fieldTypeRegistryProvider.register({
      key: 'date',
      title: 'APP.COMMONS.FIELDS.DATE.TITLE',
      description: 'APP.COMMONS.FIELDS.DATE.DESCRIPTION',
      icon: 'zmdi-calendar',
      sortOn: '',
      settings: {
        templateUrl: 'app/apps/commons/fields/settings/date-field-settings.html'
      },
      form: {
        templateUrl: 'app/apps/commons/fields/form/date-field.html',
        controller: 'DateFieldController',
        controllerAs: '$ctrl'
      },
      render: {
        templateUrl: 'app/apps/commons/fields/render/date.html'
      }
    });
  }

  function registerFileField(fieldTypeRegistryProvider) {
    fieldTypeRegistryProvider.register({
      key: 'file',
      title: 'APP.COMMONS.FIELDS.FILE.TITLE',
      description: 'APP.COMMONS.FIELDS.FILE.DESCRIPTION',
      icon: 'zmdi-file',
      sortOn: 'displayName.raw',
      settings: {
        templateUrl: 'app/apps/commons/fields/settings/file-field-settings.html'
      },
      form: {
        templateUrl: 'app/apps/commons/fields/form/file-field.html',
        controller: 'FileFieldController',
        controllerAs: '$ctrl'
      },
      render: {
        templateUrl: 'app/apps/commons/fields/render/file.html',
        controller: 'FileFieldRenderController'
      }
    });
  }

  function registerLinkField(fieldTypeRegistryProvider) {
    fieldTypeRegistryProvider.register({
      key: 'link',
      title: 'APP.COMMONS.FIELDS.LINK.TITLE',
      description: 'APP.COMMONS.FIELDS.LINK.DESCRIPTION',
      icon: 'zmdi-link',
      sortOn: '',
      settings: {
        templateUrl: 'app/apps/commons/fields/settings/link-field-settings.html'
      },
      form: {
        templateUrl: 'app/apps/commons/fields/form/link-field.html'
      },
      render: {
        templateUrl: 'app/apps/commons/fields/render/link.html'
      }
    });
  }

  function registerNumberField(fieldTypeRegistryProvider) {
    fieldTypeRegistryProvider.register({
      key: 'number',
      title: 'APP.COMMONS.FIELDS.NUMBER.TITLE',
      description: 'APP.COMMONS.FIELDS.NUMBER.DESCRIPTION',
      icon: 'zmdi-n-1-square',
      sortOn: '',
      settings: {
        templateUrl: 'app/apps/commons/fields/settings/number-field-settings.html'
      },
      form: {
        templateUrl: 'app/apps/commons/fields/form/number-field.html'
      },
      render: {
        templateUrl: 'app/apps/commons/fields/render/number.html'
      }
    });
  }

  function registerOptionsField(fieldTypeRegistryProvider) {
    fieldTypeRegistryProvider.register({
      key: 'options',
      title: 'APP.COMMONS.FIELDS.OPTIONS.TITLE',
      description: 'APP.COMMONS.FIELDS.OPTIONS.DESCRIPTION',
      icon: 'zmdi-dns',
      sortOn: 'displayName.raw',
      settings: {
        templateUrl: 'app/apps/commons/fields/settings/options-field-settings.html',
        controller: 'OptionsFieldSettingsController',
        controllerAs: '$ctrl'
      },
      form: {
        templateUrl: 'app/apps/commons/fields/form/options-field.html'
      },
      render: {
        templateUrl: 'app/apps/commons/fields/render/options.html',
        controller: 'OptionsFieldRenderController'
      }
    });
  }

  function registerTextField(fieldTypeRegistryProvider) {
    fieldTypeRegistryProvider.register({
      key: 'text',
      title: 'APP.COMMONS.FIELDS.TEXT.TITLE',
      description: 'APP.COMMONS.FIELDS.TEXT.DESCRIPTION',
      icon: 'zmdi-sort-amount-desc',
      sortOn: 'raw',
      settings: {
        templateUrl: 'app/apps/commons/fields/settings/text-field-settings.html'
      },
      form: {
        templateUrl: 'app/apps/commons/fields/form/text-field.html'
      },
      render: {
        templateUrl: 'app/apps/commons/fields/render/text.html'
      }
    });
  }

  function registerUserField(fieldTypeRegistryProvider) {
    fieldTypeRegistryProvider.register({
      key: 'user',
      title: 'APP.COMMONS.FIELDS.USER.TITLE',
      description: 'APP.COMMONS.FIELDS.USER.DESCRIPTION',
      icon: 'zmdi-accounts',
      sortOn: 'displayName.raw',
      settings: {
        templateUrl: 'app/apps/commons/fields/settings/user-field-settings.html'
      },
      form: {
        templateUrl: 'app/apps/commons/fields/form/user-field.html'
      },
      render: {
        templateUrl: 'app/apps/commons/fields/render/user.html'
      }
    });
  }

})(angular);
