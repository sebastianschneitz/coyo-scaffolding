(function (angular) {
  'use strict';

  angular
      .module('commons.i18n.custom')
      .factory('coyoTranslationLoader', coyoTranslationLoader);

  /**
   * @ngdoc service
   * @name commons.i18n.custom.coyoTranslationLoader
   *
   * @description
   * This factory provides a custom loader for angular-translate.
   * The loader fetches the translations from the backend and stores in the translation registry.
   * These original translations are extended with the custom translations the administrator provided for the current user's language.
   * The final translation tables are then returned to angular-translate under the key of one of the default languages
   * (en, de or nl) chosen (as fallback).
   *
   * @requires $q
   * @requires $localStorage
   * @requires $injector
   * @requires coyo.domain.LanguagesModel
   * @requires commons.i18n.custom.languageDeterminer
   * @requires coyo.domain.I18nModel
   */
  function coyoTranslationLoader($q, $localStorage, $injector, LanguagesModel, I18nModel, translationRegistry,
                                 languageDeterminer) {
    var cachedTranslations = {};

    function toTranslationMap(translations) {
      return _.chain(translations).keyBy('key').mapValues('translation').value();
    }

    function loadOriginalTranslations(lang) {
      return I18nModel.getTranslations(lang).then(function (result) {
        var translations = result.data;
        translationRegistry.addTranslations(lang, translations);
        return translations;
      });
    }

    function loadTranslationsAndOverrides(languageKey) {
      return loadOriginalTranslations(languageKey).then(function (originalTranslations) {
        return loadOverrides(languageKey).then(function (overrides) {
          return angular.extend({}, originalTranslations, overrides);
        });
      });
    }

    function loadOverrides(languageKey) {
      return LanguagesModel.getTranslations(languageKey).then(function (translations) {
        return toTranslationMap(translations);
      });
    }

    return function (options) {
      var userLanguage = options.key;
      var promises = [];
      if (cachedTranslations[userLanguage]) {
        return cachedTranslations[userLanguage];
      }
      cachedTranslations[userLanguage] = languageDeterminer.getLanguageKeys(userLanguage).then(function (determinedLanguages) {
        determinedLanguages.forEach(function (determinedLanguage) {
          determinedLanguage.overrideOnly
            ? promises.push(loadOverrides(determinedLanguage.languageKey))
            : promises.push(loadTranslationsAndOverrides(determinedLanguage.languageKey));
        });
        return $q.all(promises).then(function (translations) {
          var finalTranslation = {};
          translations.forEach(function (translation) {
            angular.extend(finalTranslation, translation);
          });
          return finalTranslation;
        });
      });

      return cachedTranslations[userLanguage];
    };
  }

})(angular);
