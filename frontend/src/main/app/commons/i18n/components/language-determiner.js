(function (angular) {
  'use strict';

  angular
      .module('commons.i18n.custom')
      .factory('languageDeterminer', languageDeterminer);

  /**
   * @ngdoc service
   * @name commons.i18n.custom.languageDeterminer
   *
   * @description
   * Determines other languages that might need to be loaded to show a specific language. Call `getLanguageKeys` to get
   * a list of all necessary languages in the correct loading and merging order.
   *
   * @requires coyo.domain.I18nModel
   */
  function languageDeterminer(I18nModel, LanguagesModel) {
    function getDefaultLanguage() {
      return LanguagesModel.getDefaultLanguage().then(function (defaultLanguageObject) {
        return defaultLanguageObject && defaultLanguageObject.language.toLowerCase();
      });
    }

    function getFallbackLanguage() {
      return 'en';
    }

    /**
     * @ngdoc method
     * @name commons.i18n.custom.languageDeterminer#getLanguageKeys
     * @methodOf commons.i18n.custom.languageDeterminer
     *
     * @description
     * Determines the relevant language keys in the correct loading and merging order.
     * The first language to load is always the fallback language.
     * The second language to load is the system default language (if exists and different to the fallback language).
     * The third language to load is the user language (if exists and different to the default and fallback language).
     *
     * @returns {promise} Promise resolving to an array of strings in the order described above e.g. ["en", "de", "nl"].
     *
     * @param {string} language
     * The key of the required language (user language)
     */
    function getLanguageKeys(userLanguage) {
      return I18nModel.getAvailableLanguages().then(function (result) {
        var availableLanguages = result.data;
        return getDefaultLanguage().then(function (defaultLanguage) {
          var fallbackLanguage = getFallbackLanguage();
          var determinedLanguages;
          if (userLanguage === fallbackLanguage) {
            determinedLanguages = [fallbackLanguage];
          } else if (defaultLanguage === fallbackLanguage) {
            determinedLanguages = [fallbackLanguage, userLanguage];
          } else if (userLanguage === defaultLanguage) {
            determinedLanguages = [fallbackLanguage, defaultLanguage];
          } else {
            determinedLanguages = [fallbackLanguage, defaultLanguage, userLanguage];
          }
          return determinedLanguages.map(function (key) {
            return {languageKey: key, overrideOnly: !_.includes(availableLanguages, key)};
          });
        });
      });
    }

    return {
      getLanguageKeys: getLanguageKeys
    };
  }

})(angular);
