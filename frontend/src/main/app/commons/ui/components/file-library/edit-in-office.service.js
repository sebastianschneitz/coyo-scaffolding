(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .constant('officeFileExtensions', officeFileExtensions())
      .factory('editInOfficeService', editInOfficeService);

  function officeFileExtensions() {
    return {
      'dot': 'ms-word',
      'dotm': 'ms-word',
      'dotx': 'ms-word',
      'doc': 'ms-word',
      'docx': 'ms-word',
      'docm': 'ms-word',
      'xls': 'ms-excel',
      'xlsx': 'ms-excel',
      'xlsm': 'ms-excel',
      'xlsb': 'ms-excel',
      'pot': 'ms-powerpoint',
      'potm': 'ms-powerpoint',
      'potx': 'ms-powerpoint',
      'ppt': 'ms-powerpoint',
      'pptx': 'ms-powerpoint',
      'ppsx': 'ms-powerpoint',
      'pptm': 'ms-powerpoint',
      'vsd': 'ms-visio',
      'vsdx': 'ms-visio'
    };
  }

  function editInOfficeService(backendUrlService, fileService, $q, $http, $window, $timeout, coyoEndpoints, modalService, officeFileExtensions) {
    return {
      editInOffice: editInOffice,
      canEditInOffice: canEditInOffice
    };

    function editInOffice(file) {
      if (file.locked) {
        return;
      }

      var subscription;
      var progressModal = modalService.note({
        title: 'FILE_LIBRARY.EDIT_IN_OFFICE_OPENING.TITLE',
        text: 'FILE_LIBRARY.EDIT_IN_OFFICE_OPENING.TEXT'
      });

      _requestAccessToken(file)
          .then(function (response) {
            var baseUrl = backendUrlService.getUrl() || $window.location.origin;
            var url = encodeURI(
                _getProtocolHandler(file) +
                ':ofe|u|' +
                baseUrl +
                coyoEndpoints.webdav.edit
                    .replace('{{accessCode}}', response.data.accessCode)
                    .replace('{{senderId}}', file.senderId)
                    .replace('{{fileId}}', file.id)
                    .replace('{{fileName}}', file.name));

            var defer = $q.defer();
            var promiseResolved = false;
            subscription = fileService.subscribeToLock(file, function () {
              promiseResolved = true;
              defer.resolve(response);
            });

            // reject promise after 30 seconds (timeout!)
            $timeout(function () {
              if (!promiseResolved) {
                defer.reject();
              }
            }, 30000);

            $window.open(url, '_self');
            return defer.promise;
          }).then(function () {
            progressModal.dismiss();
          }).catch(function () {
            progressModal.dismiss();
            modalService.note({
              title: 'FILE_LIBRARY.EDIT_IN_OFFICE_FAILED.TITLE',
              text: 'FILE_LIBRARY.EDIT_IN_OFFICE_FAILED.TEXT'
            });
          }).finally(function () {
            if (subscription) {
              subscription();
            }
          });
    }

    function canEditInOffice(file) {
      return angular.isDefined(_getProtocolHandler(file));
    }

    function _requestAccessToken(file) {
      var url = coyoEndpoints.webdav.requestAccessCode;
      url = url.replace('{{senderId}}', file.senderId)
          .replace('{{fileId}}', file.id)
          .replace('{{fileName}}', file.name);
      return $http.post(url);
    }

    function _getProtocolHandler(file) {
      return officeFileExtensions[file.extension];
    }
  }

})(angular);
