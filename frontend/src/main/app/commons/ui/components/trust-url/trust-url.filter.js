(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .filter('trustUrl', trustUrlFilter);

  /**
   * @ngdoc filter
   * @name commons.ui:trustUrl
   * @function
   *
   * @description
   * Parses a URL as trusted resource URL via `$sce`.
   *
   * @param {string} url The URL.
   *
   * @requires $sce
   */
  function trustUrlFilter($sce) {
    function _stripJavascript(url) {
      if (_.size(url)) {
        while (url.startsWith('javascript:')) {
          url = url.replace(/^javascript:/, '');
        }
      }
      return url;
    }

    function _stripDataUrls(url) {
      if (_.size(url)) {
        while (url.startsWith('data:')) {
          url = url.replace(/^data:/, '');
        }
      }
      return url;
    }

    return function (url) {
      url = _stripJavascript(url);
      url = _stripDataUrls(url);
      return url ? $sce.trustAsResourceUrl(url) : url;
    };
  }

})(angular);
