(function (angular) {
  'use strict';

  angular.module('commons.ui')
      .factory('senderNavigationUpdateService', senderNavigationUpdateService);

  /**
   * @ngdoc service
   * @name commons.ui.senderNavigationUpdateService
   *
   * @description
   * Can be used to update app navigation and/or prepare app navigation data response to be used in frontend
   *
   * @requires SenderModel
   */
  function senderNavigationUpdateService(SenderModel) {

    return {
      updateNavigation: updateNavigation,
      prepareNavigationUpdateResponse: prepareNavigationUpdateResponse
    };

    /**
     * @ngdoc method
     * @name commons.ui.senderNavigationUpdateService#updateNavigation
     * @methodOf commons.ui.senderNavigationUpdateService
     *
     * @description
     * Updates the app navigation of the affected sender.
     *
     * @param {array} apps A list of the full apps that used in the navigation.
     * @param {array} appNavigationGroups A list of all navigation groups.
     * @param {string} senderId The id of the sender the app navigation belongs to.
     * @param {boolean} includeTranslationsUpdates A flag to process also the translations updates. Default false.
     *
     * @returns {promise} The promise of the sender app navigation update request.
     */
    function updateNavigation(apps, appNavigationGroups, senderId, includeTranslationsUpdates) {
      var senderModel = new SenderModel({id: senderId});
      var navigationGroups = navigationPreUpdatePreparation(apps, appNavigationGroups);
      return senderModel.updateNavigation(navigationGroups, !!includeTranslationsUpdates);
    }

    /**
     * @ngdoc method
     * @name commons.ui.senderNavigationUpdateService#prepareNavigationUpdateResponse
     * @methodOf commons.ui.senderNavigationUpdateService
     *
     * @description
     * Transform the app navigation update result in the same structure that was given to the update method.
     *
     * @param {array} navigationGroups A list of all navigation groups in a structure provided by the backend.
     *
     * @returns {Array} A list of the given navigation groups in a structure used by the frontend.
     */
    function prepareNavigationUpdateResponse(navigationGroups) {
      var appNavigationGroups = [];
      _.forEach(navigationGroups, function (navigationGroup) {
        appNavigationGroups.push({
          name: navigationGroup.name,
          apps: _.map(navigationGroup.apps, 'id')
        });
      });
      return appNavigationGroups;
    }

    function navigationPreUpdatePreparation(apps, appNavigationGroups) {
      var navigationGroups = [];
      _.forEach(appNavigationGroups, function (appNavigationGroup) {
        navigationGroups.push({
          name: appNavigationGroup.name,
          apps: getAppsByAppIds(apps, appNavigationGroup.apps)
        });
      });
      return navigationGroups;
    }

    function getAppsByAppIds(apps, appIds) {
      return _.chain(appIds).map(getAppWithIdAndName).filter(isValidApp).value();

      function getAppWithIdAndName(id) {
        return _.pick(_.find(apps, {id: id}), ['id', 'name']);
      }

      /**
       * To remove apps that are listed in the sender's app navigation but no more existing as app in the system.
       * The affected apps are empty results of getAppWithIdAndName().
       *
       * @param {object} app The result of getAppWithIdAndName().
       * @returns {boolean} True if the app is valid, otherwise false.
       */
      function isValidApp(app) {
        return angular.isDefined(app) && _.isObject(app) && !_.isEmpty(app);
      }
    }
  }
})(angular);
