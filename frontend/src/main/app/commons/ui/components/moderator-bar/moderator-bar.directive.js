(function () {
  'use strict';

  angular
      .module('commons.ui')
      .directive('oyocModeratorBar', moderatorBar)
      .controller('moderatorBarController', moderatorBarController);

  /**
   * @ngdoc directive
   * @name commons.ui.moderatorBar:moderatorBar
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Displays a bar on the bottom of the screen when a user changes to moderator mode. The moderator mode can be
   * disabled with a link on the bar. Note that disabling the moderator mode causes a full page reload to happen.
   *
   * @requires $window
   * @requires commons.auth.authService
   */
  function moderatorBar() {
    return {
      restrict: 'E',
      templateUrl: 'app/commons/ui/components/moderator-bar/moderator-bar.html',
      scope: {},
      bindToController: {},
      controller: 'moderatorBarController',
      controllerAs: '$ctrl'
    };
  }

  function moderatorBarController($state, $localStorage, $scope, authService) {
    var vm = this;
    vm.deactivate = deactivate;

    $scope.$watch(function () {
      return _.get($localStorage, 'messagingSidebar.compact', true);
    }, function (newVal) {
      vm.compact = newVal;
    });

    /**
     * Disables the moderator mode.
     * For all new permissions to be shown properly, a reload is needed.
     * As updates of the user from web sockets notification may interfere with the reload (see COYOFOUR-7171)
     * we need to unsubscribe from the socket subscriptions first, and subscribe to it again if no reload happened.
     */
    function deactivate() {
      if (vm.user) {
        authService.unsubscribeFromUserUpdate();
        vm.user.setModeratorMode(false).then(function () {
          authService.getUser(true);
          $state.reload();
        }).finally(function () {
          authService.subscribeToUserUpdate();
        });
      }
    }

    (function _init() {
      authService.getUser().then(function (user) {
        vm.user = user;
      });
    })();
  }
})();
