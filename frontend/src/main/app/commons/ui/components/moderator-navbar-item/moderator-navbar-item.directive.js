(function () {
  'use strict';

  angular
      .module('commons.ui')
      .directive('oyocModeratorNavbarItem', moderatorNavbarItem)
      .controller('ModeratorNavbarItemController', ModeratorNavbarItemController);

  /**
   * @ngdoc directive
   * @name commons.ui.moderatorNavbarItem:moderatorNavbarItem
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Displays a navigation item for users (with moderator permission) to toggle the moderator mode on or off. Note that
   * enabling or disabling the moderator mode causes a full page reload to happen.
   *
   * @param {object} user
   * The user to toggle the moderator mode for.
   *
   * @requires $window
   *
   */
  function moderatorNavbarItem() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/commons/ui/components/moderator-navbar-item/moderator-navbar-item.html',
      scope: {},
      bindToController: {
        user: '<'
      },
      controller: 'ModeratorNavbarItemController',
      controllerAs: '$ctrl'
    };
  }

  function ModeratorNavbarItemController($injector, $state, authService, etagCacheService) {
    var vm = this;

    vm.toggleModeratorMode = toggleModeratorMode;

    /**
     * Enables or disables the moderator mode.
     * For all new permissions to be shown properly, a reload is needed.
     * As updates of the user from web sockets notification may interfere with the reload (see COYOFOUR-7171)
     * we need to unsubscribe from the socket subscriptions first, and subscribe to it again if no reload happened.
     */
    function toggleModeratorMode() {
      authService.unsubscribeFromUserUpdate();
      vm.user.setModeratorMode(!vm.user.moderatorMode).then(function () {
        authService.getUser(true);
        _clearEtagCache();
        $state.reload();
      }).finally(function () {
        authService.subscribeToUserUpdate();
      });
    }

    function _clearEtagCache() {
      // clear ng1 cache
      etagCacheService.clearAll();
      // clear ngx cache
      $injector.get('ngxEtagCacheService').clearAll();
    }

  }
})();
