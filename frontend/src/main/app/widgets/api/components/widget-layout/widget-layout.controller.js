(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.api')
      .controller('WidgetLayoutController', WidgetLayoutController);

  function WidgetLayoutController($q, $scope, $timeout, modalService, widgetEditService, WidgetLayoutModel, WidgetModel,
                                  utilService, $injector) {
    var vm = this;

    vm.$onInit = onInit;

    function onInit() {

      vm.renderStyle = vm.renderStyle || 'plain';
      vm.loading = true;
      vm.loadingPromises = [];
      vm.editMode = false;
      vm.canManage = angular.isDefined(vm.canManage) ? vm.canManage : true;
      vm.globalEvents = angular.isDefined(vm.globalEvents) ? vm.globalEvents : true;

      vm.addRow = addRow;
      vm.removeRow = removeRow;
      vm.isRowVisible = isRowVisible;
      vm.buildSlotName = buildSlotName;

      var createMode = vm.createMode; // preserve initial status as it might change before the save event is handled

      var layoutResponse;
      if (createMode) {
        layoutResponse = $q.resolve(new WidgetLayoutModel({
          settings: {rows: [{slots: [{cols: 12}]}]}
        }));
      } else {
        layoutResponse = new WidgetLayoutModel({name: vm.layoutName}).getWithWidgetsFromCache();
      }

      layoutResponse.then(function (layout) {
        vm.layout = layout;

        angular.forEach(vm.layout.settings.rows, function (row) {
          if (!row.name) {
            row.name = utilService.uuid();
          }
        });

        $scope.$emit('widget-layout:loaded');
      }).catch(function (error) {
        $scope.$emit('widget-layout:loadError', error);
      }).finally(function () {
        $timeout(function () {
          $q.all(vm.loadingPromises).finally(function () {
            vm.loading = false;
          });
        });
      });

      var unsubscribeEditFn = $scope.$on('widget-slot:edit', function (event, isGlobal) {
        if (!vm.globalEvents && isGlobal) {
          return;
        }
        layoutResponse.then(function () {
          vm.layout.snapshot();
          vm.editMode = true;
        });
      });

      var unsubscribeCancelFn = $scope.$on('widget-slot:cancel', function (event, isGlobal) {
        if (!vm.globalEvents && isGlobal) {
          return;
        }
        vm.layout.rollback();
        vm.editMode = false;
      });

      var unsubscribeCollectFn = $scope.$on('widget-slot:collect', function (event, data, name) {
        if (name !== vm.layoutName || !vm.layout) {
          return;
        }

        vm.layout.name = vm.layoutName;
        data.layout.name = vm.layoutName;
        data.layout.parent = vm.parent;
        data.layout.isNew = createMode || vm.copyMode;
        data.layout.rows = _.map(_.filter(vm.layout.settings.rows, function (row) {
          return !row.$deleted;
        }), function (row) {
          return {
            name: row.name,
            slots: _.map(row.slots, function (slot) {
              prepareSlotName(slot);
              return {cols: slot.cols, name: slot.name};
            })
          };
        });
      });

      var unsubscribeFillFn = $scope.$on('widget-slot:fill-layout', function (event, data, name) {
        if (name === vm.layoutName) {
          vm.layout.name = vm.layoutName;
          vm.parent = data.layout.parent;
          vm.layout.settings.rows = data.layout.rows;
        }
      });

      var unsubscribeWidgetSaveFn = $scope.$on('widget:saved', function (event, widget) {
        // Add new added widgets to the layout that these widgets will also be considered for removal if the
        // widgets are no longer referenced. See widget-slot:save event handling below.
        _addWidgetToSlot(widget);
      });

      var unsubscribeWidgetCutPasteFn = $scope.$on('widget:paste', function (event, widget, originalSlot) {
        _addWidgetToSlot(widget);
        _removeWidgetFromSlot(widget, originalSlot.name);
      });

      var unsubscribeWidgetSlotSaveFn = $scope.$on('widget-slot:save', function (event, promises, isGlobal) {
        if (!vm.globalEvents && isGlobal) {
          return;
        }
        angular.extend(vm.layout, {
          name: vm.layoutName,
          parentId: angular.isDefined(vm.parent) ? vm.parent.id : null,
          parentType: angular.isDefined(vm.parent) ? vm.parent.typeName : null
        });
        _.forEach(vm.layout.settings.rows, function (row) {
          _.forEach(row.slots, function (slot) {
            prepareSlotName(slot);
          });
          if (row.$deleted && !!vm.layout.widgets) {
            _.forEach(row.slots, function (slot) {
              // delete no longer referenced widgets
              _.forEach(vm.layout.widgets[buildSlotName(slot.name)], function (widget) {
                new WidgetModel(widget).remove();
              });
              WidgetModel.removeCachedWidgets(buildSlotName(slot.name));
            });
          }
        });

        // remove no longer used rows
        _.remove(vm.layout.settings.rows, function (row) {
          return !!row.$deleted;
        });

        if (vm.layout.$deleted) {
          promises.push(vm.layout.remove());
        } else if (createMode || vm.copyMode) {
          promises.push(vm.layout.create());
        } else {
          promises.push(vm.layout.update());
        }

        vm.editMode = false;
      });

      if (vm.canManage && vm.globalEvents) {
        widgetEditService.register(vm.layoutName, !vm.parent);
      }

      $scope.$on('$destroy', function () {
        widgetEditService.deregister(vm.layoutName);
        unsubscribeEditFn();
        unsubscribeCancelFn();
        unsubscribeCollectFn();
        unsubscribeFillFn();
        unsubscribeWidgetSaveFn();
        unsubscribeWidgetSlotSaveFn();
        unsubscribeWidgetCutPasteFn();
        if (vm.widgetLayoutChooserSubscription && !vm.widgetLayoutChooserSubscription.closed) {
          vm.widgetLayoutChooserSubscription.unsubscribe();
        }
      });
    }

    function buildSlotName(slotName) {
      return 'layout-' + vm.layout.name + '-slot-' + slotName;
    }

    function prepareSlotName(slot) {
      if (slot.name && slot.name.indexOf('-undefined') === -1) {
        return;
      }
      slot.name = utilService.uuid();
    }

    function _addWidgetToSlot(widget) {
      if (vm.layout.widgets) {
        if (!vm.layout.widgets.hasOwnProperty(widget.slot)) {
          vm.layout.widgets[widget.slot] = [];
        }
        if (!_.find(vm.layout.widgets[widget.slot], ['id', widget.id])) {
          vm.layout.widgets[widget.slot].push(widget);
        }
      }
    }

    function _removeWidgetFromSlot(widget, originalSlotName) {
      if (vm.layout.widgets) {
        if (vm.layout.widgets.hasOwnProperty(originalSlotName) &&
            _.find(vm.layout.widgets[originalSlotName], ['id', widget.id])) {
          _.remove(vm.layout.widgets[originalSlotName], ['id', widget.id]);
        }
      }
    }

    // ------------------------------------------------------------------------------------------

    function getWidgetsLayoutChooserModalService() {
      return $injector.get('ngxWidgetsLayoutChooserModalService');
    }

    function addRow(index) {
      vm.widgetLayoutChooserSubscription = getWidgetsLayoutChooserModalService().openModal()
          .subscribe(function (rowType) {
            $scope.$apply(function () {
              if (rowType) {
                if (!vm.layout.settings.rows) {
                  vm.layout.settings.rows = [];
                }
                var row = {name: utilService.uuid(), slots: []};
                angular.forEach(rowType.slots, function (cols) {
                  row.slots.push({
                    name: utilService.uuid(),
                    cols: cols
                  });
                });
                vm.layout.settings.rows.splice(index, 0, row);
              }
            });
          });
    }

    function removeRow(index) {
      modalService.confirmDelete({
        title: 'WIDGETS.LAYOUT.ROW.DELETE',
        text: 'WIDGETS.LAYOUT.ROW.DELETE.CONFIRM'
      }).result.then(function () {
        vm.layout.settings.rows[index].$deleted = true;
      });
    }

    function isRowVisible(row) {
      return row.$deleted !== true;
    }
  }

})(angular);
