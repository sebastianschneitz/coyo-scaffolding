// Load main application
require('../../ngx/src/config');
require('../../build/cli/src/main/app');

// Setup ngMock
require('../../node_modules/angular-mocks/angular-mocks');
require('../../ngx/node_modules/@uirouter/angularjs');
require('./test.downgrade');
(function () {
  'use strict';

  angular.module('ui.router.upgrade', []);

  angular.module('ngMock').config(function ($compileProvider) {
    $compileProvider.preAssignBindingsEnabled(true);
  });

  angular.module('commons.i18n').config(function ($translateProvider) {
    $translateProvider.useLoader();
  });

  beforeEach(function () {
    // eslint-disable-next-line angular/window-service
    window.localStorage.clear();
  });

})();
