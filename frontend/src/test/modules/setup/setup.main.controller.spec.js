(function () {
  'use strict';

  var moduleName = 'coyo.setup';
  var targetName = 'SetupController';

  describe('module: ' + moduleName, function () {
    var $controller, $state, $httpBackend, $q, $injector;
    var setupService, backendUrlService, isSetUp;
    var emailPattern = '', passwordPattern = '';
    var model = {
      networkName: 'Some Cool Coyo Name',
      firstName: 'First',
      lastName: 'Name',
      email: 'first@name.de',
      password: 'demo1234',
      demoData: true
    };
    var response = {
      networkName: model.networkName,
      user: {
        firstName: model.firstName,
        lastName: model.lastName,
        email: model.email
      },
      demoData: model.demoData
    };

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$state_, _$q_, _$httpBackend_) {
        $controller = _$controller_;
        $state = _$state_;
        $q = _$q_;
        $httpBackend = _$httpBackend_;

        $injector = jasmine.createSpyObj('$injector', ['get']);
        $injector.get.and.returnValue(jasmine.createSpyObj('ngxNotificationService', ['success']));
        setupService = jasmine.createSpyObj('setupService', ['setUp']);
        setupService.setUp.and.returnValue($q.resolve(response));
        backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);

        isSetUp = false;

        spyOn($state, 'transitionTo');
      });
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl() {
        return $controller(targetName, {
          $state: $state,
          setupService: setupService,
          $injector: $injector,
          backendUrlService: backendUrlService,
          isSetUp: isSetUp,
          passwordPattern: passwordPattern,
          emailPattern: emailPattern
        }, {});
      }

      it('should leave the setup state if already set up', function () {
        // given
        isSetUp = true;

        // when
        buildCtrl();

        // then
        expect($state.transitionTo).toHaveBeenCalledTimes(1);
      });

      it('should go to the previous wizard step and stop at the first "welcome screen state"', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.wizard.active = 2;

        // when
        for (var i = 0; i < 5; ++i) {
          ctrl.back();
        }

        // then
        expect(ctrl.wizard.active).toEqual(-1);
      });

      it('should not go to the next wizard step if form is invalid', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.wizard.active = 0;
        ctrl.model = model;

        // when
        ctrl.next({$valid: false}, ctrl.model);

        // then
        expect(ctrl.wizard.active).toEqual(0);
      });

      it('should go to the next wizard step and set up at the final state, should set up correctly afterwards', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.wizard.active = -1;
        ctrl.model = model;

        $httpBackend.expectPOST(backendUrlService.getUrl() + '/setup', function (data) {
          var payload = angular.fromJson(data);
          expect(payload.networkName).toBe(model.networkName);
          expect(payload.firstName).toBe(model.firstName);
          expect(payload.lastName).toBe(model.lastName);
          expect(payload.email).toBe(model.email);
          expect(payload.password).toBe(model.password);
          expect(payload.demoData).toBe(model.demoData);
          return true;
        }).respond(201, $q.resolve(response));

        // when
        for (var i = 0; i < 4; ++i) {
          ctrl.next({$valid: true}, ctrl.model);
        }

        // then
        expect(ctrl.wizard.active).toEqual(2);
        // + seet HTTP POST expectation
      });
    });
  });
})();
