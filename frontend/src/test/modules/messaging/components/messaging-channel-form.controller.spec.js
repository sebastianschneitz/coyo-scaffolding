(function () {
  'use strict';

  var moduleName = 'coyo.messaging';
  var targetName = 'MessagingChannelFormController';

  describe('module: ' + moduleName, function () {
    var $controller;
    var UserModel;

    var userMock = {id: 123};
    var user2Mock = {id: 456, displayName: 'Second User'};
    var user3Mock = {id: 789, displayName: 'Third User'};

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _UserModel_) {
        $controller = _$controller_;
        UserModel = _UserModel_;
      });
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl(channel) {
        return $controller(targetName, {}, {
          currentUser: userMock,
          channel: channel
        });
      }

      it('should init controller', function () {
        // given
        spyOn(UserModel, 'searchWithFilter').and.callThrough();

        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // then
        expect(ctrl.userSearchTerm).toEqual('');
        expect(ctrl.channel).not.toBeUndefined();
        expect(UserModel.searchWithFilter).toHaveBeenCalledTimes(1);
      });

      it('should init controller with channel and filter members', function () {
        // given
        var channel = {
          type: 'GROUP',
          name: 'Foo Bar',
          members: [
            {deleted: true, user: userMock},
            {deleted: false, user: user2Mock},
            {deleted: false, user: user3Mock}
          ]
        };

        // when
        var ctrl = buildCtrl(channel);
        ctrl.$onInit();

        // then
        expect(ctrl.channel).toBe(channel);
        expect(ctrl.channel.members.length).toBe(2);
        expect(ctrl.userOptionsVisible).toBeFalsy();
      });

      it('should load user options initially', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        var page = {
          content: [userMock, user2Mock]
        };

        spyOn(UserModel, 'searchWithFilter').and.returnValue({
          then: function (callback) {
            callback(page);
            return {
              'finally': function (callback) {
                callback();
              }
            };
          }
        });

        // when
        ctrl.loadUserOptions();

        // expect
        expect(ctrl.userOptions).toEqual([user2Mock]);
      });

      it('should add user', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.channel.type = 'GROUP';
        ctrl.userSearchTerm = 'foo';
        ctrl.focusUserSearchTerm = false;
        ctrl.userOptions = [user2Mock];

        // when
        ctrl.addUser(user2Mock);
        ctrl.addUser(user2Mock); // should not add again here

        // then
        expect(ctrl.channel.members).toEqual([{user: user2Mock}]);
        expect(ctrl.userSearchTerm).toEqual('');
        expect(ctrl.focusUserSearchTerm).toBeTruthy();
        expect(ctrl.userOptions).toEqual([]);
      });

      it('should add user and create single channel', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.userOptions = [user2Mock];

        spyOn(ctrl.channel, 'save').and.callThrough();

        // when
        ctrl.addUser(user2Mock);

        // then
        expect(ctrl.channel.members).toEqual([{user: user2Mock}]);
        expect(ctrl.channel.save).toHaveBeenCalledTimes(1);
      });

      it('should remove user', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.focusUserSearchTerm = false;
        ctrl.channel.members = [{user: user2Mock}];

        // when
        ctrl.removeUser(user2Mock);

        // then
        expect(ctrl.channel.members).toEqual([]);
        expect(ctrl.focusUserSearchTerm).toBeTruthy();
      });

      it('should focus on user search field initially', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // then
        expect(ctrl.focusUserSearchTerm).toBeTruthy();
      });

      it('should focus on channel name field initially', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // then
        expect(ctrl.focusSubject).toBeTruthy();
      });

      it('should toggle membership for already existing members', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.channel.type = 'GROUP';
        ctrl.channel.members = [{user: userMock}, {user: user2Mock}];
        ctrl.userSearchTerm = 'foo';
        ctrl.focusUserSearchTerm = false;
        ctrl.userOptions = [user2Mock];

        // when
        ctrl.toggleMembership(user2Mock);

        // then
        expect(ctrl.channel.members).toEqual([{user: userMock}]);
        expect(ctrl.userSearchTerm).toEqual('');
        expect(ctrl.focusUserSearchTerm).toBeTruthy();
        expect(ctrl.userOptions).toEqual([]);
      });

      it('should toggle membership for new members', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.channel.type = 'GROUP';
        ctrl.channel.members = [];
        ctrl.userSearchTerm = 'foo';
        ctrl.focusUserSearchTerm = false;
        ctrl.userOptions = [userMock];

        // when
        ctrl.toggleMembership(userMock);

        // then
        expect(ctrl.channel.members).toEqual([{user: userMock}]);
        expect(ctrl.userSearchTerm).toEqual('');
        expect(ctrl.focusUserSearchTerm).toBeTruthy();
        expect(ctrl.userOptions).toEqual([]);
      });

      it('should save', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        ctrl.channel.name = 'foo';
        ctrl.channel.members = [user2Mock];

        spyOn(ctrl.channel, 'save').and.callThrough();

        // when
        ctrl.createChannel();

        // then
        expect(ctrl.channel.save).toHaveBeenCalledTimes(1);
      });

      it('should not save with zero users', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        ctrl.channel.type = 'GROUP';
        ctrl.channel.name = 'foo';

        spyOn(ctrl.channel, 'create').and.callThrough();

        // when
        ctrl.createChannel();

        // then
        expect(ctrl.channel.create).not.toHaveBeenCalled();
      });

      it('should not save without name', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        ctrl.channel.type = 'GROUP';
        ctrl.channel.members = [user2Mock];

        spyOn(ctrl.channel, 'create').and.callThrough();

        // when
        ctrl.createChannel();

        // then
        expect(ctrl.channel.create).not.toHaveBeenCalled();
      });

      it('should init edit form', function () {
        // given
        var channel = {
          type: 'GROUP',
          name: 'Foo Bar',
          members: [{user: userMock}, {user: user2Mock}]
        };

        // when
        var ctrl = buildCtrl(channel);
        ctrl.$onInit();

        // then
        expect(ctrl.channel).toBe(channel);
        expect(ctrl.userOptionsVisible).toBeFalsy();
      });
    });
  });
})();
