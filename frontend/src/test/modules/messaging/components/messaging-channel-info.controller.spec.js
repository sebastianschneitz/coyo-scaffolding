(function () {
  'use strict';

  var moduleName = 'coyo.messaging';
  var targetName = 'MessagingChannelInfoController';

  describe('module: ' + moduleName, function () {
    var $controller, $scope, $q;
    var MessageAttachmentModel, externalFileHandlerService;

    var userMock = {id: 123, displayName: 'First User'};
    var channelMock = {
      id: 'channel-123'
    };

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _MessageAttachmentModel_, $rootScope, _$q_) {
        $q = _$q_;
        $controller = _$controller_;
        $scope = $rootScope.$new();
        MessageAttachmentModel = _MessageAttachmentModel_;
      });
      externalFileHandlerService =
          jasmine.createSpyObj('externalFileHandlerService', ['isExternalFile', 'getExternalFileDetails']);
      externalFileHandlerService.isExternalFile.and.returnValue(false);
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl() {
        return $controller(targetName, {
          $scope: $scope,
          externalFileHandlerService: externalFileHandlerService
        }, {
          channel: channelMock,
          currentUser: userMock
        });
      }

      it('should init controller', function () {
        // given
        spyOn(MessageAttachmentModel, 'pagedQuery').and.callThrough();

        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // then
        expect(ctrl.currentUser).toBe(userMock);
        expect(ctrl.channel).toBe(channelMock);
        expect(ctrl.memberLimit).toBeDefined();
        expect(ctrl.attachmentData).toBeDefined();
        expect(MessageAttachmentModel.pagedQuery).toHaveBeenCalledTimes(1);
      });

      it('should load more attachments', function () {
        // given
        var att1 = {id: 'msg-1', created: 2};
        var att2 = {id: 'msg-2', created: 1};
        var page = {
          content: [att1, att2],
          totalElements: 1
        };

        spyOn(MessageAttachmentModel, 'pagedQuery').and.returnValue($q.resolve(page));

        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(MessageAttachmentModel.pagedQuery).toHaveBeenCalledTimes(1);
        expect(ctrl.attachmentData.attachments[0]).toBe(att1);
        expect(ctrl.attachmentData.attachments[1]).toBe(att2);
        expect(ctrl.attachmentData.currentPage).toBe(page);
        expect(ctrl.loading).toBeFalsy();
      });

      it('should not handle external files if no external file present', function () {
        // given
        var att1 = {id: 'msg-1', created: 2};
        var page = {
          content: [att1],
          totalElements: 1
        };

        spyOn(MessageAttachmentModel, 'pagedQuery').and.returnValue($q.resolve(page));

        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(externalFileHandlerService.isExternalFile).toHaveBeenCalledTimes(1);
        expect(externalFileHandlerService.getExternalFileDetails).not.toHaveBeenCalled();
        expect(ctrl.attachmentData.attachments[0].isExternalFile).toBeFalsy();
      });

      it('should handle external files', function () {
        // given
        var attachment = {id: 'id', created: 1};
        var page = {
          content: [attachment],
          totalElements: 1
        };
        var ctrl = buildCtrl();

        var fileMetaData = {
          fileSize: 123213,
          externalUrl: 'https://drive.google.com/file/123/view'
        };
        externalFileHandlerService.isExternalFile.and.returnValue(true);
        externalFileHandlerService.getExternalFileDetails.and.returnValue($q.resolve(fileMetaData));
        spyOn(MessageAttachmentModel, 'pagedQuery').and.returnValue($q.resolve(page));

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(externalFileHandlerService.isExternalFile).toHaveBeenCalledTimes(1);
        expect(externalFileHandlerService.getExternalFileDetails).toHaveBeenCalledTimes(1);
        expect(ctrl.attachmentData.attachments[0].metaData).toBe(fileMetaData);
        expect(ctrl.attachmentData.attachments[0].isExternalFile).toBeTruthy();
      });

      it('should handle http error 404 file not found', function () {
        // given
        var response = {status: 404};
        var attachment = {id: 'id', created: 1};
        var page = {
          content: [attachment],
          totalElements: 1
        };
        var ctrl = buildCtrl();

        externalFileHandlerService.isExternalFile.and.returnValue(true);
        externalFileHandlerService.getExternalFileDetails.and.returnValue($q.reject(response));
        spyOn(MessageAttachmentModel, 'pagedQuery').and.returnValue($q.resolve(page));

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(externalFileHandlerService.isExternalFile).toHaveBeenCalledTimes(1);
        expect(externalFileHandlerService.getExternalFileDetails).toHaveBeenCalledTimes(1);
        expect(_.size(ctrl.fileErrors)).toBe(1);
      });
    });
  });
})();
