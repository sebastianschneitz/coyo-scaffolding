(function () {
  'use strict';

  var moduleName = 'coyo.messaging';
  var targetName = 'MessageChannelHeaderController';

  describe('module: ' + moduleName, function () {
    var $controller, modalService, $rootScope;

    var userMock = {
      id: 123,
      displayName: 'First User',
      muted: false
    };

    var channelMock = {
      id: 'channel-123',
      leave: angular.noop,
      getMember: function () {
        return userMock;
      }
    };

    var msgSidebarMock = {
      home: angular.noop,
      switchView: function () {
        return angular.noop;
      }
    };

    beforeEach(function () {
      modalService = jasmine.createSpyObj('modalService', ['confirm']);
      modalService.confirm.and.returnValue({
        result: {
          then: angular.noop
        }
      });

      module(moduleName, function ($provide) {
        // provide dependencies for service
        $provide.value('modalService', modalService);
      });

      inject(function (_$controller_, _$rootScope_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
      });
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl() {
        return $controller(targetName, {}, {
          channel: channelMock,
          currentUser: userMock,
          msgSidebar: msgSidebarMock,
        });
      }

      it('should init controller', function () {
        // given

        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // then
        expect(ctrl.currentUser).toBe(userMock);
        expect(ctrl.channel).toBe(channelMock);
        expect(ctrl.member).toBeDefined();
        expect(ctrl.msgSidebar).toBe(msgSidebarMock);
        expect(ctrl.member.muted).toBe(false);
      });

      it('should leave channel', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        spyOn(ctrl.member, 'delete');

        // when
        ctrl.leaveChannel();

        // then
        expect(ctrl.member.delete).toHaveBeenCalledTimes(1);
      });

      it('should open a modal if an upload is running - open info and files', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // when
        ctrl.channelLocked = true;
        ctrl.openInfoAndFiles();

        // then
        expect(modalService.confirm).toHaveBeenCalled();
      });

      it('should open a modal if an upload is running - edit channel', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // when
        ctrl.channelLocked = true;
        ctrl.editChannel();

        // then
        expect(modalService.confirm).toHaveBeenCalled();
      });

      it('should open a modal if an upload is running - back button', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // when
        ctrl.channelLocked = true;
        ctrl.back();

        // then
        expect(modalService.confirm).toHaveBeenCalled();
      });

      it('should switch from a channel to channel info', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.msgSidebar.view = 'channel';
        spyOn(ctrl.msgSidebar, 'switchView').and.callThrough();

        // when
        ctrl.openInfoAndFiles();
        $rootScope.$apply();

        // then
        expect(ctrl.msgSidebar.switchView).toHaveBeenCalledWith('channelInfo');
      });

      it('should switch from a channel to channel edit', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.msgSidebar.view = 'channel';
        spyOn(ctrl.msgSidebar, 'switchView').and.callThrough();

        // when
        ctrl.editChannel();
        $rootScope.$apply();

        // then
        expect(ctrl.msgSidebar.switchView).toHaveBeenCalledWith('channelForm');
      });

      it('should switch from a channel to channel edit', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.msgSidebar.view = 'channelInfo';
        spyOn(ctrl.msgSidebar, 'switchView').and.callThrough();

        // when
        ctrl.editChannel();
        $rootScope.$apply();

        // then
        expect(ctrl.msgSidebar.switchView).toHaveBeenCalledWith('channelForm');
      });

      it('should switch back from channel to channels', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.msgSidebar.view = 'channel';
        spyOn(ctrl.msgSidebar, 'home').and.callThrough();

        // when
        ctrl.back();
        $rootScope.$apply();

        // then
        expect(ctrl.msgSidebar.home).toHaveBeenCalledTimes(1);
      });

      it('should switch back from channel info to channel', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.msgSidebar.view = 'channelInfo';
        spyOn(ctrl.msgSidebar, 'switchView').and.callThrough();

        // when
        ctrl.back();
        $rootScope.$apply();

        // then
        expect(ctrl.msgSidebar.switchView).toHaveBeenCalledWith('channel');
      });

      it('should mute channel', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.member.mute = function () {
          return {
            then: function (cb) {
              return cb();
            }
          };
        };

        // when
        ctrl.muteChannel();

        // then
        expect(ctrl.member.muted).toBe(true);
        expect(ctrl.member.id).toBe(123);
        expect(ctrl.member.channelId).toBe('channel-123');
      });

    });

  });

})();
