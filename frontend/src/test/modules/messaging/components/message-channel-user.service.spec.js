(function () {
  'use strict';

  var moduleName = 'coyo.messaging';

  describe('module: ' + moduleName, function () {

    var messageChannelUserService;
    var $httpBackend, backendUrlService;

    beforeEach(function () {
      module(moduleName);
    });

    beforeEach(inject(function (_$httpBackend_, _backendUrlService_, _messageChannelUserService_) {
      $httpBackend = _$httpBackend_;
      backendUrlService = _backendUrlService_;
      messageChannelUserService = _messageChannelUserService_;
    }));

    describe('service: messageChannelUserService', function () {

      it('should fetch members', function () {
        // given
        var expectedResponse = {key: 'members'};
        var channelId = 'channel-identifier';
        $httpBackend.whenGET(backendUrlService.getUrl() + '/web/message-channels/' + channelId
            + '/members?includeDeletedMembers=true&includeMembersWithMessagingPermission=false')
            .respond(200, expectedResponse);

        // when
        var actualResponse = null;
        messageChannelUserService.fetchDisabledUsers(channelId, true).then(function (response) {
          actualResponse = response.data;
        });
        $httpBackend.flush();

        // then
        // -- no exception should be thrown, otherwise request was not triggered
        expect(actualResponse).toEqual(expectedResponse);
      });

    });
  });
})();
