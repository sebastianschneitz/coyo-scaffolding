(function () {
  'use strict';

  var moduleName = 'coyo.messaging';
  var targetName = 'MessagingNavbarItemController';

  describe('module: ' + moduleName, function () {
    var $controller, $scope, $rootScope, $timeout, $q;
    var authService;
    var sidebarService;
    var socketService;
    var MessageModel;

    var userMock = {id: 123, displayName: 'First User'};

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _authService_, _sidebarService_, _socketService_, _MessageModel_, _$rootScope_, _$timeout_, _$q_) {
        authService = _authService_;
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $timeout = _$timeout_;
        $scope = $rootScope.$new();
        $q = _$q_;
        sidebarService = _sidebarService_;
        socketService = _socketService_;
        MessageModel = _MessageModel_;

        spyOn(authService, 'getUser').and.returnValue({
          then: function (callback) {
            callback(userMock);
          }
        });

        spyOn(authService, 'onGlobalPermissions').and.callFake(function (permissionNames, callback) {
          callback(true, userMock);
          return angular.noop;
        });
      });
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl() {
        return $controller(targetName, {
          $scope: $scope
        });
      }

      it('should load unread count and subscribe to changes', function () {
        // given
        spyOn(MessageModel, 'getUnreadCount').and.returnValue({
          then: function (cb) {
            cb({data: 10});
          }
        });
        spyOn(socketService, 'subscribe');

        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // then
        expect(MessageModel.getUnreadCount).toHaveBeenCalledWith(userMock.id);
        expect(socketService.subscribe).toHaveBeenCalledWith('/user/topic/messaging', jasmine.any(Function), 'channelStatusUpdated');
        expect(ctrl.unreadCount).toBe(10);
      });

      it('should open chat', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        spyOn(sidebarService, 'open');

        // when
        ctrl.openMessagingSidebar();

        // then
        expect(sidebarService.open).toHaveBeenCalledWith('messaging');
      });

      it('should update unread count on reconnect', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.unreadCount = 1;
        spyOn(MessageModel, 'getUnreadCount').and.returnValue($q.resolve({data: 2}));

        // when
        $rootScope.$emit('socketService:reconnected');
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(ctrl.unreadCount).toBe(2);
      });
    });
  });
})();
