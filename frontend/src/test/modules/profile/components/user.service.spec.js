(function () {
  'use strict';

  var moduleName = 'coyo.profile';
  var targetName = 'userService';

  describe('module: ' + moduleName, function () {
    var $httpBackend;
    var $timeout;
    var userService;
    var coyoEndpoints;
    var socketService;
    var backendUrlService;

    /* User mock */
    var user = {
      id: 1,
      oldPassword: 'old',
      newPassword: 'new',
      language: 'DE',
      firstName: 'Robert',
      lastName: 'Lang'
    };

    /* User (to be returned by the http request) mock */
    var userReturn = {
      id: user.id,
      email: 'test@test.com',
      language: 'EN',
      firstName: 'Roberto',
      lastName: 'Lango'
    };

    /* authService mock */
    var authService = {
      isAuthenticated: function () {
        return false;
      },
      getCurrentUserId: function () {
        return 'user-id';
      },
      getUser: function () {
        return {
          then: function (callback) {
            callback({
              id: '1',
              lang: 'EN',
              hasGlobalPermissions: function () {
                return true;
              }
            });
          }
        };
      },
      subscribeToUserUpdate: function () {}
    };

    beforeEach(function () {
      module(moduleName, function ($provide) {
        // provide the authService mock
        $provide.value('authService', authService);
        // $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });

      inject(function ($injector, _userService_, _coyoEndpoints_, _$httpBackend_, _socketService_, _backendUrlService_, _$timeout_) {
        coyoEndpoints = _coyoEndpoints_;
        userService = _userService_;
        $httpBackend = _$httpBackend_;
        socketService = _socketService_;
        backendUrlService = _backendUrlService_;
        $timeout = _$timeout_;

        $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
      });
    });

    it('should be registered', function () {
      expect(userService).not.toBeUndefined();
    });

    describe('service: ' + targetName, function () {
      /* getUserInfo */
      it('should be possible to get user information', function () {
        // given
        $httpBackend
            .expectGET(backendUrlService.getUrl() + coyoEndpoints.user.user.replace('{id}', user.id))
            .respond(200, userReturn);
        var _user = null;

        // when
        userService.getUserInfo(user).then(function (response) {
          _user = response.data;
        });
        $httpBackend.flush();

        // then
        expect(_user).toEqual(userReturn);
      });

      /* setUserName */
      it('should be possible to set the user\'s first and last name', function () {
        // given
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + coyoEndpoints.user.setUserName.replace('{id}', user.id), function (data) {
              var payload = angular.fromJson(data);
              expect(payload.firstName).toBe(user.firstName);
              expect(payload.lastName).toBe(user.lastName);
              return true;
            })
            .respond(200, userReturn);

        var result = null;

        // when
        userService.setUserName(user, user.firstName, user.lastName).then(function (data) {
          result = data;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual(userReturn);
      });

      /* setUserLanguage */
      it('should be possible to set the user language', function () {
        // given
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + coyoEndpoints.user.setLanguage.replace('{id}', user.id), function (data) {
              var payload = angular.fromJson(data);
              expect(payload.language).toBe(user.language);
              return true;
            })
            .respond(200, userReturn);

        var result = null;

        // when
        userService.setUserLanguage(user, user.language).then(function (data) {
          result = data;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual(userReturn);
      });

      /* setUserPassword */
      it('should be possible to set the user password', function () {
        // given
        $httpBackend
            .expectPOST(backendUrlService.getUrl() + coyoEndpoints.user.changePassword.replace('{id}', user.id), function (data) {
              var payload = angular.fromJson(data);
              expect(payload.oldPassword).toBe(user.oldPassword);
              expect(payload.newPassword).toBe(user.newPassword);
              return true;
            })
            .respond(200, userReturn);
        var _answer = null;

        // when
        userService.setUserPassword(user, user.oldPassword, user.newPassword).then(function (data) {
          _answer = data;
        });
        $httpBackend.flush();

        // then
        expect(_answer).not.toBeNull();
      });

      it('should get presence status', function () {
        // given
        var userId1 = 123;
        var userId2 = 456;
        var active = true;
        var callback1 = jasmine.createSpy();
        var callback2 = jasmine.createSpy();
        var callback3 = jasmine.createSpy();

        spyOn(socketService, 'receiveFrom').and.returnValue({then: function () {}});
        spyOn(socketService, 'subscribe');
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/users/presence-status?userIds=123&userIds=456').respond(200, {
          123: 'status1',
          456: 'status2'
        });

        // when
        userService.getPresenceStatus({id: userId1, active: active}, callback1);
        userService.getPresenceStatus({id: userId2, active: active}, callback2);
        userService.getPresenceStatus({id: userId1, active: active}, callback3);
        $timeout.flush();
        $httpBackend.flush();

        // then
        expect(socketService.subscribe).not.toHaveBeenCalled();
        expect(callback1).toHaveBeenCalledWith('status1');
        expect(callback2).toHaveBeenCalledWith('status2');
        expect(callback3).toHaveBeenCalledWith('status1');
      });

      it('should not get presence status for inactive user', function () {
        // given
        var userId = 123;
        var active = false;
        var callback = jasmine.createSpy();

        spyOn(socketService, 'receiveFrom');
        spyOn(socketService, 'subscribe');

        // when
        userService.getPresenceStatus({id: userId, active: active}, callback, null, true);
        $timeout.flush();

        // then
        expect(callback).not.toHaveBeenCalled();
        expect(socketService.subscribe).not.toHaveBeenCalled();
      });

      it('should get presence status and subscribe to future changes', function () {
        // given
        var userId = 123;
        var active = true;
        var callback = jasmine.createSpy();
        var updateCallback = angular.noop;
        var $scope = jasmine.createSpyObj('$scope', ['$apply', '$on']);
        var event = {content: {}};

        spyOn(socketService, 'receiveFrom').and.returnValue({then: function () {}});
        spyOn(socketService, 'subscribe');
        socketService.subscribe.and.callFake(function (destination, cb) {
          updateCallback = cb;
        });
        $scope.$apply.and.callFake(function (cb) {
          cb();
        });
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/users/presence-status?userIds=123').respond(200, {123: {label: 'status', subscriptionInfo: {token: 'token'}}});

        // when
        userService.getPresenceStatus({id: userId, active: active}, callback, $scope);
        $timeout.flush();
        $httpBackend.flush();
        updateCallback(event);

        // then
        expect(callback).toHaveBeenCalledWith({label: 'status', subscriptionInfo: {token: 'token'}});
        expect(callback).toHaveBeenCalledTimes(2);
        expect($scope.$apply).toHaveBeenCalled();
        expect(socketService.subscribe).toHaveBeenCalledWith('/topic/user.presenceStatusChanged', jasmine.any(Function), null, userId, 'token');
      });

      it('should not get presence status and subscribe to future changes', function () {
        // given
        var userId = 123;
        var active = false;

        spyOn(socketService, 'receiveFrom');
        spyOn(socketService, 'subscribe');

        // when
        userService.getPresenceStatus({id: userId, active: active}, function () {
        }, {$on: function () {}, $apply: function () {}});

        // then
        expect(socketService.receiveFrom).not.toHaveBeenCalledWith('/app/user.' + userId + '.presenceStatus');
        expect(socketService.subscribe).not.toHaveBeenCalledWith('/topic/user.presenceStatusChanged', jasmine.any(Function), null, userId);
      });
    });
  });
})();
