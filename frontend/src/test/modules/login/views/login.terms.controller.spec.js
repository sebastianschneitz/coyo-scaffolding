(function () {
  'use strict';

  var moduleName = 'coyo.login';
  var controllerName = 'LoginTermsController';

  describe('module: ' + moduleName, function () {

    var $controller, termsService, $state, TermsModel, authService, $translate;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_) {
      $controller = _$controller_;
      termsService = jasmine.createSpyObj('termsService', ['userNeedsToAcceptTerms', 'getTargetState']);
      termsService.getTargetState.and.returnValue({name: 'main', params: {}});
      $state = jasmine.createSpyObj('$state', ['go']);
      TermsModel = jasmine.createSpyObj('TermsModel', ['getByLanguage', 'getBestSuitable', 'accept']);
      authService = jasmine.createSpyObj('authService', ['getUser']);
      $translate = jasmine.createSpy('translate');
      $translate.and.returnValue({
        then: function (callback) {
          callback({
            'MODULE.LOGIN.TERMS.ACCEPT': 'test-accept',
            'MODULE.LOGIN.TERMS.DECLINE': 'test-decline'
          });
          return this;
        }
      });
    }));

    function mockValidation(response) {
      termsService.userNeedsToAcceptTerms.and.returnValue({
        then: function (callback) {
          callback(response);
        }
      });
    }

    function mockTermsResponse(response) {
      TermsModel.getBestSuitable.and.returnValue({
        then: function (callback) {
          callback(response);
          return this;
        }
      });
      TermsModel.getByLanguage.and.returnValue({
        then: function (callback) {
          callback(response);
          return this;
        }
      });
    }

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          termsService: termsService,
          $state: $state,
          TermsModel: TermsModel,
          authService: authService,
          $translate: $translate
        });
      }

      describe('init', function () {

        it('should redirect to main if already accepted', function () {
          // given
          mockValidation(false);
          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect($state.go).toHaveBeenCalledWith('main', {});
        });

        it('should hide fetch terms', function () {
          // given
          var terms = {language: 'en'};
          mockValidation(true);
          mockTermsResponse(terms);
          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(TermsModel.getBestSuitable).toHaveBeenCalled();
          expect(ctrl.terms).toBe(terms);
        });

        it('should set default button language', function () {
          // given
          var language = 'en';
          mockValidation(true);
          mockTermsResponse({language: 'en'});

          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect($translate).toHaveBeenCalledWith(['MODULE.LOGIN.TERMS.ACCEPT', 'MODULE.LOGIN.TERMS.DECLINE'], {}, null,
              null, language);
          expect(ctrl.acceptButtonTranslation).toBe('test-accept');
          expect(ctrl.declineButtonTranslation).toBe('test-decline');
        });
      });

      describe('active controller', function () {

        it('should save', function () {
          // given
          mockValidation(true);
          mockTermsResponse({url: null});
          authService.getUser.and.returnValue({
            then: function (callback) {
              callback({id: '1234'});
            }
          });

          TermsModel.accept.and.returnValue({
            then: function (callback) {
              callback();
            }
          });

          var ctrl = buildController();
          ctrl.terms = {language: 'DE'};

          // when
          ctrl.accept();

          // expect
          expect($state.go).toHaveBeenCalledWith('main', {});
          expect(TermsModel.accept).toHaveBeenCalledWith('1234', 'DE');
          expect(authService.getUser).toHaveBeenCalled();
        });

        it('should fetch certain language on language switch', function () {
          // given
          var terms = {text: 'terms-for-language', language: 'DE'};
          mockTermsResponse(terms);

          // when
          var ctrl = buildController();
          ctrl.fetchTermsByLanguage('DE');

          // then
          expect(TermsModel.getByLanguage).toHaveBeenCalledWith('DE');
          expect(ctrl.terms).toBe(terms);
          expect($translate).toHaveBeenCalledWith(['MODULE.LOGIN.TERMS.ACCEPT', 'MODULE.LOGIN.TERMS.DECLINE'], {}, null,
              null, 'de');

        });

      });
    });
  });
})();
