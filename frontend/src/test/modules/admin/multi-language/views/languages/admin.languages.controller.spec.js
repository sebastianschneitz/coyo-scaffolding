(function () {
  'use strict';

  var moduleName = 'coyo.admin.multiLanguage';
  var controllerName = 'AdminLanguagesController';

  describe('module: ' + moduleName, function () {

    var SettingsModel;

    beforeEach(function () {
      module('coyo.admin');

      SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieve']);

      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        $provide.value('SettingsModel', SettingsModel);
      });
    });

    describe('controller: ' + controllerName, function () {
      var $q, $controller, $rootScope, $scope, $translate;
      var ctrl, LanguagesModel, languageModels;

      beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _LanguagesModel_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $q = _$q_;
        LanguagesModel = _LanguagesModel_;

        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };

        spyOn(LanguagesModel.prototype, 'toggleActive').and.returnValue();

        languageModels = [
          new LanguagesModel({language: 'en', active: true}),
          new LanguagesModel({language: 'de', active: true}),
          new LanguagesModel({language: 'aa', active: false})
        ];

        $translate = jasmine.createSpyObj('$translate', ['instant']);
        $translate.instant.and.returnValue('language name');
      }));

      function buildController() {
        var controller = $controller(controllerName, {
          $q: $q,
          $scope: $scope,
          $translate: $translate,
          languageModels: languageModels,
          SettingsModel: SettingsModel
        });
        controller.$onInit();
        $rootScope.$apply();
        return controller;
      }

      describe('controller', function () {
        it('should perform initial load', function () {
          // given
          var translationPrefix = 'LANGUAGE.LANGUAGES.';
          $translate.instant.and.returnValues('English', 'German', 'Afar');
          $rootScope.screenSize.isXs = false;
          $rootScope.screenSize.isSm = false;

          // when
          ctrl = buildController();

          // then
          expect($translate.instant.calls.allArgs()).toEqual([[translationPrefix + 'en'], [translationPrefix + 'de'], [translationPrefix + 'aa']]);

          expect(ctrl.languages[0]).toEqual({model: languageModels[0], name: 'English'});
          expect(ctrl.languages[1]).toEqual({model: languageModels[1], name: 'German'});
          expect(ctrl.languages[2]).toEqual({model: languageModels[2], name: 'Afar'});

          expect(ctrl.mobile).toBe(false);
        });

        it('should init deviating page size on mobile', function () {
          // given
          $rootScope.screenSize.isXs = true;
          $rootScope.screenSize.isMd = false;

          // when
          ctrl = buildController();

          // then
          expect(ctrl.mobile).toBe(true);
        });

        it('should toggle active state', function () {
          // given
          ctrl = buildController();
          LanguagesModel.prototype.toggleActive.and.returnValues(
              $q.resolve(new LanguagesModel({active: false})),
              $q.resolve(new LanguagesModel({active: true}))
          );
          expect(ctrl.languages[0].model.active).toBe(true);
          expect(ctrl.languages[2].model.active).toBe(false);

          // when
          ctrl.toggleActive(ctrl.languages[0].model);
          ctrl.toggleActive(ctrl.languages[2].model);
          $rootScope.$digest();

          // then
          expect(ctrl.languages[0].model.active).toBe(false);
          expect(ctrl.languages[2].model.active).toBe(true);
          expect(LanguagesModel.prototype.toggleActive.calls.count()).toBe(2);
        });

        it('should reset cache after language active toggling', function () {
          // given
          var language = jasmine.createSpyObj('language', ['toggleActive']);
          language.toggleActive.and.returnValue($q.resolve());

          // when
          ctrl = buildController();
          ctrl.toggleActive(language);
          $scope.$apply();

          // then
          expect(SettingsModel.retrieve).toHaveBeenCalledWith(true);
        });
      });
    });
  });
})();
