(function () {
  'use strict';

  var moduleName = 'coyo.admin.launchpad';
  var controllerName = 'AdminLaunchpadListController';

  describe('module: ' + moduleName, function () {

    var $scope, $q, $controller, $injector, LaunchpadCategoryModel, modalService;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$rootScope_, _$q_, _$controller_) {
        $scope = _$rootScope_.$new();
        $q = _$q_;
        $controller = _$controller_;
      });
    });

    describe('controller: ' + controllerName, function () {
      var SettingsModel, settings, categories;

      beforeEach(function () {
        categories = [{id: 123}];
        SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieve']);
        SettingsModel.retrieve.and.returnValue($q.resolve(settings));

        settings = jasmine.createSpyObj('settings', ['update']);
        settings.update.and.returnValue($q.resolve(settings));
        settings.launchpadActive = 'true';

        LaunchpadCategoryModel = jasmine.createSpyObj('LaunchpadCategoryModel', ['query']);
        LaunchpadCategoryModel.query.and.returnValue($q.resolve(categories));

        modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});

        $injector = jasmine.createSpyObj('$injector', ['get']);
        $injector.get.and.returnValue(jasmine.createSpyObj('ngxNotificationService', ['success']));
      });

      function buildController() {
        return $controller(controllerName, {
          LaunchpadCategoryModel: LaunchpadCategoryModel,
          settings: settings,
          $scope: $scope,
          $injector: $injector,
          SettingsModel: SettingsModel,
          modalService: modalService
        });
      }

      describe('controller', function () {
        it('should init', function () {
          // given

          // when
          var ctrl = buildController();
          $scope.$apply();

          expect(LaunchpadCategoryModel.query).toHaveBeenCalledWith({admin: true});
          // then
          expect(ctrl.settings).toBe(settings);
          expect(ctrl.isActive).toBe(true);
          expect(ctrl.categories).toBe(categories);
        });

        it('should save', function () {
          var ctrl = buildController();

          settings.launchpadActive = 'false';
          // when
          ctrl.save();
          $scope.$apply();

          // then
          expect(settings.update).toHaveBeenCalled();
          expect(SettingsModel.retrieve).toHaveBeenCalledWith(true);
          expect(ctrl.isActive).toBe(false);
        });

        it('should delete', function () {

          var category = jasmine.createSpyObj('category', ['delete']);
          category.id = 123;

          category.delete.and.returnValue($q.resolve());

          var ctrl = buildController();

          // when
          ctrl.actions.deleteCategory(category);
          $scope.$apply();

          // then
          expect(category.delete).toHaveBeenCalled();
          expect(modalService.confirmDelete).toHaveBeenCalled();
          expect(ctrl.categories.length).toBe(0);
        });
      });
    });
  });
})();
