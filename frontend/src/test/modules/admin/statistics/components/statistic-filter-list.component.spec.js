(function () {
  'use strict';

  var moduleName = 'coyo.admin.statistics';

  describe('module: ' + moduleName, function () {

    var $controller, $scope;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_) {
      $controller = _$controller_;
      $scope = _$rootScope_.$new();
    }));

    var controllerName = 'StatisticFilterListController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        var ctrl = $controller(controllerName, {
          $scope: $scope,
          statisticsConfig: {
            'test-statistic': {
              dataSets: ['test-1', 'test-2', 'test-3'],
              showFilters: true,
              showCount: true
            }
          }
        });
        ctrl.statisticName = 'test-statistic';
        ctrl.labels = ['1', '2', '3'];
        ctrl.activeLabels = ['1', '2'];
        ctrl.data = [[1, 5], [2, 6], [3, 7]];

        ctrl.$onInit();

        return ctrl;
      }

      it('should calculate current data value of data set', function () {
        // given
        var ctrl = buildController();

        // when
        var result = ctrl.calculateCountForSeries('1');

        // then
        expect(result).toBe(6);
      });

      it('should sum up current data value of all active data sets', function () {
        // given
        var ctrl = buildController();

        // when
        var result = ctrl.getTotalCurrentData();

        // then
        expect(result).toBe(14);
      });

      it('should calculate disabled status', function () {
        // given
        var ctrl = buildController();

        // when
        var resultDisables = ctrl.isDisabled('3');
        var resultEnabled = ctrl.isDisabled('1');

        // then
        expect(resultDisables).toBeTruthy();
        expect(resultEnabled).toBeFalsy();
      });

    });
  });

})();
