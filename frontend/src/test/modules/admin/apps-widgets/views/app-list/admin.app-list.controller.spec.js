(function () {
  'use strict';

  var moduleName = 'coyo.admin.apps-widgets';

  describe('module: ' + moduleName, function () {

    var $controller, $rootScope, $scope, $q, $timeout;

    beforeEach(function () {
      module(moduleName);
      inject(function (_$controller_, _$timeout_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        $timeout = _$timeout_;

        $scope = $rootScope.$new();

        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };
      });
    });

    var controllerName = 'AdminAppListController';

    describe('controller: ' + controllerName, function () {
      var appRegistry, AppConfigurationModel, appConfigs, adminAppSettingsModal;
      var appModel;

      beforeEach(function () {
        appRegistry = jasmine.createSpyObj('appRegistry', ['getAll', 'getAppSenderTypes']);
        appRegistry.getAll.and.returnValue([
          {key: 'app-1', moderatorsOnly: false},
          {key: 'app-2', moderatorsOnly: false},
          {key: 'app-3', moderatorsOnly: true}
        ]);
        appRegistry.getAppSenderTypes.and.returnValue(['page', 'workspace']);

        appConfigs = [];

        AppConfigurationModel = function () {
          appModel = jasmine.createSpyObj('AppConfigurationModel', ['save']);

          var deferred = $q.defer();
          deferred.resolve({
            enabledSenderTypes: {pages: false, workspaces: false}
          });
          appModel.save.and.returnValue(deferred.promise);

          return appModel;
        };

        adminAppSettingsModal = jasmine.createSpyObj('adminAppSettingsModal', ['open']);
        adminAppSettingsModal.open.and.returnValue($q.resolve());
      });

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          $q: $q,
          $timeout: $timeout,
          appRegistry: appRegistry,
          AppConfigurationModel: AppConfigurationModel,
          appConfigs: appConfigs,
          adminAppSettingsModal: adminAppSettingsModal
        });
      }

      describe('controller init', function () {

        it('should prepare app list', function () {
          // given
          appConfigs.push({
            key: 'app-1',
            enabledSenderTypes: {
              pages: true,
              workspaces: true
            },
            hasEnabledSender: function () {
              return true;
            }
          });

          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.apps).toBeArrayOfSize(3);

          expect(ctrl.apps[0].key).toBe('app-1');
          expect(ctrl.apps[0].enabledSenderTypes).toHaveTrue('pages');
          expect(ctrl.apps[0].enabledSenderTypes).toHaveTrue('workspaces');

          expect(ctrl.apps[1].key).toBe('app-2');
          expect(ctrl.apps[1].enabledSenderTypes).not.toHaveTrue('pages');
          expect(ctrl.apps[1].enabledSenderTypes).not.toHaveTrue('workspaces');

          expect(ctrl.apps[2].key).toBe('app-3');
          expect(ctrl.apps[2].enabledSenderTypes).not.toHaveTrue('pages');
          expect(ctrl.apps[2].enabledSenderTypes).not.toHaveTrue('workspaces');
        });

      });

      describe('controller active', function () {

        var ctrl;

        beforeEach(function () {
          ctrl = buildController();
          ctrl.$onInit();
        });

        it('should toggle active state for pages', function () {
          // given
          var app = {
            key: 'app-1',
            enabledSenderTypes: {
              pages: true,
              workspaces: true
            }
          };

          // when
          ctrl.toggle('pages', app);
          $timeout.flush();

          // then
          // check model
          expect(appModel.save).toHaveBeenCalled();

          // check return values
          expect(app.enabledSenderTypes.pages).toBe(false);
          expect(app.enabledSenderTypes.workspaces).toBe(true);
        });

        it('should open the settings modal', function () {
          // given
          var app = {
            key: 'app-1'
          };
          var senderTypes = ['page', 'workspace'];
          ctrl.senderTypes = senderTypes;

          // when
          ctrl.openSettings(app);
          $scope.$apply();

          // then
          expect(adminAppSettingsModal.open).toHaveBeenCalledWith(senderTypes, app);
        });

      });

    });
  });

})();
