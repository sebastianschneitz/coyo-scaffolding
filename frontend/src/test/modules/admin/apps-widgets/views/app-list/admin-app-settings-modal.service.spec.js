(function () {
  'use strict';

  var moduleName = 'coyo.admin.apps-widgets';

  describe('module: ' + moduleName, function () {

    var adminAppSettingsModal, modalService;

    beforeEach(function () {

      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.returnValue({
        result: {
          then: angular.noop
        }
      });

      module(moduleName, function ($provide) {
        // provide dependencies for service
        $provide.value('modalService', modalService);
      });

      inject(function (_adminAppSettingsModal_) {
        // inject dependencies for test
        adminAppSettingsModal = _adminAppSettingsModal_;
      });

    });

    describe('service: adminAppSettingsModal', function () {

      it('should open modal', function () {
        // given
        var senderTypes = ['page', 'workspace'];
        var app = {id: 'appId'};

        // when
        adminAppSettingsModal.open(senderTypes, app);

        // then
        expect(modalService.open).toHaveBeenCalled();
        var args = modalService.open.calls.mostRecent().args;
        expect(args[0].resolve.senderTypes()).toEqual(senderTypes);
        expect(args[0].resolve.app()).toEqual(app);
      });

    });

    describe('controller: AdminAppSettingsModalController', function () {

      var $controller, $uibModalInstance, senderTypes, app;

      beforeEach(function () {

        senderTypes = ['page', 'workspace'];

        app = {
          id: 'appId'
        };

        inject(function (_$controller_) {
          $controller = _$controller_;
          $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close']);
        });
      });

      function buildController() {
        return $controller('AdminAppSettingsModalController', {
          $uibModalInstance: $uibModalInstance,
          senderTypes: senderTypes,
          app: app
        });
      }

      it('should init controller', function () {
        // when
        var ctrl = buildController();
        ctrl.$onInit();

        // then
        expect(ctrl.senderTypes).toBe(senderTypes);
        expect(ctrl.app.id).toBe(app.id);
      });

      it('should check whether the app has enabled senders, one sender enabled', function () {
        // given
        app.enabledSenderTypes = {page: true};

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var hasEnabledSender = ctrl.hasEnabledSender();

        // then
        expect(hasEnabledSender).toBe(true);
      });

      it('should check whether the app has enabled sender, no sender enabled', function () {
        // given
        app.enabledSenderTypes = {page: false};

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var hasEnabledSender = ctrl.hasEnabledSender();

        // then
        expect(hasEnabledSender).toBe(false);
      });

      it('should save', function () {
        // when
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.save();

        // then
        expect($uibModalInstance.close).toHaveBeenCalledWith(app);
      });

    });
  });

})();
