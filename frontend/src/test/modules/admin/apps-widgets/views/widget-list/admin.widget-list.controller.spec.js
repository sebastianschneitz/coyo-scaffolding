(function () {
  'use strict';

  var moduleName = 'coyo.admin.apps-widgets';

  describe('module: ' + moduleName, function () {

    var $controller, $rootScope, $scope, $q, $timeout, widgetRegistry;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        widgetRegistry = jasmine.createSpyObj('widgetRegistry', ['getAll']);
        widgetRegistry.getAll.and.returnValue([
          {key: 'widget-1', moderatorsOnly: false},
          {key: 'widget-2', moderatorsOnly: false},
          {key: 'widget-3', moderatorsOnly: true}
        ]);
        $provide.value('ngxWidgetRegistry', widgetRegistry);
      });
      inject(function (_$controller_, _$timeout_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $timeout = _$timeout_;
        $q = _$q_;

        $scope = $rootScope.$new();

        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };
      });
    });

    var controllerName = 'AdminWidgetListController';

    describe('controller: ' + controllerName, function () {

      var WidgetConfigurationModel, widgetConfigs, adminWidgetSettingsModal;

      beforeEach(function () {
        widgetConfigs = [{key: 'widget-1', enabled: true, whitelistExternal: true}];

        WidgetConfigurationModel = jasmine.createSpyObj('WidgetConfigurationModel', ['save']);
        var deferred = $q.defer();
        deferred.resolve(widgetConfigs[0]);
        WidgetConfigurationModel.save.and.returnValue(deferred.promise);

        adminWidgetSettingsModal = jasmine.createSpyObj('adminWidgetSettingsModal', ['open']);
        adminWidgetSettingsModal.open.and.returnValue($q.resolve());
      });

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          $q: $q,
          $timeout: $timeout,
          widgetRegistry: widgetRegistry,
          WidgetConfigurationModel: WidgetConfigurationModel,
          widgetConfigs: widgetConfigs,
          adminWidgetSettingsModal: adminWidgetSettingsModal
        });
      }

      describe('controller init', function () {

        it('should prepare widget list', function () {
          // given
          widgetConfigs.push({
            key: 'widget-1',
            enabled: true
          });

          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.widgets).toBeArrayOfSize(3);

          expect(ctrl.widgets[0].key).toBe('widget-1');
          expect(ctrl.widgets[0].enabled).toBe(true);

          expect(ctrl.widgets[1].key).toBe('widget-2');
          expect(ctrl.widgets[1].enabled).toBe(false);

          expect(ctrl.widgets[2].key).toBe('widget-3');
          expect(ctrl.widgets[2].enabled).toBe(false);
        });

      });

      describe('controller active', function () {

        var ctrl;

        beforeEach(function () {
          ctrl = buildController();
          ctrl.$onInit();
        });

        it('should save widget config', function () {
          // given
          var widget = {
            key: 'widget-1',
            enabled: true
          };

          // when
          ctrl.update(widget);
          $timeout.flush();

          // then
          expect(WidgetConfigurationModel.save).toHaveBeenCalledWith(widget);
          expect(widget.enabled).toBe(true);
        });

        it('should open the settings modal', function () {
          // given
          var widget = {
            key: 'widget-1'
          };

          // when
          ctrl.openSettings(widget);
          $scope.$apply();

          // then
          expect(adminWidgetSettingsModal.open).toHaveBeenCalledWith(widget);
        });

      });

    });
  });

})();
