(function () {
  'use strict';

  var moduleName = 'coyo.admin.authenticationProviders.oauth2';
  var controllerName = 'Oauth2SettingsController';

  describe('module: ' + moduleName, function () {

    describe('controller: ' + controllerName, function () {

      var $controller, translationRegistryProviderMock;

      var OAUTH2_PRESETS = {
        gSuite: {
          url: 'https://google.com'
        },
        gSuiteIntegration: {
          url: 'https://google.com'
        },
        office365: {
          url: 'https://microsoft.com'
        },
        office365Integration: {
          url: 'https://microsoft.com'
        },
        facebook: {
          url: 'https://facebook.com'
        }
      };

      beforeEach(function () {
        angular.module('translationRegistryProviderConfig', ['commons.i18n.custom'])
            .config(function (_translationRegistryProvider_) {
              translationRegistryProviderMock = _translationRegistryProvider_;
              spyOn(translationRegistryProviderMock, 'registerTranslations').and.callThrough();
            });
        module('translationRegistryProviderConfig');
        module(moduleName);
        inject(function (_$controller_) {
          $controller = _$controller_;
        });
      });

      function buildController() {
        var ctrlFn = $controller(controllerName, {
          OAUTH2_PRESETS: OAUTH2_PRESETS
        }, true);
        ctrlFn.instance.ngModel = {slug: 'anySlug'};
        var ctrl = ctrlFn();
        ctrl.formModel = {};
        return ctrl;
      }

      describe('controller init', function () {

        it('should init default oauth config presets', function () {
          // given
          var expectedFacebookConfig = {
            name: 'Facebook',
            properties: OAUTH2_PRESETS.facebook
          };
          var expectedGSuiteConfig = {
            name: 'Google IdP',
            properties: OAUTH2_PRESETS.gSuite
          };
          var expectedGSuiteIntegrationConfig = {
            name: 'Google IdP + Integration',
            properties: OAUTH2_PRESETS.gSuiteIntegration
          };
          var expectedOffice365Config = {
            name: 'Office 365',
            properties: OAUTH2_PRESETS.office365
          };
          var expectedOffice365IntegrationConfig = {
            name: 'Office 365 + Integration',
            properties: OAUTH2_PRESETS.office365Integration
          };

          // when
          var ctrl = buildController();

          // then
          expect(ctrl.presets.length).toBe(5);
          expect(ctrl.presets).toContain(expectedFacebookConfig);
          expect(ctrl.presets).toContain(expectedGSuiteConfig);
          expect(ctrl.presets).toContain(expectedGSuiteIntegrationConfig);
          expect(ctrl.presets).toContain(expectedOffice365Config);
          expect(ctrl.presets).toContain(expectedOffice365IntegrationConfig);
        });

      });
    });
  });
})();
