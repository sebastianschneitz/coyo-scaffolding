(function () {
  'use strict';

  var moduleName = 'coyo.admin.userManagement';
  var controllerName = 'AdminUserListController';

  describe('module: ' + moduleName, function () {

    var CapabilitiesModel;

    beforeEach(function () {
      CapabilitiesModel = jasmine.createSpyObj('CapabilitiesModel', ['isFeatureToggleEnabled']);

      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        $provide.value('CapabilitiesModel', CapabilitiesModel);
      });
    });

    describe('controller: ' + controllerName, function () {
      var $controller, $rootScope, $scope, $q, modalService, ctrl, user, UserModel, emptyResolve, $log, $sessionStorage,
          settings;

      beforeEach(inject(function (_$controller_, _$rootScope_, _$q_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $q = _$q_;

        emptyResolve = {
          then: function (thenCallback) {
            thenCallback();
            return {
              finally: function (finallyCallback) {
                finallyCallback();
              }
            };
          }
        };

        // mock screensize information
        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };

        UserModel = jasmine.createSpyObj('UserModel', ['searchWithAdminFields']);
        mockUserSearch({});

        $log = jasmine.createSpyObj('$log', ['debug']);
        modalService = jasmine.createSpyObj('modalService', ['confirm']);
        $sessionStorage = {};
        user = jasmine.createSpyObj('user', ['delete', 'recover', 'activate', 'deactivate']);
        settings = jasmine.createSpyObj('settings', ['deletedUserAnonymizationActive']);
        CapabilitiesModel.isFeatureToggleEnabled.and.returnValue($q.resolve(true));
      }));

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          UserModel: UserModel,
          modalService: modalService,
          $log: $log,
          $sessionStorage: $sessionStorage,
          settings: settings,
          CapabilitiesModel: CapabilitiesModel
        });
      }

      function mockUserSearch(result) {
        UserModel.searchWithAdminFields.and.returnValue({
          then: function (callback) {
            callback(result);
            return {
              finally: function (callback) {
                callback();
              }
            };
          }
        });
      }

      describe('controller init', function () {
        beforeEach(function () {
          mockUserSearch({content: 'testdata', _queryParams: {_page: 0}, number: 0});
        });

        it('should perform initial search', function () {
          // when
          ctrl = buildController();
          $rootScope.$apply();

          // then
          expect(ctrl.page.content).toBe('testdata');
          expect(ctrl.page._queryParams._page).toBe(0);
          expect(ctrl.page.loading).toBe(false);
          expect(ctrl.firstLoad).toBe(false);
        });

        it('should initialize default query parameters', function () {
          // when
          ctrl = buildController();

          // then
          expect(UserModel.searchWithAdminFields).toHaveBeenCalledWith({
            _page: 0,
            _pageSize: 10,
            _orderBy: 'lastname.sort,firstname.sort',
            displayName: '',
            status: 'ACTIVE',
            role: null
          });
        });

        it('should initialize deviating page size on mobile', function () {
          // given
          $rootScope.screenSize.isXs = true;
          $rootScope.screenSize.isMd = false;

          // when
          ctrl = buildController();

          // then
          expect(UserModel.searchWithAdminFields).toHaveBeenCalled();
          expect(UserModel.searchWithAdminFields.calls.mostRecent().args[0]._pageSize).toBe(30);
        });

        it('should use query parameter from session storage', function () {
          // given
          $sessionStorage.userList = {orderBy: 'xyz'};

          // when
          ctrl = buildController();

          // then
          expect(UserModel.searchWithAdminFields).toHaveBeenCalledWith({orderBy: 'xyz', _pageSize: 10});
        });
      });

      describe('controller reinit', function () {
        it('should sync query params with current page number', function () {
          // given
          mockUserSearch({content: 'testdata', _queryParams: {_page: 0}, number: 1});

          // when
          ctrl = buildController();

          // then
          expect(ctrl.page._queryParams._page).toBe(1);

        });
      });

      describe('controller active', function () {
        beforeEach(function () {
          mockUserSearch({content: 'testdata', _queryParams: {_page: 0}, number: 0});
          ctrl = buildController();
        });

        describe('next page', function () {
          beforeEach(function () {
            ctrl.page = jasmine.createSpyObj('Page', ['nextAppended']);
          });

          it('should not call page when already at last page', function () {
            // given
            ctrl.page.content = ['test'];
            ctrl.page.last = true;

            // when
            ctrl.nextPage();

            // then
            expect(ctrl.page.nextAppended).not.toHaveBeenCalled();
          });

          it('should not call page when list is empty', function () {
            // given
            ctrl.page.content = [];

            // when
            ctrl.nextPage();

            // then
            expect(ctrl.page.nextAppended).not.toHaveBeenCalled();
          });

          it('should call page append', function () {
            // given
            ctrl.page.content = ['test'];

            // when
            ctrl.nextPage();

            // then
            expect(ctrl.page.nextAppended).toHaveBeenCalled();
          });
        });

        describe('actions', function () {
          it('should delete user', function () {
            //given
            user.delete.and.returnValue(emptyResolve);
            modalService.confirm.and.returnValue({result: emptyResolve});
            settings.deletedUserAnonymizationActive = false;

            // when
            // ctrl = buildController();
            ctrl.actions.deleteUser(user, true);

            // then
            expect(user.delete).toHaveBeenCalled();
            expect(modalService.confirm).toHaveBeenCalled();
            var args = modalService.confirm.calls.mostRecent().args;
            expect(args[0].title).toBe('ADMIN.USER_MGMT.USERS.OPTIONS.DELETE.MODAL.TITLE');
            expect(args[0].text).toBe('ADMIN.USER_MGMT.USERS.OPTIONS.DELETE.MODAL.TEXT');
            expect(UserModel.searchWithAdminFields).toHaveBeenCalled();
          });

          it('should how deletion warning for immediate user anonymization', function () {
            //given
            user.delete.and.returnValue(emptyResolve);
            modalService.confirm.and.returnValue({result: emptyResolve});
            settings.deletedUserAnonymizationActive = 'true';
            settings.deletedUserAnonymizationDelay = 'PT0S';

            // when
            ctrl.actions.deleteUser(user, true);

            // then
            var args = modalService.confirm.calls.mostRecent().args;
            expect(args[0].text).toBe('ADMIN.USER_MGMT.USERS.OPTIONS.DELETE.MODAL.ANONYMIZATION_HINT');
          });

          it('should how deletion warning for delayed user anonymization', function () {
            //given
            user.delete.and.returnValue(emptyResolve);
            modalService.confirm.and.returnValue({result: emptyResolve});
            settings.deletedUserAnonymizationActive = 'true';

            // when
            ctrl.actions.deleteUser(user, true);

            // then
            var args = modalService.confirm.calls.mostRecent().args;
            expect(args[0].text).toBe('ADMIN.USER_MGMT.USERS.OPTIONS.DELETE.MODAL.DELAYED_ANONYMIZATION_HINT');
          });

          it('should recover user', function () {
            //given
            user.recover.and.returnValue(emptyResolve);
            modalService.confirm.and.returnValue({result: emptyResolve});

            // when
            // ctrl = buildController();
            ctrl.actions.recoverUser(user, true);

            // then
            expect(user.recover).toHaveBeenCalled();
            expect(modalService.confirm).toHaveBeenCalled();
            var args = modalService.confirm.calls.mostRecent().args;
            expect(args[0].title).toBe('ADMIN.USER_MGMT.USERS.OPTIONS.RECOVER.MODAL.TITLE');
            expect(args[0].text).toBe('ADMIN.USER_MGMT.USERS.OPTIONS.RECOVER.MODAL.TEXT');
            expect(UserModel.searchWithAdminFields).toHaveBeenCalled();
          });
        });

        it('should activate user', function () {
          // given
          user.activate.and.returnValue(emptyResolve);

          // when
          ctrl.actions.activateUser(user, false);

          // then
          expect(user.activate).toHaveBeenCalled();
          expect(modalService.confirm).not.toHaveBeenCalled();
        });

        it('should deactivate user', function () {
          // given
          user.deactivate.and.returnValue(emptyResolve);

          // when
          ctrl.actions.deactivateUser(user, false);

          // then
          expect(user.deactivate).toHaveBeenCalled();
          expect(modalService.confirm).not.toHaveBeenCalled();
        });
      });
    });
  });
})();
