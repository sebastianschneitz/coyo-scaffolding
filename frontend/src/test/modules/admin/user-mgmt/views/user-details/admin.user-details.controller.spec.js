(function () {
  'use strict';

  var moduleName = 'coyo.admin.userManagement';
  var controllerName = 'AdminUserDetailsController';

  describe('module: ' + moduleName, function () {

    beforeEach(module(moduleName, function ($provide) {
      $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
    }));

    describe('controller: ' + controllerName, function () {
      var $controller, user, $q, $state, $scope, ctrl, $translate, authService, RoleModel;

      beforeEach(inject(function (_$controller_, _$q_, $rootScope) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = $rootScope.$new();

        authService = jasmine.createSpyObj('authService', ['getUser']);
        authService.getUser.and.returnValue($q.resolve({id: 'user-id', superadmin: true}));

        $state = jasmine.createSpyObj('$state', ['go', 'href']);
        user = jasmine.createSpyObj('UserModel', ['create', 'update', 'isNew']);
        RoleModel = jasmine.createSpyObj('RoleModel', ['getRolesForGroups']);
        $translate = jasmine.createSpy();
        $translate.and.returnValue({
          then: function (callback) {
            callback('placeholder');
          }
        });
      }));

      function buildController() {
        return $controller(controllerName, {
          authService: authService,
          user: user,
          RoleModel: RoleModel,
          $state: $state,
          $translate: $translate,
          $scope: $scope
        });
      }

      describe('controller init', function () {
        it('should set password placeholder for existing user', function () {
          // given
          user.isNew.and.returnValue(false);

          // when
          ctrl = buildController();

          // then
          expect(ctrl.passwordPlaceholder).toBe('placeholder');
        });

        it('should not set password placeholder for new user', function () {
          // given
          user.isNew.and.returnValue(true);

          // when
          ctrl = buildController();

          // then
          expect(ctrl.passwordPlaceholder).toBeUndefined();
        });

        it('should init roles in controller', function () {
          // given
          var roles = [{id: 'role-id1'}, {id: 'role-id2'}];
          user.roles = roles;

          // when
          ctrl = buildController();

          // then
          expect(ctrl.roles).toEqual(roles);
        });
      });

      describe('controller active', function () {
        beforeEach(function () {
          ctrl = buildController();
        });

        it('should save new user and go to parent state', function () {
          // given
          user.isNew.and.returnValue(true);
          user.create.and.returnValue({
            then: function (callback) {
              callback();
            }
          });
          ctrl.roles = [{id: 'role-id1'}, {id: 'role-id2'}];

          // when
          ctrl.save();

          // then
          expect(user.create).toHaveBeenCalled();
          expect($state.go).toHaveBeenCalledWith('^');
          expect(user.roleIds).toEqual(['role-id1', 'role-id2']);
        });

        it('should update existing user and go to parent state', function () {
          // given
          user.isNew.and.returnValue(false);
          user.update.and.returnValue({
            then: function (callback) {
              callback();
            }
          });

          // when
          ctrl.save();

          // then
          expect(user.update).toHaveBeenCalled();
          expect($state.go).toHaveBeenCalledWith('^');
        });

        it('should remove empty alternate login name', function () {
          // given
          user.loginNameAlt = '';
          user.isNew.and.returnValue(false);
          user.update.and.returnValue({
            then: function (callback) {
              callback();
            }
          });

          // when
          ctrl.save();

          // then
          expect(user.update).toHaveBeenCalled();
          expect(user.loginNameAlt).toBeUndefined();
        });

        describe('hasRemoteUserIdWarning', function () {
          it('should be true if remote user id is changed with remote repository configured', function () {
            // given
            ctrl.user = {remoteUserId: 'new-remote-user-id'};
            ctrl.oldRemoteUserId = 'old-remote-user-id';
            ctrl.remoteUserDirectory = {};

            // when
            var result = ctrl.hasRemoteUserIdWarning();

            // then
            expect(result).toBe(true);
          });

          it('should be false if remote user id is unchanged with remote repository configured', function () {
            // given
            ctrl.user = {remoteUserId: 'old-remote-user-id'};
            ctrl.oldRemoteUserId = 'old-remote-user-id';
            ctrl.remoteUserDirectory = {};

            // when
            var result = ctrl.hasRemoteUserIdWarning();

            // then
            expect(result).toBe(false);
          });

          it('should be false if remote user id is removed with remote repository configured', function () {
            // given
            ctrl.user = {remoteUserId: null};
            ctrl.oldRemoteUserId = 'old-remote-user-id';
            ctrl.remoteUserDirectory = {};

            // when
            var result = ctrl.hasRemoteUserIdWarning();

            // then
            expect(result).toBe(false);
          });

          it('should be false if changed and no remote repository configured', function () {
            // given
            ctrl.user = {remoteUserId: 'new-remote-user-id'};
            ctrl.oldRemoteUserId = 'old-remote-user-id';
            ctrl.remoteUserDirectory = null;

            // when
            var result = ctrl.hasRemoteUserIdWarning();

            // then
            expect(result).toBe(false);
          });
        });

        describe('hasRemoteDirectoryChangedWarning', function () {
          it('should be true if directory is changed for existing user', function () {
            // given
            ctrl.user = user;
            user.isNew.and.returnValue(false);
            ctrl.remoteUserDirectory = {id: 'new-id'};
            ctrl.oldRemoteUserDirectoryId = 'old-id';

            // when
            var result = ctrl.hasRemoteDirectoryChangedWarning();

            // then
            expect(result).toBe(true);
          });

          it('should be false if directory is unchanged for existing user', function () {
            // given
            ctrl.user = user;
            user.isNew.and.returnValue(false);
            ctrl.remoteUserDirectory = {id: 'old-id'};
            ctrl.oldRemoteUserDirectoryId = 'old-id';

            // when
            var result = ctrl.hasRemoteDirectoryChangedWarning();

            // then
            expect(result).toBe(false);
          });

          it('should be false if directory is changed for new user', function () {
            // given
            ctrl.user = user;
            user.isNew.and.returnValue(true);
            ctrl.remoteUserDirectory = {id: 'new-id'};
            ctrl.oldRemoteUserDirectoryId = 'old-id';

            // when
            var result = ctrl.hasRemoteDirectoryChangedWarning();

            // then
            expect(result).toBe(false);
          });

          it('should be true if directory is removed for existing user', function () {
            // given
            ctrl.user = user;
            user.isNew.and.returnValue(false);
            ctrl.remoteUserDirectory = null;
            ctrl.oldRemoteUserDirectoryId = 'old-id';

            // when
            var result = ctrl.hasRemoteDirectoryChangedWarning();

            // then
            expect(result).toBe(false);
          });
        });

        describe('hasRemoteDirectoryRemovedWarning', function () {
          it('should be true if remote directory is removed', function () {
            // given
            ctrl.remoteUserDirectory = null;
            ctrl.oldRemoteUserDirectoryId = 'old-id';

            // when
            var result = ctrl.hasRemoteDirectoryRemovedWarning();

            // then
            expect(result).toBe(true);
          });

          it('should be false if remote directory is changed', function () {
            // given
            ctrl.remoteUserDirectory = {id: 'new-id'};
            ctrl.oldRemoteUserDirectoryId = 'old-id';

            // when
            var result = ctrl.hasRemoteDirectoryRemovedWarning();

            // then
            expect(result).toBe(false);
          });
        });

        describe('determine combined roles of roles and groups', function () {
          it('should watch changing roles and determine accumulated groups from group and role', function () {
            // given
            ctrl.roles = [{id: 'role-0'}];
            ctrl.groups = [{id: 'group1'}];
            RoleModel.getRolesForGroups.and.returnValue($q.resolve([{id: 'role-2'}]));

            // when
            RoleModel.getRolesForGroups.calls.reset();
            ctrl.roles = [{id: 'role-1'}];
            $scope.$digest();
            $scope.$apply();

            // then
            expect(RoleModel.getRolesForGroups).toHaveBeenCalledWith(['group1']);
            expect(ctrl.accumulatedRoles.length).toBe(2);
            expect(ctrl.accumulatedRoles).toContain({id: 'role-1'});
            expect(ctrl.accumulatedRoles).toContain({id: 'role-2'});
          });

          it('should watch changing groups and determine accumulated groups from group and role', function () {
            // given
            ctrl.roles = [{id: 'role-1'}];
            ctrl.groups = [{id: 'group0'}, {id: 'group2'}];
            RoleModel.getRolesForGroups.and.returnValue($q.resolve([{id: 'role-1'}, {id: 'role-2'}, {id: 'role-3'}]));

            // when
            RoleModel.getRolesForGroups.calls.reset();
            ctrl.groups = [{id: 'group1'}, {id: 'group2'}];
            $scope.$digest();
            $scope.$apply();

            // then
            expect(RoleModel.getRolesForGroups).toHaveBeenCalledWith(['group1', 'group2']);
            expect(ctrl.accumulatedRoles.length).toBe(3);
            expect(ctrl.accumulatedRoles).toContain({id: 'role-1'});
            expect(ctrl.accumulatedRoles).toContain({id: 'role-2'});
            expect(ctrl.accumulatedRoles).toContain({id: 'role-3'});
          });
        });
      });
    });
  });
})();
