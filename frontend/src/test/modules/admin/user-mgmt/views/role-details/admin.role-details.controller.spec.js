(function () {
  'use strict';

  var moduleName = 'coyo.admin.userManagement';
  var controllerName = 'AdminRoleDetailsController';

  describe('module: ' + moduleName, function () {

    beforeEach(
        module(moduleName, function ($provide) {
          $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        })
    );

    describe('controller: ' + controllerName, function () {
      var $controller, $scope, role, $state, permissionConfiguration, $log, ctrl;

      beforeEach(inject(function (_$controller_, $rootScope) {
        $scope = $rootScope.$new();
        $controller = _$controller_;

        $state = jasmine.createSpyObj('$state', ['go', 'is']);
        $log = jasmine.createSpyObj('$log', ['debug']);
        role = jasmine.createSpyObj('RoleModel', ['save', 'isNew']);
        role.permissions = ['P1'];

        permissionConfiguration = [
          {
            permissions: [
              {key: 'P1'},
              {key: 'P2'}
            ]
          }
        ];
      }));

      function buildController(isNewRole, isViewState) {
        role.isNew.and.returnValue(isNewRole);
        $state.is.and.returnValue(isViewState);
        return $controller(controllerName, {
          $scope: $scope,
          $log: $log,
          role: role,
          $state: $state,
          permissionConfiguration: permissionConfiguration
        });
      }

      describe('controller modes', function () {

        it('should be in view mode', function () {
          ctrl = buildController(false, true);
          expect(ctrl.mode).toBe('view');
        });
        it('should be in edit mode', function () {
          ctrl = buildController(false, false);
          expect(ctrl.mode).toBe('edit');
        });
        it('should be in create mode', function () {
          ctrl = buildController(true, false);
          expect(ctrl.mode).toBe('create');
        });
      });

      describe('controller active', function () {
        beforeEach(function () {
          ctrl = buildController(false, false);
        });

        describe('permissions', function () {
          it('should permission from role be set in controller', function () {
            expect(ctrl.permissions.P1).toBe(true);
            expect(ctrl.permissions.P2).toBe(false);
          });

          it('should setting property unset property to true on controller change role permission', function () {
            expect(role.permissions.indexOf('P2')).toBe(-1);
            ctrl.permissions.P2 = true;
            expect(role.permissions.indexOf('P2')).toBeGreaterThan(-1);
          });

          it('should setting set property to false on controller change role permission', function () {
            expect(role.permissions.indexOf('P1')).toBeGreaterThan(-1);
            ctrl.permissions.P1 = false;
            expect(role.permissions.indexOf('P1')).toBe(-1);
          });
        });

        it('should save role and go to parent state', function () {
          // given
          role.save.and.returnValue({
            then: function (callback) {
              callback();
            }
          });

          // when
          ctrl.save();

          // then
          expect(role.save).toHaveBeenCalled();
          expect($state.go).toHaveBeenCalledWith('^');
        });
      });
    });
  });
})();
