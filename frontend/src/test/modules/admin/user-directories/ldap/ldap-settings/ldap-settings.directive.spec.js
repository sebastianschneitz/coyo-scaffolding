(function () {
  'use strict';

  var moduleName = 'coyo.admin.userDirectories.ldap';
  var controllerName = 'LdapSettingsController';

  describe('module: ' + moduleName, function () {
    var $controller, $stateParams, $state;
    var $rootScope, $scope, $q, modalService, profileFieldsService, UserDirectoryModel;
    var ngModel, group1, group2, group3;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$q_, _$controller_, _modalService_, _$state_, _$stateParams_) {
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $q = _$q_;
      $controller = _$controller_;
      $state = _$state_;
      $stateParams = _$stateParams_;
      modalService = _modalService_;

      $state = jasmine.createSpyObj('$state', ['go']);
      profileFieldsService = jasmine.createSpyObj('profileFieldsService', ['getGroups']);
      UserDirectoryModel = jasmine.createSpyObj('UserDirectoryModel', ['getOrphanPolicies']);
      UserDirectoryModel.getOrphanPolicies.and.returnValue($q.resolve(['POLICY1', 'POLICY2']));

      ngModel = {
        settings: {
          profileFields: {}
        }
      };
      group1 = {
        fields: [{id: 'field1', name: 'field1'}, {id: 'field2', name: 'field2'}]
      };
      group2 = {
        fields: [{id: 'field3', name: 'field3'}]
      };
      group3 = {
        fields: [{id: 'field4', name: 'field4'}]
      };
    }));

    describe('controller: ' + controllerName, function () {

      function buildController(_ngModel) {
        return $controller(controllerName, {
          modalService: modalService,
          $state: $state,
          $stateParams: $stateParams,
          profileFieldsService: profileFieldsService,
          UserDirectoryModel: UserDirectoryModel
        }, {
          ngModel: _ngModel
        });
      }

      it('should init available profile fields', function () {
        // given
        profileFieldsService.getGroups.and.returnValue($q.resolve([group1, group2, group3]));

        // when
        var ctrl = buildController(ngModel);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(_.some(ctrl.availableProfileFields, {id: 'field1'})).toBeTrue();
        expect(_.some(ctrl.availableProfileFields, {id: 'field2'})).toBeTrue();
        expect(_.some(ctrl.availableProfileFields, {id: 'field3'})).toBeTrue();
        expect(_.some(ctrl.availableProfileFields, {id: 'field4'})).toBeTrue();
        expect(ctrl.availableOrphanedUsersPolicies).toEqual(['POLICY1', 'POLICY2']);
      });

      it('should select profile field', function () {
        // given
        profileFieldsService.getGroups.and.returnValue($q.resolve([group1, group2, group3]));
        var field = {name: 'field'};
        var fieldOld = {name: 'field'};

        // when
        var ctrl = buildController(ngModel);
        ctrl.profileFieldsOrdered = [fieldOld];
        ctrl.onProfileFieldSelect(field.name, field);
        $scope.$apply();

        // then
        expect(ctrl.ngModel.settings.profileFields[field.name]).toBe('');
      });

      it('should remove profile field configuration', function () {
        // given
        profileFieldsService.getGroups.and.returnValue($q.resolve([group1, group2, group3]));
        ngModel.settings.profileFields = {
          'field1': 'mapping1',
          'field2': 'mapping2'
        };

        // when
        var ctrl = buildController(ngModel);
        ctrl.removeProfileField('field1');
        $scope.$apply();

        // then
        expect(_.has(ctrl.ngModel.settings.profileFields, 'field1')).toBeFalse();
        expect(_.has(ctrl.ngModel.settings.profileFields, 'field2')).toBeTrue();
      });

      it('should add profile field configuration', function () {
        // given
        profileFieldsService.getGroups.and.returnValue($q.resolve([group1, group2, group3]));

        // when
        var ctrl = buildController(ngModel);
        var field1 = {name: 'field1'};
        var field2 = {name: 'field2'};
        ctrl.availableProfileFields = [field1, field2];
        ctrl.profileFieldsOrdered = [];
        ctrl.addProfileFieldConfiguration({preventDefault: function () {}});
        $scope.$apply();

        // then
        expect(_.has(ctrl.ngModel.settings.profileFields, field1.name)).toBeTrue();
      });

      it('should remove deleted profile fields', function () {
        // given
        profileFieldsService.getGroups.and.returnValue($q.resolve([group1]));
        var model = {
          settings: {
            profileFields: {
            }
          }
        };
        model.settings.profileFields[group1.fields[0].name] = 'TEXT';
        model.settings.profileFields[group2.fields[0].name] = 'TEXT';
        model.settings.profileFields[group3.fields[0].name] = 'TEXT';

        // when
        var ctrl = buildController(model);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.ngModel.settings.profileFields[group1.fields[0].name]).toBe('TEXT');
        expect(ctrl.ngModel.settings.profileFields[group2.fields[0].name]).toBeUndefined();
        expect(ctrl.ngModel.settings.profileFields[group3.fields[0].name]).toBeUndefined();
      });
    });

  });

})();
