(function () {
  'use strict';

  var moduleName = 'coyo.admin.apiClients';
  var controllerName = 'AdminApiClientsDetailsController';

  describe('module: ' + moduleName, function () {

    beforeEach(
        module(moduleName, function ($provide) {
          $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        })
    );

    describe('controller: ' + controllerName, function () {
      var $httpBackend, $controller, $rootScope, $scope, ctrl, $state, client, ApiClientModel, backendUrlService;

      beforeEach(inject(function (_$httpBackend_, _$controller_, _$rootScope_, _ApiClientModel_, _backendUrlService_) {
        $httpBackend = _$httpBackend_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;
        ApiClientModel = _ApiClientModel_;
        backendUrlService = _backendUrlService_;

        $state = jasmine.createSpyObj('$state', ['go']);

        $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
      }));

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $state: $state,
          client: client
        });
      }

      describe('controller init', function () {
        it('should create new API client on save', function () {
          // given
          var clientId = 'CLIENT-ID';
          client = new ApiClientModel({clientId: clientId});
          $httpBackend
              .expectPOST(backendUrlService.getUrl() + '/web/api-clients')
              .respond(201, {});

          // when
          ctrl = buildController();
          ctrl.save();
          $httpBackend.flush();

          // then
          expect($state.go).toHaveBeenCalledWith('^.list');
        });

      });
    });
  });
})();
