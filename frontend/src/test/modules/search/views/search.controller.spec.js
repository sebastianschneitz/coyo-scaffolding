(function () {
  'use strict';

  var moduleName = 'coyo.search';
  var targetName = 'SearchController';

  describe('module: ' + moduleName, function () {
    var $rootScope, $scope, $controller, $httpBackend, $httpParamSerializer, $state, $stateParams, $q, $timeout;
    var backendUrlService, coyoConfig, targetServiceMock, url, ngxGoogleApiService, ngxO365ApiService;
    var currentUser = {};
    var searchTerm = 'search-term';
    var externalOfficeSearchActive = false;
    var externalGoogleSearchActive = true;
    var officeSearch;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });

      inject(function (_$rootScope_, _$controller_, _$httpBackend_, _$httpParamSerializer_, _$q_, _$timeout_,
                       _backendUrlService_, _coyoConfig_) {
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();
        $controller = _$controller_;
        $httpBackend = _$httpBackend_;
        $httpParamSerializer = _$httpParamSerializer_;
        $q = _$q_;
        $timeout = _$timeout_;

        $state = jasmine.createSpyObj('$state', ['transitionTo']);
        $state.transitionTo.and.returnValue($q.resolve());
        $stateParams = {
          term: searchTerm,
          'filter[]': ['search-filter']
        };

        $rootScope.search = {new: true};

        backendUrlService = _backendUrlService_;

        coyoConfig = _coyoConfig_;

        ngxO365ApiService = jasmine.createSpyObj('ngxO365ApiService',
            ['isApiActive', 'searchForDriveItemsWithCountLimitResults']);
        ngxO365ApiService.isApiActive.and.callFake(function () {
          return {
            subscribe: function (callBack) {
              return callBack(externalOfficeSearchActive);
            }
          };
        });

        officeSearch = $q.defer();
        ngxO365ApiService.searchForDriveItemsWithCountLimitResults.and.callFake(function () {
          return {
            toPromise: function () {
              return officeSearch.promise;
            }
          };
        });

        ngxGoogleApiService = jasmine.createSpyObj('ngxGoogleApiService', ['isGoogleApiActive']);
        ngxGoogleApiService.isGoogleApiActive.and.callFake(function () {
          return {
            subscribe: function (callBack) {
              return callBack(externalGoogleSearchActive);
            }
          };
        });

        targetServiceMock = jasmine.createSpyObj('targetService', ['go', 'onCanLinkTo', 'getLink']);
        targetServiceMock.onCanLinkTo.and.returnValue($q.resolve(true));
        targetServiceMock.getLink.and.returnValue($q.resolve('/some-link'));
        url = backendUrlService.getUrl()
            + '/web/search?_page=0&_pageSize=20&aggregations=author%3D100%26modified%3D%26sender%3D100%26type%3D'
            + _.keys(coyoConfig.entityTypes).length + '&filters=filter%3Dsearch-filter&term=search-term&newSearch=true';
      });
    });

    describe('controller: ' + targetName, function () {

      function buildController() {
        return $controller(targetName, {
          $rootScope: $rootScope,
          $scope: $scope,
          $httpParamSerializer: $httpParamSerializer,
          $state: $state,
          $stateParams: $stateParams,
          targetService: targetServiceMock,
          currentUser: currentUser,
          coyoConfig: coyoConfig,
          ngxGoogleApiService: ngxGoogleApiService,
          ngxO365ApiService: ngxO365ApiService
        });
      }

      it('should init search', function () {
        var response = {content: [1, 2, 3], aggregations: {}};
        $httpBackend.whenGET(url).respond(response);

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $httpBackend.flush();

        // then
        expect($rootScope.search.term).toEqual($stateParams.term);
        expect($rootScope.search.filter.filter).toEqual($stateParams['filter[]']);
        expect($rootScope.search.new).toBeFalsy();
        expect(ctrl.currentPage.content).toEqual(response.content);
      });

      it('should get type', function () {
        // when
        var ctrl = buildController();
        var type = ctrl.getType('workspace');

        // then
        expect(type).toEqual(coyoConfig.entityTypes.workspace);
      });

      it('should open message', function () {
        // when
        var ctrl = buildController();
        ctrl.clickHandler({canLinkToResult: true, target: 'target', typeName: 'message'});

        // then
        expect(targetServiceMock.go).toHaveBeenCalledWith('target');
      });

      it('should open file', function () {
        // when
        var ctrl = buildController();
        ctrl.clickHandler({canLinkToResult: true, target: 'target', typeName: 'file'});

        // then
        expect(targetServiceMock.go).toHaveBeenCalledWith('target');
      });

      it('should not open message because of wrong typeName', function () {
        // when
        var ctrl = buildController();
        ctrl.clickHandler({canLinkToResult: true, target: 'target', typeName: 'user'});

        // then
        expect(targetServiceMock.go).not.toHaveBeenCalled();
      });

      it('should not open message because of canLinkToResult', function () {
        // when
        var ctrl = buildController();
        ctrl.clickHandler({canLinkToResult: false, target: 'target', typeName: 'message'});

        // then
        expect(targetServiceMock.go).not.toHaveBeenCalled();
      });

      it('should toggle filter (single-select)', function () {
        // given
        $httpBackend.whenGET(url).respond({content: [1, 2, 3], aggregations: {}});

        var ctrl = buildController();
        ctrl.$onInit();
        $httpBackend.flush();

        // when
        ctrl.selectFilter('filter', ['new-search-filter']);

        // then
        expect($rootScope.search.term).toEqual($stateParams.term);
        expect($rootScope.search.filter.filter).toEqual(['new-search-filter']);
        expect($state.transitionTo).toHaveBeenCalledWith('main.search', {
          term: $stateParams.term,
          'filter[]': ['new-search-filter']
        }, {notify: false});
      });

      it('should toggle filter (multi-select)', function () {
        // given
        $httpBackend.whenGET(url).respond({content: [1, 2, 3], aggregations: {}});

        var ctrl = buildController();
        ctrl.$onInit();
        $httpBackend.flush();

        // when
        ctrl.selectFilter('filter', ['new-search-filter-1', 'new-search-filter-2']);

        // then
        expect($rootScope.search.term).toEqual($stateParams.term);
        expect($rootScope.search.filter.filter).toEqual(['new-search-filter-1', 'new-search-filter-2']);
        expect($state.transitionTo).toHaveBeenCalledWith('main.search', {
          term: $stateParams.term,
          'filter[]': ['new-search-filter-1', 'new-search-filter-2']
        }, {notify: false});
      });

      it('should toggle filter twice without disabling all other filter options (disabled because ctrl.loading is forever true)',
          function () {
            // given
            $httpBackend.whenGET(url).respond({content: [1, 2, 3], aggregations: {}});

            var ctrl = buildController();
            ctrl.$onInit();
            $httpBackend.flush();

            // when
            ctrl.selectFilter('type', []);
            ctrl.selectFilter('type', []);
            $scope.$apply();

            // then
            expect(ctrl.loading).toEqual(false);
          });

      it('should clear all filter', function () {
        // given
        $httpBackend.whenGET(url).respond({content: [1, 2, 3], aggregations: {}});

        var ctrl = buildController();
        ctrl.$onInit();
        $httpBackend.flush();

        // when
        ctrl.clearAllFilters();

        // then
        expect($rootScope.search.term).toEqual($stateParams.term);
        expect($rootScope.search.filter).toEqual({});
        expect($state.transitionTo).toHaveBeenCalledWith('main.search', {
          term: $stateParams.term
        }, {notify: false});
      });

      it('should load more', function () {
        // given
        $httpBackend.whenGET(url).respond({content: [1, 2, 3], aggregations: {}});
        var url2 = backendUrlService.getUrl()
            + '/web/search?_page=1&_pageSize=20&aggregations=author%3D100%26modified%3D%26sender%3D100%26type%3D'
            + _.keys(coyoConfig.entityTypes).length
            + '&filters=filter%3Dsearch-filter&term=search-term&newSearch=false';
        var response = {content: [4, 5, 6], number: 1, last: true, aggregations: {type: [{count: 2}, {count: 1}]}};
        $httpBackend.whenGET(url2).respond(response);

        var ctrl = buildController();
        ctrl.$onInit();
        $httpBackend.flush();

        // when
        ctrl.search(1, false);

        // then
        expect(ctrl.loading).toBeTrue();
        $httpBackend.flush();
        expect(ctrl.currentPage.content).toEqual(response.content);
        expect(ctrl.typeFilter.allItem.count).toBe(3);
        expect(ctrl.loading).toBeFalse();
      });

      it('should load result links', function () {
        // given
        var resource1 = {
          id: '1',
          typeName: 'workspace',
          target: 'target',
          sender: {id: 'sender1', target: 'senderTarget'}
        };
        var resource2 = {id: '2', typeName: 'page', target: 'target', sender: {id: 'sender2', target: 'senderTarget'}};
        $httpBackend.whenGET(url).respond({content: [resource1, resource2], aggregations: {}});
        targetServiceMock.onCanLinkTo.and.callFake(function () {
          arguments[1](true);
        });

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $httpBackend.flush();

        // then
        expect(_.size(ctrl.links.results)).toEqual(2);
        expect(_.size(ctrl.links.senders)).toEqual(2);
      });

      it('should not load result links for messages', function () {
        // given
        var resource1 = {
          id: '1',
          typeName: 'workspace',
          target: 'target',
          sender: {id: 'sender1', target: 'senderTarget'}
        };
        var resource2 = {id: '2', typeName: 'page', target: 'target', sender: {id: 'sender2', target: 'senderTarget'}};
        var resource3 = {
          id: '3',
          typeName: 'message',
          target: 'target',
          sender: {id: 'sender3', target: 'senderTarget'}
        };
        $httpBackend.whenGET(url).respond({content: [resource1, resource2, resource3], aggregations: {}});
        targetServiceMock.onCanLinkTo.and.callFake(function () {
          arguments[1](true);
        });

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $httpBackend.flush();

        // then
        expect(_.size(ctrl.links.results)).toEqual(2);
        expect(_.size(ctrl.links.senders)).toEqual(3);
      });

      it('should have empty links', function () {
        // given
        var resource1 = {
          id: '1',
          typeName: 'workspace',
          target: 'target',
          sender: {id: 'sender1', target: 'senderTarget'}
        };
        $httpBackend.whenGET(url).respond({content: [resource1], aggregations: {}});
        targetServiceMock.onCanLinkTo.and.callFake(function () {
          arguments[1](false);
        });

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $httpBackend.flush();

        // then
        expect(ctrl.links.results[resource1.id].length).toEqual(0);
        expect(ctrl.links.senders[resource1.sender.id].length).toEqual(0);
      });

      it('should show search term panel when coyo search results exist', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();

        // when
        _.set(ctrl, 'currentPage.totalElements', 3);
        _.set(ctrl, 'externalSearchResults', 0);
        $timeout.flush();

        // then
        expect(ctrl.hasCoyoSearchResults).toBe(true);
        expect(ctrl.hasExternalSearchResults).toBe(false);
        expect(ctrl.hasAnySearchResult).toBe(true);
        expect(ctrl.showSearchTermPanel).toBe(true);
      });
    });
  });
})();
