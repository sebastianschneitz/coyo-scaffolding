(function () {
  'use strict';

  var moduleName = 'coyo.landing-pages';
  var controllerName = 'SubNavigationController';

  describe('module: ' + moduleName, function () {
    var $controller, $q;
    var $rootScope, $scope, $state, $transitions, $injector, SettingsModel, urlService;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$rootScope_, _$q_, _$transitions_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();
        $q = _$q_;
        $transitions = _$transitions_;
      });

      SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieveByKey']);
      SettingsModel.retrieveByKey.and.returnValue($q.resolve('false'));

      urlService = jasmine.createSpyObj('urlService', ['isRelativePath']);

      $state = jasmine.createSpyObj('$state', ['includes']);
      $state.includes.and.returnValue(false);

      $injector = jasmine.createSpyObj('$injector', ['get']);
      $injector.get.and.returnValue(urlService);

      $transitions = jasmine.createSpyObj('$transitions', ['onSuccess', 'onError']);
      $transitions.onSuccess.and.callFake(function (criteria, callback) {
        var transitionMock = {
          injector: function () {
            return {
              get: function () {
                return $state;
              }
            };
          }
        };
        $rootScope.$on('transition-success-mock', function () { callback(transitionMock); });
        return function () {};
      });
    });

    function buildController() {
      return $controller(controllerName, {
        $scope: $scope,
        $state: $state,
        $transitions: $transitions,
        $injector: $injector,
        SettingsModel: SettingsModel
      });
    }

    describe('controller: ' + controllerName, function () {

      it('should always display landing pages navigation if configured', function () {
        // given
        SettingsModel.retrieveByKey.and.returnValue($q.resolve('true'));

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.subNavigationActive).toBe(true);
      });

      it('should display landing pages navigation if not configured but on landing page', function () {
        // given
        spyOn($scope, '$on');
        SettingsModel.retrieveByKey.and.returnValue($q.resolve('false'));
        $state.includes.and.returnValue(true);

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.subNavigationActive).toBe(true);
        expect($transitions.onSuccess).toHaveBeenCalledWith({}, jasmine.any(Function));
        expect($scope.$on).toHaveBeenCalledWith('$destroy', jasmine.any(Function));
      });

      it('should not display landing pages navigation if not configured and not on landing page', function () {
        // given
        spyOn($scope, '$on');
        SettingsModel.retrieveByKey.and.returnValue($q.resolve('false'));
        $state.includes.and.returnValue(false);

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.subNavigationActive).toBe(false);
        expect($transitions.onSuccess).toHaveBeenCalledWith({}, jasmine.any(Function));
        expect($scope.$on).toHaveBeenCalledWith('$destroy', jasmine.any(Function));
      });

      it('should change display when state changes', function () {
        // given
        SettingsModel.retrieveByKey.and.returnValue($q.resolve('false'));
        $state.includes.and.returnValue(false);

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();
        expect(ctrl.subNavigationActive).toBe(false);

        // state change
        $state.includes.and.returnValue(true);
        $rootScope.$emit('transition-success-mock');
        $scope.$apply();

        // then
        expect(ctrl.subNavigationActive).toBe(true);
      });

      it('should add relative flag if a relative url is configured in landing page', function () {
        // given
        var ctrl = buildController();
        var relativeLandingPage = {configuredUrl: '/relative'};
        var noPathLandingPage = {};
        ctrl.landingPages = [relativeLandingPage, noPathLandingPage];
        urlService.isRelativePath.and.returnValue(true);

        // when
        ctrl.$onInit();

        // then
        expect(relativeLandingPage.relativeUrl).toBeTruthy();
        expect(noPathLandingPage.relativeUrl).toBeFalsy();
      });

      it('should get _blank target for new tab link', function () {
        // given
        var ctrl = buildController();
        var landingPage = {configuredUrl: 'http://google.de', newTab: true};

        // when
        var result = ctrl.getTarget(landingPage);

        // then
        expect(result).toBe('_blank');
      });

      it('should get _self target for absolute same tab link', function () {
        // given
        var ctrl = buildController();
        var landingPage = {configuredUrl: 'http://google.de', newTab: false};

        // when
        var result = ctrl.getTarget(landingPage);

        // then
        expect(result).toBe('_self');
      });

      it('should get undefined target for relative same tab link', function () {
        // given
        var ctrl = buildController();
        var landingPage = {configuredUrl: '/relative', relativeUrl: true, newTab: false};

        // when
        var result = ctrl.getTarget(landingPage);

        // then
        expect(result).toBeUndefined();
      });
    });
  });

})();
