(function () {
  'use strict';

  var moduleName = 'coyo.landing-pages';

  describe('module: ' + moduleName, function () {

    var $controller;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_) {
      $controller = _$controller_;
    }));

    var controllerName = 'LandingPagesController';

    describe('controller: ' + controllerName, function () {

      var landingPages;

      beforeEach(function () {
        landingPages = [{id: 'PAGE1'}, {id: 'PAGE2'}];
      });

      function buildController() {
        return $controller(controllerName, {
          landingPages: landingPages
        });
      }

      it('should init landing pages', function () {
        // when
        var ctrl = buildController();

        // then
        expect(ctrl.landingPages = landingPages);
      });

    });
  });

})();
