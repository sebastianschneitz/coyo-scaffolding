(function () {
  'use strict';

  var moduleName = 'commons.tour';

  describe('module: ' + moduleName, function () {

    var $rootScope, $q, $timeout, tourService, authService, uiTour, uiTourService;

    beforeEach(function () {
      authService = jasmine.createSpyObj('authService', ['getUser']);
      uiTour = jasmine.createSpyObj('uiTour', ['on', 'off', 'start', 'getStatus', '_getSteps']);
      uiTourService = jasmine.createSpyObj('uiTourService', ['createDetachedTour', 'getTourByName']);
      uiTourService.getTourByName.and.returnValue(uiTour);

      module(moduleName, function ($provide) {
        $provide.value('authService', authService);
        $provide.value('uiTourService', uiTourService);
      });

      inject(function (_tourService_, _$q_, _$rootScope_, _$timeout_) {
        tourService = _tourService_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $timeout = _$timeout_;
      });

    });

    describe('Service: tourService', function () {

      it('should init mobile tour when loading module', function () {
        $rootScope.screenSize = {
          isXs: true
        };

        // when
        $timeout.flush();

        // then
        expect(uiTourService.getTourByName).toHaveBeenCalledWith('mobileTour');
      });

      it('should init desktop tour', function () {
        $rootScope.screenSize = {
          isLg: true
        };

        // when
        $timeout.flush();

        // then
        expect(uiTourService.getTourByName).toHaveBeenCalledWith('desktopTour');
      });

      it('should start tour if step was added', function () {
        // given
        var step = {
          stepId: 'testStep',
          stepTitle: 'My awesome tour step!'
        };
        $rootScope.screenSize = {
          isLg: true
        };
        uiTour.on.and.callFake(function (eventName, callback) {
          if (eventName === 'stepAdded') {
            callback(step);
          }
        });
        uiTour.getStatus.and.returnValue(0);
        uiTour._getSteps.and.returnValue([step]);

        // when
        $timeout.flush();

        // then
        expect(uiTour.start).toHaveBeenCalledTimes(1);
      });

      it('should not start tour if one is running', function () {
        // given
        var step = {
          stepId: 'testStep',
          stepTitle: 'My awesome tour step!'
        };
        $rootScope.screenSize = {
          isLg: true
        };
        uiTour.on.and.callFake(function (eventName, callback) {
          if (eventName === 'stepAdded') {
            callback(step);
          }
        });
        uiTour.getStatus.and.returnValue(1);

        // when
        $timeout.flush();

        // then
        expect(uiTour.start).not.toHaveBeenCalled();
      });

      it('should mark loaded steps as visited', function () {
        // given
        var user = {
          tourData: {
            visited: []
          },
          setTourData: angular.noop
        };

        spyOn(user, 'setTourData');
        user.setTourData.and.returnValue($q.resolve());
        authService.getUser.and.returnValue($q.resolve(user));
        uiTour.getStatus.and.returnValue(0);

        var stepOne = {
          stepId: 'testStepOne',
          stepTitle: 'My first awesome tour step!'
        };

        var stepTwo = {
          stepId: 'testStepTwo',
          stepTitle: 'Another awesome tour step!'
        };

        $rootScope.screenSize = {
          isLg: true
        };

        uiTour.on.and.callFake(function (eventName, callback) {
          if (eventName === 'stepAdded') {
            callback(stepOne);
            callback(stepTwo);
          }
          if (eventName === 'ended') {
            callback();
          }
        });
        uiTour._getSteps.and.returnValue([stepOne, stepTwo]);

        // when
        $timeout.flush();
        $rootScope.$apply();

        // then
        expect(user.tourData.visited).toBeArrayOfSize(2);
        expect(user.tourData.visited).toContain(stepOne.stepId);
        expect(user.tourData.visited).toContain(stepTwo.stepId);
        expect(user.setTourData).toHaveBeenCalledWith({visited: [stepOne.stepId, stepTwo.stepId]});
      });
    });

    it('should set already seen steps disabled', function () {
      // given
      var keys = ['stepOne', 'stepTwo'];

      $rootScope.screenSize = {
        isXs: true
      };

      var user = {
        tourData: {
          visited: []
        },
        setTourData: angular.noop
      };

      spyOn(user, 'setTourData');
      user.setTourData.and.returnValue($q.resolve());
      authService.getUser.and.returnValue($q.resolve(user));
      uiTour.getStatus.and.returnValue(0);

      // when
      $timeout.flush();
      tourService.markVisited(keys);
      $rootScope.$apply();

      // then
      var enabledOne = undefined;
      var enabledTwo = undefined;
      var enabledThree = undefined;

      tourService.isEnabled(keys[0]).then(function (enabled) {
        enabledOne = enabled;
      });
      tourService.isEnabled(keys[1]).then(function (enabled) {
        enabledTwo = enabled;
      });
      tourService.isEnabled('someRandomKey').then(function (enabled) {
        enabledThree = enabled;
      });

      $rootScope.$apply();

      expect(enabledOne).toBe(false);
      expect(enabledTwo).toBe(false);
      expect(enabledThree).toBe(true);
    });

    it('should register the steps by id', function () {
      // given
      var step = {id: 'stepId', topic: 'testTopic'};
      spyOn($rootScope, '$emit').and.callThrough();

      // when
      var checkBefore = tourService.isStepRegistered(step.id);
      tourService.registerStep(step);
      var checkAfter = tourService.isStepRegistered(step.id);

      // then
      expect(checkBefore).toBe(false);
      expect(checkAfter).toBe(true);
      expect($rootScope.$emit).toHaveBeenCalledWith('tourService:stepsChanged');
    });

    it('should restart the tour', function () {
      // given
      var key = 'testTopic';
      $rootScope.screenSize = {
        isXs: true
      };
      spyOn($rootScope, '$emit');

      // when
      $timeout.flush();
      tourService.restart(key);
      $timeout.flush();

      // then
      expect($rootScope.$emit).toHaveBeenCalledWith('tour.restart', key);
      expect(uiTour.start).toHaveBeenCalledTimes(1);
    });

    it('should register and unregister steps', function () {
      // given
      spyOn($rootScope, '$emit').and.callThrough();
      $rootScope.screenSize = {
        isXs: true
      };
      var stepOne = {id: 'testStepOne', topic: 'topicOne'};
      var stepTwo = {id: 'testStepTwo', topic: 'topicOne'};
      var stepThree = {id: 'testStepThree', topic: 'topicThree'};
      var stepFour = {id: 'testStepThree', topic: 'topicThree'};
      var stepFive = {id: 'testStepFive', topic: 'topicFive'};

      // when
      $timeout.flush();
      tourService.registerStep(stepOne);
      tourService.registerStep(stepTwo);
      tourService.registerStep(stepThree);
      tourService.registerStep(stepFour);
      tourService.registerStep(stepFive);
      var topicsBefore = tourService.getTopics();

      tourService.unregisterStep(stepThree.id);
      tourService.unregisterStep(stepTwo.id);
      var topicsAfter = tourService.getTopics();

      // then
      expect(topicsBefore).toBeArrayOfStrings();
      expect(topicsBefore).toEqual(['topicOne', 'topicThree', 'topicFive']);

      expect(topicsAfter).toBeArrayOfStrings();
      expect(topicsAfter).toEqual(['topicOne', 'topicFive']);

      expect($rootScope.$emit).toHaveBeenCalledWith('tourService:stepsChanged');
      expect($rootScope.$emit.calls.all().length).toBe(6);
    });

  });
})();
