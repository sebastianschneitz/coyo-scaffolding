(function () {
  'use strict';

  var moduleName = 'coyo.account';
  var controllerName = 'PushDevicesModalController';

  describe('module: ' + moduleName, function () {

    describe('controller: ' + controllerName, function () {

      var $controller, pushDevicesManagementService;

      beforeEach(function () {
        module(moduleName);

        inject(function (_$controller_) {
          $controller = _$controller_;
        });

        pushDevicesManagementService = jasmine.createSpyObj('pushDevicesManagementService',
            ['togglePushAppInstallationStatus', 'deletePushAppInstallation', 'preparePushDevices']);
      });

      function buildController() {
        return $controller(controllerName, {
          pushDevicesManagementService: pushDevicesManagementService
        });
      }

      it('should init controller', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(pushDevicesManagementService.preparePushDevices).toHaveBeenCalledWith(ctrl);
      });

      it('should toggle app installation', function () {
        // given
        var appInstallation = {anyProperty: 'anyValue'};
        var ctrl = buildController();

        // when
        ctrl.togglePushAppInstallationStatus(appInstallation);

        // then
        expect(pushDevicesManagementService.togglePushAppInstallationStatus)
            .toHaveBeenCalledWith(ctrl, appInstallation);
      });

      it('should remove app installation', function () {
        // given
        var appInstallation = {anyProperty: 'anyValue'};
        var ctrl = buildController();

        // when
        ctrl.removePushAppInstallationDevice(appInstallation);

        // then
        expect(pushDevicesManagementService.deletePushAppInstallation).toHaveBeenCalledWith(ctrl, appInstallation);
      });

    });
  });
})();
