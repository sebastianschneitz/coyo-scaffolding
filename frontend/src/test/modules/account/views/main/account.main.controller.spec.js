(function () {
  'use strict';

  var moduleName = 'coyo.account';
  var controllerName = 'AccountMainController';

  describe('module: ' + moduleName, function () {

    describe('controller: ' + controllerName, function () {

      var $controller, $rootScope, $scope, $q, $injector;
      var modalService, ngxNotificationService, currentUser, pushDevicesManagementService, ngxIntegrationApiService,
          SettingsModel;

      beforeEach(function () {
        module(moduleName);

        inject(function (_$controller_, _$rootScope_, _$q_) {
          $controller = _$controller_;
          $rootScope = _$rootScope_;
          $scope = $rootScope.$new();
          $q = _$q_;
        });

        ngxNotificationService = jasmine.createSpyObj('ngxNotificationService', ['success']);
        $injector = jasmine.createSpyObj('$injector', ['get']);
        $injector.get.and.returnValue(ngxNotificationService);

        SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieveByKey']);
        SettingsModel.retrieveByKey.and.returnValue($q.resolve('NONE'));

        modalService = jasmine.createSpyObj('modalService', ['confirm', 'open']);

        pushDevicesManagementService = jasmine.createSpyObj('pushDevicesManagementService',
            ['togglePushAppInstallationStatus', 'deletePushAppInstallation', 'preparePushDevices']);

        ngxIntegrationApiService = jasmine.createSpyObj('ngxIntegrationApiService', ['isIntegrationApiActiveFor']);
        ngxIntegrationApiService.isIntegrationApiActiveFor.and.callFake(function () {
          return {
            subscribe: function (callBack) {
              return callBack(true);
            }
          };
        });

        currentUser = jasmine.createSpyObj('user', ['hasGlobalPermissions']);
      });

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          $injector: $injector,
          modalService: modalService,
          currentUser: currentUser,
          passwordPattern: '',
          pushDevicesManagementService: pushDevicesManagementService,
          ngxIntegrationApiService: ngxIntegrationApiService,
          SettingsModel: SettingsModel
        });
      }

      it('should init controller', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(pushDevicesManagementService.preparePushDevices).toHaveBeenCalledWith(ctrl);
        expect(ctrl.data.account).toEqual(currentUser);
      });

      it('should toggle app installation', function () {
        // given
        var appInstallation = {anyProperty: 'anyValue'};
        var ctrl = buildController();

        // when
        ctrl.togglePushAppInstallationStatus(appInstallation);

        // then
        expect(pushDevicesManagementService.togglePushAppInstallationStatus)
            .toHaveBeenCalledWith(ctrl, appInstallation);
      });

      it('should remove app installation', function () {
        // given
        var appInstallation = {anyProperty: 'anyValue'};
        currentUser.hasGlobalPermissions.and.returnValue(true);
        modalService.confirm.and.returnValue({result: $q.resolve()});
        var ctrl = buildController();

        // when
        ctrl.removePushAppInstallation(appInstallation, 'XS');
        $scope.$apply();

        // then
        expect(currentUser.hasGlobalPermissions).toHaveBeenCalledWith('MANAGE_USER');
        expect(modalService.confirm).toHaveBeenCalledWith({
          size: 'XS',
          title: 'MODULE.ACCOUNT.MODALS.PUSH_DEVICES.REMOVE.TITLE',
          text: 'MODULE.ACCOUNT.MODALS.PUSH_DEVICES.REMOVE.TEXT',
          close: {icon: 'delete', title: 'MODULE.ACCOUNT.MODALS.PUSH_DEVICES.REMOVE.YES', style: 'btn-danger'},
          dismiss: {title: 'MODULE.ACCOUNT.MODALS.PUSH_DEVICES.REMOVE.NO'}
        });

        expect(pushDevicesManagementService.deletePushAppInstallation).toHaveBeenCalledWith(ctrl, appInstallation);
      });

      it('should not show integration panel', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.integrationSettingsPanelVisible).toBeFalsy();
      });

      it('should show integration panel for GSUITE', function () {
        // given
        SettingsModel.retrieveByKey.and.returnValue($q.resolve('G_SUITE'));
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.integrationSettingsPanelVisible).toBeTruthy();
      });

      it('should open user integration settings modal', function () {
        // given
        modalService.open.and.returnValue({result: $q.resolve()});
        var ctrl = buildController();

        // when
        ctrl.openUserIntegrationSettingsModal();
        $scope.$apply();

        // then
        expect(modalService.open).toHaveBeenCalledWith({
          templateUrl: 'app/modules/account/components/modals/changeUserIntegrationSettings.modal.html',
          controller: 'UserIntegrationSettingsModalController'
        });
        expect(ngxNotificationService.success)
            .toHaveBeenCalledWith('MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.UPDATE.SUCCESS');
      });
    });
  });
})();
