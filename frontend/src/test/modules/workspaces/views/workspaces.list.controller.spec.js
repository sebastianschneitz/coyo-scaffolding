(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';
  var targetName = 'WorkspacesListController';

  describe('module: ' + moduleName, function () {
    var $controller, $rootScope, $scope, $q, $sessionStorage, $state, $timeout;
    var WorkspaceModelMock, Pageable, modalService, user, categories;

    beforeEach(function () {
      module('commons.ui');

      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });

      inject(function (_$rootScope_, _$controller_, _$q_, _Pageable_, _$timeout_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();
        $q = _$q_;
        $sessionStorage = {workspaceQuery: {}};
        Pageable = _Pageable_;
        $timeout = _$timeout_;
        user = {id: 1, name: 'user'};
        categories = [{id: 1, name: 'cat1'}, {id: 2, name: 'cat2'}, {id: 3, name: 'cat3'}];

        $state = jasmine.createSpyObj('$state', ['transitionTo']);

        WorkspaceModelMock = jasmine.createSpyObj('WorkspaceModel', ['searchWithFilter']);

        modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
      });
    });

    describe('controller: ' + targetName, function () {

      function buildController($stateParams) {
        return $controller(targetName, {
          $rootScope: $rootScope,
          $scope: $scope,
          $q: $q,
          $sessionStorage: $sessionStorage,
          $state: $state,
          $stateParams: $stateParams || {},
          WorkspaceModel: WorkspaceModelMock,
          Pageable: Pageable,
          currentUser: user,
          categories: categories,
          modalService: modalService
        });
      }

      it('should show workspace actions when visibility is public and membershipStatus is none', function () {
        // given
        var ctrl = buildController();
        var workspace = {visibility: 'PUBLIC', membershipStatus: 'NONE'};

        // when
        var show = ctrl.showWorkspaceActions(workspace);

        // then
        expect(show).toBe(true);
      });

      it('should show workspace actions when visibility is protected and membershipStatus is none', function () {
        // given
        var ctrl = buildController();
        var workspace = {visibility: 'PROTECTED', membershipStatus: 'NONE'};

        // when
        var show = ctrl.showWorkspaceActions(workspace);

        // then
        expect(show).toBe(true);
      });

      it('should not show workspace actions when visibility is public and membershipStatus is none', function () {
        // given
        var ctrl = buildController();
        var workspace = {visibility: 'PRIVATE', membershipStatus: 'NONE'};

        // when
        var show = ctrl.showWorkspaceActions(workspace);

        // then
        expect(show).toBe(false);
      });

      function itShouldInitWithStatusFilterFromSessionStorage(filter) {
        it('should init with filter "' + filter + '" from session storage', function () {
          // given
          $sessionStorage = {workspaceStatusFilter: filter};
          spyOn($rootScope, '$on');
          $rootScope.$on.and.returnValue(function () {});

          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.query.filters.status).toBe(filter);
          expect(ctrl.statusFilterModel.getItem(filter).isSelected()).toBe(true);
        });
      }
      itShouldInitWithStatusFilterFromSessionStorage('ALL');
      itShouldInitWithStatusFilterFromSessionStorage('ACTIVE');
      itShouldInitWithStatusFilterFromSessionStorage('ARCHIVED');

      function itShouldInitWithStatusFilterFromStateParams(filter) {
        it('should init with filter "' + filter + '" from state parameters', function () {
          // given
          $sessionStorage = {workspaceStatusFilter: 'OTHER'};
          var $stateParams = {status: filter};
          spyOn($rootScope, '$on');
          $rootScope.$on.and.returnValue(function () {});

          // when
          var ctrl = buildController($stateParams);
          ctrl.$onInit();

          // then
          expect(ctrl.query.filters.status).toBe(filter);
          expect(ctrl.statusFilterModel.getItem(filter).isSelected()).toBe(true);
        });
      }
      itShouldInitWithStatusFilterFromStateParams('ALL');
      itShouldInitWithStatusFilterFromStateParams('ACTIVE');
      itShouldInitWithStatusFilterFromStateParams('ARCHIVED');

      function itShouldInitWithMembershipFilterFromSessionStorage(filter) {
        it('should init with filter "' + filter + '" from session storage', function () {
          // given
          $sessionStorage = {workspaceMembershipFilter: filter};
          spyOn($rootScope, '$on');
          $rootScope.$on.and.returnValue(function () {});

          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.query.filters.membership).toBe(filter);
          expect(ctrl.membershipFilterModel.getItem(filter).isSelected()).toBe(true);
        });
      }
      itShouldInitWithMembershipFilterFromSessionStorage('ALL');
      itShouldInitWithMembershipFilterFromSessionStorage('APPROVED');
      itShouldInitWithMembershipFilterFromSessionStorage('INVITED');
      itShouldInitWithMembershipFilterFromSessionStorage('REQUESTED');

      function itShouldInitWithMembershipFilterFromStateParams(filter) {
        it('should init with filter "' + filter + '" from state parameters', function () {
          // given
          $sessionStorage = {workspaceMembershipFilter: 'OTHER'};
          var $stateParams = {membership: filter};
          spyOn($rootScope, '$on');
          $rootScope.$on.and.returnValue(function () {});

          // when
          var ctrl = buildController($stateParams);
          ctrl.$onInit();

          // then
          expect(ctrl.query.filters.membership).toBe(filter);
          expect(ctrl.membershipFilterModel.getItem(filter).isSelected()).toBe(true);
        });
      }
      itShouldInitWithMembershipFilterFromStateParams('ALL');
      itShouldInitWithMembershipFilterFromStateParams('APPROVED');
      itShouldInitWithMembershipFilterFromStateParams('INVITED');
      itShouldInitWithMembershipFilterFromStateParams('REQUESTED');

      it('should init and read from URL', function () {
        // given
        $sessionStorage = {workspaceQuery: {term: 'term'}};
        var $stateParams = {term: 'term', 'categories[]': 'cat1'};

        // when
        var ctrl = buildController($stateParams);
        ctrl.$onInit();

        // then
        expect(ctrl.query.term).toEqual($stateParams.term);
        expect(ctrl.query.filters.categories).toEqual($stateParams['categories[]']);
      });

      it('should init and read from $sessionStorage', function () {
        // given
        $sessionStorage = {workspaceQuery: {term: 'term'}};

        // when
        var ctrl = buildController();
        ctrl.$onInit();

        // then
        expect(ctrl.query.term).toEqual($sessionStorage.workspaceQuery.term);
      });

      it('should initially load workspaces', function () {
        // given
        var page = {
          content: [{id: 1, name: 'workspace1'}, {id: 2, name: 'workspace2'}, {id: 3, name: 'workspace3'}],
          aggregations: {categories: [{key: 1, count: 2}], allCategories: [{key: 'allCategories', count: 3}]}
        };
        WorkspaceModelMock.searchWithFilter.and.returnValue($q.resolve(page));

        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.query = {term: 'term', filters: {categories: [1]}};

        // when
        $timeout.flush();

        // then
        expect($sessionStorage.workspaceQuery).toEqual(ctrl.query);
        expect(WorkspaceModelMock.searchWithFilter).toHaveBeenCalledWith(
            ctrl.query.term,
            new Pageable(0, 20, ['_score,DESC', 'displayName.sort']),
            ctrl.query.filters,
            ['displayName', 'description'],
            {categories: 0, allCategories: 0, archived: 0},
            true);

        expect(ctrl.loading).toBeFalse();
        expect(ctrl.currentPage).toEqual(page);
        expect(ctrl.categories[0].count).toEqual(2);
        expect(ctrl.categories[1].count).toEqual(0);
        expect(ctrl.categories[2].count).toEqual(0);
        expect(ctrl.totalCount).toEqual(3);
      });

      it('should not load more while loading', function () {
        // given
        buildController();

        // when
        $scope.$apply();

        // then
        expect(WorkspaceModelMock.searchWithFilter).not.toHaveBeenCalled();
      });

      it('should not load more on last page', function () {
        // given
        var ctrl = buildController();
        ctrl.currentPage = {last: true};

        // when
        $scope.$apply();

        // then
        expect(WorkspaceModelMock.searchWithFilter).not.toHaveBeenCalled();
      });

      it('should search', function () {
        // given
        var term = 'new term';
        var ctrl = buildController();
        ctrl.$onInit();
        WorkspaceModelMock.searchWithFilter.and.returnValue($q.reject());

        // when
        ctrl.search(term);

        // then
        expect(ctrl.query.term).toEqual(term);
        expect(ctrl.query.filters.categories).toEqual([]);
        expect(WorkspaceModelMock.searchWithFilter).toHaveBeenCalled();
      });

      it('should not search when editing categories', function () {
        // given
        var term = 'new term';
        var ctrl = buildController();
        ctrl.editingCategory = true;
        ctrl.query = {term: 'term', filters: {categories: [1]}};
        WorkspaceModelMock.searchWithFilter.and.returnValue($q.reject());

        // when
        ctrl.search(term);

        // then
        expect(ctrl.query.term).toEqual('term');
        expect(ctrl.query.filters.categories).toEqual([1]);
        expect(WorkspaceModelMock.searchWithFilter).not.toHaveBeenCalled();
      });

      function itShouldSetStatusFilter(filter) {
        it('should set status filter "' + filter + '"', function () {
          // given
          WorkspaceModelMock.searchWithFilter.and.returnValue($q.reject());
          var ctrl = buildController();
          ctrl.$onInit();

          // when
          ctrl.setFilterStatus(filter);

          // then
          expect(ctrl.query.filters.status).toBe(filter);
        });
      }
      itShouldSetStatusFilter('ALL');
      itShouldSetStatusFilter('ACTIVE');
      itShouldSetStatusFilter('ARCHIVED');

      it('should set status filter "ACTIVE" by default', function () {
        // given
        WorkspaceModelMock.searchWithFilter.and.returnValue($q.reject());
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.statusFilterModel.getItem('ACTIVE').isSelected()).toBe(true);
        expect(ctrl.query.filters.status).toBe('ACTIVE');
      });

      function itShouldSetMembershipFilter(filter) {
        it('should set membership filter "' + filter + '"', function () {
          // given
          WorkspaceModelMock.searchWithFilter.and.returnValue($q.reject());
          var ctrl = buildController();
          ctrl.$onInit();

          // when
          ctrl.setFilterMembership(filter);

          // then
          expect(ctrl.query.filters.membership).toBe(filter);
        });
      }
      itShouldSetMembershipFilter('ALL');
      itShouldSetMembershipFilter('APPROVED');
      itShouldSetMembershipFilter('INVITED');
      itShouldSetMembershipFilter('REQUESTED');

      it('should set membership filter "ALL" by default', function () {
        // given
        WorkspaceModelMock.searchWithFilter.and.returnValue($q.reject());
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.membershipFilterModel.getItem('ALL').isSelected()).toBe(true);
        expect(ctrl.query.filters.membership).toBe('ALL');
      });
    });
  });

})();
