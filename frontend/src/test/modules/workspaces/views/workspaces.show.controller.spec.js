(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $rootScope, $q, $state, appService, workspace, user, senderNavigationUpdateService,
        $transitions, $location, $urlRouter;

    beforeEach(module(moduleName));

    beforeEach(function () {
      user = {id: 'USER-ID-ONE'};
      appService = jasmine.createSpyObj('appService', ['addApp', 'onAppChanged', 'updateApp', 'deleteApp',
        'redirectToSender', 'getCurrentAppIdOrSlug']);
      workspace = jasmine.createSpyObj('WorkspaceModel', ['countRequested', 'join', 'leave']);
      senderNavigationUpdateService = jasmine.createSpyObj('senderNavigationUpdateService',
          ['updateNavigation', 'prepareNavigationUpdateResponse']);
      $transitions = jasmine.createSpyObj('$transitions', ['onBefore']);
      $location = jasmine.createSpyObj('$location', ['path']);
      $urlRouter = jasmine.createSpyObj('$urlRouter', ['location']);

      inject(function (_$controller_, _$rootScope_, _$q_, _$state_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $state = _$state_;
        $q = _$q_;

        // mock screensize information
        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };
      });
    });

    var controllerName = 'WorkspacesShowController';

    describe('controller: ' + controllerName, function () {

      function buildController(apps) {
        return $controller(controllerName, {
          $scope: $scope,
          appService: appService,
          senderService: jasmine.createSpyObj('senderService', ['changeAvatar']),
          currentUser: user,
          workspace: workspace,
          apps: apps || [],
          senderNavigationUpdateService: senderNavigationUpdateService,
          $transitions: $transitions,
          $location: $location,
          $urlRouter: $urlRouter
        });
      }

      it('should init controller with no apps', function () {
        // given
        spyOn($rootScope, '$on');
        _.set(workspace, '_permissions.manage', true);
        workspace.countRequested.and.returnValue($q.resolve({count: 2}));

        // when
        buildController();
        $scope.$apply();

        // then
        expect(workspace.requestedCount).toBe(2);
        expect(appService.onAppChanged).toHaveBeenCalled();
        expect($rootScope.$on).toHaveBeenCalledTimes(8);
        expect($rootScope.$on.calls.argsFor(0)[0]).toBe('app:updated');
        expect($rootScope.$on.calls.argsFor(1)[0]).toBe('app:deleted');
        expect($rootScope.$on.calls.argsFor(2)[0]).toBe('app:created');
      });

      it('should abort transition to parent if same workspace', function () {
        //given
        _.set(workspace, '_permissions.manage', false);
        var transition = jasmine.createSpyObj('transition', ['params', 'abort', 'from', 'to']);
        transition.params.and.returnValue({
          idOrSlug: workspace.idOrSlug
        });
        transition.from.and.returnValue({name: 'main.pages.show'});
        transition.to.and.returnValue({name: 'main.pages'});
        $transitions.onBefore.and.callFake(function (criteria, callback) {
          callback(transition);
        });

        // when
        buildController();
        $scope.$apply();

        // then
        expect(transition.abort).toHaveBeenCalled();
      });

      it('should not abort transition to parent if target is a different workspace', function () {
        //given
        _.set(workspace, '_permissions.manage', false);
        var transition = jasmine.createSpyObj('transition', ['params', 'abort', 'from', 'to']);
        transition.params.and.returnValue({
          idOrSlug: workspace.idOrSlug + '-sibling'
        });
        transition.from.and.returnValue({name: 'main.pages.show'});
        transition.to.and.returnValue({name: 'main.pages'});
        $transitions.onBefore.and.callFake(function (criteria, callback) {
          callback(transition);
        });

        // when
        buildController();
        $scope.$apply();

        // then
        expect(transition.abort).not.toHaveBeenCalled();
      });

      it('should change current app on event', function () {
        // given
        var appOne = {id: 'APP-ID-ONE'};
        var appTwo = {id: 'APP-ID-TWO'};
        var apps = [appOne, appTwo];
        appService.getCurrentAppIdOrSlug.and.returnValue('APP-ID-ONE');

        _.set(workspace, '_permissions.manage', false);
        spyOn($state, 'is');
        $state.is.and.returnValue(true);
        appService.onAppChanged.and.callFake(function (callback) {
          callback(appTwo.id);
        });

        // when
        var ctrl = buildController(apps);

        // then
        expect(ctrl.currentApp.id).toBe('APP-ID-TWO');
      });

      it('should update app on event', function () {
        // given
        var app = {id: 'APP-ID-ONE'};
        _.set(workspace, '_permissions.manage', false);
        var apps = [{id: 'APP-ID-ONE'}, {id: 'APP-ID-ONE'}];

        // when
        var ctrl = buildController(apps);
        $rootScope.$emit('app:updated', app);
        $scope.$apply();

        // then
        expect(appService.updateApp).toHaveBeenCalledWith(app, ctrl.apps);
      });

      it('should delete app and redirect on event', function () {
        // given
        var app = {id: 'APP-ID-ONE'};
        var apps = [app];
        _.set(workspace, '_permissions.manage', false);
        spyOn($state, 'is');
        $state.is.and.returnValue(true);
        appService.getCurrentAppIdOrSlug.and.returnValue(app.id);

        // when
        buildController(apps);
        $rootScope.$emit('app:deleted', app.id);
        $scope.$apply();

        // then
        expect(appService.deleteApp).toHaveBeenCalledWith(app.id, apps);
        expect(appService.redirectToSender).toHaveBeenCalledWith(workspace, apps);
      });

      it('should join a workspace', function () {
        // given
        _.set(workspace, '_permissions.manage', false);
        spyOn($state, 'reload');
        workspace.join.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.join();
        $scope.$apply();

        // then
        expect($state.reload).toHaveBeenCalledWith('main.workspace.show');
      });

      it('should leave a workspace', function () {
        // given
        _.set(workspace, '_permissions.manage', false);
        spyOn($state, 'go');
        workspace.leave.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.leave();
        $scope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith('main.workspace');
      });

      it('should refresh app active state', function () {
        // given
        var app = {id: 'APP-ID-TWO', active: false};
        var apps = [app];
        workspace.countRequested.and.returnValue($q.resolve({count: 1}));
        appService.getCurrentAppIdOrSlug.and.returnValue(app.id);
        _.set(workspace, '_permissions.manage', true);
        spyOn($state, 'is');
        $state.is.and.returnValue(true);

        // when
        var ctrl = buildController(apps);
        $rootScope.$emit('app:updated', {id: 'APP-ID-TWO', active: true});
        $scope.$apply();

        // then
        expect(ctrl.currentApp.active).toBe(true);
      });

    });
  });

})();
