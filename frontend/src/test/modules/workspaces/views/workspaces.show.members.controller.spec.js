(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';

  describe('module: ' + moduleName, function () {

    var $controller, $state, $q, $scope, authService, userChooserModalService, userChooserExternalModalService,
        workspace, selfState, ExternalUserModel;

    beforeEach(module(moduleName));

    beforeEach(function () {
      selfState = 'main.workspace.show.members.invited';
      workspace = jasmine.createSpyObj('WorkspaceModel', ['inviteMembers', 'inviteAdmins']);
      workspace.slug = 'test-workspace';
      workspace.id = 'workspace-id';

      userChooserModalService = jasmine.createSpyObj('userChooserModalService', ['open']);
      userChooserExternalModalService = jasmine.createSpyObj('userChooserExternalModalService', ['open']);
      ExternalUserModel = jasmine.createSpyObj('ExternalUserModel', ['inviteExternals']);

      inject(function (_$controller_, _$q_, _$state_, _authService_, $rootScope) {
        $controller = _$controller_;
        $q = _$q_;
        $state = _$state_;
        authService = _authService_;
        $scope = $rootScope.$new();
      });
    });

    var controllerName = 'WorkspaceMembersController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        var controller = $controller(controllerName, {
          $scope: $scope,
          authService: authService,
          userChooserModalService: userChooserModalService,
          userChooserExternalModalService: userChooserExternalModalService,
          ExternalUserModel: ExternalUserModel,
          workspace: workspace
        });
        controller.$onInit();
        return controller;
      }

      it('should invite members', function () {
        // given
        spyOn($state, 'go');

        spyOn(authService, 'onGlobalPermissions').and.callFake(function (permissionNames, callback) {
          callback(true);
        });

        userChooserModalService.open.and.returnValue($q.resolve({userIds: ['USER-ID'], groupIds: ['GROUP-ID']}));
        workspace.inviteMembers.and.returnValue($q.resolve({'userid': 'INVITED'}));
        var ctrl = buildController();

        // when
        ctrl.inviteMembers();
        $scope.$apply();

        // then
        expect(userChooserModalService.open).toHaveBeenCalledWith({}, {usersOnly: false});
        expect(workspace.inviteMembers).toHaveBeenCalledWith({userIds: ['USER-ID'], groupIds: ['GROUP-ID']});
        expect($state.go).toHaveBeenCalledWith(selfState, {idOrSlug: workspace.slug, showDirectlyAddedHint: false},
            {reload: selfState});
      });

      it('should invite admins', function () {
        // given
        spyOn(authService, 'onGlobalPermissions').and.callFake(function (permissionNames, callback) {
          callback(true);
        });

        spyOn($state, 'go');
        userChooserModalService.open.and.returnValue($q.resolve({userIds: ['USER-ID'], groupIds: ['GROUP-ID']}));
        workspace.inviteAdmins.and.returnValue($q.resolve({'userid': 'INVITED'}));
        var ctrl = buildController();

        // when
        ctrl.inviteAdmins();
        $scope.$apply();

        // then
        expect(userChooserModalService.open).toHaveBeenCalledWith({}, {usersOnly: false, internalOnly: true});
        expect(workspace.inviteAdmins).toHaveBeenCalledWith({userIds: ['USER-ID'], groupIds: ['GROUP-ID']});
        expect($state.go).toHaveBeenCalledWith(selfState, {idOrSlug: workspace.slug, showDirectlyAddedHint: false},
            {reload: selfState});
      });

      it('should invite members without groups', function () {
        // given
        spyOn($state, 'go');

        spyOn(authService, 'onGlobalPermissions').and.callFake(function (permissionNames, callback) {
          callback(false);
        });

        userChooserModalService.open.and.returnValue($q.resolve({userIds: ['USER-ID'], groupIds: ['GROUP-ID']}));
        workspace.inviteMembers.and.returnValue($q.resolve({'userid': 'INVITED'}));
        var ctrl = buildController();

        // when
        ctrl.inviteMembers();
        $scope.$apply();

        // then
        expect(userChooserModalService.open).toHaveBeenCalledWith({}, {usersOnly: true});
        expect(workspace.inviteMembers).toHaveBeenCalledWith({userIds: ['USER-ID'], groupIds: ['GROUP-ID']});
        expect($state.go).toHaveBeenCalledWith(selfState, {idOrSlug: workspace.slug, showDirectlyAddedHint: false},
            {reload: selfState});
      });

      it('should invite admins without groups', function () {
        // given
        spyOn(authService, 'onGlobalPermissions').and.callFake(function (permissionNames, callback) {
          callback(false);
        });

        spyOn($state, 'go');
        userChooserModalService.open.and.returnValue($q.resolve({userIds: ['USER-ID'], groupIds: ['GROUP-ID']}));
        workspace.inviteAdmins.and.returnValue($q.resolve({'userid': 'INVITED'}));
        var ctrl = buildController();

        // when
        ctrl.inviteAdmins();
        $scope.$apply();

        // then
        expect(userChooserModalService.open).toHaveBeenCalledWith({}, {usersOnly: true, internalOnly: true});
        expect(workspace.inviteAdmins).toHaveBeenCalledWith({userIds: ['USER-ID'], groupIds: ['GROUP-ID']});
        expect($state.go).toHaveBeenCalledWith(selfState, {idOrSlug: workspace.slug, showDirectlyAddedHint: false},
            {reload: selfState});
      });

      it('should invite externals', function () {
        // given
        spyOn($state, 'go');

        spyOn(authService, 'onGlobalPermissions').and.callFake(function (permissionNames, callback) {
          callback(true);
        });

        userChooserExternalModalService.open.and.returnValue($q.resolve(['external@user.com']));
        ExternalUserModel.inviteExternals.and.returnValue($q.resolve({'external@user.com': 'INVITED'}));
        var ctrl = buildController();

        // when
        ctrl.inviteExternals();
        $scope.$apply();

        // then
        expect(userChooserExternalModalService.open).toHaveBeenCalledWith(workspace.id);
        expect(ExternalUserModel.inviteExternals).toHaveBeenCalledWith(workspace.id, ['external@user.com']);
        expect($state.go).toHaveBeenCalledWith(selfState, {idOrSlug: workspace.slug, showDirectlyAddedExternalsHint: false},
            {reload: selfState});
      });

      it('should show invited state with directly added hint when approved users were among selection', function () {
        // given
        spyOn($state, 'go');

        spyOn(authService, 'onGlobalPermissions').and.callFake(function (permissionNames, callback) {
          callback(true);
        });

        userChooserExternalModalService.open.and.returnValue($q.resolve(['external@user.com', 'existing_external@user.com']));
        ExternalUserModel.inviteExternals.and.returnValue($q.resolve({data: {'external@user.com': 'INVITED', 'userid': 'APPROVED'}}));
        var ctrl = buildController();

        // when
        ctrl.inviteExternals();
        $scope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith(selfState, {idOrSlug: workspace.slug, showDirectlyAddedExternalsHint: true},
            {reload: selfState});
      });
    });
  });

})();
