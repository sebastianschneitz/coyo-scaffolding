(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';
  var directiveName = 'coyo-workspace-subscribe';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $rootScope, $scope, subscriptionsService, user, workspace;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();

        workspace = {id: 'WORKSPACE-ID'};
        user = {id: 'user-id'};
        subscriptionsService = jasmine.createSpyObj('subscriptionsService', ['subscribe', 'unsubscribe', 'onSubscriptionChange']);

      }));

      var controllerName = 'WorkspaceSubscribeController';

      describe('controller: ' + controllerName, function () {

        function buildController(subscription) {
          subscriptionsService.onSubscriptionChange.and.callFake(function (userId, targetId, callback) {
            callback(subscription);
            $rootScope.$on('currentUser:updated', function (event, payload) {
              callback(payload);
            });
          });

          return $controller(controllerName, {
            $scope: $scope,
            subscriptionsService: subscriptionsService
          }, {
            user: user,
            workspace: workspace
          });
        }

        it('should subscribe when unsubscribed', function () {
          // given
          subscriptionsService.subscribe.and.returnValue($q.resolve());
          var ctrl = buildController();

          // when
          ctrl.toggle();
          $rootScope.$emit('currentUser:updated', {});
          $scope.$apply();

          // then
          expect(subscriptionsService.subscribe).toHaveBeenCalledWith(user.id, workspace.id, 'workspace', workspace.id);
          expect(ctrl.loading).toBe(false);
        });

        it('should unsubscribe when subscribed', function () {
          // given
          subscriptionsService.unsubscribe.and.returnValue($q.resolve());
          var subscriptions = [{userId: user.id, targetId: workspace.id}];
          var ctrl = buildController(subscriptions);

          // when
          ctrl.toggle();
          $rootScope.$emit('currentUser:updated');
          $scope.$apply();

          // then
          expect(subscriptionsService.unsubscribe).toHaveBeenCalledWith(user.id, workspace.id);
          expect(ctrl.loading).toBe(false);
        });

      });

    });

  });
})();
