(function () {
  'use strict';

  var moduleName = 'coyo.pages';
  var directiveName = 'oyocPageAutoSubscribeSettings';

  describe('module: ' + moduleName, function () {

    var $controller, page, $scope;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, $rootScope) {
        $controller = _$controller_;
        page = jasmine.createSpyObj('PageModel', ['isNew']);
        $scope = $rootScope.$new();
      }));

      var controllerName = 'PageAutoSubscribeSettingsController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {$scope: $scope}, {page: page});
        }

        it('should clear user and group IDs when changing to NONE', function () {
          // given
          var ctrl = buildController();
          page.autoSubscribeUserIds = ['user-id'];
          page.autoSubscribeGroupIds = ['group-id'];
          ctrl.page.autoSubscribeType = 'NONE';

          // when
          ctrl.onSelectionChange();

          // then
          expect(page.autoSubscribeUserIds).toEqual([]);
          expect(page.autoSubscribeGroupIds).toEqual([]);
        });

        it('should clear user and group IDs when changing to ALL', function () {
          // given
          var ctrl = buildController();
          page.autoSubscribeUserIds = ['user-id'];
          page.autoSubscribeGroupIds = ['group-id'];
          ctrl.page.autoSubscribeType = 'ALL';

          // when
          ctrl.onSelectionChange();

          // then
          expect(page.autoSubscribeUserIds).toEqual([]);
          expect(page.autoSubscribeGroupIds).toEqual([]);
        });

        it('should not clear user and group IDs when changing to SELECTED', function () {
          // given
          var ctrl = buildController();
          page.autoSubscribeUserIds = ['user-id'];
          page.autoSubscribeGroupIds = ['group-id'];
          ctrl.page.autoSubscribeType = 'SELECTED';

          // when
          ctrl.onSelectionChange();

          // then
          expect(page.autoSubscribeUserIds).toEqual(['user-id']);
          expect(page.autoSubscribeGroupIds).toEqual(['group-id']);
        });
      });
    });
  });
})();
