(function () {
  'use strict';

  var moduleName = 'coyo.pages';
  var targetName = 'PageImprintController';

  describe('module: ' + moduleName, function () {
    var $controller, $scope, page, UserModel, widgetLayoutService, $rootScope, $q;

    var admin1 = {
      id: 'userId1'
    };

    var admin2 = {
      id: 'userId2'
    };

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });

      inject(function (_$rootScope_, _$controller_, _$q_) {
        $rootScope = _$rootScope_;
        $q = _$q_;
        widgetLayoutService = jasmine.createSpyObj('widgetLayoutService',
            ['cancel', 'save', 'edit', 'onload', 'collect', 'fill']);
        widgetLayoutService.save.and.returnValue($q.resolve());
        widgetLayoutService.edit.and.returnValue($q.resolve());
        widgetLayoutService.cancel.and.returnValue($q.resolve());
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        page = {id: 'page-id',
          adminsIds: [admin1.id, admin2.id]};
        UserModel = jasmine.createSpyObj('UserModel', ['getUsers']);
        UserModel.getUsers.and.returnValue($q.resolve([admin1, admin2]));
      });
    });

    describe('controller: ' + targetName, function () {

      function buildController() {
        return $controller(targetName, {
          $scope: $scope,
          page: page,
          UserModel: UserModel,
          widgetLayoutService: widgetLayoutService
        });
      }

      it('should init', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        $rootScope.$apply();

        // then
        expect(UserModel.getUsers).toHaveBeenCalledWith(ctrl.page.adminIds);
        expect(ctrl.admins[0]).toBe(admin1);
        expect(ctrl.admins[1]).toBe(admin2);
      });

      it('should edit widget layout', function () {
        //given
        var ctrl = buildController();

        //when
        ctrl.edit($scope, false, false);

        //then
        expect(widgetLayoutService.edit).toHaveBeenCalled();
        expect(ctrl.editMode === true);
        expect(ctrl.isNew === false);
      });

      it('should cancel editing widget layout', function () {
        //given
        var ctrl = buildController();

        //when
        ctrl.cancel($scope, false, false);

        //then
        expect(widgetLayoutService.cancel).toHaveBeenCalled();
        expect(ctrl.editMode === false);
      });

      it('should save widget layout', function () {
        //given
        var ctrl = buildController();

        //when
        ctrl.save($scope, false, false);

        //then
        expect(widgetLayoutService.save).toHaveBeenCalled();
        expect(ctrl.editMode === false);
      });
    });
  });

})();
