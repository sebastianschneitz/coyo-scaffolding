(function () {
  'use strict';

  var moduleName = 'coyo.pages';
  var targetName = 'PageSettingsController';

  describe('module: ' + moduleName, function () {
    var $scope, $controller, $q, $state, $injector, modalService, ngxNotificationService, $rootScope;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });

      inject(function (_$rootScope_, _$controller_, _$q_) {
        $scope = _$rootScope_.$new();
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        $state = jasmine.createSpyObj('$state', ['go', 'href', 'is']);
        $state.is.and.returnValue(true);
        modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
        ngxNotificationService = jasmine.createSpyObj('ngxNotificationService', ['success']);
        $injector = jasmine.createSpyObj('$injector', ['get']);
        $injector.get.and.returnValue(ngxNotificationService);
      });
    });

    describe('controller: ' + targetName, function () {
      var pageOrigin, members;

      function buildController() {
        return $controller(targetName, {
          $state: $state,
          $scope: $scope,
          $rootScope: $rootScope,
          $injector: $injector,
          pageOrigin: pageOrigin,
          members: members,
          modalService: modalService
        });
      }

      beforeEach(function () {
        pageOrigin = jasmine.createSpyObj('pageOrigin', ['update', 'delete', 'isTranslationRequired']);
        pageOrigin.update.and.returnValue($q.resolve(pageOrigin));
        angular.extend(pageOrigin, {
          name: 'Some page',
          description: 'Description',
          defaultLanguage: null,
          slug: 'some-page',
          categories: [{
            id: 'cat123',
            name: 'Cat 123'
          }]
        });

        members = {
          adminIds: [1, 2],
          memberIds: [3, 4],
          adminGroupIds: [5],
          memberGroupIds: [6]
        };
      });

      it('should init', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.oldName).toEqual(pageOrigin.displayName);
        expect(ctrl.oldSlug).toEqual(pageOrigin.slug);
        expect(ctrl.pageOrigin.description).toEqual(pageOrigin.description);
        expect(ctrl.pageOrigin.adminIds).toEqual(members.adminIds);
        expect(ctrl.pageOrigin.memberIds).toEqual(members.memberIds);
        expect(ctrl.pageOrigin.adminGroupIds).toEqual(members.adminGroupIds);
        expect(ctrl.pageOrigin.memberGroupIds).toEqual(members.memberGroupIds);
        expect(ctrl.pageOrigin.categories[0].displayName).toEqual(pageOrigin.categories[0].name);
      });

      describe('save', function () {

        it('should save', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();

          // when
          ctrl.save();
          $scope.$apply();

          // then
          expect($state.is).toHaveBeenCalled();
          expect($state.go).toHaveBeenCalled();
        });
      });

      it('should delete', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        pageOrigin.delete.and.returnValue($q.resolve());

        // when
        ctrl.delete();
        $rootScope.$apply();

        // then
        expect(pageOrigin.delete).toHaveBeenCalled();
        expect(ngxNotificationService.success).toHaveBeenCalledWith('PAGE.DELETE.SUCCESS');
        expect($state.go).toHaveBeenCalledWith('main.page');
      });

      it('should delegate isTranslationRequired call to page', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();

        // when
        ctrl.isTranslationRequired('EN');

        // then
        expect(pageOrigin.isTranslationRequired).toHaveBeenCalledWith(ctrl.languages, ctrl.currentLanguage, 'EN');
      });

      it('should not inherit state params', function () {
        // when
        var ctrl = buildController();
        ctrl.$onInit();

        // then
        expect($state.href).toHaveBeenCalledWith('main.page', {}, {inherit: false});
      });

    });
  });
})();
