(function () {
  'use strict';

  describe('domain: CommentModel', function () {

    var $httpBackend, backendUrlService, CommentModel, UserModel;

    beforeEach(function () {

      module('coyo.domain');

      inject(function (_$httpBackend_, _backendUrlService_, _CommentModel_, _UserModel_) {
        $httpBackend = _$httpBackend_;
        backendUrlService = _backendUrlService_;
        CommentModel = _CommentModel_;
        UserModel = _UserModel_;

        $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
      });
    });

    it('should retrieve original author', function () {
      // given
      var result = undefined;
      spyOn(UserModel, 'get').and.callThrough();
      $httpBackend.expectGET(backendUrlService.getUrl() + '/web/users/userId').respond(200, {id: 'userId'});

      // when
      new CommentModel({
        id: 'commentId',
        originalAuthorId: 'userId'
      }).getOriginalAuthor().then(function (model) {
        result = model;
      });

      $httpBackend.flush();

      // then
      expect(result.id).toBe('userId');
      expect(UserModel.get).toHaveBeenCalledWith('userId');
    });
  });
}());
