(function () {
  'use strict';

  describe('domain: LaunchpadCategoryModel', function () {

    beforeEach(module('coyo.domain'));

    var $httpBackend, LaunchpadCategoryModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _LaunchpadCategoryModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      LaunchpadCategoryModel = _LaunchpadCategoryModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('order', function () {
        // given
        var ids = ['2', '3'];
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/launchpad/categories/order')
            .respond(200, {content: ids});

        // when
        var result = null;
        LaunchpadCategoryModel.order('2', '3')
            .then(function (response) {
              result = response;
            });
        $httpBackend.flush();

        // then
        expect(result.content).toEqual(ids);
      });
    });
  });
}());
