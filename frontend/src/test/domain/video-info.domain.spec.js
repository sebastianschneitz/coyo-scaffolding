(function () {
  'use strict';

  describe('domain: VideoInfoModel', function () {

    var $httpBackend, VideoInfoModel, coyoEndpoints, backendUrlService;

    beforeEach(function () {

      module('coyo.domain');

      inject(function (_VideoInfoModel_, _$httpBackend_, _coyoEndpoints_, _backendUrlService_) {
        VideoInfoModel = _VideoInfoModel_;
        $httpBackend = _$httpBackend_;
        coyoEndpoints = _coyoEndpoints_;
        backendUrlService = _backendUrlService_;

        $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
      });
    });

    describe('retrieve video info', function () {
      var videoUrl = 'http://test';

      it('should be successful', function () {
        // given
        var result = undefined,
            response = 'test';
        $httpBackend.whenPOST(coyoEndpoints.webPreviews.generate, {urls: [videoUrl]}).respond(response);

        // when
        VideoInfoModel.generateVideoInfo(videoUrl).then(function (_response_) {
          result = _response_;
        });
        $httpBackend.flush();

        // then
        expect(result).toBe(response);
      });
    });
  });
}());
