(function () {
  'use strict';

  describe('domain: WorkspaceModel', function () {

    var $rootScope, $httpBackend, WorkspaceModel, backendUrlService, workspace, Pageable;

    beforeEach(module('coyo.domain'));

    beforeEach(inject(function (_$rootScope_, _$httpBackend_, _WorkspaceModel_, _Pageable_, _backendUrlService_) {
      $rootScope = _$rootScope_;
      $httpBackend = _$httpBackend_;
      WorkspaceModel = _WorkspaceModel_;
      backendUrlService = _backendUrlService_;
      Pageable = _Pageable_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});

      spyOn($rootScope, '$emit').and.callThrough();
      workspace = new WorkspaceModel({id: 'workspace-id'});
    }));

    describe('instance members', function () {

      it('should trigger event on join', function () {
        // given
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/workspaces/workspace-id/users/join').respond(200, {
          status: 'APPROVED'
        });

        // when
        workspace.join();
        $httpBackend.flush();

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('workspace:joined', workspace);
      });

      it('should not trigger event on join if not approved', function () {
        // given
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/workspaces/workspace-id/users/join').respond(200, {
          status: !'APPROVED'
        });

        // when
        workspace.join();
        $httpBackend.flush();

        // then
        expect($rootScope.$emit).not.toHaveBeenCalledWith('workspace:joined', workspace);
      });

      it('should trigger event on leave', function () {
        // given
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/workspaces/workspace-id/users/leave').respond(200);

        // when
        workspace.leave();
        $httpBackend.flush();

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('workspace:left', workspace.id);
      });

      it('should trigger event on create', function () {
        // given
        delete workspace.id;
        $httpBackend.expectPOST(backendUrlService.getUrl() + '/web/workspaces').respond(200);

        // when
        workspace.create();
        $httpBackend.flush();

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('workspace:joined', workspace);
      });

      it('should trigger event on delete', function () {
        // given
        $httpBackend.expectDELETE(backendUrlService.getUrl() + '/web/workspaces/workspace-id').respond(200);

        // when
        workspace.delete();
        $httpBackend.flush();

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('workspace:left', workspace.id);
      });

      it('should get invited members including externals as Page', function () {
        // given
        var actual;
        var response = {
          'content': [{status: 'INVITED', workspace: 'workspace-id', user: 'invited-user'}],
          'aggregations': {},
          'totalPages': 1,
          'totalElements': 1,
          'size': 10,
          'number': 0,
          'first': true,
          'last': true,
          'numberOfElements': 1
        };
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/workspaces/workspace-id/invited/external/included')
            .respond(200, response);

        // when
        workspace.getInvitedExternalIncluded(new Pageable(0, 10)).then(function (result) {
          actual = result;
          expect(actual._config.url).toEqual('/web/workspaces/workspace-id/invited/external/included');
          expect(actual.content).toEqual([{status: 'INVITED', workspace: 'workspace-id', user: 'invited-user'}]);
        });
        $httpBackend.flush();

        // then
        expect(actual).toBeDefined();
      });
    });
  });
}());
