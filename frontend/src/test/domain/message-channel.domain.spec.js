(function () {
  'use strict';

  describe('domain: MessageChannelModel', function () {

    beforeEach(module('coyo.domain'));

    var $httpBackend, MessageChannelModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _MessageChannelModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      MessageChannelModel = _MessageChannelModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('should fetch mute of user', function () {
        // when
        var model = new MessageChannelModel({id: 123, channelId: 'test', members: [{user: {id: 123}, muted: true}]});
        var isMuted = model.isMuted(123);

        // Then
        expect(isMuted).toBeTruthy();
      });

      it('should fetch mute of user negated', function () {
        // when
        var model = new MessageChannelModel({id: 123, channelId: 'test', members: [{user: {id: 123}, muted: false}]});
        var isMuted = model.isMuted(123);
        // Then
        expect(isMuted).toBeFalsy();
      });
    });
  });
}());
