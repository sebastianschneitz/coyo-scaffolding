(function () {
  'use strict';

  describe('domain: AppModel', function () {
    beforeEach(module('coyo.domain'));

    var AppModel, app;

    beforeEach(inject(function (_AppModel_) {
      AppModel = _AppModel_;
      app = new AppModel({id: 'app-id'});
    }));

    describe('instance members', function () {

      it('should prepare translations for save', function () {
        // given
        var sender = jasmine.createSpyObj('sender', ['getDefaultLanguage']);
        sender.getDefaultLanguage.and.returnValue('DE');
        var languages = {
          DE: {
            active: true,
            translations: {
              name: 'Der Name'
            }
          },
          EN: {
            active: true,
            translations: {
              name: 'The name'
            }
          }
        };
        var viewModel = {sender: sender, languages: languages};

        // when
        app.prepareTranslationsForSave(viewModel);

        // then
        expect(app.defaultLanguage).toBe('DE');
        expect(app.translations).not.toEqual(jasmine.objectContaining({
          DE: {
            name: 'Der Name'
          }
        }));
        expect(app.translations).toEqual(jasmine.objectContaining({
          EN: {
            name: 'The name'
          }
        }));
      });
    });
  });
}());
