(function () {
  'use strict';

  describe('domain: ExternalUserModel', function () {

    var $rootScope, $httpBackend, ExternalUserModel, backendUrlService;

    beforeEach(module('coyo.domain'));

    beforeEach(inject(function (_$rootScope_, _$httpBackend_, _ExternalUserModel_, _backendUrlService_) {
      $rootScope = _$rootScope_;
      $httpBackend = _$httpBackend_;
      ExternalUserModel = _ExternalUserModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});

      spyOn($rootScope, '$emit').and.callThrough();
    }));

    describe('class members', function () {

      it('should check email', function () {
        // given
        $httpBackend.expectGET(backendUrlService.getUrl() +
          '/web/external-user/workspace/invitation/check-email?idOrSlug=workspaceId&email=mail').respond(200, {
          infoMessage: 'activeInvitation',
          isUsable: true
        });

        // when
        var result = null;
        ExternalUserModel.checkEmail('workspaceId', 'mail').then(function (data) {
          result = data;
        });
        $httpBackend.flush();

        // then
        expect(result).toBeDefined();
        expect(result.data.infoMessage).toBe('activeInvitation');
      });

      it('should invite externals', function () {
        // given
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/external-user/workspace/invite', function (jsonData) {
          var data = angular.fromJson(jsonData);
          expect(data.idOrSlug).toBe('workspaceId');
          expect(data.emailAddresses[0]).toBe('mail');
          return true;
        }).respond(200, [{email: 'mail'}]);

        // when
        var result = null;
        ExternalUserModel.inviteExternals('workspaceId', ['mail']).then(function (data) {
          result = data;
        });
        $httpBackend.flush();

        // then
        expect(result).toBeDefined();
        expect(result.data[0].email).toBe('mail');
      });

      it('should decline externals', function () {
        // given
        $httpBackend.expectDELETE(backendUrlService.getUrl() +
          '/web/external-user/workspace/delete-invitation?idOrSlug=workspaceId&email=mail').respond(204,
            [{email: 'mail'}]);

        // when
        ExternalUserModel.declineExternalMember('workspaceId', 'mail');
        $httpBackend.flush();
      });
    });
  });
}());
