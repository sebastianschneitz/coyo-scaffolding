(function () {
  'use strict';

  describe('domain: AppConfigurationModel', function () {
    beforeEach(module('coyo.domain'));

    var $httpBackend, AppConfigurationModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _AppConfigurationModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      AppConfigurationModel = _AppConfigurationModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('class members', function () {
      it('should save configuration', function () {
        // given
        var model = new AppConfigurationModel({key: 'app-key', activeForPages: true, activeForWorkspaces: true});
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/apps/configurations/app-key', function (data) {
          var payload = angular.fromJson(data);
          expect(payload.activeForPages).toBe(true);
          expect(payload.activeForWorkspaces).toBe(true);
          return true;
        }).respond(200);

        // when
        model.save();
        $httpBackend.flush();
      });

      it('should load configuration', function () {
        // given
        var app1 = {key: 'app1', activeForPages: true, activeForWorkspaces: false};
        var app2 = {key: 'app2', activeForPages: false, activeForWorkspaces: true};
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/apps/configurations').respond(200, [app1, app2]);

        // when
        var result = null;
        AppConfigurationModel.query().then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toBeDefined();
        expect(result).toEqual([
          new AppConfigurationModel(app1),
          new AppConfigurationModel(app2)
        ]);
      });

      it('should restrict an app', function () {
        // given
        var app = {key: 'app', moderatorsOnly: true};
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + '/web/apps/configurations/' + app.key + '/restrict')
            .respond(200);

        // when
        AppConfigurationModel.restrict(app.key);
        $httpBackend.flush();

        // then
      });

      it('should derestrict an app', function () {
        // given
        var app = {key: 'app', moderatorsOnly: false};
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + '/web/apps/configurations/' + app.key + '/derestrict')
            .respond(200);

        // when
        AppConfigurationModel.derestrict(app.key);
        $httpBackend.flush();

        // then
      });
    });
  });
}());
