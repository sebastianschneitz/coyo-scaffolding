(function () {
  'use strict';

  describe('domain: WidgetLayoutModel', function () {
    beforeEach(module('coyo.domain'));

    var $rootScope, WidgetLayoutModel, layout, WidgetModel;

    beforeEach(inject(function (_$rootScope_, _WidgetLayoutModel_, _WidgetModel_) {
      $rootScope = _$rootScope_;
      WidgetModel = _WidgetModel_;
      WidgetLayoutModel = _WidgetLayoutModel_;
      layout = new WidgetLayoutModel({
        _permissions: {
          manageWidgets: true
        },
        name: 'layout-name',
        widgets: {
          slot1: [
            {id: 'slot1-widget1'},
            {id: 'slot1-widget2'}
          ],
          slot2: [
            {id: 'slot2-widget1'}
          ]
        }
      });
    }));

    describe('instance members', function () {

      it('should cache the layout and its widgets', function () {
        // given
        spyOn(WidgetModel, 'cache');

        // when
        layout.cacheWithWidgets();

        // then
        expect(WidgetModel.cache).toHaveBeenCalledTimes(2);
        expect(WidgetModel.cache).toHaveBeenCalledWith('slot1', [
          new WidgetModel({id: 'slot1-widget1', _permissions: {manage: true}}),
          new WidgetModel({id: 'slot1-widget2', _permissions: {manage: true}})
        ]);
        expect(WidgetModel.cache).toHaveBeenCalledWith('slot2', [
          new WidgetModel({id: 'slot2-widget1', _permissions: {manage: true}})
        ]);
        var layoutFromCache;
        new WidgetLayoutModel({name: 'layout-name'}).getWithWidgetsFromCache().then(function (result) {
          layoutFromCache = result;
        });
        $rootScope.$apply();
        expect(layoutFromCache).toEqual(layout);
      });
    });
  });
}());
