(function () {
  'use strict';

  var moduleName = 'commons.auth';
  var targetName = 'deeplinkService';

  describe('module: ' + moduleName, function () {
    var STATE_NAME = 'where.i.came.from';
    var STATE_PARAMS = {foo: 'bar', baz: 'ploink'};

    var $sessionStorage;
    var deeplinkService;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.value('$sessionStorage', {});
      });

      inject(function (_$sessionStorage_, _deeplinkService_) {
        $sessionStorage = _$sessionStorage_;
        deeplinkService = _deeplinkService_;
      });
    });

    describe('service: ' + targetName, function () {

      /* setReturnToState */
      it('should be possible to store the current state name and parameters', function () {
        // when
        deeplinkService.setReturnToState(STATE_NAME, STATE_PARAMS);

        // then
        expect($sessionStorage.returnToState).toEqual(STATE_NAME);
        expect($sessionStorage.returnToStateParams).toEqual(STATE_PARAMS);
      });
    });

    describe('service: ' + targetName, function () {

      /* setReturnToState */
      it('should be possible to store the current state object and parameters', function () {
        // given
        var state = {bzzz: 'dontcare', name: STATE_NAME};

        // when
        deeplinkService.setReturnToState(state, STATE_PARAMS);

        // then
        expect($sessionStorage.returnToState).toEqual(STATE_NAME);
        expect($sessionStorage.returnToStateParams).toEqual(STATE_PARAMS);
      });
    });

    describe('service: ' + targetName, function () {

      /* setReturnToState */
      it('should be possible to store clear the state and its parameters', function () {
        // given
        $sessionStorage.returnToState = STATE_NAME;
        $sessionStorage.returnToStateParams = STATE_PARAMS;

        // when
        deeplinkService.clearReturnToState();

        // then
        expect($sessionStorage.returnToState).toBeNull();
        expect($sessionStorage.returnToStateParams).toBeNull();
      });
    });

    describe('service: ' + targetName, function () {

      /* getReturnToState */
      it('should be possible to get the state name', function () {
        // given
        $sessionStorage.returnToState = STATE_NAME;

        // when
        var returnToState = deeplinkService.getReturnToState();

        // then
        expect(returnToState).toEqual(STATE_NAME);
      });
    });

    describe('service: ' + targetName, function () {

      /* getReturnToStateParams */
      it('should be possible to get the state parameters', function () {
        // given
        $sessionStorage.returnToStateParams = STATE_PARAMS;

        // when
        var returnToStateParams = deeplinkService.getReturnToStateParams();

        // then
        expect(returnToStateParams).toEqual(STATE_PARAMS);
      });
    });
  });
})();
