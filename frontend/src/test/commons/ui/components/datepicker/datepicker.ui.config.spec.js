(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'uiDatetimePickerConfig';

  describe('module: ' + moduleName, function () {
    var uiDatetimePickerConfig;

    beforeEach(function () {
      module(moduleName);

      inject(function (_uiDatetimePickerConfig_) {
        uiDatetimePickerConfig = _uiDatetimePickerConfig_;
      });
    });

    describe('constant: ' + targetName, function () {
      it('should not append to body', function () {
        expect(uiDatetimePickerConfig.appendToBody).toBeFalsy();
      });
    });
  });

})();
