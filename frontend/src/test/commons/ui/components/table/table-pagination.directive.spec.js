(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller, $q;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_) {
      $controller = _$controller_;
      $q = _$q_;
    }));

    describe('directive: coyo-table-pagination', function () {

      var controllerName = 'TablePaginationController';

      describe('controller: ' + controllerName, function () {

        function buildController(onChange) {
          var ctrl = $controller(controllerName,
              {$attrs: {onChange: onChange}},
              {onChange: onChange});
          ctrl.page = jasmine.createSpyObj('Page', ['page']);
          ctrl.page._queryParams = {};
          ctrl.page.page.and.returnValue($q.resolve());
          return ctrl;
        }

        it('should change call page function with page number', function () {
          // given
          var ctrl = buildController();
          ctrl.page._queryParams._page = 1;

          // when
          ctrl.change();

          // then
          expect(ctrl.page.page).toHaveBeenCalledWith(1);
        });

        it('should change call custom function with page if defined', function () {
          // given
          var onChange = jasmine.createSpy('onChange').and.returnValue($q.resolve());
          var ctrl = buildController(onChange);
          ctrl.page._queryParams._page = 1;

          // when
          ctrl.change();

          // then
          expect(onChange).toHaveBeenCalledWith(ctrl.page);
        });

        describe('currentPage', function () {

          it('should subtract one from set value in setter', function () {
            // given
            var ctrl = buildController();

            // when
            ctrl.currentPage = 2;

            // then
            expect(ctrl.page._queryParams._page).toBe(1);
          });

          it('should add one to page index in getter', function () {
            // given
            var ctrl = buildController();
            ctrl.page._queryParams._page = 1;

            // when
            var result = ctrl.currentPage;

            // then
            expect(result).toBe(2);
          });
        });
      });
    });
  });
})();
