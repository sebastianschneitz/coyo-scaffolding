(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_) {
      $controller = _$controller_;
    }));

    describe('directive: coyo-table-sorting', function () {

      var ctrl, controllerName = 'TableSortingController';

      describe('controller: ' + controllerName, function () {

        beforeEach(function () {
          ctrl = $controller(controllerName);
          ctrl.page = jasmine.createSpyObj('Page', ['sort']);
        });

        it('should sort', function () {
          // given
          ctrl.property = 'prop';
          ctrl.isDirection = function (dir) {
            return dir === 'ASC';
          };

          // when
          ctrl.sort();

          // then
          expect(ctrl.page.sort).toHaveBeenCalledWith('prop,desc');
        });

        describe('isDirection', function () {
          it('should be false if sorting is undefined', function () {
            // when
            var result = ctrl.isDirection('ASC');

            // then
            expect(result).toBe(false);
          });

          it('should return false if properties do not match', function () {
            // given
            ctrl.property = 'prop1';
            ctrl.page.sorting = [{property: 'prop2', direction: 'ASC'}];

            // when
            var result = ctrl.isDirection('ASC');

            // then
            expect(result).toBe(false);
          });

          it('should return true if property and direction match', function () {
            // given
            ctrl.property = 'prop';
            ctrl.page.sorting = [{property: 'prop', direction: 'ASC'}];

            // when
            var result = ctrl.isDirection('ASC');

            // then
            expect(result).toBe(true);
          });
        });
      });
    });
  });
})();
