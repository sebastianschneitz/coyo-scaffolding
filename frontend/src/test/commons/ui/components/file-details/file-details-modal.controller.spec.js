(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'FileDetailsModalController';

  describe('module: ' + moduleName, function () {
    var $q, $controller, $filter, $scope, $rootScope, $uibModalInstance;
    var coyoEndpoints, FileModel, SenderModel, backendUrlService, CapabilitiesModel;
    var appId, sender, files, nextFiles, fileAuthors, currentUser, externalFileHandlerService;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$rootScope_, _$controller_, _$q_, _coyoEndpoints_, _FileModel_, _SenderModel_,
                       _CapabilitiesModel_) {
        $rootScope = _$rootScope_;
        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $q = _$q_;
        coyoEndpoints = _coyoEndpoints_;
        FileModel = _FileModel_;
        SenderModel = _SenderModel_;
        CapabilitiesModel = _CapabilitiesModel_;

        $filter = jasmine.createSpy('$filter');
        $filter.and.returnValue(function (contentType) {
          return (contentType || '').toUpperCase();
        });
        backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);
        backendUrlService.getUrl.and.returnValue('BASE-URL');
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close']);

        externalFileHandlerService = jasmine.createSpyObj('externalFileHandlerService', ['isExternalFile', 'isGoogleMediaType']);
        externalFileHandlerService.isExternalFile.and.returnValue(false);
        externalFileHandlerService.isGoogleMediaType.and.returnValue(false);

        sender = {
          id: 'SENDER-ID',
          displayName: 'Max Mustermann'
        };

        files = [{
          id: 'FILE1-ID',
          senderId: sender.id,
          name: 'file name',
          contentType: 'image'
        }, {
          id: 'FILE2-ID',
          senderId: sender.id,
          name: 'file name2',
          contentType: 'pdf'
        }];

        nextFiles = [{
          id: 'FILE3-ID',
          senderId: sender.id,
          name: 'file name3',
          contentType: 'image'
        }, {
          id: 'FILE4-ID',
          senderId: sender.id,
          name: 'file name4',
          contentType: 'pdf'
        }];

        fileAuthors = [{
          id: 'AUTHOR-ID1'
        }, {
          id: 'AUTHOR-ID2'
        }];

        currentUser = {
          id: 'user-id',
          displayName: 'Robert Lang'
        };

        spyOn(SenderModel, 'getWithPermissions').and.returnValue($q.resolve(sender));

        spyOn(FileModel, 'getWithPermissions').and.callFake(function (param) {
          if (param.id === files[0].id) {
            return angular.extend(files[0], {_permissions: {manage: true}});
          }
          if (param.id === files[1].id) {
            return angular.extend(files[1], {_permissions: {manage: false}});
          }
          if (param.id === nextFiles[0].id) {
            return angular.extend(nextFiles[0], {_permissions: {manage: false}});
          }
          return param;
        });
        appId = 'documents';
      });
    });

    describe('controller: ' + controllerName, function () {

      function initCtrl(files, currentIndex, appId, totalFiles, loadNext) {
        return $controller(controllerName, {
          $q: $q,
          $rootScope: $rootScope,
          $scope: $scope,
          $filter: $filter,
          $uibModalInstance: $uibModalInstance,
          FileModel: FileModel,
          SenderModel: SenderModel,
          backendUrlService: backendUrlService,
          coyoEndpoints: coyoEndpoints,
          files: files,
          currentIndex: currentIndex,
          linkToFileLibrary: true,
          showAuthors: true,
          fileAuthors: fileAuthors,
          appIdOrSlug: appId,
          filesTotal: totalFiles,
          loadNext: loadNext,
          currentUser: currentUser,
          externalFileHandlerService: externalFileHandlerService
        });
      }

      it('should init with array of library files', function () {
        // when
        var ctrl = initCtrl(files, 0, appId);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl).not.toBeUndefined();
        expect(ctrl.files).toBe(files);
        expect(ctrl.fileAuthors).toBe(fileAuthors);
        expect(ctrl.appIdOrSlug).toBe(appId);
        expect(ctrl.linkToFileLibrary).toBe(true);
        expect(ctrl.showAuthors).toBe(true);
        expect(ctrl.currentIndex).toBe(0);

        expect(ctrl.file.id).toBe(files[0].id);
        expect(ctrl.file.name).toBe(files[0].name);
        expect(ctrl.file.type).toBe('IMAGE');
        expect(ctrl.file.sender).toBe(sender);
        expect(ctrl.file._permissions.manage).toBe(true);
        expect(ctrl.previewUrl).toBe(coyoEndpoints.sender.preview);
        expect(ctrl.files[0]._permissions.manage).toBe(true);
        expect(ctrl.files[0].sender).toBe(sender);

        expect($filter).toHaveBeenCalledWith('fileTypeName');
        expect(FileModel.getWithPermissions)
            .toHaveBeenCalledWith({senderId: files[0].senderId, id: files[0].id}, {}, ['*']);
        expect(SenderModel.getWithPermissions).toHaveBeenCalledWith(files[0].senderId, {}, ['manage', 'createFile']);
      });

      it('should init with single library file', function () {
        // given
        var singleFile = files[1];

        // when
        var ctrl = initCtrl(singleFile, undefined, appId);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl).not.toBeUndefined();
        expect(ctrl.files).toEqual([singleFile]);
        expect(ctrl.fileAuthors).toBe(fileAuthors);
        expect(ctrl.appIdOrSlug).toBe('documents');
        expect(ctrl.linkToFileLibrary).toBe(true);
        expect(ctrl.showAuthors).toBe(true);
        expect(ctrl.currentIndex).toBe(0);

        expect(ctrl.file.id).toBe(singleFile.id);
        expect(ctrl.file.name).toBe(singleFile.name);
        expect(ctrl.file.type).toBe('PDF');
        expect(ctrl.file.sender).toBe(sender);
        expect(ctrl.file._permissions.manage).toBe(false);
        expect(ctrl.previewUrl).toBe(coyoEndpoints.sender.preview);
        expect(ctrl.files[0]._permissions.manage).toBe(false);
        expect(ctrl.files[0].sender).toBe(sender);

        expect($filter).toHaveBeenCalledWith('fileTypeName');
        expect(FileModel.getWithPermissions)
            .toHaveBeenCalledWith({senderId: singleFile.senderId, id: singleFile.id}, {}, ['*']);
        expect(SenderModel.getWithPermissions).toHaveBeenCalledWith(singleFile.senderId, {}, ['manage', 'createFile']);
      });

      it('should init with single attachment file', function () {
        // given
        var file = {
          id: 'ATTACHMENT-ID',
          attachment: true,
          groupId: 'GROUP-ID',
          name: 'file name',
          contentType: 'image',
          previewUrl: '/web/attachment/{{groupId}}/{{id}}'
        };

        // when
        var ctrl = initCtrl(file);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl).not.toBeUndefined();
        expect(ctrl.files).toEqual([file]);
        expect(ctrl.fileAuthors).toBe(fileAuthors);
        expect(ctrl.appIdOrSlug).toBeUndefined();
        expect(ctrl.currentIndex).toBe(0);

        expect(ctrl.file.id).toBe(file.id);
        expect(ctrl.file.name).toBe(file.name);
        expect(ctrl.file.type).toBe('IMAGE');
        expect(ctrl.file.sender).toBeUndefined();
        expect(ctrl.file._permissions).toBeUndefined();
        expect(ctrl.previewUrl).toBe(file.previewUrl);
        expect(ctrl.downloadUrl).toBe('BASE-URL/web/attachment/GROUP-ID/ATTACHMENT-ID');

        expect($filter).toHaveBeenCalledWith('fileTypeName');
        expect(SenderModel.getWithPermissions).not.toHaveBeenCalled();
        expect(backendUrlService.getUrl).toHaveBeenCalled();
      });

      it('should show next file', function () {
        // given
        var ctrl = initCtrl(files, 0, appId);
        ctrl.$onInit();
        $scope.$apply();
        FileModel.getWithPermissions.calls.reset();
        SenderModel.getWithPermissions.calls.reset();

        // when
        ctrl.next();
        $scope.$apply();

        // then
        expect(ctrl.currentIndex).toBe(1);
        expect(ctrl.file.id).toBe(files[1].id);
        expect(ctrl.file.name).toBe(files[1].name);
        expect(ctrl.file.type).toBe('PDF');

        expect(FileModel.getWithPermissions)
            .toHaveBeenCalledWith({senderId: files[1].senderId, id: files[1].id}, {}, ['*']);
        expect(SenderModel.getWithPermissions).toHaveBeenCalledWith(files[1].senderId, {}, ['manage', 'createFile']);
      });

      it('should show previous file', function () {
        // given
        var ctrl = initCtrl(files, 1, appId);
        ctrl.$onInit();
        $scope.$apply();
        FileModel.getWithPermissions.calls.reset();
        SenderModel.getWithPermissions.calls.reset();

        // when
        ctrl.previous();
        $scope.$apply();

        // then
        expect(ctrl.currentIndex).toBe(0);
        expect(ctrl.file.id).toBe(files[0].id);
        expect(ctrl.file.name).toBe(files[0].name);
        expect(ctrl.file.type).toBe('IMAGE');

        expect(FileModel.getWithPermissions)
            .toHaveBeenCalledWith({senderId: files[0].senderId, id: files[0].id}, {}, ['*']);
        expect(SenderModel.getWithPermissions).toHaveBeenCalledWith(files[0].senderId, {}, ['manage', 'createFile']);
      });

      it('should show next file', function () {
        // given
        var loadNext = jasmine.createSpy('loadNext');
        loadNext.and.returnValue($q.resolve(nextFiles));

        var ctrl = initCtrl(files, 1, appId, files.length + 2, loadNext);
        ctrl.$onInit();
        $scope.$apply();
        FileModel.getWithPermissions.calls.reset();
        SenderModel.getWithPermissions.calls.reset();

        // when
        ctrl.next();
        $scope.$apply();

        // then
        expect(loadNext).toHaveBeenCalled();
        expect(ctrl.currentIndex).toBe(2);
        expect(ctrl.file.id).toBe(nextFiles[0].id);
        expect(ctrl.file.name).toBe(nextFiles[0].name);

        expect(FileModel.getWithPermissions)
            .toHaveBeenCalledWith({senderId: nextFiles[0].senderId, id: nextFiles[0].id}, {}, ['*']);
        expect(SenderModel.getWithPermissions)
            .toHaveBeenCalledWith(nextFiles[0].senderId, {}, ['manage', 'createFile']);
      });

      it('should update current file', function () {
        // given
        var ctrl = initCtrl(files, 1, appId);
        ctrl.$onInit();
        $scope.$apply();
        FileModel.getWithPermissions.calls.reset();
        SenderModel.getWithPermissions.calls.reset();

        // when
        ctrl.updateFile();
        $scope.$apply();

        // then
        expect(ctrl.currentIndex).toBe(1);
        expect(ctrl.file.id).toBe(files[1].id);
        expect(ctrl.file.name).toBe(files[1].name);
        expect(ctrl.file.type).toBe('PDF');

        expect(FileModel.getWithPermissions)
            .toHaveBeenCalledWith({senderId: files[1].senderId, id: files[1].id}, {}, ['*']);
        expect(SenderModel.getWithPermissions).toHaveBeenCalledWith(files[1].senderId, {}, ['manage', 'createFile']);
      });


      it('should not show PDF available hint when file is a pdf and screen size is medium', function () {
        // given
        var singleFile = files[1];
        spyOn(CapabilitiesModel, 'pdfAvailable').and.returnValue($q.resolve(true));

        // when
        var ctrl = initCtrl(singleFile, undefined, appId);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.showPDFLoadingHint).toBeFalse();
        expect(CapabilitiesModel.pdfAvailable).not.toHaveBeenCalled();
      });

      it('should show PDF available hint when file is a pdf and screen size is small', function () {
        // given
        $rootScope.screenSize = {
          isXs: false,
          isSm: true,
          isMd: false,
          isLg: false,
          isRetina: true
        };
        var file = {
          id: 'ATTACHMENT-ID',
          attachment: true,
          groupId: 'GROUP-ID',
          name: 'file name',
          contentType: 'application/pdf',
          previewUrl: '/web/attachment/{{groupId}}/{{id}}'
        };
        spyOn(CapabilitiesModel, 'pdfAvailable').and.returnValue($q.resolve(true));

        // when
        var ctrl = initCtrl(file);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.showPDFLoadingHint).toBeTrue();
      });

      it('should show PDF available hint when no previewUrl is provided and use default previewUrl', function () {
        // given
        $rootScope.screenSize = {
          isXs: false,
          isSm: true,
          isMd: false,
          isLg: false,
          isRetina: true
        };
        var file = {
          id: 'ATTACHMENT-ID',
          attachment: true,
          groupId: 'GROUP-ID',
          name: 'file name',
          contentType: 'application/pdf',
          previewUrl: null
        };
        spyOn(CapabilitiesModel, 'pdfAvailable').and.returnValue($q.resolve(true));

        // when
        var ctrl = initCtrl(file);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.previewUrl).toEqual(coyoEndpoints.sender.preview);
        expect(ctrl.showPDFLoadingHint).toBeTruthy();
        expect(CapabilitiesModel.pdfAvailable).toHaveBeenCalled();
      });

      it('should show PDF available hint for Google Docs files', function () {
        // given
        $rootScope.screenSize = {
          isXs: false,
          isSm: true,
          isMd: false,
          isLg: false,
          isRetina: true
        };
        var file = {
          id: 'ATTACHMENT-ID',
          attachment: true,
          groupId: 'GROUP-ID',
          name: 'file name',
          contentType: 'some google media type',
          previewUrl: 'https://some/google/url'
        };
        spyOn(CapabilitiesModel, 'pdfAvailable').and.returnValue($q.resolve(true));
        externalFileHandlerService.isGoogleMediaType.and.returnValue(true);


        // when
        var ctrl = initCtrl(file);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.showPDFLoadingHint).toBeTrue();
        expect(CapabilitiesModel.pdfAvailable).toHaveBeenCalledWith('application/pdf');
      });
    });
  });
})();
