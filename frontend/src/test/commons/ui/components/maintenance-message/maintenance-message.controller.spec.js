(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $rootScope, $http, authService, $timeout;
    var message = {
      id: 'messageId',
      endDate: Date.now() + 10000,
      message: 'my maintenance message'
    };

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _$timeout_) {
      $controller = _$controller_;
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $http = jasmine.createSpyObj('$http', ['get']);
      $timeout = _$timeout_;
      authService = jasmine.createSpyObj('authService', ['getUser']);

      $http.get.and.returnValue(_$q_.resolve({data: message}));

      var user = jasmine.createSpyObj('UserModel', ['hasGlobalPermissions']);
      user.hasGlobalPermissions.and.callFake(function (permission) {
        return permission === 'P1';
      });
      authService.getUser.and.returnValue(_$q_.resolve(user));
    }));

    var controllerName = 'MaintenanceMessageController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $rootScope: $rootScope,
          adminStates: [{globalPermission: 'P1'}],
          $http: $http,
          authService: authService
        });
      }

      it('should init with message', function () {
        // when
        message.endDate = Date.now() + 10000;
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.display).toBe(true);
        expect(ctrl.message.message).toBe(message.message);
        expect(ctrl.message.id).toBe(message.id);
      });

      it('should remove expired message', function () {
        // given
        message.endDate = Date.now() + 10000;
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();
        $timeout.flush(9990);
        expect(ctrl.display).toBe(true);

        // when
        $timeout.flush(10);
        $scope.$apply();

        // then
        expect(ctrl.display).toBe(false);
      });

    });
  });

})();
