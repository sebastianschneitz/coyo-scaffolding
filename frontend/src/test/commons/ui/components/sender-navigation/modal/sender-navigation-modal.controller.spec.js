(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var modalService;

    beforeEach(function () {

      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.returnValue({
        result: {
          then: function (callback) {
            return callback('some-string-about-the-status');
          }
        }
      });

      module(moduleName, function ($provide) {
        $provide.value('modalService', modalService);
      });

    });

    describe('controller: SenderNavigationModalController', function () {

      var sender, apps, currentApp, SenderModelClass, senderModel, appService, appRegistry, appSettingsModalService,
          appTranslationsModalService, senderNavigationUpdateService,
          $scope, $q, $controller, $state, $uibModalInstance;
      sender = {'id': 'SENDER-TEST-ID'};
      sender.appNavigation = [{
        name: 'Title 1',
        apps: ['app-id-1', 'app-id-2']
      }, {
        name: 'Title 2',
        apps: ['app-id-3', 'app-id-4']
      }, {
        name: 'Title 3',
        apps: ['app-id-1']
      }];
      apps = [{
        'id': 'app-id-1',
        'name': 'app1',
        'key': 'blog'
      }, {
        'id': 'app-id-2',
        'name': 'app2',
        'key': 'blog'
      }, {
        'id': 'app-id-3',
        'name': 'app3',
        'key': 'blog'
      }, {
        'id': 'app-id-4',
        'name': 'app4',
        'key': 'blog'
      }, {
        'id': 'app-id-5',
        'name': 'app5',
        'key': 'blog'
      }];
      currentApp = {
        'id': 'app-id-1',
        'name': 'app1',
        'key': 'blog',
        'senderId': 'test'
      };

      beforeEach(function () {

        inject(function ($rootScope, _$q_, _$controller_, _$state_, _appRegistry_, _appService_,
                         _appSettingsModalService_, _appTranslationsModalService_, _senderNavigationUpdateService_) {
          $scope = $rootScope.$new();
          $q = _$q_;
          $controller = _$controller_;
          $state = _$state_;
          appRegistry = _appRegistry_;
          appService = _appService_;
          appSettingsModalService = _appSettingsModalService_;
          appTranslationsModalService = _appTranslationsModalService_;
          senderNavigationUpdateService = _senderNavigationUpdateService_;
        });

        spyOn(appRegistry, 'getIcon').and.returnValue('blog');

        spyOn(appService, 'addApp').and.returnValue({'appResponseBody': {}});
        spyOn(appSettingsModalService, 'open').and.callThrough();

        senderModel = jasmine.createSpyObj('senderModel', ['updateNavigation']);
        senderModel.updateNavigation.and.returnValue($q.resolve({'appNavigation': [{}]}));

        SenderModelClass = function (data) {
          return angular.extend(senderModel, data);
        };

        SenderModelClass.get = jasmine.createSpy('get');
        SenderModelClass.get.and.returnValue($q.resolve(sender));

        spyOn(senderNavigationUpdateService, 'prepareNavigationUpdateResponse').and
            .returnValue({'appNavigation': [{}]});
        var translationDeferred = $q.defer();
        translationDeferred.resolve(sender);
        spyOn(appTranslationsModalService, 'open').and.returnValue(translationDeferred.promise);
      });

      function buildController() {
        return $controller('SenderNavigationModalController', {
          $q: $q,
          $uibModalInstance: $uibModalInstance,
          sender: sender,
          apps: apps,
          currentApp: currentApp,
          SenderModel: SenderModelClass,
          appRegistry: appRegistry,
          senderNavigationUpdateService: senderNavigationUpdateService
        });
      }

      it('should open app translation modal with sender and generate navGroup object', function () {
        // given sender

        // when
        var ctrl = buildController();
        ctrl.openTranslations(ctrl.sender);

        // then
        expect(appTranslationsModalService.open).toHaveBeenCalledWith(ctrl.sender);
        expect(ctrl.navGroups).toBeDefined();
      });

      it('should add new group to navigation and save', function () {
        // given sender

        // when
        var ctrl = buildController();
        ctrl.addGroup();
        $scope.$apply();

        // then
        expect(senderModel.updateNavigation).toHaveBeenCalledTimes(1);
        expect(senderNavigationUpdateService.prepareNavigationUpdateResponse)
            .toHaveBeenCalledWith({'appNavigation': [{}]});
      });

      it('should open app settings modal to edit current app', function () {
        // given sender
        // when
        var ctrl = buildController();
        ctrl.editApp();
        $scope.$apply();

        // then
        expect(appSettingsModalService.open).toHaveBeenCalledWith(ctrl.currentApp, sender);
      });

      it('should call sender method join workspace and reload state', function () {
        // given sender
        // when

        var ctrl = buildController();
        spyOn($state, 'reload');
        ctrl.sender.join = jasmine.createSpy('join');
        ctrl.sender.join.and.returnValue($q.resolve());
        ctrl.join();
        $scope.$apply();

        // then
        expect(sender.join).toHaveBeenCalled();
        expect($state.reload).toHaveBeenCalledWith('main.workspace.show');
      });

      it('should call sender method leave workspace and go back to state main.workspace', function () {
        // given sender
        // when

        var ctrl = buildController();
        spyOn($state, 'go');
        ctrl.sender.leave = jasmine.createSpy('leave');
        ctrl.sender.leave.and.returnValue($q.resolve());
        ctrl.leave();
        $scope.$apply();

        // then
        expect(sender.leave).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('main.workspace');
      });

    });
  });
})();
