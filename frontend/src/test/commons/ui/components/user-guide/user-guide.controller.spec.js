(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'UserGuideController';

  describe('module: ' + moduleName, function () {
    var $controller, $httpBackend, userGuideService, backendUrlService;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$httpBackend_, _$translate_, _userGuideService_, _backendUrlService_) {
        $controller = _$controller_;
        $httpBackend = _$httpBackend_;
        userGuideService = _userGuideService_;
        backendUrlService = _backendUrlService_;

        spyOn(_$translate_, 'use').and.returnValue('en');
      });
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl(guideKey) {
        return $controller(targetName, {
          key: guideKey
        });
      }

      it('should init controller', function () {
        // given
        var key = 'my-guide';
        spyOn(userGuideService, 'getGuideDefinition').and.callThrough();
        spyOn(userGuideService, 'getUserGuideMainUrl').and.callThrough();

        // when
        var ctrl = buildCtrl(key);

        // then
        expect(ctrl.loading).toBeTruthy();
        expect(ctrl.key).toBe(key);
        expect(userGuideService.getGuideDefinition).toHaveBeenCalledTimes(1);
        expect(userGuideService.getUserGuideMainUrl).toHaveBeenCalledTimes(1);
      });

      it('should load and display guide', function () {
        // given
        var key = 'my-guide';
        var guide = 'My Guide';
        var content = '## My Guide';
        spyOn(userGuideService, 'getGuideUrl').and.callThrough();

        $httpBackend.expectGET(backendUrlService.getUrl() + '/docs/guides/user-guides.json')
            .respond({en: {'my-guide': guide}});
        $httpBackend.expectGET(backendUrlService.getUrl() + '/docs/guides/user/en/my-guide.md')
            .respond(content);

        // when
        var ctrl = buildCtrl(key);
        $httpBackend.flush();

        // then
        expect(ctrl.loading).toBeFalsy();
        expect(ctrl.key).toBe(key);
        expect(ctrl.guide).toEqual(guide);
        expect(ctrl.content).toEqual(content);
        expect(userGuideService.getGuideUrl).toHaveBeenCalledTimes(1);
      });

      it('should display error when guide definition fails to load', function () {
        // given
        var key = 'my-guide';

        $httpBackend.expectGET(backendUrlService.getUrl() + '/docs/guides/user-guides.json').respond(404);

        // when
        var ctrl = buildCtrl(key);
        $httpBackend.flush();

        // then
        expect(ctrl.loading).toBeFalsy();
        expect(ctrl.key).toBe(key);
        expect(ctrl.error).toBeTruthy();
      });

      it('should display "notFound" page when guide fails to load', function () {
        // given
        var key = 'my-guide';
        spyOn(userGuideService, 'getGuideUrl').and.callThrough();

        $httpBackend.expectGET(backendUrlService.getUrl() + '/docs/guides/user-guides.json')
            .respond({en: {'my-guide': 'My Guide'}});
        $httpBackend.expectGET(backendUrlService.getUrl() + '/docs/guides/user/en/my-guide.md')
            .respond(404);

        // when
        var ctrl = buildCtrl(key);
        $httpBackend.flush();

        // then
        expect(ctrl.loading).toBeFalsy();
        expect(ctrl.key).toBe(key);
        expect(ctrl.notFound).toBeTruthy();
        expect(userGuideService.getGuideUrl).toHaveBeenCalledTimes(1);
      });
    });
  });
})();
