(function () {
  'use strict';

  var moduleName = 'commons.ui',
      componentName = 'coyoRenderBirthday',
      controllerName = 'RenderBirthdayController';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $rootScope, $filter, amDateFormatFilter, translateFilter, birthdayService;

    var DATE_STRING = '2012-04-30',
        DATE_STRING_WITHOUT_YEAR = '05-25',
        FORMAT = 'FORMAT',
        FORMAT_INCLUDING_YEAR = 'FORMAT_INCLUDING_YEAR',
        FALLBACK_FORMAT = 'FALLBACK_FORMAT',
        FALLBACK_FORMAT_INCLUDING_YEAR = 'FALLBACK_FORMAT_INCLUDING_YEAR',
        REFERENCE_LEAP_YEAR = '2016';

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$filter_) {
      $controller = _$controller_;
      $scope = _$rootScope_.$new();
      $rootScope = _$rootScope_;
      $filter = _$filter_;
    }));

    beforeEach(function () {
      birthdayService = jasmine.createSpyObj('birthdayService', ['getReferenceLeapYear', 'hasYear']);
      birthdayService.getReferenceLeapYear.and.returnValue(REFERENCE_LEAP_YEAR);
      birthdayService.hasYear.and.returnValue(true);

      amDateFormatFilter = jasmine.createSpy('amDateFormatFilter');
      amDateFormatFilter.and.callFake(function (dateString, format) {
        return dateString + format;
      });
      translateFilter = jasmine.createSpy('translateFilter');
      translateFilter.and.returnValue(FALLBACK_FORMAT);
      $filter = jasmine.createSpy('$filter');
      $filter.and.callFake(function (filterType) {
        if (filterType === 'translate') {
          return translateFilter;
        } else {
          return amDateFormatFilter;
        }
      });

      $rootScope.dateFormat = {medium: FALLBACK_FORMAT_INCLUDING_YEAR};
    });

    describe('component: ' + componentName, function () {

      describe('controller: ' + controllerName, function () {

        function buildController(dateString, format, formatIncludingYear) {
          var ctrl = $controller(controllerName, {
            $scope: $scope,
            $rootScope: $rootScope,
            $filter: $filter,
            birthdayService: birthdayService
          }, {
            date: dateString,
            format: format,
            formatIncludingYear: formatIncludingYear
          });
          ctrl.$onInit();
          return ctrl;
        }

        describe('init', function () {

          it('should provide display value for given date string with year and format with year', function () {
            // given

            // when
            var ctrl = buildController(DATE_STRING, FORMAT, FORMAT_INCLUDING_YEAR);
            $scope.$apply();

            // then
            expect(ctrl.displayValue).toBe(DATE_STRING + FORMAT_INCLUDING_YEAR);
          });

          it('should provide display value for given date string with year and format without year', function () {
            // given

            // when
            var ctrl = buildController(DATE_STRING, FORMAT);
            $scope.$apply();

            // then
            expect(ctrl.displayValue).toBe(DATE_STRING + FORMAT);
          });

          it('should provide display value for given date string with year and fallback format with year', function () {
            // given

            // when
            var ctrl = buildController(DATE_STRING);
            $scope.$apply();

            // then
            expect(ctrl.displayValue).toBe(DATE_STRING + FALLBACK_FORMAT_INCLUDING_YEAR);
          });

          it('should provide display value for given date string without year and format without year', function () {
            // given
            var expectedDisplayValue = REFERENCE_LEAP_YEAR + '-' + DATE_STRING_WITHOUT_YEAR + FORMAT;
            birthdayService.hasYear.and.returnValue(false);

            // when
            var ctrl = buildController(DATE_STRING_WITHOUT_YEAR, FORMAT, FORMAT_INCLUDING_YEAR);
            $scope.$apply();

            // then
            expect(ctrl.displayValue).toBe(expectedDisplayValue);
          });

          it('should provide display value for given date string without year and fallback format without year', function () {
            // given
            var expectedDisplayValue = REFERENCE_LEAP_YEAR + '-' + DATE_STRING_WITHOUT_YEAR + FALLBACK_FORMAT;
            birthdayService.hasYear.and.returnValue(false);

            // when
            var ctrl = buildController(DATE_STRING_WITHOUT_YEAR);
            $scope.$apply();

            // then
            expect(ctrl.displayValue).toBe(expectedDisplayValue);
          });
        });

        describe('active', function () {
          it('should provide display value for changed date string with year and format with year', function () {
            // given
            var changedDateString = '2010-01-15';
            var ctrl = buildController(DATE_STRING, FORMAT, FORMAT_INCLUDING_YEAR);
            $scope.$apply();
            expect(ctrl.displayValue).toBe(DATE_STRING + FORMAT_INCLUDING_YEAR);

            // when
            ctrl.date = changedDateString;
            $scope.$apply();

            // then
            expect(ctrl.displayValue).toBe(changedDateString + FORMAT_INCLUDING_YEAR);
          });
        });
      });
    });
  });
})();
