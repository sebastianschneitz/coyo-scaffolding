(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'CategoryFilterController';

  describe('module: ' + moduleName, function () {

    var $controller, $rootScope, $scope, $q, $event, $injector, modalService, onSelectionChange,
        CategoryModelMock, categoryModel, bindings, ngxNotificationService;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();
        $q = _$q_;
      });

      $injector = jasmine.createSpyObj('$injector', ['get']);
      ngxNotificationService = jasmine.createSpyObj('ngxNotificationService', ['error']);
      $injector.get.and.returnValue(ngxNotificationService);
      modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);

      onSelectionChange = jasmine.createSpy('onSelectionChange');

      categoryModel = angular.extend(jasmine.createSpyObj('category', ['save', 'delete']), {id: 1, name: 'new cat1'});
      categoryModel.save.and.returnValue($q.resolve(categoryModel));
      categoryModel.delete.and.returnValue($q.resolve());
      CategoryModelMock = jasmine.createSpy('CategoryModel').and.returnValue(categoryModel);
      CategoryModelMock.order = jasmine.createSpy('order');

      bindings = {
        categories: [{id: 1, name: 'cat1'}, {id: 2, name: 'cat2'}, {id: 3, name: 'cat3'}],
        filterModel: [],
        categoryModelClass: CategoryModelMock,
        onSelectionChange: onSelectionChange
      };

      $event = jasmine.createSpyObj('$event', ['preventDefault', 'stopImmediatePropagation']);
    });

    describe('controller: ' + targetName, function () {

      function buildController() {
        var ctrl = $controller(targetName, {
          $scope: $scope,
          $injector: $injector,
          modalService: modalService
        }, bindings);
        ctrl.$onInit();
        return ctrl;
      }

      it('should init', function () {
        // given
        spyOn($rootScope, '$on');
        $rootScope.$on.and.returnValue(function () {
        });

        // when
        var ctrl = buildController();

        // then
        expect(ctrl.treeOptions.dropped).toEqual(jasmine.any(Function));
        expect(onSelectionChange).not.toHaveBeenCalled();
        expect($rootScope.$on).toHaveBeenCalledWith('keyup:esc', jasmine.any(Function));
        expect($scope.$on).toHaveBeenCalledWith('$destroy', jasmine.any(Function));
      });

      it('should check if category is active', function () {
        // given
        bindings.filterModel = [1];
        var ctrl = buildController();
        ctrl.$onInit();

        // when
        var result1 = ctrl.isCategoryActive({id: 1});
        var result2 = ctrl.isCategoryActive({id: 2});

        // then
        expect(result1).toBe(true);
        expect(result2).toBe(false);
      });

      it('should select category', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();

        // when
        ctrl.toggleCategory({id: 1});

        // then
        expect(ctrl.isCategoryActive()).toBe(false);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection([1]));
      });

      it('should select multiple categories', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.toggleCategory({id: 1});

        // when
        ctrl.toggleCategory({id: 2});

        // then
        expect(ctrl.isCategoryActive()).toBe(false);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection([1, 2]));
      });

      it('should deselect a category', function () {
        // given
        var ctrl = buildController();
        ctrl.toggleCategory({id: 1});
        onSelectionChange.calls.reset();

        // when
        ctrl.toggleCategory({id: 1});

        // then
        expect(ctrl.isCategoryActive()).toBe(true);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection([]));
      });

      it('should select two categories and deselect the first again', function () {
        // given
        var ctrl = buildController();
        ctrl.toggleCategory({id: 1});
        ctrl.toggleCategory({id: 2});
        onSelectionChange.calls.reset();

        // when
        ctrl.toggleCategory({id: 1});

        // then
        expect(ctrl.isCategoryActive()).toBe(false);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection([2]));
      });

      it('should reset filter', function () {
        // given
        var ctrl = buildController();
        ctrl.toggleCategory({id: 1});
        onSelectionChange.calls.reset();

        // when
        ctrl.resetCategory();

        // then
        expect(ctrl.isCategoryActive()).toBe(true);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection([]));
      });

      it('should not toggle category when editing categories', function () {
        // given
        var ctrl = buildController();
        ctrl.editingCategory = true;

        // when
        ctrl.toggleCategory({id: 1});

        // then
        expect(onSelectionChange).not.toHaveBeenCalled();
      });

      it('should add a category', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.addCategory($event);
        $scope.$apply();

        // then
        expect(CategoryModelMock).toHaveBeenCalled();
        expect(ctrl.categories[ctrl.categories.length - 1]).toBe(categoryModel);
        expect(ctrl.categories.length).toBe(4);
        expect(ctrl.editingCategory).toBe(true);
      });

      function itShouldNotAddCategory(editingCategory, savingCategory) {
        it('should not add a category when editing is ' + editingCategory + ' and saving is ' + savingCategory, function () {
          // given
          var ctrl = buildController();
          ctrl.editingCategory = editingCategory;
          ctrl.savingCategory = savingCategory;

          // when
          ctrl.addCategory($event);

          // then
          expect(CategoryModelMock).not.toHaveBeenCalled();
          expect(ctrl.categories.length).toBe(3);
        });
      }

      itShouldNotAddCategory(true, true);
      itShouldNotAddCategory(true, false);
      itShouldNotAddCategory(false, true);

      it('should not add a category when saving a category', function () {
        // given
        var ctrl = buildController();
        ctrl.editingCategory = false;
        ctrl.savingCategory = true;

        // when
        ctrl.addCategory($event);

        // then
        expect(CategoryModelMock).not.toHaveBeenCalled();
        expect(ctrl.categories.length).toBe(3);
      });

      it('should edit a category', function () {
        // given
        var copyMock = spyOn(angular, 'copy').and.callThrough(),
            ctrl = buildController();

        // when
        ctrl.editCategory(ctrl.categories[0], $event);

        // then
        expect($event.preventDefault).toHaveBeenCalled();
        expect($event.stopImmediatePropagation).toHaveBeenCalled();
        expect(ctrl.editingCategory).toBe(true);
        expect(copyMock).toHaveBeenCalledWith(ctrl.categories[0]);
      });

      function itShouldNotEditCategory(editingCategory, savingCategory) {
        it('should not edit a category when editing is ' + editingCategory + ' and saving is ' + savingCategory, function () {
          // given
          var copyMock = spyOn(angular, 'copy').and.callThrough(),
              ctrl = buildController();
          ctrl.editingCategory = editingCategory;
          ctrl.savingCategory = savingCategory;

          // when
          ctrl.editCategory(ctrl.categories[0], $event);

          // then
          expect($event.preventDefault).toHaveBeenCalled();
          expect($event.stopImmediatePropagation).toHaveBeenCalled();
          expect(copyMock).not.toHaveBeenCalled();
        });
      }

      itShouldNotEditCategory(true, true);
      itShouldNotEditCategory(true, false);
      itShouldNotEditCategory(false, true);

      it('should save a category', function () {
        // given
        var ctrl = buildController();
        ctrl.editingCategory = true;

        // when
        ctrl.saveCategory(categoryModel, $event);

        // then
        expect($event.preventDefault).toHaveBeenCalled();
        expect($event.stopImmediatePropagation).toHaveBeenCalled();
        expect(ctrl.savingCategory).toBe(true);
        expect(categoryModel.save).toHaveBeenCalled();

        $scope.$apply();

        expect(ctrl.categories[0].name).toEqual(categoryModel.name);
        expect(ctrl.savingCategory).toBe(false);
        expect(ctrl.editingCategory).toBe(false);
      });

      it('should not save a category when saving categories', function () {
        // given
        var ctrl = buildController();
        ctrl.savingCategory = true;

        // when
        ctrl.saveCategory(categoryModel, $event);

        // then
        expect($event.preventDefault).toHaveBeenCalled();
        expect($event.stopImmediatePropagation).toHaveBeenCalled();
        expect(categoryModel.save).not.toHaveBeenCalled();
      });

      it('should not save an invalid category', function () {
        // given
        categoryModel.name = '';
        var ctrl = buildController();

        // when
        ctrl.saveCategory(categoryModel, $event);

        // then
        expect($event.preventDefault).toHaveBeenCalled();
        expect($event.stopImmediatePropagation).toHaveBeenCalled();
        expect(categoryModel.save).not.toHaveBeenCalled();
      });

      it('should not save N/A', function () {
        // given
        categoryModel.name = 'N/A';
        var ctrl = buildController();

        // when
        ctrl.saveCategory(categoryModel, $event);
        $scope.$apply();

        // then
        expect($event.preventDefault).toHaveBeenCalled();
        expect($event.stopImmediatePropagation).toHaveBeenCalled();
        expect(ngxNotificationService.error).toHaveBeenCalledWith('ERRORS.STATUSCODE.NAME_TAKEN', null, categoryModel);
        expect(ctrl.editingCategory).toBe(false);
        expect(ctrl.savingCategory).toBe(false);
      });

      it('should delete a category', function () {
        // given
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        var ctrl = buildController();

        // when
        ctrl.deleteCategory(categoryModel, $event);

        // then
        expect($event.preventDefault).toHaveBeenCalled();
        expect($event.stopImmediatePropagation).toHaveBeenCalled();

        $scope.$apply();

        expect(categoryModel.delete).toHaveBeenCalled();
        expect(ctrl.categories.length).toBe(2);
        expect(ctrl.savingCategory).toBe(false);
      });

      it('should initialize the categoryIcon with the default value', function () {
        // given
        // when
        var ctrl = buildController();
        // then
        expect(ctrl.categoryIcon).toBe('layers');
      });

      it('should override the categoryIcon with a custom value', function () {
        // given
        var iconLabel = 'apps';
        _.assign(bindings, {categoryIcon: iconLabel});
        // when
        var ctrl = buildController();
        // then
        expect(ctrl.categoryIcon).toBe(iconLabel);
      });

      function itShouldNotDeleteCategory(editingCategory, savingCategory) {
        it('should not delete a category when editing is ' + editingCategory + ' and saving is ' + savingCategory, function () {
          // given
          var ctrl = buildController();
          ctrl.editingCategory = editingCategory;
          ctrl.savingCategory = savingCategory;

          // when
          ctrl.deleteCategory(categoryModel, $event);

          // then
          expect($event.preventDefault).toHaveBeenCalled();
          expect($event.stopImmediatePropagation).toHaveBeenCalled();
          expect(modalService.confirmDelete).not.toHaveBeenCalled();
        });
      }

      itShouldNotDeleteCategory(true, true);
      itShouldNotDeleteCategory(true, false);
      itShouldNotDeleteCategory(false, true);

      function expectedSelection(selected) {
        return {
          selected: selected
        };
      }
    });
  });

})();
