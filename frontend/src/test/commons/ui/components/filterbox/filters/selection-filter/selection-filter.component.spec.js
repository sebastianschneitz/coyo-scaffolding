(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'SelectionFilterController';

  describe('module: ' + moduleName, function () {

    var $controller, onSelectionChange, onSingleSelectionChange, selectionFilterService, filterModel, bindings;

    var ITEMS = [
      {key: 'key1', count: 1},
      {key: 'key2', count: 2},
      {key: 'key3', count: 4}
    ];

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _selectionFilterService_) {
        $controller = _$controller_;
        selectionFilterService = _selectionFilterService_;
      });

      filterModel = selectionFilterService.builder().items(ITEMS).build();

      onSelectionChange = jasmine.createSpy('onSelectionChange');
      onSingleSelectionChange = jasmine.createSpy('onSingleSelectionChange');

      bindings = {
        filterModel: filterModel,
        onSelectionChange: onSelectionChange,
        onSingleSelectionChange: onSingleSelectionChange,
        singleSelect: false
      };
    });

    describe('controller: ' + targetName, function () {

      function buildController() {
        var ctrl = $controller(targetName, {}, bindings);
        return ctrl;
      }

      it('should init and filter for all items', function () {
        // when
        var ctrl = buildController();

        // then
        expect(ctrl.filterModel.allItem.active).toBe(true);
        expect(onSelectionChange).not.toHaveBeenCalled();
      });

      it('should select a filter', function () {
        // given
        bindings.singleSelect = true;
        var ctrl = buildController();

        // when
        ctrl.toggle('key1');

        // then
        expect(ctrl.filterModel.allItem.active).toBe(false);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection(['key1'], ['key1']));
        expect(onSingleSelectionChange).toHaveBeenCalledWith(expectedSelection('key1', 'key1'));
      });

      it('should deselect a filter', function () {
        // given
        bindings.singleSelect = true;
        var ctrl = buildController();
        ctrl.toggle('key1');
        onSelectionChange.calls.reset();

        // when
        ctrl.toggle('key1');

        // then
        expect(ctrl.filterModel.allItem.active).toBe(true);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection([], ['key1']));
        expect(onSingleSelectionChange).toHaveBeenCalledWith(expectedSelection(undefined, 'key1'));
      });

      it('should select two filters', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.toggle('key1');
        ctrl.toggle('key3');

        // then
        expect(ctrl.filterModel.allItem.active).toBe(false);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection(['key1'], ['key1']));
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection(['key1', 'key3'], ['key3']));
      });

      it('should select two filters and deselect the first again', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.toggle('key1');
        ctrl.toggle('key3');
        ctrl.toggle('key1');

        // then
        expect(ctrl.filterModel.allItem.active).toBe(false);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection(['key1'], ['key1']));
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection(['key1', 'key3'], ['key3']));
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection(['key3'], ['key1']));
      });

      it('should clear a filter', function () {
        // given
        bindings.singleSelect = true;
        var ctrl = buildController();
        ctrl.toggle('key1');

        // when
        ctrl.clear();

        // then
        expect(ctrl.filterModel.allItem.active).toBe(true);
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection(['key1'], ['key1']));
        expect(onSingleSelectionChange).toHaveBeenCalledWith(expectedSelection('key1', 'key1'));
        expect(onSelectionChange).toHaveBeenCalledWith(expectedSelection([], ['key1']));
        expect(onSingleSelectionChange).toHaveBeenCalledWith(expectedSelection(undefined, 'key1'));
      });

      it('should show expected number of filter list items', function () {
        // given
        bindings.filterModel.items = ['Filter1', 'Filter2', 'Filter3'];
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(_.size(ctrl.filterModel.items)).toBe(3);
      });

      it('should show expected number of filter list items with pagination', function () {
        // given
        bindings.filterModel.items = _.range(15);
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.limitTo).toBe(7);
      });

      it('should add filter to list when clicking on filter expand link', function () {
        // given
        bindings.filterModel.items = _.range(15);
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        expect(ctrl.limitTo).toBe(7);
        ctrl.onClickExpand();

        // then
        expect(ctrl.limitTo).toBe(14);
      });

      it('should show expand button when max list length exceeds', function () {
        // given

        bindings.filterModel.items = _.range(15);
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.showExpand).toBe(true);
      });

      it('should not show expand button when max list length does not exceed', function () {
        // given
        bindings.filterModel.items = _.range(5);
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.showExpand).toBe(false);
      });

      it('should hide expand button when all list items are revealed', function () {
        // given
        bindings.filterModel.items = _.range(12);
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.showExpand).toBe(true);
        ctrl.onClickExpand();
        expect(ctrl.showExpand).toBe(false);
      });

      it('should move active aggregation to the top', function () {
        // given
        var aggregationItems = [
          {key: 'AggregationItem 1', active: false},
          {key: 'AggregationItem 2', active: true},
          {key: 'AggregationItem 3', active: false},
          {key: 'AggregationItem 4', active: true},
        ];
        bindings.filterModel.items = aggregationItems;
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.filterModel.items[0].active).toBe(true);
        expect(ctrl.filterModel.items[1].active).toBe(true);
        expect(ctrl.filterModel.items[2].active).toBe(false);
        expect(ctrl.filterModel.items[3].active).toBe(false);
      });

      it('should keep the count order for active aggregation items', function () {
        // given
        var aggregationItems = [
          {key: 'AggregationItem 1', active: false, count: 10},
          {key: 'AggregationItem 2', active: true, count: 9},
          {key: 'AggregationItem 3', active: false, count: 8},
          {key: 'AggregationItem 4', active: true, count: 7},
        ];
        bindings.filterModel.items = aggregationItems;
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.filterModel.items[0].count).toBe(9);
        expect(ctrl.filterModel.items[1].count).toBe(7);
        expect(ctrl.filterModel.items[2].count).toBe(10);
        expect(ctrl.filterModel.items[3].count).toBe(8);
      });

      function expectedSelection(selected, toggled) {
        return {
          selected: selected,
          toggled: toggled
        };
      }
    });
  });

})();
