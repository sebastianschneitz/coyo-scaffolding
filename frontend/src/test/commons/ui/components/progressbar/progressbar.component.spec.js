(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyoProgressbar';

  describe('module: ' + moduleName, function () {

    var $controller, target;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {
        target = {};
        inject(function (_$controller_) {
          $controller = _$controller_;
        });
      });

      var controllerName = 'ProgressbarController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {}, {
            target: target,
            warningPercent: 90
          });
        }

        it('should calculate percentage correctly', function () {
          // given
          target.current = 20;
          target.max = 50;
          var ctrl = buildController(),
              expected = 40;

          // when
          var result = ctrl.percentage();

          // then
          expect(result).toEqual(expected);
          expect(ctrl.class).toEqual('');
        });

        it('should return 100 if max is 0', function () {
          // given
          target.current = 100;
          target.max = 0;
          var ctrl = buildController(),
              expected = 100;

          // when
          var result = ctrl.percentage();

          // then
          expect(result).toEqual(expected);
          expect(ctrl.class).toEqual('full');
        });

        it('should return 0 if current is not a number', function () {
          // given
          target.current = NaN;
          target.max = 100;
          var ctrl = buildController(),
              expected = 0;

          // when
          var result = ctrl.percentage();

          // then
          expect(result).toEqual(expected);
          expect(ctrl.class).toEqual('');
        });

        it('should return 0 if max is not a number', function () {
          // given
          target.current = 100;
          target.max = NaN;
          var ctrl = buildController(),
              expected = 0;

          // when
          var result = ctrl.percentage();

          // then
          expect(result).toEqual(expected);
          expect(ctrl.class).toEqual('');
        });

        it('should return 100 if above 100', function () {
          // given
          target.current = 101;
          target.max = 100;
          var ctrl = buildController(),
              expected = 100;

          // when
          var result = ctrl.percentage();

          // then
          expect(result).toEqual(expected);
          expect(ctrl.class).toEqual('full');
        });

        it('should warn if close to 100', function () {
          // given
          target.current = 9;
          target.max = 10;
          var ctrl = buildController(),
              expected = 90;

          // when
          var result = ctrl.percentage();

          // then
          expect(result).toEqual(expected);
          expect(ctrl.class).toEqual('warn');
        });
      });
    });
  });
})();
