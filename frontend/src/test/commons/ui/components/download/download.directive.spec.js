(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyoDownload';

  describe('module: ' + moduleName, function () {

    var $scope, $compile, template, $window, $timeout, $http, $q, stateLockService;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {
        module(function ($provide) {
          $window = {location: 'http://coyo.url'};
          $provide.value('$window', $window);
        });
      });

      beforeEach(inject(function ($rootScope, _$compile_, _$timeout_, _$http_, _$q_, _stateLockService_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $timeout = _$timeout_;
        $http = _$http_;
        $q = _$q_;
        stateLockService = _stateLockService_;
        template = '<a href coyo-download="someUrl"></a>';
      }));

      it('should redirect location to download url', function () {
        // given
        stateLockService.lock();
        var url = 'http://some.url';
        $scope.someUrl = url;
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();
        spyOn($http, 'head').and.returnValue($q.resolve({data: {status: 'SUCCESS'}}));

        // when
        element.triggerHandler('click');
        $scope.$apply();
        $timeout.flush();

        // then
        expect($window.location).toBe(url);
        expect(stateLockService.isLocked()).toBe(true);
      });
    });
  });
})();
