(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'CopyFileLinkController';

  describe('module: ' + moduleName, function () {

    var $controller, $injector, $window, ngxNotificationService, file;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_) {
      $controller = _$controller_;
      $injector = jasmine.createSpyObj('$injector', ['get']);
      ngxNotificationService = jasmine.createSpyObj('ngxNotificationService', ['success']);
      $injector.get.and.returnValue(ngxNotificationService);

      $window = {
        location: {
          host: 'testhost',
          protocol: 'http:'
        },
        encodeURIComponent: function (str) {
          return str;
        }
      };

      file = {
        id: 'file-id',
        senderId: 'sender-id',
        displayName: 'display-name'
      };
    }));

    describe('controller: ' + controllerName, function () {
      function buildController() {
        return $controller(controllerName, {
          $injector: $injector,
          $window: $window
        }, {file: file});
      }

      it('should init link', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.link).toBe('http://testhost/files/sender-id/file-id/display-name');
      });

      it('should show success message', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.success();

        // then
        expect(ngxNotificationService.success).toHaveBeenCalledWith('FILE_LIBRARY.COPY_LINK.SUCCESS');
      });
    });
  });

})();
