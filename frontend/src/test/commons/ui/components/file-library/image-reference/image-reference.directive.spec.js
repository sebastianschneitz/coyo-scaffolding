(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyoImageReference';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $q, $log, $httpParamSerializerJQLike, $http, FileModel, backendUrlService,
        errorService, file, testDomain;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {
        file = {
          id: 'FILE-ID',
          senderId: 'SENDER-ID',
          modified: new Date().getTime()
        };

        FileModel = jasmine.createSpyObj('FileModel', ['get']);

        testDomain = 'http://test.coyo4.com';
        errorService = jasmine.createSpyObj('errorService', ['suppressNotification']);
        backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);
        backendUrlService.getUrl.and.returnValue(testDomain);

        $log = jasmine.createSpyObj('$log', ['error']);

        inject(function (_$controller_, _$q_, _$httpParamSerializerJQLike_, _$http_, $rootScope) {
          $controller = _$controller_;
          $q = _$q_;
          $httpParamSerializerJQLike = _$httpParamSerializerJQLike_;
          $http = _$http_;
          $scope = $rootScope.$new();
        });
      });

      var controllerName = 'ImageReferenceController';

      describe('controller: ' + controllerName, function () {

        function buildController(sizeDefinitions) {
          return $controller(controllerName, {
            $scope: $scope,
            $log: $log,
            FileModel: FileModel,
            backendUrlService: backendUrlService,
            errorService: errorService
          }, {
            fileId: file.id,
            senderId: file.senderId,
            sizeDefinitions: sizeDefinitions
          });
        }

        function buildImageUrl(imageFile, params) {
          return testDomain
              + '/web/senders/'
              + imageFile.senderId
              + '/documents/'
              + imageFile.id
              + '?'
              + $httpParamSerializerJQLike(params);
        }

        it('should return url of original without sizeDefinitions set', function () {
          // given
          FileModel.get.and.returnValue($q.resolve(file));

          // when
          var ctrl = buildController();
          $scope.$apply();

          // then
          expect(ctrl.imageUrls).toBeEmptyObject();
          expect(ctrl.defaultUrl).toBeUndefined();
          expect(ctrl.imageUrl).toBe(buildImageUrl(file, {'modified': file.modified}));
        });

        it('should return image variants if sizeDefinitions are passed', function () {
          // given
          FileModel.get.and.returnValue($q.resolve(file));
          var sizeDefinitions = {
            'default': 'S',
            'screen-md': 'M',
            'screen-lg': 'XXL'
          };

          // when
          var ctrl = buildController(sizeDefinitions);
          $scope.$apply();

          // then
          expect(ctrl.imageUrl).toBe(buildImageUrl(file, {'modified': file.modified}));
          expect(ctrl.defaultUrl).toBe(buildImageUrl(file, {'modified': file.modified, type: 'S'})
              + ', ' + buildImageUrl(file, {'modified': file.modified, type: 'M'}) + ' 2x');
          expect(ctrl.imageUrls).toEqual({
            'screen-md': buildImageUrl(file, {'modified': file.modified, type: 'M'})
            + ', ' + buildImageUrl(file, {'modified': file.modified, type: 'L'}) + ' 2x',
            'screen-lg': buildImageUrl(file, {'modified': file.modified, type: 'XXL'})
            + ', ' + buildImageUrl(file, {'modified': file.modified, type: 'ORIGINAL'}) + ' 2x'
          });
        });

        it('should log an error if the default image size is missing', function () {
          // given
          FileModel.get.and.returnValue($q.resolve(file));
          var sizeDefinitions = {
            'screen-md': 'M',
            'screen-lg': 'XXL'
          };

          // when
          var ctrl = buildController(sizeDefinitions);
          $scope.$apply();

          // then
          expect(ctrl.imageUrl).toBe(buildImageUrl(file, {'modified': file.modified}));
          expect(ctrl.imageUrls).toBeEmptyObject();
          expect(ctrl.defaultUrl).toBeUndefined();
          expect($log.error).toHaveBeenCalled();
          expect($log.error.calls.mostRecent().args[0]).toMatch(/Default missing/);
        });

        it('should load preview status if image loaded with error and handle FAILED status', function () {
          // given
          FileModel.get.and.returnValue($q.resolve(file));
          spyOn($http, 'get').and.returnValue($q.resolve({data: {status: 'FAILED'}}));

          // when
          var ctrl = buildController(file);
          $scope.$apply();
          ctrl.onError();
          $scope.$apply();

          // then
          expect($http.get).toHaveBeenCalled();
          expect(ctrl.previewAvailable).toBeFalse();
          expect(ctrl.isProcessing).toBeFalse();
        });

        it('should load preview status if image loaded with error and set status to failed after unsuccessful reload', function () {
          // given
          var timestamp = 123456789;
          FileModel.get.and.returnValue($q.resolve(file));
          // reload would only happen if the status return SUCCESS (e.g. variant is saved in DB but not in FileStore)
          spyOn($http, 'get').and.returnValue($q.resolve({data: {status: 'SUCCESS'}}));
          spyOn(Date.prototype, 'getTime').and.callFake(function () {
            return timestamp;
          });

          // when
          var ctrl = buildController(file);
          $scope.$apply();
          ctrl.onError();
          $scope.$apply();

          expect(ctrl.imageUrl).toBe(buildImageUrl(file, {'modified': file.modified}) + '&t=' + timestamp,
              'triggering reload with timestamp was not invoked');
          ctrl.onError();
          $scope.$apply();

          // then
          //preview status should only be inquired once, not again after second loading error
          expect($http.get).toHaveBeenCalledTimes(1);
          expect(ctrl.previewAvailable).toBeFalse();
          expect(ctrl.isProcessing).toBeFalse();
        });

      });

    });

  });
})();
