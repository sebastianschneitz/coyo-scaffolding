(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'FilenameFormController';

  describe('module: ' + moduleName, function () {

    var $controller, bindings, focusTrigger, onSubmit;
    var FILENAME = 'filename';

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_) {
        $controller = _$controller_;
      });

      onSubmit = jasmine.createSpy('onSubmit');
      focusTrigger = {};

      bindings = {
        file: {
          name: FILENAME
        },
        focusTrigger: focusTrigger,
        onSubmit: onSubmit,
        saving: false
      };
    });

    describe('controller: ' + targetName, function () {

      function initCtrl() {
        var controller = $controller(targetName, {}, bindings);
        return controller;
      }

      it('should init', function () {
        // given

        // when
        var ctrl = initCtrl();

        // then
        expect(ctrl).not.toBeUndefined();
        expect(ctrl.file.name).toBe(FILENAME);
        expect(ctrl.focusTrigger).toBe(focusTrigger);
        expect(ctrl.onSubmit).toBe(onSubmit);
        expect(ctrl.saving).toBe(false);
      });

      it('should call submit handler', function () {
        // given
        var newFilename = 'new-filename',
            ctrl = initCtrl();
        ctrl.newFilename = newFilename;

        // when
        ctrl.handleSubmit();

        // then
        expect(ctrl.onSubmit).toHaveBeenCalledWith({file: ctrl.file, newFilename: newFilename});
      });

      it('should not call submit handler again while saving', function () {
        // given
        var ctrl = initCtrl();
        ctrl.saving = true;

        // when
        ctrl.handleSubmit();

        // then
        expect(ctrl.onSubmit).not.toHaveBeenCalled();
      });

    });
  });
})
();
