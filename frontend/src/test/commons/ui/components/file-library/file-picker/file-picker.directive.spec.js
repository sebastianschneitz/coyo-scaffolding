(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyoFilePicker';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $scope, fileLibraryModalService, ngModelController, appService;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {
        appService = jasmine.createSpyObj('appService', ['getCurrentAppIdOrSlug']);
        fileLibraryModalService = jasmine.createSpyObj('fileLibraryModalService', ['open']);
        ngModelController = jasmine.createSpyObj('ngModelController', ['$setViewValue']);

        inject(function (_$controller_, _$q_, $rootScope) {
          $q = _$q_;
          $scope = $rootScope.$new();
          $controller = _$controller_;
        });
      });

      var controllerName = 'FilePickerController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          var controller = $controller(controllerName, {
            fileLibraryModalService: fileLibraryModalService,
            appService: appService
          });
          controller.ngModel = ngModelController;
          return controller;
        }

        it('should select single file', function () {
          // given
          var oldFile = [{id: 'OLD-FILE-ID'}];
          var newFile = [{id: 'FILE-ID'}];
          var options = {selectMode: 'single'};

          var ctrl = buildController();
          ngModelController.$viewValue = oldFile;
          ctrl.options = options;
          appService.getCurrentAppIdOrSlug.and.returnValue(undefined);
          fileLibraryModalService.open.and.returnValue($q.resolve(newFile));

          // when
          ctrl.pickFile();
          $scope.$apply();

          // then
          expect(ngModelController.$setViewValue).toHaveBeenCalledWith(newFile);
        });

        it('should select single file with initial app folder', function () {
          // given
          var oldFile = [{id: 'OLD-FILE-ID'}];
          var newFile = [{id: 'FILE-ID'}];
          var options = {selectMode: 'single'};

          var app = {id: 'app-id', rootFolderId: 'rootFolder-id'};
          var initialFolder = {id: app.rootFolderId};
          var sender = jasmine.createSpyObj('SenderInstance', ['getApp']);
          sender.getApp.and.returnValue($q.resolve(app));
          sender.id = 'sender-id';

          var ctrl = buildController();
          ctrl.selectedFiles = oldFile;
          ctrl.options = options;
          ctrl.sender = sender;
          appService.getCurrentAppIdOrSlug.and.returnValue(app.id);
          fileLibraryModalService.open.and.returnValue($q.resolve(newFile));

          // when
          ctrl.pickFile();
          $scope.$apply();

          // then
          expect(fileLibraryModalService.open).toHaveBeenCalled();
          var args = fileLibraryModalService.open.calls.mostRecent().args;
          expect(args[0]).toBe(sender);
          expect(args[1].selectMode).toBe('single');
          expect(args[1].initialFolder).toEqual(initialFolder);
          expect(ngModelController.$setViewValue).toHaveBeenCalledWith(newFile);
        });

        it('should select multiple files', function () {
          // given
          var selectedFiles = [{id: 'FILE-ID-1'}, {id: 'FILE-ID-2'}];
          var addedFiles = [{id: 'FILE-ID-1'}, {id: 'FILE-ID-3'}];
          var options = {selectMode: 'multiple'};

          var ctrl = buildController();
          ngModelController.$viewValue = selectedFiles;
          ctrl.options = options;
          fileLibraryModalService.open.and.returnValue($q.resolve(addedFiles));

          // when
          ctrl.pickFile();
          $scope.$apply();

          // then
          expect(ngModelController.$setViewValue).toHaveBeenCalled();
          var updatedFiles = ngModelController.$setViewValue.calls.mostRecent().args[0];
          expect(updatedFiles).toBeArrayOfSize(3);
          expect(updatedFiles[0].id).toBe('FILE-ID-1');
          expect(updatedFiles[1].id).toBe('FILE-ID-2');
          expect(updatedFiles[2].id).toBe('FILE-ID-3');
        });

        it('should remove a selected file', function () {
          // given
          var selectedFiles = [{id: 'FILE-ID-1'}, {id: 'FILE-ID-2'}];

          var ctrl = buildController();
          ngModelController.$viewValue = selectedFiles;
          ctrl.options = {selectMode: 'multiple'};

          // when
          ctrl.removeFile(selectedFiles[0]);

          // then
          expect(ngModelController.$setViewValue).toHaveBeenCalled();
          var updatedFiles = ngModelController.$setViewValue.calls.mostRecent().args[0];
          expect(updatedFiles).toBeArrayOfSize(1);
          expect(updatedFiles[0].id).toBe('FILE-ID-2');
        });

        it('should not open app folder if optional param is set', function () {
          // given
          var ctrl = buildController();
          ctrl.noAppContext = true;
          appService.getCurrentAppIdOrSlug.and.returnValue('App-slug');
          ctrl.sender = jasmine.createSpyObj('SenderInstance', ['getApp']);
          fileLibraryModalService.open.and.returnValue($q.resolve({}));

          // when
          ctrl.pickFile();

          // then
          expect(ctrl.sender.getApp).toHaveBeenCalledTimes(0);
          expect(fileLibraryModalService.open).toHaveBeenCalled();
        });

        it('should not try to retrieve app if sender is not set', function () {
          // given
          var ctrl = buildController();
          appService.getCurrentAppIdOrSlug.and.returnValue('App-slug');
          fileLibraryModalService.open.and.returnValue($q.resolve({}));

          // when
          ctrl.pickFile();

          // then
          expect(fileLibraryModalService.open).toHaveBeenCalled();
        });

      });

    });

  });
})();
