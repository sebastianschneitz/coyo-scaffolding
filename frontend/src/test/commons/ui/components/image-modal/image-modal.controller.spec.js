(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'ImageModalController';

  describe('module: ' + moduleName, function () {
    var $document, $controller, $scope, $uibModalInstance;

    beforeEach(function () {
      module(moduleName);

      inject(function ($rootScope, _$controller_) {
        $document = jasmine.createSpyObj('$document', ['on', 'off']);
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close']);
      });
    });

    describe('controller: ' + controllerName, function () {

      function initCtrl() {
        return $controller(controllerName, {
          $document: $document,
          $scope: $scope,
          $uibModalInstance: $uibModalInstance,
          imageUrl: '/test/url',
          title: 'title'
        });
      }

      it('should bind key listener event', function () {
        // given
        var ctrl = initCtrl();

        // when
        ctrl.$onInit();

        // then
        expect($document.on).toHaveBeenCalled();
      });

      it('should unbind key listener event on destroy', function () {
        // given
        var ctrl = initCtrl();

        // when & then
        ctrl.$onInit();
        expect($document.off).not.toHaveBeenCalled();

        $scope.$destroy();
        expect($document.off).toHaveBeenCalled();
      });
    });
  });
})();
