(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'imageModalService';

  describe('module: ' + moduleName, function () {
    var imageModalService, $uibModal;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $uibModal = jasmine.createSpyObj('$uibModal', ['open']);
        $provide.value('$uibModal', $uibModal);
      });

      inject(function (_imageModalService_) {
        imageModalService = _imageModalService_;
      });
    });

    describe('controller: ' + controllerName, function () {
      it('should open modal', function () {
        // when
        imageModalService.open('/test/url', 'title');

        // then
        expect($uibModal.open).toHaveBeenCalled();
      });
    });
  });
})();
