(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyoIsolateForm';

  describe('module: ' + moduleName, function () {
    var $scope, $compile, invalidFormElement;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function ($rootScope, _$compile_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        invalidFormElement = '<input type="text" ng-model="doesNotExist" required>';
      }));

      it('should invalid nested form not make parent form invalid if isolated', function () {
        // given
        var template = '<form name="mainForm"><div ng-form="nestedForm" coyo-isolate-form>' +
            invalidFormElement + '</div></form>';

        // when
        $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect($scope.mainForm.$valid).toBe(true);
      });

      it('should invalid nested form make parent form invalid if not isolated', function () {
        // given
        var template = '<form name="mainForm"><div ng-form="nestedForm">' + invalidFormElement + '</div></form>';

        // when
        $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect($scope.mainForm.$valid).toBe(false);
      });
    });
  });

})();
