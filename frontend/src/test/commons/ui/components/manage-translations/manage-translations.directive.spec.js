(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'ManageTranslationsController';

  describe('module: ' + moduleName, function () {
    var $controller, $filter, $q, $scope;
    var manageTranslationsModalService, modalService, SettingsModel;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$filter_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $filter = _$filter_;
        $q = _$q_;
      });

      SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieveByKey']);
      SettingsModel.retrieveByKey.and.returnValue($q.resolve('true'));

      modalService = jasmine.createSpyObj('modalService', ['confirm', 'confirmDelete']);
    });

    function confirm(success, event) {
      return {
        result: {
          then: function (yes, no) {
            success ? yes() : event === 'cancel' ? no(event) : no();
            return {
              finally: function (finallyCallback) {
                finallyCallback();
              }
            };
          }
        }
      };
    }

    function event() {
      return jasmine.createSpyObj('Event', ['stopPropagation']);
    }

    function buildController() {
      return $controller(controllerName, {
        $filter: $filter,
        manageTranslationsModalService: manageTranslationsModalService,
        modalService: modalService,
        current: 'EN',
        SettingsModel: SettingsModel
      });
    }

    function setupLanguages(ctrl) {
      ctrl.default = 'EN';
      ctrl.languages = {
        'EN': {
          'active': true,
          'valid': true,
          'translations': {
            'name': 'Hello World'
          }
        },
        'DE': {
          'active': true,
          'valid': true,
          'translations': {
            'name': 'Hallo Welt'
          }
        }
      };
    }

    describe('controller: ' + controllerName, function () {
      it('should init the controller', function () {
        // given
        var ctrl = buildController();
        setupLanguages(ctrl);

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.multiLanguageActive).toBe('true');
        expect(ctrl.enabled).toBe(true);
      });

      it('should disable directive when no default language active', function () {
        // given
        var ctrl = buildController();
        ctrl.default = null;
        ctrl.languages = {
          'EN': {
            'active': true
          },
          'DE': {
            'active': false
          }
        };

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.enabled).toBe(false);
      });

      it('should invalidate languages, on delete', function () {
        // given
        var ctrl = buildController(),
            toBeDeleted = 'DE';
        setupLanguages(ctrl);

        // when
        modalService.confirmDelete.and.returnValue(confirm(true));
        ctrl.deleteTranslation(event(), toBeDeleted);

        // then
        expect(modalService.confirmDelete).toHaveBeenCalled();
        expect(ctrl.current).toBe(ctrl.default);
        expect(ctrl.languages[toBeDeleted].valid).toBeUndefined();
        expect(Object.keys(ctrl.languages[toBeDeleted].translations).length).toBe(0);
      });

      it('should invalidate languages and trigger onDelete callback, on delete', function () {
        // given
        var ctrl = buildController(),
            toBeDeleted = 'DE';
        setupLanguages(ctrl);
        ctrl.onDelete = jasmine.createSpy('vm.onDelete');
        ctrl.onDelete.and.returnValue($q.resolve());

        // when
        modalService.confirmDelete.and.returnValue(confirm(true));
        ctrl.deleteTranslation(event(), toBeDeleted);
        $scope.$apply();

        // then
        expect(modalService.confirmDelete).toHaveBeenCalled();
        expect(ctrl.current).toBe(ctrl.default);
        expect(ctrl.languages[toBeDeleted].valid).toBeUndefined();
        expect(Object.keys(ctrl.languages[toBeDeleted].translations).length).toBe(0);
        expect(ctrl.onDelete).toHaveBeenCalledWith(toBeDeleted);
      });

      it('should apply translations, on confirm true', function () {
        // given
        var ctrl = buildController(),
            expected = 'DE';
        setupLanguages(ctrl);
        ctrl.languages.DE.translations = {};

        // when
        modalService.confirm.and.returnValue(confirm(true));
        ctrl.applyTranslations(expected);

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect(ctrl.languages.DE.translations.name).toBe(ctrl.languages.EN.translations.name);
      });

      it('should apply translations and trigger onChange callback, on confirm true', function () {
        // given
        var ctrl = buildController(),
            expected = 'DE',
            expectedResult = true;
        setupLanguages(ctrl);
        ctrl.languages.DE.translations = {};
        ctrl.onChange = jasmine.createSpy('vm.onChange');
        ctrl.onChange.and.returnValue($q.resolve(expectedResult));

        // when
        modalService.confirm.and.returnValue(confirm(true));
        ctrl.applyTranslations(expected);

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect(ctrl.languages[expected].translations.name).toBe(ctrl.languages.EN.translations.name);
        expect(ctrl.onChange).toHaveBeenCalledWith(expectedResult);
      });

      it('should not apply translations, on confirm false', function () {
        // given
        var ctrl = buildController(),
            expected = 'DE';
        setupLanguages(ctrl);
        ctrl.languages[expected].translations = {};

        // when
        modalService.confirm.and.returnValue(confirm(false));
        ctrl.applyTranslations(expected);

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect(Object.keys(ctrl.languages[expected].translations).length).toBe(0);
      });

      it('should not apply translations and trigger onChange callback, on confirm false', function () {
        // given
        var ctrl = buildController(),
            expected = 'DE',
            expectedResult = false;
        setupLanguages(ctrl);
        ctrl.languages[expected].translations = {};
        ctrl.onChange = jasmine.createSpy('vm.onChange');
        ctrl.onChange.and.returnValue($q.resolve(expectedResult));

        // when
        modalService.confirm.and.returnValue(confirm(false));
        ctrl.applyTranslations(expected);

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect(Object.keys(ctrl.languages[expected].translations).length).toBe(0);
        expect(ctrl.onChange).toHaveBeenCalledWith(expectedResult);
      });

      it('should not change current language of translation form, on cancel', function () {
        // given
        var ctrl = buildController(),
            expected = 'DE';
        setupLanguages(ctrl);
        ctrl.current = 'EN';
        ctrl.languages[expected].translations = {};

        // when
        modalService.confirm.and.returnValue(confirm(false, 'cancel'));
        ctrl.applyTranslations(expected);

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect(ctrl.current).toBe(ctrl.default);
      });

      it('should not change current language of translation form, on escape key press', function () {
        // given
        var ctrl = buildController(),
            expected = 'DE';
        setupLanguages(ctrl);
        ctrl.current = 'EN';
        ctrl.languages[expected].translations = {};

        // when
        modalService.confirm.and.returnValue(confirm(false, 'escape key press'));
        ctrl.applyTranslations(expected);

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect(ctrl.current).toBe(ctrl.default);
      });

    });
  });
})();
