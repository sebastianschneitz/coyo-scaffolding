(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'ImageUploadController';

  describe('module: ' + moduleName, function () {
    var $scope, $controller;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$controller_) {
      $scope = _$rootScope_.$new();
      $controller = _$controller_;
    }));

    describe('directive: ' + moduleName, function () {

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName);
        }

        it('should init with selecting false', function () {
          // given

          // when
          var ctrl = buildController();
          $scope.$apply();

          // then
          expect(ctrl.selecting).toBeFalsy();
        });

        it('should mark as selecting when images load', function () {
          // given
          var ctrl = buildController();
          $scope.$apply();

          // when
          ctrl.beforeChange();

          // then
          expect(ctrl.selecting).toBeTruthy();
        });

        it('should mark as not selecting when images finished loading', function () {
          // given
          var ctrl = buildController();
          $scope.$apply();

          // when
          ctrl.beforeChange();
          ctrl.onChange();

          // then
          expect(ctrl.selecting).toBeFalsy();
        });

        it('should mark as not selecting when images finished loading before start loading', function () {
          // given
          var ctrl = buildController();
          $scope.$apply();

          // when
          ctrl.onChange();
          ctrl.beforeChange();

          // then
          expect(ctrl.selecting).toBeFalsy();
        });
      });
    });
  });
})();
