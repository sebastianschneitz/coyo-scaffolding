(function () {
  'use strict';

  var moduleName = 'coyo.profile';
  var ctrlName = 'ProfileGroupController';

  describe('module: ' + moduleName, function () {

    beforeEach(module(moduleName, function ($provide) {
      $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
    }));

    var $q, $controller, _, moment, $scope;
    var user, group, setHeader, isEditable, onFormSubmit, onFormCancel, linkPattern, emailPattern;

    beforeEach(inject(function ($rootScope, _$q_, _$controller_, _moment_) {
      $scope = $rootScope.$new();
      $q = _$q_;
      $controller = _$controller_;
      moment = _moment_;
      setHeader = true;
      isEditable = false;
      /* eslint-disable no-useless-escape */
      linkPattern = /^((ftp|http|https):\/\/|(\/))[^ "]+$/i;
      emailPattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
      /* eslint-enable no-useless-escape */

      user = {
        id: 42,
        properties: {
          field3: 'true',
          field4: 'male',
          field5: '2016-02-10',
          field6: '2016-foo-10',
          field7: 'test@test.com',
          field8: 'wrongEmail.com',
          field9: 'http://correctlink.com',
          field10: 'http://incorrect link.com'
        }
      };

      group = {
        id: 1,
        name: 'group1',
        fields: [
          {id: 3, name: 'field3', type: 'CHECKBOX'},
          {id: 4, name: 'field4', type: 'OPTIONS', options: ['male', 'female', 'robot']},
          {id: 5, name: 'field5', type: 'DATE'},
          {id: 6, name: 'field6', type: 'DATE'},
          {id: 7, name: 'field7', type: 'EMAIL'},
          {id: 8, name: 'field8', type: 'EMAIL'},
          {id: 9, name: 'field9', type: 'LINK'},
          {id: 10, name: 'field10', type: 'LINK'}
        ],
        sortOrder: 0,
        editMode: false,
        saving: false
      };

      onFormSubmit = function () {};
      onFormCancel = function () {
        var deferred = $q.defer();
        deferred.resolve(user);
        return deferred.promise;
      };
    }));

    describe('directive: profile-group', function () {

      describe('controller: ' + ctrlName, function () {

        function buildController() {
          return $controller(ctrlName, {
            _: _,
            moment: moment
          }, {
            group: group,
            user: user,
            setHeader: setHeader,
            isEditable: isEditable,
            onFormSubmit: onFormSubmit,
            onFormCancel: onFormCancel,
            linkPattern: linkPattern
          });
        }

        describe('controller: init', function () {

          it('should prepare field values', function () {
            // given
            // nothing to see here

            // when
            var ctrl = buildController();

            // then
            expect(ctrl.user.properties.field3).toBeTrue();
            expect(ctrl.group.fields[1].options).toEqual(['male', 'female', 'robot']);
          });

        });

        describe('controller: functions', function () {

          it('should select the correct option on option select', function () {
            // given
            // nothing to see here

            // when
            var ctrl = buildController();
            ctrl.user.properties.field4 = 'choice1';
            ctrl.selectOption({name: 'field4'}, 'choice2');

            // then
            expect(ctrl.user.properties.field4).toBe('choice2');
          });

          it('should reset the fields on cancel', function () {
            // given
            // nothing to see here

            // when
            var ctrl = buildController();
            var form = jasmine.createSpyObj('form', ['$setPristine']);
            ctrl.form = form;
            ctrl.onCancel(group);
            $scope.$apply();

            // then
            expect(form.$setPristine).toHaveBeenCalled();
            expect(ctrl.group.editMode).toBeFalse();
            expect(ctrl.isEditable).toBeFalse();
          });

          it('should validate correct dates correctly', function () {
            // given
            // nothing to see here

            // when
            var ctrl = buildController();
            ctrl.validateDate(ctrl.group.fields[2]);

            // then
            expect(ctrl.group.fields[2].valid).toBeTrue();
            expect(ctrl.formValid).toBeTrue();
          });

          it('should validate incorrect dates correctly', function () {
            // given
            // nothing to see here

            // when
            var ctrl = buildController();
            ctrl.validateDate(ctrl.group.fields[3]);

            // then
            expect(ctrl.group.fields[3].valid).toBeFalse();
            expect(ctrl.formValid).toBeFalse();
          });

          it('should validate correct email addresses correctly', function () {
            // given
            // nothing to see here

            // when
            var ctrl = buildController();
            ctrl.validatePattern(ctrl.group.fields[4], emailPattern);

            // then
            expect(ctrl.group.fields[4].valid).toBeTrue();
            expect(ctrl.formValid).toBeTrue();
          });

          it('should validate incorrect email addresses correctly', function () {
            // given
            // nothing to see here

            // when
            var ctrl = buildController();
            ctrl.validatePattern(ctrl.group.fields[5], emailPattern);

            // then
            expect(ctrl.group.fields[5].valid).toBeFalse();
            expect(ctrl.formValid).toBeFalse();
          });

          it('should validate correct links correctly', function () {
            // given
            // nothing to see here

            // when
            var ctrl = buildController();
            ctrl.validatePattern(ctrl.group.fields[6], linkPattern);

            // then
            expect(ctrl.group.fields[6].valid).toBeTrue();
            expect(ctrl.formValid).toBeTrue();
          });

          it('should validate incorrect links correctly', function () {
            // given
            // nothing to see here

            // when
            var ctrl = buildController();
            ctrl.validatePattern(ctrl.group.fields[7], linkPattern);

            // then
            expect(ctrl.group.fields[7].valid).toBeFalse();
            expect(ctrl.formValid).toBeFalse();
          });
        });
      });
    });
  });

})();
