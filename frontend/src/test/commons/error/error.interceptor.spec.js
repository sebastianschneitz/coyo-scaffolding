(function () {
  'use strict';

  var moduleName = 'commons.error';

  describe('module: ' + moduleName, function () {
    var $httpBackend, $state, $http;
    var authServiceMock, errorServiceMock, authService, mockedUser;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        var backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);
        backendUrlService.getUrl.and.returnValue('SOME-URL');
        mockedUser = jasmine.createSpyObj('UserModel', ['hasGlobalPermissions']);
        authServiceMock = jasmine.createSpyObj('authService',
            ['getUser', 'isAuthenticated', 'getCurrentUserId', 'subscribeToUserUpdate']);
        authServiceMock.isAuthenticated.and.returnValue(true);
        authServiceMock.getCurrentUserId.and.returnValue('user-id');
        authServiceMock.getUser.and.returnValue({
          then: function (cb) {
            return cb(mockedUser);
          },
          catch: function (cb) {
            return cb();
          }
        });
        mockedUser.hasGlobalPermissions.and.returnValue(false);
        errorServiceMock = jasmine.createSpyObj('errorService', ['getMessage', 'suppressNotification', 'isNotificationSuppressed']);

        $provide.value('authService', authServiceMock);
        $provide.value('backendUrlService', backendUrlService);
        $provide.value('errorService', errorServiceMock);
      });

      inject(function (_$httpBackend_, _$http_, _$state_, _authService_) {
        $http = _$http_;
        $state = _$state_;
        $httpBackend = _$httpBackend_;
        authService = _authService_;
      });
    });

    describe('error response interceptor', function () {
      it('should redirect users to maintenance state', function () {
        // given
        var url = '/some/url';
        $httpBackend.expectGET(url).respond(503, {errorStatus: 'TENANT_MAINTENANCE'});
        spyOn($state, 'go');

        // when
        $http.get(url);
        $httpBackend.flush();

        // then
        expect(authService.getUser).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledTimes(1);
        expect($state.go).toHaveBeenCalledWith('front.maintenance', {'global': false}, {location: false});
        expect(errorServiceMock.suppressNotification).toHaveBeenCalled();
      });

      it('should not redirect maintenance admins to maintenance state', function () {
        // given
        var url = '/some/url';
        $httpBackend.expectGET(url).respond(503, {errorStatus: 'TENANT_MAINTENANCE'});
        spyOn($state, 'go');
        mockedUser.hasGlobalPermissions.and.returnValue(true);

        // when
        $http.get(url);
        $httpBackend.flush();

        // then
        expect(authService.getUser).toHaveBeenCalled();
        expect($state.go).not.toHaveBeenCalled();
        expect(errorServiceMock.suppressNotification).toHaveBeenCalled();
      });

      it('should redirect maintenance admins to maintenance state for global maintenance mode', function () {
        // given
        var url = '/some/url';
        $httpBackend.expectGET(url).respond(503, {errorStatus: 'GLOBAL_MAINTENANCE'});
        spyOn($state, 'go');
        mockedUser.hasGlobalPermissions.and.returnValue(false);

        // when
        $http.get(url);
        $httpBackend.flush();

        // then
        expect(authService.getUser).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('front.maintenance', {'global': true}, {location: false});
        expect(errorServiceMock.suppressNotification).toHaveBeenCalled();
      });

      it('should not redirect users to error state if response is 503 with html', function () {
        // given
        var url = '/some/url';
        $httpBackend.expectGET(url).respond(503, '<html>response</html>');
        spyOn($state, 'go');

        // when
        $http.get(url);
        $httpBackend.flush();

        // then
        expect($state.go).not.toHaveBeenCalled();
        expect(errorServiceMock.suppressNotification).not.toHaveBeenCalled();
      });
    });
  });
}());
