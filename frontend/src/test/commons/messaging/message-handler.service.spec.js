(function () {
  'use strict';

  var moduleName = 'coyo.messaging';
  var targetName = 'messageHandler';

  describe('module: ' + moduleName, function () {

    var messageHandler;
    var optimisticService, $q, optimisticStatus, $timeout, MessageModel, tempUploadService;
    var optimisticTypeMessage = 'OPTIMISTIC_TYPE_MESSAGE';
    var UPLOADS_TTL_SECONDS = 86400;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        var spies = jasmine.createSpyObj('MessageModel', ['create', 'markAsRead']);
        spies.create.and.returnValue({'catch': _.noop, then: _.noop});
        spies.markAsRead.and.returnValue({'catch': _.noop, then: _.noop});

        MessageModel = function () {
        };
        MessageModel.prototype.create = spies.create;
        MessageModel.create = spies.create;
        MessageModel.markAsRead = spies.markAsRead;

        $provide.value('MessageModel', MessageModel);
        optimisticStatus = {
          NEW: 'NEW',
          PENDING: 'PENDING',
          ERROR: 'ERROR'
        };
        $provide.value('optimisticStatus', optimisticStatus);
        optimisticService = jasmine.createSpyObj('optimisticService', ['find', 'findAll', 'remove']);
        $provide.value('optimisticService', optimisticService);
        tempUploadService = jasmine.createSpyObj('tempUploadService', ['upload']);
        $provide.value('tempUploadService', tempUploadService);
        $provide.value('optimisticTypeMessage', optimisticTypeMessage);
      });

      inject(function (_messageHandler_, _$q_, _$timeout_) {
        messageHandler = _messageHandler_;
        $q = _$q_;
        $timeout = _$timeout_;
      });
    });

    describe('service: ' + targetName, function () {

      it('should send message with text', function () {
        // given
        var message = {
          data: {
            message: 'some message text'
          }
        };

        // when
        messageHandler.sendMessage(message);

        // then
        expect(MessageModel.create).toHaveBeenCalledTimes(1);
      });

      it('should send messages with only attachment', function () {
        // given
        var message = {
          attachments: ['attachment'],
          data: {
            message: ''
          }
        };

        // when
        messageHandler.sendMessage(message);

        // then
        expect(MessageModel.create).toHaveBeenCalledTimes(1);
      });

      it('should send message with only file library attachment', function () {
        // given
        var message = {
          fileLibraryAttachments: ['attachment'],
          data: {
            message: ''
          }
        };

        // when
        messageHandler.sendMessage(message);

        // then
        expect(MessageModel.create).toHaveBeenCalledTimes(1);
      });

      it('should send message with file library attachment and text', function () {
        // given
        var message = {
          fileLibraryAttachments: ['attachment'],
          data: {
            message: 'Some message'
          }
        };

        // when
        messageHandler.sendMessage(message);

        // then
        expect(MessageModel.create).toHaveBeenCalledTimes(1);
      });

      it('should send messages with attachments and text', function () {
        // given
        var message = {
          attachments: ['attachment'],
          data: {
            message: 'Some message body.'
          }
        };

        // when
        messageHandler.sendMessage(message);

        // then
        expect(MessageModel.create).toHaveBeenCalledTimes(1);
      });

      it('should not send empty messages', function () {
        // given
        var message = {
          attachments: [],
          data: {
            message: ''
          }
        };

        // when
        messageHandler.sendMessage(message);

        // then
        expect(MessageModel.create).not.toHaveBeenCalled();
      });

      it('should not delete error before sending a message', function () {
        // given
        var message = {
          error: 'ERR',
          data: {
            message: 'Some failed message.'
          }
        };

        // when
        messageHandler.sendMessage(message);

        // then
        expect(MessageModel.create).toHaveBeenCalledTimes(1);
        expect(message.error).toBeUndefined();
      });

      it('should mark a message channel as read', function () {
        // given
        var userId = 'USER_ID';
        var channelId = 'CHANNEL_ID';

        // when
        messageHandler.markAsRead(userId, channelId);

        // then
        expect(MessageModel.markAsRead).toHaveBeenCalledTimes(1);
        expect(MessageModel.markAsRead).toHaveBeenCalledWith(userId, channelId);
      });

      it('should mark channel as read', function () {
        // given
        var userId = 'USER_ID';
        var channelId = 'CHANNEL_ID';

        // when
        messageHandler.markAsRead(userId, channelId);

        // then
        expect(MessageModel.markAsRead).toHaveBeenCalledTimes(1);
        expect(MessageModel.markAsRead).toHaveBeenCalledWith(userId, channelId);
      });

      it('should get all messages from local storage', function () {
        // given
        optimisticService.findAll.and.returnValue([
          {status: optimisticStatus.NEW},
          {status: optimisticStatus.PENDING},
          {status: optimisticStatus.ERROR}
        ]);

        // when
        var result = messageHandler.getAllMessagesFromStorage();

        // then
        expect(result.length).toEqual(3);
      });

      it('should get new messages from local storage', function () {
        // given
        optimisticService.findAll.and.returnValue([
          {status: optimisticStatus.NEW},
          {status: optimisticStatus.PENDING},
          {status: optimisticStatus.ERROR}
        ]);

        // when
        var result = messageHandler.getNewMessagesFromStorage();

        // then
        expect(result.length).toEqual(1);
        expect(result[0].status).toEqual(optimisticStatus.NEW);
      });

      it('should get pending messages from local storage', function () {
        // given
        optimisticService.findAll.and.returnValue([
          {status: optimisticStatus.NEW},
          {status: optimisticStatus.PENDING},
          {status: optimisticStatus.ERROR}
        ]);

        // when
        var result = messageHandler.getPendingMessagesFromStorage();

        // then
        expect(result.length).toEqual(1);
        expect(result[0].status).toEqual(optimisticStatus.PENDING);
      });

      it('should resend a pending message', function () {
        // given
        var pendingMessages = [
          {
            status: optimisticStatus.PENDING,
            entity: {
              data: {
                message: 'MESSAGE-1'
              }
            }
          }
        ];
        optimisticService.findAll.and.returnValue(pendingMessages);

        // when
        messageHandler.resendPendingMessages();

        // then
        $timeout.flush();
        expect(MessageModel.create).toHaveBeenCalledTimes(1);
      });

      it('should retrieve latest working copy for the current channel', function () {
        // given
        var channelId = 'CHANNEL_ID';
        var messages = [
          {
            entityId: 'SOME_OTHER_MESSAGE-1',
            status: optimisticStatus.NEW,
            entity: {
              channelId: 'SOME_OTHER_CHANNEL-1'
            }
          },
          {
            entityId: 'LATEST_CHANNEL_MESSAGE',
            status: optimisticStatus.NEW,
            entity: {
              channelId: channelId
            }
          },
          {
            entityId: 'SOME_OTHER_MESSAGE-2',
            status: optimisticStatus.NEW,
            entity: {
              channelId: 'SOME_OTHER_CHANNEL-2'
            }
          }
        ];
        optimisticService.findAll.and.returnValue(messages);

        // when
        var result = messageHandler.getLatestWorkingCopy(channelId);

        // then
        expect(result.entityId).toEqual(messages[1].entityId);
      });

      it('should remove messages from local storage', function () {
        // given
        var messageId = 'MESSAGE_ID';
        var message = {
          clientMessageId: messageId,
          data: {}
        };
        optimisticService.find.and.returnValue('something-was-found-here');

        // when
        messageHandler.removeLocalMessage(message);

        // then
        expect(optimisticService.remove).toHaveBeenCalledWith(messageId);
      });

      it('should cleanup messages from local storage that are no longer needed', function () {
        // given
        var messages = [
          {
            entityId: 'MESSAGE_1',
            attachments: [],
            entity: {
              data: {
                message: ''
              }
            }
          },
          {
            entityId: 'MESSAGE_2',
            entity: {}
          }
        ];
        optimisticService.findAll.and.returnValue(messages);

        // when
        messageHandler.cleanupLocalMessages();

        // then
        expect(optimisticService.remove).toHaveBeenCalledWith([messages[0].entityId, messages[1].entityId]);
      });

      it('should not cleanup messages that have content', function () {
        // given
        var messages = [
          {
            entityId: 'MESSAGE_1',
            entity: {
              attachments: ['attachment'],
              data: {
                message: ''
              }
            }
          },
          {
            entityId: 'MESSAGE_2',
            entity: {
              attachments: [],
              data: {
                message: 'message'
              }
            }
          },
          {
            entityId: 'MESSAGE_3',
            entity: {
              attachments: ['attachment'],
              data: {
                message: 'message'
              }
            }
          }
        ];
        optimisticService.findAll.and.returnValue(messages);

        // when
        messageHandler.cleanupLocalMessages();

        // then
        expect(optimisticService.remove).not.toHaveBeenCalled();
      });

      it('should upload files', function () {
        // given
        var messageId = 'MESSAGE_ID';
        var onFileUploadSuccess = _.noop;
        var files = [
          {name: 'file-1'},
          {name: 'file-2'},
          {name: 'file-3'}
        ];
        tempUploadService.upload.and.returnValue($q.when());

        // when
        messageHandler.uploadFiles(files, messageId, onFileUploadSuccess);

        // then
        expect(tempUploadService.upload).toHaveBeenCalledTimes(3);
        expect(tempUploadService.upload.calls.argsFor(0)).toEqual([files[0], UPLOADS_TTL_SECONDS]);
        expect(tempUploadService.upload.calls.argsFor(1)).toEqual([files[1], UPLOADS_TTL_SECONDS]);
        expect(tempUploadService.upload.calls.argsFor(2)).toEqual([files[2], UPLOADS_TTL_SECONDS]);
      });

    });
  });
})();
