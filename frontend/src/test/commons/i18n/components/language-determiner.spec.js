(function () {
  'use strict';

  var moduleName = 'commons.i18n.custom';

  describe('module: ' + moduleName, function () {
    var $scope, $q, LanguagesModel, I18nModel, languageDeterminer;

    beforeEach(function () {
      LanguagesModel = jasmine.createSpyObj('LanguagesModel', ['getDefaultLanguage']);
      I18nModel = jasmine.createSpyObj('I18nModel', ['getAvailableLanguages']);

      module(moduleName, function ($provide) {
        $provide.value('LanguagesModel', LanguagesModel);
        $provide.value('I18nModel', I18nModel);
      });
    });

    beforeEach(inject(function (_$rootScope_, _languageDeterminer_, _$localStorage_, _$q_) {
      $scope = _$rootScope_.$new();
      languageDeterminer = _languageDeterminer_;
      $q = _$q_;
    }));


    describe('service: languageDeterminer', function () {
      var fallbackLanguage = 'en';

      it('should initialize', function () {
        // when/then
        expect(languageDeterminer).toBeDefined();
      });

      it('should load fallback language when user language equals fallback language', function (result) {
        var param = {
          userLanguage: fallbackLanguage,
          defaultLanguage: 'DE',
          availableLanguages: ['en', 'de', 'nl'],
          expectedOutput: [{languageKey: fallbackLanguage, overrideOnly: false}],
          value: result
        };
        testForLanguageSettings(param);
      });

      it('should load fallback language and default language when user language equals default language',
          function (done) {
            var param = {
              userLanguage: 'de',
              defaultLanguage: 'DE',
              availableLanguages: ['en', 'de', 'nl'],
              expectedOutput: [{languageKey: fallbackLanguage, overrideOnly: false},
                {languageKey: 'de', overrideOnly: false}],
              value: done
            };
            testForLanguageSettings(param);
          });

      it('should load fallback language when user language equals default language and fallback language',
          function (done) {
            var param = {
              userLanguage: fallbackLanguage,
              defaultLanguage: fallbackLanguage,
              availableLanguages: ['en', 'de', 'nl'],
              expectedOutput: [{languageKey: fallbackLanguage, overrideOnly: false}],
              value: done
            };
            testForLanguageSettings(param);
          });

      it('should load fallback, default and user language when all are different', function (done) {
        var param = {
          userLanguage: 'nl',
          defaultLanguage: 'DE',
          availableLanguages: ['en', 'de', 'nl'],
          expectedOutput: [{languageKey: fallbackLanguage, overrideOnly: false},
            {languageKey: 'de', overrideOnly: false},
            {languageKey: 'nl', overrideOnly: false}],
          value: done
        };
        testForLanguageSettings(param);
      });

      it('should load fallback and user language when default language equals fallback language', function (done) {
        var param = {
          userLanguage: 'de',
          defaultLanguage: fallbackLanguage,
          availableLanguages: ['en', 'de', 'nl'],
          expectedOutput: [{languageKey: fallbackLanguage, overrideOnly: false},
            {languageKey: 'de', overrideOnly: false}],
          value: done
        };
        testForLanguageSettings(param);
      });

      it('should mark user language as overrideOnly if its not available', function (done) {
        var param = {
          userLanguage: 'nl',
          defaultLanguage: 'DE',
          availableLanguages: ['en', 'de'],
          expectedOutput: [{languageKey: fallbackLanguage, overrideOnly: false},
            {languageKey: 'de', overrideOnly: false},
            {languageKey: 'nl', overrideOnly: true}],
          value: done
        };
        testForLanguageSettings(param);
      });

      it('should mark default language as overrideOnly if its not available', function (done) {
        var param = {
          userLanguage: 'nl',
          defaultLanguage: 'DE',
          availableLanguages: ['en', 'nl'],
          expectedOutput: [{languageKey: fallbackLanguage, overrideOnly: false},
            {languageKey: 'de', overrideOnly: true},
            {languageKey: 'nl', overrideOnly: false}],
          value: done
        };
        testForLanguageSettings(param);
      });

      it('should mark default and user language as overrideOnly if its not available', function (done) {
        var param = {
          userLanguage: 'nl',
          defaultLanguage: 'DE',
          availableLanguages: ['en'],
          expectedOutput: [{languageKey: fallbackLanguage, overrideOnly: false},
            {languageKey: 'de', overrideOnly: true},
            {languageKey: 'nl', overrideOnly: true}],
          value: done
        };
        testForLanguageSettings(param);
      });

      function testForLanguageSettings(param) {
        // given
        LanguagesModel.getDefaultLanguage.and.returnValue($q.resolve({language: param.defaultLanguage}));
        I18nModel.getAvailableLanguages.and.returnValue($q.resolve({data: param.availableLanguages}));

        // when/then
        languageDeterminer.getLanguageKeys(param.userLanguage).then(function (languageKeys) {
          expect(languageKeys).toEqual(param.expectedOutput);
        }).finally(param.value);
        $scope.$apply();
      }

    });
  });
})();
