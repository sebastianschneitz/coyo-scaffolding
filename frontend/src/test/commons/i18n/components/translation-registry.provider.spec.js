(function () {
  'use strict';

  var moduleName = 'commons.i18n.custom';

  describe('module: ' + moduleName, function () {
    var translationRegistryProvider, translationRegistry;

    // load the main app
    beforeEach(module(moduleName));

    // load the provider with module to be able to call its configuration methods
    beforeEach(function () {
      module(['translationRegistryProvider', function (_translationRegistryProvider) {
        translationRegistryProvider = _translationRegistryProvider;
        translationRegistryProvider.registerTranslations('de', {
          'KEY1': 'test1',
          'KEY2': 'test2'
        });

        translationRegistryProvider.registerTranslations('de', {
          'KEY2': 'test2.1',
          'KEY3': 'test3'
        });

        translationRegistryProvider.registerTranslations('en', {
          'test1': 'abc'
        });
      }]);
    });

    beforeEach(inject(function (_translationRegistry_) {
      translationRegistry = _translationRegistry_;
    }));

    describe('service: translationRegistry', function () {
      it('should get all registered translations', function () {
        // when
        var translations = translationRegistry.getTranslationTables();

        // then
        expect(Object.keys(translations)).toBeArrayOfSize(2);
      });

      it('should get all registered translation', function () {
        var lang = 'de';

        // when
        var translation = translationRegistry.getTranslationTable(lang);

        expect(Object.keys(translation)).toBeArrayOfSize(3);
        expect(translation.KEY2).toBe('test2.1');
        expect(translation.test1).toBeUndefined();
      });
    });
  });

})();
