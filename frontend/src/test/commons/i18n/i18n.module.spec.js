(function () {
  'use strict';

  var moduleName = 'commons.i18n';

  describe('module: ' + moduleName, function () {

    var authService, $rootScope, currentUser, i18nService, bodyElementSpy, $document;
    bodyElementSpy = jasmine.createSpyObj('bodyElement', ['append']);

    var initModule = function () {

      module(moduleName, function ($provide) {
        authService =
          jasmine.createSpyObj('authService', ['getUser', 'isAuthenticated', 'logout', 'subscribeToUserUpdate']);
        authService.getUser.and.returnValue({
          then: function (cb) {
            return cb(currentUser);
          }
        });
        authService.isAuthenticated.and.returnValue(true);
        i18nService = jasmine.createSpyObj('i18nService', ['setInterfaceLanguage']);

        $document = jasmine.createSpyObj('$document', ['find']);
        $document.find.and.callFake(function (arg) {
          if (arg === 'body') {
            return bodyElementSpy;
          } else {
            return undefined;
          }
        });

        $provide.value('authService', authService);
        $provide.value('i18nService', i18nService);
        $provide.value('$document', $document);
      });

      inject(function (_$rootScope_) {
        $rootScope = _$rootScope_;
      });
    };

    describe('set language', function () {

      beforeEach(function () {
        bodyElementSpy.append.calls.reset();
        currentUser = jasmine.createSpyObj('UserModel', ['hasGlobalPermissions']);
        currentUser.language = 'ru';
        initModule();

      });

      it('should set language on init', function () {
        // when -> loading module

        // then
        expect(i18nService.setInterfaceLanguage).toHaveBeenCalledWith('ru');
      });

      it('should set language on login success', function () {
        // given
        currentUser.language = 'fr';

        // when
        $rootScope.$emit('authService:login:success');

        // then
        expect(i18nService.setInterfaceLanguage).toHaveBeenCalledWith('fr');
      });

      it('should load moment language file on init', function () {
        // when -> loading module

        // then
        expect(bodyElementSpy.append).toHaveBeenCalledWith('<script type="application/javascript" src="/moment-locale/ru.js"></script>');
      });

    });

    describe('load correct language file', function () {

      beforeEach(function () {
        bodyElementSpy.append.calls.reset();
        currentUser = jasmine.createSpyObj('UserModel', ['hasGlobalPermissions']);
        currentUser.language = 'ZH';
        initModule();
      });

      it('should load specific moment file for not fitting country code', function () {
        // when -> loading module

        // then
        expect(i18nService.setInterfaceLanguage).toHaveBeenCalledWith('ZH');
        expect(bodyElementSpy.append).toHaveBeenCalledWith('<script type="application/javascript" src="/moment-locale/zh-cn.js"></script>');
      });
    });
  });
})();
