(function () {
  'use strict';

  var moduleName = 'coyo.apps.commons.fields';

  describe('module: ' + moduleName, function () {

    var $controller, fieldTypeRegistry, fieldValueService, formCtrl, $scope;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _fieldTypeRegistry_, _fieldValueService_, $rootScope) {
      $controller = _$controller_;
      $scope = $rootScope.$new();
      fieldTypeRegistry = _fieldTypeRegistry_;
      fieldValueService = _fieldValueService_;
      formCtrl = jasmine.createSpyObj('formCtrl', ['getField', 'getFieldName']);
    }));

    var controllerName = 'EntryFormController';

    describe('controller: ' + controllerName, function () {

      function buildController(fields) {
        var controller = $controller(controllerName, {
          $scope: $scope,
          fieldTypeRegistry: fieldTypeRegistry,
          fieldValueService: fieldValueService
        }, {
          fields: fields,
          formCtrl: formCtrl,
          entry: {values: []}
        });
        controller.$onInit();

        $scope.$apply();

        return controller;
      }

      it('should initialize fields with empty value and return the proper field', function () {
        // given
        var ctrl = buildController([{
          id: 'field-test-1'
        }, {
          id: 'field-test-2'
        }]);

        // when
        var value = ctrl.getFieldValue('field-test-1');

        // then
        expect(value).toEqual({
          fieldId: 'field-test-1',
          value: undefined
        });
      });

    });
  });
})();
