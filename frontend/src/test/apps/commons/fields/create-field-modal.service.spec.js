(function () {
  'use strict';

  var moduleName = 'coyo.apps.commons.fields';

  describe('module: ' + moduleName, function () {

    var $controller, $uibModalInstance, app, $q, $scope;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_, $rootScope) {
      $controller = _$controller_;
      $q = _$q_;
      app = {
        id: 'app-id',
        senderId: 'sender-id',
        key: 'test-key'
      };
      $scope = $rootScope.$new();
      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
    }));

    var controllerName = 'CreateFieldModalController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {$uibModalInstance: $uibModalInstance, app: app});
      }

      it('should save field', function () {
        // given
        var ctrl = buildController();
        ctrl.field = {save: function () {
          var deferred = $q.defer();
          deferred.resolve(this);
          return deferred.promise;
        }};

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(ctrl.field.appId).toBe(app.id);
        expect(ctrl.field.appKey).toBe(app.key);
        expect(ctrl.field.senderId).toBe(app.senderId);
        expect($uibModalInstance.close).toHaveBeenCalledTimes(1);
      });

      it('should clear inputs when go back', function () {
        // given
        var ctrl = buildController();
        ctrl.field = {id: 'test-id'};
        ctrl.selectedFieldType = {id: 'field-type-test-id'};

        // when
        ctrl.goBack();

        // then
        expect(ctrl.field).toBeUndefined();
        expect(ctrl.selectedFieldType).toBeUndefined();
      });

    });
  });

})();
