(function () {
  'use strict';

  var moduleName = 'coyo.apps.commons.fields';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $q, fieldOne, fieldTwo, fields, app;
    var modalService, fieldTypeRegistry, createFieldModalService, editFieldModalService;

    beforeEach(module(moduleName));

    beforeEach(function () {
      app = {
        key: 'list',
        settings: {
          elementName: 'Test Data'
        }
      };

      fieldOne = {
        id: 'field-1',
        key: 'text',
        name: 'Text Field',
        icon: 'test-icon',
        settings: {required: true}
      };

      fieldTwo = {
        id: 'field-2',
        key: 'number',
        name: 'Number Field',
        icon: 'test-icon-two',
        settings: {min: 0},
        remove: angular.noop
      };

      fields = [fieldOne, fieldTwo];

      fieldTypeRegistry = jasmine.createSpyObj('fieldTypeRegistry', ['getAll']);
      createFieldModalService = jasmine.createSpyObj('createFieldModalService', ['open']);
      editFieldModalService = jasmine.createSpyObj('editFieldModalService', ['open']);
      modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);

      inject(function (_$controller_, $rootScope, _$q_) {
        $controller = _$controller_;
        $scope = $rootScope.$new();
        $q = _$q_;
      });
    });

    var controllerName = 'ConfigureFieldsController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        var controller = $controller(controllerName, {
          app: app,
          fields: fields,
          fieldTypeRegistry: fieldTypeRegistry,
          createFieldModalService: createFieldModalService,
          editFieldModalService: editFieldModalService,
          modalService: modalService
        });
        controller.$onInit();
        return controller;
      }

      it('should initialize', function () {
        // when
        var ctrl = buildController();

        // then
        expect(ctrl.fields).toEqual(fields);
        expect(ctrl.elementName).toBe(app.settings.elementName);
        expect(ctrl.treeOptions.dropped).toBeFunction();
        expect(fieldTypeRegistry.getAll).toHaveBeenCalled();
      });

      it('should add newly created field', function () {
        // given
        var ctrl = buildController();
        var newField = {
          key: 'test',
          name: 'Test Field',
          icon: 'new-test-icon',
          settings: {}
        };
        createFieldModalService.open.and.returnValue($q.resolve(newField));

        // when
        ctrl.createField();
        $scope.$apply();

        // then
        expect(fields).toContain(newField);
        expect(createFieldModalService.open).toHaveBeenCalledTimes(1);
      });

      it('should update and edited field', function () {
        // given
        var ctrl = buildController();
        var editedField = angular.copy(fields[0]);
        editedField.name = 'My edited Text Field';
        editFieldModalService.open.and.returnValue($q.resolve(editedField));

        // when
        ctrl.editField(fields[0]);
        $scope.$apply();

        // then
        expect(fields[0].name).toBe(editedField.name);
        expect(fields[0]).toEqual(editedField);
        expect(editFieldModalService.open).toHaveBeenCalledTimes(1);
      });

      it('should delete a field', function () {
        // given
        var ctrl = buildController();
        spyOn(fieldTwo, 'remove').and.returnValue($q.resolve());
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});

        // when
        ctrl.deleteField(fieldTwo);
        $scope.$apply();

        // then
        expect(fields).toBeArrayOfSize(1);
        expect(fields).not.toContain(fieldTwo);
      });

    });
  });

})();
