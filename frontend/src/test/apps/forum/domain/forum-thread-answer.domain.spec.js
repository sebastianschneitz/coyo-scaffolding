(function () {
  'use strict';

  describe('domain: ForumThreadAnswerModel', function () {

    beforeEach(module('commons.target'));
    beforeEach(module('coyo.apps.forum'));

    var $httpBackend, ForumThreadAnswerModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _ForumThreadAnswerModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      ForumThreadAnswerModel = _ForumThreadAnswerModel_;
      backendUrlService = _backendUrlService_;
      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('class members', function () {

      it('should count forum thread answers', function () {
        // given
        var app = {senderId: 'senderId', id: 'appId'};
        var thread = {id: 'threadId'};
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/threadId/answers/count')
            .respond(200, 2);

        // when
        var response = null;
        ForumThreadAnswerModel.count(app, thread).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response).toEqual(2);
      });

    });

    describe('instance members', function () {

      it('should load all thread answers', function () {
        // given
        var response = null;
        var threadAnswers = [
          {id: 'id1'},
          {id: 'id2'}
        ];
        var model = new ForumThreadAnswerModel({senderId: 'senderId', appId: 'appId', threadId: 'threadId'});
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/threadId/answers')
            .respond(200, threadAnswers);

        // when
        model.get().then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.length).toEqual(2);
      });

      it('should load a specific forum thread answer', function () {
        // given
        var answer = {
          text: 'text1'
        };
        var response = null;
        var model = new ForumThreadAnswerModel({senderId: 'senderId', appId: 'appId', threadId: 'threadId'});
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/threadId/answers/answerId')
            .respond(200, answer);

        // when
        model.get('answerId').then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.text).toEqual(answer.text);
      });

      it('should save a new forum thread answer', function () {
        // given
        var answer = {
          text: 'text1'
        };
        var response = null;
        var model = new ForumThreadAnswerModel({senderId: 'senderId', appId: 'appId', threadId: 'threadId'});
        $httpBackend
            .expectPOST(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/threadId/answers')
            .respond(200, answer);

        // when
        model.create(true).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.text).toEqual(answer.text);
      });

      it('should delete a forum thread', function () {
        // given
        var answer = {
          text: 'text1'
        };
        var response = null;
        var model = new ForumThreadAnswerModel({senderId: 'senderId', appId: 'appId', threadId: 'threadId', id: 'answerId'});
        $httpBackend
            .expectDELETE(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/threadId/answers/answerId')
            .respond(200, answer);

        // when
        model.delete().then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.text).toEqual(answer.text);
      });

    });
  });
}());
