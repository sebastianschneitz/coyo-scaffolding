(function () {
  'use strict';

  var moduleName = 'coyo.apps.forum';

  describe('module: ' + moduleName, function () {

    var $rootScope, $state, $q, $timeout, $controller;
    var tempUploadService, app, thread;

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$q_, _$controller_, _$timeout_) {
      $rootScope = _$rootScope_;
      $state = jasmine.createSpyObj('$state', ['go']);
      $q = _$q_;
      $timeout = _$timeout_;
      $controller = _$controller_;

      tempUploadService = jasmine.createSpyObj('tempUploadService', ['upload']);
      tempUploadService.upload.and.returnValue($q.resolve());

      app = jasmine.createSpyObj('app', ['']);
      thread = jasmine.createSpyObj('thread', ['isNew', 'create']);

      thread.id = 'ThreadId';
      thread.title = 'Title1';
      app.settings = {};
    }));

    var controllerName = 'ForumThreadCreateController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $rootScope.$new(),
          $state: $state,
          $q: $q,
          $timeout: $timeout,
          tempUploadService: tempUploadService,
          app: app,
          thread: thread
        });
      }

      it('should init correctly', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.newItemAttachments).toEqual([]);
      });

      it('should save a new forum thread', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        var defer = $q.defer();
        thread.isNew.and.returnValue(true);
        thread.create.and.returnValue(defer.promise);
        ctrl.editMode = true;
        ctrl.loading = false;

        ctrl.newTitle = 'Some-New-Title-1';

        // when
        ctrl.save();
        defer.resolve(thread);
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(thread.create).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('^.view', {id: thread.id});
      });

      it('should be able to cancel', function () {
        // given
        var ctrl = buildController();
        ctrl.editMode = true;
        ctrl.loading = false;
        ctrl.newTitle = 'Random-New-Title-1';

        // when
        ctrl.cancel();

        // then
        expect(ctrl.saving).toEqual(false);
        expect($state.go).toHaveBeenCalledWith('^');
      });

      it('should add attachments', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        var files = [{id: 'file-id-1'}, {id: 'file-id-2'}];

        // when
        ctrl.addAttachments(files);

        // then
        expect(tempUploadService.upload).toHaveBeenCalledTimes(2);
        expect(ctrl.newItemAttachments).toEqual(files);
      });

      it('should remove attachment', function () {
        // given
        var ctrl = buildController();
        var file1 = {id: 'file-id-1'};
        var file2 = {id: 'file-id-2'};
        ctrl.newItemAttachments = [file1, file2];

        // when
        ctrl.removeAttachment(file1);

        // then
        expect(ctrl.newItemAttachments).toEqual([file2]);
      });

    });
  });

})();
