(function () {
  'use strict';

  var moduleName = 'coyo.apps.forum';

  describe('module: ' + moduleName, function () {

    var $rootScope, $q, $controller, $timeout, $element, $compile;
    var app, thread, answer, ForumThreadAnswerModel, forumAppConfig, forumThreadService, backendUrlService;

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$q_, _$timeout_, _$compile_, _$controller_, _ForumThreadAnswerModel_) {
      $rootScope = _$rootScope_;
      $q = _$q_;
      $controller = _$controller_;
      $timeout = _$timeout_;
      $compile = _$compile_;

      $element = jasmine.createSpyObj('$element', ['find']);
      app = jasmine.createSpyObj('app', ['']);
      thread = jasmine.createSpyObj('thread', ['isNew', 'create']);
      answer = jasmine.createSpyObj('answer', ['isNew', 'createWithPermissions']);
      forumThreadService = jasmine.createSpyObj('forumThreadService', ['deleteAnswer']);

      backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);
      backendUrlService.getUrl.and.returnValue('http://localhost:8080');

      forumAppConfig = {
        paging: {
          threads: {
            pageSize: 10
          },
          threadAnswers: {
            pageSize: 10
          }
        },
        endpoints: {
          thread: {
            preview: '/attachments/{{id}}'
          },
          threadAnswer: {
            preview: '/attachments/{{id}}'
          }
        },
        templates: {
          contextMenu: 'app/apps/forum/forum-thread-list-context-menu.html'
        }
      };

      ForumThreadAnswerModel = _ForumThreadAnswerModel_;
      ForumThreadAnswerModel = jasmine.createSpyObj('ForumThreadAnswerModel', ['pagedQueryWithPermissions', 'count']);

      thread.id = 'ThreadId';
      answer.id = 'AnswerId';
      answer.typeName = 'forum-thread-answer';
      app.settings = {};
      app.id = 'appId';
      app.senderId = 'senderId';
    }));

    var controllerName = 'ForumThreadAnswersController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        var controller = $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $rootScope.$new(),
          $timeout: $timeout,
          $element: $element,
          $compile: $compile,
          $q: $q,
          ForumThreadAnswerModel: ForumThreadAnswerModel,
          forumAppConfig: forumAppConfig,
          forumThreadService: forumThreadService,
          backendUrlService: backendUrlService
        });
        return controller;
      }

      it('should init correctly', function () {
        // given
        var ctrl = buildController();
        ctrl.app = app;
        ctrl.thread = thread;
        var answerList = [{id: 1}, {id: 2}];
        ForumThreadAnswerModel.count.and.returnValue({
          then: function () {
            return {
              then: function () {
                return {
                  then: function (cb3) {
                    cb3({
                      count: answerList.length,
                      page: 0
                    });
                  }
                };
              }
            };
          }
        });
        ForumThreadAnswerModel.pagedQueryWithPermissions.and.returnValue($q.resolve(answerList));

        // when
        ctrl.$onInit();
        $rootScope.$apply();

        // then
        expect(ctrl.backendUrl).toEqual('http://localhost:8080');
        expect(backendUrlService.getUrl).toHaveBeenCalled();
        expect(ctrl.status.loadingAnswers).toEqual(false);
        expect(ForumThreadAnswerModel.pagedQueryWithPermissions).toHaveBeenCalled();
        expect(ctrl.forumThreadAnswers).toEqual(answerList);
      });

      it('should delete answer', function () {
        // given
        var ctrl = buildController();
        ctrl.app = app;
        ctrl.thread = thread;
        forumThreadService.deleteAnswer.and.returnValue($q.resolve());
        _.set(ctrl, 'forumThreadAnswers.content', [{id: 1}, {id: 2}]);

        // when
        ctrl.deleteAnswer({id: 1});
        $rootScope.$apply();

        // then
        expect(forumThreadService.deleteAnswer).toHaveBeenCalled();
      });

    });
  });

})();
