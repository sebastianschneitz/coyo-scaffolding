(function () {
  'use strict';

  var moduleName = 'coyo.apps.blog';
  var ctrlName = 'BlogSettingsController';

  describe('module: ' + moduleName, function () {

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    describe('controller: ' + ctrlName, function () {
      var $controller, $scope;

      beforeEach(inject(function ($rootScope, _$controller_) {
        $scope = $rootScope.$new();
        $controller = _$controller_;

        $scope.model = {
          settings: {}
        };

      }));

      function buildController() {
        return $controller(ctrlName, {
          $scope: $scope
        });
      }

      describe('controller init', function () {

        it('should initialize with default values for authorType, publisherType and commentsAllowed', function () {
          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.app.settings.authorType).toEqual('ADMIN');
          expect(ctrl.app.settings.publisherType).toEqual('ADMIN');
          expect(ctrl.app.settings.commentsAllowed).toBe(true);
          expect(ctrl.app.settings.folderPermissions.modifyRole).toBe('ADMIN');
        });

        it('should initialize authorType, publisherType and commentsAllowed if set', function () {
          // given
          $scope.model.settings.authorType = 'VIEWER';
          $scope.model.settings.publisherType = 'CUSTOM';
          $scope.model.settings.commentsAllowed = false;

          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.app.settings.authorType).toEqual('VIEWER');
          expect(ctrl.app.settings.publisherType).toEqual('CUSTOM');
          expect(ctrl.app.settings.commentsAllowed).toBe(false);
          expect(ctrl.app.settings.folderPermissions.modifyRole).toBe('VIEWER');
        });

        it('should initialize authorType with selected users', function () {
          // given
          $scope.model.settings.authorType = 'BLOGAPP_LIST_OF_USERS';
          $scope.model.settings.authorIds = ['a', 'b', 'c'];

          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.app.settings.authorType).toEqual('BLOGAPP_LIST_OF_USERS');
          expect(ctrl.app.settings.folderPermissions.modifyRole).toBeUndefined();
          expect(ctrl.app.settings.folderPermissions.users).toEqual(['a', 'b', 'c']);
        });

      });

    });
  });
})();
