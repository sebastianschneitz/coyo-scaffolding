(function () {
  'use strict';

  var moduleName = 'coyo.apps.blog';

  describe('module: ' + moduleName, function () {

    var $rootScope, $scope, $q, $httpBackend, $controller, $state, widgetLayoutService, app, article,
        sender, $timeout;

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$q_, _$httpBackend_, _$controller_, _$timeout_) {
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $q = _$q_;
      $httpBackend = _$httpBackend_;
      $controller = _$controller_;
      $state = jasmine.createSpyObj('$state', ['go']);
      $timeout = _$timeout_;
      app = jasmine.createSpyObj('app', ['']);
      article = jasmine.createSpyObj('article', ['isNew', 'save', 'buildLayoutName']);
      article.save.and.returnValue($q.resolve());
      widgetLayoutService = jasmine.createSpyObj('widgetLayoutService',
          ['cancel', 'save', 'edit', 'onload', 'collect', 'fill']);
      widgetLayoutService.save.and.returnValue($q.resolve());
      widgetLayoutService.edit.and.returnValue($q.resolve());
      widgetLayoutService.cancel.and.returnValue($q.resolve());
      widgetLayoutService.onload.and.returnValue($q.resolve());
      widgetLayoutService.collect.and.returnValue($q.resolve({layout: {}, slots: []}));
      widgetLayoutService.fill.and.returnValue($q.resolve());

      article.id = 'ArticleId';
      sender = {isSenderTranslated: function () {}};
      spyOn(sender, 'isSenderTranslated');
      app.settings = {
        publisherIds: []
      };
    }));

    var controllerName = 'BlogArticleEditController';

    describe('controller: ' + controllerName, function () {

      function buildController(canPublishArticle) {
        app._permissions = {
          publishArticle: canPublishArticle
        };
        return $controller(controllerName, {
          $state: $state,
          $scope: $scope,
          widgetLayoutService: widgetLayoutService,
          app: app,
          article: article,
          sender: sender
        });
      }

      it('should activate edit mode on init', function () {
        // when
        var ctrl = buildController(true);
        ctrl.$onInit();
        $rootScope.$apply();

        // then
        expect(ctrl.originalArticle).not.toBe(ctrl.article);
        expect(widgetLayoutService.onload).toHaveBeenCalled();
        expect(widgetLayoutService.edit).toHaveBeenCalled();
      });

      it('should save a new blog article', function () {
        // given
        var ctrl = buildController(true);
        var defer = $q.defer();
        ctrl.$onInit();
        article.isNew.and.returnValue(true);
        article.save.and.returnValue(defer.promise);

        // when
        ctrl.save();
        defer.resolve();
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(widgetLayoutService.save).toHaveBeenCalled();
        expect(article.save).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('^.view', {id: article.id});
      });

      it('should save an updated blog article', function () {
        // given
        var ctrl = buildController(true);
        var defer = $q.defer();
        ctrl.$onInit();
        article.isNew.and.returnValue(false);
        article.save.and.returnValue(defer.promise);

        // when
        ctrl.save();
        defer.resolve();
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(widgetLayoutService.save).toHaveBeenCalled();
        expect(article.save).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('^.view', {id: article.id});
      });

      it('should flatten teaserImage/Wide translations before save or update blog article', function () {
        // given
        var ctrl = buildController(false);
        var defer = $q.defer();
        sender.isSenderTranslated.and.returnValue(true);
        ctrl.sender.defaultLanguage = 'EN';
        ctrl.sender.translations = {
          'DE': {}
        };
        ctrl.$onInit();
        article.save.and.returnValue(defer.promise);

        // when
        ctrl.languages.DE.translations.teaserImage = {'fileId': 'file-uuid-de', 'senderId': 'sender-uuid-de'};
        ctrl.languages.DE.translations.teaserImageWide = {'testAttribute': 'testValue'};

        ctrl.save();

        // then
        expect(Object.getOwnPropertyNames(ctrl.article.translations.DE).toString()).toBe(['teaserImageFileId',
          'teaserImageSenderId'].toString());
        expect(ctrl.article.translations.DE.teaserImageFileId).toBe('file-uuid-de');
        expect(ctrl.article.translations.DE.teaserImageSenderId).toBe('sender-uuid-de');
        expect(ctrl.article.translations.DE.teaserImageWideFileId).toBe(undefined);
        expect(ctrl.article.translations.DE.teaserImageWideSenderId).toBe(undefined);
        expect(ctrl.article.translations.DE.teaserImage).toBe(undefined);
        expect(ctrl.article.translations.DE.teaserImageWide).toBe(undefined);
      });

      it('should be able to cancel', function () {
        // given
        var ctrl = buildController(true);

        // when
        ctrl.cancel();

        // then
        expect(widgetLayoutService.cancel).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('^.view', {id: article.id});
      });

      it('should init publish status as PUBLISHED_AT if article is published', function () {
        // given
        article.isNew.and.returnValue(false);
        article.publishDate = 946684800;

        // when
        var ctrl = buildController(true);

        // then
        expect(ctrl.publishStatus).toBe('PUBLISHED_AT');
        expect(ctrl.article.publishDate).toEqual(new Date(946684800));
      });

      it('should init publish status as PUBLISHED if article is new and user can publish', function () {
        // given
        article.isNew.and.returnValue(true);

        // when
        var ctrl = buildController(true);

        // then
        expect(ctrl.publishStatus).toBe('PUBLISHED');
      });

      it('should init publish status as DRAFT if article is a draft and user can publish', function () {
        // given
        article.isNew.and.returnValue(false);

        // when
        var ctrl = buildController(true);

        // then
        expect(ctrl.publishStatus).toBe('DRAFT');
      });

      it('should init publish status as DRAFT if article is a draft and user can not publish', function () {
        // given
        article.isNew.and.returnValue(false);

        // when
        var ctrl = buildController(false);

        // then
        expect(ctrl.publishStatus).toBe('DRAFT');
      });

      it('should init publish status as DRAFT if article is new and user can not publish', function () {
        // given
        article.isNew.and.returnValue(true);

        // when
        var ctrl = buildController(false);

        // then
        expect(ctrl.publishStatus).toBe('DRAFT');
      });

      it('should collect layout + widgets and discard them, on language delete', function () {
        // given
        var ctrl = buildController(),
            current = 'EN';

        // when
        ctrl.onLanguageDeleted(current);
        $scope.$apply();

        // then
        expect(widgetLayoutService.collect).toHaveBeenCalled();
      });

      it('should delete collected layout + widgets, on save', function () {
        // given
        var ctrl = buildController(),
            current = 'EN';
        ctrl.defaultLanguage = 'DE';
        ctrl.languages[ctrl.defaultLanguage] = {
          translations: {}
        };
        $httpBackend.expectDELETE('/web/widgets/slotName/modelId').respond(200);
        $httpBackend.expectDELETE('/web/widget-layouts/layoutName').respond(200);
        widgetLayoutService.collect.and.returnValue($q.resolve({
          layout: {name: 'layoutName', rows: [{slots: [{name: 'slotName'}]}]},
          slots: [{
            model: {
              _id: 'modelId', slot: 'slotName', isNew: function () {
                return false;
              }
            }
          }]
        }));

        // when
        ctrl.onLanguageDeleted(current);
        $scope.$apply();
        ctrl.save();
        $timeout.flush();

        // then
        expect(widgetLayoutService.collect).toHaveBeenCalled();
      });

      it('should not collect layout + widgets if not copyFromDefault, on language change', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.onLanguageChange(false);
        $timeout.flush();

        // then
        expect(widgetLayoutService.edit).toHaveBeenCalled();
        expect(widgetLayoutService.collect).not.toHaveBeenCalled();
      });

      it('should collect/fill layout + widgets and activate edit if copyFromDefault, on language change', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.onLanguageChange(true);
        $scope.$apply();

        // then
        expect(widgetLayoutService.collect).toHaveBeenCalled();
        expect(widgetLayoutService.fill).toHaveBeenCalledTimes(2);
        expect(widgetLayoutService.edit).toHaveBeenCalled();
      });

      it('should initialise articles defaultLanguage if not set', function () {
        // given
        var ctrl = buildController(),
            expected = 'EN';
        sender.defaultLanguage = expected;
        sender.translations = {'EN': {}};
        article.defaultLanguage = undefined;

        // when
        ctrl.$onInit();

        // then
        expect(article.defaultLanguage).toBe(expected);
      });

      it('should init translations for blog-article with translated sender and no translation', function () {
        // given
        article.isNew.and.returnValue(true);
        sender.defaultLanguage = 'EN';
        sender.translations = {'EN': {}};
        sender.isSenderTranslated.and.returnValue(true);

        var ctrl = buildController(false);

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.isSenderTranslated).toBe(true);
        expect(ctrl.currentLanguage).toBe(ctrl.defaultLanguage);
        expect(ctrl.languages[ctrl.defaultLanguage].active).toBe(true);
        expect(ctrl.languages[ctrl.defaultLanguage].translations.title).toBe('');
        expect(ctrl.languages[ctrl.defaultLanguage].translations.teaserText).toBe('');
        expect(ctrl.languages[ctrl.defaultLanguage].translations.teaserImage).toBe('');
        expect(ctrl.languages[ctrl.defaultLanguage].translations.teaserImageWide).toBe('');
      });

      it('should init translations for blog-article with existing article translation', function () {
        // given
        article.isNew.and.returnValue(true);
        sender.defaultLanguage = 'DE';
        sender.translations = {'DE': {}};
        sender.isSenderTranslated.and.returnValue(true);
        article.translations = {
          'DE': {
            'title': 'Titel',
            'teaserText': 'Teaser Text',
            'teaserImageFileId': 'imageFileId',
            'teaserImageSenderId': 'imageSenderId',
            'teaserImageWideFileId': 'imageWideFileId',
            'teaserImageWideSenderId': 'imageWideSenderId'
          }
        };

        var ctrl = buildController(false);

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.isSenderTranslated).toBe(true);
        expect(ctrl.currentLanguage).toBe(ctrl.defaultLanguage);
        expect(ctrl.languages[ctrl.defaultLanguage].active).toBe(true);
        expect(ctrl.languages[ctrl.defaultLanguage].translations.title).toBe('Titel');
        expect(ctrl.languages[ctrl.defaultLanguage].translations.teaserText).toBe('Teaser Text');
        expect(angular.toJson(ctrl.languages[ctrl.defaultLanguage].translations.teaserImage))
            .toBe(angular.toJson({'fileId': 'imageFileId', 'senderId': 'imageSenderId'}));
        expect(angular.toJson(ctrl.languages[ctrl.defaultLanguage].translations.teaserImageWide))
            .toBe(angular.toJson({'fileId': 'imageWideFileId', 'senderId': 'imageWideSenderId'}));
      });
    });
  });

})();
