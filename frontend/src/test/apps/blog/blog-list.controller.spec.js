(function () {
  'use strict';

  var moduleName = 'coyo.apps.blog';
  var controllerName = 'BlogListController';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $rootScope, $scope, $state, moment, modalService, BlogArticleModel, page, app;

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$q_, _$rootScope_, _moment_, _$controller_) {
      $q = _$q_;
      $rootScope = _$rootScope_;
      $scope = _$rootScope_.$new();
      moment = _moment_;
      $controller = _$controller_;

      BlogArticleModel = jasmine.createSpyObj('BlogArticleModel', ['pagedQueryWithPermissions', 'count']);
      page = jasmine.createSpyObj('Page', ['page']);
      app = jasmine.createSpyObj('app', ['']);
      modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
      $state = jasmine.createSpyObj('$state', ['go']);
      $state.current = {};

      page._queryParams = {};

      BlogArticleModel.pagedQueryWithPermissions.and.returnValue($q.resolve(page));
      BlogArticleModel.count.and.returnValue($q.resolve({}));
    }));

    function buildController() {
      var ctrl = $controller(controllerName, {
        $scope: $scope,
        $state: $state,
        moment: moment,
        modalService: modalService,
        BlogArticleModel: BlogArticleModel,
        app: app,
        currentUser: {}
      });
      ctrl.$onInit();
      return ctrl;
    }

    describe('controller init: ' + controllerName, function () {

      it('should initialize the controller correctly', function () {
        // given
        BlogArticleModel.pagedQueryWithPermissions.and.returnValue($q.resolve({foo: 'bar'}));

        // when
        var ctrl = buildController();
        $rootScope.$apply();

        // then
        expect(ctrl.blogArticles.foo).toBe('bar');
        expect(ctrl.blogArticles.loading).toBe(false);
      });

    });

    describe('controller active: ' + controllerName, function () {

      var ctrl;

      beforeEach(function () {
        ctrl = buildController();
        $rootScope.$apply();
      });

      it('should check that first list item is not a new month', function () {
        // given

        // when
        var result = ctrl.isNewMonth(0);

        // then
        expect(result).toBe(false);
      });

      it('should check that the month is the same', function () {
        // given
        ctrl.blogArticles = {
          content: [
            {publishDate: new Date('2016-03-18')},
            {publishDate: new Date('2016-03-20')}
          ]
        };

        // when
        var result = ctrl.isNewMonth(1);

        // then
        expect(result).toBe(false);
      });

      it('should check that the month is a different one', function () {
        // given
        ctrl.blogArticles = {
          content: [
            {publishDate: new Date('2016-03-18')},
            {publishDate: new Date('2016-04-20')}
          ]
        };

        // when
        var result = ctrl.isNewMonth(1);

        // then
        expect(result).toBe(true);
      });

      describe('filterActive', function () {

        it('should be false if all options set', function () {
          // given
          ctrl.blogArticles = {
            _queryParams: {
              includePublished: true,
              includeScheduled: true,
              includeDrafts: true
            }
          };

          // when
          var result = ctrl.filterActive();

          // then
          expect(result).toBe(false);
        });

        it('should be true if includePublished option unset', function () {
          // given
          ctrl.blogArticles = {
            _queryParams: {
              includePublished: false,
              includeScheduled: true,
              includeDrafts: true
            }
          };

          // when
          var result = ctrl.filterActive();

          // then
          expect(result).toBe(true);
        });

        it('should be true if includeScheduled option unset', function () {
          // given
          ctrl.blogArticles = {
            _queryParams: {
              includePublished: true,
              includeScheduled: false,
              includeDrafts: true
            }
          };

          // when
          var result = ctrl.filterActive();

          // then
          expect(result).toBe(true);
        });

        it('should be true if includeDrafts option unset', function () {
          // given
          ctrl.blogArticles = {
            _queryParams: {
              includePublished: true,
              includeScheduled: true,
              includeDrafts: false
            }
          };

          // when
          var result = ctrl.filterActive();

          // then
          expect(result).toBe(true);
        });

        it('should be true if limitDate option is set', function () {
          // given
          ctrl.blogArticles = {
            _queryParams: {
              includePublished: true,
              includeScheduled: true,
              includeDrafts: true,
              limitDate: '2016-01'
            }
          };

          // when
          var result = ctrl.filterActive();

          // then
          expect(result).toBe(true);
        });

      });

      it('should toggle include published', function () {
        // given
        ctrl.blogArticles._queryParams.includePublished = true;

        // when
        ctrl.toggleIncludePublished();

        // then
        expect(ctrl.blogArticles._queryParams.includePublished).toBe(false);
        expect(page.page).toHaveBeenCalledWith(0);
      });

      it('should toggle include scheduled', function () {
        // given
        ctrl.blogArticles._queryParams.includeScheduled = true;

        // when
        ctrl.toggleIncludeScheduled();

        // then
        expect(ctrl.blogArticles._queryParams.includeScheduled).toBe(false);
        expect(page.page).toHaveBeenCalledWith(0);
      });

      it('should toggle include drafts', function () {
        // given
        ctrl.blogArticles._queryParams.includeDrafts = true;

        // when
        ctrl.toggleIncludeDrafts();

        // then
        expect(ctrl.blogArticles._queryParams.includeDrafts).toBe(false);
        expect(page.page).toHaveBeenCalledWith(0);
      });

      it('should set limit date', function () {
        // when
        ctrl.filterMonth = null;
        ctrl.toggleFilterMonth('2016-01');

        // then
        expect(ctrl.blogArticles._queryParams.limitDate).toBe('2016-01');
        expect(page.page).toHaveBeenCalledWith(0);
      });

      it('should unset limit date', function () {
        // given
        ctrl.filterMonth = '2016-01';
        ctrl.blogArticles._queryParams.limitDate = '2016-01';

        // when
        ctrl.toggleFilterMonth('2016-01');

        // then
        expect(ctrl.blogArticles._queryParams.limitDate).toBeUndefined();
        expect(page.page).toHaveBeenCalledWith(0);
      });

      it('should delete an article', function () {
        // given
        modalService.confirmDelete.and.returnValue({
          result: $q.resolve()
        });
        var article = jasmine.createSpyObj('article', ['delete']);
        article.shares = [];
        article.delete.and.returnValue(
            $q.resolve()
        );
        $state.current.name = 'current-state';

        // when
        ctrl.deleteArticle(article);
        $rootScope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith('current-state', null, {reload: 'current-state'});
      });

      it('should show a warning if article has shares', function () {
        // given
        var article = jasmine.createSpyObj('article', ['delete']);
        article.shares = [{}, {}];
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        article.delete.and.returnValue($q.resolve());

        // when
        ctrl.deleteArticle(article);
        $rootScope.$apply();

        // then
        var modalOptions = modalService.confirmDelete.calls.mostRecent().args[0];
        expect(modalOptions.alerts).toEqual([{
          level: 'danger',
          title: 'APP.BLOG.ARTICLE.DELETE.WARNING.TITLE',
          text: 'APP.BLOG.ARTICLE.DELETE.MULTIPLE.SHARE.TEXT'
        }]);
        expect(modalOptions.translationContext).toEqual({
          shareCount: article.shares.length,
          title: article.title
        });
        expect(article.delete).toHaveBeenCalled();
      });
    });
  });

})();
