(function () {
  'use strict';

  var moduleName = 'coyo.apps.list';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $q, $state, $injector, app, fields, ListEntryModel, Pageable,
        createFieldModalService, addEntryModalService;

    beforeEach(module(moduleName));

    beforeEach(function () {

      $injector = jasmine.createSpyObj('$injector', ['get']);
      $injector.get.and.returnValue(jasmine.createSpyObj('ngxNotificationService', ['success', 'error']));
      createFieldModalService = jasmine.createSpyObj('createFieldModalService', ['open']);
      addEntryModalService = jasmine.createSpyObj('addEntryModalService', ['open']);

      app = {
        key: 'list',
        settings: {
          elementName: 'Test Data'
        },
        _permissions: {
          readEntries: true,
          createEntry: true
        },
        subscriptionInfo: {
          token: 'wsToken'
        }
      };

      fields = [];

      inject(function (_$controller_, $rootScope, _$q_, _ListEntryModel_, _Pageable_) {
        $controller = _$controller_;
        $scope = $rootScope.$new();
        $q = _$q_;
        ListEntryModel = _ListEntryModel_;
        Pageable = _Pageable_;
      });
    });

    var controllerName = 'ListController';

    describe('controller: ' + controllerName, function () {

      function buildController(overrides) {
        var controller = $controller(controllerName, angular.extend({
          app: app,
          $scope: $scope,
          $state: $state,
          $injector: $injector,
          fields: fields,
          createFieldModalService: createFieldModalService,
          addEntryModalService: addEntryModalService,
          ListEntryModel: ListEntryModel,
          Pageable: Pageable
        }, overrides));
        controller.$onInit();
        return controller;
      }

      it('should navigate to configure state when clicking create field', function () {
        // given
        var $state = jasmine.createSpyObj('$state', ['go']);
        var ctrl = buildController({
          $state: $state
        });

        createFieldModalService.open.and.returnValue($q.resolve({}));

        // when
        ctrl.createField();
        $scope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith('^.configure');
        expect(createFieldModalService.open).toHaveBeenCalledTimes(1);
      });

      it('should reload data after adding a new entry', function () {
        // given
        var entry = jasmine.createSpyObj('$state', ['go']);
        var ctrl = buildController();
        ctrl.entry = entry;

        addEntryModalService.open.and.returnValue($q.resolve({}));

        // when
        ctrl.addEntry();
        $scope.$apply();

        // then
        expect(addEntryModalService.open).toHaveBeenCalledTimes(1);
      });

      it('should find the list value', function () {
        // given
        var valueOne = {
          fieldId: 'field-1',
          value: '123'
        };
        var entry = {
          values: [valueOne]
        };

        var fieldOne = {
          fieldId: 'field-1',
          key: 'text'
        };
        var fieldTwo = {
          fieldId: 'field-2',
          key: 'text'
        };

        fields = [fieldOne, fieldTwo];
        var ctrl = buildController({});

        // when
        var foundValue = ctrl.getValue(entry, 'field-1');
        var noValue = ctrl.getValue(entry, 'field-3');

        // then
        expect(foundValue).toBe(valueOne);
        expect(noValue).toBe(undefined);

      });

      it('should only show the latest queried data', function () {
        // given
        var ListEntryModel = jasmine.createSpyObj('ListEntryModel', ['pagedQuery']);
        var listService = jasmine.createSpyObj('listService', ['initEntries']);
        var ctrl = buildController({
          ListEntryModel: ListEntryModel,
          listService: listService
        });

        var deferred = $q.defer();

        ListEntryModel.pagedQuery.and.callFake(function (pageable, queryParams) {
          if (queryParams.query === 'tes') {
            return deferred.promise.then(function () {
              $q.resolve({content: ['wrong']});
            });
          } else {
            deferred.resolve({content: ['correct']});
            return deferred.promise;
          }
        });

        // when
        ctrl.search = 'tes';
        ctrl.searchKeyPressed();
        ctrl.search = 'test';
        ctrl.searchKeyPressed();
        $scope.$apply();

        // then
        expect(listService.initEntries).toHaveBeenCalledTimes(1);
        expect(listService.initEntries).toHaveBeenCalledWith(['correct'], []);
      });

    });
  });

})();
