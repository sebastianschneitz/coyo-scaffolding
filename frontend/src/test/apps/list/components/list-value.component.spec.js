(function () {
  'use strict';

  var moduleName = 'coyo.apps.list';
  var componentName = 'coyo-list-value';

  describe('module: ' + moduleName, function () {

    var $scope, $compile, template, fields;

    beforeEach(module(moduleName));
    beforeEach(module('commons.templates'));

    describe('component: ' + componentName, function () {

      fields = [{
        id: 'field-test-1',
        key: 'checkbox'
      }, {
        id: 'field-test-2',
        key: 'text'
      }];

      beforeEach(inject(function ($rootScope, _$compile_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        template = '<coyo-list-value app="app" entry="entry" field="field"></coyo-list-value>';
      }));

      it('should execute all bindings correctly', function () {
        // given
        var app = {},
            entry = {};
        $scope.app = app;
        $scope.entry = entry;
        $scope.field = fields[0];

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        var controller = element.controller('coyoListValue');
        expect(controller.app).toBe(app);
        expect(controller.entry).toBe(entry);
        expect(controller.field).toBe(fields[0]);
      });

      it('should activate inline-edit directive with edit permissions', function () {
        // given
        var entry = {
          _permissions: {
            edit: true
          }
        };
        $scope.entry = entry;
        $scope.field = fields[0];

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.find('coyo-render-value').length).toBe(0);
        expect(element.find('coyo-list-inline-edit').length).toBe(1);
      });

      it('should activate render directive without edit permissions', function () {
        // given
        var entry = {
          _permissions: {
            edit: false
          }
        };
        $scope.entry = entry;

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.find('coyo-render-value').length).toBe(1);
        expect(element.find('coyo-list-inline-edit').length).toBe(0);
      });

      it('should activate render directive for non editable field', function () {
        // given
        var entry = {
          _permissions: {
            edit: true
          }
        };
        $scope.entry = entry;
        $scope.field = fields[1];

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.find('coyo-render-value').length).toBe(1);
        expect(element.find('coyo-list-inline-edit').length).toBe(0);
      });
    });
  });

})();
