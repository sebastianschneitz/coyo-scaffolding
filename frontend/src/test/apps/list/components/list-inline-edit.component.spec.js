(function () {
  'use strict';

  var moduleName = 'coyo.apps.list';
  var componentName = 'coyo-list-inline-edit';

  describe('module: ' + moduleName, function () {

    var $scope, $compile, $componentController, template, fields, ListEntryModel;

    describe('component: ' + componentName, function () {

      fields = [{
        id: 'field-test-1',
        key: 'checkbox'
      }, {
        id: 'field-test-2',
        key: 'text'
      }];

      beforeEach(module('commons.templates'));

      beforeEach(module(moduleName, function ($provide) {
        ListEntryModel = jasmine.createSpyObj('ListEntryModel', ['updateValue']);
        $provide.value('ListEntryModel', ListEntryModel);
      }));

      beforeEach(inject(function ($rootScope, _$compile_, _$componentController_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        template = '<coyo-list-inline-edit app="app" entry="entry" field="field"></coyo-list-inline-edit>';
        $componentController = _$componentController_;
      }));

      it('should execute all bindings correctly', function () {
        // given
        var app = {},
            entry = {};
        $scope.app = app;
        $scope.entry = entry;
        $scope.field = fields[0];

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        var controller = element.controller('coyoListInlineEdit');
        expect(controller.app).toBe(app);
        expect(controller.entry).toBe(entry);
        expect(controller.field).toBe(fields[0]);
      });

      it('should activate checkbox inline-edit directive', function () {
        // given
        var entry = {};
        $scope.entry = entry;
        $scope.field = fields[0];

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.find('coyo-list-inline-edit-checkbox').length).toBe(1);
      });

      it('should save entry model', function () {
        // given
        var app, entry, context;
        app = {
          id: 'test-app-id',
          senderId: 'test-sender-id'
        };
        entry = {
          id: 'test-entry-id',
          values: [
            {
              fieldId: fields[0].id,
              value: 'Test'
            },
            {
              fieldId: fields[1].id,
              value: ''
            }
          ]
        };
        context = {
          appId: app.id,
          senderId: app.senderId,
          id: entry.id
        };
        ListEntryModel.updateValue.and.returnValue({
          then: function () {
            return {
              catch: function () {}
            };
          }
        });
        var controller = $componentController('coyoListInlineEdit', {}, {app: app, entry: entry, field: fields[0]});
        controller.$onInit();

        // when
        controller.save();

        // then
        expect(ListEntryModel.updateValue).toHaveBeenCalledWith(context, entry.values[0]);
      });
    });
  });

})();
