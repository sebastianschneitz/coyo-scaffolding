(function () {
  'use strict';

  var moduleName = 'coyo.apps.commons.fields';

  describe('module: ' + moduleName, function () {

    var fieldTypeRegistryProvider, fieldTypeRegistry;

    var fieldTypeOne, fieldTypeTwo;

    fieldTypeOne = {
      key: 'test',
      title: 'Checkbox',
      description: 'A nice checkbox',
      icon: 'zmdi-check-square',
      render: {
        templateUrl: 'checkbox.html'
      }
    };

    fieldTypeTwo = {
      key: 'test2',
      title: 'Text',
      description: 'A nice textfield',
      icon: 'zmdi-check-text',
      render: {
        templateUrl: 'test2.html'
      }
    };

    beforeEach(function () {

      // initialize the service provider by injecting it to a fake module's config block
      angular.module('module.test', ['coyo.apps.commons.fields']);
      angular.module('module.test').config(function (_fieldTypeRegistryProvider_) {
        fieldTypeRegistryProvider = _fieldTypeRegistryProvider_;

        // Call methods on the provider in config phase here
        fieldTypeRegistryProvider.register(fieldTypeOne);
        fieldTypeRegistryProvider.register(fieldTypeTwo);
      });

      // initialize test.app injector
      module('module.test');
    });

    // inject the actual service
    beforeEach(inject(function (_fieldTypeRegistry_) {
      fieldTypeRegistry = _fieldTypeRegistry_;
    }));

    describe('Service: fieldTypeRegistry', function () {

      it('should get all fields', function () {
        // when
        var fieldTypes = fieldTypeRegistry.getAll();

        // then
        expect(fieldTypes).toContain(fieldTypeOne);
        expect(fieldTypes).toContain(fieldTypeTwo);
      });

      it('should get a field', function () {
        // when
        var field = fieldTypeRegistry.get('test');

        // then
        expect(field).toEqual(fieldTypeOne);
      });

    });

  });

  describe('Module: ' + moduleName, function () {

    describe('Provider: fieldTypeRegistryProvider', function () {

      // Create a fake module with the provider. You can use this method in any test
      function createModule(fieldType) {
        angular.module('module.test', ['coyo.apps.commons.fields']);
        angular.module('module.test').config(function (_fieldTypeRegistryProvider_) {
          _fieldTypeRegistryProvider_.register(fieldType);
        });

        module('module.test');
        inject(function () {});
      }

      it('should register valid field type', function () {
        // given
        var fieldType = {
          key: 'text',
          title: 'Text',
          description: 'A nice textfield',
          icon: 'zmdi-check-text',
          render: {
            templateUrl: 'text.html'
          }
        };

        // when
        createModule(fieldType);

        // then -> perform without error
      });

      it('should fail if key is missing', function () {
        // given
        var fieldType = {
          title: 'Text',
          description: 'A nice textfield',
          icon: 'zmdi-check-text',
          render: {
            templateUrl: 'text.html'
          }
        };

        // when
        function instantiate() {
          createModule(fieldType);
        }

        // then -> perform without error
        expect(instantiate).toThrowError(/Config property "key" is required/);
      });

      it('should fail if icon is missing', function () {
        // given
        var fieldType = {
          key: 'text',
          title: 'Text',
          description: 'A nice textfield',
          render: {
            templateUrl: 'text.html'
          }
        };

        // when
        function instantiate() {
          createModule(fieldType);
        }

        // then -> perform without error
        expect(instantiate).toThrowError(/Config property "icon" is required/);
      });

      it('should fail if title is missing', function () {
        // given
        var fieldType = {
          key: 'text',
          icon: 'xyz',
          description: 'A nice textfield',
          render: {
            templateUrl: 'text.html'
          }
        };

        // when
        function instantiate() {
          createModule(fieldType);
        }

        // then -> perform without error
        expect(instantiate).toThrowError(/Config property "title" is required/);
      });

      it('should fail if description is missing', function () {
        // given
        var fieldType = {
          key: 'text',
          icon: 'zmdi-text',
          title: 'Text',
          render: {
            templateUrl: 'text.html'
          }
        };

        // when
        function instantiate() {
          createModule(fieldType);
        }

        // then -> perform without error
        expect(instantiate).toThrowError(/Config property "description" is required/);
      });

      it('should fail if render is missing', function () {
        // given
        var fieldType = {
          key: 'text',
          icon: 'zmdi-text',
          description: 'A Text field',
          title: 'Text'
        };

        // when
        function instantiate() {
          createModule(fieldType);
        }

        // then -> perform without error
        expect(instantiate).toThrowError(/Config property "render" with "templateUrl" is required/);
      });

    });

  });

})();
