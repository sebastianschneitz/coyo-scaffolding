(function () {
  'use strict';

  var moduleName = 'coyo.apps.task';
  var controllerName = 'TaskListModalController';

  describe('module: ' + moduleName, function () {
    var $controller, $rootScope, modalService, $state, $uibModalInstance;
    var root, lists;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_) {
      $controller = _$controller_;
      $rootScope = _$rootScope_;

      modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
      modalService.confirmDelete.and.returnValue({result: _$q_.resolve()});
      $state = jasmine.createSpyObj('$state', ['go']);
      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['dismiss']);

      root = 'main.page.show.apps.task';
      lists = [
        angular.extend(jasmine.createSpyObj('list', ['delete']), {id: 'task-list-1', title: 'Task List #1'}),
        angular.extend(jasmine.createSpyObj('list', ['delete']), {id: 'task-list-2', title: 'Task List #2'}),
        angular.extend(jasmine.createSpyObj('list', ['delete']), {id: 'task-list-3', title: 'Task List #3'})];
      lists[0].delete.and.returnValue(_$q_.resolve());
      lists[1].delete.and.returnValue(_$q_.resolve());
      lists[2].delete.and.returnValue(_$q_.resolve());
    }));

    function buildController(list) {
      return $controller(controllerName, {
        modalService: modalService,
        $state: $state,
        $uibModalInstance: $uibModalInstance,
        root: root,
        lists: lists,
        list: list
      });
    }

    describe('controller: ' + controllerName, function () {

      it('should delete a list', function () {
        // given
        var list = lists[0];
        var ctrl = buildController(list);
        var $event = jasmine.createSpyObj('$event', ['preventDefault', 'stopImmediatePropagation']);

        // when
        ctrl.deleteList($event);

        // then
        expect($event.preventDefault).toHaveBeenCalled();
        expect($event.stopImmediatePropagation).toHaveBeenCalled();
        $rootScope.$apply();
        expect(list.delete).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('.list.details', {id: 'task-list-2'}, {relative: root});
        expect($uibModalInstance.dismiss).toHaveBeenCalled();
        expect(lists.length).toEqual(2);
      });
    });
  });

})();
