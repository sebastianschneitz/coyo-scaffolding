(function (Config) {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    var appRegistryProvider, appRegistry;

    beforeEach(function () {
      Config.additionalAppSenderTypes = ['testSender'];

      // initialize the service provider by injecting it to a fake module's config block
      angular.module('coyo.apps.test', ['coyo.apps.api']);
      angular.module('coyo.apps.test').config(function (_appRegistryProvider_) {
        appRegistryProvider = _appRegistryProvider_;

        appRegistryProvider.register({
          key: 'test',
          name: 'Test App',
          description: 'Test App Description',
          states: [{}]
        });

        appRegistryProvider.register({
          key: 'test2',
          name: 'Test App 2',
          description: 'Test App 2 Description',
          allowedInstances: 1,
          allowedInstancesErrorMessage: 'CUSTOM.MESSAGE',
          states: [{name: 'child', url: '/child', default: true}]
        });
      });

      // initialize test.app injector
      module('coyo.apps.test');
    });

    beforeEach(inject(function (_appRegistry_) {
      appRegistry = _appRegistry_;
    }));

    afterEach(function () {
      Config.additionalAppSenderTypes = [];
    });

    describe('service: appRegistry', function () {

      it('returns an registered app with its properties', function () {
        expect(appRegistryProvider).not.toBeUndefined();

        var apps = appRegistry.getAll();
        expect(apps).toBeArrayOfSize(2);
        expect(apps[0]).toBeNonEmptyObject();
        expect(apps[0].key).toBe('test');
        expect(apps[0].name).toBe('Test App');
        expect(apps[0].description).toBe('Test App Description');
        expect(apps[0].allowedInstances).toBe(0);
        expect(apps[0].allowedInstancesErrorMessage).toBe('APPS.SETTINGS.NUMBEROFAPPS.ERROR');
        expect(apps[1]).toBeNonEmptyObject();
        expect(apps[1].key).toBe('test2');
        expect(apps[1].name).toBe('Test App 2');
        expect(apps[1].description).toBe('Test App 2 Description');
        expect(apps[1].allowedInstances).toBe(1);
        expect(apps[1].allowedInstancesErrorMessage).toBe('CUSTOM.MESSAGE');
      });

      it('should get a registered app', function () {
        // when
        var app = appRegistry.get('test');

        // then
        expect(app.name).toBe('Test App');
      });

      it('should return null if requested app is not available', function () {
        // when
        var app = appRegistry.get('unknown');

        // then
        expect(app).toBeNull();
      });

      it('should get default state name for a root state', function () {
        // when
        var defaultStateName = appRegistry.getDefaultStateName('test', 'testSender');

        // then
        expect(defaultStateName).toBe('main.testSender.show.apps.test');
      });

      it('should get default state name for a default state', function () {
        // when
        var defaultStateName = appRegistry.getDefaultStateName('test2', 'testSender');

        // then
        expect(defaultStateName).toBe('main.testSender.show.apps.test2.child');
      });

      it('should get all available senders', function () {
        // when
        var senderTypes = appRegistry.getAppSenderTypes();

        // then
        expect(senderTypes).toBeArrayOfSize(3);
        expect(senderTypes[0]).toBe('page');
        expect(senderTypes[1]).toBe('workspace');
        expect(senderTypes[2]).toBe('testSender');
      });

      it('should get root state name', function () {
        // when
        var defaultStateName = appRegistry.getRootStateName('test', 'testSender');

        // then
        expect(defaultStateName).toBe('main.testSender.show.apps.test');
      });
    });

  });

  describe('module: ' + moduleName, function () {

    describe('provider: appRegistryProvider', function () {

      function createModule(appConfig) {
        appConfig = appConfig || {
          key: 'test',
          name: 'Test App',
          description: 'Test App Description',
          states: [
            {
              // empty root state
            },
            {
              name: 'testState',
              url: '/test/state'
            }
          ]};
        var $stateProvider = null;

        angular.module('coyo.apps.test', ['coyo.apps.api']);
        angular.module('coyo.apps.test').config(function (_appRegistryProvider_, _$stateProvider_) {

          $stateProvider = _$stateProvider_;
          spyOn($stateProvider, 'state');

          _appRegistryProvider_.register(appConfig);
        });

        // when
        module('coyo.apps.test');
        inject(function () {
        });
        return $stateProvider;
      }

      it('should register valid states', function () {
        // given
        var $stateProvider = createModule();

        // then
        expect($stateProvider.state).toHaveBeenCalledTimes(4);

        var args = $stateProvider.state.calls.allArgs();

        // first call: sender "page", root state
        expect(args[0][0]).toBe('main.page.show.apps.test');
        expect(args[0][1].name).toBe('main.page.show.apps.test');
        expect(args[0][1].url).toBe('/test/:appIdOrSlug');

        // second call: sender "page", child state
        expect(args[1][0]).toBe('main.page.show.apps.test.testState');
        expect(args[1][1].name).toBe('main.page.show.apps.test.testState');
        expect(args[1][1].url).toBe('/test/state');

        // third call: sender "workspace", root state
        expect(args[2][0]).toBe('main.workspace.show.apps.test');
        expect(args[2][1].name).toBe('main.workspace.show.apps.test');
        expect(args[2][1].url).toBe('/test/:appIdOrSlug');

        // fourth call: sender "workspace", child state
        expect(args[3][0]).toBe('main.workspace.show.apps.test.testState');
        expect(args[3][1].name).toBe('main.workspace.show.apps.test.testState');
        expect(args[3][1].url).toBe('/test/state');
      });

      it('should transform a views name if the $appRoot is referenced', function () {
        // given
        var appConfig = {
          name: 'Test App',
          key: 'test',
          description: 'A test app',
          states: [{
            views: {
              'test@$appRoot': {}
            }
          }]
        };

        // when
        var $stateProvider = createModule(appConfig);

        // then
        expect($stateProvider.state).toHaveBeenCalledTimes(2);

        var args = $stateProvider.state.calls.allArgs();
        expect(args[0][0]).toBe('main.page.show.apps.test');
        expect(args[0][1].views).toHaveObject('test@main.page.show.apps.test');
        expect(args[0][1].views).not.toHaveObject('test@$appRoot');

        expect(args[1][0]).toBe('main.workspace.show.apps.test');
        expect(args[1][1].views).toHaveObject('test@main.workspace.show.apps.test');
        expect(args[1][1].views).not.toHaveObject('test@$appRoot');
      });

      it('should transform a views name if $sender is referenced', function () {
        // given
        var appConfig = {
          key: 'test',
          name: 'Test App',
          description: 'A test app',
          states: [{
            views: {
              'test@main.$sender.edit': {}
            }
          }]
        };

        // when
        var $stateProvider = createModule(appConfig);

        // then
        expect($stateProvider.state).toHaveBeenCalledTimes(2);

        var args = $stateProvider.state.calls.allArgs();
        expect(args[0][0]).toBe('main.page.show.apps.test');
        expect(args[0][1].views).toHaveObject('test@main.page.edit');
        expect(args[0][1].views).not.toHaveObject('test@main.$sender.edit');

        expect(args[1][0]).toBe('main.workspace.show.apps.test');
        expect(args[1][1].views).toHaveObject('test@main.workspace.edit');
        expect(args[1][1].views).not.toHaveObject('test@main.$sender.edit');
      });

      it('should fail when key is missing', function () {
        // given
        var appConfig = {
          name: 'Test App',
          description: 'A test app'
        };

        // when
        function instantiate() {
          createModule(appConfig);
        }

        // then
        expect(instantiate).toThrowError(/"key" is required/);
      });

      it('should fail when name is missing', function () {
        // given
        var appConfig = {
          key: 'test',
          description: 'A test app'
        };

        // when
        function instantiate() {
          createModule(appConfig);
        }

        // then
        expect(instantiate).toThrowError(/"name" is required/);
      });

      it('should fail when description is missing', function () {
        // given
        var appConfig = {
          key: 'test',
          name: 'Test App'
        };

        // when
        function instantiate() {
          createModule(appConfig);
        }

        // then
        expect(instantiate).toThrowError(/"description" is required/);
      });

      it('should fail when no default state present', function () {
        // given
        var appConfig = {
          key: 'test',
          name: 'Test App',
          description: 'A test app',
          states: [
            {
              name: 'test',
              url: 'url'
            }
          ]
        };

        // when
        function instantiate() {
          createModule(appConfig);
        }

        // then
        expect(instantiate).toThrowError(/Config must contain exactly one default state/);
      });

      it('should fail when more than one root state present', function () {
        // given
        var appConfig = {
          key: 'test',
          name: 'Test App',
          description: 'A test app',
          states: [{}, {}]
        };

        // when
        function instantiate() {
          createModule(appConfig);
        }

        // then
        expect(instantiate).toThrowError(/Config must contain exactly one default state/);
      });

      it('should fail when more than one default state present', function () {
        // given
        var appConfig = {
          key: 'test',
          name: 'Test App',
          description: 'A test app',
          states: [{default: true, url: '/test'}, {default: true, url: '/test'}]
        };

        // when
        function instantiate() {
          createModule(appConfig);
        }

        // then
        expect(instantiate).toThrowError(/Config must not contain more than one default state/);
      });

      it('should fail when root state is abstract', function () {
        // given
        var appConfig = {
          key: 'test',
          name: 'Test App',
          description: 'A test app',
          states: [{abstract: true}, {default: false, url: '/test'}]
        };

        // when
        function instantiate() {
          createModule(appConfig);
        }

        // then
        expect(instantiate).toThrowError(/Config must not contain an abstract default state/);
      });

      it('should fail when default state is abstract', function () {
        // given
        var appConfig = {
          key: 'test',
          name: 'Test App',
          description: 'A test app',
          states: [{}, {
            abstract: true,
            default: true,
            url: '/test'
          }]
        };

        // when
        function instantiate() {
          createModule(appConfig);
        }

        // then
        expect(instantiate).toThrowError(/Config must not contain an abstract default state/);
      });
    });
  });

})(Config);
