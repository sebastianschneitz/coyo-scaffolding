(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';
  var directiveName = 'coyo-app-language-switch';

  describe('module: ' + moduleName, function () {

    var $controller, $timeout, utilService, articleId, currentLanguage, availableLanguages, fetchTranslation;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$timeout_) {
        $controller = _$controller_;
        $timeout = _$timeout_;
        utilService = jasmine.createSpyObj('utilService', ['uuid']);
        articleId = 'BLOG-ARTICLE-ID';
        currentLanguage = 'EN';
        availableLanguages = ['DE', 'EN'];
      }));

      var controllerName = 'AppLanguageSwitchController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            $timeout: $timeout,
            utilService: utilService,
            $controller: $controller,
            articleId: articleId,
            currentLanguage: currentLanguage,
            availableLanguages: availableLanguages,
            fetchTranslation: fetchTranslation
          });
        }

        it('should init open with false and id with an uuid', function () {
          // when
          var ctrl = buildController();

          // then
          expect(ctrl.open).toBe(false);
          expect(utilService.uuid).toHaveBeenCalled();
        });

        it('should init open with false and id with an uuid', function () {
          // when
          var ctrl = buildController();
          ctrl.translateTo('DE');
          ctrl.fetchTranslation = jasmine.createSpy('vm.fetchTranslation');
          ctrl.fetchTranslation.and.returnValue();
          $timeout.flush();
          // then
          expect(ctrl.fetchTranslation).toHaveBeenCalledWith('DE');
        });
      });
    });
  });

})();
