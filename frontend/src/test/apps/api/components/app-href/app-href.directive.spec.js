(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';
  var directiveName = 'coyo-app-href';

  describe('module: ' + moduleName, function () {

    var $scope, $compile, template, appRegistry;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {
        appRegistry = jasmine.createSpyObj('appRegistry', ['getDefaultStateName']);
        module(function ($provide) {
          $provide.value('appRegistry', appRegistry);
        });
      });

      beforeEach(inject(function ($rootScope, _$compile_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        template = '<a coyo-app-href="app" sender-type="{{senderType}}">Link</a>';
      }));

      it('should add href attribute to link', function () {
        // given
        $scope.app = {
          id: 'TEST-ID',
          key: 'test'
        };
        $scope.senderType = 'testSenders';

        appRegistry.getDefaultStateName.and.returnValue('main.testSenders.show.apps.test');

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.attr('ui-sref')).toBe('main.testSenders.show.apps.test({"appIdOrSlug":"TEST-ID"})');
        expect(element.attr('coyo-app-href')).toBeUndefined();
        expect(appRegistry.getDefaultStateName).toHaveBeenCalled();

        var args = appRegistry.getDefaultStateName.calls.mostRecent().args;
        expect(args[0]).toBe('test');
        expect(args[1]).toBe('testSenders');
      });

      it('should add href attribute to link including sender slug', function () {
        // given
        $scope.app = {
          id: 'TEST-ID',
          key: 'test',
          senderSlug: 'testSenderSlug',
          senderId: 'testSenderId'
        };
        $scope.senderType = 'testSenders';

        appRegistry.getDefaultStateName.and.returnValue('main.testSenders.show.apps.test');

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.attr('ui-sref')).toBe('main.testSenders.show.apps.test({"idOrSlug":"testSenderSlug","appIdOrSlug":"TEST-ID"})');
      });

      it('should add href attribute to link including sender id', function () {
        // given
        $scope.app = {
          id: 'TEST-ID',
          key: 'test',
          senderId: 'testSenderId'
        };
        $scope.senderType = 'testSenders';

        appRegistry.getDefaultStateName.and.returnValue('main.testSenders.show.apps.test');

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.attr('ui-sref')).toBe('main.testSenders.show.apps.test({"idOrSlug":"testSenderId","appIdOrSlug":"TEST-ID"})');
      });
    });
  });

})();
