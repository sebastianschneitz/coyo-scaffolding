(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';
  var directiveName = 'oyoc-app-settings';

  describe('module: ' + moduleName, function () {

    var $controller, $log, appRegistry;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$log_) {
        $controller = _$controller_;
        $log = _$log_;
        appRegistry = jasmine.createSpyObj('appRegistry', ['get']);
      }));

      var controllerName = 'AppSettingsController';

      describe('controller: ' + controllerName, function () {

        function buildController(settings) {
          return $controller(controllerName, {
            $log: $log,
            appRegistry: appRegistry
          }, {
            app: {
              name: 'Test App',
              key: 'test',
              settings: settings
            }
          });
        }

        it('should init with undefined settings', function () {
          // when
          var ctrl = buildController();

          // then
          expect(ctrl.app.settings).toBeEmptyObject();
          expect(appRegistry.get).toHaveBeenCalledWith('test');
        });

        it('should init with given settings', function () {
          // given
          var settings = {test: 'test'};

          // when
          var ctrl = buildController(settings);

          // then
          expect(ctrl.app.settings).toBe(settings);
        });
      });
    });
  });

})();
