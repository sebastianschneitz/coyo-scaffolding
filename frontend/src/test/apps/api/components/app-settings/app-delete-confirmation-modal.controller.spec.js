(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    var $controller, FolderModel, $q;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_) {
      $controller = _$controller_;
      $q = _$q_;
      FolderModel = jasmine.createSpyObj('FolderModel', ['hasChildren']);
    }));

    var controllerName = 'AppDeleteConfirmationModalController';

    describe('controller: ' + controllerName, function () {

      function buildController(app) {
        return $controller(controllerName, {FolderModel: FolderModel, app: app});
      }

      it('should fetch children of app folder on init', function () {
        // given
        var app = {id: 'app-id', senderId: 'senderId', rootFolderId: 'rootFolderId'};
        var ctrl = buildController(app);
        FolderModel.hasChildren.and.returnValue($q.resolve(true));

        // when
        ctrl.$onInit();

        // then
        expect(FolderModel.hasChildren).toHaveBeenCalledWith(app.senderId, app.rootFolderId);
      });

      it('should not fetch children of app folder if there is no app folder', function () {
        // given
        var ctrl = buildController({});

        // when
        ctrl.$onInit();

        // then
        expect(FolderModel.hasChildren).toHaveBeenCalledTimes(0);
      });

    });
  });

})();
