(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    var appService, appRegistry, $scope, $state, $rootScope, $stateParams, $q, $transitions, appChooserModalService,
        app, sender;

    beforeEach(function () {
      app = {
        id: 'TEST-ID',
        key: 'test',
        name: 'Test App',
        active: true
      };

      sender = {
        id: 'SENDER-ID'
      };

      $state = jasmine.createSpyObj('$state', ['go', 'includes', 'href']);
      $state.$current = {name: 'main.testSenderType.show.apps'};
      $stateParams = {appIdOrSlug: 'TEST-ID'};
      $transitions = jasmine.createSpyObj('$transitions', ['onSuccess', 'onError']);
      $transitions.onSuccess.and.callFake(function (criteria, callback) {
        $rootScope.$on('transition-success-mock', callback);
      });

      appRegistry = jasmine.createSpyObj('appRegistry', ['getDefaultStateName', 'getAppSenderTypes']);
      appRegistry.getAppSenderTypes.and.returnValue(['testSenderType']);

      appChooserModalService = jasmine.createSpyObj('appChooserModalService', ['open']);

      module(moduleName, function ($provide) {
        $provide.value('$state', $state);
        $provide.value('$stateParams', $stateParams);
        $provide.value('$transitions', $transitions);
        $provide.value('appRegistry', appRegistry);
        $provide.value('appChooserModalService', appChooserModalService);
      });

      inject(function (_appService_, _$rootScope_, _$q_, _$transitions_) {
        $rootScope = _$rootScope_;
        $q = _$q_;
        $scope = $rootScope.$new();
        appService = _appService_;
        $transitions = _$transitions_;
      });

    });

    describe('service: appService', function () {

      it('should add an app', function () {
        // given
        var apps = [];
        appChooserModalService.open.and.returnValue($q.resolve(app));
        appRegistry.getDefaultStateName.and.returnValue('main.testSenderType.show.apps.test');

        // when
        appService.addApp(sender, apps);
        $scope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith('main.testSenderType.show.apps.test',
            {idOrSlug: 'SENDER-ID', appIdOrSlug: 'TEST-ID', created: true},
            {location: 'replace', inherit: false});

        expect(apps).toBeArrayOfSize(1);
        expect(apps[0]).toBe(app);
      });

      it('should update an app', function () {
        // given
        var updatedApp = {id: 'TEST-ID', key: 'test', name: 'Updated App'};
        var apps = [app];

        // when
        appService.updateApp(updatedApp, apps);

        // then
        expect(apps).toBeArrayOfSize(1);
        expect(apps[0]).not.toBe(app);
        expect(apps[0]).toBe(updatedApp);
      });

      it('should delete an app', function () {
        // given
        var apps = [app];

        // when
        appService.deleteApp(app.id, apps);

        // then
        expect(apps).toBeEmptyArray();
      });

      it('should reload the current app', function () {
        // given
        $state.includes.and.returnValue(true);

        // when
        appService.reloadApp(app);

        // then
        expect($state.go).toHaveBeenCalled();
        var calls = $state.go.calls.mostRecent().args;
        expect(calls[2]).toEqual({reload: 'main.testSenderType.show.apps.test'});
      });

      it('should not reload if app is not current', function () {
        // given
        $state.includes.and.returnValue(false);

        // when
        appService.reloadApp(app);

        // then
        expect($state.go).not.toHaveBeenCalled();
      });

      it('should check if app is current', function () {
        // given

        // when
        appService.isCurrentApp(app);

        // then
        expect($state.includes).toHaveBeenCalledWith('main.testSenderType.show.apps.test', {appIdOrSlug: 'TEST-ID'});
      });

      it('should redirect to the default app', function () {
        // given
        appRegistry.getDefaultStateName.and.returnValue('main.testSenderType.show.apps.test');

        // when
        appService.redirectToSender({
          id: 'SENDER-ID',
          typeName: 'testSenderType',
          appNavigation: [{name: '', apps: [app.id]}]
        }, [app]);

        // then
        expect($state.go).toHaveBeenCalledWith('main.testSenderType.show.apps.test',
            {idOrSlug: 'SENDER-ID', appIdOrSlug: 'TEST-ID', created: false},
            {location: 'replace', inherit: false});
      });

      it('should redirect to the sender if no apps present', function () {
        // given
        appRegistry.getDefaultStateName.and.returnValue('main.testSenderType.show.apps.test');

        // when
        appService.redirectToSender({
          id: 'SENDER-ID',
          typeName: 'testSenderType',
          appNavigation: [{name: '', apps: []}]
        });

        // then
        expect($state.go).toHaveBeenCalledWith('main.testSenderType.show',
            {idOrSlug: 'SENDER-ID'},
            {location: 'replace', inherit: false});
      });

      it('should redirect to app', function () {
        // given
        appRegistry.getDefaultStateName.and.returnValue('main.testSenderType.show.apps.test');

        // when
        appService.redirectToApp({
          id: 'SENDER-ID',
          typeName: 'testSenderType'
        }, app, true);

        // then
        expect($state.go).toHaveBeenCalledWith('main.testSenderType.show.apps.test',
            {idOrSlug: 'SENDER-ID', appIdOrSlug: 'TEST-ID', created: true},
            {location: 'replace', inherit: false});
      });

      it('should navigate to app', function () {
        // given
        var stateOptions = {test: true};
        appRegistry.getDefaultStateName.and.returnValue('main.testSenderType.show.apps.test');

        // when
        appService.getAppLinkForCreatedApp({
          id: 'SENDER-ID',
          typeName: 'testSenderType'
        }, app, stateOptions);

        // then
        expect($state.href).toHaveBeenCalledWith('main.testSenderType.show.apps.test',
            {idOrSlug: 'SENDER-ID', appIdOrSlug: 'TEST-ID', created: true}, {location: 'replace', inherit: false});
      });

      it('should return the current app id or null', function () {
        // when
        $state.includes.and.returnValue(true);

        // then
        expect(appService.getCurrentAppIdOrSlug()).toEqual('TEST-ID');
      });

      it('should return null if not app id active', function () {
        // when
        $state.includes.and.returnValue(false);

        // then
        expect(appService.getCurrentAppIdOrSlug()).toBeNull();
      });

      it('should execute a callback on app change', function () {
        // given
        $state.includes.and.returnValue(true);
        var callback = jasmine.createSpy('callback');

        // when
        appService.onAppChanged(callback);
        $rootScope.$emit('transition-success-mock');

        // then
        expect(callback).toHaveBeenCalledWith('TEST-ID');
      });

      it('should return relative url for given app', function () {
        // given
        var app = {id: 'appId', slug: 'appSlug'};
        var sender = {id: 'senderId', slug: 'senderSlug'};
        var url = '/senderSlug/appSlug';
        appRegistry.getDefaultStateName.and.returnValue('main.testSenderType.show.apps.test');
        $state.href.and.returnValue('/senderSlug/appSlug');

        // when
        var result = appService.getAppLinkForCreatedApp(sender, app);

        // then
        expect(result).toEqual(url);
      });
    });
  });

})();
