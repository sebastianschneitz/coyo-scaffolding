(function () {
  'use strict';

  var moduleName = 'coyo.apps.wiki';

  describe('module: ' + moduleName, function () {

    var $rootScope, $q, $controller, $state, $timeout, app, article, utilService, widgetLayoutService,
        wikiArticleTranslationService, sender, currentUser, SettingsModel;

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$q_, _$controller_, _$timeout_, _SettingsModel_) {
      $rootScope = _$rootScope_;
      $q = _$q_;
      $controller = _$controller_;
      $state = jasmine.createSpyObj('$state', ['go']);
      $timeout = _$timeout_;
      SettingsModel = _SettingsModel_;
      app = jasmine.createSpyObj('app', ['']);
      article = jasmine.createSpyObj('article', ['isNew', 'create', 'update']);
      utilService = jasmine.createSpyObj('utilService', ['uuid']);
      widgetLayoutService = jasmine.createSpyObj('widgetLayoutService', ['onload', 'save', 'edit', 'cancel']);
      widgetLayoutService.save.and.returnValue($q.resolve());
      widgetLayoutService.onload.and.returnValue($q.resolve());
      wikiArticleTranslationService =
          jasmine.createSpyObj('wikiArticleTranslationService', ['init', 'prepareTranslations']);

      article.id = 'ArticleId';
      article.title = 'Title1';
      sender = {};
      app.settings = {
        publisherIds: []
      };
    }));

    currentUser = jasmine.createSpyObj('UserModel', ['getUser']);

    var controllerName = 'WikiArticleCreateController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $rootScope.$new(),
          $state: $state,
          utilService: utilService,
          widgetLayoutService: widgetLayoutService,
          currentUser: currentUser,
          app: app,
          article: article,
          editMode: false,
          sender: sender,
          wikiArticleTranslationService: wikiArticleTranslationService
        });
      }

      it('should save a new wiki article', function () {
        // given
        spyOn(SettingsModel, 'retrieveByKey').and.callFake(function () {
          return $q.resolve('true');
        });
        var ctrl = buildController();
        var defer = $q.defer();
        article.isNew.and.returnValue(true);
        article.create.and.returnValue(defer.promise);
        ctrl.editMode = true;
        ctrl.loading = false;

        ctrl.newTitle = 'Some-New-Title-1';

        // when
        ctrl.save();
        defer.resolve(article);
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(widgetLayoutService.save).toHaveBeenCalled();
        expect(article.create).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('^.view', {id: article.id, currentLanguage: ctrl.currentLanguage});
      });

      it('should be able to cancel', function () {
        // given
        spyOn(SettingsModel, 'retrieveByKey').and.callFake(function () {
          return $q.resolve('true');
        });
        var ctrl = buildController();
        ctrl.editMode = true;
        ctrl.loading = false;
        ctrl.newTitle = 'Random-New-Title-1';

        // when
        ctrl.cancel();

        // then
        expect(ctrl.editMode).toEqual(false);
        expect(widgetLayoutService.cancel).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('^');
      });

    });
  });

})();
