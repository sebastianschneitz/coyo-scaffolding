(function () {
  'use strict';

  var moduleName = 'coyo.apps.timeline';
  var ctrlName = 'TimelineSettingsController';

  describe('module: ' + moduleName, function () {

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    describe('controller: ' + ctrlName, function () {
      var $controller, $scope;

      beforeEach(inject(function ($rootScope, _$controller_) {
        $scope = $rootScope.$new();
        $controller = _$controller_;

        $scope.model = {
          settings: {}
        };

      }));

      function buildController() {
        return $controller(ctrlName, {
          $scope: $scope
        });
      }

      describe('controller init', function () {

        it('should initialize with default values for authorType', function () {
          // when
          var ctrl = buildController();

          // then
          expect(ctrl.app.settings.authorType).toBe('VIEWER');
        });

        it('should initialize authorType if set', function () {
          // given
          $scope.model.settings.authorType = 'ADMIN';

          // when
          var ctrl = buildController();

          // then
          expect(ctrl.app.settings.authorType).toBe('ADMIN');
        });

      });

    });
  });
})();
