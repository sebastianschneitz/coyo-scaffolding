(function () {
  'use strict';

  var moduleName = 'coyo.app';

  describe('module: ' + moduleName, function () {

    var $timeout, SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieveByKey']), $rootScope, $window;

    function mockTrackingCodeSettings(code) {
      SettingsModel.retrieveByKey.and.callFake(function (key) {
        return {
          then: function (cb) {
            cb(key === 'trackingCode' ? code : null);
          }
        };
      });
    }

    function expectTrackingCode(expectedCode) {
      expect($window.document.body.innerHTML.indexOf(expectedCode)).not.toBe(-1);
    }

    beforeEach(function () {

      module(moduleName, function ($provide) {
        mockTrackingCodeSettings('<script>function test() {}</script>');
        $provide.value('SettingsModel', SettingsModel);
      });

      inject(function (_$timeout_, _$window_, _$rootScope_) {
        $timeout = _$timeout_;
        $rootScope = _$rootScope_;
        $window = _$window_;
      });

    });

    describe('application init', function () {

      it('should set tracking code on app init', function () {
        // when
        $timeout.flush();

        // then
        expectTrackingCode('<script class="tracking-code-script">function test() {}</script>');
      });

      it('should set tracking code after login', function () {
        // given
        var code = '<script>function test() {}</script>';
        mockTrackingCodeSettings(code);

        // when
        $rootScope.$emit('authService:login:success');
        $timeout.flush();

        // then
        expectTrackingCode('<script class="tracking-code-script">function test() {}</script>');
      });
    });

  });
})();
