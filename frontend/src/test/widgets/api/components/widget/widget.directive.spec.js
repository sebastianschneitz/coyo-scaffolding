(function () {
  'use strict';

  var moduleName = 'coyo.widgets.api';
  var directiveName = 'widget';

  describe('module: ' + moduleName, function () {

    var $scope, $compile, template;

    // create a fake module with an example directive
    beforeEach(function () {
      angular.module('module.test', [moduleName]);
      angular.module('module.test').directive('testDirective', function () {
        return {
          scope: {
            widget: '='
          },
          template: '<div>{{widget.name}}</div>'
        };
      });

      // initialize the module
      module('module.test');
    });

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function ($rootScope, _$compile_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        template = '<coyo-widget widget-model="model" widget-config="config"></coyo-widget>';
      }));

      it('should compile a given directive', function () {
        // given
        $scope.model = {
          name: 'Test Widget'
        };

        $scope.config = {
          directive: 'test-directive'
        };

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        var testDirective = element.find('test-directive');
        expect(testDirective.length).toBe(1);
        expect(testDirective.find('div').text()).toBe('Test Widget');
      });

    });

  });
})();
