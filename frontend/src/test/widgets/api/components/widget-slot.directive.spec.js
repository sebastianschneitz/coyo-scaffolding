(function () {
  'use strict';

  var moduleName = 'coyo.widgets.api';
  var directiveName = 'widget-slot';

  describe('module: ' + moduleName, function () {

    var parent, widgets, modalService, widgetRegistry, widgetChooserModalService, widgetSettingsModalService, WidgetModel, $rootScope, ngxWidgetSettingsService, ngxWidgetVisibilityService;

    function createWidget(data) {
      return angular.extend(jasmine.createSpyObj('WidgetModel', ['isNew', 'snapshot', 'unsnappedChanges', 'rollback', 'save', 'remove', 'create']), data);
    }

    beforeEach(module('commons.templates'));
    beforeEach(module(moduleName, function ($provide) {

      parent = {
        id: 'SENDER-ID',
        typeName: 'senderType'
      };

      widgets = [createWidget({
        id: 'ID-ONE',
        key: 'widget-one',
        settings: {},
        _permissions: {'manage': true, 'update': true}
      }), createWidget({
        id: 'ID-TWO',
        key: 'widget-two',
        settings: {},
        _permissions: {'manage': true, 'update': true}
      })];

      modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
      modalService.confirmDelete.and.returnValue({
        result: {
          then: function (callback) {
            return callback();
          }
        }
      });

      widgetRegistry = jasmine.createSpyObj('widgetRegistry', ['get', 'getEnabled']);
      widgetRegistry.get.and.returnValue({name: 'Test Widget'});

      widgetChooserModalService = jasmine.createSpyObj('widgetChooserModalService', ['open']);
      widgetSettingsModalService = jasmine.createSpyObj('widgetSettingsModalService', ['open']);
      ngxWidgetSettingsService = jasmine.createSpyObj('ngxWidgetSettingsService', ['propagateChanges', 'getAllChanges$']);
      ngxWidgetSettingsService.propagateChanges.and.returnValue('');
      ngxWidgetSettingsService.getAllChanges$.and.returnValue({subscribe: function () {}});
      ngxWidgetVisibilityService = jasmine.createSpyObj('ngxWidgetVisibilityService', ['getHiddenIds$']);
      ngxWidgetVisibilityService.getHiddenIds$.and.returnValue({subscribe: function () {}});

      WidgetModel = angular.extend(function (data) {
        return data;
      }, jasmine.createSpyObj('WidgetModel', ['remove', 'order', 'queryWithCache', 'fromConfig']));
      WidgetModel.queryWithCache.and.returnValue({
        then: function (callback) {
          return callback(widgets);
        }
      });
      WidgetModel.fromConfig.and.callFake(createWidget);

      $provide.value('modalService', modalService);
      $provide.value('ngxWidgetRegistry', widgetRegistry);
      $provide.value('ngxWidgetChooser', widgetChooserModalService);
      $provide.value('ngxWidgetSettingsModalService', widgetSettingsModalService);
      $provide.value('WidgetModel', WidgetModel);
      $provide.value('ngxWidgetSettingsService', ngxWidgetSettingsService);
      $provide.value('ngxWidgetVisibilityService', ngxWidgetVisibilityService);
    }));

    describe('directive: ' + directiveName, function () {

      describe('controller: WidgetSlotController', function () {

        var $controller, $q, $scope, $timeout, deferredLoadingSuccess;

        beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _$timeout_) {
          $controller = _$controller_;
          $q = _$q_;
          $rootScope = _$rootScope_;
          $timeout = _$timeout_;
          $scope = $rootScope.$new();
        }));

        function buildController(parent, addInitialWidget, initialEditMode) {
          deferredLoadingSuccess = $q.defer();
          return $controller('WidgetSlotController', {
            $scope: $scope
          }, {
            name: 'test-slot',
            parent: parent,
            addInitialWidget: addInitialWidget,
            initialEditMode: initialEditMode,
            loadingPromises: [deferredLoadingSuccess.promise]
          });
        }

        it('should init with a list of enabled widgets', function () {
          // when
          var ctrl = buildController();
          $scope.$apply();

          // then
          expect(WidgetModel.queryWithCache).toHaveBeenCalledWith({}, 'test-slot');
          expect(widgetRegistry.get).toHaveBeenCalledTimes(2);
          expect(ctrl.widgets).toBeArrayOfSize(2);
          expect(ctrl.widgets[0].model.key).toBe('widget-one');
          expect(ctrl.widgets[0].config.name).toBe('Test Widget');
          expect(ctrl.widgets[1].model.key).toBe('widget-two');
          expect(ctrl.widgets[1].config.name).toBe('Test Widget');
        });

        it('should init with a set parent', function () {
          // when
          buildController(parent);
          $scope.$apply();

          // then
          expect(WidgetModel.queryWithCache).toHaveBeenCalled();

          var args = WidgetModel.queryWithCache.calls.mostRecent().args;
          expect(args[0].parentId).toBe(parent.id);
          expect(args[0].parentType).toBe(parent.typeName);
          expect(args[1]).toBe('test-slot');
        });

        it('should init add text widget if configured with no widgets present', function () {
          // given
          WidgetModel.queryWithCache.and.returnValue($q.resolve([]));

          // when
          var ctrl = buildController(null, true);
          $scope.$apply();

          // then
          expect(ctrl.widgets).toBeArrayOfSize(1);
          expect(ctrl.widgets[0].model.key).toBe('rte');
          expect(ctrl.widgets[0].model._permissions.manage).toBe(true);
          expect(ctrl.widgets[0].model._permissions.update).toBe(true);
          expect(ctrl.widgets[0].model.settings).toEqual({});
        });

        it('should init not add text widget if configured with widgets present', function () {
          // given
          WidgetModel.queryWithCache.and.returnValue($q.resolve([{key: 'test'}]));

          // when
          var ctrl = buildController(null, true);
          $scope.$apply();

          // then
          expect(ctrl.widgets).toBeArrayOfSize(1);
          expect(ctrl.widgets[0].model.key).toBe('test');
        });

        it('should enable edit mode', function () {
          // given
          var ctrl = buildController();
          var initialEditModeValue = ctrl.editMode;

          // when
          $scope.$apply();
          ctrl.enableEditMode(false);

          // then
          expect(initialEditModeValue).toBe(false);
          expect(ctrl.editMode).toBe(true);
          expect(ctrl.widgets === ctrl.originalWidgets).toBe(false);
          expect(ctrl.widgets).toEqual(ctrl.originalWidgets);
          _.forEach(ctrl.widgets, function (widget) {
            expect(widget.model.snapshot).toHaveBeenCalled();
          });
        });

        it('should enable edit mode after loading', function () {
          // given
          var ctrl = buildController(null, null, true);

          // when
          deferredLoadingSuccess.resolve();
          $scope.$apply();

          // then
          expect(ctrl.editMode).toBe(true);
          expect(ctrl.widgets).toBeArrayOfSize(2);
          _.forEach(ctrl.widgets, function (widget) {
            expect(widget.model.snapshot).toHaveBeenCalled();
          });
        });

        it('should cancel', function () {
          // given
          var ctrl = buildController();
          ctrl.enableEditMode();
          var originalWidgetsSize = ctrl.widgets.length;
          ctrl.widgets.push({model: {}});
          ctrl.widgets[0].deleted = true;

          // when
          ctrl.cancel();

          // then
          expect(ctrl.editMode).toBe(false);
          expect(ctrl.widgets.length).toBe(originalWidgetsSize);
          _.forEach(ctrl.widgets, function (widget) {
            expect(widget.deleted).toBe(false);
            expect(widget.model.rollback).toHaveBeenCalled();
          });
        });

        it('should save widgets', function () {
          // given
          var ctrl = buildController();
          ctrl.editMode = true;
          var deletedWidget = ctrl.widgets[0];
          deletedWidget.model.remove.and.returnValue($q.resolve());
          ctrl.removeWidget(deletedWidget);
          var modifiedWidget = ctrl.widgets[1];
          modifiedWidget.model.unsnappedChanges.and.returnValue(true);
          modifiedWidget.model.save.and.returnValue($q.resolve(modifiedWidget.model));

          // when
          ctrl.save();
          $rootScope.$apply();
          $timeout.flush();

          // then
          expect(deletedWidget.model.remove).toHaveBeenCalled();
          expect(deletedWidget.model.save).not.toHaveBeenCalled();
          expect(ctrl.widgets.length).toBe(1);

          expect(modifiedWidget.model.save).toHaveBeenCalled();
          expect(modifiedWidget.model.remove).not.toHaveBeenCalled();

          expect(WidgetModel.order).toHaveBeenCalledWith('test-slot', undefined, [modifiedWidget.model.id]);
          expect(ctrl.editMode).toBe(false);
        });

        it('should not save unmodified widgets', function () {
          // given
          var ctrl = buildController();
          ctrl.enableEditMode();

          // when
          ctrl.save();
          $rootScope.$apply();

          // then
          _.forEach(ctrl.widgets, function (widget) {
            expect(widget.model.save).not.toHaveBeenCalled();
            expect(widget.model.remove).not.toHaveBeenCalled();
          });
          expect(WidgetModel.order).not.toHaveBeenCalled();
        });

        it('should create new version of widget in copy mode if not set saved before', function () {
          // given
          var ctrl = buildController();
          ctrl.copyMode = true;
          ctrl.enableEditMode();

          var unsavedWidget = ctrl.widgets[1];
          unsavedWidget.saved = false;
          unsavedWidget.model.unsnappedChanges.and.returnValue(true);
          unsavedWidget.model.create.and.returnValue($q.resolve(unsavedWidget.model));
          var savedWidget = ctrl.widgets[0];
          savedWidget.saved = true;
          savedWidget.model.unsnappedChanges.and.returnValue(true);
          savedWidget.model.save.and.returnValue($q.resolve(savedWidget.model));

          // when
          ctrl.save();
          $rootScope.$apply();
          $timeout.flush();

          // then
          expect(unsavedWidget.model.create).toHaveBeenCalled();
          expect(unsavedWidget.model.save).not.toHaveBeenCalled();
          expect(savedWidget.model.create).not.toHaveBeenCalled();
          expect(savedWidget.model.save).toHaveBeenCalled();
        });

        it('should add a widget', function () {
          // given
          var ctrl = buildController(parent);
          var config = {key: 'widget-three'};
          widgetChooserModalService.open.and.returnValue($q.resolve(config));
          widgetRegistry.get.and.returnValue(config);

          // when
          ctrl.addWidget();
          $timeout.flush();
          $scope.$apply();

          // then
          expect(ctrl.widgets).toBeArrayOfSize(3);
          expect(ctrl.widgets[2].config.key).toBe('widget-three');
          expect(ctrl.widgets[2].model.slot).toBe('test-slot');
          expect(ctrl.widgets[2].model.parentId).toBe(parent.id);
        });

        it('should edit a widget', function () {
          // given
          var ctrl = buildController();
          var editResult = {settings: {'test': 'test'}};
          var editedWidget = angular.extend(widgets[0], editResult);
          var widgetScope = jasmine.createSpyObj('$childScope', ['$broadcast']);
          widgetSettingsModalService.open.and.returnValue($q.resolve(editResult));

          // when
          ctrl.editWidget(widgets[0], widgetScope);
          $scope.$apply();

          // then
          expect(ctrl.widgets).toBeArrayOfSize(2);
          expect(ctrl.widgets[0].model).toBe(editedWidget);
          expect(widgetScope.$broadcast).not.toHaveBeenCalled();
          $timeout.flush();
          // expect(widgetScope.$broadcast).toHaveBeenCalled();
        });

        it('should delete a new widget', function () {
          // given
          var ctrl = buildController();
          widgets[0].remove.and.returnValue($q.resolve());
          widgets[0].isNew.and.returnValue(true);

          // when
          ctrl.removeWidget(ctrl.widgets[0]);
          $scope.$apply();

          // then
          expect(ctrl.widgets).toBeArrayOfSize(1);
          expect(_.some(ctrl.widgets, function (widget) {
            return widget.model.id === ctrl.widgets[0].id;
          })).toBe(false);
          expect(ctrl.widgets[0].model.id).toBe('ID-TWO');
        });

        it('should soft delete an existing widget', function () {
          // given
          var ctrl = buildController();
          widgets[0].remove.and.returnValue($q.resolve());
          widgets[0].isNew.and.returnValue(false);

          // when
          ctrl.removeWidget(ctrl.widgets[0]);
          $scope.$apply();

          // then
          expect(ctrl.widgets).toBeArrayOfSize(2);
          expect(ctrl.widgets[0].deleted).toBe(true);
          expect(ctrl.widgets[0].model.id).toBe('ID-ONE');
        });

        it('should order widgets', function () {
          // given
          var ctrl = buildController();
          var widgetModel = widgets[0];

          // when
          ctrl.orderWidget({model: widgetModel}, 0, false);

          // then
          expect(ctrl.widgets[1].model.id).toEqual(widgetModel.id);
        });

        it('should cut widget and clear clipboard', function () {
          // given
          var ctrl = buildController();
          var widget = ctrl.widgets[0];

          // when
          ctrl.cutWidget(ctrl.widgets, widget);

          // then
          expect($rootScope.widgetClipboard.widget).toEqual(widget.model);
          expect($rootScope.widgetClipboard.originalParentSlot).toEqual(ctrl);
          expect($rootScope.widgetClipboard.onAfterMove).toBeDefined();

          // when
          ctrl.clearClipboard();

          // then
          expect($rootScope.widgetClipboard).not.toBeDefined();
        });

        it('should paste widget', function () {
          // given
          var ctrl = buildController();
          var ctrl2 = buildController();

          var movedWidget = {
            model: createWidget({id: 123, key: 'widget-key'})
          };

          ctrl2.widgets = [movedWidget];

          ctrl2.cutWidget(ctrl.widgets, movedWidget);

          // when
          ctrl.pasteWidget();

          // then
          expect(ctrl.widgets.length).toBe(widgets.length + 1);
          expect(ctrl2.widgets.length).toBe(0);
          expect(_.filter(ctrl.widgets, {model: {id: movedWidget.model.id}}).length).toBe(1);
        });

        it('should not filter non-deleted widgets', function () {
          // given
          var ctrl = buildController();

          // when
          var result = ctrl.hasVisibleWidgets();

          // then
          expect(result).toBe(true);
        });

        it('should filter deleted widgets', function () {
          // given
          var ctrl = buildController();
          _.forEach(ctrl.widgets, function (widget) {
            widget.deleted = true;
          });

          // when
          var result = ctrl.hasVisibleWidgets();

          // then
          expect(result).toBe(false);
        });

        describe('mobile hidden flag', function () {
          it('should enable toggle', function () {
            // given
            var ctrl = buildController();
            var widget = {settings: {}};

            // when
            ctrl.toggleMobile(widget);

            // then
            expect(widget.settings._hideMobile).toBe(true);
          });

          it('should disable toggle', function () {
            // given
            var ctrl = buildController();
            var widget = {settings: {_hideMobile: true}};

            // when
            ctrl.toggleMobile(widget);

            // then
            expect(widget.settings._hideMobile).toBe(false);
          });

          it('should default to not hidden', function () {
            // given
            var ctrl = buildController();
            var widget = {};

            // when
            var result = ctrl.isMobileHidden(widget);

            // then
            expect(result).toBe(false);
          });

          it('should get hidden flag from settings', function () {
            // given
            var ctrl = buildController();
            var widget = {model: {settings: {_hideMobile: true}}};

            // when
            var result = ctrl.isMobileHidden(widget);

            // then
            expect(result).toBe(true);
          });
        });

        describe('buttons', function () {

          var ctrl;

          beforeEach(function () {
            ctrl = buildController();
          });

          describe('add button', function () {

            it('should be visible', function () {
              // given
              ctrl.editMode = true;
              $rootScope.widgetClipboard = undefined;
              ctrl.widgets = [{model: {}}];
              ctrl.hideSlotPlaceholder = false;
              ctrl.simpleMode = false;

              // when
              var visible = ctrl.isAddButtonVisible();

              // then
              expect(visible).toBe(true);
            });

            it('should not be visible without edit mode', function () {
              // given
              ctrl.editMode = false;
              $rootScope.widgetClipboard = undefined;
              ctrl.widgets = [{model: {}}];
              ctrl.hideSlotPlaceholder = false;
              ctrl.simpleMode = false;

              // when
              var visible = ctrl.isAddButtonVisible();

              // then
              expect(visible).toBe(false);
            });

            it('should not be visible without edit mode and widgets and controls hidden but with placeholder hidden', function () {
              // given
              ctrl.editMode = false;
              $rootScope.widgetClipboard = undefined;
              ctrl.widgets = [];
              ctrl.hideSlotPlaceholder = true;
              ctrl.simpleMode = false;

              // when
              var visible = ctrl.isAddButtonVisible();

              // then
              expect(visible).toBe(false);
            });

            it('should not be visible with full clipboard', function () {
              // given
              ctrl.editMode = true;
              $rootScope.widgetClipboard = {};
              ctrl.widgets = [{model: {}}];
              ctrl.hideSlotPlaceholder = false;
              ctrl.simpleMode = false;

              // when
              var visible = ctrl.isAddButtonVisible();

              // then
              expect(visible).toBe(false);
            });

            it('should not be visible in simple mode', function () {
              // given
              ctrl.editMode = true;
              $rootScope.widgetClipboard = undefined;
              ctrl.widgets = [{model: {}}];
              ctrl.hideSlotPlaceholder = false;
              ctrl.simpleMode = true;

              // when
              var visible = ctrl.isAddButtonVisible();

              // then
              expect(visible).toBe(false);
            });

          });

          describe('paste button', function () {

            it('should be visible', function () {
              // given
              ctrl.editMode = true;
              $rootScope.widgetClipboard = {};
              ctrl.widgets = [{model: {}}];
              ctrl.renderStyle = 'plain';

              // when
              var visible = ctrl.isPasteButtonVisible();

              // then
              expect(visible).toBe(true);
            });

            it('should be visible without widgets without panel', function () {
              // given
              ctrl.editMode = true;
              $rootScope.widgetClipboard = {};
              ctrl.widgets = [];
              ctrl.renderStyle = 'plain';

              // when
              var visible = ctrl.isPasteButtonVisible();

              // then
              expect(visible).toBe(true);
            });

            it('should be visible with widgets with panel', function () {
              // given
              ctrl.editMode = true;
              $rootScope.widgetClipboard = {};
              ctrl.widgets = [{model: {}}];
              ctrl.renderStyle = 'panel';

              // when
              var visible = ctrl.isPasteButtonVisible();

              // then
              expect(visible).toBe(true);
            });

            it('should not be visible without edit mode', function () {
              // given
              ctrl.editMode = false;
              $rootScope.widgetClipboard = {};
              ctrl.widgets = [{model: {}}];
              ctrl.renderStyle = 'plain';

              // when
              var visible = ctrl.isPasteButtonVisible();

              // then
              expect(visible).toBe(false);
            });

            it('should not be visible without clipboard', function () {
              // given
              ctrl.editMode = true;
              $rootScope.widgetClipboard = undefined;
              ctrl.widgets = [{model: {}}];
              ctrl.renderStyle = 'plain';

              // when
              var visible = ctrl.isPasteButtonVisible();

              // then
              expect(visible).toBe(false);
            });
          });
        });

        describe('options divider', function () {
          var ctrl;

          beforeEach(function () {
            ctrl = buildController();
          });

          it('should be visible', function () {
            // given
            ctrl.widgets = [];
            ctrl.hideSlotPlaceholder = false;
            ctrl.editMode = true;
            ctrl.simpleMode = false;

            // when
            var visible = ctrl.showOptionsDivider();

            // then
            expect(visible).toBe(true);
          });

          it('should not be visible with widgets if controls are not hidden', function () {
            // given
            ctrl.widgets = [{model: {}}];
            ctrl.hideSlotPlaceholder = false;
            ctrl.editMode = false;
            ctrl.simpleMode = false;

            // when
            var visible = ctrl.showOptionsDivider();

            // then
            expect(visible).toBe(false);
          });

          it('should not be visible without widgets if controls are hidden', function () {
            // given
            ctrl.widgets = [];
            ctrl.hideSlotPlaceholder = false;
            ctrl.editMode = false;
            ctrl.simpleMode = false;

            // when
            var visible = ctrl.showOptionsDivider();

            // then
            expect(visible).toBe(false);
          });

          it('should not be visible without widgets if placeholders are hidden', function () {
            // given
            ctrl.widgets = [];
            ctrl.hideSlotPlaceholder = true;
            ctrl.editMode = false;
            ctrl.simpleMode = false;

            // when
            var visible = ctrl.showOptionsDivider();

            // then
            expect(visible).toBe(false);
          });

          it('should not be visible in simple mode', function () {
            // given
            ctrl.widgets = [];
            ctrl.hideSlotPlaceholder = false;
            ctrl.editMode = true;
            ctrl.simpleMode = true;

            // when
            var visible = ctrl.showOptionsDivider();

            // then
            expect(visible).toBe(false);
          });

        });

        describe('panel', function () {
          var ctrl;

          beforeEach(function () {
            ctrl = buildController();
          });

          describe('hasPanel', function () {
            it('should return false if render style is "panel"', function () {
              // given
              ctrl.renderStyle = 'panel';
              var widget = {};

              // when
              var result = ctrl.hasPanel(widget);

              // then
              expect(result).toBe(false);
            });

            it('should return false if render style is "plain"', function () {
              // given
              ctrl.renderStyle = 'plain';
              var widget = {};

              // when
              var result = ctrl.hasPanel(widget);

              // then
              expect(result).toBe(false);
            });

            it('should return true if render style is "panels"', function () {
              // given
              ctrl.renderStyle = 'panels';
              var widget = {};

              // when
              var result = ctrl.hasPanel(widget);

              // then
              expect(result).toBe(true);
            });

            it('should return false if render style is "panels" and render options disable panel', function () {
              // given
              ctrl.renderStyle = 'panels';
              var widget = {};
              _.set(widget, 'config.renderOptions.panels.noPanel', true);

              // when
              var result = ctrl.hasPanel(widget);

              // then
              expect(result).toBe(false);
            });
          });

          describe('hasPanelBody', function () {
            it('should return true if render style is "panel', function () {
              // given
              ctrl.renderStyle = 'panel';
              var widget = {};

              // when
              var result = ctrl.hasPanelBody(widget);

              // then
              expect(result).toBe(true);
            });

            it('should return false if render style is "plain"', function () {
              // given
              ctrl.renderStyle = 'plain';
              var widget = {};

              // when
              var result = ctrl.hasPanelBody(widget);

              // then
              expect(result).toBe(false);
            });

            it('should return true if render style is "panels', function () {
              // given
              ctrl.renderStyle = 'panels';
              var widget = {};

              // when
              var result = ctrl.hasPanelBody(widget);

              // then
              expect(result).toBe(true);
            });

            it('should return false if render style is "panels" and render options disable panel', function () {
              // given
              ctrl.renderStyle = 'panels';
              var widget = {};
              _.set(widget, 'config.renderOptions.panels.noPanel', true);

              // when
              var result = ctrl.hasPanelBody(widget);

              // then
              expect(result).toBe(false);
            });
          });

        });

        describe('events', function () {

          var ctrl;

          beforeEach(function () {
            ctrl = buildController();
          });

          it('should enable edit mode if listening to global events and event is triggered', function () {
            // given
            ctrl.editMode = false;
            ctrl.globalEvents = true;

            // when
            $rootScope.$broadcast('widget-slot:edit', true);
            $scope.$apply();

            // then
            expect(ctrl.editMode).toBe(true);
          });

          it('should not enable edit mode if only listening to local events and global event is triggered', function () {
            // given
            ctrl.editMode = false;
            ctrl.globalEvents = false;

            // when
            $rootScope.$broadcast('widget-slot:edit', true);
            $scope.$apply();

            // then
            expect(ctrl.editMode).toBe(false);
          });

          it('should cancel if listening to global events and event is triggered', function () {
            // given
            ctrl.editMode = true;
            ctrl.globalEvents = true;

            // when
            $rootScope.$broadcast('widget-slot:cancel', true);
            $scope.$apply();

            // then
            expect(ctrl.editMode).toBe(false);
          });

          it('should not cancel if only listening to local events and global event is triggered', function () {
            // given
            ctrl.editMode = true;
            ctrl.globalEvents = false;

            // when
            $rootScope.$broadcast('widget-slot:cancel', true);
            $scope.$apply();

            // then
            expect(ctrl.editMode).toBe(true);
          });

          it('should save if listening to global events and event is triggered', function () {
            // given
            ctrl.editMode = true;
            ctrl.globalEvents = true;

            // when
            $rootScope.$broadcast('widget-slot:save', [], true);
            $scope.$apply();
            $timeout.flush();

            // then
            expect(ctrl.editMode).toBe(false);
          });

          it('should not save if only listening to local events and global event is triggered', function () {
            // given
            ctrl.editMode = true;
            ctrl.globalEvents = false;

            // when
            $rootScope.$broadcast('widget-slot:save', [], true);
            $scope.$apply();

            // then
            expect(ctrl.editMode).toBe(true);
          });
        });
      });
    });
  });
})();
