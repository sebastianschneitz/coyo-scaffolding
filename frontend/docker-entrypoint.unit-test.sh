#!/bin/bash

set -e;

cd /coyo

# retry flaky yarn install

for i in $(seq 1 5); do ./gradlew coyo-frontend:ngx:yarn_install && s=0 && break || s=$? && echo \"Retrying...\"; sleep 1; done; (exit $s)

./gradlew coyo-frontend:ngx:yarn_test -x coyo-frontend:ngx:yarn_install
