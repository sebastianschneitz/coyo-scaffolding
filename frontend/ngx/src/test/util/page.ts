import {ComponentFixture} from '@angular/core/testing';

/**
 * A test page class that handles access to component properties and encapsulates the logic that sets them.
 */
export abstract class Page<T> {

  constructor(private fixture: ComponentFixture<T>) {
  }

  /**
   * Searches for an element
   *
   * @param selector
   * The query selector
   *
   * @return the element
   */
  protected query<S>(selector: string): S {
    return this.fixture.nativeElement.querySelector(selector);
  }

  /**
   * Searches for all element of the given page
   *
   * @param selector
   * The query selector
   *
   * @return an Array of found elements
   */
  protected queryAll<S>(selector: string): S[] {
    return this.fixture.nativeElement.querySelectorAll(selector);
  }
}
