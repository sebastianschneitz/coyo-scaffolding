import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {EventCreateComponent} from '@app/events/event-create/event-create.component';

getAngularJSGlobal()
  .module('coyo.events')
  .directive('coyoEventCreate', downgradeComponent({
    component: EventCreateComponent
  }));
