import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {SenderEvent} from '@domain/event/SenderEvent';
import * as moment from 'moment';

/**
 * Event date label with image component
 */
@Component({
  selector: 'coyo-event-date-label',
  templateUrl: './event-date-label.component.html',
  styleUrls: ['./event-date-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventDateLabelComponent implements OnInit {

  /**
   * The event to be shown.
   */
  @Input() event: SenderEvent;
  timeFormat: { short: string, long: string };
  startDatePattern: string;
  endDatePattern: string;
  startDate: any;
  endDate: any;

  constructor() {
  }

  ngOnInit(): void {
    this.timeFormat = {short: 'h:mm a', long: 'MMMM Do YYYY'};
    const fullDay = this.event.fullDay;
    this.startDatePattern = fullDay ? '' : this.timeFormat.short;
    this.endDatePattern = fullDay ? '' : this.timeFormat.short;
    this.startDate = moment(this.event.startDate);
    this.endDate = moment(this.event.endDate);
    if (!this.startDate.isSame(this.endDate, 'day')) {
      this.endDatePattern = this.timeFormat.long + (fullDay ? '' : ',') + this.endDatePattern;
    }
  }

}
