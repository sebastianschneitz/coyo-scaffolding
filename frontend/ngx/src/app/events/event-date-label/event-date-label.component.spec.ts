import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SenderEvent} from '@domain/event/SenderEvent';
import * as moment from 'moment';
import {Moment} from 'moment';
import {MomentModule} from 'ngx-moment';
import {EventDateLabelComponent} from './event-date-label.component';

describe('EventDateLabelComponent', () => {
  let component: EventDateLabelComponent;
  let fixture: ComponentFixture<EventDateLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventDateLabelComponent],
      imports: [MomentModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    const start = date2Str(moment());
    const end = date2Str(moment());
    fixture = TestBed.createComponent(EventDateLabelComponent);
    component = fixture.componentInstance;
    component.event = {
      startDate: new Date(start.toString()),
      endDate: new Date(end.toString()),
      fullDay: false
    } as SenderEvent;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should format to short format if the event ends on the same day', () => {
    // given / when
    component.ngOnInit();

    // then
    expect(component.startDatePattern).toEqual(component.timeFormat.short);
    expect(component.endDatePattern).toEqual(component.timeFormat.short);
  });

  it('should format to long format plus short format if the event does not end on the same day', () => {
    // given
    const start = moment();
    const end = moment().add(1, 'days');
    component.event.startDate = new Date(start.toString());
    component.event.startDate = new Date(end.toString());

    // given / when
    component.ngOnInit();

    // then
    expect(component.startDatePattern).toEqual(component.timeFormat.short);
    expect(component.endDatePattern).toEqual(component.timeFormat.long + ',' + component.timeFormat.short);
  });

  function date2Str(date: Moment): string {
    return date.format('YYYY-MM-DDTHH:mm:ss');
  }
});
