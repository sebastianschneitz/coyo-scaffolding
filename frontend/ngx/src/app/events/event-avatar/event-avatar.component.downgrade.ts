import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {EventAvatarComponent} from '@app/events/event-avatar/event-avatar.component';

getAngularJSGlobal()
  .module('coyo.events')
  .directive('coyoEventAvatar', downgradeComponent({
    component: EventAvatarComponent,
    propagateDigest: false
  }));
