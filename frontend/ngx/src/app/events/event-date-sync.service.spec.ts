import {TestBed} from '@angular/core/testing';
import {EventDateSyncService} from '@app/events/event-date-sync.service';
import {EventDates} from '@app/events/event-dates';

describe('EventDateSyncService', () => {
  let eventDateSyncService: jasmine.SpyObj<EventDateSyncService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [EventDateSyncService]
    });

    eventDateSyncService = TestBed.get(EventDateSyncService);
  });

  it('should be created', () => {
    expect(eventDateSyncService).toBeTruthy();
  });

  it('should init offset', () => {
    // given
    const event: EventDates = {
      startDate: new Date('2013-01-06T12:00:00'),
      startTime: new Date('2013-01-06T12:00:00'),
      endDate: new Date('2013-01-06T13:00:00'),
      endTime: new Date('2013-01-06T13:00:00')
    };

    // when
    eventDateSyncService.updateWithStartDate(event);

    // then
    expect(event.startDate.toISOString()).toBe(new Date('2013-01-06T12:00:00').toISOString());
    expect(event.startTime.toISOString()).toBe(new Date('2013-01-06T12:00:00').toISOString());
    expect(event.endDate.toISOString()).toBe(new Date('2013-01-06T13:00:00').toISOString());
    expect(event.endTime.toISOString()).toBe(new Date('2013-01-06T13:00:00').toISOString());
    expect(event.offset).toBe(1000 * 60 * 60);
  });

  it('should calculate the time difference in milliseconds', () => {
    // given
    const dateA: Date = new Date('1988-10-18T12:00:00');
    const dateB: Date = new Date('1988-10-18T13:45:00');
    const expected: number = 1000 * 60 * 60 * 1.75; // 1 hour and 45 minutes

    // when
    const result = eventDateSyncService.differenceInMilliseconds(dateA, dateB);

    // then
    expect(result).toEqual(expected);
  });

  it('should skip processing if a date is invalid', () => {
    // given
    const invalidDate = new Date('Invalid date');
    const event = {
      startDate: new Date('2012-01-01T12:00:00'),
      startTime: new Date('2012-01-01T12:00:00'),
      endDate: invalidDate,
      endTime: new Date('2012-01-08T12:00:00')
    };

    // when
    eventDateSyncService.updateWithEndDate(event);

    // then
    expect(event.startDate.toISOString()).toBe(new Date('2012-01-01T12:00:00').toISOString());
    expect(event.startTime.toISOString()).toBe(new Date('2012-01-01T12:00:00').toISOString());
    expect(event.endDate).toBe(invalidDate);
    expect(event.endTime.toISOString()).toBe(new Date('2012-01-08T12:00:00').toISOString());
  });

  describe('Changing the date', () => {

    it('should not update the end date if not necessary', () => {
      // given
      const event = {
        startDate: new Date('2013-01-01T12:00:00'),
        startTime: new Date('2013-01-01T12:00:00'),
        endDate: new Date('2013-01-08T12:00:00'),
        endTime: new Date('2013-01-08T12:00:00')
      };

      // when
      event.startDate = new Date('2013-01-02T12:00:00');
      eventDateSyncService.updateWithStartDate(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-08T12:00:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-08T12:00:00').toISOString());
    });

    it('should not update the start date if not necessary', () => {
      // given
      const event = {
        startDate: new Date('2013-01-01T12:00:00'),
        startTime: new Date('2013-01-01T12:00:00'),
        endDate: new Date('2013-01-08T12:00:00'),
        endTime: new Date('2013-01-08T12:00:00')
      };

      // when
      event.endDate = new Date('2013-01-02T12:00:00');
      eventDateSyncService.updateWithEndDate(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T12:00:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T12:00:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
    });

    it('should update the end date if start date is after the end date', () => {
      // given
      const event = {
        startDate: new Date('2013-01-01T12:00:00'),
        startTime: new Date('2013-01-01T12:00:00'),
        endDate: new Date('2013-01-02T12:00:00'),
        endTime: new Date('2013-01-02T12:00:00')
      };

      // when
      event.startDate = new Date('2013-01-03T12:00:00');
      eventDateSyncService.updateWithStartDate(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-03T12:00:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-03T12:00:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-03T13:00:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-03T13:00:00').toISOString());
    });

    it('should update the start date if end date is before the start date', () => {
      // given
      const event = {
        startDate: new Date('2013-01-02T12:00:00'),
        startTime: new Date('2013-01-02T12:00:00'),
        endDate: new Date('2013-01-02T13:00:00'),
        endTime: new Date('2013-01-02T13:00:00')
      };

      // when
      event.endDate = new Date('2013-01-01T13:00:00');
      eventDateSyncService.updateWithEndDate(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T12:00:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T12:00:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-01T13:00:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-01T13:00:00').toISOString());
    });
  });

  describe('Changing the date time', () => {

    it('should update the end date time if start date time is changed', () => {
      // given
      const event = {
        startDate: new Date('2013-01-02T12:00:00'),
        startTime: new Date('2013-01-02T12:00:00'),
        endDate: new Date('2013-01-02T13:00:00'),
        endTime: new Date('2013-01-02T13:00:00')
      };

      // when
      event.startTime = new Date('2013-01-02T14:30:00');
      eventDateSyncService.updateWithStartTime(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-02T14:30:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-02T14:30:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-02T15:30:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-02T15:30:00').toISOString());
    });

    it('should not update the end date time if not necessary', () => {
      // given
      const event = {
        startDate: new Date('2013-01-01T12:00:00'),
        startTime: new Date('2013-01-01T12:00:00'),
        endDate: new Date('2013-01-02T12:00:00'),
        endTime: new Date('2013-01-02T12:00:00')
      };

      // when
      event.startTime = new Date('2013-01-01T14:00:00');
      eventDateSyncService.updateWithStartTime(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T14:00:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T14:00:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
    });

    it('should update the end date time with custom offset', () => {
      // given
      const event = {
        startDate: new Date('2013-01-01T12:00:00'),
        startTime: new Date('2013-01-01T12:00:00'),
        endDate: new Date('2013-01-01T16:00:00'),
        endTime: new Date('2013-01-01T16:00:00'),
      };
      eventDateSyncService.updateWithEndTime(event);

      // when
      event.startTime = new Date('2013-01-01T14:30:00');
      eventDateSyncService.updateWithStartTime(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T14:30:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T14:30:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-01T18:30:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-01T18:30:00').toISOString());
    });

    it('should update the end date time with set custom offset', () => {
      // given
      const fourHours = 1000 * 60 * 60 * 4;
      const event = {
        startDate: new Date('2001-01-01T12:00:00'),
        startTime: new Date('2001-01-01T12:00:00'),
        endDate: new Date('2001-01-01T16:00:00'),
        endTime: new Date('2001-01-01T16:00:00'),
        offset: fourHours
      };

      // when
      event.startTime = new Date('2001-01-01T14:30:00');
      eventDateSyncService.updateWithStartTime(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2001-01-01T14:30:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2001-01-01T14:30:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2001-01-01T18:30:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2001-01-01T18:30:00').toISOString());
    });

    it('should update the end date time if start date time is after the end date time', () => {
      // given
      const event = {
        startDate: new Date('2013-01-02T12:00:00'),
        startTime: new Date('2013-01-02T12:00:00'),
        endDate: new Date('2013-01-02T13:00:00'),
        endTime: new Date('2013-01-02T13:00:00')
      };

      // when
      event.startTime = new Date('2013-01-02T13:37:00');
      eventDateSyncService.updateWithStartTime(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-02T13:37:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-02T13:37:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-02T14:37:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-02T14:37:00').toISOString());
    });

    it('should update the start time if end time is before the start time', () => {
      // given
      const event = {
        startDate: new Date('2013-01-01T12:00:00'),
        startTime: new Date('2013-01-01T12:00:00'),
        endDate: new Date('2013-01-01T13:00:00'),
        endTime: new Date('2013-01-01T13:00:00')
      };

      // when
      event.endTime = new Date('2013-01-01T08:45:00');
      eventDateSyncService.updateWithEndTime(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T07:45:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T07:45:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-01T08:45:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-01T08:45:00').toISOString());
    });

    it('should update the start time if end date changes and times becomes invalid', () => {
      // given
      const event = {
        startDate: new Date('2013-01-01T12:00:00'),
        startTime: new Date('2013-01-01T12:00:00'),
        endDate: new Date('2013-01-02T08:00:00'),
        endTime: new Date('2013-01-02T08:00:00')
      };

      // when
      event.endDate = new Date('2013-01-01T08:00:00');
      eventDateSyncService.updateWithEndDate(event);

      // then
      expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T07:00:00').toISOString());
      expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T07:00:00').toISOString());
      expect(event.endDate.toISOString()).toBe(new Date('2013-01-01T08:00:00').toISOString());
      expect(event.endTime.toISOString()).toBe(new Date('2013-01-01T08:00:00').toISOString());
    });

  });

});
