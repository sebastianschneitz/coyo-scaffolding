import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {EventCreateMenuComponent} from '@app/events/event-create-menu/event-create-menu.component';

getAngularJSGlobal()
  .module('coyo.events')
  .directive('coyoEventCreateMenu', downgradeComponent({
    component: EventCreateMenuComponent
  }));
