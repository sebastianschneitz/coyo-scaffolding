import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EventVisibility} from '@app/events/event-create-menu/event-visibility';
import {NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import {EventCreateMenuComponent} from './event-create-menu.component';

describe('EventCreateMenuComponent', () => {
  let component: EventCreateMenuComponent;
  let fixture: ComponentFixture<EventCreateMenuComponent>;
  let stateService: jasmine.SpyObj<IStateService>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [EventCreateMenuComponent],
        providers: [{
          provide: NG1_STATE_SERVICE,
          useValue: jasmine.createSpyObj('stateService', ['go'])
        }, ChangeDetectorRef]
      })
      .overrideTemplate(EventCreateMenuComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCreateMenuComponent);
    component = fixture.componentInstance;
    stateService = TestBed.get(NG1_STATE_SERVICE);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect to create public event', () => {
    // given
    const visibility: EventVisibility = {name: 'PUBLIC', icon: 'globe'};

    // when
    component.redirectToEventCreate(visibility);

    // then
    expect(stateService.go).toHaveBeenCalledWith('main.event.create', {public: true});
  });

  it('should redirect to create private event', () => {
    // given
    const visibility: EventVisibility = {name: 'PRIVATE', icon: 'lock'};

    // when
    component.redirectToEventCreate(visibility);

    // then
    expect(stateService.go).toHaveBeenCalledWith('main.event.create', {public: false});
  });
});
