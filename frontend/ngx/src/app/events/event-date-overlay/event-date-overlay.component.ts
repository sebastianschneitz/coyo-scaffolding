import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

/**
 * Component for showing an date overlay above the parent component
 */
@Component({
  selector: 'coyo-event-date-overlay',
  templateUrl: './event-date-overlay.component.html',
  styleUrls: ['./event-date-overlay.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventDateOverlayComponent {

  /**
   * The date to display
   */
  @Input() date: Date;

  constructor() { }
}
