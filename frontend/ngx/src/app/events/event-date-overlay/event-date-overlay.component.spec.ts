import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EventDateOverlayComponent} from './event-date-overlay.component';

describe('EventDateOverlayComponent', () => {
  let component: EventDateOverlayComponent;
  let fixture: ComponentFixture<EventDateOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventDateOverlayComponent]
    }).overrideTemplate(EventDateOverlayComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDateOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
