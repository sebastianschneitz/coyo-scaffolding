/**
 * The date sync object for events.
 */
export interface EventDates {
  startDate: Date;
  startTime: Date;
  endDate: Date;
  endTime: Date;
  offset?: number;
}
