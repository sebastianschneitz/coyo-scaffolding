import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {TimeService} from '@shared/time/time/time.service';
import {TokenService} from './token.service';

describe('TokenService', () => {
  let service: TokenService;
  let httpMock: HttpTestingController;
  let timeService: jasmine.SpyObj<TimeService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{
        provide: TimeService, useValue: jasmine.createSpyObj('timeService', ['getCurrentTimeMillis'])
      }]
    });

    service = TestBed.get(TokenService);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpMock = TestBed.get(HttpTestingController);
    timeService = TestBed.get(TimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get a token', () => {
    // given
    const token = 'example token';

    // when
    const result = service.getToken();

    // then
    result.subscribe(resultToken => expect(resultToken).toBe(token));
    const request = httpMock.expectOne('/web/authorization/token');
    request.flush({token: token});
    httpMock.verify();
  });

  it('should cache the token', () => {
    // given
    timeService.getCurrentTimeMillis.and.returnValues(0, 0, TokenService.INVALIDATE_TIME - 1);
    const token = 'example token';

    // when
    const first = service.getToken();
    const second = service.getToken();

    // then
    first.subscribe(result => expect(result).toBe(token));
    second.subscribe(result => expect(result).toBe(token));
    const request = httpMock.expectOne('/web/authorization/token');
    request.flush({token: token});
    httpMock.verify();
  });

  it('should request token from backend when forced', () => {
    // given
    const token = 'example token';
    const token2 = 'example token 2';

    // when
    const first = service.getToken();
    const second = service.getToken(true);

    // then
    first.subscribe(result => expect(result).toBe(token));
    second.subscribe(result => expect(result).toBe(token2));
    const requests = httpMock.match('/web/authorization/token');
    expect(requests.length).toBe(2);
    requests[0].flush({token: token});
    requests[1].flush({token: token2});
    httpMock.verify();
  });

  it('should reload the token from backend after ' + TokenService.INVALIDATE_TIME + 'ms', () => {
    // given
    const token = 'example token';
    const token2 = 'example token 2';
    timeService.getCurrentTimeMillis.and.returnValue(0);

    // when
    const first = service.getToken();
    first.subscribe(result => expect(result).toBe(token));
    const request = httpMock.expectOne('/web/authorization/token');
    request.flush({token: token});

    timeService.getCurrentTimeMillis.and.returnValue(TokenService.INVALIDATE_TIME + 1);

    const second = service.getToken();

    // then
    second.subscribe(result => expect(result).toBe(token2));
    const request2 = httpMock.expectOne('/web/authorization/token');
    request2.flush({token: token2});
    httpMock.verify();
  });
});
