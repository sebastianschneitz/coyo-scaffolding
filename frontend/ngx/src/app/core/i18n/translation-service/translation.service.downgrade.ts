import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {TranslationService} from './translation.service';

getAngularJSGlobal()
  .module('commons.i18n')
  .factory('translationService', downgradeInjectable(TranslationService) as any);
