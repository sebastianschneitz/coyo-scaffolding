import {Inject, Injectable, NgZone} from '@angular/core';
import {User} from '@domain/user/user';
import {Ng1AuthService} from '@root/typings';
import {attachToZone} from '@upgrade/attach-to-zone';
import {NG1_AUTH_SERVICE} from '@upgrade/upgrade.module';
import {from, merge, Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';

/**
 * Service for authentication and retrieving authentication information.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(@Inject(NgZone) private ngZone: NgZone,
              @Inject(NG1_AUTH_SERVICE) private authService: Ng1AuthService) {
  }

  /**
   * Returns the current user and caches the current user during application lifetime.
   *
   * @param forceRefresh force a refresh of the user cache
   * @returns an `Observable` containing the current user
   */
  getUser(forceRefresh: boolean = false): Observable<User> {
    const observable = from(this.authService.getUser(forceRefresh));
    return attachToZone(this.ngZone, observable);
  }

  /**
   * Returns the current user.
   *
   * The `Observable` provides a continues stream of the current user and will
   * not complete after the initial load. Subsequent changes to the user will
   * be delivered.
   *
   * @returns an `Observable` holding the current user.
   */
  getUser$(): Observable<User> {
    return merge(
      this.getUser(),
      this.authService.userUpdated$
        .pipe(map(([_$event, newUser, _oldUser]) => newUser))
    );
  }

  /**
   * Returns the user's authentication state.
   *
   * @returns the authentication state
   */
  isAuthenticated(): boolean {
    return this.authService.isAuthenticated();
  }

  /**
   * Returns an observable as a stream that triggers true or false whenever
   * a user logs in or out.
   *
   * @returns an `Observable` that triggers true when an authentication happened,
   * otherwise false.
   */
  isAuthenticated$(): Observable<boolean> {
    return merge(
      of(this.isAuthenticated()),
      this.authService.userLoggedIn$.pipe(map(() => true)),
      this.authService.userLoggedOut$.pipe(map(() => false))
    );
  }

  /**
   * Returns if the user can use hashtags.
   *
   * @returns true if the user can use hashtags, false else
   */
  canUseHashtags(): boolean {
    return this.authService.canUseHashtags();
  }

  /**
   * Gets the current user id or null if no user is authenticated
   *
   * @returns the id of the current authenticated user or null if no user is authenticated
   */
  getCurrentUserId(): string | null {
    return this.authService.getCurrentUserId();
  }
}
