/**
 * The position of a caret in an input or textarea.
 */
export interface Caret {
  top: number;
  left: number;
  height: number;
}
