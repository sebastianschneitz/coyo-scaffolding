import {TestBed} from '@angular/core/testing';
import {WINDOW} from '@root/injection-tokens';
import {NG1_LOCAL_STORAGE} from '@upgrade/upgrade.module';
import {LocalStorageService} from './local-storage.service';

describe('LocalStorageService', () => {
  let service: LocalStorageService;
  let localStorage: jasmine.SpyObj<Storage>;
  let $localStorage: jasmine.SpyObj<{ $sync(): void }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalStorageService, {
        provide: WINDOW,
        useValue: {localStorage: jasmine.createSpyObj('localStorage', ['setItem', 'getItem'])}
      }, {
        provide: NG1_LOCAL_STORAGE,
        useValue: jasmine.createSpyObj('localstorage', ['$sync'])
      }]
    });

    service = TestBed.get(LocalStorageService);

    $localStorage = TestBed.get(NG1_LOCAL_STORAGE);
    localStorage = TestBed.get(WINDOW).localStorage;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set an item to the local storage', () => {
    // when
    service.setValue('test', 'test-value');

    // then
    expect(localStorage.setItem).toHaveBeenCalledWith('ngStorage-test', '"test-value"');
    expect($localStorage.$sync).toHaveBeenCalled();
  });

  it('should get an item from the local storage', () => {
    localStorage.getItem.and.returnValue('"test-value"');

    // when
    const result = service.getValue<string>('test', 'test-fallback');

    // then
    expect(result).toBe('test-value');
    expect(localStorage.getItem).toHaveBeenCalledWith('ngStorage-test');
  });

  it('should get the fallback value for a missing item', () => {
    localStorage.getItem.and.returnValue(null);

    // when
    const result = service.getValue<string>('test', 'test-fallback');

    // then
    expect(result).toBe('test-fallback');
    expect(localStorage.getItem).toHaveBeenCalledWith('ngStorage-test');
  });
});
