import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {App} from '@domain/apps/app';
import {DomainService} from '@domain/domain/domain.service';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {SearchRequest} from '@domain/search/SearchRequest';
import {Sender} from '@domain/sender/sender';
import {Ng1AppService} from '@root/typings';
import {NG1_APP_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import * as _ from 'lodash';
import {EMPTY, Observable} from 'rxjs';

/**
 * Service for all request methods to the sender domain.
 */
@Injectable({
  providedIn: 'root'
})
export class SenderService extends DomainService<Sender, Sender> {

  constructor(@Inject(NG1_STATE_SERVICE) private $state: IStateService,
              @Inject(NG1_APP_SERVICE) private appService: Ng1AppService,
              @Inject(HttpClient) protected http: HttpClient,
              @Inject(UrlService) protected urlService: UrlService) {
    super(http, urlService);
  }

  protected getBaseUrl(): string {
    return '/web/senders';
  }

  /**
   * Retrieves the current sender ID or slug defined in the state.
   *
   * @return the ID or slug
   */
  getCurrentIdOrSlug(): string {
    const senderParam = _.get(this.$state.current, 'data.senderParam');
    return this.$state.params[senderParam];
  }

  /**
   * Retrieves the current app.
   *
   * @param senderId the ID of the sender
   * @return an `Observable` containing the corresponding app or just completes
   * if there is no app context
   */
  getCurrentApp(senderId: string): Observable<App> {
    const appId = this.appService.getCurrentAppIdOrSlug();
    return appId && senderId
      ? this.http.get<App>(this.getUrl({senderId, appId}, '/{senderId}/apps/{appId}'))
      : EMPTY;
  }

  /**
   * Retrieves a page of all senders the current user can use as functional
   * user in the current context.
   *
   * @param pageable the pagination information
   * @param id the ID of the context object (e.g. a timeline item ID or the ID of the sender of the timeline)
   * @param type the type of the context ('timeline-item' or 'sender')
   * @return the observable of the page
   */
  getActableSenders(pageable: Pageable, id: string, type: 'timeline-item' | 'sender'): Observable<Page<Sender>> {
    const searchRequest = new SearchRequest('', ['displayName'], {type: ['page', 'workspace', 'event']});
    return this.getPage(pageable, {
      path: 'search/actable-senders',
      params: searchRequest.toHttpParams().append('id', id).append('type', type)
    });
  }
}
