/**
 * Data structure for a normal timeline item
 */
import {BlogArticle} from '@domain/blog-article/blog-article';

export interface TimelineItemData {
  message: string;
  edited?: boolean;
  article?: BlogArticle;
}
