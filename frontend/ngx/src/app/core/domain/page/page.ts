import {App} from '@domain/apps/app';
import {BaseModel} from '@domain/base-model/base-model';
import {PageCategory} from '@domain/page/page-category';

/**
 * Page entity model
 */
export interface Page extends BaseModel {
  name: string;
  description: string;
  categories: PageCategory;
  visibility: 'PUBLIC' | 'PRIVATE';
  topApps: App[];
  autoSubscribe: boolean;
  autoSubscribeType: 'NONE' | 'SELECTED' | 'ALL';
  autoSubscribeUserIds: string[];
  autoSubscribeGroupIds: string[];
  adminIds: string[];
  userSubscriptionCount: number;
}
