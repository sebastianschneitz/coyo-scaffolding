import {BaseModel} from '@domain/base-model/base-model';

/**
 * Entity model for the category of a page
 */
export interface PageCategory extends BaseModel {
  name: string;
}
