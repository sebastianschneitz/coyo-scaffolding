import {Page} from './page';

/**
 * A rich wrapper for a page of type `T`
 * with additional data of type `R`.
 */
export interface RichPage<T, R> {

  /**
   * The page of type `T`.
   */
  page: Page<T>;

  /**
   * The additional data of type `R`.
   */
  data: R;
}
