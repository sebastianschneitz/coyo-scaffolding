/**
 * Base class for all entities in COYO providing a UUID and creation / modification timestamps.
 */
export interface BaseModel {
  id: string;
  created: number;
  modified: number;
}
