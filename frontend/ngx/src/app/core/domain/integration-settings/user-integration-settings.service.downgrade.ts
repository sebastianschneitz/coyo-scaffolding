import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {UserIntegrationSettingsService} from '@domain/integration-settings/user-integration-settings.service';

getAngularJSGlobal()
  .module('coyo.account')
  .factory('ngxUserIntegrationSettingsService', downgradeInjectable(UserIntegrationSettingsService));
