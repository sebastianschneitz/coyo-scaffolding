import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {discardPeriodicTasks, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {Pageable} from '@domain/pagination/pageable';
import {SenderService} from '@domain/sender/sender/sender.service';
import {TargetService} from '@domain/sender/target/target.service';
import {SubscriptionService} from '@domain/subscription/subscription.service';
import {User} from '@domain/user/user';
import {UserService} from '@domain/user/user.service';
import {SublineService} from '@shared/sender-ui/subline/subline.service';
import {of} from 'rxjs';
import {MentionService} from './mention.service';

describe('MentionService', () => {
  let httpTestingController: HttpTestingController;
  let senderService: jasmine.SpyObj<SenderService>;
  let targetService: jasmine.SpyObj<TargetService>;
  let userService: jasmine.SpyObj<UserService>;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;
  let sublineService: jasmine.SpyObj<SublineService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        MentionService, {
          provide: SenderService,
          useValue: jasmine.createSpyObj('SenderService', ['getUrl'])
        }, {
          provide: TargetService,
          useValue: jasmine.createSpyObj('TargetService', ['canLinkTo', 'getLinkTo'])
        }, {
          provide: UserService,
          useValue: jasmine.createSpyObj('UserService', ['getPage'])
        }, {
          provide: SubscriptionService,
          useValue: jasmine.createSpyObj('SubscriptionService', ['getSubscribedUsers'])
        }, {
          provide: SublineService,
          useValue: jasmine.createSpyObj('SublineService', ['getSublineForUser'])
        }
      ]
    });

    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    senderService = TestBed.get(SenderService);
    targetService = TestBed.get(TargetService);
    userService = TestBed.get(UserService);
    subscriptionService = TestBed.get(SubscriptionService);
    sublineService = TestBed.get(SublineService);
  });

  beforeEach(() => {
    senderService.getUrl.and.returnValue('senderUrl');
  });

  it('should be created', inject([MentionService], (service: MentionService) => {
    expect(service).toBeTruthy();
  }));

  it('should getItems and return without data', inject([MentionService], (service: MentionService) => {
    // given
    subscriptionService.getSubscribedUsers.and.returnValue(of({content: []}));

    // when
    const result$ = service.getDetails();

    // then
    result$.subscribe(result => {
      expect(result).toEqual({});
      httpTestingController.verify();
    });
  }));

  it('should getItems and return mention items', inject([MentionService], (service: MentionService) => {
    // given
    const user = {
      color: 'color',
      imageUrls: {avatar: 'avatar'},
      slug: 'slug',
      displayName: 'displayName',
      displayNameInitials: 'displayNameInitials',
      externalWorkspaceMember: true
    } as User;

    subscriptionService.getSubscribedUsers.and.returnValue(of({content: [user]}));
    sublineService.getSublineForUser.and.returnValue(of('subline'));

    // when
    const result$ = service.getItems('', new Pageable(0, 10));

    // then
    result$.subscribe(result =>
      expect(result.content[0]).toEqual({
        user: user,
        subline: 'subline'
      })
    );
  }));

  it('should getItems and return subscribed users without search term', inject([MentionService], (service: MentionService) => {
    // given
    const term = '';
    const pageable = new Pageable(0, 10);
    const user = {imageUrls: {}} as User;

    subscriptionService.getSubscribedUsers.and.returnValue(of({content: [user]}));
    sublineService.getSublineForUser.and.returnValue(of('subline'));

    // when
    const result$ = service.getItems(term, pageable);

    // then
    result$.subscribe(_result => {
      expect(userService.getPage).not.toHaveBeenCalled();
      expect(sublineService.getSublineForUser).toHaveBeenCalledWith(user);
      expect(subscriptionService.getSubscribedUsers).toHaveBeenCalledWith(pageable);
    });
  }));

  it('should getItems and return searched users with search term', inject([MentionService], (service: MentionService) => {
    // given
    const term = 'term';
    const pageable = new Pageable(0, 10);
    const user = {imageUrls: {}} as User;

    userService.getPage.and.returnValue(of({content: [user]}));
    sublineService.getSublineForUser.and.returnValue(of('subline'));

    // when
    const result$ = service.getItems(term, pageable);

    // then
    result$.subscribe(_result => {
      expect(subscriptionService.getSubscribedUsers).not.toHaveBeenCalled();
      expect(sublineService.getSublineForUser).toHaveBeenCalledWith(user);
      expect(userService.getPage).toHaveBeenCalledWith(pageable, {
        params: {
          term,
          searchFields: 'displayName'
        }
      });
    });
  }));

  it('should getDetails and return immediately without slugs', inject([MentionService], (service: MentionService) => {
    // when
    const result$ = service.getDetails();

    // then
    result$.subscribe(result => {
      expect(result).toEqual({});
      httpTestingController.verify();
    });
  }));

  it('should getDetails and return mention details', fakeAsync(inject([MentionService], (service: MentionService) => {
    // given
    const slug = 'slug';

    targetService.canLinkTo.and.returnValue(of(true));
    targetService.getLinkTo.and.returnValue('link');

    // when
    const result$ = service.getDetails(slug);

    // then
    result$.subscribe(result => {
      expect(result.slug).toEqual(
        jasmine.objectContaining({
          slug: 'slug',
          name: 'displayName',
          link: 'link'
        })
      );
    });

    // verify request
    tick(MentionService.THROTTLE);
    const req = httpTestingController.expectOne(request => request.url === 'senderUrl');

    expect(req.request.method).toEqual('GET');
    expect(req.request.params.getAll('slug')).toEqual([slug]);
    req.flush({
      slug: {
        slug: 'slug',
        displayName: 'displayName',
        target: {}
      }
    });

    // when - again (cached)
    const result2$ = service.getDetails(slug);
    result2$.subscribe(result => {
      expect(result.slug).toEqual(
        jasmine.objectContaining({
          slug: 'slug',
          name: 'displayName',
          link: 'link'
        })
      );
    });
    tick(MentionService.THROTTLE);
    discardPeriodicTasks();
    httpTestingController.verify();
  })));

  it('should get details from the correct request', fakeAsync(inject([MentionService], (service: MentionService) => {
    // given
    targetService.canLinkTo.and.returnValue(of(true));
    targetService.getLinkTo.and.returnValue('link');
    const firstSlug = 'first';
    const secondSlug = 'second';
    const firstSender = {slug: firstSlug};
    const secondSender = {slug: secondSlug};
    const firstResponse = {} as any;
    firstResponse[firstSlug] = firstSender;

    const secondResponse = {} as any;
    secondResponse[secondSlug] = secondSender;

    // when
    const firstResult = service.getDetails(firstSlug);
    const secondResult = service.getDetails(secondSlug);
    secondResult.subscribe(result => expect(result[secondSlug].slug).toBe(secondSlug));
    firstResult.subscribe(result => expect(result[firstSlug].slug).toBe(firstSlug));
    tick(MentionService.THROTTLE);
    httpTestingController.expectOne(request => request.url === 'senderUrl');
    httpTestingController.verify();
    discardPeriodicTasks();
  })));
});
