/**
 * Represents a report.
 */
import {BaseModel} from '@domain/base-model/base-model';
import {Sender} from '@domain/sender/sender';

/**
 * Represents a report of content.
 */
export interface Report extends BaseModel {
  author: Sender;
  target: any;
  message: string;
  anonymous: boolean;
}
