import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Terms} from '@domain/terms/terms';
import {TermsService} from '@domain/terms/terms.service';
import * as _ from 'lodash';

describe('TermsService', () => {
  let httpTestingController: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TermsService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['join'])
      }]
    });
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  it('should be created', inject([TermsService], (service: TermsService) => {
    expect(service).toBeTruthy();
  }));

  it('should get best suitable terms', inject([TermsService], (service: TermsService) => {
    // given
    const terms: Terms = {
      title: 'Thou shall not pass!',
      text: '...until you sold your soul to me'
    };
    service.getBestSuitable()
      .subscribe(response => expect(response).toEqual(terms));

    // then
    const request = httpTestingController.expectOne(
      req => req.method === 'GET'
        && req.url === '/web/terms/suitable'
    );

    // Finally
    request.flush(terms);
    httpTestingController.verify();
  }));
});
