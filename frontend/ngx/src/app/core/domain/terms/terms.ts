// backend: TermsResponseBody
/**
 * Terms entity model
 */
export interface Terms {

  // id: string;
  // defaultLanguage: boolean;
  // language: string;
  // availableLanguages: string[];
  title: string;
  text: string;
  // url?: string;
}
