/**
 * Model for permissions
 */
export interface Permissions {
  [key: string]: boolean;
}
