/**
 * Model for social permissions.
 */
export interface SocialPermissions {
  commentsShown: boolean;
  commentsAndLikesNotAllowed: boolean;
  likesShown: boolean;
}
