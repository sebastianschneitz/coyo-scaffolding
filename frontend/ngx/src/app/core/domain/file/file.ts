import {StorageType} from '@domain/attachment/storage';
import {BaseModel} from '@domain/base-model/base-model';

/**
 * A file symbolizes a document or a folder.
 */
export interface File extends BaseModel {
  senderId: string;
  name: string;
  displayName: string;
  folder: string;
  appRoot: string;
  uploadFailed: boolean;
  contentType: string;
  extension: string;
  storage?: StorageType;
}
