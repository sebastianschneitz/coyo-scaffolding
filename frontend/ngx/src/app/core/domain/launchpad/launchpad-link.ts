import {BaseModel} from '@domain/base-model/base-model';

/**
 * Represents the links present inside the categories
 */
export interface LaunchpadLink extends BaseModel {
  url: string;
  name: string;
  ownerId: string;
  icon?: object;
}
