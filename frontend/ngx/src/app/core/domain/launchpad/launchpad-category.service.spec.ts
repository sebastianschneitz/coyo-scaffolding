import {HttpClientTestingModule} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import * as _ from 'lodash';
import {LaunchpadCategoryService} from './launchpad-category.service';

describe('LaunchpadCategoryService', () => {
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: UrlService, useValue: jasmine.createSpyObj('UrlService', ['join'])}
      ]
    });

    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  it('should be created', () => {
    const service: LaunchpadCategoryService = TestBed.get(LaunchpadCategoryService);
    expect(service).toBeTruthy();
  });

  it('should return the correct URL', inject([LaunchpadCategoryService], (service: LaunchpadCategoryService) => {
    // when
    const url = service.getUrl();

    // then
    expect(url).toEqual('/web/launchpad/categories');
  }));
});
