/**
 * The request object to create or update a launchpad link.
 */
export interface LaunchpadLinkRequest {
  url: string;
  name: string;
  newIconUid?: string;
  newIconContentType?: string;
}
