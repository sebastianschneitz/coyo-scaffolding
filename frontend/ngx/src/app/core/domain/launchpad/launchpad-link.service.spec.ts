import {HttpClientTestingModule} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import * as _ from 'lodash';
import {LaunchpadLinkService} from './launchpad-link.service';

describe('LaunchpadLinkService', () => {
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: UrlService, useValue: jasmine.createSpyObj('UrlService', ['join'])}
      ]
    });

    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  it('should be created', () => {
    const service: LaunchpadLinkService = TestBed.get(LaunchpadLinkService);
    expect(service).toBeTruthy();
  });

  it('should return the correct URL', inject([LaunchpadLinkService], (service: LaunchpadLinkService) => {
    // when
    const url = service.getUrl({
      categoryId: '123'
    });

    // then
    expect(url).toEqual('/web/launchpad/categories/123/links');
  }));
});
