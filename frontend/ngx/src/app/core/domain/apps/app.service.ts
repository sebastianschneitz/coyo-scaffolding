import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {App} from '@domain/apps/app';
import {DomainService} from '@domain/domain/domain.service';

/**
 * Service to retrieve apps
 */
@Injectable({
  providedIn: 'root'
})
export class AppService extends DomainService<App, App> {

  constructor(http: HttpClient, urlService: UrlService) {
    super(http, urlService);
  }

  protected getBaseUrl(): string {
    return '/web/senders/{senderId}/apps';
  }
}
