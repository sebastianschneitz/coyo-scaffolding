import {HttpClientTestingModule} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {AppService} from './app.service';

describe('AppService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppService, {
        provide: UrlService, useValue: jasmine.createSpyObj('urlService', [''])
      }]
    });
  });

  it('should be created',
    inject([AppService], (service: AppService) => {
      expect(service).toBeTruthy();
    })
  );
});
