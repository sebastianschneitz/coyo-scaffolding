import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {DomainService} from '@domain/domain/domain.service';
import {SenderEvent} from '@domain/event/SenderEvent';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService extends DomainService<Event, Event> {

  constructor(http: HttpClient,
              urlService: UrlService) {
    super(http, urlService);
  }

  protected getBaseUrl(): string {
    return '/web/events';
  }

  /**
   * Create a new event.
   *
   * @param event The event data.
   * @return the created event.
   */
  createEvent(event: SenderEvent): Observable<SenderEvent> {
    return this.http.post<SenderEvent>(this.getUrl(), event);
  }

  /**
   * Gets event data.
   *
   * @param eventId the event id.
   * @return the requested event.
   */
  getEvent(eventId: string): Observable<SenderEvent> {
    const url = this.getUrl({eventId}, '/{eventId}');
    return this.http.get<SenderEvent>(url, {params: {_permissions: '*'}});
  }

  /**
   * Update a given event data.
   *
   * @param event the event with the updated data.
   * @return the updated event.
   */
  updateEvent(event: SenderEvent): Observable<SenderEvent> {
    const url = this.getUrl({eventId: event.id}, '/{eventId}');
    return this.http.put<SenderEvent>(url, event, {params: {_permissions: '*'}});
  }

  /**
   * Delete the event.
   *
   * @param eventId the event id.
   * @return the removed event.
   */
  deleteEvent(eventId: string): Observable<SenderEvent> {
    const url = this.getUrl({eventId}, '/{eventId}');
    return this.http.delete<SenderEvent>(url);
  }

  /**
   * Retrieves upcoming events for the widget with the given ID.
   *
   * @param widgetId the ID of the corresponding widget
   * @param limit the maximum number of results
   * @param includeOngoing flag to indicate if ongoing events should be included
   * @param fetchType the type of events to fetch
   * @param host the corresponding event host
   * @return the requested events.
   */
  getUpcomingEvents(widgetId: string, limit: number, includeOngoing: boolean, fetchType: string, host: string): Observable<SenderEvent[]> {
    const headers = {handleErrors: 'true'};
    let params = new HttpParams()
      .append('limit', limit.toString())
      .append('includeOngoing', includeOngoing.toString())
      .append('fetchType', fetchType);
    if (host) {
      params = params.append('host', host);
    }

    return this.http.get<SenderEvent[]>(`/web/widgets/upcoming-events/${widgetId}`, {headers, params});
  }
}
