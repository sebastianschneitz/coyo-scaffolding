export interface ParticipantsLimit {
  numberOfAttendingParticipants: number;
  participantsLimit: number;
}
