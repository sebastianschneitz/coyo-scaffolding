/**
 * A theme
 */
export interface Theme {
  id: string;
  modified: Date;
  css: string;
  colors: { [key: string]: string };
  images: { [key: string]: string };
}
