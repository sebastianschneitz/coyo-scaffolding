import {TestBed} from '@angular/core/testing';
import {GSUITE, OFFICE365} from '@domain/attachment/storage';
import {IconService} from './icon.service';

describe('iconService', () => {
  let iconService: IconService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    iconService = TestBed.get(IconService);
  });

  it('should be created', () => {
    expect(iconService).toBeTruthy();
  });

  it('should get gsuite document icon name for gsuite storage and docs mime type', () => {
    // given
    const mimeType = 'application/vnd.google-apps.document';
    const storage = GSUITE;
    // when
    const result = iconService.getFileIconByMimeType(mimeType, storage);
    // then
    expect(result).toEqual('google-document');
  });

  it('should get gsuite image icon name for gsuite storage and image mime type', () => {
    // given
    const mimeType = 'image/png';
    const storage = GSUITE;
    // when
    const result = iconService.getFileIconByMimeType(mimeType, storage);
    // then
    expect(result).toEqual('google-image');
  });

  it('should get general gsuite icon name for gsuite storage and unknown mime type', () => {
    // given
    const mimeType = 'application/unknown';
    const storage = GSUITE;
    // when
    const result = iconService.getFileIconByMimeType(mimeType, storage);
    // then
    expect(result).toEqual('google-file');
  });

  it('should get office365 icon name for office365 storage and excel mime type', () => {
    // given
    const mimeType = 'application/vnd.ms-excel';
    const storage = OFFICE365;
    // when
    const result = iconService.getFileIconByMimeType(mimeType, storage);
    // then
    expect(result).toEqual('excel_mc');
  });

  it('should get office365 folder icon name for office365 storage and folder mime type', () => {
    // given
    const mimeType = 'application/folder';
    const storage = OFFICE365;
    // when
    const result = iconService.getFileIconByMimeType(mimeType, storage);
    // then
    expect(result).toEqual('folder_mc');
  });

  it('should get general plain text icon name for office365 storage and plain mime type', () => {
    // given
    const mimeType = 'text/plain';
    const storage = OFFICE365;
    // when
    const result = iconService.getFileIconByMimeType(mimeType, storage);
    // then
    expect(result).toEqual('text');
  });

  it('should get general pdf icon name for undefined storage and pdf mime type', () => {
    // given
    const mimeType = 'application/pdf';
    // when
    const result = iconService.getFileIconByMimeType(mimeType, undefined);
    // then
    expect(result).toEqual('pdf');
  });

  it('should get generic file icon name for undefined storage and mime type', () => {
    // given
    const mimeType = 'application/json';
    // when
    const result = iconService.getFileIconByMimeType(mimeType, undefined);
    // then
    expect(result).toEqual('generic-file');
  });
});
