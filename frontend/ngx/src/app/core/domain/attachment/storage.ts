export type StorageType = 'LOCAL_BLOB_STORAGE' | 'LOCAL_FILE_LIBRARY' | 'G_SUITE' | 'OFFICE_365';

export const LOCAL_BLOB: StorageType = 'LOCAL_BLOB_STORAGE';
export const LOCAL_FILE_LIBRARY: StorageType = 'LOCAL_FILE_LIBRARY';
export const GSUITE: StorageType = 'G_SUITE';
export const OFFICE365: StorageType = 'OFFICE_365';
