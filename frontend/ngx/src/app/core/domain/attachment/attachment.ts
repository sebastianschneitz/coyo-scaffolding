import {GoogleFileMetaData} from '@app/integration/gsuite/google-api/google-file-metadata';
import {StorageType} from '@domain/attachment/storage';

/**
 * An attachment symbolizes an uploaded file
 */
export interface Attachment {
  serialVersionUID: number;
  id: string;
  name: string;
  contentType: string;
  length: number;
  created: Date;
  modified: Date;
  storage: StorageType;
  downloadUrl: string;
  fileId: string;
  modelId: string;
  deleted: boolean;
  canEdit?: boolean;
  exportLinks?: GoogleFileMetaData['exportLinks'];
}
