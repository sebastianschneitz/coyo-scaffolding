export interface AdditionalInformation {
  translationKey: string;
  translationArgs: string[];
  searchable: boolean;
}

export interface Guest {
  id: string;
  typeName: string;
  displayName: string;
  displayNameInitials: string;
  color: string;
  avatarImageUrl: string;
  additionalInformation: AdditionalInformation;
}
