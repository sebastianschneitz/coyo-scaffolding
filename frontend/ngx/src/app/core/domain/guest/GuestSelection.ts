import {Guest} from '@domain/guest/Guest';

export interface GuestSelection extends Guest {
  selected: boolean;
}
