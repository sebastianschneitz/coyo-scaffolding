import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {UserChooserService} from '@domain/guest/user-chooser.service';
import {Pageable} from '@domain/pagination/pageable';
import * as _ from 'lodash';

describe('UserChooserService', () => {
  let urlService: jasmine.SpyObj<UrlService>;
  let httpMock: HttpTestingController;
  let userChooserService: UserChooserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserChooserService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService',
          ['join'])
      }]
    });
    httpMock = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    userChooserService = TestBed.get(UserChooserService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  it('should be created', () => {
    expect(userChooserService).toBeTruthy();
  });

  it('should create the correct url', () => {
    // given
    const term = 'A';
    const types = {users: true, groups: true, pages: false, workspaces: true};
    const pageable = new Pageable(0, 20);

    // when
    const subscription = userChooserService.searchGuests(term, types, pageable).subscribe(() => {});

    // '/web/users/chooser/search?term=A&types=user,group,workspace'
    const req = httpMock.expectOne('/web/users/chooser/search?term=A&types=user,group,workspace&_page=0&_pageSize=20');
    expect(req.request.method).toBe('GET');
    req.flush({});
    subscription.unsubscribe();
  });

});
