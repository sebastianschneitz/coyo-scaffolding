/**
 * A single subscription of a user to a subscribable entity.
 */
export interface Subscription {
  userId: string;
  senderId: string;
  targetId: string;
  targetType: string;
  autoSubscribe: boolean;
}
