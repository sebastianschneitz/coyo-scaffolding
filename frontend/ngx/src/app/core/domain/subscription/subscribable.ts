import {Sender} from '@domain/sender/sender';

/**
 * An entity that can be subscribed by a user.
 */
export interface Subscribable {
  id: string;
  typeName: 'timeline-item' | 'blog-article' | 'wiki-article';
  senderId?: string;
  author?: Sender;
}
