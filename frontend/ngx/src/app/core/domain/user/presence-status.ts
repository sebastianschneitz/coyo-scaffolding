/**
 * Presence status type
 */
export interface PresenceStatus {
  state: 'OFFLINE' | 'GONE' | 'AWAY' | 'BUSY' | 'ONLINE';
  label?: string;
  subscriptionInfo?: {
    token: string;
  };
}
