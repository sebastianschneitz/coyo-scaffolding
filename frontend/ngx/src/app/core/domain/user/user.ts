import {Sender} from '../sender/sender';

/**
 * User entity model
 */
export interface User extends Sender {
  email: string;
  moderatorMode: boolean;
  language: string;
  globalPermissions?: [string];
  properties?: {
    [key: string]: any
  };
}
