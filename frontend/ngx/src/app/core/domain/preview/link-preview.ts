import {WebPreview} from '@domain/preview/web-preview';

/**
 * Contains all relevant information for a preview of a link inside a timeline post
 */
export interface LinkPreview extends WebPreview {
  internal: boolean;
  contentType?: string;
  description?: string;
  imageUrl?: string;
  imageBlobUid?: string;
  iconUrl?: string;
  title?: string;
  tld?: string;
}
