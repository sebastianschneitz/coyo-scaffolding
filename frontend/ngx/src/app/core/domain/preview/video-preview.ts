import {WebPreview} from '@domain/preview/web-preview';

/**
 * Contains all relevant data to embed a video by url and iframe
 */
export interface VideoPreview extends WebPreview {
  width: number;
  height: number;
  videoUrl: string;
}
