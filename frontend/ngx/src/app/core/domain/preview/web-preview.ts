/**
 * Super interface for all preview like entities
 */
export interface WebPreview {
  url: string;
  type: string;
  position: number;
}
