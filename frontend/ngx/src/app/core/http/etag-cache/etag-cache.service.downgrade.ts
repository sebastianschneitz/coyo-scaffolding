import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {EtagCacheService} from '@core/http/etag-cache/etag-cache.service';

getAngularJSGlobal()
  .module('commons.resource')
  .factory('ngxEtagCacheService', downgradeInjectable(EtagCacheService) as any);
