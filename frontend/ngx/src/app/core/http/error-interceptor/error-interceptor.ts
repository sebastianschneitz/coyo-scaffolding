import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {ServiceRecognitionService} from '@core/http/service-recognition/service-recognition.service';
import {NotificationService} from '@shared/notifications/notification/notification.service';
import {NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import * as _ from 'lodash';
import {NgxPermissionsService} from 'ngx-permissions';
import {from, Observable, Observer, of, throwError} from 'rxjs';
import {catchError, first, switchMap} from 'rxjs/operators';

/**
 * Intercepts errors and checks for the error status code.
 * Displays a graphical message if a status code has been found.
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  errorThrottle: { [key: string]: number } = {};

  constructor(@Inject(NG1_STATE_SERVICE) private stateService: IStateService,
              private notificationService: NotificationService,
              private authService: AuthService,
              private errorService: ErrorService,
              private permissionsService: NgxPermissionsService,
              private serviceValidationService: ServiceRecognitionService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const handleErrors = req.headers.get('handleErrors') !== 'false';
    return next.handle(req.clone({headers: req.headers.delete('handleErrors')}))
      .pipe(catchError((err: any) => this.handleError(err, handleErrors)));
  }

  private handleError(error: any, handleErrors: boolean): Observable<any> {
    if (error instanceof HttpErrorResponse) {
      if (error.status === 304) { // NOT MODIFIED, not an error
        return of(error);
      } else if (error.status === 401) { // UNAUTHORIZED
        throwError(error);
      } else if ((error.status === 503 || error.status === 504) &&
        this.serviceValidationService.getTargetService(error.url) === null) { // SYSTEM UNAVAILABLE
        return this.handleMaintenance(error);
      } else {
        return new Observable((observer: Observer<HttpErrorResponse>) => {
          if (handleErrors) {
            this.errorService.getMessage(error).subscribe((translation: string) => {
              if (!this.handleInvalidTenant(error, translation)) {
                const now = new Date().getTime();
                this.cleanupErrorThrottle(now);
                const hash = this.hashCode(translation);
                if (!this.errorThrottle[hash]) {
                  this.errorThrottle[hash] = now;
                  this.notificationService.error(translation, null, false);
                }
              }
            });
          }
          observer.error(error);
        }).pipe(first());
      }
    }
    return of(error);
  }

  /**
   * Special handling when the response indicated the tenant is invalid or inactive.
   * In that case that it is invalid we always redirect to the error page and offer options to configure the url
   * (if configurable). If it is inactive we show the error message with a link to the customer center.
   *
   * @param error A response that represents an error or failure
   * @param translation The (already translated) error message to be displayed.
   * @returns return false if (error_status != (INVALID_TENANT ^ INACTIVE_TENANT)) else true
   */
  private handleInvalidTenant(error: HttpErrorResponse, translation: string): boolean {
    const status: string = _.get(error, 'error.errorStatus');

    if (status !== 'INVALID_TENANT' && status !== 'INACTIVE_TENANT') {
      return false;
    }

    const buttons = ['RETRY'];
    if (status === 'INACTIVE_TENANT') {
      buttons.push('CUSTOMER_CENTER');
    }

    this.errorService.showErrorPage(translation, error.status, buttons);
    return true;
  }

  /**
   * Special handling when the response indicated that a maintenance mode is active.
   * In case the maintenance mode is set globally also the admins that can manage maintenance cannot login anymore.
   * If the maintenance mode is not global the tenant's admins can login to edit maintenance mode.
   *
   * @param error A response that represents an error or failure
   * @returns promise that resolves
   */
  private handleMaintenance(error: HttpErrorResponse): Observable<HttpErrorResponse> {
    const status: string = _.get(error, 'error.errorStatus');
    let res: any;

    if (status === 'GLOBAL_MAINTENANCE') {
      res = this.stateService.go('front.maintenance', {global: true}, {
        location: false
      });
      return this.handleMaintenanceHelper(res, error);
    } else {
      return this.authService.getUser()
        .pipe(switchMap(() => from(this.permissionsService.hasPermission('MANAGE_MAINTENANCE'))))
        .pipe(switchMap(hasPermission => {
            if (!hasPermission) {
              res = this.stateService.go('front.maintenance', {global: false}, {location: false});
            }
            return this.handleMaintenanceHelper(res, error);
          }), catchError(() => {
            if (this.stateService.current.name !== 'front.login') {
              res = this.stateService.go('front.login');
            }
            return this.handleMaintenanceHelper(res, error);
          })
        );
    }
  }

  private handleMaintenanceHelper(res: Promise<any>, error: HttpErrorResponse): Observable<HttpErrorResponse> {
    if (res) {
      return of(error);
    }
    return throwError(error);
  }

  private cleanupErrorThrottle(now: number): void {
    const bufferObject: { [key: string]: number } = {};

    Object.keys(this.errorThrottle).forEach(key => {
      const timeValue: number = this.errorThrottle[key];

      if ((now - 2500) <= timeValue) {
        bufferObject[key] = timeValue;
      }
    });
    this.errorThrottle = bufferObject;
  }

  private hashCode(string: string): string {
    let result = 0;

    if (string.length <= 0) {
      return result + '';
    }

    for (let i = 0; i < string.length; i++) {
      result = Math.imul(31, result) + string.charCodeAt(i);
    }
    return result + '';
  }
}
