import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {ServiceRecognitionService} from '@core/http/service-recognition/service-recognition.service';
import {NotificationService} from '@shared/notifications/notification/notification.service';
import {NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import {NgxPermissionsService} from 'ngx-permissions';
import {of} from 'rxjs';
import {ErrorInterceptor} from './error-interceptor';

describe('ErrorInterceptor', () => {
  const url = '/some/url';
  let stateMock: jasmine.SpyObj<IStateService>;
  let authService: jasmine.SpyObj<AuthService>;
  let backend: HttpTestingController;
  let httpClient: HttpClient;
  let permissionsService: jasmine.SpyObj<NgxPermissionsService>;
  let serviceRecognitionService: jasmine.SpyObj<ServiceRecognitionService>;

  beforeEach(() => {
    stateMock = jasmine.createSpyObj('$state', ['go']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ErrorInterceptor, {
        provide: ErrorService,
        useValue: jasmine.createSpyObj('errorService', ['isNotificationSuppressed', 'getMessage', 'showErrorPage'])
      }, {
        provide: NG1_STATE_SERVICE,
        useValue: stateMock
      }, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['getUser'])
      }, {
        provide: NotificationService,
        useValue: jasmine.createSpyObj('notificationService', ['error'])
      }, {
        provide: NgxPermissionsService,
        useValue: jasmine.createSpyObj('NgxPermissionsService', ['hasPermission'])
      }, {
        provide: HTTP_INTERCEPTORS,
        useClass: ErrorInterceptor, multi: true
      }, {
        provide: ServiceRecognitionService,
        useValue: jasmine.createSpyObj('serviceRecognitionService', ['getTargetService'])
      }]
    });

    // initialize spy's objects
    authService = TestBed.get(AuthService);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    backend = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
    permissionsService = TestBed.get(NgxPermissionsService);
    serviceRecognitionService = TestBed.get(ServiceRecognitionService);

    // define methods
    authService.getUser.and.returnValue(of({id: '1337'}));
    permissionsService.hasPermission.and.returnValue(of(false));
    serviceRecognitionService.getTargetService.and.returnValue(null);
  });

  afterEach(() => {
    backend.verify();
  });

  describe('error response interceptor', () => {

    it('should redirect users to maintenance state', fakeAsync(() => {
      // when
      httpClient.get(url).subscribe(() => {}, () => {
        // then
        expect(authService.getUser).toHaveBeenCalled();
        expect(stateMock.go).toHaveBeenCalledTimes(1);
        expect(stateMock.go).toHaveBeenCalledWith('front.maintenance', {global: false}, {location: false});
      });

      // given
      backend.expectOne(url).flush({errorStatus: 'TENANT_MAINTENANCE'}, {status: 503, statusText: 'what_ever'});
      tick();
    }));

    it('should not redirect maintenance admins to maintenance state', fakeAsync(() => {
      // given
      permissionsService.hasPermission.and.returnValue(of(true));

      // when
      httpClient.get(url).subscribe(() => {}, () => {
        // then
        expect(authService.getUser).toHaveBeenCalled();
        expect(stateMock.go).not.toHaveBeenCalled();
      });

      // given
      backend.expectOne(url).flush({errorStatus: 'TENANT_MAINTENANCE'}, {status: 503, statusText: 'what_ever'});
      tick();
    }));

    it('should redirect maintenance admins to maintenance state for global maintenance mode', fakeAsync(() => {
      // when
      httpClient.get(url).subscribe(() => {}, () => {
        // then
        expect(authService.getUser).not.toHaveBeenCalled();
        expect(stateMock.go).toHaveBeenCalled();
        expect(stateMock.go).toHaveBeenCalledWith('front.maintenance', {global: true}, {location: false});
      });

      // given
      backend.expectOne(url).flush({errorStatus: 'GLOBAL_MAINTENANCE'}, {status: 503, statusText: 'what_ever'});
      tick();
    }));

    it('should not redirect to login when calling translation service and service is unavailable', fakeAsync(() => {
      // given
      permissionsService.hasPermission.and.returnValue(of(true));

      // when
      httpClient.get(url).subscribe(() => {}, () => {
        // then
        expect(authService.getUser).toHaveBeenCalled();
        expect(stateMock.go).not.toHaveBeenCalled();
      });

      // then
      backend.expectOne(url).flush({errorStatus: 'UNAVAILABLE'}, {status: 503, statusText: 'what_ever'});
      tick();
    }));
  });
});
