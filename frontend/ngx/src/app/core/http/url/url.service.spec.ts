import {inject, TestBed} from '@angular/core/testing';
import {WINDOW} from '@root/injection-tokens';
import {NG1_BACKEND_URL_SERVICE} from '@upgrade/upgrade.module';
import {UrlService} from './url.service';

class MockBackendService {
  /* tslint:disable-next-line:completed-docs */
  getUrl(): string {
    return 'https://backendUrl.com';
  }
}

describe('UrlService', () => {
  let mockBackendService: MockBackendService;

  beforeEach(() => {
    mockBackendService = new MockBackendService();
    TestBed.configureTestingModule({
      providers: [UrlService, {
        provide: NG1_BACKEND_URL_SERVICE,
        useValue: mockBackendService
      }, {
        provide: WINDOW,
        useValue: {
          location: {
            host: 'frontendUrl'
          }
        }
      }]
    });
  });

  it('should be created', inject([UrlService], (service: UrlService) => {
    expect(service).toBeTruthy();
  }));

  it('should return the backend URL', inject([UrlService], (service: UrlService) => {
    // when
    const url = service.getBackendUrl();

    // then
    expect(url).toEqual('https://backendUrl.com');
  }));

  it('should recognize backend URLs', inject([UrlService], (service: UrlService) => {
    expect(service.isAbsoluteBackendUrl('https://backendUrl.com/api/dummy-request')).toBeTruthy();
  }));

  it('should recognize non-backend URLs', inject([UrlService], (service: UrlService) => {
    expect(service.isAbsoluteBackendUrl('https://api.google.com/api/dummy-request')).toBeFalsy();
    expect(service.isAbsoluteBackendUrl('/')).toBeFalsy();
    expect(service.isAbsoluteBackendUrl(null)).toBeFalsy();
  }));

  it('should recognize host URLs as backend URLs when backendUrl is empty', inject([UrlService], (service: UrlService) => {
    spyOn(mockBackendService, 'getUrl').and.returnValue('');
    expect(service.isAbsoluteBackendUrl('https://backendUrl.com/api/dummy-request')).toBeFalsy();
    expect(service.isAbsoluteBackendUrl('https://frontendUrl.com/api/dummy-request')).toBeTruthy();
  }));

  it('should join URL parts', inject([UrlService], (service: UrlService) => {
    // when
    const url = service.join('/part1/', '/part2', 'part3');

    // then
    expect(url).toEqual('part1/part2/part3');
  }));

  it('should check relative paths', inject([UrlService], (service: UrlService) => {
    // given
    const relative1 = 'foo/bar';
    const relative2 = '/foo/bar';
    const relative3 = 'http://frontendUrl/foo/bar';
    const relative4 = 'https://frontendUrl/foo/bar';
    const relative5 = 'https://www.frontendUrl/foo/bar';

    // when
    const result1 = service.isRelativePath(relative1);
    const result2 = service.isRelativePath(relative2);
    const result3 = service.isRelativePath(relative3);
    const result4 = service.isRelativePath(relative4);
    const result5 = service.isRelativePath(relative5);

    // then
    expect(result1).toEqual(true);
    expect(result2).toEqual(true);
    expect(result3).toEqual(true);
    expect(result4).toEqual(true);
    expect(result5).toEqual(true);
  }));

  it('should check absolute paths', inject([UrlService], (service: UrlService) => {
    // given
    const absolute1 = 'http://test.com';
    const absolute2 = 'https://test.com/foo/bar';

    // when
    const result1 = service.isRelativePath(absolute1);
    const result2 = service.isRelativePath(absolute2);

    // then
    expect(result1).toEqual(false);
    expect(result2).toEqual(false);
  }));

  it('should create params url with number values', inject([UrlService], (service: UrlService) => {
    // when
    const url = service.toUrlParamString('id', [1, 2, 3]);

    // then
    expect(url).toEqual('id=1&id=2&id=3');
  }));

  it('should create params url with string values', inject([UrlService], (service: UrlService) => {
    // when
    const url = service.toUrlParamString('key', ['value-1', 'value-2', 'value-3']);

    // then
    expect(url).toEqual('key=value-1&key=value-2&key=value-3');
  }));

  it('should not create params with undefined value', inject([UrlService], (service: UrlService) => {
    // when
    const url = service.toUrlParamString('key', ['value-1', undefined, 'value-3']);

    // then
    expect(url).toEqual('key=value-1&key=value-3');
  }));

  it('should not create params with empty value', inject([UrlService], (service: UrlService) => {
    // when
    const url = service.toUrlParamString('key', ['value-1', '', 'value-3']);

    // then
    expect(url).toEqual('key=value-1&key=value-3');
  }));

  it('should insert params into url', inject([UrlService], (service: UrlService) => {
    // given
    const url = '/web/senders/{{groupId}}/documents/{{id}}';
    const groupId = '12345';
    const fileId = '54321';

    // when
    const result = service.insertPathVariablesIntoUrl(url, {groupId: groupId, id: fileId});

    // then
    expect(result).toEqual('/web/senders/12345/documents/54321');
  }));

  it('should create a param string out of a map of params', inject([UrlService], (service: UrlService) => {
    // given
    const params = {
      a: ['test1', 'test2'],
      b: [0]
    };

    // when
    const result = service.toMultiUrlParamString(params);

    // then
    expect(result).toBe('a=test1&a=test2&b=0');
  }));

  it('should create remove empty params', inject([UrlService], (service: UrlService) => {
    // given
    const params = {
      a: ['test1'],
      b: [] as string[]
    };

    // when
    const result = service.toMultiUrlParamString(params);

    // then
    expect(result).toBe('a=test1');
  }));

  it('should return empty string if no param is set', inject([UrlService], (service: UrlService) => {
    // given
    const params = {
      a: [] as string[],
    };

    // when
    const result = service.toMultiUrlParamString(params);

    // then
    expect(result).toBe('');
  }));

  it('should remove empty or undefined values', inject([UrlService], (service: UrlService) => {
    // given
    const params = {
      a: ['test1', '', 'test2', undefined],
      b: [0]
    };

    // when
    const result = service.toMultiUrlParamString(params);

    // then
    expect(result).toBe('a=test1&a=test2&b=0');
  }));
});
