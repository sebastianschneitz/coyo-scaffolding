import {HTTP_INTERCEPTORS, HttpClient, HttpResponse} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {Ng1AuthService} from '@root/typings';
import {NG1_AUTH_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import {UrlService} from '../url/url.service';
import {AuthInterceptor} from './auth-interceptor';

describe('AuthInterceptor', () => {
  const backendUrl = 'http://host';
  let mockedAuthService: jasmine.SpyObj<Ng1AuthService>;
  let mockedState: jasmine.SpyObj<IStateService>;

  beforeEach(() => {
    const mockedUrlService = jasmine.createSpyObj('urlService', ['getBackendUrl', 'isBackendUrlSet']);
    mockedUrlService.getBackendUrl.and.returnValue(backendUrl);
    mockedUrlService.isBackendUrlSet.and.returnValue(true);

    mockedAuthService = jasmine.createSpyObj('authService', ['isAuthenticated', 'clearSession']);
    mockedState = jasmine.createSpyObj('$state', ['go']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: UrlService, useValue: mockedUrlService},
        {provide: NG1_AUTH_SERVICE, useValue: mockedAuthService},
        {provide: NG1_STATE_SERVICE, useValue: mockedState},
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
      ]
    });
  });

  it('should redirect to login page when respose status is 401', fakeAsync(inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
      // given
      mockedAuthService.isAuthenticated.and.returnValue(true);
      mockedAuthService.clearSession.and.returnValue(Promise.resolve());

      // when
      http.get(backendUrl + '/test').subscribe();
      const request = mock.expectOne(backendUrl + '/test');
      request.event(new HttpResponse({status: 401, statusText: 'Unauthorized'}));

      // then
      tick();
      expect(mockedAuthService.clearSession).toHaveBeenCalled();
      expect(mockedState.go).toHaveBeenCalledWith('front.login');
      mock.verify();
    })));

  it('should do nothing on http response other than 401', inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
      // given
      mockedAuthService.isAuthenticated.and.returnValue(true);

      // when
      http.get(backendUrl + '/test').subscribe();
      const request = mock.expectOne(backendUrl + '/test');
      request.event(new HttpResponse({status: 200, statusText: 'Ok'}));

      // then
      expect(mockedAuthService.clearSession).not.toHaveBeenCalled();
      mock.verify();
    }));

  it('should not redirect if user is not authenticated', inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
      // given
      mockedAuthService.isAuthenticated.and.returnValue(false);

      // when
      http.get(backendUrl + '/test').subscribe();
      const request = mock.expectOne(backendUrl + '/test');
      request.event(new HttpResponse({status: 401, statusText: 'Unauthorized'}));

      // then
      expect(mockedAuthService.clearSession).not.toHaveBeenCalled();
      expect(mockedState.go).not.toHaveBeenCalledWith('front.login');
      mock.verify();
    }));
});
