import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {NGXLogger} from 'ngx-logger';
import {UrlService} from '../url/url.service';
import {BackendUrlInterceptor} from './backend-url-interceptor';

describe('BackendUrlInterceptor', () => {
  let urlService: jasmine.SpyObj<UrlService>;
  let log: jasmine.SpyObj<NGXLogger>;

  beforeEach(() => {
    urlService = jasmine.createSpyObj('urlService', ['getBackendUrl', 'join', 'isBackendUrlSet']);
    log = jasmine.createSpyObj('NGXLogger', ['error']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: UrlService, useValue: urlService},
        {provide: NGXLogger, useValue: log},
        {provide: HTTP_INTERCEPTORS, useClass: BackendUrlInterceptor, multi: true}
      ]
    });
  });

  it('should prepend backendUrl to all requests to /web', inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
      // given
      const backendUrl = 'http://testBackend:8080';
      urlService.getBackendUrl.and.returnValue(backendUrl);
      urlService.isBackendUrlSet.and.returnValue(true);
      urlService.join.and.callFake((url: string, reqUrl: string) => url + reqUrl);

      // when
      http.get('/web').subscribe(() => {});
      http.post('/web', {}).subscribe(() => {});
      http.put('/web', {}).subscribe(() => {});
      http.delete('/web').subscribe(() => {});

      // then
      mock.expectOne(req => req.method === 'GET' && req.url === backendUrl + '/web',
        'Prepend backend url to get');
      mock.expectOne(req => req.method === 'POST' && req.url === backendUrl + '/web',
        'Prepend backend url to post');
      mock.expectOne(req => req.method === 'PUT' && req.url === backendUrl + '/web',
        'Prepend backend url to put');
      mock.expectOne(req => req.method === 'DELETE' && req.url === backendUrl + '/web',
        'Prepend backend url to delete');
      mock.verify();
    }));

  it('should throw error if no backend url is set', inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
      // given
      urlService.isBackendUrlSet.and.returnValue(false);

      // when
      http.get('/web').subscribe(() => {}, error => expect(error).toBeTruthy());

      // then
      expect(log.error).toHaveBeenCalled();
      mock.verify();
    }));

  it('should not prepend backendUrl to all requests not pointing to /web', inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
      // given
      const backendUrl = 'http://testBackend:8080';
      urlService.getBackendUrl.and.returnValue(backendUrl);
      urlService.join.and.callFake((url: string, reqUrl: string) => url + reqUrl);

      // when
      http.get('/1').subscribe(() => {});
      http.post('/2', {}).subscribe(() => {});
      http.put('/3', {}).subscribe(() => {});
      http.delete('/4').subscribe(() => {});

      // then
      mock.expectOne('/1');
      mock.expectOne('/2');
      mock.expectOne('/3');
      mock.expectOne('/4');
      mock.verify();
    }));
});
