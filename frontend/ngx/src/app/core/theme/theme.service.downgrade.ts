import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {ThemeService} from './theme.service';

getAngularJSGlobal()
  .module('coyo.admin.themes')
  .factory('ngxThemeService', downgradeInjectable(ThemeService));
