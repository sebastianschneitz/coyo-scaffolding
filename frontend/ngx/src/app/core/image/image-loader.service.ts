/**
 * Service for loading images with the correct orientation
 */
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
/**
 * Image loader service mainly used to correct image orientation by reading exif tag information
 * about the rotation: @see {@link http://sylvana.net/jpegcrop/exif_orientation.html}.
 * First the image is loaded via an HttpClient as a blob and piped into a FileReader.
 * A base64 string representation of the image is created and the raw data is checked for
 * an exif orientation tag (0x0112). If the image does not qualify (not a valid jpeg, no tag given or nor change in
 * orientation required) the base64 string is emitted.
 * If the image qualifies for correction, it is drawn onto a transformed invisible canvas which is then
 * converted back into a dataUrl (base64) and emitted.
 */
export class ImageLoaderService {
  constructor(private httpClient: HttpClient) {

  }

  /**
   * Loads an image and guarantees that the image has been loaded when the observable completes
   * @param source The url of the image to be loaded
   * @returns an observable HTMLImageElement
   */
  loadImage(source: string): Observable<HTMLImageElement> {
    const subject = new Subject<HTMLImageElement>();
    const image = new Image();
    image.onload = () => subject.next(image);
    image.onerror = err => subject.error(err);
    this.httpClient.get(source, {responseType: 'blob', headers: {handleErrors: 'false'}}).subscribe(data => {
      const fileReader = new FileReader();
      fileReader.onloadend = () => {
        const base64 = this.createBase64String(data.type, fileReader.result as ArrayBufferLike);
        const orientation = this.findRotation(fileReader.result as ArrayBufferLike);
        this.correctOrientation(base64, orientation).subscribe(imageData => image.src = imageData);
      };
      fileReader.readAsArrayBuffer(data);
    }, error => subject.error(error));
    return subject;
  }

  /**
   * Creates a valid base64 string with data type from a data type string and a buffer
   * @param dataType The data type (Content-Type)
   * @param buffer The buffer containing the binary image data
   * @returns A base64 string
   */
  private createBase64String(dataType: string, buffer: ArrayBufferLike): string {
    return `data:${dataType};base64,${this.arrayBufferToBase64(buffer)}`;
  }

  /**
   * Converts an arraybuffer into the equivalent base64 representation
   * @param buffer The buffer object
   * @returns A base64 representation of the buffer data
   */
  private arrayBufferToBase64(buffer: ArrayBufferLike): string {
    let str = '';
    const bytes = new Uint8Array(buffer);
    const length = bytes.length;
    for (let i = 0; i < length; i++) {
      str += String.fromCharCode(bytes[i]);
    }
    return btoa(str);
  }

  /**
   * Takes a base64 representation of an image and the images current orientation number
   * @see {@link http://sylvana.net/jpegcrop/exif_orientation.html}
   * @param base64 The raw image data in base64
   * @param currentOrientation The image orientation number
   * @returns An observable that will emit a base64 string of the correctly oriented image
   */
  private correctOrientation(base64: string, currentOrientation: number): Observable<string> {
    // Rotation happens only between 2 and 8 (1 is correct orientation)
    if (!currentOrientation || currentOrientation < 2 || currentOrientation > 8) {
      return of(base64);
    }
    const subject = new Subject<string>();
    // Create a temporary image object to load the base64 into the canvas
    const image = new Image();
    image.onload = () => {
      // Create the temporary canvas
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');

      // Switch height and width if necessary
      const imageWidth = image.width;
      const imageHeight = image.height;
      if (4 < currentOrientation && currentOrientation < 9) {
        canvas.width = imageHeight;
        canvas.height = imageWidth;
      } else {
        canvas.width = imageWidth;
        canvas.height = imageHeight;
      }

      switch (currentOrientation) {
        case 2:
          context.transform(-1, 0, 0, 1, imageWidth, 0);
          break;
        case 3:
          context.transform(-1, 0, 0, -1, imageWidth, imageHeight);
          break;
        case 4:
          context.transform(1, 0, 0, -1, 0, imageHeight);
          break;
        case 5:
          context.transform(0, 1, 1, 0, 0, 0);
          break;
        case 6:
          context.transform(0, 1, -1, 0, imageHeight, 0);
          break;
        case 7:
          context.transform(0, -1, -1, 0, imageHeight, imageWidth);
          break;
        case 8:
          context.transform(0, -1, 1, 0, 0, imageWidth);
          break;
        default:
          break;
      }
      // Draw to transformed canvas
      context.drawImage(image, 0, 0);
      // Emit new base64 string
      subject.next(canvas.toDataURL());
    };
    image.src = base64;
    return subject;
  }

  /**
   * Attempts to find the exif orientation tag and extract its value
   *
   * Sources: https://github.com/Donaldcwl/browser-image-compression/blob/master/lib/utils.js
   * https://stackoverflow.com/a/32490603/10395024
   *
   * @param rawData The image data in an array buffer
   * @returns The orientation or 0 if none is found
   */

  /*tslint:disable:no-bitwise*/
  private findRotation(rawData: ArrayBufferLike): number {

    const view = new DataView(rawData);
    if (view.getUint16(0, false) !== 0xFFD8) {
      return -2;
    }
    const length = view.byteLength;
    let offset = 2;
    while (offset < length) {
      if (view.getUint16(offset + 2, false) <= 8) {
        return -1;
      }
      const marker = view.getUint16(offset, false);
      offset += 2;
      if (marker === 0xFFE1) {
        if (view.getUint32(offset += 2, false) !== 0x45786966) {
          return -1;
        }

        const little = view.getUint16(offset += 6, false) === 0x4949;
        offset += view.getUint32(offset + 4, little);
        const tags = view.getUint16(offset, little);
        offset += 2;
        for (let i = 0; i < tags; i++) {
          if (view.getUint16(offset + (i * 12), little) === 0x0112) {
            return view.getUint16(offset + (i * 12) + 8, little);
          }
        }
      } else if ((marker & 0xFF00) !== 0xFF00) {
        break;
      } else {
        offset += view.getUint16(offset, false);
      }
    }
    return -1;
  }
}
