import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimeProviderService {

  constructor() { }

  getCurrentDate(): Date {
    return new Date();
  }
}
