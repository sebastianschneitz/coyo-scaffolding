import {inject, TestBed} from '@angular/core/testing';
import {ScreenSize} from '@core/window-size/screen-size';
import {WINDOW} from '@root/injection-tokens';
import {skip} from 'rxjs/operators';
import {breakpointLg, breakpointMd, breakpointSm, WindowSizeService} from './window-size.service';

describe('WindowSizeService', () => {
  let windowMock: jasmine.SpyObj<any>;
  let windowSizeService: WindowSizeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WindowSizeService, {
        provide: WINDOW, useValue: jasmine.createSpyObj('windowService', ['blur'])
      }]
    });

    windowMock = TestBed.get(WINDOW);
    windowMock.innerWidth = breakpointSm - 1;
    windowMock.innerHeight = 600;
    windowMock.devicePixelRatio = 1;
    windowSizeService = TestBed.get(WindowSizeService);
  });

  it('should be created', inject([WindowSizeService], (service: WindowSizeService) => {
    expect(service).toBeTruthy();
    expect(service.isRetina()).toBeFalsy();
    expect(service.getWidth()).toEqual(breakpointSm - 1);
    expect(service.getHeight()).toEqual(600);
    expect(service.isXs()).toBeTruthy();
  }));

  it('should update to large display size', () => {
    // given
    windowMock.innerWidth = breakpointLg;

    // when
    windowSizeService.updateWindow();

    // then
    expect(windowSizeService.isLg()).toBeTruthy();
    expect(windowSizeService.isMd()).toBeFalsy();
    expect(windowSizeService.isSm()).toBeFalsy();
    expect(windowSizeService.isXs()).toBeFalsy();
  });

  it('should update to medium display size', () => {
    // given
    windowMock.innerWidth = breakpointMd;

    // when
    windowSizeService.updateWindow();

    // then
    expect(windowSizeService.isLg()).toBeFalsy();
    expect(windowSizeService.isMd()).toBeTruthy();
    expect(windowSizeService.isSm()).toBeFalsy();
    expect(windowSizeService.isXs()).toBeFalsy();
  });

  it('should update to small display size', () => {
    // given
    windowMock.innerWidth = breakpointSm;

    // when
    windowSizeService.updateWindow();

    // then
    expect(windowSizeService.isLg()).toBeFalsy();
    expect(windowSizeService.isMd()).toBeFalsy();
    expect(windowSizeService.isSm()).toBeTruthy();
    expect(windowSizeService.isXs()).toBeFalsy();
  });

  it('should set the retina flag for displays with high pixel ratio', () => {
    // given
    windowMock.devicePixelRatio = 1.1;

    // when
    const windowSizeServiceNew = new WindowSizeService(windowMock);

    // then
    expect(windowSizeServiceNew.isRetina()).toBeTruthy();
  });

  it('should set the retina flag for displays matching retina media type', () => {
    // given
    windowMock.matchMedia = jasmine.createSpy('matchMedia');
    windowMock.matchMedia.and.returnValue({matches: true});

    // when
    const windowSizeServiceNew = new WindowSizeService(windowMock);

    // then
    expect(windowSizeServiceNew.isRetina()).toBeTruthy();
  });

  it('should not set the retina flag for displays not matching retina media type', () => {
    // given
    windowMock.matchMedia = jasmine.createSpy('matchMedia');
    windowMock.matchMedia.and.returnValue({matches: false});

    // when
    const windowSizeServiceNew = new WindowSizeService(windowMock);

    // then
    expect(windowSizeServiceNew.isRetina()).toBeFalsy();
  });

  it('should emit screen size changed to the screen size observable', () => {
    // given
    windowSizeService.observeScreenChange().pipe(skip(1)).subscribe(screenSize => {
      expect(screenSize).toBe(ScreenSize.SM);
    });

    windowMock.innerWidth = breakpointSm;

    // when
    windowSizeService.updateWindow();

    // then --> observable
  });
});
