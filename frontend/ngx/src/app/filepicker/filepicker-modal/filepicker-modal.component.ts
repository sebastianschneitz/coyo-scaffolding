import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerSearch} from '@app/filepicker/filepicker-modal/filepicker-search/filepicker-search';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {Subject, Subscription} from 'rxjs';
import {FilepickerSelection} from './filepicker-selection/filepicker-selection';

export interface FilePickerModalData {
  /**
   * The folder that is the entrypoint of the filepicker
   */
  rootFolder: FilepickerItem;

  /**
   * Optional - The first folder that should be opened. If not set, it will be the root folder.
   */
  search?: FilepickerSearch;

  /**
   * Optional - The first folder that should be opened. If not set, it will be the root folder.
   */
  firstOpenFolder?: FilepickerItem;
}

/**
 * This component contains the filepicker inside of a modal
 */
@Component({
  selector: 'coyo-filepicker-modal',
  templateUrl: './filepicker-modal.component.html',
  styleUrls: ['./filepicker-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilepickerModalComponent implements OnInit, OnDestroy {
  breadcrumbItems: FilepickerItem[] = [];
  loading: boolean = true;
  items$: Subject<FilepickerItem[]>;
  fileSelection: FilepickerSelection;

  private getChildrenSubscription: Subscription;

  constructor(public dialogRef: MatDialogRef<FilepickerModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: FilePickerModalData,
              private windowSizeService: WindowSizeService) {
  }

  ngOnInit(): void {
    this.items$ = new Subject();
    if (!!this.data.firstOpenFolder) {
      this.setCurrentBreadcrumbFolder(this.data.rootFolder);
    }
    this.openFolder(this.data.firstOpenFolder || this.data.rootFolder);
    this.fileSelection = new FilepickerSelection(this.items$);
  }

  ngOnDestroy(): void {
    this.cancelOngoingRequests();
  }

  /**
   * Handles the click event on filepicker items
   * Navigates inside the folder for folders, toggles the selection for files
   *
   * @param item the filepicker item that has been clicked
   * @param $event the click event dispatched by the browser
   */
  filePickerItemClicked(item: FilepickerItem, $event?: Event): void {
    $event.preventDefault();
    $event.stopPropagation();
    item.isFolder ? this.openFolder(item) : this.toggleFileSelection(item, $event);
  }

  /**
   * Load the contents of the given folder item
   * @param item a folder item
   */
  openFolder(item: FilepickerItem): void {
    this.loading = true;
    this.cancelOngoingRequests();
    this.items$.next([]);
    this.setCurrentBreadcrumbFolder(item);
    this.getChildrenSubscription = item.getChildren().subscribe({
      next: items => this.items$.next(items),
      complete: () => this.loading = false
    });
  }

  private cancelOngoingRequests(): void {
    if (!!this.getChildrenSubscription) {
      this.getChildrenSubscription.unsubscribe();
    }
  }

  openRootFolder(): void {
    this.breadcrumbItems = [];
    this.openFolder(this.data.rootFolder);
  }

  /**
   * Checks for mobile view.
   *
   * @returns true if the window size is typical for mobile.
   */
  isMobile(): boolean {
    return this.windowSizeService.isXs() || this.windowSizeService.isSm();
  }

  /**
   * Submits the selected files
   */
  onSubmitSelectedFiles(): void {
    this.dialogRef.close(this.fileSelection.selectedFiles);
  }

  /**
   * Goes back to previous folder.
   */
  goBackToPreviousFolder(): void {
    const latestPath = this.breadcrumbItems[this.breadcrumbItems.length - 2];
    this.openFolder(latestPath);
  }

  openSearchResultsFolder(searchResultFolder: FilepickerItem | null): void {
    if (!searchResultFolder) {
      this.openRootFolder();
      return;
    }

    this.breadcrumbItems.length = 1;
    this.openFolder(searchResultFolder);
  }

  private toggleFileSelection(item: FilepickerItem, $event?: Event): void {
    $event.preventDefault();
    this.fileSelection.toggleFileSelection(item);
  }

  private setCurrentBreadcrumbFolder(item: FilepickerItem): void {
    const index = this.breadcrumbItems.indexOf(item);
    if (index >= 0) {
      this.breadcrumbItems.length = index;
    }

    this.breadcrumbItems = [...this.breadcrumbItems, item];
  }
}
