import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerSearch} from '@app/filepicker/filepicker-modal/filepicker-search/filepicker-search';
import {TranslateService} from '@ngx-translate/core';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'coyo-filepicker-search',
  templateUrl: './filepicker-search.component.html',
  styleUrls: ['./filepicker-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilepickerSearchComponent implements OnInit {

  /**
   * The search that should be executed.
   */
  @Input() search: FilepickerSearch | undefined;

  /**
   * Emits the search results as a FilepickerItem folder. Emits null, if the search term is empty
   */
  @Output() searchResult: EventEmitter<FilepickerItem | null> = new EventEmitter<FilepickerItem | null>();

  fileSearchInput: FormControl = new FormControl();

  constructor(private translate: TranslateService) {
  }

  ngOnInit(): void {
    this.fileSearchInput.valueChanges.pipe(
      debounceTime(250),
      distinctUntilChanged()
    ).subscribe(query => this.executeSearch(query));
  }

  private executeSearch(query: string): void {
    if (!query) {
      this.searchResult.emit(null);
      return;
    }

    this.searchResult.emit({
      name: this.translate.instant('FILEPICKER.SEARCH_RESULTS.TITLE'),
      isFolder: true,
      sizeInBytes: null,
      lastModified: null,
      mimeType: null,
      storageType: null,
      customSvgIcon: 'search',
      getChildren: () => this.search.execute(query)
    });
  }
}
