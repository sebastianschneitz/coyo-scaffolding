import {FormControl} from '@angular/forms';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {of, Subject} from 'rxjs';
import {FilepickerSelection} from './filepicker-selection';

describe('FilepickerSelection', () => {
  const event = jasmine.createSpyObj('event', ['preventDefault']);
  let items$: Subject<FilepickerItem[]>;
  let filePickerSelection: FilepickerSelection;

  function createFolder(name: string, children: FilepickerItem[]): FilepickerItem & jasmine.Spy {
    const folder = jasmine.createSpyObj(name, ['getChildren']);
    folder.isFolder = true;
    folder.getChildren.and.returnValue(of(children));
    return folder;
  }

  function createFile(id: string): FilepickerItem {
    return {id, isFolder: false} as FilepickerItem;
  }

  beforeEach(() => {
    items$ = new Subject() as Subject<FilepickerItem[]>;
    filePickerSelection = new FilepickerSelection(items$);
  });

  it('should initialize correctly', () => {
    // given
    const fileItem = {isFolder: false, id: 'a1'} as FilepickerItem;

    // when
    items$.next([fileItem]);

    // then
    expect(filePickerSelection.currentItems).toEqual([fileItem]);
    expect(filePickerSelection.selectAllControl).toEqual(jasmine.any(FormControl));
  });

  it('should not create formControl for a file', () => {
    // given
    const fileItem = createFile('a1');
    const fileItem2 = createFile('a2');
    const fileItem3 = createFile('a3');

    // when
    items$.next([fileItem, fileItem2, fileItem3]);

    // then
    expect(filePickerSelection.selectControls[fileItem.id]).toEqual(jasmine.any(FormControl));
    expect(filePickerSelection.selectControls[fileItem2.id]).toEqual(jasmine.any(FormControl));
    expect(filePickerSelection.selectControls[fileItem3.id]).toEqual(jasmine.any(FormControl));
  });

  it('should not create formControl for a folder', () => {
    // given
    const folder1 = createFolder('folder1', []);

    // when
    items$.next([folder1]);

    // then
    expect(filePickerSelection.selectControls['folder1']).toBeUndefined();
  });

  it('should not remove already created form controls', () => {
    // given
    filePickerSelection.selectControls = {
      a1: new FormControl(),
      a2: new FormControl()
    };
    const expectedControls = {...filePickerSelection.selectControls};
    filePickerSelection.selectedFiles = [];

    // when
    items$.next([{id: 'a1', isFolder: false}, {id: 'a2', isFolder: false}] as FilepickerItem[]);

    // then
    expect(filePickerSelection.selectControls).toEqual(expectedControls);
  });

  it('should count the number of files correctly', () => {
    // given
    const fileItem = createFile('a1');
    const fileItem2 = createFile('a2');
    const fileItem3 = createFile('a3');
    const folderItem = createFolder('folder1', []);
    filePickerSelection.currentItems = [fileItem, fileItem2, fileItem3, folderItem];

    // when / then
    expect(filePickerSelection.numberOfFilesInCurrentFolder).toBe(3);
  });

  it('should extend the select controls', () => {
    // given
    filePickerSelection.selectControls = {
      a1: new FormControl(),
      a2: new FormControl()
    };

    // when
    items$.next([createFile('a3'), createFile('a4')]);

    // then
    expect(filePickerSelection.selectControls['a1']).toEqual(jasmine.any(FormControl));
    expect(filePickerSelection.selectControls['a2']).toEqual(jasmine.any(FormControl));
    expect(filePickerSelection.selectControls['a3']).toEqual(jasmine.any(FormControl));
    expect(filePickerSelection.selectControls['a4']).toEqual(jasmine.any(FormControl));
  });

  it('should initialize selectAll with true when opening a folder with all files selected', () => {
    // given
    const mockFiles = [
      createFile('a1'), createFile('a2'), createFile('a3')
    ] as FilepickerItem[];
    filePickerSelection.selectControls = {
      a1: new FormControl(),
      a2: new FormControl(),
      a3: new FormControl()
    };
    filePickerSelection.selectControls['a1'].setValue(true);
    filePickerSelection.selectControls['a2'].setValue(true);
    filePickerSelection.selectControls['a3'].setValue(true);

    // when
    items$.next(mockFiles);

    // then
    expect(filePickerSelection.selectAllControl.value).toBe(true);
  });

  it('should initialize selectAll with false when opening a folder with all files deselected', () => {
    // given
    const mockFiles = [
      createFile('a1'), createFile('a2'), createFile('a3')
    ] as FilepickerItem[];
    filePickerSelection.selectControls = {
      a1: new FormControl(),
      a2: new FormControl(),
      a3: new FormControl()
    };
    filePickerSelection.selectControls['a1'].setValue(false);
    filePickerSelection.selectControls['a2'].setValue(false);
    filePickerSelection.selectControls['a3'].setValue(false);

    // when
    items$.next(mockFiles);

    // then
    expect(filePickerSelection.selectAllControl.value).toBe(false);
  });

  it('should set to checked on file select', () => {
    // given
    filePickerSelection.selectControls = {
      a1: new FormControl()
    };
    filePickerSelection.selectedFiles = [];

    // when
    filePickerSelection.toggleFileSelection(createFile('a1'));

    // then
    expect(filePickerSelection.selectControls['a1'].value).toBe(true);
  });

  it('should set to unchecked on file deselect', () => {
    // given
    filePickerSelection.selectControls = {
      a1: new FormControl()
    };
    const fileItem = createFile('a1');
    filePickerSelection.selectedFiles = [fileItem];
    filePickerSelection.selectControls[fileItem.id].setValue(true);

    // when
    filePickerSelection.toggleFileSelection(createFile(fileItem.id));

    // then
    expect(filePickerSelection.selectControls[fileItem.id].value).toBe(false);
  });

  it('should add checked file to selected files array ', () => {
    // given
    filePickerSelection.selectedFiles = [];
    const fileItem = createFile('a1');
    items$.next([fileItem]);

    // when
    filePickerSelection.selectControls['a1'].setValue(true);

    // then
    expect(filePickerSelection.selectedFiles.length).toBe(1);
    expect(filePickerSelection.selectedFiles[0]).toEqual(fileItem);
  });

  it('should remove unchecked file from selected files array', () => {
    // given
    const fileItem = createFile('a1');
    items$.next([fileItem]);
    filePickerSelection.selectedFiles = [fileItem];

    // when
    filePickerSelection.selectControls['a1'].setValue(false);

    // then
    expect(filePickerSelection.selectedFiles.length).toBe(0);
  });

  it('should select all files in the folder', () => {
    // given
    const fileItem1 = createFile('a1');
    const fileItem2 = createFile('a2');
    items$.next([fileItem1, fileItem2]);

    // when
    filePickerSelection.selectAllControl.setValue(true);
    filePickerSelection.toggleAllFileSelectionsInCurrentFolder();

    // then
    expect(filePickerSelection.selectControls['a1'].value).toBe(true);
    expect(filePickerSelection.selectControls['a2'].value).toBe(true);
    expect(filePickerSelection.selectedFiles).toContain(fileItem1);
    expect(filePickerSelection.selectedFiles).toContain(fileItem2);
    expect(filePickerSelection.selectedFiles.length).toBe(2);
  });

  it('should deselect all files in the directory', () => {
    // given
    const fileItem1 = createFile('a1');
    const fileItem2 = createFile('a2');
    items$.next([fileItem1, fileItem2]);

    // when
    filePickerSelection.selectAllControl.setValue(false);
    filePickerSelection.toggleAllFileSelectionsInCurrentFolder();
    // then
    expect(filePickerSelection.selectControls['a1'].value).toBe(false);
    expect(filePickerSelection.selectControls['a2'].value).toBe(false);
    expect(filePickerSelection.selectedFiles.length).toBe(0);
  });

  it('should check selectAll as soon as all files were selected', () => {
    // given
    const fileItem1 = createFile('a1');
    const fileItem2 = createFile('a2');
    const areAllFilesSelectedInCurrentFolder = spyOn(filePickerSelection, 'areAllFilesSelectedInCurrentFolder');

    // when
    areAllFilesSelectedInCurrentFolder.and.returnValue(true);
    items$.next([fileItem1, fileItem2]);

    // then
    expect(filePickerSelection.selectAllControl.value).toBe(true);
  });

  it('should check if at least one file is selected in the current folder', () => {
    // given
    const mockFiles = [
      createFile('a1'), createFile('a2'), createFile('a3')
    ] as FilepickerItem[];
    filePickerSelection.currentItems = mockFiles;
    filePickerSelection.selectControls = {
      a1: new FormControl(),
      a2: new FormControl(),
      a3: new FormControl()
    };

    // when
    filePickerSelection.selectControls['a1'].setValue(true);
    filePickerSelection.selectControls['a2'].setValue(true);
    filePickerSelection.selectControls['a3'].setValue(false);

    // then
    expect(filePickerSelection.isAtLeastOneFileInCurrentFolderSelected()).toBe(true);
  });

  it('should check if all files are selected in the current folder', () => {
    // given
    const mockFiles = [
      createFile('a1'), createFile('a2'), createFile('a3')
    ] as FilepickerItem[];
    filePickerSelection.currentItems = mockFiles;
    filePickerSelection.selectControls = {
      a1: new FormControl(),
      a2: new FormControl(),
      a3: new FormControl()
    };

    // when
    filePickerSelection.selectControls['a1'].setValue(true);
    filePickerSelection.selectControls['a2'].setValue(true);
    filePickerSelection.selectControls['a3'].setValue(true);

    // then
    expect(filePickerSelection.areAllFilesSelectedInCurrentFolder()).toBe(true);
  });

  it('should check if at least on but not all files are selected in current folder', () => {
    const isAtLeastOneFileInCurrentFolderSelected = spyOn(filePickerSelection, 'isAtLeastOneFileInCurrentFolderSelected');
    const areAllFilesSelectedInCurrentFolder = spyOn(filePickerSelection, 'areAllFilesSelectedInCurrentFolder');

    // when / then
    isAtLeastOneFileInCurrentFolderSelected.and.returnValue(true);
    areAllFilesSelectedInCurrentFolder.and.returnValue(false);
    expect(filePickerSelection.isAtLeastOneButNotAllFilesSelectedInCurrentFolder()).toBe(true);

    // when / then
    isAtLeastOneFileInCurrentFolderSelected.and.returnValue(false);
    areAllFilesSelectedInCurrentFolder.and.returnValue(true);
    expect(filePickerSelection.isAtLeastOneButNotAllFilesSelectedInCurrentFolder()).toBe(false);

    // when / then
    isAtLeastOneFileInCurrentFolderSelected.and.returnValue(true);
    areAllFilesSelectedInCurrentFolder.and.returnValue(true);
    expect(filePickerSelection.isAtLeastOneButNotAllFilesSelectedInCurrentFolder()).toBe(false);
  });
});
