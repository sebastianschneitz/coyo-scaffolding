import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BlogArticleHeadlineComponent} from './blog-article-headline.component';

describe('BlogArticleHeadlineComponent', () => {
  let component: BlogArticleHeadlineComponent;
  let fixture: ComponentFixture<BlogArticleHeadlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlogArticleHeadlineComponent]
    }).overrideTemplate(BlogArticleHeadlineComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogArticleHeadlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
