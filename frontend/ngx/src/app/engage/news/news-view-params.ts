/**
 * State parameters for the engage app blog article view.
 */
export interface NewsViewParams {
  senderId: string;
  appId: string;
  id: string;
  hideTeaserText?: boolean;
  hideHeadline?: boolean;
  hideTeaserImg?: boolean;
  hideSocialFeatures?: boolean;
  preferredLanguage?: string;
}
