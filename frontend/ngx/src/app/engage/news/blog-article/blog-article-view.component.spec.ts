import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {BlogArticle} from '@domain/blog-article/blog-article';
import {SenderService} from '@domain/sender/sender/sender.service';
import {NewsViewParams} from '../news-view-params';
import {BlogArticleViewComponent} from './blog-article-view.component';

describe('BlogArticleViewComponent', () => {
  let component: BlogArticleViewComponent;
  let fixture: ComponentFixture<BlogArticleViewComponent>;
  let senderService: jasmine.SpyObj<SenderService>;
  let authService: jasmine.SpyObj<AuthService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlogArticleViewComponent],
      providers: [{
        provide: SenderService,
        useValue: jasmine.createSpyObj('senderService', ['get'])
      }, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('authService', ['getUser'])
      }]
    }).overrideTemplate(BlogArticleViewComponent, '')
      .compileComponents();

    senderService = TestBed.get(SenderService);
    authService = TestBed.get(AuthService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogArticleViewComponent);
    component = fixture.componentInstance;
  });

  it('should request the sender and the author', () => {
    // given
    component.article = {
      senderId: 'sender-id'
    } as BlogArticle;

    // when
    fixture.detectChanges();

    // then
    expect(senderService.get).toHaveBeenCalledWith(component.article.senderId);
    expect(authService.getUser).toHaveBeenCalled();
  });

  it('should hide social features when flag is set', () => {
    // given
    component.article = {
      _permissions: {
        like: true,
        comment: true
      }
    } as BlogArticle;

    component.params = {
      hideSocialFeatures: true
    } as NewsViewParams;

    // when
    const result = component.showSocialFeatures();

    // then
    expect(result).toBeFalsy();
  });

  it('should hide social features when no permissions to comment or like', () => {
    // given
    component.article = {
      _permissions: {
        like: false,
        comment: false
      }
    } as BlogArticle;

    component.params = {
      hideSocialFeatures: false
    } as NewsViewParams;

    // when
    const result = component.showSocialFeatures();

    // then
    expect(result).toBeFalsy();
  });

  it('should show social features when no permissions to comment', () => {
    // given
    component.article = {
      _permissions: {
        like: false,
        comment: true
      }
    } as BlogArticle;

    component.params = {
      hideSocialFeatures: false
    } as NewsViewParams;

    // when
    const result = component.showSocialFeatures();

    // then
    expect(result).toBeTruthy();
  });

  it('should show social features when no permissions to like', () => {
    // given
    component.article = {
      _permissions: {
        like: true,
        comment: false
      }
    } as BlogArticle;

    component.params = {
      hideSocialFeatures: false
    } as NewsViewParams;

    // when
    const result = component.showSocialFeatures();

    // then
    expect(result).toBeTruthy();
  });

  it('should hide teaser image if not configured in article', () => {
    // given
    component.article = {
      showTeaserWithText: false,
      teaserImage: {}
    } as BlogArticle;

    component.params = {
      hideTeaserImg: false
    } as NewsViewParams;

    // when
    const result = component.showTeaserImage();

    // then
    expect(result).toBeFalsy();
  });

  it('should show teaser image if not configured in article', () => {
    // given
    component.article = {
      showTeaserWithText: true,
      teaserImage: {}
    } as BlogArticle;

    component.params = {
      hideTeaserImg: false
    } as NewsViewParams;

    // when
    const result = component.showTeaserImage();

    // then
    expect(result).toBeTruthy();
  });

  it('should hide teaser text if not configured in article', () => {
    // given
    component.article = {
      showTeaserWithText: false
    } as BlogArticle;

    component.params = {
      hideTeaserText: false
    } as NewsViewParams;

    // when
    const result = component.showTeaserText();

    // then
    expect(result).toBeFalsy();
  });

  it('should hide teaser text if not configured in article', () => {
    // given
    component.article = {
      showTeaserWithText: true
    } as BlogArticle;

    component.params = {
      hideTeaserText: false
    } as NewsViewParams;

    // when
    const result = component.showTeaserText();

    // then
    expect(result).toBeTruthy();
  });
});
