import {TestBed} from '@angular/core/testing';
import {EngageTermsViewHandlerService} from '@app/engage/terms/engage-terms-view-handler/engage-terms-view-handler.service';
import {StateService} from '@uirouter/core';

describe('EngageTermsViewHandlerService', () => {
  let service: EngageTermsViewHandlerService;
  let stateService: jasmine.SpyObj<StateService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EngageTermsViewHandlerService, {
        provide: StateService, useValue: jasmine.createSpyObj('stateService', ['go'])
      }]
    });
    service = TestBed.get(EngageTermsViewHandlerService);
    stateService = TestBed.get(StateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open the terms view state', () => {
    // when
    service.showTermsView();

    // then
    expect(stateService.go).toHaveBeenCalledWith('engage.terms');
  });
});
