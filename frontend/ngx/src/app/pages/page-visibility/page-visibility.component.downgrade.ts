import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {PageVisibilityComponent} from '@app/pages/page-visibility/page-visibility.component';

getAngularJSGlobal()
  .module('coyo.pages')
  .directive('coyoPageVisibility', downgradeComponent({
    component: PageVisibilityComponent,
    propagateDigest: false
  }));
