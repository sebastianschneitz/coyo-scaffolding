import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {SubscriberInfoComponent} from '@app/pages/subscriber-info/subscriber-info.component';

getAngularJSGlobal()
  .module('coyo.pages')
  .directive('coyoSubscriberInfo', downgradeComponent({
    component: SubscriberInfoComponent,
    propagateDigest: false
  }));
