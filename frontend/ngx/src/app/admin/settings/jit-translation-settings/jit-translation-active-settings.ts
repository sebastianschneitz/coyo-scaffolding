export interface JitTranslationActiveSettings {
  activeLanguages: string[];
}
