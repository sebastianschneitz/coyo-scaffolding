import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {TrustUrlPipe} from './trust-url.pipe';

describe('TrustUrlPipe', () => {

  it('should set url as trusted', () => {
    // given
    const sanitizer: jasmine.SpyObj<DomSanitizer> = jasmine
      .createSpyObj('sanitizer', ['bypassSecurityTrustResourceUrl']);
    const safeUrl = {} as SafeResourceUrl;
    sanitizer.bypassSecurityTrustResourceUrl.and.returnValue(safeUrl);
    const pipe = new TrustUrlPipe(sanitizer);

    // when
    const result = pipe.transform('Test');

    // then
    expect(sanitizer.bypassSecurityTrustResourceUrl).toHaveBeenCalledWith('Test');
    expect(result).toBe(safeUrl);
  });
});
