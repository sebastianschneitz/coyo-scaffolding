import {HtmlTagSearchResult} from '@shared/collapsible-content/html-tag-tree/html-tag-search-result';
import {HtmlTagTree} from '@shared/collapsible-content/html-tag-tree/html-tag-tree';

const expectEmptyObject = (tree: HtmlTagTree, textShouldBe: any) => {
  expect(tree.roots).toEqual([]);
  expect(tree.getContentIndexAt(999)).toEqual(0);
  expect(tree.getTextIndexAt(999)).toEqual(0);
  expect(tree.text).toEqual(textShouldBe);
  expect(tree.textLength).toEqual(0);
  expect(tree.contentLength).toEqual(0);
  expect(tree.contentIndexOf(' ')).toEqual(-1);
  expect(tree.getOpenNodesAt(999)).toEqual([]);
  expect(tree.tags).toEqual([]);
  expect(tree.isIndexInTag(999)).toBeFalsy();
  expect(tree.isIndexInScope(0)).toBeFalsy();
};

describe('HtmlTagAggregatorService', () => {

  it('should handle invalid input', () => {
    const tree1 = new HtmlTagTree(null);
    const tree2 = new HtmlTagTree(undefined);
    const tree3 = new HtmlTagTree('');
    expectEmptyObject(tree1, null);
    expectEmptyObject(tree2, undefined);
    expectEmptyObject(tree3, '');
  });

  it('should create html tag search results according to the input', () => {
    const input = 'abc <br> def<a href="http://www.google.com" id="test-id">link with image <img src="http://localhost:8080/myImage.jpg"></a> ghi\njkl';
    const tree = new HtmlTagTree(input);
    expect(tree.tags.length).toEqual(4);
    expect(tree.tags[0]).toEqual(new HtmlTagSearchResult('self-closing', 4, 8, 4, 'br', '<br>'));
    expect(tree.tags[1]).toEqual(new HtmlTagSearchResult('opening', 12, 57, 45, 'a', '<a href="http://www.google.com" id="test-id">'));
    expect(tree.tags[2]).toEqual(new HtmlTagSearchResult('self-closing', 73, 118, 45, 'img', '<img src="http://localhost:8080/myImage.jpg">'));
    expect(tree.tags[3]).toEqual(new HtmlTagSearchResult('closing', 118, 122, 4, 'a', '</a>'));
  });

  it('should create a correct tree structure', () => {
    const input = 'abc <br> def<a href="http://www.google.com" id="test-id">link with image <img src="http://localhost:8080/myImage.jpg"></a> ghi\njkl';
    const tree = new HtmlTagTree(input);
    expect(tree.roots.length).toBe(2);

    expect(tree.roots[0].openingTag).toEqual(new HtmlTagSearchResult('self-closing', 4, 8, 4, 'br', '<br>'));
    expect(tree.roots[0].closingTag).toEqual(new HtmlTagSearchResult('self-closing', 4, 8, 4, 'br', '<br>'));
    expect(tree.roots[0].children.length).toBe(0);

    expect(tree.roots[1].openingTag).toEqual(new HtmlTagSearchResult('opening', 12, 57, 45, 'a', '<a href="http://www.google.com" id="test-id">'));
    expect(tree.roots[1].closingTag).toEqual(new HtmlTagSearchResult('closing', 118, 122, 4, 'a', '</a>'));
    expect(tree.roots[1].children.length).toBe(1);

    expect(tree.roots[1].children[0].openingTag).toEqual(
      new HtmlTagSearchResult('self-closing', 73, 118, 45, 'img', '<img src="http://localhost:8080/myImage.jpg">'));
    expect(tree.roots[1].children[0].closingTag).toEqual(
      new HtmlTagSearchResult('self-closing', 73, 118, 45, 'img', '<img src="http://localhost:8080/myImage.jpg">'));
    expect(tree.roots[1].children[0].children.length).toBe(0);
  });

  it('should correctly transform content-length to text-length', () => {
    const input = 'abc <br> def<a href="http://www.google.com" id="test-id">link with image <img src="http://localhost:8080/myImage.jpg"></a> ghi\njkl';
    const tree = new HtmlTagTree(input);

    expect(tree.getTextIndexAt(11)).toBe(7);
    expect(tree.getTextIndexAt(62)).toBe(13);
    expect(tree.getTextIndexAt(130)).toBe(32);
  });

  it('should correctly transform text-length to content-length', () => {
    const input = 'abc <br> def<a href="http://www.google.com" id="test-id">link with image <img src="http://localhost:8080/myImage.jpg"></a> ghi\njkl';
    const tree = new HtmlTagTree(input);

    expect(tree.getContentIndexAt(7)).toBe(11);
    expect(tree.getContentIndexAt(13)).toBe(62);
    expect(tree.getContentIndexAt(32)).toBe(130);
  });

  it('it should correctly find strings', () => {
    const input = 'com <br> goo<a href="http://www.google.com" id="test-id">http idcomwwwww <img src="http://localhost:8080/myImage.jpg"></a> ghi\njpg';
    const tree = new HtmlTagTree(input);

    expect(tree.contentIndexOf('com')).toBe(0);
    expect(tree.contentIndexOf('goo')).toBe(9);
    expect(tree.contentIndexOf('id')).toBe(62);
    expect(tree.contentIndexOf('com', 10)).toBe(64);
    expect(tree.contentIndexOf('www')).toBe(67);
    expect(tree.contentIndexOf('jpg')).toBe(127);
    expect(tree.contentIndexOf('id', 65)).toBe(-1);
  });

  it('should correctly tell if an index is within a tag', () => {
    const input = 'com <br> goo<a href="http://www.google.com" id="test-id">http idcomwwwww <img src="http://localhost:8080/myImage.jpg"></a> ghi\njpg';
    const tree = new HtmlTagTree(input);

    expect(tree.isIndexInTag(0)).toBeFalsy();
    expect(tree.isIndexInTag(3)).toBeFalsy();
    expect(tree.isIndexInTag(4)).toBeTruthy();
    expect(tree.isIndexInTag(7)).toBeTruthy();
    expect(tree.isIndexInTag(8)).toBeFalsy();

    expect(tree.isIndexInTag(72)).toBeFalsy();
    expect(tree.isIndexInTag(73)).toBeTruthy();
    expect(tree.isIndexInTag(121)).toBeTruthy();
    expect(tree.isIndexInTag(122)).toBeFalsy();
  });

  it('should correctly tell if an index is within the scope of a node', () => {
    const input = 'com <br> goo<a href="http://www.google.com" id="test-id">http idcomwwwww <img src="http://localhost:8080/myImage.jpg"></a> ghi\njpg';
    const tree = new HtmlTagTree(input);

    expect(tree.isIndexInScope(0)).toBeTruthy();
    expect(tree.isIndexInScope(129)).toBeTruthy();
    expect(tree.isIndexInScope(-1)).toBeFalsy();
    expect(tree.isIndexInScope(130)).toBeFalsy();

    expect(tree.roots[0].isIndexInScope(0)).toBeFalsy();
    expect(tree.roots[0].isIndexInScope(3)).toBeFalsy();
    expect(tree.roots[0].isIndexInScope(4)).toBeTruthy();
    expect(tree.roots[0].isIndexInScope(7)).toBeTruthy();
    expect(tree.roots[0].isIndexInScope(8)).toBeFalsy();

    expect(tree.roots[1].isIndexInScope(11)).toBeFalsy();
    expect(tree.roots[1].isIndexInScope(12)).toBeTruthy();
    expect(tree.roots[1].isIndexInScope(64)).toBeTruthy();
    expect(tree.roots[1].isIndexInScope(121)).toBeTruthy();
    expect(tree.roots[1].isIndexInScope(122)).toBeFalsy();

  });

  it('should give the correct text and content length', () => {
    const input = 'com <br> goo<a href="http://www.google.com" id="test-id">http idcomwwwww <img src="http://localhost:8080/myImage.jpg"></a> ghi\njpg';
    const tree = new HtmlTagTree(input);

    expect(tree.textLength).toBe(32);
    expect(tree.contentLength).toBe(130);
  });
});
