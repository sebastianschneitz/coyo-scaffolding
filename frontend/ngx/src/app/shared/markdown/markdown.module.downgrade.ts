import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {MarkdownService} from './markdown.service';

const angularJS = getAngularJSGlobal();

// downgrade module
angularJS
  .module('commons.markdown', ['coyo.base']);

// downgrade service
angularJS
  .module('commons.markdown')
  .factory('ngxMarkdownService', downgradeInjectable(MarkdownService));

// downgrade filter
angularJS
  .module('commons.markdown')
  .filter('markdown', ['$injector', markdownFilter]);

function markdownFilter($injector: any): (text: string, minimal?: boolean) => string  {
  const markdownService: MarkdownService = $injector.get('ngxMarkdownService');
  return (text, minimal) => markdownService.markdown(text, minimal);
}
