import {MarkdownPipe} from './markdown.pipe';
import {MarkdownService} from './markdown.service';

describe('MarkdownPipe', () => {
  let pipe: MarkdownPipe;
  let markdownService: jasmine.SpyObj<MarkdownService>;

  beforeEach(() => {
    markdownService = jasmine.createSpyObj('MarkdownService', ['markdown']);
    pipe = new MarkdownPipe(markdownService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('parse markdown', () => {
    // given
    const text = 'This is **Markdown**!';
    const minimal = false;

    markdownService.markdown.and.returnValue('This is <b>Markdown<b>!');

    // when
    const result = pipe.transform(text, minimal);

    // then
    expect(result).toEqual('This is <b>Markdown<b>!');
    expect(markdownService.markdown).toHaveBeenCalledWith(text, minimal);
  });
});
