import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import '../markdown/markdown.module.downgrade';
import {MarkdownPipe} from '../markdown/markdown.pipe';

/**
 * Module for markdown functionality.
 */
@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    MarkdownPipe
  ],
  declarations: [
    MarkdownPipe
  ]
})
export class MarkdownModule {}
