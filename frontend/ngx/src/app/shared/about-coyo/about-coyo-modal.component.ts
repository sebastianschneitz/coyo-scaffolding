import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {SessionStorageService} from '@core/storage/session-storage/session-storage.service';
import {Subject, Subscription} from 'rxjs';
import {bufferCount} from 'rxjs/operators';

/**
 * Renders a modal with the "About COYO" content.
 */
@Component({
  templateUrl: './about-coyo-modal.component.html',
  styleUrls: ['./about-coyo-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AboutCoyoModalComponent implements OnInit, OnDestroy {
  private static readonly KEY: string = 'heart-feature-toggle-activated';
  private subscription: Subscription;

  clicks: Subject<void> = new Subject<void>();
  isActive: boolean;

  constructor(private ref: ChangeDetectorRef,
              private sessionStorage: SessionStorageService,
              @Inject(MAT_DIALOG_DATA) public versionString: string) {}

  ngOnInit(): void {
    this.isActive = this.sessionStorage.getValue(AboutCoyoModalComponent.KEY, false);
    this.subscription = this.clicks
      .pipe(bufferCount(3))
      .subscribe(() => {
        this.sessionStorage.setValue(AboutCoyoModalComponent.KEY, true);
        this.isActive = true;
        this.ref.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
