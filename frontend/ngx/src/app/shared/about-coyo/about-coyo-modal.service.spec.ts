import {TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {MatDialogSize} from '@coyo/ui';
import {Ng1CoyoConfig} from '@root/typings';
import {AboutCoyoModalComponent} from '@shared/about-coyo/about-coyo-modal.component';
import {AboutCoyoModalService} from '@shared/about-coyo/about-coyo-modal.service';
import {NG1_COYO_CONFIG} from '@upgrade/upgrade.module';

describe('AboutCoyoModalService', () => {
  let aboutCoyoModalService: AboutCoyoModalService;
  let dialog: jasmine.SpyObj<MatDialog>;
  let coyoConfig: Ng1CoyoConfig;

  beforeEach( () => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialog,
          useValue: jasmine.createSpyObj('MatDialog', ['open'])
        },
        {
          provide: NG1_COYO_CONFIG,
          useValue: {
            versionString: () => '13.3.7-SNAPSHOT'
          }
        }
      ]
    });

    aboutCoyoModalService = TestBed.get(AboutCoyoModalService);
    dialog = TestBed.get(MatDialog);
    coyoConfig = TestBed.get(NG1_COYO_CONFIG);
  });

  it('should be created', () => {
    expect(aboutCoyoModalService).toBeTruthy();
  });

  it('should open the dialog and add class to panel', () => {
    // When
    aboutCoyoModalService.openAboutCoyoModal();

    // Then
    expect(dialog.open).toHaveBeenCalledWith(AboutCoyoModalComponent, {
      width: MatDialogSize.Medium,
      data: coyoConfig.versionString(),
      panelClass: 'about-coyo-modal'
    });
  });
});
