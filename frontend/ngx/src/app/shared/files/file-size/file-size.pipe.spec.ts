import {FileSize} from './file-size.pipe';

describe('FileSize', () => {
  let pipe: FileSize;

  beforeEach(() => {
    pipe = new FileSize();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transform fileSize', () => {
    const bytes = 1024;
    const result = pipe.transform(bytes);
    expect(result).toEqual('1.0 KB');
  });

  it('negative length', () => {
    const bytes = -8;
    const result = pipe.transform(bytes);
    expect(result).toEqual('-');
  });
});
