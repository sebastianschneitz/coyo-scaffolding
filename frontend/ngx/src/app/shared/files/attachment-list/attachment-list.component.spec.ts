import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AttachmentListComponent} from './attachment-list.component';

describe('AttachmentListComponent', () => {
  let component: AttachmentListComponent;
  let fixture: ComponentFixture<AttachmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AttachmentListComponent]
    }).overrideTemplate(AttachmentListComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit remove event for an attachment', () => {
    const attachment = {} as any;
    spyOn(component.attachmentRemoved, 'emit');

    // when
    component.removeAttachment(attachment);

    // then
    expect(component.attachmentRemoved.emit).toHaveBeenCalledWith(attachment);
  });
});
