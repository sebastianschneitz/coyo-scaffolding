import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {File} from '@domain/file/file';
import {IconCssService} from '@domain/icon/icon-css.service';
import {FileIconComponent} from './file-icon.component';

describe('FileIconComponent', () => {
  let component: FileIconComponent;
  let fixture: ComponentFixture<FileIconComponent>;
  let iconServiceMock: jasmine.SpyObj<IconCssService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileIconComponent ],
      providers: [
        {provide: IconCssService, useValue: jasmine.createSpyObj('iconCssService', ['getFileIconsCss'])}
      ]
    }).overrideTemplate(FileIconComponent, '<div></div>')
    .compileComponents();

    iconServiceMock = TestBed.get(IconCssService);
    iconServiceMock.getFileIconsCss.and.returnValue(['zmdi-coyo zmdi-coyo-image']);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get icons on changes', () => {
    // given
    component.file = {contentType: 'image/png'} as File;
    // when
    component.ngOnChanges();
    // then
    expect(component.icons).toEqual(['zmdi-coyo zmdi-coyo-image']);
  });
});
