import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {FileIconComponent} from './file-icon.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoNgxFileIcon', downgradeComponent({
    component: FileIconComponent,
    propagateDigest: false
  }));
