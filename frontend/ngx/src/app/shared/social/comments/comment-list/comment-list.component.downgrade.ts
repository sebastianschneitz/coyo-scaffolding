import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {CommentListComponent} from './comment-list.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoCommentList', downgradeComponent({
    component: CommentListComponent,
    propagateDigest: false
  }));
