/**
 * The status of the comment form.
 */
export enum CommentFormStatus {
  START, SUBMITTING, ERROR
}
