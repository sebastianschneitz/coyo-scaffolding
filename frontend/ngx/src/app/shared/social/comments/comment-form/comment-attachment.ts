import {TimelineFormAttachment} from '@shared/files/attachment-btn/types/timeline-form-attachment';
import {TimelineFormCoyoLibraryAttachment} from '@shared/files/attachment-btn/types/timeline-form-coyo-library-attachment';

/**
 * A generic comment attachment representation that contains the properties of usual attachments and
 * file library attachments as well as a model id property.
 */
export interface CommentAttachment extends TimelineFormAttachment, TimelineFormCoyoLibraryAttachment {
  modelId: string;
}
