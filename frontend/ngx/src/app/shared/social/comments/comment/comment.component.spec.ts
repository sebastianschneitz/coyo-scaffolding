import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {CommentService} from '@domain/comment/comment.service';
import {of} from 'rxjs';
import {CommentComponent} from './comment.component';

describe('CommentComponent', () => {
  let component: CommentComponent;
  let fixture: ComponentFixture<CommentComponent>;
  let commentService: jasmine.SpyObj<CommentService>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentComponent],
      providers: [{
        provide: CommentService,
        useValue: jasmine.createSpyObj('commentService', ['getOriginalAuthor'])
      }, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['observeScreenChange'])
      }]
    }).overrideTemplate(CommentComponent, '')
      .compileComponents();

    commentService = TestBed.get(CommentService);
    windowSizeService = TestBed.get(WindowSizeService);
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.MD));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set show author true', async(() => {
    // given
    component.comment = {
      id: 'comment-id',
      author: {}
    } as any;
    const originalAuthor = of({});
    commentService.getOriginalAuthor.and.returnValue(originalAuthor);

    // when
    component.showAuthor(true);

    // then
    component.originalAuthor$.subscribe(author => {
      expect(author).toBeDefined();
      expect(component.showOriginalAuthor).toBe(true);
    });
    expect(commentService.getOriginalAuthor).toHaveBeenCalledWith('comment-id');
  }));

  it('should set show author false', () => {
    // given
    component.comment = {
      id: 'comment-id',
      author: {}
    } as any;
    const originalAuthor = {} as any;
    commentService.getOriginalAuthor.and.returnValue(originalAuthor);

    // when
    component.showAuthor(false);

    // then
    expect(component.originalAuthor$).toBe(null);
    expect(component.showOriginalAuthor).toBe(false);
  });

  it('should emit event on edit comment', () => {
    // given
    let emitted = false;
    component.edit.subscribe(() => emitted = true);

    // when
    component.editComment();

    // then
    expect(emitted).toBe(true);
  });

  it('should emit event on delete comment', () => {
    // given
    let emitted = false;
    component.delete.subscribe(() => emitted = true);

    // when
    component.deleteComment();

    // then
    expect(emitted).toBe(true);
  });
});
