import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatDialogSize} from '@coyo/ui';
import {LikeState} from '@domain/like/like-state';
import {Likeable} from '@domain/like/likeable';
import {Sender} from '@domain/sender/sender';
import {LikesModalComponent} from '../likes-modal/likes-modal.component';

interface LikeCountInfo {
  othersCount: number;
  latest: string;
  isLiked: boolean;
  count: number;
  others: Sender[];
  total: Sender[];
  totalCount: number;
}

/**
 * Component showing information on who liked a target.
 */
@Component({
  selector: 'coyo-like-info',
  templateUrl: './like-info.component.html',
  styleUrls: ['./like-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LikeInfoComponent implements OnChanges, OnDestroy {
  private dialogRef: MatDialogRef<LikesModalComponent, void>;

  /**
   * The like target.
   */
  @Input() target: Likeable;

  /**
   * Flag for condensed information (only the count will be shown).
   */
  @Input() condensed: boolean;

  /**
   * The like state.
   */
  @Input() state: LikeState;

  info: LikeCountInfo;

  constructor(private dialog: MatDialog) {
  }

  /**
   * Open a modal showing all the likes for the target.
   */
  openLikes(): void {
    this.dialogRef = this.dialog.open<LikesModalComponent, Likeable>(LikesModalComponent, {
      width: MatDialogSize.Small,
      data: this.target
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.info = this.getCountAndLatest(this.state);
  }

  ngOnDestroy(): void {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }

  get tooltipOthers(): string {
    const names = this.state.total.slice(1, 6).map(sender => sender.displayName);
    return names.join('\n') + (this.state.othersCount > 6 ? '\n…' : '');
  }

  get tooltipOthersCondensed(): string {
    const names = this.state.total.slice(0, 5).map(sender => sender.displayName);
    return names.join('\n') + (this.state.totalCount > 5 ? '\n…' : '');
  }

  private getCountAndLatest(state: LikeState): LikeCountInfo {
    return state ? {
      count: state.totalCount,
      othersCount: state.othersCount,
      latest: state.others.length ? state.others[0].displayName : undefined,
      isLiked: state.isLiked,
      others: state.others,
      totalCount: state.totalCount,
      total: state.total
    } : null;
  }
}
