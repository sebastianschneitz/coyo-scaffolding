import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {LikeContainerComponent} from './like-container.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoLikeContainer', downgradeComponent({
    component: LikeContainerComponent,
    propagateDigest: false
  }));
