import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Like} from '@domain/like/like';
import {LikeService} from '@domain/like/like.service';
import {Page} from '@domain/pagination/page';
import {of, throwError} from 'rxjs';
import {LikesModalComponent} from './likes-modal.component';

describe('LikesModalComponent', () => {
  let component: LikesModalComponent;
  let fixture: ComponentFixture<LikesModalComponent>;
  let likeServiceMock: jasmine.SpyObj<LikeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LikesModalComponent],
      providers: [{
        provide: LikeService,
        useValue: jasmine.createSpyObj('likeService', ['getPage'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: {
          id: 'id',
          typeName: 'timeline-item'
        }
      }]
    }).overrideTemplate(LikesModalComponent, '')
      .compileComponents();

    likeServiceMock = TestBed.get(LikeService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikesModalComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.loading).toBeTruthy();
  });

  it('should load likes initially', fakeAsync(() => {
    // given
    const page = {
      content: [{
        sender: {id: 'sender-id'}
      }],
      totalElements: 1,
      numberOfElements: 1,
      totalPages: 1,
      last: true,
      first: true
    } as Page<Like>;
    likeServiceMock.getPage.and.returnValue(of(page));

    component.likes$.subscribe((likes: Like[]) => {
      expect(likes).toBe(page.content);
      expect(component.currentPage).toBe(page);
    });

    // when
    component.ngAfterViewInit();
    tick();

    // then
    // --> subscription
    expect(likeServiceMock.getPage).toHaveBeenCalled();
    expect(component.loading).toBeFalsy();
  }));

  it('should load next page and concat the likes', fakeAsync(() => {
    // given
    const firstPage = {
      content: [{
        sender: {id: 'sender-id1'}
      }],
      last: false
    } as Page<Like>;
    const secondPage = {
      content: [{
        sender: {id: 'sender-id2'}
      }]
    } as Page<Like>;
    likeServiceMock.getPage.and.returnValues(of(firstPage), of(secondPage));
    let requestCount = 0;

    component.likes$.subscribe((likes: Like[]) => {
      requestCount++;
      if (requestCount === 2) {
        expect(likes[0].sender.id).toBe(firstPage.content[0].sender.id);
        expect(likes[1].sender.id).toBe(secondPage.content[0].sender.id);
        expect(component.currentPage).toBe(secondPage);
      }
    });
    component.ngAfterViewInit();
    tick();

    // when
    component.loadMore();
    tick();

    // then
    expect(likeServiceMock.getPage).toHaveBeenCalledTimes(2);
    expect(component.loading).toBeFalsy();
  }));

  it('should set the loading flag when loading more', fakeAsync(() => {
    // given
    likeServiceMock.getPage.and.returnValues(of());
    component.currentPage = {last: false} as Page<Like>;

    // when
    component.loadMore();
    tick();

    // then
    expect(component.loading).toBeTruthy();
  }));

  it('should not load more when on the last page', () => {
    // given
    component.currentPage = {last: true} as Page<Like>;
    component.loading = false;
    component.likes$.subscribe();

    // when
    component.loadMore();

    // then
    expect(likeServiceMock.getPage).not.toHaveBeenCalled();
  });

  it('should not load more when loading', () => {
    // given
    component.currentPage = {last: false} as Page<Like>;
    component.loading = true;
    component.likes$.subscribe();

    // when
    component.loadMore();

    // then
    expect(likeServiceMock.getPage).not.toHaveBeenCalled();
  });

  it('should reset loading flag when request throws error', fakeAsync(() => {
    // given
    likeServiceMock.getPage.and.returnValue(throwError('test'));
    component.likes$.subscribe();

    // when
    component.ngAfterViewInit();
    tick();

    // then
    expect(component.loading).toBeFalsy();
  }));
});
