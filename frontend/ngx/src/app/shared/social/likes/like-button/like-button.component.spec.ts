import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {LikeState} from '@domain/like/like-state';
import {LikeButtonComponent} from './like-button.component';

describe('LikeButtonComponent', () => {
  let component: LikeButtonComponent;
  let fixture: ComponentFixture<LikeButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LikeButtonComponent]
    }).overrideTemplate(LikeButtonComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikeButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle when not liked', fakeAsync(() => {
    // given
    component.state = {isLiked: false} as LikeState;
    let toggled = false;
    component.toggle.subscribe((like: boolean) => {
      toggled = true;
      expect(like).toBeTruthy();
    });

    // when
    component.onToggle();

    // then
    expect(toggled).toBeTruthy();
  }));

  it('should toggle when liked', fakeAsync(() => {
    // given
    component.state = {isLiked: true} as LikeState;
    let toggled = false;
    component.toggle.subscribe((like: boolean) => {
      toggled = true;
      expect(like).toBeFalsy();
    });

    // when
    component.onToggle();

    // then
    expect(toggled).toBeTruthy();
  }));
});
