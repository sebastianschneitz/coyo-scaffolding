import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {SubscribeButtonComponent} from './subscribe-button.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoSubscribeButton', downgradeComponent({
    component: SubscribeButtonComponent,
    propagateDigest: false
  }));
