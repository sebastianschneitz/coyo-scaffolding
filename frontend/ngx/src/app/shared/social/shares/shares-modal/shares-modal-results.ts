import {Share} from '@domain/share/share';

/**
 * The shares modal result object.
 */
export interface SharesModalResults {
  deletedShares: Share[];
}
