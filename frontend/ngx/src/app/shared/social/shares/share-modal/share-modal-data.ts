import {User} from '@domain/user/user';
import {Observable} from 'rxjs';

/**
 * The data that needs to be provided to the ShareModalComponent.
 */
export interface ShareModalData {
  currentUser: User;
  parentIsPublic: boolean;
  targetId: string;
  typeName: string;
  canCreateStickyShare: Observable<boolean>;
}
