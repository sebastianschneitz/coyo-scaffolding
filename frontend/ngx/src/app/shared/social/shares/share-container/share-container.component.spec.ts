import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {ShareService} from '@domain/share/share.service';
import {of} from 'rxjs';
import {ShareContainerComponent} from './share-container.component';

describe('ShareContainerComponent', () => {
  let component: ShareContainerComponent;
  let fixture: ComponentFixture<ShareContainerComponent>;
  let shareService: jasmine.SpyObj<ShareService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShareContainerComponent],
      providers: [{
        provide: ShareService,
        useValue: jasmine.createSpyObj('shareService', ['getShareCount'])
      }]
    }).overrideTemplate(ShareContainerComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    shareService = TestBed.get(ShareService);

    fixture = TestBed.createComponent(ShareContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should count shares when a share is created', fakeAsync(() => {
    // given
    let count = 0;
    shareService.getShareCount.and.returnValue(of(5));
    component.count$.subscribe(newCount => {
      count = newCount.count;
    });

    // when
    component.shareCreated.emit();

    // then
    expect(count).toBe(5);
  }));

  it('should count shares when a share is deleted', fakeAsync(() => {
    // given
    let count = 0;
    shareService.getShareCount.and.returnValue(of(5));
    component.count$.subscribe(newCount => {
      count = newCount.count;
    });

    // when
    component.sharesDeleted.emit();

    // then
    expect(count).toBe(5);
  }));
});
