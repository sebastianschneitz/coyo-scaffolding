import {fakeAsync, tick} from '@angular/core/testing';
import {NgControl} from '@angular/forms';
import {WebPreviewService} from '@domain/preview/web-preview/web-preview.service';
import {ExtractUrlsDirective} from '@shared/forms/extract-urls/extract-urls.directive';

describe('WebPreviewDirective', () => {

  let linkPreviewService: jasmine.SpyObj<WebPreviewService>;
  let control: NgControl;
  const urls: string[] = ['a', 'b', 'c'];

  beforeEach(() => {
    linkPreviewService = jasmine.createSpyObj('linkPreviewService', ['extractUrls']);
    linkPreviewService.extractUrls.and.returnValue(urls);
    control = {value: 'test'} as unknown as NgControl;
  });

  it('should extract urls after space', () => {
    testKeyCode(32);
  });

  it('should extract urls after enter', () => {
    testKeyCode(13);
  });

  it('should extract urls after tab', () => {
    testKeyCode(9);
  });

  it('should extract urls after paste', fakeAsync(() => {
    const directive = new ExtractUrlsDirective(control, linkPreviewService);

    directive.urls.subscribe((emittedUrls: string[]) => {
      expect(emittedUrls).toBe(urls);
    });

    // when
    directive.onPaste();
    tick();

    // then
    expect(linkPreviewService.extractUrls).toHaveBeenCalledWith(control.value);
  }));

  function testKeyCode(code: number): void {
    // given
    const directive = new ExtractUrlsDirective(control, linkPreviewService);
    directive.urls.subscribe((emittedUrls: string[]) => {
      expect(emittedUrls).toBe(urls);
    });

    // when
    directive.onKeyDown({code: code} as unknown as KeyboardEvent);

    // then
    expect(linkPreviewService.extractUrls).toHaveBeenCalledWith(control.value);
  }
});
