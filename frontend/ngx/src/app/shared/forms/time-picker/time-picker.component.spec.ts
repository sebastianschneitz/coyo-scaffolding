import {SimpleChange, SimpleChanges} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslationService} from '@core/i18n/translation-service/translation.service';
import {TimePickerComponent} from './time-picker.component';

describe('TimePickerComponent', () => {
  let component: TimePickerComponent;
  let fixture: ComponentFixture<TimePickerComponent>;
  let translationService: jasmine.SpyObj<TranslationService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimePickerComponent],
      providers: [{
        provide: TranslationService,
        useValue: jasmine.createSpyObj('TranslationService', ['getActiveLanguage'])
      }]
    }).overrideTemplate(TimePickerComponent, '')
      .compileComponents();

    translationService = TestBed.get(TranslationService);
  }));

  beforeEach(() => {
    translationService.getActiveLanguage.and.returnValue('en');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimePickerComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given
    const initialDate = new Date('2020-01-01T13:37:00');
    component.time = initialDate;

    // when
    fixture.detectChanges();

    // then
    expect(translationService.getActiveLanguage).toHaveBeenCalled();
    expect(component.isMeridian).toBeTruthy();
    expect(component.currentDateTime).toBe(initialDate);
    expect(component.currentDateTimeString).toBe('01:37 pm');
  });

  it('should set value on change', () => {
    // given
    component.currentDateTime = new Date('2024-01-01T13:37:00');
    component.currentDateTimeString = '01:37 pm';
    const expectedNewDateTime = new Date('2024-01-01T12:34:00');
    const changes = {
      time: {
        previousValue: component.currentDateTime,
        currentValue: expectedNewDateTime,
        firstChange: false
      } as SimpleChange
    } as SimpleChanges;

    // when
    component.ngOnChanges(changes);

    // then
    expect(component.currentDateTime).toBe(expectedNewDateTime);
    expect(component.currentDateTimeString).toBe('12:34 pm');
  });

  it('should not set value on change if first change', () => {
    // given
    const expectedNewDateTime = new Date('2024-01-01T13:37:00');
    component.currentDateTime = expectedNewDateTime;
    component.currentDateTimeString = '01:37 pm';
    const changes = {
      time: {
        previousValue: null,
        currentValue: new Date('2024-01-01T13:37:01'),
        firstChange: true
      } as SimpleChange
    } as SimpleChanges;

    // when
    component.ngOnChanges(changes);

    // then
    expect(component.currentDateTime).toBe(expectedNewDateTime);
    expect(component.currentDateTimeString).toBe('01:37 pm');
  });

  it('should close popover on ESC key press', () => {
    // given
    component.showPopover = true;

    // when
    component.closeOnEsc();

    // then
    expect(component.showPopover).toBeFalsy();
  });

  it('should close popover on outside click', () => {
    // given
    component.showPopover = true;
    component.input = {
      nativeElement: {
        contains: () => {
        },
        value: ''
      }
    };
    component.popover = {
      nativeElement: {
        contains: () => {
        }
      }
    };
    spyOn(component.input.nativeElement, 'contains');
    spyOn(component.popover.nativeElement, 'contains');
    component.input.nativeElement.contains.and.returnValue(false);
    component.popover.nativeElement.contains.and.returnValue(false);

    // when
    component.closeOnOutsideClick(new MouseEvent('click', {}));

    // then
    expect(component.showPopover).toBeFalsy();
  });

  it('should emit a date on time input as string', () => {
    // given
    const initialDate = new Date('2020-01-01T12:00:00');
    const expectedDate = new Date(initialDate.getTime());
    expectedDate.setHours(13);
    expectedDate.setMinutes(23);
    component.time = initialDate;
    spyOn(component.timeChange, 'emit');
    fixture.detectChanges();

    // when
    component.onInputChange('01:23 pm');

    // then
    expect(component.timeChange.emit).toHaveBeenCalled();
    expect(component.currentDateTime.getHours()).toBe(expectedDate.getHours());
    expect(component.currentDateTime.getMinutes()).toBe(expectedDate.getMinutes());
  });

  it('should keep the date', () => {
    // given
    const initialDate = new Date('2120-01-01T12:00:00');
    const expectedDate = new Date(initialDate.getTime());
    expectedDate.setHours(14);
    expectedDate.setMinutes(30);
    component.time = initialDate;
    spyOn(component.timeChange, 'emit');
    fixture.detectChanges();

    // when
    component.onInputChange('02:30 pm');

    // then
    expect(component.timeChange.emit).toHaveBeenCalled();
    expect(component.currentDateTime.getFullYear()).toBe(expectedDate.getFullYear());
    expect(component.currentDateTime.getMonth()).toBe(expectedDate.getMonth());
    expect(component.currentDateTime.getDay()).toBe(expectedDate.getDay());
    expect(component.currentDateTime.getHours()).toBe(expectedDate.getHours());
    expect(component.currentDateTime.getMinutes()).toBe(expectedDate.getMinutes());
  });

  it('should emit a date on time input via date selection', () => {
    // given
    const initialDate = new Date('2020-01-01T12:00:00');
    const expectedDate = new Date(initialDate.getTime());
    expectedDate.setHours(13);
    expectedDate.setMinutes(23);
    component.time = initialDate;
    spyOn(component.timeChange, 'emit');
    fixture.detectChanges();

    // when
    component.onPickerChange(expectedDate);

    // then
    expect(component.timeChange.emit).toHaveBeenCalled();
    expect(component.currentDateTimeString).toBe('01:23 pm');
  });

  it('should reset input value', () => {
    // given
    component.currentDateTimeString = '01:37 pm';
    component.input = {
      nativeElement: {
        value: ''
      }
    };

    // when
    component.resetInputValue();

    // then
    expect(component.input.nativeElement.value).toBe('01:37 pm');
  });
});
