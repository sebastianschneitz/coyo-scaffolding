import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DateAdapter} from '@angular/material/core';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {TranslationService} from '@core/i18n/translation-service/translation.service';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {of} from 'rxjs';
import {DatePickerComponent} from './date-picker.component';

describe('DatePickerComponent', () => {
  let component: DatePickerComponent;
  let fixture: ComponentFixture<DatePickerComponent>;
  let translationService: jasmine.SpyObj<TranslationService>;
  let dateAdapter: jasmine.SpyObj<DateAdapter<Date>>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ DatePickerComponent ],
        providers: [{
          provide: TranslationService,
          useValue: jasmine.createSpyObj('TranslationService', ['getActiveLanguage'])
        }, {
          provide: DateAdapter,
          useValue: jasmine.createSpyObj('DateAdapter', ['setLocale'])
        }, {
          provide: WindowSizeService,
          useValue: jasmine.createSpyObj('WindowSizeService', ['observeScreenChange'])
        }]
      })
      .overrideTemplate(DatePickerComponent, '')
      .compileComponents();

    translationService = TestBed.get(TranslationService);
    dateAdapter = TestBed.get(DateAdapter);
    windowSizeService = TestBed.get(WindowSizeService);
  }));

  beforeEach(() => {
    translationService.getActiveLanguage.and.returnValue('en');
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.XS));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatePickerComponent);
    component = fixture.componentInstance;
    component.datepickerInput = {
      nativeElement: {
        value: 'initial value',
        dispatchEvent: () => {}
      }
    };
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(translationService.getActiveLanguage).toHaveBeenCalled();
    expect(dateAdapter.setLocale).toHaveBeenCalled();
    expect(windowSizeService.observeScreenChange).toHaveBeenCalled();
    expect(component.datepickerInput.nativeElement.value).toBe('initial value');
  });

  it('should emit value if value changed', () => {
    // given
    const date = new Date('2018-10-18T13:37:00');
    const expectedDate = {value: date} as MatDatepickerInputEvent<Date>;
    spyOn(component.dateChange, 'emit');

    // when
    component.datepickerValueChange(expectedDate);

    // then
    expect(component.dateChange.emit).toHaveBeenCalledWith(date);
  });

  it('should reset input value', () => {
    // given
    fixture.detectChanges();
    spyOn(component.datepickerInput.nativeElement, 'dispatchEvent');

    // when
    component.datepickerInput.nativeElement.value = '';
    component.resetInputValue();

    // then
    expect(component.datepickerInput.nativeElement.value).toBe('initial value');
    expect(component.datepickerInput.nativeElement.dispatchEvent).toHaveBeenCalledWith(new Event('change'));
  });
});
