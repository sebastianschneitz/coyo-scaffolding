import {Ng1StateLockService} from '@root/typings';
import {DownloadDirective} from './download.directive';

describe('DownloadDirective', () => {
  it('should download a file with the given link', () => {
    // given
    const windowService = {} as any;
    const stateLockService: jasmine.SpyObj<Ng1StateLockService> =
      jasmine.createSpyObj('stateLockService', ['ignoreLock']);
    stateLockService.ignoreLock.and.callFake((callback: () => void) => callback());

    const directive = new DownloadDirective(windowService, stateLockService);
    directive.coyoDownload = '\'download-link\'';
    const event = {} as Event;

    // when
    directive.onClick(event);

    // then
    expect(windowService.location).toBe('download-link');
    expect(event['handledByAngular']).toBeTruthy();
  });
});
