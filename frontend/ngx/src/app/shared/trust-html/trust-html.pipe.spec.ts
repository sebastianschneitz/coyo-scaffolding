import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {TrustHtmlPipe} from './trust-html.pipe';

describe('TrustHtmlPipe', () => {

  it('should set html as trusted', () => {
    // given
    const sanitizer: jasmine.SpyObj<DomSanitizer> = jasmine
      .createSpyObj('sanitizer', ['bypassSecurityTrustHtml']);
    const safeHtml = {} as SafeHtml;
    sanitizer.bypassSecurityTrustHtml.and.returnValue(safeHtml);
    const pipe = new TrustHtmlPipe(sanitizer);

    // when
    const result = pipe.transform('Test');

    // then
    expect(sanitizer.bypassSecurityTrustHtml).toHaveBeenCalledWith('Test');
    expect(result).toBe(safeHtml);
  });
});
