import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {User} from '@domain/user/user';
import {MentionComponent} from './mention.component';

describe('MentionComponent', () => {
  let component: MentionComponent;
  let fixture: ComponentFixture<MentionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MentionComponent]
    }).overrideTemplate(MentionComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentionComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select', () => {
    // given
    spyOn(component.selected, 'emit');
    const user = {slug: 'slug'} as User;

    // when
    component.select(user);

    // then
    expect(component.selected.emit).toHaveBeenCalledWith(user.slug);
  });
});
