import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {SelectFileComponent} from '@shared/select-file/select-file.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoSelectFile', downgradeComponent({
    component: SelectFileComponent,
    propagateDigest: false
  }));
