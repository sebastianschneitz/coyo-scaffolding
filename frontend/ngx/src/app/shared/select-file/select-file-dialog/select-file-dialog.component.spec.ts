import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SelectFileDialogComponent} from './select-file-dialog.component';

describe('SelectFileDialogComponent', () => {
  let component: SelectFileDialogComponent;
  let fixture: ComponentFixture<SelectFileDialogComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<SelectFileDialogComponent>>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectFileDialogComponent],
      providers: [{
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('ref', ['close'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: {

        }
      }]
    }).overrideTemplate(SelectFileDialogComponent, '')
      .compileComponents();

    dialogRef = TestBed.get(MatDialogRef);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectFileDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should pass selected files on close', () => {
    // given
    component.selectedFiles = [];

    // when
    component.saveAndClose();

    // then
    expect(dialogRef.close).toHaveBeenCalledWith(component.selectedFiles);
  });

  it('should store the selected files in the component', () => {
    // given
    component.selectedFiles = null;

    // when
    component.changed([]);

    // then
    expect(component.selectedFiles).toEqual([]);
  });
});
