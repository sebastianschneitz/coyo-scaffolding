import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Document} from '@domain/file/document';
import {Sender} from '@domain/sender/sender';
import {CropSettings, SelectFileOptions} from '@shared/select-file/select-file-options';

/**
 * Select file dialog that displays the file library so the user can select a file (or more).
 *
 * Important note: This component is not using ChangeDetectionStrategy.OnPush because it is using an upgraded directive
 * from angularJS: the file library. Default strategy is required in this case.
 */
@Component({
  selector: 'coyo-select-file-dialog',
  templateUrl: './select-file-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class SelectFileDialogComponent {

  selectedFiles: Document[];

  constructor(private dialogRef: MatDialogRef<SelectFileDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: SelectFileDialogData) {
    this.selectedFiles = data.selectedFiles;
  }

  saveAndClose(): void {
    this.dialogRef.close(this.selectedFiles);
  }

  changed(selectedFiles: Document[]): void {
    this.selectedFiles = selectedFiles;
  }
}

interface SelectFileDialogData {
  selectedFiles: Document[];
  sender: Sender;
  options: SelectFileOptions;
  cropSettings: CropSettings;
}
