import {MatIcon} from '@angular/material/icon';
import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('matIcon', downgradeComponent({component: MatIcon}));
