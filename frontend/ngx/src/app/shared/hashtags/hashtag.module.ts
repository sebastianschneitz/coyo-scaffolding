import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import '../hashtags/hashtag.service.downgrade';
import {HashtagsPipe} from '../hashtags/hashtags.pipe';
import '../hashtags/hashtags.pipe.downgrade';

/**
 * Module for hashtag functionality.
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    HashtagsPipe
  ],
  exports: [
    HashtagsPipe
  ]
})
export class HashtagModule {}
