import {TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import {HashtagService} from './hashtag.service';

describe('HashtagService', () => {
  let authService: jasmine.SpyObj<AuthService>;
  let stateService: jasmine.SpyObj<IStateService>;
  let hashtagService: HashtagService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HashtagService, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('Ng1AuthService', ['canUseHashtags'])
      }, {
        provide: NG1_STATE_SERVICE,
        useValue: jasmine.createSpyObj('StateService', ['href'])
      }]
    });

    authService = TestBed.get(AuthService);
    stateService = TestBed.get(NG1_STATE_SERVICE);
    hashtagService = TestBed.get(HashtagService);
  });

  beforeEach(() => {
    authService.canUseHashtags.and.returnValue(true);
    stateService.href.and.callFake((state: string, opts: object) => state + '(' + JSON.stringify(opts) + ')');
  });

  it('should be created', () => {
    expect(hashtagService).toBeTruthy();
  });

  it('should handle empty values', () => {
    expect(hashtagService.hashtags(undefined)).toBeUndefined();
    expect(hashtagService.hashtags(null)).toEqual(null);
    expect(hashtagService.hashtags('')).toEqual('');
  });

  it('should handle tags correctly', () => {
    expect(hashtagService.hashtags('#h')).toEqual('<a href="main.search({"term":"#h"})">#h</a>');
    expect(hashtagService.hashtags('#ho')).toEqual('<a href="main.search({"term":"#ho"})">#ho</a>');
    expect(hashtagService.hashtags('#Ho')).toEqual('<a href="main.search({"term":"#Ho"})">#Ho</a>');
    expect(hashtagService.hashtags('#HO')).toEqual('<a href="main.search({"term":"#HO"})">#HO</a>');
    expect(hashtagService.hashtags('#hoHo')).toEqual('<a href="main.search({"term":"#hoHo"})">#hoHo</a>');
    expect(hashtagService.hashtags('#höhö')).toEqual('<a href="main.search({"term":"#höhö"})">#höhö</a>');
    expect(hashtagService.hashtags('#hoho123')).toEqual('<a href="main.search({"term":"#hoho123"})">#hoho123</a>');
    expect(hashtagService.hashtags('#123hoho')).toEqual('<a href="main.search({"term":"#123hoho"})">#123hoho</a>');
    expect(hashtagService.hashtags('#12345')).toEqual('<a href="main.search({"term":"#12345"})">#12345</a>');
    expect(hashtagService.hashtags('#mañana')).toEqual('<a href="main.search({"term":"#mañana"})">#mañana</a>');
  });

  it('should handle multiple tags correctly', () => {
    expect(hashtagService.hashtags('#ho #ho'))
      .toEqual('<a href="main.search({"term":"#ho"})">#ho</a> <a href="main.search({"term":"#ho"})">#ho</a>');
  });

  it('should handle tags\' contexts correctly', () => {
    expect(hashtagService.hashtags('start #ho end')).toEqual('start <a href="main.search({"term":"#ho"})">#ho</a> end');
    expect(hashtagService.hashtags('<em>#ho</em>')).toEqual('<em><a href="main.search({"term":"#ho"})">#ho</a></em>');
  });

  it('should handle invalid tags correctly', () => {
    expect(hashtagService.hashtags('#')).toEqual('#');
    expect(hashtagService.hashtags('##')).toEqual('##');
    expect(hashtagService.hashtags('#ho#ho')).toEqual('<a href="main.search({"term":"#ho"})">#ho</a>#ho');
    expect(hashtagService.hashtags('#ho-ho')).toEqual('<a href="main.search({"term":"#ho"})">#ho</a>-ho');
    expect(hashtagService.hashtags('#ho_ho')).toEqual('<a href="main.search({"term":"#ho"})">#ho</a>_ho');
    expect(hashtagService.hashtags('#ho$ho')).toEqual('<a href="main.search({"term":"#ho"})">#ho</a>$ho');
  });

  it('should not handle tags if hashtags are disabled', () => {
    authService.canUseHashtags.and.returnValue(false);
    expect(hashtagService.hashtags('#ho')).toEqual('#ho');
  });

  it('should return true if text just contains hashtag without any other char', () => {
    // given
    const text = '#coyo';

    // when
    const result = hashtagService.isConcludedHashtag(text);

    // then
    expect(result).toBeTruthy();
  });

  it('should return false if charSequence contains other char', () => {
    // given
    const text = '#coyo ladida';

    // when
    const result = hashtagService.isConcludedHashtag(text);

    // then
    expect(result).toBeFalsy();
  });

  it('should return false if charSequence contains no #', () => {
    // given
    const text = 'coyo';

    // when
    const result = hashtagService.isConcludedHashtag(text);

    // then
    expect(result).toBeFalsy();
  });
});
