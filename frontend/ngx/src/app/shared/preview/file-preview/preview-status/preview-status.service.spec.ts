import {TestBed} from '@angular/core/testing';

import {SocketService} from '@core/socket/socket.service';
import {CapabilitiesService} from '@domain/capability/capabilities/capabilities.service';
import {FileService} from '@domain/file/file/file.service';
import {FilePreview} from '@domain/preview/file-preview';
import {FilePreviewStatus} from '@shared/preview/file-preview/file-preview-status';
import {EMPTY, of} from 'rxjs';
import {PreviewStatusService} from './preview-status.service';
import SpyObj = jasmine.SpyObj;

describe('PreviewStatusService', () => {
  let previewStatusService: PreviewStatusService;
  let fileService: SpyObj<FileService>;
  let socketService: SpyObj<SocketService>;
  let capabilitiesService: SpyObj<CapabilitiesService>;

  const url = 'url';
  const groupId = 'groupId';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PreviewStatusService, {
        provide: FileService,
        useValue: jasmine.createSpyObj('fileService', ['getPreviewStatus'])
      }, {
        provide: SocketService,
        useValue: jasmine.createSpyObj('socketService', ['listenTo$'])
      }, {
        provide: CapabilitiesService,
        useValue: jasmine.createSpyObj('capabilitiesService', ['imgAvailable'])
      }]
    });
    previewStatusService = TestBed.get(PreviewStatusService);
    fileService = TestBed.get(FileService);
    fileService.getPreviewStatus.and.returnValue(EMPTY);
    socketService = TestBed.get(SocketService);
    socketService.listenTo$.and.returnValue(EMPTY);
    capabilitiesService = TestBed.get(CapabilitiesService);
  });

  it('should be created', () => {
    expect(previewStatusService).toBeTruthy();
  });

  it('should check if preview can be generated', done => {
    // given
    const filePreview = {storage: 'LOCAL_BLOB_STORAGE', contentType: 'image/jpeg'} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(false));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(previewStatus => {
        expect(capabilitiesService.imgAvailable).toHaveBeenCalledWith(filePreview.contentType);
        expect(socketService.listenTo$).not.toHaveBeenCalled();
        expect(fileService.getPreviewStatus).not.toHaveBeenCalled();
        expect(isCannotProcessStatus(previewStatus)).toBeTruthy();
        done();
      });
  });

  it('should not generate if file is GSuite file', done => {
    // given
    const filePreview = {storage: 'G_SUITE', contentType: 'image/jpeg'} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(previewStatus => {
        expect(capabilitiesService.imgAvailable).toHaveBeenCalledWith(filePreview.contentType);
        expect(socketService.listenTo$).not.toHaveBeenCalled();
        expect(fileService.getPreviewStatus).not.toHaveBeenCalled();
        expect(isCannotProcessStatus(previewStatus)).toBeTruthy();
        done();
      });
  });

  it('should subscribe to preview status', done => {
    // given
    const filePreview = {storage: 'LOCAL_BLOB_STORAGE', contentType: 'image/jpeg'} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));
    socketService.listenTo$.and.returnValue(of({content: {status: 'SUCCESS'}}));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(status => {
        expect(socketService.listenTo$).toHaveBeenCalled();
        expect(fileService.getPreviewStatus).toHaveBeenCalled();
        expect(status.previewAvailable).toEqual(true);
        done();
      });
  });

  it('should return CANNOT_PROCESS as default status', done => {
    // given
    const filePreview = {storage: 'LOCAL_BLOB_STORAGE', contentType: 'image/jpeg'} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(false));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(status => {
        expect(status).toEqual({
          loading: false,
          conversionError: false,
          isProcessing: false,
          previewAvailable: false
        });
        done();
      });
  });

  it('should return status directly when retrieved by fileService', done => {
    // given
    const filePreview = {storage: 'LOCAL_BLOB_STORAGE', contentType: 'image/jpeg'} as FilePreview;
    const filePreviewStatus = {
      loading: false,
      conversionError: false,
      isProcessing: false,
      previewAvailable: true
    } as FilePreviewStatus;
    capabilitiesService.imgAvailable.and.returnValue(of(true));
    fileService.getPreviewStatus.and.returnValue(of(filePreviewStatus));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(status => {
        expect(status).toEqual(filePreviewStatus);
        done();
      });
  });

  it('should use url provided by file', done => {
    // given
    const filePreviewUrl = 'this-url-should-be-used';
    const filePreview = {
      id: 'id',
      storage: 'LOCAL_BLOB_STORAGE',
      contentType: 'image/jpeg',
      previewUrl: filePreviewUrl
    } as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));
    socketService.listenTo$.and.returnValue(of({content: {status: 'SUCCESS'}}));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(() => {
        expect(fileService.getPreviewStatus).toHaveBeenCalledWith(filePreviewUrl, groupId, filePreview.id);
        done();
      });
  });

  it('should use url fallback when not provided by the file preview', done => {
    // given
    const filePreview = {
      id: 'id',
      storage: 'LOCAL_BLOB_STORAGE',
      contentType: 'image/jpeg',
    } as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));
    socketService.listenTo$.and.returnValue(of({content: {status: 'SUCCESS'}}));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(() => {
        expect(fileService.getPreviewStatus).toHaveBeenCalledWith(url, groupId, filePreview.id);
        done();
      });
  });

  it('should return cannot process status', done => {
    // given
    const filePreview = {storage: 'LOCAL_BLOB_STORAGE', contentType: 'image/jpeg'} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));
    socketService.listenTo$.and.returnValue(of({content: {status: 'CANNOT_PROCESS'}}));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(status => {
        expect(status).toEqual({
          loading: false,
          conversionError: false,
          isProcessing: false,
          previewAvailable: false
        } as FilePreviewStatus);
        done();
      });
  });

  it('should return processing status', done => {
    // given
    const filePreview = {storage: 'LOCAL_BLOB_STORAGE', contentType: 'image/jpeg'} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));
    socketService.listenTo$.and.returnValue(of({content: {status: 'PROCESSING'}}));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(status => {
        expect(status).toEqual({
          loading: true,
          conversionError: false,
          isProcessing: true,
          previewAvailable: false
        } as FilePreviewStatus);
        done();
      });
  });

  it('should return failed status', done => {
    // given
    const filePreview = {storage: 'LOCAL_BLOB_STORAGE', contentType: 'image/jpeg'} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));
    socketService.listenTo$.and.returnValue(of({content: {status: 'FAILED'}}));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(status => {
        expect(status).toEqual({
          loading: false,
          conversionError: true,
          isProcessing: false,
          previewAvailable: false
        } as FilePreviewStatus);
        done();
      });
  });

  it('should return cannot process status as default', done => {
    // given
    const filePreview = {storage: 'LOCAL_BLOB_STORAGE', contentType: 'image/jpeg'} as FilePreview;
    capabilitiesService.imgAvailable.and.returnValue(of(true));
    socketService.listenTo$.and.returnValue(of({content: {status: 'SOME_OTHER_STATUS'}}));

    // when / then
    previewStatusService.getPreviewStatus$(filePreview, url, groupId)
      .subscribe(status => {
        expect(status).toEqual({
          loading: false,
          conversionError: false,
          isProcessing: false,
          previewAvailable: false
        } as FilePreviewStatus);
        done();
      });
  });

  function isCannotProcessStatus(status: FilePreviewStatus): boolean {
    return !status.previewAvailable && !status.isProcessing && !status.conversionError && !status.loading;
  }
});
