import {SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {FilePreview} from '@domain/preview/file-preview';
import {FileVideoPreviewComponent} from './file-video-preview.component';

describe('VideoPreviewComponent', () => {
  let component: FileVideoPreviewComponent;
  let fixture: ComponentFixture<FileVideoPreviewComponent>;
  let urlServiceMock: jasmine.SpyObj<UrlService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileVideoPreviewComponent ],
      providers: [
        {provide: UrlService, useValue: jasmine.createSpyObj('urlService', ['join', 'getBackendUrl', 'insertPathVariablesIntoUrl'])}
      ]
    }).overrideTemplate(FileVideoPreviewComponent, '')
    .compileComponents();

    urlServiceMock = TestBed.get(UrlService);
    urlServiceMock.join.and.callFake((parts: string) => parts.slice(1));
    urlServiceMock.getBackendUrl.and.returnValue('http://localhost:8080');
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileVideoPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should generate video url on change', () => {
    // given
    component.url = '/web/senders/{{groupId}}/documents/{{id}}';
    component.groupId = '12345';
    component.file = {contentType: 'video/mp4', id: '54321', fileId: '54321'} as FilePreview;
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');
    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();
    // then
    expect(component.videoUrl).toBe('http://localhost:8080/web/senders/12345/documents/54321/stream');
  });

  it('should generate G Suite video url on change', () => {
    // given
    component.file = {contentType: 'video/mp4', id: '54321', fileId: '54321', storage: 'G_SUITE'} as FilePreview;
    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();
    // then
    expect(component.videoUrl).toBe('https://drive.google.com/file/d/54321/preview');
  });
});
