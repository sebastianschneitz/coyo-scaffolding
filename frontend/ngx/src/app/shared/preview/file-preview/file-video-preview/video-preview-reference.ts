import {StorageType} from '@domain/attachment/storage';

export interface VideoPreviewReference {
  id: string;
  fileId: string;
  storage?: StorageType;
}
