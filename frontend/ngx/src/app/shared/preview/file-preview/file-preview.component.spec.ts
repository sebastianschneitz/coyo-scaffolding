import {SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {UrlService} from '@core/http/url/url.service';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {GSUITE} from '@domain/attachment/storage';
import {FileService} from '@domain/file/file/file.service';
import {FilePreview} from '@domain/preview/file-preview';
import {WINDOW} from '@root/injection-tokens';
import {FilePreviewTypeService} from '@shared/preview/file-preview/file-preview-type/file-preview-type.service';
import {PreviewType} from '@shared/preview/file-preview/file-preview-type/preview.type';
import {of} from 'rxjs';
import {FilePreviewOptions} from './file-preview-options';
import {FilePreviewStatus} from './file-preview-status';
import {FilePreviewComponent} from './file-preview.component';

describe('FilePreviewComponent', () => {
  let component: FilePreviewComponent;
  let fixture: ComponentFixture<FilePreviewComponent>;
  let windowSizeServiceMock: jasmine.SpyObj<WindowSizeService>;
  let urlServiceMock: jasmine.SpyObj<UrlService>;
  let googleApiService: jasmine.SpyObj<GoogleApiService>;
  let filePreviewTypeService: jasmine.SpyObj<FilePreviewTypeService>;
  let windowService: jasmine.SpyObj<Window>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilePreviewComponent],
      providers: [
        {
          provide: WindowSizeService,
          useValue: jasmine.createSpyObj('windowSizeService', ['getScreenSize', 'isXs', 'isSm', 'isMd', 'isLg', 'isRetina'])
        },
        {
          provide: UrlService,
          useValue: jasmine.createSpyObj('urlService', ['join', 'getBackendUrl', 'insertPathVariablesIntoUrl'])
        },
        {
          provide: GoogleApiService,
          useValue: jasmine.createSpyObj('googleApiService', ['appendAccessTokenParam', 'getFileContentRequestUrl'])
        },
        {
          provide: FileService,
          useValue: jasmine.createSpyObj('fileService', ['getImagePreviewUrl'])
        },
        {
          provide: FilePreviewTypeService,
          useValue: jasmine.createSpyObj('filePreviewTypeService', ['determinePreviewType', 'isInternalFile'])
        },
        {
          provide: WINDOW,
          useValue: jasmine.createSpyObj('windowService', ['setTimeout', 'clearTimeout'])
        }
      ]
    }).overrideTemplate(FilePreviewComponent, '<div></div>')
      .compileComponents();

    windowSizeServiceMock = TestBed.get(WindowSizeService);
    urlServiceMock = TestBed.get(UrlService);
    urlServiceMock.join.and.callFake((parts: string) => parts.slice(1));
    urlServiceMock.getBackendUrl.and.returnValue('http://localhost:8080');
    googleApiService = TestBed.get(GoogleApiService);
    windowService = TestBed.get(WINDOW);
    filePreviewTypeService = TestBed.get(FilePreviewTypeService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilePreviewComponent);
    component = fixture.componentInstance;
    component.options = {showPdfDesktop: true, showPdfMobile: true};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update status', () => {
    // given
    const status = {
      loading: false,
      isProcessing: false,
      conversionError: false,
      previewAvailable: false
    } as FilePreviewStatus;

    component.options = {hidePreviewGenerationInformation: false} as FilePreviewOptions;
    // when
    component.onStatusUpdated(status);
    // then
    expect(component.loading).toBe(false);
    expect(component.previewIsGenerating).toBe(false);
    expect(component.conversionFailed).toBe(false);
    expect(component.isPreviewAvailable()).toBe(false);
  });

  it('should enable cover image for gsuite pdfs on mobile', () => {
    // given
    component.url = '/link/to/google/pdf';
    component.file = {contentType: 'application/pdf', storage: GSUITE, previewUrl: '/pdf/preview/url'} as FilePreview;
    filePreviewTypeService.determinePreviewType.and.returnValue(of(PreviewType.GSUITE_PDF));
    component.options = {showPdfMobile: false};
    windowSizeServiceMock.isXs.and.returnValue(true);
    googleApiService.getFileContentRequestUrl.and.returnValue(of('http://someExportUrl?access_token=WITH_TOKEN'));
    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });
    fixture.detectChanges();

    // then
    expect(component.isPreviewAvailable()).toBe(true);
  });

  it('should show image previews for image type', () => {
    // given
    component.file = {} as FilePreview;
    component.previewType = PreviewType.IMAGE;

    // when
    const result = component.useImagePreview();

    // then
    expect(result).toBeTruthy();
  });

  it('should use image preview for PDF file on mobile', () => {
    // given
    component.options = {showPdfMobile: false};
    component.file = {} as FilePreview;
    component.previewType = PreviewType.PDF;
    windowSizeServiceMock.isXs.and.returnValue(true);

    // when
    const usePdfPreview = component.usePdfPreview();
    const useImagePreview = component.useImagePreview();

    // then
    expect(usePdfPreview).toBeFalsy();
    expect(useImagePreview).toBeTruthy();
  });

  it('should not show image previews for image type when preview is processing', () => {
    // given
    spyOn(component, 'isPreviewAvailable').and.returnValue(true);
    spyOn(component, 'isPreviewIsProcessing').and.returnValue(true);
    component.file = {contentType: 'image/png', id: '54321', fileId: '54321', length: 123} as FilePreview;

    // when
    const result = component.useImagePreview();

    // then
    expect(result).toBeFalsy();
  });

  it('should use image preview', () => {
    // given
    component.file = {} as FilePreview;
    component.previewType = PreviewType.IMAGE;

    // when
    const result = component.useImagePreview();

    // then
    expect(result).toBe(true);
  });

  it('should use video preview', () => {
    // given
    component.file = {} as FilePreview;
    component.previewType = PreviewType.VIDEO;

    // when
    const result = component.useVideoPreview();

    // then
    expect(result).toBeTruthy();
  });

  it('should stop loading when PDF viewer has completed loading', () => {
    // given
    component.loading = true;

    // when
    component.onPdfLoadingCompleted();

    // then
    expect(component.loading).toBe(false);
  });

  it('should stop loading when PDF viewer failed loading', () => {
    // given
    component.loading = true;

    // when
    component.onPdfLoadingFailed();

    // then
    expect(component.loading).toBe(false);
  });

  it('should generate pdf url on change', () => {
    // given
    component.url = '/web/senders/{{groupId}}/documents/{{id}}';
    component.groupId = '12345';
    component.file = {contentType: 'application/pdf'} as FilePreview;
    filePreviewTypeService.determinePreviewType.and.returnValue(of(PreviewType.PDF));
    urlServiceMock.insertPathVariablesIntoUrl.and.returnValue('/web/senders/12345/documents/54321');

    // when
    component.ngOnChanges({file: new SimpleChange(null, component.file, true)});
    fixture.detectChanges();

    // then
    expect(component.pdfSrc).toBe('http://localhost:8080/web/senders/12345/documents/54321?');
  });

  it('should use PDF preview on desktop', () => {
    // given
    component.options = {showPdfDesktop: true};
    component.file = {} as FilePreview;
    component.previewType = PreviewType.PDF;
    windowSizeServiceMock.isLg.and.returnValue(true);

    // when
    const shouldShowPdfPreview = component.usePdfPreview();

    // then
    expect(shouldShowPdfPreview).toBeTruthy();
  });

  it('should use PDF preview on mobile', () => {
    // given
    component.options = {showPdfMobile: true};
    component.file = {} as FilePreview;
    component.previewType = PreviewType.PDF;
    windowSizeServiceMock.isXs.and.returnValue(true);

    // when
    const shouldShowPdfPreview = component.usePdfPreview();

    // then
    expect(shouldShowPdfPreview).toBeTruthy();
  });

  it('should return true if preview is available', () => {
    // given
    spyOn(component, 'isPreviewAvailable').and.returnValue(true);
    component.loading = false;
    component.file = {} as FilePreview;

    // when
    const result = component.isPreviewAvailable();

    // then
    expect(result).toBe(true);
  });

  it('should show preview generation information', () => {
    // given
    component.options = {hidePreviewGenerationInformation: false} as FilePreviewOptions;

    // when
    const result = component.showPreviewGenerationInformation();

    // then
    expect(result).toBe(true);
  });

  it('should not show preview generation information', () => {
    // given
    component.options = {hidePreviewGenerationInformation: true} as FilePreviewOptions;

    // when
    const result = component.showPreviewGenerationInformation();

    // then
    expect(result).toBe(false);
  });

  it('should get preview size lg as default', () => {
    // given
    component.size = null;
    windowSizeServiceMock.isRetina.and.returnValue(false);

    // when
    const result = component.getImagePreviewSize();

    // then
    expect(result).toBe('L');
  });

  it('should emit cannotProcess when preview conversion error occurs', () => {
    // given
    const id = 'id';
    const spy = jasmine.createSpy('subscribe spy');
    component.options = {hidePreviewGenerationInformation: true};
    component.file = {id: id} as FilePreview;
    component.cannotProcess.subscribe(spy);

    // when
    component.onStatusUpdated({conversionError: true} as FilePreviewStatus);

    // then
    expect(spy).toHaveBeenCalledWith(id);
  });

  it('should start a 1 minute timeout for G Suite files that should be exported as PDF', () => {
    // given
    const timeoutId = 123;
    component.file = {exportLinks: {'application/pdf': 'something'}} as FilePreview;
    filePreviewTypeService.determinePreviewType.and.returnValue(of(PreviewType.GSUITE_AS_PDF_EXPORTABLE_DOCUMENT));
    googleApiService.appendAccessTokenParam.and.returnValue(of('src'));
    windowService.setTimeout.and.callFake(() => timeoutId);

    // when
    component.ngOnChanges({file: new SimpleChange(null, component.file, true)});
    fixture.detectChanges();

    // then
    expect(windowService.setTimeout).toHaveBeenCalledWith(jasmine.any(Function), 60000);
    expect(component.externalFileLoadingTimeout).toBe(timeoutId);
  });

  it('should clear timeout on destroy', () => {
    // given
    const timeoutId = 123;
    component.externalFileLoadingTimeout = timeoutId;

    // when
    component.ngOnDestroy();

    // then
    expect(windowService.clearTimeout).toHaveBeenCalledWith(timeoutId);
  });

  it('should use full res image view for internal image files', () => {
    // given
    component.interactiveImageView = true;
    component.file = {} as FilePreview;
    component.previewType = PreviewType.IMAGE;
    filePreviewTypeService.isInternalFile.and.returnValue(true);

    // when
    const result = component.isFullResImageOverlayAvailable();

    // then
    expect(result).toBeTruthy();
  });

  it('should not use full res image view for GSuite images', () => {
    // given
    component.interactiveImageView = true;
    component.file = {} as FilePreview;
    component.previewType = PreviewType.GSUITE_IMAGE;
    filePreviewTypeService.isInternalFile.and.returnValue(false);

    // when
    const result = component.isFullResImageOverlayAvailable();

    // then
    expect(result).toBeFalsy();
  });

  it('should not use full res image view for GIF', () => {
    // given
    component.interactiveImageView = true;
    component.file = {contentType: 'image/gif'} as FilePreview;
    component.previewType = PreviewType.IMAGE;
    filePreviewTypeService.isInternalFile.and.returnValue(true);

    // when
    const result = component.isFullResImageOverlayAvailable();

    // then
    expect(result).toBeFalsy();
  });

  it('should not use full res image view on conversion error', () => {
    // given
    component.interactiveImageView = true;
    component.conversionFailed = true;
    component.file = {} as FilePreview;
    component.previewType = PreviewType.IMAGE;
    filePreviewTypeService.isInternalFile.and.returnValue(true);

    // when
    const result = component.useFullResImageOverlay();

    // then
    expect(result).toBeFalsy();
  });

  it('should show spinner instead of animation', () => {
    // given
    component.loading = true;
    component.usePreviewGeneratingAnimation = false;

    // when
    const spinner = component.showSpinner();
    const animation = component.showPreviewAnimation();

    // then
    expect(spinner).toBe(true);
    expect(animation).toBe(false);
  });

  it('should show animation instead of spinner', () => {
    // given
    component.loading = true;
    component.usePreviewGeneratingAnimation = true;

    // when
    const spinner = component.showSpinner();
    const animation = component.showPreviewAnimation();

    // then
    expect(spinner).toBe(false);
    expect(animation).toBe(true);
  });
});
