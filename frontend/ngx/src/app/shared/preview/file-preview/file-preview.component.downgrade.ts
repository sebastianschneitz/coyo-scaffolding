import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {FilePreviewComponent} from './file-preview.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoFilePreview', downgradeComponent({
    component: FilePreviewComponent,
    propagateDigest: false
  }));
