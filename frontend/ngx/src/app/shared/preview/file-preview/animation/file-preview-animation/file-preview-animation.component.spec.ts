import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AnimationItem} from 'lottie-web/build/player/lottie_light';
import {FilePreviewAnimationComponent, LOTTIE} from './file-preview-animation.component';

describe('FilePreviewGeneratingAnimationComponent', () => {
  let component: FilePreviewAnimationComponent;
  let fixture: ComponentFixture<FilePreviewAnimationComponent>;
  let lottie: jasmine.SpyObj<{ loadAnimation(): AnimationItem; }>;
  let animation: jasmine.SpyObj<AnimationItem>;
  let animationCompletedCallback: Function;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilePreviewAnimationComponent],
      providers: [{
        provide: LOTTIE,
        useValue: jasmine.createSpyObj('Lottie', ['loadAnimation'])
      }]
    }).compileComponents();

    lottie = TestBed.get(LOTTIE);
  }));

  beforeEach(() => {
    animation = jasmine.createSpyObj('animation', ['addEventListener', 'goToAndPlay', 'setDirection', 'play', 'stop']);
    lottie.loadAnimation.and.returnValue(animation);
    animation.addEventListener.and.callFake((name: string, cb: Function) => animationCompletedCallback = cb);

    fixture = TestBed.createComponent(FilePreviewAnimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should play the animation reversed', () => {
    // when
    animationCompletedCallback();

    // then
    expect(animation.setDirection).toHaveBeenCalledWith(-1);
    expect(animation.play).toHaveBeenCalled();
  });

  it('should play the animation forward again after reversed', () => {
    // given
    component['reverse'] = true;

    // when
    animationCompletedCallback();

    // then
    expect(animation.setDirection).toHaveBeenCalledWith(1);
    expect(animation.play).toHaveBeenCalled();
  });

  it('should stop the animation on destroy', () => {
    // when
    fixture.destroy();

    // then
    expect(animation.stop).toHaveBeenCalled();
  });
});
