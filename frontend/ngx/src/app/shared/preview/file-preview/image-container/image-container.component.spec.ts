import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ImageLoaderService} from '@core/image/image-loader.service';
import {FileService} from '@domain/file/file/file.service';
import {FilePreview} from '@domain/preview/file-preview';
import {FilePreviewStatus} from '@shared/preview/file-preview/file-preview-status';
import {ImageContainerComponent} from '@shared/preview/file-preview/image-container/image-container.component';
import {PreviewStatusService} from '@shared/preview/file-preview/preview-status/preview-status.service';
import {of, throwError} from 'rxjs';
import {Vec2} from './vec2';
import SpyObj = jasmine.SpyObj;

describe('ImageContainer', () => {
  let component: ImageContainerComponent;
  let fixture: ComponentFixture<ImageContainerComponent>;
  let fileService: SpyObj<FileService>;
  let previewStatusService: SpyObj<PreviewStatusService>;
  let imageLoaderService: SpyObj<ImageLoaderService>;
  const mockImage = {width: 50, height: 30};
  const mockFilePreview = {
    isProcessing: false,
    previewAvailable: true,
    conversionError: false,
    loading: false
  } as FilePreviewStatus;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ImageContainerComponent],
      providers: [
        {
          provide: ImageLoaderService,
          useValue: jasmine.createSpyObj('imageLoaderService', ['loadImage'])
        },
        {
          provide: FileService,
          useValue: jasmine.createSpyObj('fileService', ['getImagePreviewUrl'])
        },
        {
          provide: PreviewStatusService,
          useValue: jasmine.createSpyObj('previewStatusService', ['getPreviewStatus$'])
        }
      ]
    })
      .compileComponents();
    fileService = TestBed.get(FileService);
    fileService.getImagePreviewUrl.and.returnValue(of('preview-url'));
    previewStatusService = TestBed.get(PreviewStatusService);
    previewStatusService.getPreviewStatus$.and.returnValue(of(mockFilePreview));
    imageLoaderService = TestBed.get(ImageLoaderService);
    imageLoaderService.loadImage.and.callFake(() => of(mockImage));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageContainerComponent);
    component = fixture.componentInstance;
    component.file = {id: '123'} as FilePreview;
    component.previewUrl = 'url';
    component.groupId = 'groupId';
    spyOnProperty(component, 'canvasSize', 'get').and.returnValue(new Vec2(100, 80));
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should correctly initialize with an image', () => {
    fixture.detectChanges();
    expect(component.imageSize.x).toEqual(mockImage.width);
    expect(component.imageSize.y).toEqual(mockImage.height);
    expect(component.location).toEqual(new Vec2(0, 0));
  });

  it('should correctly convert screen coordinates into image coordinates', () => {
    fixture.detectChanges();
    component.zoom = 1;
    expect(component.screen2ImageLocation(new Vec2(26, 71))).toEqual(new Vec2(1, 46));

    component.zoom = 0.5;
    expect(component.screen2ImageLocation(new Vec2(67, 33))).toEqual(new Vec2(59, 1));

    component.zoom = 1;
    component.location = new Vec2(4, 6);
    expect(component.screen2ImageLocation(new Vec2(26, 71))).toEqual(new Vec2(-3, 40));

    component.zoom = 2;
    expect(component.screen2ImageLocation(new Vec2(67, 33))).toEqual(new Vec2(29.5, 5.5));
  });

  it('should correctly convert image coordinates into screen coordinates', () => {
    fixture.detectChanges();
    component.zoom = 1;
    expect(component.image2ScreenLocation(new Vec2(1, 46))).toEqual(new Vec2(26, 71));

    component.zoom = 0.5;
    expect(component.image2ScreenLocation(new Vec2(59, 1))).toEqual(new Vec2(67, 33));

    component.zoom = 1;
    component.location = new Vec2(4, 6);
    expect(component.image2ScreenLocation(new Vec2(-3, 40))).toEqual(new Vec2(26, 71));

    component.zoom = 2;
    expect(component.image2ScreenLocation(new Vec2(29.5, 5.5))).toEqual(new Vec2(67, 33));
  });

  it('should correctly set the velocity', () => {
    fixture.detectChanges();
    component.location = new Vec2(10, 5);
    component.setLocation(new Vec2(30, 0));
    expect(component.velocity).toEqual(new Vec2(20, -5));
  });

  it('should subscribe to preview status on image loading error', () => {
    // given
    imageLoaderService.loadImage.and.returnValues(throwError(404), of(mockFilePreview));

    // when
    fixture.detectChanges();

    // then
    expect(previewStatusService.getPreviewStatus$).toHaveBeenCalledWith(component.file, component.previewUrl, component.groupId);
  });

  it('should try to load image again when preview is available', () => {
    // given
    imageLoaderService.loadImage.and.returnValues(throwError(404), of(mockFilePreview));

    // when
    fixture.detectChanges();

    // then
    expect(imageLoaderService.loadImage).toHaveBeenCalledTimes(2);
  });
});
