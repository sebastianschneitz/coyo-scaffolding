import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {FileIntegrationLinkComponent} from '@shared/preview/file-integration-link/file-integration-link.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoFileIntegrationLink', downgradeComponent({
    component: FileIntegrationLinkComponent
  }));
