import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {GSUITE} from '@domain/attachment/storage';
import {ExternalFileDetails} from '@shared/files/external-file-handler/external-file-details';
import {ExternalFileHandlerService} from '@shared/files/external-file-handler/external-file-handler.service';
import {File} from '@shared/files/external-file-handler/file';
import {of} from 'rxjs';
import {FileIntegrationLinkComponent} from './file-integration-link.component';

describe('FileIntegrationLinkComponent', () => {
  let component: FileIntegrationLinkComponent;
  let fixture: ComponentFixture<FileIntegrationLinkComponent>;
  let fileService: jasmine.SpyObj<ExternalFileHandlerService>;
  let googleApiService: jasmine.SpyObj<GoogleApiService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileIntegrationLinkComponent],
      providers: [{
        provide: ExternalFileHandlerService,
        useValue: jasmine.createSpyObj('fileService', ['getExternalFileDetails'])
      }, {
        provide: GoogleApiService,
        useValue: jasmine.createSpyObj('googleApiService', ['isGoogleApiActive'])
      }]
    }).overrideTemplate(FileIntegrationLinkComponent, '<div></div>')
      .compileComponents();

    fileService = TestBed.get(ExternalFileHandlerService);
    googleApiService = TestBed.get(GoogleApiService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileIntegrationLinkComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    // given
    fileService.getExternalFileDetails.and.returnValue(Promise.resolve({}));
    googleApiService.isGoogleApiActive.and.returnValue(of(false));

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init and get external file details', () => {
    // given
    const file: File = {
      fileId: 'file-id',
      storage: GSUITE
    } as File;
    const externalFileDetails = {
      externalUrl: 'external-url',
      canEdit: true
    } as ExternalFileDetails;
    googleApiService.isGoogleApiActive.and.returnValue(of(true));
    fileService.getExternalFileDetails.and.returnValue(Promise.resolve(externalFileDetails));

    // when
    component.file = file;
    fixture.detectChanges();

    // then
    expect(fileService.getExternalFileDetails).toHaveBeenCalledWith(file);
    component.isGoogleActive$.subscribe(active => {
      expect(active).toBeTruthy();
    });
    component.externalFileDetails$.subscribe(actualFileDetails => {
      expect(actualFileDetails.externalUrl).toEqual(externalFileDetails.externalUrl);
      expect(actualFileDetails.canEdit).toEqual(externalFileDetails.canEdit);
    });
  });

  it('should wrap external file details error response', () => {
    // given
    googleApiService.isGoogleApiActive.and.returnValue(of(true));
    fileService.getExternalFileDetails.and.returnValue(Promise.reject('e. g. 404 response'));

    // when
    fixture.detectChanges();

    // then
    component.isGoogleActive$.subscribe(active => {
      expect(active).toBeTruthy();
    });
    component.externalFileDetails$.subscribe(actualFileDetails => {
      expect(actualFileDetails).toEqual({} as ExternalFileDetails);
    });
  });
});
