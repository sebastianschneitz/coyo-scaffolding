import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {GSUITE, OFFICE365} from '@domain/attachment/storage';
import {CapabilitiesService} from '@domain/capability/capabilities/capabilities.service';
import {WINDOW} from '@root/injection-tokens';
import {Ng1fileDetailsModalService} from '@root/typings';
import {ExternalFileHandlerService} from '@shared/files/external-file-handler/external-file-handler.service';
import {NG1_FILE_DETAILS_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {Attachment} from 'app/core/domain/attachment/attachment';
import {FilePreview} from 'app/core/domain/preview/file-preview';
import {of} from 'rxjs';
import {FilePreviewListComponent} from './file-preview-list.component';

describe('PreviewListComponent', () => {
  let component: FilePreviewListComponent;
  let fixture: ComponentFixture<FilePreviewListComponent>;
  let capabilitiesServiceMock: jasmine.SpyObj<CapabilitiesService>;
  let externalFileHandlerServiceMock: jasmine.SpyObj<ExternalFileHandlerService>;
  let fileDetailsModalService: jasmine.SpyObj<Ng1fileDetailsModalService>;
  let windowServiceMock: jasmine.SpyObj<Window>;

  const attachments = [{
    id: '1',
    name: 'nameA',
    contentType: 'image/png',
    length: 8,
    created: new Date(),
    modified: new Date(),
    storage: 'LOCAL_FILE_LIBRARY',
    downloadUrl: 'downloadURL',
    fileId: '1',
    modelId: '1',
    deleted: false
  } as Attachment,
  {
    id: '2',
    name: 'nameB',
    contentType: 'image/jpeg',
    length: 88,
    created: new Date(),
    modified: new Date(),
    storage: 'G_SUITE',
    downloadUrl: 'downloadURL',
    fileId: '2',
    modelId: '2',
    deleted: false
  } as Attachment,
  {
    id: '3',
    name: 'nameC',
    contentType: 'application/pdf',
    length: 0,
    created: new Date(),
    modified: new Date(),
    storage: 'LOCAL_BLOB_STORAGE',
    downloadUrl: 'downloadURL',
    fileId: '3',
    modelId: '3',
    deleted: true
  } as Attachment];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ FilePreviewListComponent ],
      providers: [
        {provide: NG1_FILE_DETAILS_MODAL_SERVICE, useValue: jasmine.createSpyObj('fileDetailsModalService', ['open'])},
        {provide: CapabilitiesService, useValue: jasmine.createSpyObj('capabilitiesService', ['imgAvailable'])},
        {
          provide: ExternalFileHandlerService,
          useValue: jasmine.createSpyObj('externalFileHandlerService', ['isExternalFile', 'getExternalFileDetails'])
        },
        {provide: WINDOW, useValue: jasmine.createSpyObj('windowService', ['open'])}
      ]
    }).overrideTemplate(FilePreviewListComponent, '<div></div>')
    .compileComponents();

    fileDetailsModalService = TestBed.get(NG1_FILE_DETAILS_MODAL_SERVICE);
    capabilitiesServiceMock = TestBed.get(CapabilitiesService);
    capabilitiesServiceMock.imgAvailable.and.returnValue(of(true));
    externalFileHandlerServiceMock = TestBed.get(ExternalFileHandlerService);
    externalFileHandlerServiceMock.isExternalFile.and.callFake((attachment: Attachment) => attachment.storage === GSUITE || attachment.storage === OFFICE365);
    windowServiceMock = TestBed.get(WINDOW);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilePreviewListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.attachments = [];
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given
    component.groupId = 'this33';
    component.previewUrl = 'thisPreview';
    component.attachments = attachments;
    // when
    component.ngOnInit();
    // then
    expect(component.size).toBeDefined();
    expect(component.filePreviews$).toBeDefined();
    expect(component.previewFiles$).toBeDefined();
    expect(component.nonPreviewFiles$).toBeDefined();

    component.filePreviews$.subscribe(previews => {
      expect(previews.length).toBe(3);
      expect(previews[0].previewAvailable).toBe(true);
      expect(previews[1].previewAvailable).toBe(false);
      expect(previews[2].previewAvailable).toBe(true);
      expect(previews[0].showSize).toBe(true);
      expect(previews[1].showSize).toBe(false);
      expect(previews[2].showSize).toBe(true);
      expect(previews[0].deleted).toBe(false);
      expect(previews[1].deleted).toBe(false);
      expect(previews[2].deleted).toBe(true);
      expect(previews[0].id).toEqual(component.attachments[0].fileId);
      expect(previews[0].attachment).toBe(false);
      expect(previews[0].groupId).toEqual(component.attachments[0].modelId);
      expect(previews[0].senderId).toEqual(component.attachments[0].modelId);
      expect(previews[1].attachment).toBe(true);
      expect(previews[1].groupId).toEqual(component.groupId);
      expect(previews[1].previewUrl).toEqual(component.previewUrl);
      expect(previews[2].attachment).toBe(true);
      expect(previews[2].groupId).toEqual(component.groupId);
      expect(previews[2].previewUrl).toEqual(component.previewUrl);
    });

    component.previewFiles$.subscribe(withPreviews => {
      expect(withPreviews.length).toBe(2);
    });

    component.nonPreviewFiles$.subscribe(noPreviews => {
      expect(noPreviews.length).toBe(1);
    });
  });

  it('should show details', () => {
    // given
    const event = jasmine.createSpyObj('event', ['preventDefault']);
    const attachment = {fileId: '0', modelId: '0', storage: 'LOCAL_FILE_LIBRARY', name: 'helloWorld'} as Attachment;
    component.attachments = [attachment];
    const expectedFiles: FilePreview[] = [{
      ...attachment,
      id: attachment.fileId,
      attachment: false,
      groupId: attachment.modelId,
      senderId: attachment.modelId} as FilePreview];
    const expectedIndex = 0;
    // when
    component.showDetails(attachment, event);
    // then
    expect(event.preventDefault).toHaveBeenCalled();
    expect(fileDetailsModalService.open).toHaveBeenCalled();
    expect(fileDetailsModalService.open).toHaveBeenCalledWith(expectedFiles, expectedIndex, true);
  });

  it('should show details for non file library attachments', () => {
    // given
    const event = jasmine.createSpyObj('event', ['preventDefault']);
    const attachment = {id: '0', modelId: '0', name: 'helloWorld'} as Attachment;
    component.attachments = [attachment];
    component.groupId = '0';
    const expectedFiles: FilePreview[] = [{
      ...attachment,
      attachment: true,
      groupId: component.groupId,
      previewUrl: component.previewUrl,
      author: component.author
    } as FilePreview];
    const expectedIndex = 0;
    // when
    component.showDetails(attachment, event);
    // then
    expect(event.preventDefault).toHaveBeenCalled();
    expect(fileDetailsModalService.open).toHaveBeenCalled();
    expect(fileDetailsModalService.open).toHaveBeenCalledWith(expectedFiles, expectedIndex, true);
  });

  it('should onCannotProcess', () => {
    // given
    component.groupId = 'this33';
    component.previewUrl = 'thisPreview';
    component.attachments = attachments;
    // when
    component.onCannotProcess(attachments[0].fileId);
    // then
    expect(component.previewFiles$).toBeDefined();
    expect(component.nonPreviewFiles$).toBeDefined();
    component.previewFiles$.subscribe(withPreviews => {
      expect(withPreviews.length).toBe(1);
    });
    component.nonPreviewFiles$.subscribe(nonPreviews => {
      expect(nonPreviews.length).toBe(2);
    });
  });

  it('should sort attachments', () => {
    // given
    const oldDate = new Date();
    oldDate.setMonth(oldDate.getMonth() - 2);
    const unsortedAttachments = [
      {
        id: '1',
        name: 'NAMEC',
        contentType: 'image/png',
        created: new Date(),
        storage: 'LOCAL_FILE_LIBRARY'
      } as Attachment,
      {
        id: '2',
        name: 'nameA',
        contentType: 'image/jpeg',
        created: new Date(),
        storage: 'LOCAL_FILE_LIBRARY'
      } as Attachment,
      {
        id: '3',
        name: 'nameB',
        contentType: 'application/pdf',
        created: new Date(),
        storage: 'LOCAL_FILE_LIBRARY'
      } as Attachment,
      {
        id: '4',
        name: 'nameB',
        contentType: 'application/pdf',
        created: oldDate,
        storage: 'LOCAL_FILE_LIBRARY'
      } as Attachment,
      {
        id: '5',
        name: 'b.sh',
        contentType: 'application/x-sh',
        created: new Date(),
        storage: 'G_SUITE',
      } as Attachment,
      {
        id: '6',
        name: 'a.sh',
        contentType: 'application/x-sh',
        created: new Date(),
        storage: 'G_SUITE',
      } as Attachment
    ];

    component.groupId = 'group-id';
    component.previewUrl = 'preview-url';
    component.attachments = unsortedAttachments;

    // when
    fixture.detectChanges();

    // then
    expect(component.previewFiles$).toBeDefined();
    expect(component.nonPreviewFiles$).toBeDefined();

    component.previewFiles$.subscribe(previews => {
      expect(previews[0].name).toEqual('nameA');
      expect(previews[1].name).toEqual('nameB');
      expect(previews[2].name).toEqual('nameB');
      expect(previews[3].name).toEqual('NAMEC');
      expect(previews[1].created.valueOf()).toBeLessThan(previews[2].created.valueOf());
    });

    component.nonPreviewFiles$.subscribe(previews => {
      expect(previews[0].name).toEqual('a.sh');
      expect(previews[1].name).toEqual('b.sh');
    });
  });

  it('should open SharePoint files in a new tab', fakeAsync(() => {
    // given
    const event = jasmine.createSpyObj('event', ['preventDefault']);
    const url = 'URL';
    const attachment = {storage: OFFICE365} as Attachment;
    externalFileHandlerServiceMock.getExternalFileDetails.and.returnValue(of({externalUrl: url}).toPromise());

    // when
    component.showDetails(attachment, event);
    tick();

    // then
    expect(externalFileHandlerServiceMock.getExternalFileDetails).toHaveBeenCalledWith(attachment);
    expect(windowServiceMock.open).toHaveBeenCalledWith(url, '_blank');
  }));
});
