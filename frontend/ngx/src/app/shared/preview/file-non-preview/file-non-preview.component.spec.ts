import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileNonPreviewComponent } from './file-non-preview.component';

describe('FileNonPreviewComponent', () => {
  let component: FileNonPreviewComponent;
  let fixture: ComponentFixture<FileNonPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileNonPreviewComponent ]
    }).overrideTemplate(FileNonPreviewComponent, '<div></div>')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileNonPreviewComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
