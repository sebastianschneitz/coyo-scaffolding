/**
 * ng-select scroll position event.
 */
export interface ScrollPositionEvent {
  start: number;
  end: number;
}
