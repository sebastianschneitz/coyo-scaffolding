import {HttpParams} from '@angular/common/http';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {Options} from '@root/typings';
import {SelectUiComponent} from '@shared/select-ui/select-ui.component';
import {FindOptions, SenderType} from '@shared/sender-ui/select-sender/select-sender-options';

import {of} from 'rxjs';
import {SelectSenderComponent} from './select-sender.component';

class TestCallSettings {
  term: string = 'test-term';
  pageable: Pageable = new Pageable(0, 10);

  constructor(
    public component: SelectSenderComponent,
    public senderService: SenderService,
    public findOptions: FindOptions,
    public senderTypes: SenderType[]) {
  }
}

const execCallVariant = (testCallSettings: TestCallSettings) => {
  let result: Page<Sender> = null;
  testCallSettings.component.options.findOptions = testCallSettings.findOptions;
  testCallSettings.component.options.senderTypes = testCallSettings.senderTypes;
  testCallSettings.component.settings.searchFn(testCallSettings.pageable, testCallSettings.term).subscribe(res => {
    result = res;
  });
  return result;
};

const testCallVariant = (testCallSettings: TestCallSettings) => {
  const options = {
    path: {
      [FindOptions.ALL]: '/search',
      [FindOptions.MANAGED_SENDERS_ONLY]: '/search/managed',
      [FindOptions.SHARE_TARGETS_ONLY]: '/search/sharing-recipients'
    }[testCallSettings.findOptions],
    params: new HttpParams({
      fromObject: {
        term: testCallSettings.term,
        filters: 'type=' + testCallSettings.senderTypes.join('&type=')
      }
    })
  };
  // then
  expect(testCallSettings.senderService.getPage).toHaveBeenCalledWith(testCallSettings.pageable, options);
  if (!!testCallSettings.component.options.showSender) {
    expect(testCallSettings.component.options.showSender).toHaveBeenCalled();
  }
};

describe('SelectSenderComponent', () => {
  let component: SelectSenderComponent;
  let fixture: ComponentFixture<SelectSenderComponent>;
  let senderService: jasmine.SpyObj<SenderService>;
  let urlService: jasmine.SpyObj<UrlService>;
  const showSender: jasmine.Spy = jasmine.createSpy('showSender');
  const select: jasmine.SpyObj<SelectUiComponent<any>> = jasmine.createSpyObj('SelectUiComponent',
    ['registerOnChange', 'registerOnTouched', 'setDisabledState', 'writeValue']);
  const setting_placeholder = 'test-placeholder';
  beforeEach(
    async(() => {
    TestBed
      .configureTestingModule({
        declarations: [SelectSenderComponent],
        providers: [{
          provide: SenderService,
          useValue: jasmine.createSpyObj('SenderService', ['getPage'])
        },
          {
            provide: UrlService,
            useValue: jasmine.createSpyObj('UrlService', ['toUrlParamString'])
          }]
      })
      .overrideTemplate(SelectSenderComponent, '<div></div>')
      .compileComponents()
      .then(() => {
        senderService = TestBed.get(SenderService);
        urlService = TestBed.get(UrlService);
        fixture = TestBed.createComponent(SelectSenderComponent);
        senderService.getPage.and.callFake((pageable: Pageable, options?: Options) => of({content: ['a', 'b', 'c']} as any as Page<Sender>));
        urlService.toUrlParamString.and.callFake((key: string, values: any[]) => `${key}=${values.join('&' + key + '=')}`);
        showSender.and.returnValue(true);
        component = fixture.componentInstance;
        component.options = {
          findOptions: FindOptions.ALL,
          senderTypes: ['user'],
          showSender: showSender
        };
        component.placeholder = setting_placeholder;
      });
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should pass through ControlValueAccessor functions to the child component', fakeAsync(() => {
    // given
    // when
    fixture.detectChanges();
    component.select = select;
    component.registerOnChange('a');
    component.registerOnTouched('b');
    component.writeValue('c');
    component.setDisabledState(true);
    tick();
    // then
    expect(select.registerOnChange).toHaveBeenCalledWith('a');
    expect(select.registerOnTouched).toHaveBeenCalledWith('b');
    expect(select.writeValue).toHaveBeenCalledWith('c');
    expect(select.setDisabledState).toHaveBeenCalledWith(true);
  }));

  it('should correctly init settings', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component.settings).toEqual({
      closeOnSelect: component.closeOnSelect,
      multiselect: component.multiselect,
      searchFn: component.settings.searchFn,
      compareFn: component.settings.compareFn,
      clearable: component.clearable,
      placeholder: component.placeholder,
      debounceTime: component.typeaheadDebounceTime,
      pageSize: component.scrollPageSize,
      scrollOffsetTrigger: component.scrollPageTriggerOffset
    });
  });

  it('should correctly compare apps', () => {
    // given
    const sender1: Sender = {id: 'sender1'} as any as Sender;
    const sender2: Sender = {id: 'sender1'} as any as Sender;
    const sender3: Sender = {id: 'sender3'} as any as Sender;
    // when
    fixture.detectChanges();

    // then

    expect(component.settings.compareFn(sender1, sender1)).toBeTruthy();
    expect(component.settings.compareFn(sender1, sender2)).toBeTruthy();
    expect(component.settings.compareFn(sender1, sender3)).toBeFalsy();
  });

  it('should use the sender service to find senders', () => {
    // given
    const tests = [
      new TestCallSettings(component, senderService, FindOptions.ALL, ['user']),
      new TestCallSettings(component, senderService, FindOptions.ALL, ['page']),
      new TestCallSettings(component, senderService, FindOptions.ALL, ['workspace']),
      new TestCallSettings(component, senderService, FindOptions.ALL, ['event']),
      new TestCallSettings(component, senderService, FindOptions.ALL, ['user', 'page', 'workspace', 'event']),
      new TestCallSettings(component, senderService, FindOptions.MANAGED_SENDERS_ONLY, ['user', 'page', 'workspace', 'event']),
      new TestCallSettings(component, senderService, FindOptions.SHARE_TARGETS_ONLY, ['user', 'page', 'workspace', 'event']),
    ];
    // when
    fixture.detectChanges();
    const results = tests.map(execCallVariant);

    // then
    results.forEach(result => expect(result).toBeTruthy());
    tests.forEach(testCallVariant);
  });

  it('should correctly update settings', () => {
    // given
    component.typeaheadDebounceTime = 1;

    // when
    fixture.detectChanges();

    // then
    expect(component.settings).toEqual({
      closeOnSelect: component.closeOnSelect,
      multiselect: component.multiselect,
      searchFn: component.settings.searchFn,
      compareFn: component.settings.compareFn,
      clearable: component.clearable,
      placeholder: component.placeholder,
      debounceTime: component.typeaheadDebounceTime,
      pageSize: component.scrollPageSize,
      scrollOffsetTrigger: component.scrollPageTriggerOffset
    });
  });
});
