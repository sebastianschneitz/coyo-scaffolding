import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {TargetService} from '@domain/sender/target/target.service';
import {Sender} from 'app/core/domain/sender/sender';
import {of} from 'rxjs';
import {SenderAvatarComponent} from './sender-avatar.component';

describe('SenderAvatarComponent', () => {
  let component: SenderAvatarComponent;
  let fixture: ComponentFixture<SenderAvatarComponent>;
  let targetServiceMock: jasmine.SpyObj<TargetService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SenderAvatarComponent],
      providers: [{
        provide: TargetService, useValue: jasmine.createSpyObj('targetService', ['getLinkForCurrentUser'])
      }]
    }).overrideTemplate(SenderAvatarComponent, '<div></div>')
      .compileComponents();

    targetServiceMock = TestBed.get(TargetService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderAvatarComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should link to target sender', fakeAsync(() => {
    // given
    component.sender = {
      target: {}
    } as Sender;

    targetServiceMock.getLinkForCurrentUser.and.returnValue(of('test-link'));

    // when
    component.ngOnInit();

    // then
    component.$senderLink.subscribe(value => {
      expect(value).toBe('test-link');
    });
    tick();
  }));

  it('should not link to target sender if user has no permission', fakeAsync(() => {
    // given
    component.sender = {
      target: {}
    } as Sender;

    targetServiceMock.getLinkForCurrentUser.and.returnValue(of(undefined));

    // when
    component.ngOnInit();

    // then
    component.$senderLink.subscribe(value => {
      expect(value).toBeUndefined();
    });
    tick();
  }));

  it('should not link to target sender if no link is set', fakeAsync(() => {
    // given
    component.noLink = true;

    // when
    component.ngOnInit();

    // then
    expect(component.$senderLink.subscribe().closed).toBeTruthy();
    expect(targetServiceMock.getLinkForCurrentUser).not.toHaveBeenCalled();
  }));
});
