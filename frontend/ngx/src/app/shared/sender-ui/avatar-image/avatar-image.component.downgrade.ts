import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {AvatarImageComponent} from './avatar-image.component';

getAngularJSGlobal()
  .module('commons.sender')
  .directive('coyoAvatarImage', downgradeComponent({
    component: AvatarImageComponent,
    propagateDigest: false
  }));
