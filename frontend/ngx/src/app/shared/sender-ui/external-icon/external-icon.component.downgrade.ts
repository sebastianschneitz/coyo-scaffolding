import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {ExternalIconComponent} from './external-icon.component';

getAngularJSGlobal()
  .module('commons.sender')
  .directive('coyoExternalIcon', downgradeComponent({
    component: ExternalIconComponent,
    propagateDigest: false
  }));
