import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {UserChooserComponent} from '@shared/sender-ui/user-chooser/user-chooser.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoUserChooserView', downgradeComponent({
    component: UserChooserComponent,
    propagateDigest: false
  }));
