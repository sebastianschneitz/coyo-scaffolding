import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Guest} from '@domain/guest/Guest';
import {GuestSelection} from '@domain/guest/GuestSelection';
import {UserChooserService} from '@domain/guest/user-chooser.service';
import {UserChooserSelectionConfig} from '@domain/guest/UserChooserSelectionConfig';
import {Pageable} from '@domain/pagination/pageable';
import {BehaviorSubject, Observable, Subject, Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

export class UserChooserDataSource extends DataSource<GuestSelection | null> {

  private static readonly PAGE_SIZE: number = 20;
  private static readonly DEBOUNCE_TIME: number = 250;

  /**
   * Indicates whether the data source has content or not.
   */
  isEmpty$: Subject<boolean> = new Subject<boolean>();

  private dataStream: BehaviorSubject<(GuestSelection | null)[]> = new BehaviorSubject<(GuestSelection | null)[]>([]);
  private fetchedPages: Set<number> = new Set<number>();
  private subscription: Subscription;
  private selectedGuestsSubscription: Subscription;

  constructor(private userChooserService: UserChooserService,
              private searchTerm: string,
              private selectionTypes: UserChooserSelectionConfig,
              private selectedGuests$: BehaviorSubject<GuestSelection[]>) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<(GuestSelection | null)[]> {
    this.loadPage(0);
    this.selectedGuestsSubscription = this.selectedGuests$.subscribe(guests => this.updateDataStream(guests));
    this.subscription = collectionViewer.viewChange
      .pipe(debounceTime(UserChooserDataSource.DEBOUNCE_TIME))
      .subscribe(range => {
        const pageStart = Math.floor(range.start / UserChooserDataSource.PAGE_SIZE);
        const pageEnd = Math.floor(range.end / UserChooserDataSource.PAGE_SIZE);
        for (let i = pageStart; i <= pageEnd; ++i) {
          if (!this.fetchedPages.has(i)) {
            this.loadPage(i);
          }
        }
      });
    return this.dataStream;
  }

  disconnect(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.selectedGuestsSubscription) {
      this.selectedGuestsSubscription.unsubscribe();
    }
  }

  private loadPage(pageNumber: number): void {
    this.fetchedPages.add(pageNumber);
    const pageable = new Pageable(pageNumber, UserChooserDataSource.PAGE_SIZE);
    this.userChooserService.searchGuests(this.searchTerm, this.selectionTypes, pageable)
      .subscribe(page => {
        const value = this.dataStream.getValue();
        const result = value.length ? value : Array.from({length: page.totalElements}).map(() => null);
        const updatedPageContent = this.setSelectedState(page.content, this.selectedGuests$.getValue());
        result.splice(pageNumber * UserChooserDataSource.PAGE_SIZE, page.numberOfElements, ...updatedPageContent);
        this.isEmpty$.next(page.empty);
        this.dataStream.next(result);
      }, error => {
        this.fetchedPages.delete(pageNumber);
      });
  }

  private setSelectedState(content: Guest[], selectedGuests: GuestSelection[]): GuestSelection[] {
    content.forEach(guest => {
      if (guest) {
        const index = selectedGuests.findIndex(selectedGuest => selectedGuest.id === guest.id);
        guest['selected'] = index > -1;
      }
    });
    return content as GuestSelection[];
  }

  private updateDataStream(guests: GuestSelection[]): void {
    this.dataStream.next(this.setSelectedState(this.dataStream.getValue(), guests));
  }
}
