export class UserChooserSelection {

  static readonly MEMBER_TYPE: string = 'user';
  static readonly MEMBER_GROUP_TYPE: string = 'group';

  memberIds?: string[];
  memberGroupIds?: string[];

  constructor(memberIds: string[] = [], memberGroupIds: string[] = []) {
    this.memberIds = memberIds;
    this.memberGroupIds = memberGroupIds;
  }
}
