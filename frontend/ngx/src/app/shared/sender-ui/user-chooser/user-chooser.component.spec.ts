import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormControl} from '@angular/forms';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {GuestSelection} from '@domain/guest/GuestSelection';
import {UserChooserService} from '@domain/guest/user-chooser.service';
import {TranslateService} from '@ngx-translate/core';
import {UserChooserSelection} from '@shared/sender-ui/user-chooser/UserChooserSelection';
import {Observable, Subject, Subscription} from 'rxjs';
import {UserChooserComponent} from './user-chooser.component';

describe('UserChooserComponent', () => {
  let component: UserChooserComponent;
  let fixture: ComponentFixture<UserChooserComponent>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;
  let userChooserService: jasmine.SpyObj<UserChooserService>;
  let translateService: jasmine.SpyObj<TranslateService>;
  let queryChange: Subject<string>;
  const subscription: jasmine.SpyObj<Subscription> = jasmine.createSpyObj('subscription', ['unsubscribe']);
  const observable: jasmine.SpyObj<Observable<any>> = jasmine.createSpyObj('observable', ['subscribe', 'pipe']);

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [UserChooserComponent],
        providers: [{
          provide: WindowSizeService, useValue: jasmine.createSpyObj('windowSizeService', ['observeScreenChange'])
        }, {
          provide: UserChooserService, useValue: jasmine.createSpyObj('userChooserService', ['searchGuests'])
        }, {
          provide: TranslateService, useValue: jasmine.createSpyObj('translateService', ['instant'])
        }]
      })
      .overrideTemplate(UserChooserComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserChooserComponent);
    windowSizeService = TestBed.get(WindowSizeService);
    windowSizeService.observeScreenChange.and.callFake(() => observable);
    userChooserService = TestBed.get(UserChooserService);
    userChooserService.searchGuests.and.returnValue(observable);
    translateService = TestBed.get(TranslateService);
    translateService.instant.and.callFake((key: string) => {
        if (key === 'USER.CHOOSER.PLACEHOLDER.SEARCH') { return 'Search for '; }
        if (key === 'USER.CHOOSER.PLACEHOLDER.GROUPS') { return 'groups'; }
        if (key === 'USER.CHOOSER.PLACEHOLDER.PAGES') { return 'pages'; }
        if (key === 'USER.CHOOSER.PLACEHOLDER.WORKSPACES') { return 'workspaces'; }
        if (key === 'USER.CHOOSER.PLACEHOLDER.USERS') { return 'users'; }
        if (key === 'USER.CHOOSER.PLACEHOLDER.AND') { return ' and '; }
    });
    observable.subscribe.and.returnValue(subscription);
    observable.pipe.and.callFake(() => observable);
    component = fixture.componentInstance;
    queryChange = new Subject<string>();
    component.searchInput = {valueChanges: queryChange} as unknown as FormControl;
    queryChange.next('');
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should initialize with guests', done => {
    // given
    const expected = [{
      id: 'id-1',
      selected: true
    }] as GuestSelection[];
    component.initialSelection = expected;

    // when
    fixture.detectChanges();

    // then
    component.selectedGuestsList$.subscribe(guestList => {
      expect(guestList).toEqual(expected);
      done();
    });
  });

  it('should increase steps', () => {
    // given
    component.step = 0;
    fixture.detectChanges();

    // when
    component.nextStep();

    // then
    expect(component.step).toEqual(1);
  });

  it('should decrease steps', () => {
    // given
    component.step = 1;
    fixture.detectChanges();

    // when
    component.previousStep();

    // then
    expect(component.step).toEqual(0);
  });

  it('should emit on submit', () => {
    // given
    spyOn(component.result, 'emit');
    fixture.detectChanges();

    // when
    component.submit();

    // then
    expect(component.result.emit).toHaveBeenCalled();
  });

  it('should initialize submit button on desktop', () => {
    // given
    component.step = 1;
    component.isMobile = false;
    fixture.detectChanges();

    // when
    const result = component.isNextButtonSubmitButton();

    // then
    expect(result).toEqual(true);
  });

  it('should initialize submit button on mobile', () => {
    // given
    component.step = 2;
    component.isMobile = true;
    fixture.detectChanges();

    // when
    const result = component.isNextButtonSubmitButton();

    // then
    expect(result).toEqual(true);
  });

  it('should set submit button on mobile', () => {
    // given
    component.step = 1;
    component.isMobile = true;
    fixture.detectChanges();
    expect(component.isNextButtonSubmitButton()).toEqual(false);

    // when
    component.nextStep();
    const result = component.isNextButtonSubmitButton();

    // then
    expect(result).toEqual(true);
  });

  it('should call submit with correct result', () => {
    // given
    fixture.detectChanges();
    const guestList = [{
      id: 'user-id',
      selected: true,
      typeName: 'user'
    }, {
      id: 'group-id',
      selected: true,
      typeName: 'group'
    }] as GuestSelection[];
    const expected = new UserChooserSelection(['user-id'], ['group-id']);
    component.isMobile = false;
    component.updateInviteList(guestList[0]);
    component.updateInviteList(guestList[1]);
    spyOn(component.result, 'emit');
    fixture.detectChanges();

    // when
    component.submit();

    // then
    expect(component.result.emit).toHaveBeenCalledWith(expected);
  });

  it('should search with searchTerm', () => {
    // given
    fixture.detectChanges();
    spyOn(component.userChooserDataSource$, 'next');

    // when
    component.search('search-term');

    // then
    expect(component.userChooserDataSource$.next).toHaveBeenCalled();
  });

  it('should initialize search bar placeholder', () => {
    // given
    component.config = {groups: true, pages: true, users: true, workspaces: true};

    // when
    fixture.detectChanges();

    // then
    expect(component.searchBarPlaceholder).toEqual('Search for groups, pages, users and workspaces');
  });

});
