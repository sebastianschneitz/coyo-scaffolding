import {User} from '@domain/user/user';

export interface UserList {
  users: User[];
  pages: number;
  last: boolean;
  loading: boolean;
}
