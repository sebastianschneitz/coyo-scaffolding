import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {UserList} from '@shared/sender-ui/user-list/user-list';

@Component({
  selector: 'coyo-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserListComponent {
  /**
   * Received the userList to handle the information in the list
   */
  @Input() state: UserList;

  /**
   * Enables or disables the "show more" button
   */
  @Input() enableShowMore: boolean = true;

  /**
   * Emit the loadMore action to the widget when the loadMore button is pressed
   */
  @Output() load: EventEmitter<Event> = new EventEmitter();

  constructor() {
  }

  onLoadMore(): void {
    this.load.emit();
  }
}
