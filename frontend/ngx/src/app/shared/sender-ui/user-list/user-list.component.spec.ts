import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {UserFollowComponent} from '@shared/sender-ui/user-follow/user-follow.component';

import { UserListComponent } from './user-list.component';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserListComponent ]
    }).overrideTemplate(UserListComponent, '')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should emit the userList load event', () => {

    // given
    spyOn(component.load, 'emit');

    // when
    component.onLoadMore();

    // then
    expect(component.load.emit).toHaveBeenCalled();
  });
});
