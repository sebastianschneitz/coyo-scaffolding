import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Sender} from '@domain/sender/sender';
import {TargetService} from '@domain/sender/target/target.service';
import {SenderLinkTarget} from '@shared/sender-ui/sender-link/sender-link-target';
import {of} from 'rxjs';
import {SenderLinkComponent} from './sender-link.component';

describe('SenderLinkComponent', () => {
  let component: SenderLinkComponent;
  let fixture: ComponentFixture<SenderLinkComponent>;
  let targetService: jasmine.SpyObj<TargetService>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [SenderLinkComponent],
        providers: [{
          provide: TargetService,
          useValue: jasmine.createSpyObj('targetService', ['getLinkTo', 'canLinkTo'])
        }]
      }).compileComponents();

    targetService = TestBed.get(TargetService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderLinkComponent);
    component = fixture.componentInstance;
  });

  it('should init the link', () => {
    // given
    component.sender = {target: {}} as SenderLinkTarget;

    const link = 'some-link';
    const active$ = of(true);
    targetService.getLinkTo.and.returnValue(link);
    targetService.canLinkTo.and.returnValue(active$);

    // when
    fixture.detectChanges();

    // then
    expect(component.link).toEqual(link);
    expect(component.active$).toEqual(active$);
  });

  it('should update the link', () => {
    // given
    component.sender = {target: {}} as SenderLinkTarget;

    const link = 'some-link';
    const active$ = of(true);
    targetService.getLinkTo.and.returnValue(link);
    targetService.canLinkTo.and.returnValue(active$);
    fixture.detectChanges();

    const link2 = 'some-link2';
    const active2$ = of(false);
    targetService.getLinkTo.and.returnValue(link2);
    targetService.canLinkTo.and.returnValue(active2$);

    // when
    component.ngOnChanges(null);

    // then
    expect(component.link).toEqual(link2);
    expect(component.active$).toEqual(active2$);
  });
});
