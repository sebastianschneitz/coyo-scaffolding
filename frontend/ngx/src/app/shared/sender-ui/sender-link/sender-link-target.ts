import {Target} from '@domain/sender/target';

export interface SenderLinkTarget {
  displayName: string;
  target: Target;
}
