import {TestBed} from '@angular/core/testing';
import {DocumentService} from '@domain/file/document/document.service';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {Ng1FileLibraryModalService} from '@root/typings';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import {VideoFromFileLibraryPlugin} from '@shared/rte/video-plugin/video-from-file-library-plugin';
import {NG1_FILE_LIBRARY_MODAL_SERVICE} from '@upgrade/upgrade.module';
import * as _ from 'lodash';

describe('VideoFromGSuitePlugin', () => {
  let videoFromFileLibraryPlugin: VideoFromFileLibraryPlugin;
  let froala: jasmine.SpyObj<any>;
  let fileLibraryModalService: jasmine.SpyObj<Ng1FileLibraryModalService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VideoFromFileLibraryPlugin, {
        provide: FROALA_EDITOR,
        useValue: jasmine.createSpyObj('froala', ['RegisterCommand'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }, {
        provide: DocumentService,
        useValue: jasmine.createSpyObj('documentService', ['getStreamUrl'])
      }, {
        provide: NG1_FILE_LIBRARY_MODAL_SERVICE,
        useValue: jasmine.createSpyObj('fileLibraryModalService', ['open'])
      }]
    });

    videoFromFileLibraryPlugin = TestBed.get(VideoFromFileLibraryPlugin);
    froala = TestBed.get(FROALA_EDITOR);
    fileLibraryModalService = TestBed.get(NG1_FILE_LIBRARY_MODAL_SERVICE);
  });

  it('should be created', () => {
    expect(videoFromFileLibraryPlugin).toBeTruthy();
  });

  it('should not initialize video file library command without permissions', () => {
    // when
    videoFromFileLibraryPlugin.initialize({canAccessFiles: false} as RteSettings);

    // then
    expect(froala.RegisterCommand).not.toHaveBeenCalled();
  });

  it('should initialize video file library command', () => {
    // when
    videoFromFileLibraryPlugin.initialize({canAccessFiles: true} as RteSettings);

    // then
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertVideoFromFileLibrary', jasmine.any(Object));
  });

  it('should open file library', () => {
    // given
    const editor = {
      selection: jasmine.createSpyObj('selection', ['text', 'save', 'restore']),
      $oel: jasmine.createSpyObj('$oel', ['find']),
      video: jasmine.createSpyObj('video', ['insert']),
      getSender: () => '',
      getApp: () => '',
    };
    editor.$oel.find.and.returnValue({
      scrollTop: () => {}
    });
    fileLibraryModalService.open.and.returnValue(Promise.resolve());

    // when
    videoFromFileLibraryPlugin.initialize({canAccessFiles: true} as RteSettings);
    callCallbackOfCommand('coyoInsertVideoFromFileLibrary', editor);

    // then
    expect(fileLibraryModalService.open).toHaveBeenCalled();
  });

  function callCallbackOfCommand(commandKey: string, editor: any): void {
    const calls = froala.RegisterCommand.calls.all();
    const command = _.find(calls, (call: jasmine.CallInfo): boolean => call.args[0] === commandKey);
    command.args[1].callback.apply(editor);
  }
});
