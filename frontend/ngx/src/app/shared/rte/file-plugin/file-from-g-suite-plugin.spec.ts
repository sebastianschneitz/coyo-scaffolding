import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {GDrivePickerService} from '@app/integration/gsuite/g-drive-picker/g-drive-picker.service';
import {UrlService} from '@core/http/url/url.service';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {NotificationService} from '@shared/notifications/notification/notification.service';
import {FileFromGSuitePlugin} from '@shared/rte/file-plugin/file-from-g-suite-plugin';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import * as _ from 'lodash';

describe('FileFromGSuitePlugin', () => {
  let fileFromGSuitePlugin: FileFromGSuitePlugin;
  let gDrivePickerService: jasmine.SpyObj<GDrivePickerService>;
  let froala: jasmine.SpyObj<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FileFromGSuitePlugin, {
        provide: FROALA_EDITOR,
        useValue: jasmine.createSpyObj('froala', ['RegisterCommand'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }, {
        provide: GDrivePickerService,
        useValue: jasmine.createSpyObj('gDrivePickerService', ['open'])
      }, {
        provide: NotificationService,
        useValue: jasmine.createSpyObj('notificationService', ['error'])
      }, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['getBackendUrl'])
      }]
    });

    fileFromGSuitePlugin = TestBed.get(FileFromGSuitePlugin);
    gDrivePickerService = TestBed.get(GDrivePickerService);
    froala = TestBed.get(FROALA_EDITOR);
  });

  it('should be created', () => {
    expect(fileFromGSuitePlugin).toBeTruthy();
  });

  it('should not initialize file GSuite command without permissions', () => {
    // when
    fileFromGSuitePlugin.initialize({canAccessGoogle: false} as RteSettings);

    // then
    expect(froala.RegisterCommand).not.toHaveBeenCalled();
  });

  it('should initialize file GSuite command', () => {
    // when
    fileFromGSuitePlugin.initialize({canAccessGoogle: true} as RteSettings);

    // then
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertFileFromGSuite', jasmine.any(Object));
  });

  it('should open GSuite picker', () => {
    // given
    const editor = {
      selection: jasmine.createSpyObj('selection', ['text', 'save', 'restore']),
      $oel: jasmine.createSpyObj('$oel', ['find']),
      link: jasmine.createSpyObj('link', ['insert']),
      undo: jasmine.createSpyObj('undo', ['saveStep'])
    };
    editor.$oel.find.and.returnValue({
      scrollTop: () => {}
    });
    gDrivePickerService.open.and.returnValue(Promise.reject());

    // when
    fileFromGSuitePlugin.initialize({canAccessGoogle: true} as RteSettings);
    callCallbackOfCommand('coyoInsertFileFromGSuite', editor);

    // then
    expect(gDrivePickerService.open).toHaveBeenCalled();
  });

  function callCallbackOfCommand(commandKey: string, editor: any): void {
    const calls = froala.RegisterCommand.calls.all();
    const command = _.find(calls, (call: jasmine.CallInfo): boolean => call.args[0] === commandKey);
    command.args[1].callback.apply(editor);
  }
});
