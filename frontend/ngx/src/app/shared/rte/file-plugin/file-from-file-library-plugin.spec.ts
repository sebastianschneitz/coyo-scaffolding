import {TestBed} from '@angular/core/testing';
import {FileService} from '@domain/file/file/file.service';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {Ng1FileLibraryModalService} from '@root/typings';
import {FileFromFileLibraryPlugin} from '@shared/rte/file-plugin/file-from-file-library-plugin';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import {NG1_FILE_LIBRARY_MODAL_SERVICE} from '@upgrade/upgrade.module';
import * as _ from 'lodash';

describe('FileFromFileLibraryPlugin', () => {
  let fileFromFileLibraryPlugin: FileFromFileLibraryPlugin;
  let froala: jasmine.SpyObj<any>;
  let fileLibraryModalService: jasmine.SpyObj<Ng1FileLibraryModalService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileFromFileLibraryPlugin, {
        provide: FROALA_EDITOR,
        useValue: jasmine.createSpyObj('froala', ['RegisterCommand'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }, {
        provide: FileService,
        useValue: jasmine.createSpyObj('fileService', ['getDeepUrl'])
      }, {
        provide: NG1_FILE_LIBRARY_MODAL_SERVICE,
        useValue: jasmine.createSpyObj('fileLibraryModalService', ['open'])
      }]
    });

    fileFromFileLibraryPlugin = TestBed.get(FileFromFileLibraryPlugin);
    froala = TestBed.get(FROALA_EDITOR);
    fileLibraryModalService = TestBed.get(NG1_FILE_LIBRARY_MODAL_SERVICE);
  });

  it('should be created', () => {
    expect(fileFromFileLibraryPlugin).toBeTruthy();
  });

  it('should not initialize file library command without permissions', () => {
    // when
    fileFromFileLibraryPlugin.initialize({canAccessFiles: false} as RteSettings);

    // then
    expect(froala.RegisterCommand).not.toHaveBeenCalled();
  });

  it('should initialize file library command', () => {
    // when
    fileFromFileLibraryPlugin.initialize({canAccessFiles: true} as RteSettings);

    // then
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertFileFromFileLibrary', jasmine.any(Object));
  });

  it('should open file library', () => {
    // given
    const editor = {
      id: '',
      selection: jasmine.createSpyObj('selection', ['text', 'save', 'restore']),
      $oel: jasmine.createSpyObj('$oel', ['find']),
      link: jasmine.createSpyObj('link', ['insert']),
      getSender: () => '',
      getApp: () => '',
    };
    editor.$oel.find.and.returnValue({
      scrollTop: () => {}
    });
    fileLibraryModalService.open.and.returnValue(Promise.resolve({displayName: 'name'}));

    // when
    fileFromFileLibraryPlugin.initialize({canAccessFiles: true} as RteSettings);
    callCallbackOfCommand('coyoInsertFileFromFileLibrary', editor);

    // then
    expect(fileLibraryModalService.open).toHaveBeenCalled();
  });

  function callCallbackOfCommand(commandKey: string, editor: any): void {
    const calls = froala.RegisterCommand.calls.all();
    const command = _.find(calls, (call: jasmine.CallInfo): boolean => call.args[0] === commandKey);
    command.args[1].callback.apply(editor);
  }
});
