import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {RteComponent} from './rte.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoRte', downgradeComponent({
    component: RteComponent
  }));
