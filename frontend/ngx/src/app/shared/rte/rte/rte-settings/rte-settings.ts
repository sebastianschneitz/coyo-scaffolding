/**
 * The settings for the rte
 */
export class RteSettings {
  license: string;
  canAccessFiles: boolean;
  canAccessGoogle: boolean;
}
