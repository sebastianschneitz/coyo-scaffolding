import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {RibbonComponent} from './ribbon.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoRibbon', downgradeComponent({
    component: RibbonComponent,
    propagateDigest: false
  }));
