import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RibbonComponent} from './ribbon.component';

describe('RibbonComponent', () => {
  let component: RibbonComponent;
  let fixture: ComponentFixture<RibbonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RibbonComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RibbonComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the ribbon type on change', () => {
    // given
    component.type = 'sticky';

    // when
    fixture.detectChanges();

    // then
    expect(component.ribbonType).toBeDefined();
    expect(component.ribbonType.className).toBe('ribbon-sticky');
  });

  it('should set the ribbon type on init', () => {
    // given
    component.type = 'sticky';

    // when
    component.ngOnInit();

    // then
    expect(component.ribbonType).toBeDefined();
    expect(component.ribbonType.className).toBe('ribbon-sticky');
  });
});
