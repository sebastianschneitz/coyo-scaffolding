/**
 * Interface for pdf view settings
 */
export interface SettingPreset {
  title: string;
  stickToPage: boolean;
  fitToPage: boolean;
  autoresize: boolean;
  originalSize: boolean;
  zoom: number;
}
