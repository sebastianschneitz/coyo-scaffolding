import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {PdfViewerToolbarComponent} from './pdf-viewer-toolbar/pdf-viewer-toolbar.component';
import {PdfViewerComponent} from './pdf-viewer/pdf-viewer.component';
import './pdf-viewer/pdf-viewer.component.downgrade';

/**
 * A lazy loaded module that contains components and services used to display PDF files.
 */
@NgModule({
  imports: [
    PdfViewerModule,
    CoyoCommonsModule,
    CoyoFormsModule
  ],
  declarations: [
    PdfViewerComponent,
    PdfViewerToolbarComponent
  ],
  exports: [
    PdfViewerComponent
  ],
  entryComponents: [
    PdfViewerComponent
  ]
})
export class PdfLazyModule {

}
