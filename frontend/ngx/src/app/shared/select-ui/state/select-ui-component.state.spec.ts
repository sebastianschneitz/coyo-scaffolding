import {TestBed} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {NgxsModule, Store} from '@ngxs/store';
import {Load, LoadMore} from '@shared/select-ui/state/select-ui-component.actions';
import {
  SelectUiComponentState,
  SelectUiComponentStateModel,
  SelectUiComponentStatesModel
} from '@shared/select-ui/state/select-ui-component.state';

import {Observable, of} from 'rxjs';

/*tslint:disable:arrow-return-shorthand*/
const createSearchResult = (...searchTerms: string[]) => {
  return {
    content: searchTerms,
    first: true,
    last: false,
    number: 0,
    numberOfElements: searchTerms.length,
    size: 10,
    sort: null,
    totalElements: 20,
    totalPages: 2,
    empty: false
  } as Page<string>;
};
const createDefaultSettings = (
  searchFn: (pageable: Pageable, term: string) => Observable<Page<string>>,
  compareFn: (a: string, b: string) => boolean) => {
  return {
    closeOnSelect: true,
    multiselect: false,
    searchFn,
    compareFn,
    clearable: true,
    placeholder: 'test-placeholder',
    debounceTime: 150,
    pageSize: 10,
    scrollOffsetTrigger: 5,
  };
};
const createDefaultState = () => {
  return {
    items: new Array<string>(),
    loading: true,
    itemCount: 0,
    currentPage: 0,
    lastPage: false,
    typeahead: '',
    searchTerm: ''
  };
};
describe('SelectUiComponentState', () => {

  let store: Store;
  let searchFn: jasmine.Spy;
  let compareFn: jasmine.Spy;
  let defaultSearchResult: Observable<Page<string>>;
  let defaultState: { [component: string]: { [id: string]: SelectUiComponentStateModel } };
  const componentId = 'test-component-id';
  const componentStateName = 'selectUiComponent';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([SelectUiComponentState])],
      providers: []
    });
    store = TestBed.get(Store);
    // create defaults
    store = TestBed.get(Store);

    // default states
    defaultSearchResult = of(createSearchResult('test-1', 'test-2', 'test-3'));
    defaultState = {[componentStateName]: {[componentId]: createDefaultState()}};

    // search and compare callback spies
    searchFn = jasmine.createSpy('searchFn');
    searchFn.and.callFake(((pageable: Pageable, term: string) => defaultSearchResult));
    compareFn = jasmine.createSpy('compareFn');
    compareFn.and.callFake((a: string, b: string) => a === b);

  });

  it('should load', () => {
    // given
    const settings = createDefaultSettings(searchFn, compareFn);
    const expectedState = createDefaultState();
    expectedState.itemCount = 3;
    expectedState.items = ['test-1', 'test-2', 'test-3'];
    expectedState.loading = false;

    // when
    store.dispatch(new Load(componentId, settings, ''));
    const snapshot = store.selectSnapshot<SelectUiComponentStatesModel>(s => s[componentStateName]);
    // then
    expect(searchFn).toHaveBeenCalledWith(new Pageable(0, settings.pageSize), '');
    expect(snapshot).toBeDefined();
    expect(snapshot[componentId]).toBeDefined();
    expect(snapshot[componentId]).toEqual(expectedState);
  });

  it('should load more', () => {
    // given
    const settings = createDefaultSettings(searchFn, compareFn);
    settings.pageSize = 3;

    const defaultSearchResult1 = createSearchResult('test-1', 'test-2', 'test-3');
    defaultSearchResult1.totalElements = 5;
    const defaultSearchResult2 = createSearchResult('test-4', 'test-5');
    defaultSearchResult2.first = false;
    defaultSearchResult2.last = true;
    defaultSearchResult2.totalElements = 5;
    // expected states
    const expectedState1 = createDefaultState();
    expectedState1.itemCount = defaultSearchResult1.numberOfElements;
    expectedState1.items = defaultSearchResult1.content;
    expectedState1.loading = false;

    const expectedState2 = createDefaultState();
    expectedState2.itemCount = defaultSearchResult1.numberOfElements + defaultSearchResult2.numberOfElements;
    expectedState2.items = [...defaultSearchResult1.content, ...defaultSearchResult2.content];
    expectedState2.loading = false;
    expectedState2.lastPage = true;

    // when
    searchFn.and.callFake(((pageable: Pageable, term: string) => of(defaultSearchResult1)));
    store.dispatch(new Load(componentId, settings, ''));
    const snapshot1 = store.selectSnapshot<SelectUiComponentStatesModel>(s => s[componentStateName]);
    searchFn.and.callFake(((pageable: Pageable, term: string) => of(defaultSearchResult2)));
    store.dispatch(new LoadMore(componentId, settings));
    const snapshot2 = store.selectSnapshot<SelectUiComponentStatesModel>(s => s[componentStateName]);

    // then
    expect(searchFn).toHaveBeenCalledWith(new Pageable(0, settings.pageSize), '');
    expect(searchFn).toHaveBeenCalledWith(new Pageable(1, settings.pageSize), '');
    expect(searchFn).toHaveBeenCalledTimes(2);

    expect(snapshot1).toBeDefined();
    expect(snapshot1[componentId]).toBeDefined();
    expect(snapshot1[componentId]).toEqual(expectedState1);

    expect(snapshot2).toBeDefined();
    expect(snapshot2[componentId]).toBeDefined();
    expect(snapshot2[componentId]).toEqual(expectedState2);
  });

  it('should not load more after the last page', () => {
    // given
    const result = createSearchResult('test-1', 'test-2', 'test-3');
    result.last = true;
    result.totalElements = 3;
    defaultSearchResult = of(result);
    const settings = createDefaultSettings(searchFn, compareFn);
    const expectedState = createDefaultState();
    expectedState.itemCount = 3;
    expectedState.items = ['test-1', 'test-2', 'test-3'];
    expectedState.loading = false;
    expectedState.lastPage = true;

    // when
    store.dispatch(new Load(componentId, settings, ''));
    const snapshot1 = store.selectSnapshot<SelectUiComponentStatesModel>(s => s[componentStateName]);
    store.dispatch(new LoadMore(componentId, settings));
    const snapshot2 = store.selectSnapshot<SelectUiComponentStatesModel>(s => s[componentStateName]);
    // then
    expect(searchFn).toHaveBeenCalledWith(new Pageable(0, settings.pageSize), '');
    expect(snapshot1).toBeDefined();
    expect(snapshot1[componentId]).toBeDefined();
    expect(snapshot1[componentId]).toEqual(expectedState);
    expect(snapshot2).toBeDefined();
    expect(snapshot2[componentId]).toBeDefined();
    expect(snapshot2[componentId]).toEqual(expectedState);
  });
});
