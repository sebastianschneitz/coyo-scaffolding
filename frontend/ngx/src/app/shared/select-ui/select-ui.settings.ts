import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {Observable} from 'rxjs';

export interface SelectUiSettings<T> {
  /**
   * Will the dropdown close when a value is selected
   */
  closeOnSelect: boolean;
  /**
   * Can multiple values be selected
   */
  multiselect: boolean;
  /**
   * Service for selecting objects
   */
  searchFn: ((pageable: Pageable, term: string) => Observable<Page<T>>);
  /**
   * Comparator for elements
   */
  compareFn: ((a: T, b: T) => boolean);
  /**
   * Can the value be cleared by the user
   */
  clearable: boolean;
  /**
   * Placeholder for the select input field
   */
  placeholder: string;
  /**
   * Debounce time for typeahead inputs
   */
  debounceTime: number;
  /**
   * Number of entries to load per page request
   */
  pageSize: number;
  /**
   * Scrolling offset to trigger loading of next page (Typically set to 5)
   */
  scrollOffsetTrigger: number;
}
