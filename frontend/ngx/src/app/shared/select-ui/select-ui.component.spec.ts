import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {NgSelectComponent} from '@ng-select/ng-select';
import {Store} from '@ngxs/store';
import {SelectUiComponent} from '@shared/select-ui/select-ui.component';
import {Load, LoadMore} from '@shared/select-ui/state/select-ui-component.actions';
import {SelectUiComponentStateModel} from '@shared/select-ui/state/select-ui-component.state';
import {UuidService} from '@shared/uuid/uuid.service';

import {Observable, of} from 'rxjs';

/*tslint:disable:arrow-return-shorthand*/
const createSearchResult = (...searchTerms: string[]) => {
  return {
    content: searchTerms,
    first: true,
    last: false,
    number: 0,
    numberOfElements: searchTerms.length,
    size: 10,
    sort: null,
    totalElements: 20,
    totalPages: 2,
    empty: false
  } as Page<string>;
};
const createDefaultSettings = (
  searchFn: (pageable: Pageable, term: string) => Observable<Page<string>>,
  compareFn: (a: string, b: string) => boolean) => {
  return {
    closeOnSelect: true,
    multiselect: false,
    searchFn,
    compareFn,
    clearable: true,
    placeholder: 'test-placeholder',
    debounceTime: 150,
    pageSize: 10,
    scrollOffsetTrigger: 5,
  };
};
const createDefaultState = () => {
  return {
    items: new Array<string>(),
    loading: true,
    itemCount: 0,
    currentPage: 0,
    lastPage: false,
    typeahead: '',
    searchTerm: ''
  };
};
describe('SelectUiComponent', () => {

  let component: SelectUiComponent<string>;
  let fixture: ComponentFixture<SelectUiComponent<any>>;
  let uuidService: jasmine.SpyObj<UuidService>;
  let store: jasmine.SpyObj<Store>;
  let searchFn: jasmine.Spy;
  let compareFn: jasmine.Spy;
  let defaultSearchResult: Observable<Page<string>>;
  let defaultState: { [component: string]: { [id: string]: SelectUiComponentStateModel } };
  const componentId = 'test-component-id';
  const componentStateName = 'selectUiComponent';
  const select = {
    registerOnChange: jasmine.createSpy(),
    registerOnTouched: jasmine.createSpy(),
    writeValue: jasmine.createSpy(),
    setDisabledState: jasmine.createSpy()
  };
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [SelectUiComponent],
        providers: [{
          provide: Store,
          useValue: jasmine.createSpyObj('store', ['select', 'dispatch'])
        },
          {
            provide: UuidService,
            useValue: jasmine.createSpyObj('uuidService', ['getUuid'])
          }
        ]
      })
      .overrideTemplate(SelectUiComponent, '<div></div>')
      .compileComponents()
      .then(() => {

        // create defaults
        uuidService = TestBed.get(UuidService);
        uuidService.getUuid.and.returnValue(componentId);
        store = TestBed.get(Store);

        // default states
        defaultSearchResult = of(createSearchResult('test-1', 'test-2', 'test-3'));
        defaultState = {[componentStateName]: {[componentId]: createDefaultState()}};

        // search and compare callback spies
        searchFn = jasmine.createSpy('searchFn');
        searchFn.and.callFake(((pageable: Pageable, term: string) => defaultSearchResult));
        compareFn = jasmine.createSpy('compareFn');
        compareFn.and.callFake((a: string, b: string) => a === b);

        // setup
        store.select.and.callFake((selector: (state: any, ...states: any[]) => string) => of(selector(defaultState)));
        fixture = TestBed.createComponent(SelectUiComponent);
        component = fixture.componentInstance;
        component.settings = createDefaultSettings(searchFn, compareFn);
        component.ngSelect = select as any as NgSelectComponent;
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', fakeAsync(() => {
    // given

    // when
    fixture.detectChanges();
    tick();
    // then
    component.state$.subscribe(result => {
      expect(result).toEqual(createDefaultState());
    });
    expect(component.typeahead$).toBeDefined();
    expect(store.select).toHaveBeenCalledTimes(1);
    expect(store.dispatch).toHaveBeenCalledTimes(1);
    expect(store.dispatch).toHaveBeenCalledWith(new Load(componentId, component.settings, ''));
  }));

  it('should correctly handle typeahead events', fakeAsync(() => {

    // when
    fixture.detectChanges();
    tick();
    component.typeahead$.next('x');
    tick(component.settings.debounceTime);
    // expect
    expect(store.dispatch).toHaveBeenCalledTimes(2);
    expect(store.dispatch).toHaveBeenCalledWith(new Load(componentId, component.settings, ''));
    expect(store.dispatch).toHaveBeenCalledWith(new Load(componentId, component.settings, 'x'));
  }));

  it('should not load more when scroll does not reach reload trigger', fakeAsync(() => {

    // given
    const itemCountBase = 10;
    const event = {
      start: 0,
      end: itemCountBase
    };
    // when
    fixture.detectChanges();
    tick();
    component.ngSelect = {items: Array.from(Array(itemCountBase + component.settings.scrollOffsetTrigger).keys())} as any as NgSelectComponent;
    component.onScroll(event);

    // then
    expect(store.dispatch).toHaveBeenCalledTimes(1);
    expect(store.dispatch).toHaveBeenCalledWith(new Load(componentId, component.settings, ''));
  }));

  it('should load more when scroll does reach reload trigger', fakeAsync(() => {

    // given
    const itemCountBase = 10;
    const event = {
      start: 1,
      end: itemCountBase + 1
    };
    // when
    fixture.detectChanges();
    tick();
    component.ngSelect = {items: Array.from(Array(itemCountBase + component.settings.scrollOffsetTrigger).keys())} as any as NgSelectComponent;
    component.onScroll(event);

    // then
    expect(store.dispatch).toHaveBeenCalledTimes(2);
    expect(store.dispatch).toHaveBeenCalledWith(new Load(componentId, component.settings, ''));
    expect(store.dispatch).toHaveBeenCalledWith(new LoadMore(componentId, component.settings));
  }));

  it('should pass through ControlValueAccessor functions to the child component', fakeAsync(() => {
    // given

    // when
    fixture.detectChanges();
    component.ngSelect = jasmine.createSpyObj('NgSelectComponent',
      ['registerOnChange', 'registerOnTouched', 'setDisabledState', 'writeValue']);
    component.registerOnChange('a');
    component.registerOnTouched('b');
    component.writeValue('c');
    component.setDisabledState(true);
    tick();
    // then

    expect(component.ngSelect.registerOnChange).toHaveBeenCalledWith('a');
    expect(component.ngSelect.registerOnTouched).toHaveBeenCalledWith('b');
    expect(component.ngSelect.writeValue).toHaveBeenCalledWith('c');
    expect(component.ngSelect.setDisabledState).toHaveBeenCalledWith(true);
  }));
});
