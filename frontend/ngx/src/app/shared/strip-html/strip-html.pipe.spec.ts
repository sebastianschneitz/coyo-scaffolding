import {StripHtmlService} from '@shared/strip-html/strip-html.service';
import {StripHtmlPipe} from './strip-html.pipe';

describe('StripHtmlPipe', () => {
  let pipe: StripHtmlPipe;
  let stripHtmlService: jasmine.SpyObj<StripHtmlService>;

  beforeEach(() => {
    stripHtmlService = jasmine.createSpyObj('StripHtmlService', ['strip']);
    pipe = new StripHtmlPipe(stripHtmlService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('remove html', () => {
    // given
    const text = 'some random text embedded in html';
    const html = '<div><span>' + text + '</span></div>';

    stripHtmlService.strip.and.returnValue(text);

    // when
    const result = pipe.transform(html);

    // then
    expect(result).toEqual(text);
    expect(stripHtmlService.strip).toHaveBeenCalledWith(html);
  });

  it('remove html from lists and keep whitespaces', () => {
    // given
    const text = 'some random list entry';
    const html = '<ul><li>' + text + '</li></ul>';

    stripHtmlService.strip.and.returnValue(text);

    // when
    const result = pipe.transform(html);

    // then
    expect(result).toEqual(text);
    expect(stripHtmlService.strip).toHaveBeenCalledWith(html);
  });
});
