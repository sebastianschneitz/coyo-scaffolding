import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {PageTitleService} from '@shared/notifications/page-title/page-title.service';

getAngularJSGlobal()
  .module('commons.ui')
  .factory('ngxPageTitleService', downgradeInjectable(PageTitleService));
