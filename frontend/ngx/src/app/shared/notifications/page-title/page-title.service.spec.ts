import {TestBed} from '@angular/core/testing';
import {Title} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {PageTitleService} from './page-title.service';

describe('PageTitleService', () => {
  let titleService: jasmine.SpyObj<Title>;
  let translateService: jasmine.SpyObj<TranslateService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: Title,
        useValue: jasmine.createSpyObj('Title', ['setTitle'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('TranslateService', ['instant', 'get'])
      }]
    });

    titleService = TestBed.get(Title);
    translateService = TestBed.get(TranslateService);
  });

  it('should be created', () => {
    const service = TestBed.get(PageTitleService);

    // then
    expect(service).toBeTruthy();
  });

  it('should get and set the page prefix', () => {
    const service = TestBed.get(PageTitleService);

    // when
    const prefix = service.getPrefix();
    service.setPrefix('COYO');
    const prefix2 = service.getPrefix();

    // then
    expect(prefix).toEqual('');
    expect(prefix2).toEqual('COYO');
    expect(titleService.setTitle).toHaveBeenCalledWith('COYO');
  });

  it('should get and set the page title', () => {
    const service = TestBed.get(PageTitleService);

    // when
    const title = service.getTitle();
    service.setTitle('My Page');
    const title2 = service.getTitle();

    // then
    expect(title).toEqual('');
    expect(title2).toEqual('My Page');
    expect(titleService.setTitle).toHaveBeenCalledWith('My Page');
  });

  it('should translate the page prefix if i18n flag set', () => {
    const service = TestBed.get(PageTitleService);
    const translation = {'PAGE.NAME': 'My Page'};
    translateService.get.and.callFake(() => of(translation['PAGE.NAME']));

    // when
    service.setTitle('PAGE.NAME', true);
    const title = service.getTitle();

    // then
    expect(title).toEqual('My Page');
    expect(titleService.setTitle).toHaveBeenCalledWith('My Page');
  });

  it('should not translate the page prefix if i18n flag not set', () => {
    const service = TestBed.get(PageTitleService);

    // when
    service.setTitle('My Page');
    const title = service.getTitle();

    // then
    expect(title).toEqual('My Page');
    expect(titleService.setTitle).toHaveBeenCalledWith('My Page');
  });

  it('should set the count', () => {
    const service = TestBed.get(PageTitleService);

    // when
    service.setCount(3);

    // then
    expect(titleService.setTitle).toHaveBeenCalledWith('(3)');
  });

  it('should combine the title', () => {
    const service = TestBed.get(PageTitleService);

    // when
    service.setPrefix('COYO');
    service.setTitle('My Page');
    service.setCount(3);

    // then
    expect(titleService.setTitle).toHaveBeenCalledWith('(3) COYO | My Page');
  });

  it('should not display the count if it is zero', () => {
    const service = TestBed.get(PageTitleService);

    // when
    service.setPrefix('COYO');
    service.setTitle('My Page');

    // then
    expect(titleService.setTitle).toHaveBeenCalledWith('COYO | My Page');
  });
});
