import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {JitTranslationService} from '@shared/jit-translation/jit-translation/jit-translation.service';
import {UserNotificationChannel} from '@shared/notifications/user-notification-settings/user-notification-channel.enum';
import {UserNotificationSetting} from '@shared/notifications/user-notification-settings/user-notification-setting';
import {Subject} from 'rxjs';
import {take} from 'rxjs/operators';
import {UserNotificationSettingsService} from './user-notification-settings.service';

describe('UserNotificationSettingsService', () => {
  let httpTestingController: HttpTestingController;
  let authService: jasmine.SpyObj<AuthService>;
  let isAuthenticated$: Subject<boolean>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [JitTranslationService, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['isAuthenticated$', 'getCurrentUserId'])
      }]
    });

    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    authService = TestBed.get(AuthService);

    isAuthenticated$ = new Subject<boolean>();
    authService.isAuthenticated$.and.returnValue(isAuthenticated$.asObservable());
    authService.getCurrentUserId.and.returnValue('userId');
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const service: UserNotificationSettingsService = TestBed.get(UserNotificationSettingsService);

    // then
    expect(service).toBeTruthy();
  });

  it('should retrieve settings', done => {
    const service: UserNotificationSettingsService = TestBed.get(UserNotificationSettingsService);

    // when
    const response: UserNotificationSetting<any>[] = [
      {active: true, channel: UserNotificationChannel.Browser, properties: {}},
      {active: true, channel: UserNotificationChannel.Sound},
      {active: false, channel: UserNotificationChannel.Push}];

    // then
    service.retrieve().pipe(take(1)).subscribe(settings => expect(settings).toEqual(response));
    service.retrieve().pipe(take(1)).subscribe(_ => done()); // this should be cached!

    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/notification-settings');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(response);
  });

  it('should retrieve settings by channel', done => {
    const service: UserNotificationSettingsService = TestBed.get(UserNotificationSettingsService);

    // when
    const response: UserNotificationSetting<any>[] = [
      {active: true, channel: UserNotificationChannel.Browser, properties: {}},
      {active: true, channel: UserNotificationChannel.Sound},
      {active: false, channel: UserNotificationChannel.Push}];

    // then
    service.retrieveByChannel(UserNotificationChannel.Sound).pipe(take(1)).subscribe(settings => {
      expect(settings).toEqual(response[1]);
      done();
    });

    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/notification-settings');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(response);
  });

  it('should fail if user is not authenticated', done => {
    authService.getCurrentUserId.and.returnValue(null);
    const service: UserNotificationSettingsService = TestBed.get(UserNotificationSettingsService);

    // then
    service.retrieve().pipe(take(1)).subscribe(() => {
    }, error => {
      expect(error).toEqual('Not authenticated');
      done();
    });
  });

  it('should not cache failed requests', done => {
    const service: UserNotificationSettingsService = TestBed.get(UserNotificationSettingsService);

    // when
    const error = new ErrorEvent('any error');
    const response: UserNotificationSetting<any>[] = [
      {active: true, channel: UserNotificationChannel.Browser, properties: {}},
      {active: true, channel: UserNotificationChannel.Sound},
      {active: false, channel: UserNotificationChannel.Push}];

    service.retrieve().pipe(take(1)).subscribe(() => {}, e => expect(e.error).toEqual(error));
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/notification-settings');
    req.error(error);

    // then
    service.retrieve().pipe(take(1)).subscribe(settings => {
      expect(settings).toEqual(response);
      done();
    });

    const req2 = httpTestingController.expectOne(request => request.url === '/web/users/userId/notification-settings');
    expect(req2.request.method).toEqual('GET');

    // finally
    req2.flush(response);
  });

  it('should reset cache on user logout', done => {
    const service: UserNotificationSettingsService = TestBed.get(UserNotificationSettingsService);

    // when
    const response: UserNotificationSetting<any>[] = [
      {active: true, channel: UserNotificationChannel.Browser, properties: {}},
      {active: true, channel: UserNotificationChannel.Sound},
      {active: false, channel: UserNotificationChannel.Push}];

    // then
    service.retrieve().pipe(take(1)).subscribe(settings => expect(settings).toEqual(response));
    const req = httpTestingController.expectOne(request => request.url === '/web/users/userId/notification-settings');
    req.flush(response);

    // when
    isAuthenticated$.next(false);

    // then
    service.retrieve().pipe(take(1)).subscribe(_ => done()); // this should not be cached!
    const req2 = httpTestingController.expectOne(request => request.url === '/web/users/userId/notification-settings');
    req2.flush(response);
  });
});
