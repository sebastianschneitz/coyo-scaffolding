/**
 * The different notification channels for a user.
 */
export enum UserNotificationChannel {
  Browser = 'BROWSER',
  Push = 'PUSH',
  Sound = 'SOUND'
}
