import {UserNotificationChannel} from './user-notification-channel.enum';

/**
 * The notification settings for a specific channel.
 */
export interface UserNotificationSetting<T> {
  active: boolean;
  channel: UserNotificationChannel;
  properties?: T;
}
