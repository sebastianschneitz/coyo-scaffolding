import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {NotificationService} from './notification.service';

getAngularJSGlobal()
  .module('commons.ui')
  .factory('ngxNotificationService', downgradeInjectable(NotificationService));
