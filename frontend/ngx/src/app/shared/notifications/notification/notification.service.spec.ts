import {TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {NotificationService} from './notification.service';

describe('NotificationService', () => {
  let toastrService: jasmine.SpyObj<ToastrService>;
  let translateService: jasmine.SpyObj<TranslateService>;
  let service: NotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: ToastrService,
        useValue: jasmine.createSpyObj('ToastrService', ['success', 'info', 'warning', 'error', 'clear'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('TranslateService', ['instant'])
      }]
    });

    toastrService = TestBed.get(ToastrService);
    translateService = TestBed.get(TranslateService);
    service = TestBed.get(NotificationService);

    translateService.instant.and.callFake((key: string) => `i18n-${key}`);
  });

  const shouldShowNotification = (level: 'success' | 'info' | 'warning' | 'error', title: string, message: string) => {
    it(`should show notification with '${level}'-level (with i18n)`, () => {
      // when
      service[level](message, title);

      // then
      expect(translateService.instant).toHaveBeenCalledWith(title, undefined);
      expect(translateService.instant).toHaveBeenCalledWith(message, undefined);
      expect(toastrService[level]).toHaveBeenCalledWith(`i18n-${message}`, `i18n-${title}`, undefined);
    });

    it(`should show notification with '${level}'-level (with i18n params)`, () => {
      // given
      const params = {param: 'value'};

      // when
      service[level](message, title, params);

      // then
      expect(translateService.instant).toHaveBeenCalledWith(title, params);
      expect(translateService.instant).toHaveBeenCalledWith(message, params);
      expect(toastrService[level]).toHaveBeenCalledWith(`i18n-${message}`, `i18n-${title}`, undefined);
    });

    it(`should show notification with '${level}'-level (without i18n)`, () => {
      // when
      service[level](message, title, false);

      // then
      expect(translateService.instant).not.toHaveBeenCalled();
      expect(toastrService[level]).toHaveBeenCalledWith(message, title, undefined);
    });

    it(`should show notification with '${level}'-level and custom configuration`, () => {
      // given
      const config = {timeOut: 2500};

      // when
      service[level](message, title, false, config);

      // then
      expect(toastrService[level]).toHaveBeenCalledWith(message, title, config);
    });
  };

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  shouldShowNotification('success', 'Title-success', 'Message-success');
  shouldShowNotification('info', 'Title-info', 'Message-info');
  shouldShowNotification('warning', 'Title-warning', 'Message-warning');
  shouldShowNotification('error', 'Title-error', 'Message-error');

  it('should clear', () => {
    // when
    service.clear();

    // then
    expect(toastrService.clear).toHaveBeenCalledWith();
  });
});
