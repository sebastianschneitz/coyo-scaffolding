import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {TabNotificationService} from '@shared/notifications/tab-notification/tab-notification.service';

getAngularJSGlobal()
  .module('commons.ui')
  .factory('ngxTabNotificationService', downgradeInjectable(TabNotificationService));
