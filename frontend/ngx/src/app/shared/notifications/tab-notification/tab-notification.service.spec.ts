import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {PageTitleService} from '@shared/notifications/page-title/page-title.service';
import {of, Subject} from 'rxjs';
import {PageVisibilityService} from '../page-visibility/page-visibility.service';
import {UserNotificationChannel} from '../user-notification-settings/user-notification-channel.enum';
import {UserNotificationSettingsService} from '../user-notification-settings/user-notification-settings.service';
import {TabNotificationService} from './tab-notification.service';

describe('TabNotificationService', () => {
  let pageTitleService: jasmine.SpyObj<PageTitleService>;
  let authService: jasmine.SpyObj<AuthService>;
  let pageVisibilityService: jasmine.SpyObj<PageVisibilityService>;
  let userNotificationSettingsService: jasmine.SpyObj<UserNotificationSettingsService>;

  let isAuthenticated$: Subject<boolean>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['isAuthenticated$'])
      }, {
        provide: PageTitleService,
        useValue: jasmine.createSpyObj('PageTitleService', ['setCount'])
      }, {
        provide: PageVisibilityService,
        useValue: jasmine.createSpyObj('PageVisibilityService', ['isVisible$'])
      }, {
        provide: UserNotificationSettingsService,
        useValue: jasmine.createSpyObj('UserNotificationSettingsService', ['retrieveByChannel'])
      }]
    });

    pageTitleService = TestBed.get(PageTitleService);
    authService = TestBed.get(AuthService);
    pageVisibilityService = TestBed.get(PageVisibilityService);
    userNotificationSettingsService = TestBed.get(UserNotificationSettingsService);

    isAuthenticated$ = new Subject<boolean>();

    authService.isAuthenticated$.and.returnValue(isAuthenticated$.asObservable());
    pageVisibilityService.isVisible$.and.returnValue(of(true));
    userNotificationSettingsService.retrieveByChannel.and.returnValue(of({
      active: true,
      channel: UserNotificationChannel.Sound
    }));
  });

  it('should be created', () => {
    const service = TestBed.get(TabNotificationService);

    // then
    expect(service).toBeTruthy();
  });

  it('should get and set the current category count', () => {
    const service = TestBed.get(TabNotificationService);

    // given
    service.set('category', 1);
    service.set('category2', 2);

    // when
    const result1 = service.get('category');
    const result2 = service.get('category2');
    const result3 = service.get('category3');

    // then
    expect(result1).toEqual(1);
    expect(result2).toEqual(2);
    expect(result3).toEqual(0);
  });

  it('should get and set the total count', () => {
    const service = TestBed.get(TabNotificationService);

    // given
    service.set('category', 1);
    service.set('category2', 2);

    // when
    const result = service.get();

    // then
    expect(result).toEqual(3);
  });

  it('should reset the category count', () => {
    const service = TestBed.get(TabNotificationService);

    // given
    service.set('category', 1);
    service.set('category2', 2);

    // when
    service.reset('category');
    const result1 = service.get('category');
    const result2 = service.get('category2');

    // then
    expect(result1).toEqual(0);
    expect(result2).toEqual(2);
  });

  it('should reset the total count', () => {
    const service = TestBed.get(TabNotificationService);

    // given
    service.set('category', 1);
    service.set('category2', 2);

    // when
    service.reset();
    const result1 = service.get('category');
    const result2 = service.get('category2');
    const result3 = service.get();

    // then
    expect(result1).toEqual(0);
    expect(result2).toEqual(0);
    expect(result3).toEqual(0);
  });

  it('should reset on logout', () => {
    const service = TestBed.get(TabNotificationService);

    // given
    service.set('category', 1);

    // when
    isAuthenticated$.next(false);
    const result = service.get('category');

    // then
    expect(result).toEqual(0);
  });

  it('should update count', fakeAsync(() => {
    const service = TestBed.get(TabNotificationService);

    // when
    service.set('category', 1);
    service.set('category2', 2);
    tick(250);

    // then
    expect(pageTitleService.setCount).toHaveBeenCalledTimes(1);
    expect(pageTitleService.setCount).toHaveBeenCalledWith(3);
  }));

  it('should play sound', fakeAsync(() => {
    pageVisibilityService.isVisible$.and.returnValue(of(false));
    const service = TestBed.get(TabNotificationService);
    const playSound = spyOn<any>(service, 'playSound').and.callThrough();

    // when
    service.set('category', 1);
    tick(250);

    // then
    expect(playSound).toHaveBeenCalledTimes(1);
  }));

  it('should play sound if count does not increase', fakeAsync(() => {
    pageVisibilityService.isVisible$.and.returnValue(of(false));
    const service = TestBed.get(TabNotificationService);
    const playSound = spyOn<any>(service, 'playSound').and.callThrough();

    // when
    service.set('category', 2);
    tick(250);
    playSound.calls.reset();
    service.set('category', 2);
    tick(250);
    service.set('category', 1);
    tick(250);

    // then
    expect(playSound).not.toHaveBeenCalled();
  }));

  it('should not play sound if page is visible', fakeAsync(() => {
    const service = TestBed.get(TabNotificationService);
    const playSound = spyOn<any>(service, 'playSound').and.callThrough();

    // when
    service.set('category', 1);
    tick(250);

    // then
    expect(playSound).not.toHaveBeenCalled();
  }));

  it('should not play sound if sound is disabled', fakeAsync(() => {
    pageVisibilityService.isVisible$.and.returnValue(of(false));
    userNotificationSettingsService.retrieveByChannel.and.returnValue(of({
      active: false,
      channel: UserNotificationChannel.Sound
    }));
    const service = TestBed.get(TabNotificationService);
    const playSound = spyOn<any>(service, 'playSound').and.callThrough();

    // when
    service.set('category', 1);
    tick(250);

    // then
    expect(playSound).not.toHaveBeenCalled();
  }));
});
