import {DOCUMENT} from '@angular/common';
import {TestBed} from '@angular/core/testing';
import {skip, take} from 'rxjs/operators';
import {PageVisibilityService} from './page-visibility.service';

describe('PageVisibilityService', () => {
  let document: jasmine.SpyObj<Document>;
  let service: PageVisibilityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: DOCUMENT,
        useValue: jasmine.createSpyObj('Document', ['addEventListener'])
      }]
    });

    document = TestBed.get(DOCUMENT);
  });

  it('should be created', () => {
    service = TestBed.get(PageVisibilityService);

    // then
    expect(service).toBeTruthy();
  });

  it('should add event listener (default)', done => {
    // given
    (document as any).hidden = true;

    // when
    service = TestBed.get(PageVisibilityService);
    service.isVisible$().pipe(take(1)).subscribe(isVisible => {
      expect(isVisible).toEqual(false);
      done();
    });

    // then
    expect(document.addEventListener).toHaveBeenCalledWith('visibilitychange', jasmine.any(Function), false);
  });

  it('should add event listener (ms)', done => {
    // given
    (document as any).msHidden = true;

    // when
    service = TestBed.get(PageVisibilityService);
    service.isVisible$().pipe(take(1)).subscribe(isVisible => {
      expect(isVisible).toEqual(false);
      done();
    });

    // then
    expect(document.addEventListener).toHaveBeenCalledWith('msvisibilitychange', jasmine.any(Function), false);
  });

  it('should add event listener (webkit)', done => {
    // given
    (document as any).webkitHidden = true;

    // when
    service = TestBed.get(PageVisibilityService);
    service.isVisible$().pipe(take(1)).subscribe(isVisible => {
      expect(isVisible).toEqual(false);
      done();
    });

    // then
    expect(document.addEventListener).toHaveBeenCalledWith('webkitvisibilitychange', jasmine.any(Function), false);
  });

  it('should fail if the API is not available', done => {
    // when
    service = TestBed.get(PageVisibilityService);
    service.isVisible$().subscribe(() => {
    }, error => {
      expect(error).toEqual('EventListener or Page Visibility API not supported');
      done();
    });

    // then
    expect(document.addEventListener).not.toHaveBeenCalled();
  });

  it('should execute the event listener', done => {
    // given
    (document as any).hidden = true;

    // when
    service = TestBed.get(PageVisibilityService);
    service.isVisible$().pipe(skip(1)).pipe(take(1)).subscribe(isVisible => {
      expect(isVisible).toEqual(false);
      done();
    });

    // then
    document.addEventListener.calls.mostRecent().args[1]();
  });
});
