import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {MatDialogSize} from '@coyo/ui';
import {HelpModalComponent} from '../help-modal/help-modal.component';
import {HelpComponent} from './help.component';

describe('HelpComponent', () => {
  let component: HelpComponent;
  let fixture: ComponentFixture<HelpComponent>;
  let dialog: jasmine.SpyObj<MatDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HelpComponent],
      providers: [{
        provide: MatDialog,
        useValue: jasmine.createSpyObj('dialog', ['open'])
      }]
    }).overrideTemplate(HelpComponent, '')
      .compileComponents();

    dialog = TestBed.get(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpComponent);
    component = fixture.componentInstance;
  });

  it('should initialize with default values', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component.placement).toBe('left');
  });

  it('should open a modal on click when modal text is set', () => {
    // given
    component.modalText = 'TEST';
    const event = jasmine.createSpyObj('event', ['preventDefault']) as unknown as Event;

    // when
    component.openModal(event);

    // then
    expect(dialog.open).toHaveBeenCalledWith(HelpModalComponent, {
      width: MatDialogSize.Large,
      data: component.modalText
    });
    expect(event.preventDefault).toHaveBeenCalled();
  });
});
