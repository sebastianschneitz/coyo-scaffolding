import {ChangeDetectionStrategy, Component, Input, OnDestroy} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TooltipPosition} from '@angular/material/tooltip';
import {MatDialogSize} from '@coyo/ui';
import {HelpModalComponent} from '../help-modal/help-modal.component';

/**
 * Renders a help icon with an info text in a tooltip.
 */
@Component({
  selector: 'coyo-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HelpComponent implements OnDestroy {
  private dialogRef: MatDialogRef<HelpModalComponent, void>;

  /**
   * Open a modal when the button is clicked.
   */
  @Input() modalText?: string;

  /**
   * The tooltip text.
   */
  @Input() tip: string;

  /**
   * The placement of the tooltip.
   */
  @Input() placement?: TooltipPosition = 'left';

  constructor(private dialog: MatDialog) {
  }

  /**
   * Opens a modal.
   *
   * @param $event The click event
   */
  openModal($event: Event): void {
    $event.preventDefault();
    if (this.modalText) {
      this.dialogRef = this.dialog.open<HelpModalComponent, string>(HelpModalComponent, {
        width: MatDialogSize.Large,
        data: this.modalText
      });
    }
  }

  ngOnDestroy(): void {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }
}
