import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {StickyBtnComponent} from './sticky-btn.component';

describe('StickyBtnComponent', () => {
  let component: StickyBtnComponent;
  let fixture: ComponentFixture<StickyBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StickyBtnComponent]
    }).overrideTemplate(StickyBtnComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StickyBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should register on change', () => {
    // given
    const onChange = (_: number) => {};

    // when
    component.registerOnChange(onChange);

    // then
    expect((component as any).onChange).toBe(onChange);
  });

  it('should write model value', () => {
    // given
    const value = 1;

    // when
    component.writeValue(value);

    // then
    expect(component.model).toBe(value);
  });

  it('should toggle button state', () => {
    // given
    const onChange = jasmine.createSpy();
    component.model = 1;
    component.registerOnChange(onChange);
    fixture.detectChanges();

    // when
    component.setSelectedSticky(2);

    // then
    expect(component.model).toBe(2);
    expect(onChange).toHaveBeenCalled();
  });
});
