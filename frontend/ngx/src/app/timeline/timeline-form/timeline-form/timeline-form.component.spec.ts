import {ChangeDetectorRef, Renderer2, Type} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {AuthService} from '@core/auth/auth.service';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {User} from '@domain/user/user';
import {Ng1ScrollBehaviourService} from '@root/typings';
import {NG1_SCROLL_BEHAVIOR_SERVICE} from '@upgrade/upgrade.module';
import {NgxPermissionsService} from 'ngx-permissions';
import {of, Subject} from 'rxjs';
import {StickyExpiryPeriods} from '../sticky-btn/sticky-expiry-periods';
import {TimelineFormComponent} from './timeline-form.component';

describe('TimelineFormComponent', () => {
  let component: TimelineFormComponent;
  let fixture: ComponentFixture<TimelineFormComponent>;

  let authService: jasmine.SpyObj<AuthService>;
  let timelineItemService: jasmine.SpyObj<TimelineItemService>;
  let senderService: jasmine.SpyObj<SenderService>;
  let permissionService: jasmine.SpyObj<NgxPermissionsService>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;
  let scrollBehaviourService: jasmine.SpyObj<Ng1ScrollBehaviourService>;

  let user: User;
  let sender: Sender;
  let context: Sender;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [FormBuilder, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['getUser'])
      }, {
        provide: TimelineItemService,
        useValue: jasmine.createSpyObj('TimelineItemService', ['post'])
      }, {
        provide: SenderService,
        useValue: jasmine.createSpyObj('SenderService', ['get'])
      }, {
        provide: NgxPermissionsService,
        useValue: jasmine.createSpyObj('NgxPermissionsService', ['hasPermission'])
      }, {
        provide: ChangeDetectorRef,
        useValue: jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck'])
      }, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['isXs', 'observeScreenChange'])
      }, {
        provide: NG1_SCROLL_BEHAVIOR_SERVICE, useValue: jasmine.createSpyObj('scrollBehaviourService',
          ['enableBodyScrolling', 'disableBodyScrolling', 'disableBodyScrollingOnXsScreen'])
      }, Renderer2],
      declarations: [TimelineFormComponent]
    }).overrideTemplate(TimelineFormComponent, '<textarea #message></textarea>')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineFormComponent);
    component = fixture.componentInstance;
  });

  beforeEach(() => {
    authService = TestBed.get(AuthService);
    timelineItemService = TestBed.get(TimelineItemService);
    senderService = TestBed.get(SenderService);
    permissionService = TestBed.get(NgxPermissionsService);
    windowSizeService = TestBed.get(WindowSizeService);
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.XS));
    scrollBehaviourService = TestBed.get(NG1_SCROLL_BEHAVIOR_SERVICE);

    user = {id: 'userId', displayName: 'user'} as User;
    sender = {id: 'senderId', displayName: 'sender'} as Sender;
    context = {id: 'contextId', displayName: 'context'} as Sender;
    authService.getUser.and.returnValue(of(user));
    senderService.get.and.returnValue(of(context));
    permissionService.hasPermission.and.returnValue(of(true));

    component.type = 'personal';
    component.context = context;
  });

  it('should create', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init the form data for "personal" type', fakeAsync(() => {
    // given
    component.type = 'personal';
    component.context = undefined;

    // when
    fixture.detectChanges();
    tick();

    // then
    expect(component.timelineForm.get('author').value).toEqual(user);
    expect(component.timelineForm.get('recipientIds').value).toEqual(['userId']);
    expect(component.timelineForm.get('restricted').value).toEqual(false);
    expect(component.timelineForm.get('message').value).toEqual('');
  }));

  it('should init the form data for "sender" type', fakeAsync(() => {
    // given
    component.type = 'sender';
    component.context = context;

    // when
    fixture.detectChanges();
    tick();

    // then
    expect(component.timelineForm.get('recipientIds').value).toEqual(['contextId']);
    expect(component.timelineForm.get('restricted').value).toEqual(false);
    expect(component.timelineForm.get('message').value).toEqual('');
  }));

  it('should submit a post', fakeAsync(() => {
    // given
    fixture.detectChanges();
    tick();

    timelineItemService.post.and.returnValue(of({} as TimelineItem));
    component.timelineForm.get('message').setValue('Hello world!');
    component.timelineForm.get('restricted').setValue(true);
    component.timelineForm.get('stickyExpiry').setValue(StickyExpiryPeriods.oneDay.expiry);
    component.timelineForm.get('author').setValue(sender);

    // when
    component.submit();
    tick();

    // then
    const args = timelineItemService.post.calls.mostRecent().args[0];
    expect(args.type).toEqual('post');
    expect(args.authorId).toEqual('senderId');
    expect(args.recipientIds).toEqual(['userId']);
    expect(args.data).toEqual({message: 'Hello world!'});
    expect(args.restricted).toEqual(true);
    expect(args.attachments).toEqual([]);
    expect(args.fileLibraryAttachments).toEqual([]);
    expect(args.webPreviews).toEqual({});

    // check sticky expiry date
    const compareDate = new Date(new Date().getTime() + StickyExpiryPeriods.oneDay.expiry).setSeconds(0, 0);
    const stickyExpiryDate = new Date(args.stickyExpiry).setSeconds(0, 0);
    expect(stickyExpiryDate).toEqual(compareDate);
  }));

  it('should prevent background scrolling', () => {
    // given
    fixture.detectChanges();

    windowSizeService.isXs.and.returnValue(true);
    const renderer = fixture.componentRef.injector.get<Renderer2>(Renderer2 as Type<Renderer2>);
    spyOn(renderer, 'addClass');

    // when
    component.onFocus();

    // then
    expect(scrollBehaviourService.disableBodyScrollingOnXsScreen).toHaveBeenCalled();
  });

  it('should reenable after reset background scrolling', () => {
    // given
    fixture.detectChanges();

    windowSizeService.isXs.and.returnValue(true);

    // when
    component.reset();

    // then
    expect(scrollBehaviourService.enableBodyScrolling).toHaveBeenCalled();
  });

  it('should reenable scrolling when changing from XS to another screen size', () => {
    // given
    const subject = new Subject<ScreenSize>();
    windowSizeService.observeScreenChange.and.returnValue(subject.asObservable());

    fixture.detectChanges();

    component.timelineForm.markAsTouched();

    // when
    subject.next(ScreenSize.SM);

    // then
    expect(scrollBehaviourService.enableBodyScrolling).toHaveBeenCalled();
  });

  it('should submit form on control and enter keydown', () => {
    // given
    spyOn(component, 'submit');
    spyOn(component.input.nativeElement, 'blur');
    fixture.detectChanges();
    component.timelineForm.get('message').setValue('Hello world!');

    // when
    component.onCtrlEnter();

    // then
    expect(component.submit).toHaveBeenCalled();
    expect(component.input.nativeElement.blur).toHaveBeenCalled();
  });

  it('should not submit form on control and enter keydown if form is invalid', () => {
    // given
    spyOn(component, 'submit');
    spyOn(component.input.nativeElement, 'blur');
    fixture.detectChanges();
    component.timelineForm.setErrors({invalid: true});

    // when
    component.onCtrlEnter();

    // then
    expect(component.submit).not.toHaveBeenCalled();
    expect(component.input.nativeElement.blur).not.toHaveBeenCalled();
  });

  it('should change the recipient when using funcitonal user on a personal timeline', () => {
    // given
    fixture.detectChanges();
    expect(component.timelineForm.get('recipientIds').value).toEqual([user.id]);

    // when
    component.changeSelectedSender(sender);

    // then
    expect(component.timelineForm.get('recipientIds').value).toEqual([sender.id]);
  });
});
