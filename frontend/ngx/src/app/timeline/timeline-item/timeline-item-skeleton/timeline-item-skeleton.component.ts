import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

/**
 * Skeleton component for timeline items
 */
@Component({
  selector: 'coyo-timeline-item-skeleton',
  template: '<div [class.single]="single" class="container"><div [class.single]="single"></div></div>',
  styleUrls: ['./timeline-item-skeleton.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineItemSkeletonComponent {

  /**
   * Flag for showing just a single timeline item skeleton instead of 8
   */
  @Input() single: boolean;
}
