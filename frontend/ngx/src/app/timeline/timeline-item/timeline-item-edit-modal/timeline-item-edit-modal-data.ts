/**
 * The data that needs to be provided to the TimelineItemEditModalComponent.
 */
export interface TimelineItemEditModalData {
  itemId: string;
}
