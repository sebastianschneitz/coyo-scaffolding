/**
 * The data that needs to be provided to the TimelineItemReportModalComponent.
 */
export interface TimelineItemReportModalData {
  targetId: string;
  targetType: string;
}
