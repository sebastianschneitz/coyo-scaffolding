import {HttpClient} from '@angular/common/http';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subject} from 'rxjs';
import {TimelineItemReportModalComponent} from './timeline-item-report-modal.component';

describe('TimelineItemReportModalComponent', () => {
  let component: TimelineItemReportModalComponent;
  let fixture: ComponentFixture<TimelineItemReportModalComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<TimelineItemReportModalComponent>>;
  let httpClient: jasmine.SpyObj<HttpClient>;
  let backendResponse: Subject<object>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineItemReportModalComponent],
      providers: [{
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('dialogRef', ['close'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: {
          targetId: 'target-id',
          targetType: 'target-type'
        }
      }, {
        provide: HttpClient,
        useValue: jasmine.createSpyObj('HttpClient', ['post'])
      }, FormBuilder]
    }).overrideTemplate(TimelineItemReportModalComponent, '')
      .compileComponents();

    dialogRef = TestBed.get(MatDialogRef);
    backendResponse = new Subject<object>();
    httpClient = TestBed.get(HttpClient);
    httpClient.post.and.returnValue(backendResponse.asObservable());
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemReportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    backendResponse.complete();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // then
    expect(component.reportForm.contains('targetId')).toBeTruthy();
    expect(component.reportForm.get('targetId').value).toEqual('target-id');
    expect(component.reportForm.contains('targetType')).toBeTruthy();
    expect(component.reportForm.get('targetType').value).toEqual('target-type');
    expect(component.reportForm.contains('message')).toBeTruthy();
    expect(component.reportForm.get('message').value).toEqual('');
    expect(component.reportForm.contains('anonymous')).toBeTruthy();
    expect(component.reportForm.get('anonymous').value).toBeFalsy();
  });

  it('should submit form data', fakeAsync(() => {
    // given
    component.reportForm.get('message').setValue('Report form message');
    component.reportForm.get('anonymous').setValue(true);

    // when
    component.onSubmit();
    backendResponse.next({});
    tick();

    // then
    expect(httpClient.post).toHaveBeenCalledWith('/web/reports', {
      targetId: 'target-id',
      targetType: 'target-type',
      message: 'Report form message',
      anonymous: true
    });
    expect(dialogRef.close).toHaveBeenCalledWith(true);
  }));
});
