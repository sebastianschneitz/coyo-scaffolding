import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {TimelineStreamComponent} from './timeline-stream.component';

getAngularJSGlobal()
  .module('coyo.app')
  .directive('coyoTimelineStream', downgradeComponent({
    component: TimelineStreamComponent,
    propagateDigest: false
  }));
