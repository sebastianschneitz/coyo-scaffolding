import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {IntegrationSettingsComponent} from './integration-settings/integration-settings.component';
import './integration-settings/integration-settings.component.downgrade';

/**
 * Module which contains user setting related components.
 */
@NgModule({
  imports: [
    CommonModule,
    CoyoFormsModule,
    TranslateModule
  ],
  declarations: [
    IntegrationSettingsComponent
  ],
  entryComponents: [
    IntegrationSettingsComponent
  ]
})
export class AccountModule {}
