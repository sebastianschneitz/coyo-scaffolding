import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {IntegrationSettingsComponent} from '@app/account/integration-settings/integration-settings.component';

getAngularJSGlobal()
  .module('coyo.account')
  .directive('coyoIntegrationSettingsComponent', downgradeComponent({
    component: IntegrationSettingsComponent
  }));
