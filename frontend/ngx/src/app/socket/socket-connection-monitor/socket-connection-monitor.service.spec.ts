import {async, TestBed} from '@angular/core/testing';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {SocketConnectionLostModalComponent} from '@app/socket/socket-connection-lost-modal/socket-connection-lost-modal.component';
import {SocketConnectionMonitorService} from '@app/socket/socket-connection-monitor/socket-connection-monitor.service';
import {SocketService} from '@core/socket/socket.service';
import {WINDOW} from '@root/injection-tokens';
import {NotificationService} from '@shared/notifications/notification/notification.service';
import {ToastRef} from 'ngx-toastr/toastr/toast-injector';
import {Subject} from 'rxjs';

describe('SocketConnectionMonitorService', () => {
  let service: SocketConnectionMonitorService;
  let socketService: jasmine.SpyObj<SocketService>;
  let offlineObservable: Subject<void>;
  let disconnectObservable: Subject<[Event, boolean]>;
  let reconnectObservable: Subject<void>;
  let notificationService: jasmine.SpyObj<NotificationService>;
  let toastRef: jasmine.SpyObj<ToastRef<void>>;
  let toastAfterClosedObservable: Subject<void>;
  let location: jasmine.SpyObj<Location>;
  let dialog: jasmine.SpyObj<MatDialog>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<SocketConnectionLostModalComponent, boolean>>;
  let dialogAfterClosedObservable: Subject<boolean>;
  const event: Event = {} as any as Event;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [SocketConnectionMonitorService, {
        provide: SocketService,
        useValue: jasmine.createSpyObj('SocketService', ['listenToOffline$', 'listenToDisconnect$', 'listenToReconnect$'])
      }, {
        provide: NotificationService,
        useValue: jasmine.createSpyObj('NotificationService', ['error'])
      }, {
        provide: WINDOW,
        useValue: {location: jasmine.createSpyObj('Location', ['reload'])}
      }, {
        provide: MatDialog,
        useValue: jasmine.createSpyObj('MatDialog', ['open'])
      }]
    });
  }));

  beforeEach(() => {
    service = TestBed.get(SocketConnectionMonitorService);
    socketService = TestBed.get(SocketService);
    notificationService = TestBed.get(NotificationService);
    location = TestBed.get(WINDOW).location;
    dialog = TestBed.get(MatDialog);

    offlineObservable = new Subject();
    socketService.listenToOffline$.and.returnValue(offlineObservable);
    disconnectObservable = new Subject();
    socketService.listenToDisconnect$.and.returnValue(disconnectObservable);
    reconnectObservable = new Subject();
    socketService.listenToReconnect$.and.returnValue(reconnectObservable);

    toastRef = jasmine.createSpyObj('ToastRef', ['close', 'afterClosed']);
    toastAfterClosedObservable = new Subject();
    toastRef.afterClosed.and.returnValue(toastAfterClosedObservable);
    toastRef.close.and.callFake(() => toastAfterClosedObservable.next());
    notificationService.error.and.returnValue({toastRef: toastRef});

    dialogRef = jasmine.createSpyObj('MatDialogRef', ['close', 'afterClosed']);
    dialogAfterClosedObservable = new Subject();
    dialogRef.afterClosed.and.returnValue(dialogAfterClosedObservable);
    dialogRef.close.and.callFake((reload: boolean) => dialogAfterClosedObservable.next(reload));
    dialog.open.and.returnValue(dialogRef);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize', () => {
    // when
    service.init();

    // then
    expect(notificationService.error).not.toHaveBeenCalled();
    expect(dialog.open).not.toHaveBeenCalled();
    expect(location.reload).not.toHaveBeenCalled();
  });

  it('should not show notification when disconnected without notification flag', () => {
    // given
    service.init();

    // when
    disconnectObservable.next([event, false]);

    // then
    expect(notificationService.error).not.toHaveBeenCalled();
    expect(dialog.open).not.toHaveBeenCalled();
    expect(location.reload).not.toHaveBeenCalled();
  });

  it('should show notification when disconnected with notification flag', () => {
    // given
    service.init();

    // when
    disconnectObservable.next([event, true]);

    // then
    expect(notificationService.error).toHaveBeenCalled();
    expect(dialog.open).not.toHaveBeenCalled();
    expect(location.reload).not.toHaveBeenCalled();
  });

  it('should show notification just once on consecutive disconnects with notification flag', () => {
    // given
    service.init();
    disconnectObservable.next([event, true]);

    // when
    disconnectObservable.next([event, true]);

    // then
    expect(notificationService.error).toHaveBeenCalled();
    expect(notificationService.error.calls.count()).toBe(1);
    expect(dialog.open).not.toHaveBeenCalled();
    expect(location.reload).not.toHaveBeenCalled();
  });

  it('should close notification when reconnected', () => {
    // given
    service.init();
    disconnectObservable.next([event, true]);

    // when
    reconnectObservable.next();

    // then
    expect(toastRef.close).toHaveBeenCalled();
  });

  it('should close notification when destroyed', () => {
    // given
    service.init();
    disconnectObservable.next([event, true]);

    // when
    service.ngOnDestroy();

    // then
    expect(toastRef.close).toHaveBeenCalled();
  });

  it('should show connection lost warning modal dialog when offline', () => {
    // given
    const noReload = false;
    service.init();

    // when
    offlineObservable.next();

    // then
    expect(notificationService.error).not.toHaveBeenCalled();
    expect(toastRef.close).not.toHaveBeenCalled();
    expect(dialog.open).toHaveBeenCalled();

    // when modal is dismissed
    dialogRef.close(noReload);

    // then
    expect(location.reload).not.toHaveBeenCalled();
    expect(notificationService.error).not.toHaveBeenCalled();
  });

  it('should show connection lost warning modal dialog when offline and restore toast if shown before', () => {
    // given
    const noReload = false;
    service.init();
    disconnectObservable.next([event, true]);
    notificationService.error.calls.reset();

    // when
    offlineObservable.next();

    // then
    expect(notificationService.error).not.toHaveBeenCalled();
    expect(toastRef.close).toHaveBeenCalled();
    expect(dialog.open).toHaveBeenCalled();

    // when modal is dismissed
    toastRef.close.calls.reset();
    dialogRef.close(noReload);

    // then
    expect(toastRef.close).not.toHaveBeenCalled();
    expect(location.reload).not.toHaveBeenCalled();
    expect(notificationService.error).toHaveBeenCalled();
  });

  it('should reload page if connection lost warning modal was closed by reload button', () => {
    // given
    const doReload = true;
    service.init();
    offlineObservable.next();

    // when
    dialogRef.close(doReload);

    // then
    expect(location.reload).toHaveBeenCalled();
  });
});
