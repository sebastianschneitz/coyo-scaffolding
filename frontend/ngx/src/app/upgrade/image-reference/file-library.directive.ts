import {Directive, ElementRef, EventEmitter, Injector, Input, Output} from '@angular/core';
import {UpgradeComponent} from '@angular/upgrade/static';
import {Document} from '@domain/file/document';
import {Sender} from '@domain/sender/sender';
import {CropSettings} from '@shared/select-file/select-file-options';

/**
 * Renders an image from the file library as a picture element. Supports rendering different image sizes for
 * different screen sizes and also checks whether the display supports retina.
 *
 * This component has been upgraded from angularjs.
 */
@Directive({
  selector: 'coyo-file-library' // tslint:disable-line
})
export class FileLibraryDirective extends UpgradeComponent {

  /**
   * File library options
   */
  @Input() options: any;

  /**
   * The starting sender
   */
  @Input() sender: Sender;

  /**
   * Crop settings for the selected files
   */
  @Input() cropSettings: CropSettings;

  /**
   * Modal mode
   */
  @Input() modalMode: 'single' | 'multi';

  /**
   * The initially selected files
   */
  @Input() selectedFiles: Document[];

  /**
   * Event that is fired when files are selected
   */
  @Output() selectedFilesChange: EventEmitter<Document[]>;

  constructor(elementRef: ElementRef, injector: Injector) {
    super('coyoFileLibrary', elementRef, injector);
  }
}
