import {Report} from '@domain/reports/report';

/**
 * The state of the report list.
 */
export interface ReportListState {
  isLoading: boolean;
  reports: Report[];
  count: number;
  total: number;
}
