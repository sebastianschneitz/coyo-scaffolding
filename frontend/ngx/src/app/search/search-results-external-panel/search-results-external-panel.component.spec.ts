import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {O365ApiService} from '@app/integration/o365/o365-api/o365-api.service';
import {SearchResultsList} from '@app/search/search-results-list';
import {of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {SearchResultsExternalPanelComponent} from './search-results-external-panel.component';

describe('SearchResultsExternalPanelComponent', () => {
  let component: SearchResultsExternalPanelComponent;
  let fixture: ComponentFixture<SearchResultsExternalPanelComponent>;
  let o365apiService: jasmine.SpyObj<O365ApiService>;
  const url = 'http://test.com';
  const searchQuery = 'some-search-query';
  const searchProvider = 'some-search-provider';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchResultsExternalPanelComponent],
      providers: [{
        provide: O365ApiService,
        useValue: jasmine.createSpyObj('o365apiService', ['getSharePointUrl'])
      }]
    })
      .overrideTemplate(SearchResultsExternalPanelComponent, '')
      .compileComponents();

    o365apiService = TestBed.get(O365ApiService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultsExternalPanelComponent);
    component = fixture.componentInstance;
    component.externalSearchResults = {} as SearchResultsList;
    component.searchQuery = searchQuery;
    component.externalSearchResults.searchProvider = searchProvider;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get correct microsoft url', done => {
    // given
    component.externalSearchResults.searchProvider = 'MICROSOFT';
    o365apiService.getSharePointUrl.and.returnValue(of(url));

    // when
    component.ngOnInit();

    // then
    component.externalSearchURL$.subscribe(response => {
      expect(response).toEqual(url + '/_layouts/15/sharepoint.aspx?q=' + searchQuery + '&v=%2Fsearch%2Ffiles');
      done();
    });
  });

  it('should get correct google drive url', done => {
    // given
    component.externalSearchResults.searchProvider = 'GOOGLE';

    // when
    component.ngOnInit();

    // then
    component.externalSearchURL$.subscribe(response => {
      expect(response).toEqual('https://drive.google.com/drive/search?q=' + searchQuery);
      done();
    });
  });

  it('should get empty response for unknown search provider', done => {
    // given
    component.externalSearchResults.searchProvider = 'UNKNOWN-SEARCH-PROVIDER';

    // when
    component.ngOnInit();

    // then
    component.externalSearchURL$
      .pipe(catchError(() => of('error')))
      .subscribe(response => {
        expect(response).toBe('error');
        done();
    });
  });

  it('should get correct icon for google drive', () => {
    // given
    component.externalSearchResults.searchProvider = 'GOOGLE';

    // when
    component.ngOnInit();

    // then
    expect(component.externalSearchIcon).toEqual('mc-google-drive');
  });

  it('should get correct icon for microsoft', () => {
    // given
    component.externalSearchResults.searchProvider = 'MICROSOFT';
    o365apiService.getSharePointUrl.and.returnValue(of(url));

    // when
    component.ngOnInit();

    // then
    expect(component.externalSearchIcon).toEqual('sharepoint_mc');
  });

  it('should get empty response for unknown search provider ', () => {
    // given
    component.externalSearchResults.searchProvider = 'UNKNOWN-SEARCH-PROVIDER';

    // when
    component.ngOnInit();

    // then
    expect(component.externalSearchIcon).toBeUndefined();
  });
});
