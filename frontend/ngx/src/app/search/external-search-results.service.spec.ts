import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {ExternalSearchResultsService} from './external-search-results.service';
import {SearchResultsList} from './search-results-list';

describe('ExternalSearchResultsService', () => {
  let service: ExternalSearchResultsService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExternalSearchResultsService],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.get(ExternalSearchResultsService);
    http = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make external search results request', done => {
    // given
    const params = {term: '3', limit: '3'};
    const resp = [{
      searchProvider: 'MICROSOFT',
      searchResults: [],
      searchResultCountString: '0'
    } as SearchResultsList];

    // when
    const result = service.getExternalSearchesResults(params);

    // then
    result.subscribe(response => {
      expect(response).toBe(resp);
      done();
    });

    const req = http.expectOne(request => request.method === 'GET' && request.url === 'web/search/external');
    req.flush(resp);
  });
});
