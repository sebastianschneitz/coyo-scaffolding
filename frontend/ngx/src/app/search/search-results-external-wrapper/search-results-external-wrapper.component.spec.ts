import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {combineLatest, of} from 'rxjs';
import {ExternalSearchResultsService} from '../external-search-results.service';
import {SearchResultsList} from '../search-results-list';
import {SearchResultsExternalWrapperComponent} from './search-results-external-wrapper.component';

describe('SearchResultsExternalWrapperComponent', () => {
  let component: SearchResultsExternalWrapperComponent;
  let fixture: ComponentFixture<SearchResultsExternalWrapperComponent>;
  let integrationApiService: jasmine.SpyObj<IntegrationApiService>;
  let externalSearchResultsService: jasmine.SpyObj<ExternalSearchResultsService>;

  const response = [{
    searchProvider: 'MICROSOFT',
    searchResults: [],
    searchResultCountString: '0'
  } as SearchResultsList];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchResultsExternalWrapperComponent ],
      providers: [{
        provide: IntegrationApiService,
        useValue: jasmine.createSpyObj('integrationApiService', ['updateAndGetActiveState'])
      },
      {
        provide: ExternalSearchResultsService,
        useValue: jasmine.createSpyObj('externalSearchResultsService', ['getExternalSearchesResults'])
      }]
    })
    .overrideTemplate(SearchResultsExternalWrapperComponent, '')
    .compileComponents();

    integrationApiService = TestBed.get(IntegrationApiService);
    integrationApiService.updateAndGetActiveState.and.returnValue(of(true));
    externalSearchResultsService = TestBed.get(ExternalSearchResultsService);
    externalSearchResultsService.getExternalSearchesResults.and.returnValue(of(response));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultsExternalWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get searches and providers', done => {
    // given
    component.searchQuery = 'test';

    // when
    component.ngOnInit();

    // then
    combineLatest([component.providers$, component.searches$]).subscribe(([providerResp, searchesResp]) => {
      expect(providerResp).toEqual(['OFFICE_365', 'G_SUITE']);
      expect(searchesResp).toEqual(response);
      done();
    });
  });
});
