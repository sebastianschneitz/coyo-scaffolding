import {ChangeDetectorRef, SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Ng1LightBoxModalService} from '@root/typings';
import {NG1_LIGHT_BOX_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {ContentTypeService} from '@widgets/media/content-type/content-type.service';
import {MediaWidget} from '@widgets/media/media-widget';
import {MediaWidgetFile} from '@widgets/media/media-widget-settings/media-widget-settings.component';
import {MediaWidgetComponent} from './media-widget.component';

describe('MediaWidgetComponent', () => {
  let component: MediaWidgetComponent;
  let fixture: ComponentFixture<MediaWidgetComponent>;
  let contentTypeService: jasmine.SpyObj<ContentTypeService>;
  let lightBoxModalService: jasmine.SpyObj<Ng1LightBoxModalService>;
  let media1: MediaWidgetFile;
  let media2: MediaWidgetFile;
  let media3: MediaWidgetFile;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [MediaWidgetComponent],
        providers: [ChangeDetectorRef, {
          provide: ContentTypeService,
          useValue: jasmine.createSpyObj('contentTypeService', ['isImage', 'isVideo'])
        }, {
          provide: NG1_LIGHT_BOX_MODAL_SERVICE,
          useValue: jasmine.createSpyObj('ligthboxModalService', ['open'])
        }]
      }).overrideTemplate(MediaWidgetComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    contentTypeService = TestBed.get(ContentTypeService);
    lightBoxModalService = TestBed.get(NG1_LIGHT_BOX_MODAL_SERVICE);
    fixture = TestBed.createComponent(MediaWidgetComponent);

    media1 = {
      id: '1',
      sortOrderId: 0,
      preview: true
    } as MediaWidgetFile;

    media2 = {
      id: '2',
      sortOrderId: 1,
      preview: false
    } as MediaWidgetFile;

    media3 = {
      id: '3',
      sortOrderId: 2,
      preview: true
    } as MediaWidgetFile;

    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {
        _media: [media1, media2, media3]
      }
    } as MediaWidget;
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    expect(component.previewMedia).toEqual([media1, media3]);
  });

  it('should recalculate preview media after changes', () => {
    // given
    fixture.detectChanges();
    media2.preview = true;
    // when
    component.ngOnChanges({widget: {currentValue: component.widget} as SimpleChange});

    // then
    expect(component).toBeTruthy();
    expect(component.previewMedia).toEqual([media1, media2, media3]);
  });

  it('should check video type', () => {
    // given
    const contentType = 'video/mp4';
    contentTypeService.isVideo.and.returnValue(true);

    // when
    const result = component.isVideo({contentType} as MediaWidgetFile);

    // then
    expect(result).toBeTruthy();
    expect(contentTypeService.isVideo).toHaveBeenCalledWith(contentType);
  });

  it('should check image type', () => {
    // given
    const contentType = 'image/jpeg';
    contentTypeService.isImage.and.returnValue(true);

    // when
    const result = component.isImage({contentType} as MediaWidgetFile);

    // then
    expect(result).toBeTruthy();
    expect(contentTypeService.isImage).toHaveBeenCalledWith(contentType);
  });

  it('should show additional media overlay on last element', () => {
    // given
    fixture.detectChanges();

    // when
    const result = component.showAdditionalMediaOverlay(true);

    // then
    expect(result).toBeTruthy();
  });

  it('should not show additional media overlay if every element is previewed', () => {
    // given
    media2.preview = true;
    fixture.detectChanges();

    // when
    const result = component.showAdditionalMediaOverlay(true);

    // then
    expect(result).toBeFalsy();
  });

  it('should not show additional media overlay if every element is not last element', () => {
    // given
    fixture.detectChanges();

    // when
    const result = component.showAdditionalMediaOverlay(false);

    // then
    expect(result).toBeFalsy();
  });

  it('should open light box modal on click', () => {
    // when
    component.openLightBox(media1);

    // then
    expect(lightBoxModalService.open).toHaveBeenCalledWith(component.widget.settings, media1.id, media1.sortOrderId);
  });
});
