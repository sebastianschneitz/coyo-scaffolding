import {TestBed} from '@angular/core/testing';

import {ContentTypeService} from './content-type.service';

describe('ContentTypeService', () => {
  let service: ContentTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(ContentTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should recognize video files', () => {
    // when
    const resultVideo = service.isVideo('video/mp4');
    const resultImage = service.isVideo('image/jpeg');
    const resultJson = service.isVideo('application/json');

    // then
    expect(resultVideo).toBeTruthy();
    expect(resultImage).toBeFalsy();
    expect(resultJson).toBeFalsy();
  });

  it('should recognize image files', () => {
    // when
    const resultVideo = service.isImage('video/mp4');
    const resultImage = service.isImage('image/jpeg');
    const resultJson = service.isImage('application/json');

    // then
    expect(resultVideo).toBeFalsy();
    expect(resultImage).toBeTruthy();
    expect(resultJson).toBeFalsy();
  });

});
