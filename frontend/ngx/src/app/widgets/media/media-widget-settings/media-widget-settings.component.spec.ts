import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {Document} from '@domain/file/document';
import {SenderService} from '@domain/sender/sender/sender.service';
import {MediaWidget} from '@widgets/media/media-widget';
import {MediaWidgetSettings} from '@widgets/media/media-widget-settings';
import {Subject} from 'rxjs';
import {MediaWidgetFile, MediaWidgetSettingsComponent} from './media-widget-settings.component';

describe('MediaWidgetSettingsComponent', () => {
  let component: MediaWidgetSettingsComponent;
  let fixture: ComponentFixture<MediaWidgetSettingsComponent>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;
  let subject: Subject<ScreenSize>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [MediaWidgetSettingsComponent],
        providers: [FormBuilder, {
          provide: SenderService,
          useValue: jasmine.createSpyObj('senderService', ['getCurrentIdOrSlug', 'get'])
        }, {
          provide: WindowSizeService,
          useValue: jasmine.createSpyObj('windowSizeService', ['observeScreenChange'])
        }]
      })
      .overrideTemplate(MediaWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    subject = new Subject<ScreenSize>();
    windowSizeService = TestBed.get(WindowSizeService);
    windowSizeService.observeScreenChange.and.returnValue(subject);
    fixture = TestBed.createComponent(MediaWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {}
    } as MediaWidget;
    component.parentForm = new FormGroup({});
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init form with setting values', () => {
    // given
    const settings = {
      album: {
        title: 'title',
        description: 'description',
        location: 'location'
      },
      _media: [{
        description: 'media-description',
        id: 'media-id',
        preview: true,
        name: 'media-name',
        senderId: 'media-sender-id',
        contentType: 'image/jpeg',
        sortOrderId: 0
      }]
    } as MediaWidgetSettings;
    component.widget = {
      id: 'widget-id',
      settings
    } as MediaWidget;

    // when
    fixture.detectChanges();

    // then
    expect(component.parentForm.getRawValue()).toEqual(settings);
  });

  it('should add media that is not already inside the array', () => {
    // given
    const addedMedia1 = [{
      id: '1',
    }, {
      id: '2',
    }] as unknown as Document[];

    const addedMedia2 = [
      {
        id: '2'
      }, {
        id: '3'
      }
    ] as unknown as Document[];

    component.widget = {
      id: 'widget-id',
      settings: {}
    } as MediaWidget;

    fixture.detectChanges();

    // when
    component.onFilesAdded(addedMedia1);
    component.onFilesAdded(addedMedia2);

    // then
    expect(component.parentForm.getRawValue()._media).toEqual([{
      id: '1',
      sortOrderId: 0,
      name: null,
      senderId: null,
      contentType: null,
      description: null,
      preview: true
    }, {
      id: '2',
      sortOrderId: 1,
      name: null,
      senderId: null,
      contentType: null,
      description: null,
      preview: true
    }, {
      id: '3',
      sortOrderId: 2,
      name: null,
      senderId: null,
      contentType: null,
      description: null,
      preview: true
    }]);
  });

  it('should change the order by drag and drop', () => {
    // given
    const settings = {
      _media: [{
        id: '1',
        sortOrderId: 0
      }, {
        id: '2',
        sortOrderId: 1
      }, {
        id: '3',
        sortOrderId: 2
      }]
    } as MediaWidgetSettings;
    component.widget = {
      id: 'widget-id',
      settings
    } as MediaWidget;

    fixture.detectChanges();

    // when
    component.dropMedia({currentIndex: 0, previousIndex: 2} as CdkDragDrop<MediaWidgetFile>);

    // then
    expect(component.parentForm.getRawValue()._media).toEqual([{
      id: '3',
      sortOrderId: 0,
      name: null,
      senderId: null,
      contentType: null,
      description: null,
      preview: true
    }, {
      id: '1',
      sortOrderId: 1,
      name: null,
      senderId: null,
      contentType: null,
      description: null,
      preview: true
    }, {
      id: '2',
      sortOrderId: 2,
      name: null,
      senderId: null,
      contentType: null,
      description: null,
      preview: true
    }]);
  });

  it('should delete media', () => {
    // given
    const media = {
      id: '1',
      sortOrderId: 0
    } as MediaWidgetFile;

    const settings = {
      _media: [media]
    } as MediaWidgetSettings;

    component.widget = {
      id: 'widget-id',
      settings
    } as MediaWidget;

    fixture.detectChanges();

    // when
    component.deleteMedia(media);

    // then
    expect(component.parentForm.getRawValue()._media).toEqual([]);
  });
});
