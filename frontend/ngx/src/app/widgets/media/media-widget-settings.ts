import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a Media widget
 */
export interface MediaWidgetSettings extends WidgetSettings {
  _media: {
    name: string,
    id: string,
    senderId: string,
    contentType: string,
    description: string,
    preview: boolean,
    sortOrderId: number
  }[];
  album: {
    title: string;
    description: string;
    location: string;
  };
}
