import {Widget} from '@domain/widget/widget';
import {MediaWidgetSettings} from '@widgets/media/media-widget-settings';

export interface MediaWidget extends Widget<MediaWidgetSettings> {
}
