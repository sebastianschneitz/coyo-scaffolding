import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {VideoPreviewCoverComponent} from './video-preview-cover.component';

describe('VideoPreviewCoverComponent', () => {
  let component: VideoPreviewCoverComponent;
  let fixture: ComponentFixture<VideoPreviewCoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoPreviewCoverComponent ]
    }).overrideTemplate(VideoPreviewCoverComponent, '')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPreviewCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
