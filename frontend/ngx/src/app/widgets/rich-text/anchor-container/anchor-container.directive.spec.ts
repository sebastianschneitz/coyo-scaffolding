import {AnchorContainerDirective} from './anchor-container.directive';

describe('AnchorContainerDirective', () => {
  it('should catch clicks on anchors', () => {
    // given
    const windowMock = {
      location: {}
    };

    const setHashes: string[] = [];

    Object.defineProperty(windowMock.location, 'hash', {
      set(v: any): void {
        setHashes.push(v);
      }
    });

    const directive = new AnchorContainerDirective(windowMock as any);
    const event: MouseEvent = {
      target: {
        getAttribute: () => '',
        hash: '#anchor'
      }
    } as any;

    // when
    directive.onClick(event);

    // then
    expect(setHashes).toEqual(['', '#anchor']);
  });
});
