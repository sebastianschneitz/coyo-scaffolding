import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RichTextWidgetComponent} from './rich-text-widget.component';

describe('RichTextWidgetComponent', () => {
  let component: RichTextWidgetComponent;
  let fixture: ComponentFixture<RichTextWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RichTextWidgetComponent]
    }).overrideTemplate(RichTextWidgetComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RichTextWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      settings: {
        html_content: ''
      }
    } as any;
    fixture.detectChanges();
  });

  it('should update content', () => {
    // given
    const newContent = 'test';

    // when
    component.contentUpdated(newContent);

    // then this.widget.settings.html_content
    expect(component.widget.settings.html_content).toEqual(newContent);
  });
});
