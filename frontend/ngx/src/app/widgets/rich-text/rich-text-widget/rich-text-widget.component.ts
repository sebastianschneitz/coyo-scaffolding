import {ChangeDetectionStrategy, Component} from '@angular/core';
import {WidgetComponent} from '@widgets/api/widget-component';
import {RichTextWidget} from '@widgets/rich-text/rich-text-widget';

@Component({
  selector: 'coyo-rich-text-editor-widget',
  templateUrl: './rich-text-widget.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RichTextWidgetComponent extends WidgetComponent<RichTextWidget> {

  contentUpdated($event: string): void {
    this.widget.settings.html_content = $event;
  }
}
