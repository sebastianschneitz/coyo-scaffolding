import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BirthdayWidget} from '@widgets/birthday/birthday-widget';
import {BirthdayWidgetSettings} from '@widgets/birthday/birthday-widget-settings.model';

import {BirthdayWidgetSettingsComponent} from './birthday-widget-settings.component';

describe('BirthdayWidgetSettingsComponent', () => {
  let component: BirthdayWidgetSettingsComponent;
  let fixture: ComponentFixture<BirthdayWidgetSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BirthdayWidgetSettingsComponent],
      providers: [FormBuilder]
    }).overrideTemplate(BirthdayWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthdayWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.parentForm = new FormGroup({});
    component.widget = {settings: {}} as BirthdayWidget;
  });

  it('should init the form with default values', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    expect(component.parentForm.getRawValue()).toEqual({
      _daysBeforeBirthday: 10,
      _displayAge: true,
      _birthdayNumber: 5,
      _fetchBirthdays: 'ALL'
    });
  });

  it('should init the form with value of the settings', () => {
    // given
    const settings = {
      _daysBeforeBirthday: 32,
      _displayAge: false,
      _birthdayNumber: 2,
      _fetchBirthdays: 'FOLLOWERS'
    } as BirthdayWidgetSettings;
    component.widget.settings = settings;

    // when
    fixture.detectChanges();

    // then
    expect(component.parentForm.getRawValue()).toEqual(settings);
  });
});
