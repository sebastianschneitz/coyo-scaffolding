 import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {TimeProviderService} from '@core/time-provider/time-provider.service';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {BirthdayUser} from '@widgets/birthday/birthday-user';

import {BirthdayService} from './birthday.service';

describe('BirthdayService', () => {
  let service: BirthdayService;
  let timeProviderService: jasmine.SpyObj<TimeProviderService>;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BirthdayService, {
        provide: TimeProviderService, useValue: jasmine.createSpyObj('timeProviderService', ['getCurrentDate'])
      }]
    });

    service = TestBed.get(BirthdayService);
    timeProviderService = TestBed.get(TimeProviderService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get birthdays', () => {
    // given
    const daysInFuture = 5;
    const widgetId = 'id';
    const onlySubscribed = true;
    const pageable = new Pageable(0, 5);
    const page = {
      number: 0,
      content: [{id: 'id'}]
    } as Page<BirthdayUser>;

    // when
    const result = service.getBirthdays(daysInFuture, widgetId, onlySubscribed, pageable);

    // then
    let called = false;
    result.subscribe(resultPage => {
      called = true;
      expect(resultPage).toBe(page);
    });
    const request = httpMock
      .expectOne('/web/widgets/birthday/birthdays?_page=0&_pageSize=5&daysFuture=5&id=id&filters=subscribedTo');
    request.flush(page);
    expect(called).toBeTruthy();
  });

  it('should not add undefined id', () => {
    // given
    const daysInFuture = 5;
    const onlySubscribed = true;
    const pageable = new Pageable(0, 5);
    const page = {
      number: 0,
      content: [{id: 'id'}]
    } as Page<BirthdayUser>;

    // when
    const result = service.getBirthdays(daysInFuture, null, onlySubscribed, pageable);

    // then
    let called = false;
    result.subscribe(resultPage => {
      called = true;
      expect(resultPage).toBe(page);
    });
    const request = httpMock
      .expectOne('/web/widgets/birthday/birthdays?_page=0&_pageSize=5&daysFuture=5&filters=subscribedTo');
    request.flush(page);
    expect(called).toBeTruthy();
  });

  it('should not add subscribedTo filter when all colleagues are requested', () => {
    // given
    const daysInFuture = 5;
    const onlySubscribed = false;
    const widgetId = 'id';
    const pageable = new Pageable(0, 5);
    const page = {
      number: 0,
      content: [{id: 'id'}]
    } as Page<BirthdayUser>;

    // when
    const result = service.getBirthdays(daysInFuture, widgetId, onlySubscribed, pageable);

    // then
    let called = false;
    result.subscribe(resultPage => {
      called = true;
      expect(resultPage).toBe(page);
    });
    const request = httpMock
      .expectOne('/web/widgets/birthday/birthdays?_page=0&_pageSize=5&daysFuture=5&id=id');
    request.flush(page);
    expect(called).toBeTruthy();
  });

  it('should find year', () => {
    // given
    const date = '1999-01-01';

    // when
    const result = service.hasYear(date);

    // then
    expect(result).toBeTruthy();
  });

  it('should not find year', () => {
    // given
    const date = '01-01';

    // when
    const result = service.hasYear(date);

    // then
    expect(result).toBeFalsy();
  });
});
