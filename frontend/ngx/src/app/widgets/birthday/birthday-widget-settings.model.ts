import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export interface BirthdayWidgetSettings extends WidgetSettings {
  _titles: string[];
  _daysBeforeBirthday: number;
  _displayAge: boolean;
  _birthdayNumber: number;
  _fetchBirthdays: 'ALL' | 'FOLLOWERS';
}
