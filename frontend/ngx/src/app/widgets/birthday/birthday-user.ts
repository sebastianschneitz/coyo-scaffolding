import {Target} from '@domain/sender/target';

export interface BirthdayUser {
  id: string;
  slug: string;
  color: string;
  target: Target;
  displayName: string;
  birthday: string;
  displayNameInitial: string;
  externalWorkspaceMember: boolean;
  imageUrls: {[key: string]: string};
  index: number;
}
