import {async, TestBed} from '@angular/core/testing';
import {TimeProviderService} from '@core/time-provider/time-provider.service';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {NgxsModule, Store} from '@ngxs/store';
import {BirthdayUser} from '@widgets/birthday/birthday-user';
import {BirthdayWidgetSettings} from '@widgets/birthday/birthday-widget-settings.model';
import {Init, LoadMore} from '@widgets/birthday/birthday-widget/birthday-widget.actions';
import {BirthdayService, REFERENCE_LEAP_YEAR} from '@widgets/birthday/birthday/birthday.service';
import {of, Subject} from 'rxjs';
import {BirthdayWidgetState} from './birthday-widget.state';

describe('BirthdayWidget store', () => {
  let store: Store;
  let birthdayService: jasmine.SpyObj<BirthdayService>;
  let timeProviderService: jasmine.SpyObj<TimeProviderService>;
  let settings: BirthdayWidgetSettings;
  let users: BirthdayUser[];
  let currentDate: Date;
  let page: Page<BirthdayUser>;
  const id = 'ID';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([BirthdayWidgetState])],
      providers: [{
        provide: BirthdayService, useValue: jasmine.createSpyObj('birthdayService', ['getBirthdays', 'hasYear'])
      }, {
        provide: TimeProviderService, useValue: jasmine.createSpyObj('timeProviderService', ['getCurrentDate'])
      }]
    }).compileComponents();
    store = TestBed.get(Store);
    birthdayService = TestBed.get(BirthdayService);
    timeProviderService = TestBed.get(TimeProviderService);

    settings = {
      _birthdayNumber: 2,
      _fetchBirthdays: 'ALL',
      _daysBeforeBirthday: 10
    } as BirthdayWidgetSettings;

    users = [{
      id: 1,
      birthday: '2000-01-01'
    } as unknown as BirthdayUser, {
      id: 2,
      birthday: '2001-01-02'
    } as unknown as BirthdayUser];

    page = {number: 0, content: users} as Page<BirthdayUser>;

    currentDate = new Date('2019-01-01');

    birthdayService.getBirthdays.and.returnValue(of(page));

    birthdayService.hasYear.and.returnValue(true);

    timeProviderService.getCurrentDate.and.returnValue(currentDate);
  }));

  it('should init birthday widget', () => {
    // when
    store.dispatch(new Init(settings, id));

    // then
    checkInit();
  });

  it('should load more birthdays', () => {
    // given
    store.dispatch(new Init(settings, id));

    const moreUsers = [{
      id: 3,
      birthday: '2000-01-03'
    } as unknown as BirthdayUser, {
      id: 4,
      birthday: '2001-01-04'
    } as unknown as BirthdayUser];

    const nextPage = {content: moreUsers} as Page<BirthdayUser>;

    birthdayService.getBirthdays.and.returnValue(of(nextPage));

    // when
    store.dispatch(new LoadMore(settings, id));

    // then
    const actual = store.selectSnapshot((state => state.birthdayWidget));
    expect(actual[id].dates.length).toBe(4);
    expect(actual[id].dates[2]).toEqual({
      birthday: new Date(REFERENCE_LEAP_YEAR + '-01-03'),
      users: [{...moreUsers[0], index: 2}],
      today: false,
      minIndex: 2
    });
    expect(actual[id].dates[3]).toEqual({
      birthday: new Date(REFERENCE_LEAP_YEAR + '-01-04'),
      users: [{...moreUsers[1], index: 3}],
      today: false,
      minIndex: 3
    });
    expect(birthdayService.getBirthdays).toHaveBeenCalledWith(settings._daysBeforeBirthday,
      id, false, new Pageable(1, settings._birthdayNumber));
  });

  it('should init if there is no current state when load more', () => {
    // when
    store.dispatch(new LoadMore(settings, id));

    // then
    checkInit();
  });

  it('should group users with same birth date', () => {
    // given
    users = [{
      id: 1,
      birthday: '2000-01-01'
    } as unknown as BirthdayUser, {
      id: 2,
      birthday: '2001-01-01'
    } as unknown as BirthdayUser];

    page = {content: users} as Page<BirthdayUser>;

    birthdayService.getBirthdays.and.returnValue(of(page));

    // when
    store.dispatch(new Init(settings, id));

    // then
    const actual = store.selectSnapshot((state => state.birthdayWidget));
    expect(actual[id]).toBeDefined();
    expect(actual[id].dates.length).toBe(1);
    expect(actual[id].dates[0]).toEqual({
      birthday: new Date(REFERENCE_LEAP_YEAR + '-01-01'),
      users: [{...users[0], index: 0}, {...users[1], index: 1}],
      today: true,
      minIndex: 0
    });
  });

  it('should set loading flag while requesting', () => {
    // given
    const subject = new Subject<Page<BirthdayUser>>();
    birthdayService.getBirthdays.and.returnValue(subject);

    // when
    store.dispatch(new Init(settings, id));

    // then
    const actual = store.selectSnapshot((state => state.birthdayWidget));
    expect(actual[id]).toBeDefined();
    expect(actual[id].loading).toBeTruthy();
  });

  it('should work if users do not configure their birth year', () => {
    // given
    users[0].birthday = '01-01';
    users[1].birthday = '01-02';

    birthdayService.hasYear.and.returnValue(false);

    // when
    store.dispatch(new Init(settings, id));

    // then
    checkInit();
  });

  function checkInit(): void {
    const actual = store.selectSnapshot((state => state.birthdayWidget));
    expect(actual[id]).toBeDefined();
    expect(actual[id].loading).toBeFalsy();
    expect(actual[id].dates.length).toBe(2);
    expect(actual[id].dates[0]).toEqual({
      birthday: new Date(REFERENCE_LEAP_YEAR + '-01-01'),
      users: [{...users[0], index: 0}],
      today: true,
      minIndex: 0
    });
    expect(actual[id].dates[1]).toEqual({
      birthday: new Date(REFERENCE_LEAP_YEAR + '-01-02'),
      users: [{...users[1], index: 1}],
      today: false,
      minIndex: 1
    });
    expect(birthdayService.getBirthdays).toHaveBeenCalledWith(settings._daysBeforeBirthday,
      id, false, new Pageable(0, settings._birthdayNumber));
  }

});
