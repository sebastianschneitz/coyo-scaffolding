import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TimeProviderService} from '@core/time-provider/time-provider.service';
import {Store} from '@ngxs/store';
import {BirthdayWidgetSettings} from '@widgets/birthday/birthday-widget-settings.model';
import {Init, LoadMore} from '@widgets/birthday/birthday-widget/birthday-widget.actions';
import {BirthdayWidgetStateModel} from '@widgets/birthday/birthday-widget/birthday-widget.state';

import {BirthdayWidgetComponent} from './birthday-widget.component';

describe('BirthdayWidgetComponent', () => {
  let component: BirthdayWidgetComponent;
  let fixture: ComponentFixture<BirthdayWidgetComponent>;
  let store: jasmine.SpyObj<Store>;
  let timeProviderService: jasmine.SpyObj<TimeProviderService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BirthdayWidgetComponent],
      providers: [{
        provide: Store, useValue: jasmine.createSpyObj('store', ['dispatch', 'select'])
      }, {
        provide: TimeProviderService, useValue: jasmine.createSpyObj('timeProviderService', ['getCurrentDate'])
      }]
    }).overrideTemplate(BirthdayWidgetComponent, '')
      .compileComponents();

    store = TestBed.get(Store);
    timeProviderService = TestBed.get(TimeProviderService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthdayWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      tempId: 'temp-id',
      id: 'id',
      settings: {
        _birthdayNumber: 3
      } as BirthdayWidgetSettings
    };
    component.editMode = false;
    fixture.detectChanges();
  });

  it('should init', () => {
    expect(component).toBeTruthy();
    expect(store.select).toHaveBeenCalled();
    expect(store.select.calls.mostRecent().args[0]({birthdayWidget: {[component.widget.id]: 'test'}})).toBe('test');
    expect(store.dispatch)
      .toHaveBeenCalledWith(new Init(component.widget.settings, component.widget.id, component.editMode));
  });

  it('should reinit on changes', () => {
    // when
    component.ngOnChanges({});

    // then
    expect(store.dispatch)
      .toHaveBeenCalledWith(new Init(component.widget.settings, component.widget.id, component.editMode));
  });

  it('should load more', () => {
    // when
    component.loadMore();

    // then
    expect(store.dispatch)
      .toHaveBeenCalledWith(new LoadMore(component.widget.settings, component.widget.id, component.editMode));
  });

  it('should get current date', () => {
    // given
    const date = new Date();
    timeProviderService.getCurrentDate.and.returnValue(date);

    // when
    const result = component.getCurrentDate();

    // then
    expect(result).toBe(date);
  });

  it('should get number of skeletons', () => {
    // when
    const result = component.getNeededSkeletons({} as BirthdayWidgetStateModel);

    // then
    expect(result).toEqual([0, 1, 2]);
  });

  it('should get number of skeletons when one page is already loaded', () => {
    // when
    const result = component.getNeededSkeletons({page: {}} as BirthdayWidgetStateModel);

    // then
    expect(result).toEqual([0]);
  });
});
