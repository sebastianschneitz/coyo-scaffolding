import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {WelcomeWidget} from '@widgets/welcome/welcome-widget';
import {of} from 'rxjs';
import {WelcomeWidgetSettingsComponent} from './welcome-widget-settings.component';

describe('WelcomeWidgetSettingsComponent', () => {
  let component: WelcomeWidgetSettingsComponent;
  let fixture: ComponentFixture<WelcomeWidgetSettingsComponent>;
  let translateService: jasmine.SpyObj<TranslateService>;

  let parentForm: FormGroup;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WelcomeWidgetSettingsComponent],
      providers: [
        {provide: TranslateService, useValue: jasmine.createSpyObj('translateService', ['get'])}
      ]
    }).overrideTemplate(WelcomeWidgetSettingsComponent, '')
      .compileComponents();

    translateService = TestBed.get(TranslateService);
    translateService.get.and.returnValue(of('Welcome!'));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeWidgetSettingsComponent);
    component = fixture.componentInstance;
    parentForm = new FormGroup({});
    component.widget = {settings: {}} as WelcomeWidget;
    component.parentForm = parentForm;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.widget.settings.welcomeText).toEqual('Welcome!');
  });
});
