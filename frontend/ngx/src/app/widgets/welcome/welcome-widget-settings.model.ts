import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a welcome widget
 */
export interface WelcomeWidgetSettings extends WidgetSettings {
  welcomeText: string;
  _showCover: boolean;
}
