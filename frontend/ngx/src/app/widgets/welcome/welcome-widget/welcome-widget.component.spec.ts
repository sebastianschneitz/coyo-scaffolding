import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {UrlService} from '@core/http/url/url.service';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {User} from '@domain/user/user';
import {WelcomeWidget} from '@widgets/welcome/welcome-widget';
import {of} from 'rxjs';
import {WelcomeWidgetComponent} from './welcome-widget.component';

describe('WelcomeWidgetComponent', () => {
  let component: WelcomeWidgetComponent;
  let fixture: ComponentFixture<WelcomeWidgetComponent>;
  let urlService: jasmine.SpyObj<UrlService>;
  let authService: jasmine.SpyObj<AuthService>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  let widget: WelcomeWidget;

  const user = {
    id: 'user-id',
    imageUrls: {
      cover: '/web/senders/xxx-zzz/images/cover?modified=1'
    }
  } as User;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WelcomeWidgetComponent],
      providers: [{provide: UrlService, useValue: jasmine.createSpyObj('urlService', ['getBackendUrl'])},
        {provide: AuthService, useValue: jasmine.createSpyObj('authService', ['getUser'])},
        {provide: WindowSizeService, useValue: jasmine.createSpyObj('windowSizeService', ['isRetina'])}
      ]
    }).overrideTemplate(WelcomeWidgetComponent, '')
      .compileComponents();

    urlService = TestBed.get(UrlService);
    urlService.getBackendUrl.and.returnValue('http://localhost:8080');
    authService = TestBed.get(AuthService);
    authService.getUser.and.returnValue(of(user));
    windowSizeService = TestBed.get(WindowSizeService);
    windowSizeService.isRetina.and.returnValue(false);
  }));

  beforeEach(() => {
    widget = {settings: {welcomeText: 'Moin Moin', _showCover: false}} as WelcomeWidget;
    fixture = TestBed.createComponent(WelcomeWidgetComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    fixture.detectChanges();
  });

  it('should set Observables on init', () => {
    expect(component).toBeTruthy();
    component.currentUser$.subscribe(u => expect(u).toBe(user));
    component.showCover$.subscribe(show => expect(show).toBe(false));
  });

  it('should set showCover on change', () => {
    // given
    component.widget = {settings: {welcomeText: 'Moin Moin', _showCover: true}} as WelcomeWidget;
    // when
    component.ngOnChanges();
    fixture.detectChanges();
    // then
    component.showCover$.subscribe(show => expect(show).toBe(true));
    expect(component.coverStyle).toEqual('url(http://localhost:8080/web/senders/xxx-zzz/images/cover?modified=1&imageSize=XL)');
  });

  it('should set showCover on change with Retina screen', () => {
    // given
    windowSizeService.isRetina.and.returnValue(true);
    component.widget = {settings: {welcomeText: 'Moin Moin', _showCover: true}} as WelcomeWidget;
    // when
    component.ngOnChanges();
    fixture.detectChanges();
    // then
    component.showCover$.subscribe(show => expect(show).toBe(true));
    expect(component.coverStyle).toEqual('url(http://localhost:8080/web/senders/xxx-zzz/images/cover?modified=1&imageSize=XXL)');
  });
});
