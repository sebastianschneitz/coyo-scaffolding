/**
 * The profile information stored in the backend.
 */
export interface ProfileInfo {
  profileFields: boolean;
  avatar: boolean;
  cover: boolean;
  followingUser: boolean;
  pageMember: boolean;
  createdPost: boolean;
}
