import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The complete profile widget model.
 */
export interface CompleteProfileWidget extends Widget<WidgetSettings> {
}
