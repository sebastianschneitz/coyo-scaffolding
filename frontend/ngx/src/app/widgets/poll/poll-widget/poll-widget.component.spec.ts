import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngxs/store';
import {environment} from '@root/environments/environment';
import {PollWidget} from '@widgets/poll/poll-widget';
import {HydratePollWidget, RemoveVote, Vote} from '@widgets/poll/poll-widget.actions';
import {PollWidgetOptionsStateModel, PollWidgetStateModel} from '@widgets/poll/poll-widget.state';
import {VotersModalComponent} from '@widgets/poll/voters-modal/voters-modal.component';
import {BsModalService} from 'ngx-bootstrap';
import {of} from 'rxjs';
import {PollWidgetComponent} from './poll-widget.component';

describe('PollWidgetComponent', () => {
  let component: PollWidgetComponent;
  let fixture: ComponentFixture<PollWidgetComponent>;
  let store: jasmine.SpyObj<Store>;
  let modal: jasmine.SpyObj<BsModalService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PollWidgetComponent],
      providers: [
        {
          provide: Store,
          useValue: jasmine.createSpyObj('Store', ['select', 'dispatch'])
        }, {
          provide: BsModalService,
          useValue: jasmine.createSpyObj('modalService', ['show'])
        }
      ]
    })
      .overrideTemplate(PollWidgetComponent, '')
      .compileComponents();
    store = TestBed.get(Store);
    modal = TestBed.get(BsModalService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {}
    } as PollWidget;

    store.select.and.returnValue(of({}));
    modal.show.and.returnValue({
      content: jasmine.createSpyObj('votersModalComponent', ['initModal'])
    });
  });

  it('should init', () => {
    // given
    store.select.and.returnValue(of({}));
    component.widget.settings._options = [];
    component.widget.settings._showResults = true;

    // when
    fixture.detectChanges();

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new HydratePollWidget(component.widget.id, [], true));
  });

  it('should reload state on changes', () => {
    // given
    store.select.and.returnValue(of({}));
    component.widget.settings._options = [];
    component.widget.settings._showResults = true;

    // when
    fixture.detectChanges();
    component.ngOnChanges({});

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new HydratePollWidget(component.widget.id, [], true));
  });

  it('should toggle option on', () => {
    // given
    component.widget.settings._options = [];
    component.widget.settings._showResults = true;
    const option: PollWidgetOptionsStateModel = {
      id: '0',
      answer: 'Answer A',
      votes: 0
    };

    // when
    component.toggleOption(option);

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new Vote(component.widget.id, option));
  });

  it('should toggle option off', () => {
    // given
    component.widget.settings._options = [];
    component.widget.settings._showResults = true;
    const option: PollWidgetOptionsStateModel = {
      id: '0',
      answer: 'Answer A',
      votes: 0,
      answerId: 'answer-id'
    };

    // when
    component.toggleOption(option);

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new RemoveVote(component.widget.id, option.id, option.answerId));
  });

  it('should show voters modal', () => {
    // given
    component.widget.settings._options = [];
    component.widget.settings._showResults = true;
    const option: PollWidgetOptionsStateModel = {
      id: '0',
      answer: 'Answer A',
      votes: 0,
      answerId: 'answer-id'
    };

    // when
    component.showVoters(option);

    // then
    expect(modal.show).toHaveBeenCalledWith(VotersModalComponent, {
      animated: environment.enableAnimations,
      class: 'modal-md'
    });
  });

  it('should calculate percentage', done => {
    // given
    const option: PollWidgetOptionsStateModel = {
      id: '0',
      answer: 'Answer A',
      votes: 5,
      answerId: 'answer-id'
    };
    const widgetState: PollWidgetStateModel = {
      totalVotes: 10,
      options: [option]
    };
    store.select.and.returnValue(of(widgetState));
    fixture.detectChanges();

    // when
    component.getPercentage$(option).subscribe(percentage => {
      // then
      expect(percentage).toBe(50);
      done();
    });
  });

  it('should be able to vote', done => {
    // given
    const option: PollWidgetOptionsStateModel = {
      id: '0',
      answer: 'Answer A',
      votes: 5,
      answerId: 'answer-id'
    };
    const widgetState: PollWidgetStateModel = {
      totalVotes: 5,
      options: [option]
    };
    store.select.and.returnValue(of(widgetState));
    fixture.detectChanges();

    // when
    component.canVote$(option).subscribe(result => {
      // then
      expect(result).toBe(true);
      done();
    });
  });
});
