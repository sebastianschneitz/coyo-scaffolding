import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {Sender} from '@domain/sender/sender';
import {PollWidgetOptionVoteResponse, PollWidgetService} from './poll-widget.service';

describe('PollWidgetService', () => {
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PollWidgetService]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should get selected answers', inject([PollWidgetService], (service: PollWidgetService) => {
    // Given
    const widgetId = 'widget-id';
    const answer1 = {id: '234-23423', optionId: '1'};
    const answer2 = {id: '634-23523', optionId: '2'};
    service.getSelectedAnswers(widgetId)
      .subscribe(response => expect(response).toEqual([answer1, answer2]));

    // Then
    const request = httpTestingController.expectOne(
      req => req.method === 'GET'
        && req.url === `/web/widgets/poll/${widgetId}/selected-answers`
    );

    // Finally
    request.flush([answer1, answer2]);
    httpTestingController.verify();
  }));

  it('should select an answer', inject([PollWidgetService], (service: PollWidgetService) => {
    // Given
    const widgetId = 'widget-id';
    const optionId = 'option-id';
    const answerId = 'answer-id';
    const body = {
      id: answerId,
      optionId: optionId
    };
    service.selectAnswer(widgetId, optionId)
      .subscribe(response => expect(response).toEqual(body));

    // Then
    const request = httpTestingController.expectOne(
      req => req.method === 'POST'
        && req.url === `/web/widgets/poll/${widgetId}/selected-answers` && req.body.optionId === optionId
    );

    // Finally
    request.flush(body);
    httpTestingController.verify();
  }));

  it('should get votes', inject([PollWidgetService], (service: PollWidgetService) => {
    // Given
    const widgetId = 'widget-id';
    const vote1: PollWidgetOptionVoteResponse = {count: 0, optionId: '1'};
    const vote2: PollWidgetOptionVoteResponse = {count: 0, optionId: '2'};
    service.getVotes(widgetId)
      .subscribe(response => expect(response).toEqual([vote1, vote2]));

    // Then
    const request = httpTestingController.expectOne(
      req => req.method === 'GET'
        && req.url === `/web/widgets/poll/${widgetId}/votes`
    );

    // Finally
    request.flush([vote1, vote2]);
    httpTestingController.verify();
  }));

  it('should delete answer', inject([PollWidgetService], (service: PollWidgetService) => {
    // Given
    const widgetId = 'widget-id';
    const answerId = 'answer-id';
    service.deleteAnswer(widgetId, answerId)
      .subscribe(() => {});

    // Then
    const request = httpTestingController.expectOne(
      req => req.method === 'DELETE'
        && req.url === `/web/widgets/poll/${widgetId}/selected-answers/${answerId}`
    );

    // Finally
    request.flush({});
    httpTestingController.verify();
  }));

  it('should get voters', inject([PollWidgetService], (service: PollWidgetService) => {
    // Given
    const widgetId = 'widget-id';
    const optionId = 'option-id';
    const expectedResponse = {
      content: []
    } as Page<Sender>;
    service.getVoters(widgetId, optionId)
      .subscribe(response => expect(response).toEqual(expectedResponse));

    // Then
    const request = httpTestingController.expectOne(
      req => req.method === 'GET'
        && req.url === `/web/widgets/poll/${widgetId}/${optionId}/voters?_page=0`
    );

    // Finally
    request.flush(expectedResponse);
    httpTestingController.verify();
  }));
});
