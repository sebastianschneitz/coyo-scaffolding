import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import {Store} from '@ngxs/store';
import {environment} from '@root/environments/environment';
import {WidgetComponent} from '@widgets/api/widget-component';
import {PollWidget} from '@widgets/poll/poll-widget';
import {HydratePollWidget, RemoveVote, Vote} from '@widgets/poll/poll-widget.actions';
import {PollWidgetOptionsStateModel, PollWidgetStateModel} from '@widgets/poll/poll-widget.state';
import {VotersModalComponent} from '@widgets/poll/voters-modal/voters-modal.component';
import * as _ from 'lodash';
import {BsModalService} from 'ngx-bootstrap/modal';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'coyo-poll-widget',
  templateUrl: './poll-widget.component.html',
  styleUrls: ['./poll-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PollWidgetComponent extends WidgetComponent<PollWidget> implements OnInit, OnChanges {

  /**
   * Edit layout mode active
   */
  @Input() settingsMode: boolean = false;

  private widgetState$: Observable<PollWidgetStateModel>;
  options$: Observable<PollWidgetOptionsStateModel[]>;

  constructor(cd: ChangeDetectorRef, private store: Store, private modalService: BsModalService) {
    super(cd);
  }

  ngOnInit(): void {
    this.widgetState$ = this.store
      .select(state => state.pollWidget[this.widget.id])
      .pipe(filter(state => !!state));
    this.options$ = this.widgetState$
      .pipe(filter(widgetState => !!widgetState && !!widgetState.options), map(widgetState => widgetState.options));
    const showResults = this.settingsMode || this.widget.settings._showResults;
    this.store.dispatch(new HydratePollWidget(this.widget.id, this.widget.settings._options, showResults));
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
    const showResults = this.settingsMode || this.widget.settings._showResults;
    if (changes.widget && !changes.widget.firstChange || changes.editMode) {
      this.store.dispatch(new HydratePollWidget(this.widget.id, this.widget.settings._options, showResults));
    }
  }

  toggleOption(option: PollWidgetOptionsStateModel): void {
    if (option.answerId) {
      this.store.dispatch(new RemoveVote(this.widget.id, option.id, option.answerId));
    } else {
      this.store.dispatch(new Vote(this.widget.id, option));
    }
  }

  showVoters(option: PollWidgetOptionsStateModel): void {
    if (!this.widget.settings._anonymous && (this.widget.settings._showResults || this.settingsMode)) {
      const modal = this.modalService.show(VotersModalComponent, {
        animated: environment.enableAnimations,
        class: 'modal-md'
      });
      modal.content.initModal({
        widgetId: this.widget.id,
        optionId: option.id
      });
    }
  }

  canVote$({id}: PollWidgetOptionsStateModel): Observable<boolean> {
    return this.widgetState$.pipe(map(state => {
      const model = state.options.find(val => val.id === id);
      return !this.editMode && !this.settingsMode && !this.widget.settings._frozen && !model.optimistic
        && (!!model.answerId || (_.sum(state.options.map(option => option.answerId ? 1 : 0)) < this.widget.settings._maxAnswers));
    }));
  }

  getPercentage$(option: PollWidgetOptionsStateModel): Observable<number> {
    return this.widgetState$.pipe(map(state => (option.votes / Math.max(1, state.totalVotes)) * 100));
  }
}
