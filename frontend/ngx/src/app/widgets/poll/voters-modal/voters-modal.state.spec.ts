import {async, TestBed} from '@angular/core/testing';
import {Sender} from '@domain/sender/sender';
import {NgxsModule, Store} from '@ngxs/store';
import {PollWidgetService} from '@widgets/poll/poll-widget/poll-widget.service';
import {InitializeVoters, LoadMoreVoters} from '@widgets/poll/voters-modal/voters-modal.actions';
import {VotersModalState, VotersModalStateModel} from '@widgets/poll/voters-modal/voters-modal.state';
import {of} from 'rxjs';

describe('VotersModal state', () => {
  let store: Store;
  let service: jasmine.SpyObj<PollWidgetService>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([VotersModalState])],
      providers: [
        {
          provide: PollWidgetService,
          useValue: jasmine.createSpyObj('pollWidgetService', ['getVoters'])
        }
      ]
    }).compileComponents();
    store = TestBed.get(Store);
    service = TestBed.get(PollWidgetService);
  }));

  it('should create a default state', () => {
    const actual = store.selectSnapshot(VotersModalState.getState);
    const expected: VotersModalStateModel = {
      loading: true,
      voters: [] as Sender[],
      currentPage: 0,
      last: false
    };
    expect(actual).toEqual(expected);
  });

  it('should initialize voters', () => {
    // given
    const widgetId = 'widget-id';
    const optionId = 'option-id';
    const sender = {
      id: 'sender-id'
    } as Sender;
    service.getVoters.and.returnValue(of({
      content: [sender],
      last: true,
      number: 0
    }));

    // when
    store.dispatch(new InitializeVoters(widgetId, optionId));
    const actual = store.selectSnapshot(VotersModalState.getState);

    // then
    const expected: VotersModalStateModel = {
      loading: false,
      voters: [sender] as Sender[],
      currentPage: 0,
      last: true
    };
    expect(actual).toEqual(expected);
  });

  it('should load more voters', () => {
    // given
    const widgetId = 'widget-id';
    const optionId = 'option-id';
    const sender = {
      id: 'sender-id'
    } as Sender;

    const sender2 = {
      id: 'sender-id'
    } as Sender;
    service.getVoters.and.returnValue(of({
      content: [sender],
      last: false,
      number: 0
    }));
    store.dispatch(new InitializeVoters(widgetId, optionId));
    service.getVoters.and.returnValue(of({
      content: [sender2],
      last: true,
      number: 1
    }));

    // when
    store.dispatch(new LoadMoreVoters(widgetId, optionId));
    const actual = store.selectSnapshot(VotersModalState.getState);

    // then
    const expected: VotersModalStateModel = {
      loading: false,
      voters: [sender, sender2] as Sender[],
      currentPage: 1,
      last: true
    };
    expect(actual).toEqual(expected);
  });
});
