import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngxs/store';
import {LoadMoreVoters} from '@widgets/poll/voters-modal/voters-modal.actions';
import {VotersModalStateModel} from '@widgets/poll/voters-modal/voters-modal.state';
import {BsModalRef} from 'ngx-bootstrap';
import {BehaviorSubject} from 'rxjs';
import {VotersModalComponent} from './voters-modal.component';

describe('VotersModalComponent', () => {
  let component: VotersModalComponent;
  let fixture: ComponentFixture<VotersModalComponent>;
  let store: jasmine.SpyObj<Store>;
  const stateSubject = new BehaviorSubject<VotersModalStateModel>(null);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VotersModalComponent],
      providers: [
        {
          provide: Store,
          useValue: jasmine.createSpyObj('store', ['dispatch', 'select', 'selectSnapshot'])
        },
        {
          provide: BsModalRef,
          useValue: jasmine.createSpyObj('modalRef', [''])
        }
      ]
    })
      .overrideTemplate(VotersModalComponent, '')
      .compileComponents();

    store = TestBed.get(Store);
    store.select.and.returnValue(stateSubject);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should init', () => {
    // given
    stateSubject.next({
      voters: [],
      loading: true,
      currentPage: 0,
      last: true
    });

    // when
    component.initModal({
      widgetId: 'widget-id',
      optionId: 'option-id'
    });

    // then
    component.voters$.subscribe(value => expect(value).toEqual([]));
    component.loading$.subscribe(value => expect(value).toEqual(true));
    component.isEmpty$.subscribe(value => expect(value).toEqual(false));
    component.hasMore$.subscribe(value => expect(value).toEqual(false));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

  it('should load more', () => {
    // given
    const widgetId = 'widget-id';
    const optionId = 'option-id';

    // when
    component.initModal({
      widgetId: 'widget-id',
      optionId: 'option-id'
    });
    component.loadMore();

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new LoadMoreVoters(widgetId, optionId));
  });
});
