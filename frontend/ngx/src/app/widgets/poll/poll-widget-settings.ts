import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export interface PollWidgetSettings extends WidgetSettings {
  description: string;
  question: string;
  _options: PollWidgetSettingsOption[];
  _anonymous: boolean;
  _showResults: boolean;
  _frozen: boolean;
  _maxAnswers: number;
  _nextOptionId: number;
}

export interface PollWidgetSettingsOption {
  id: string;
  newAnswer: boolean;
  answer: string;
}
