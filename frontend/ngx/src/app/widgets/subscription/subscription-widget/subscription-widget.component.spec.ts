import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {RichPage} from '@domain/pagination/richPage';
import {Sender} from '@domain/sender/sender';
import {Target} from '@domain/sender/target';
import {TargetService} from '@domain/sender/target/target.service';
import {SubscriptionInfo} from '@domain/subscription/subscription-info';
import {SubscriptionService} from '@domain/subscription/subscription.service';
import {of} from 'rxjs';
import {SubscriptionWidgetComponent} from './subscription-widget.component';

describe('SubscriptionWidgetComponent', () => {
  let component: SubscriptionWidgetComponent;
  let fixture: ComponentFixture<SubscriptionWidgetComponent>;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;
  let targetService: jasmine.SpyObj<TargetService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubscriptionWidgetComponent],
      providers: [{
        provide: SubscriptionService,
        useValue: jasmine.createSpyObj('subscriptionService', ['getSubscribedSenders', 'setFavorite'])
      }, {
        provide: TargetService,
        useValue: jasmine.createSpyObj('targetService', ['getLinkTo'])
      }]
    }).overrideTemplate(SubscriptionWidgetComponent, '')
      .compileComponents();

    subscriptionService = TestBed.get(SubscriptionService);
    targetService = TestBed.get(TargetService);

    subscriptionService.getSubscribedSenders.and.returnValue(of({
      page: {
        content: [{id: 'ID'}] as Sender[],
        numberOfElements: 1,
        totalElements: 2
      } as Page<Sender>,
      data: {
        autoSubscribe: ['autoID'],
        favorite: ['favID']
      } as SubscriptionInfo
    } as RichPage<Sender, SubscriptionInfo>));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionWidgetComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize items', () => {
    // when
    fixture.detectChanges();

    // then
    expect(subscriptionService.getSubscribedSenders.calls.count()).toBe(2);

    expect(component.sections[0]).toBeDefined();
    expect(component.sections[0].type).toBe('page');
    expect(component.sections[0].state$.getValue()).toEqual({
      isLoading: false,
      autoSubscribe: ['autoID'],
      favorite: ['favID'],
      content: [{id: 'ID'}] as Sender[],
      last: false
    });

    expect(component.sections[1]).toBeDefined();
    expect(component.sections[1].type).toBe('workspace');
    expect(component.sections[1].state$.getValue()).toEqual({
      isLoading: false,
      autoSubscribe: ['autoID'],
      favorite: ['favID'],
      content: [{id: 'ID'}] as Sender[],
      last: false
    });
  });

  it('should load more items', () => {
    // given
    fixture.detectChanges();
    subscriptionService.getSubscribedSenders.and.returnValue(of({
      page: {
        content: [{id: 'ID2'}] as Sender[],
        numberOfElements: 1,
        totalElements: 2
      } as Page<Sender>,
      data: {
        autoSubscribe: ['autoID2'],
        favorite: ['favID2']
      } as SubscriptionInfo
    } as RichPage<Sender, SubscriptionInfo>));

    // when
    component.load(component.sections[0]);

    // then
    expect(component.sections[0].state$.getValue()).toEqual({
      isLoading: false,
      autoSubscribe: ['autoID', 'autoID2'],
      favorite: ['favID', 'favID2'],
      content: [{id: 'ID'}, {id: 'ID2'}] as Sender[],
      last: true
    });
  });

  it('check autoSubscribe state', () => {
    // given
    fixture.detectChanges();

    // when
    const result1 = component.isAutoSubscribe(component.sections[0], {id: 'ID'} as Sender);
    const result2 = component.isAutoSubscribe(component.sections[0], {id: 'autoID'} as Sender);

    // then
    expect(result1).toBe(false);
    expect(result2).toBe(true);
  });

  it('check favorite state', () => {
    // given
    fixture.detectChanges();

    // when
    const result1 = component.isFavorite(component.sections[0], {id: 'ID'} as Sender);
    const result2 = component.isFavorite(component.sections[0], {id: 'favID'} as Sender);

    // then
    expect(result1).toBe(false);
    expect(result2).toBe(true);
  });

  it('get icon', () => {
    // given
    fixture.detectChanges();

    // when
    const result1 = component.getIcon(component.sections[0], {id: 'ID'} as Sender);
    const result2 = component.getIcon(component.sections[0], {id: 'favID'} as Sender);
    const result3 = component.getIcon(component.sections[0], {id: 'autoID'} as Sender);

    // then
    expect(result1).toBe('page');
    expect(result2).toBe('star');
    expect(result3).toBe('pin');
  });

  it('get link', () => {
    // given
    const sender = {
      id: 'ID',
      target: {
        name: 'target',
        params: {param: 'param'}
      } as Target
    } as Sender;

    targetService.getLinkTo.and.returnValue('link');

    // when
    const result1 = component.getLink(sender);
    const result2 = component.getLink(sender);

    // then
    expect(result1).toBe('link');
    expect(result2).toBe('link');
    expect(targetService.getLinkTo).toHaveBeenCalledWith(sender.target);
    expect(targetService.getLinkTo.calls.count()).toBe(1);
  });

  it('set favorite flag', () => {
    // given
    fixture.detectChanges();
    const $event = jasmine.createSpyObj('$event', ['preventDefault', 'stopPropagation']);
    subscriptionService.setFavorite.and.returnValue(of(null));

    // when
    component.toggleFavorite(component.sections[0], {id: 'ID'} as Sender, $event);

    // then
    expect($event.preventDefault).toHaveBeenCalled();
    expect($event.stopPropagation).toHaveBeenCalled();
    expect(subscriptionService.setFavorite).toHaveBeenCalledWith('ID', true);
    expect(component.sections[0].state$.getValue().favorite).toEqual(['favID', 'ID']);
  });

  it('unset favorite flag', () => {
    // given
    fixture.detectChanges();
    const $event = jasmine.createSpyObj('$event', ['preventDefault', 'stopPropagation']);
    subscriptionService.setFavorite.and.returnValue(of(null));

    // when
    component.toggleFavorite(component.sections[0], {id: 'favID'} as Sender, $event);

    // then
    expect($event.preventDefault).toHaveBeenCalled();
    expect($event.stopPropagation).toHaveBeenCalled();
    expect(subscriptionService.setFavorite).toHaveBeenCalledWith('favID', false);
    expect(component.sections[0].state$.getValue().favorite).toEqual([]);
  });
});
