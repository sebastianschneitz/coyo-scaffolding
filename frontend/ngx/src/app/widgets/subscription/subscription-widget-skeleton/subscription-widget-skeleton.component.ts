import {ChangeDetectionStrategy, Component} from '@angular/core';

/**
 * Renders the skeleton for the subscription widget.
 */
@Component({
  selector: 'coyo-subscription-widget-skeleton',
  template: '<div></div>',
  styleUrls: ['./subscription-widget-skeleton.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubscriptionWidgetSkeletonComponent {
}
