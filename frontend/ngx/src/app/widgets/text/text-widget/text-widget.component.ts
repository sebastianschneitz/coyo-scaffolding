import {Component, ElementRef, ViewChild} from '@angular/core';
import {WidgetComponent} from '@widgets/api/widget-component';
import {TextWidget} from '@widgets/text/text-widget';

/**
 * This widget displays an input for plain text.
 */
@Component({
  selector: 'coyo-text-widget',
  templateUrl: './text-widget.component.html',
  styleUrls: ['./text-widget.component.scss']
})
export class TextWidgetComponent extends WidgetComponent<TextWidget> {

  @ViewChild('editContainer', {
    static: true
  }) editContainer: ElementRef<HTMLDivElement>;

  showInput: boolean = true;

  /**
   * Hides the input on blur.
   */
  onBlur(): void {
    this.showInput = false;
  }

  /**
   * Shows the input on click.
   */
  onClick(): void {
    this.showInput = true;
    setTimeout(() => (this.editContainer.nativeElement.firstElementChild as HTMLTextAreaElement).focus());
  }
}
