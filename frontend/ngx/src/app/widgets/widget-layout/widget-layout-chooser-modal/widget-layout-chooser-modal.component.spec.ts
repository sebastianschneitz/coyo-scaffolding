import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {LayoutType} from '@widgets/widget-layout/widget-layout-chooser-modal/layout-type';

import {WidgetLayoutChooserModalComponent} from './widget-layout-chooser-modal.component';

describe('WidgetLayoutChooserModalComponent', () => {
  let component: WidgetLayoutChooserModalComponent;
  let fixture: ComponentFixture<WidgetLayoutChooserModalComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<WidgetLayoutChooserModalComponent>>;
  let translateService: jasmine.SpyObj<TranslateService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WidgetLayoutChooserModalComponent],
      providers: [{
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('dialogRef', ['close'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }]
    }).overrideTemplate(WidgetLayoutChooserModalComponent, '')
      .compileComponents();

    dialogRef = TestBed.get(MatDialogRef);
    translateService = TestBed.get(TranslateService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetLayoutChooserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should add a new layout', () => {
    // given
    const slot: LayoutType = {slots: [5, 5]};

    // when
    component.save(slot);

    // then
    expect(dialogRef.close).toHaveBeenCalledWith(slot);
  });

  it('should create aria label', () => {
    // given
    const msg = 'Slots Layout';
    translateService.instant.and.returnValue(msg);

    // when
    const result = component.getLayoutAriaLabel(0);

    // then
    expect(result).toBe('3,6,3 Slots Layout');
    expect(translateService.instant).toHaveBeenCalledWith('WIDGETS.LAYOUT.ROW.LAYOUT.INFO');

  });
});
