import {Injectable, NgZone} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatDialogSize} from '@coyo/ui';
import {WidgetLayoutChooserModalComponent} from '@widgets/widget-layout/widget-layout-chooser-modal/widget-layout-chooser-modal.component';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WidgetLayoutChooserModalService {

  constructor(private dialog: MatDialog, private ngZone: NgZone) {
  }

  /**
   * Temporary method which opens the modal called out of AngularJS component until this component is also mirgrated to
   * Angular.
   * @return Observable which notifies when the modal is closed
   */
  openModal(): Observable<any> {
    return this.ngZone.run(() => this.dialog.open<WidgetLayoutChooserModalComponent>(WidgetLayoutChooserModalComponent, {
          width: MatDialogSize.Medium
      })
    ).afterClosed();
  }
}
