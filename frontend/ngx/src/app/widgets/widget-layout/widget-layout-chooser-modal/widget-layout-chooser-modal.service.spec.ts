import {TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {MatDialogSize} from '@coyo/ui';
import {WidgetLayoutChooserModalComponent} from '@widgets/widget-layout/widget-layout-chooser-modal/widget-layout-chooser-modal.component';
import {of} from 'rxjs';

import {WidgetLayoutChooserModalService} from './widget-layout-chooser-modal.service';

describe('WidgetLayoutChooserModalService', () => {
  let service: WidgetLayoutChooserModalService;
  let dialog: jasmine.SpyObj<MatDialog>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WidgetLayoutChooserModalService, {
        provide: MatDialog,
        useValue: jasmine.createSpyObj('MatDialog', ['open'])
      }]
    });

    dialog = TestBed.get(MatDialog);
    service = TestBed.get(WidgetLayoutChooserModalService);
  });

  it('should open the layout configuration modal', () => {
    // given
    const dialogRef = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    dialog.open.and.returnValue(dialogRef);
    dialogRef.afterClosed.and.returnValue(of({}));
    // when
    service.openModal().subscribe();
    // then
    expect(dialog.open).toHaveBeenCalledWith(WidgetLayoutChooserModalComponent, {
      width: MatDialogSize.Medium
    });
    expect(dialogRef.afterClosed).toHaveBeenCalled();
  });
});
