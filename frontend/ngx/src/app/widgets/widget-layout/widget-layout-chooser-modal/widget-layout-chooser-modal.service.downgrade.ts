import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetLayoutChooserModalService} from '@widgets/widget-layout/widget-layout-chooser-modal/widget-layout-chooser-modal.service';

getAngularJSGlobal()
  .module('coyo.widgets.api')
  .factory('ngxWidgetsLayoutChooserModalService', downgradeInjectable(WidgetLayoutChooserModalService));
