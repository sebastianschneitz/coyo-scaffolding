import {Widget} from '@domain/widget/widget';
import {VideoWidgetSettings} from '@widgets/video/video-widget-settings.model';

/**
 * The interface of a video widget
 */
export interface VideoWidget extends Widget<VideoWidgetSettings> {
}
