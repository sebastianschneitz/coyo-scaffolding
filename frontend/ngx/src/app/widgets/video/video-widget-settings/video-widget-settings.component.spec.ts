import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {WebPreviewService} from '@domain/preview/web-preview/web-preview.service';
import {of, throwError} from 'rxjs';
import {VideoWidget} from '../video-widget';
import {VideoWidgetSettingsComponent} from './video-widget-settings.component';

describe('VideoWidgetSettingsComponent', () => {
  let component: VideoWidgetSettingsComponent;
  let fixture: ComponentFixture<VideoWidgetSettingsComponent>;
  let webPreviewService: jasmine.SpyObj<WebPreviewService>;

  let parentForm: FormGroup;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoWidgetSettingsComponent ],
      providers: [
        {provide: WebPreviewService, useValue: jasmine.createSpyObj('webPreviewService', ['generateWebPreview'])}
      ]
    }).overrideTemplate(VideoWidgetSettingsComponent, '')
      .compileComponents();

    webPreviewService = TestBed.get(WebPreviewService);
    webPreviewService.generateWebPreview.and.returnValue(of({
      height: 270,
      id: null,
      position: 0,
      type: 'VIDEO',
      videoUrl: 'https://www.youtube.com/embed/Bh4x2jCWsB4?feature=oembed',
      width: 480
    }));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoWidgetSettingsComponent);
    component = fixture.componentInstance;
    parentForm = new FormGroup({});
    component.widget = {settings: {
      description: 'Coyo video',
      title: 'Coyo',
      _url: 'https://www.youtube.com/watch?v=Bh4x2jCWsB4'
    }} as VideoWidget;
    component.parentForm = parentForm;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call OnBeforeSave and append the backendData to the existing settings', () => {
    // given
    // when
    const result = component.onBeforeSave(component.widget.settings);

    // then
    expect(webPreviewService.generateWebPreview).toHaveBeenCalledWith(component.widget.settings._url);
    result.subscribe(res => {
      expect(res._backendData.videoUrl).toBe('https://www.youtube.com/embed/Bh4x2jCWsB4?feature=oembed');
      expect(res._backendData.ratio).toBe(56.25);
    });
  });

  it('should explicitly set backendData to null (missing videoUrl)', () => {
    // given
    webPreviewService.generateWebPreview.and.returnValue(of({
      height: 270,
      id: null,
      position: 0,
      type: 'VIDEO',
      width: 480
    }));

    // when
    const result = component.onBeforeSave(component.widget.settings);

    // then
    result.subscribe(res => {
      expect(res._backendData).toBeNull();
    });
  });

  it('should explicitly set backendData to null (Observable Error)', () => {
    // given
    webPreviewService.generateWebPreview.and.returnValue(throwError({status: 404}));

    // when
    const result = component.onBeforeSave(component.widget.settings);

    // then
    result.subscribe(res => {
      expect(res._backendData).toBeNull();
    });
  });
});
