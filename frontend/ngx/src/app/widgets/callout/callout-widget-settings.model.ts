import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a callout widget
 */
export interface CalloutWidgetSettings extends WidgetSettings {
  md_text: string;
  _callout: {
    alertClass: string;
  };
}
