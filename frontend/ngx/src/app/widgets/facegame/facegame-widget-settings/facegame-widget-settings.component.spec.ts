import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SenderService} from '@domain/sender/sender/sender.service';
import {SettingsService} from '@domain/settings/settings.service';
import {Subject} from 'rxjs';
import {FacegameWidgetSettingsComponent} from './facegame-widget-settings.component';

describe('FacegameWidgetSettingsComponent', () => {
  let component: FacegameWidgetSettingsComponent;
  let fixture: ComponentFixture<FacegameWidgetSettingsComponent>;
  let settingsService: jasmine.SpyObj<SettingsService>;
  let senderService: jasmine.SpyObj<SenderService>;
  let widget: any;
  let parentForm: FormGroup;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FacegameWidgetSettingsComponent],
      providers: [FormBuilder,
        {provide: SettingsService},
        {
          provide: SenderService,
          useValue: jasmine.createSpyObj('SenderService', ['get'])
        }
      ]
    }).overrideTemplate(FacegameWidgetSettingsComponent, '')
      .compileComponents();

    settingsService = TestBed.get(SettingsService);
    senderService = TestBed.get(SenderService);
  }));

  beforeEach(() => {
    widget = {settings: {}};
    parentForm = new FormGroup({});
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FacegameWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    component.parentForm = parentForm;
    component.onSubmit = new Subject<any>();
    fixture.detectChanges();
  });

  it('should set validators on init', () => {
    // given
    spyOn(component.parentForm, 'addControl');

    // when
    component.ngOnInit();

    // then
    expect(component.parentForm.addControl).toHaveBeenCalledTimes(4);
  });

  it('should invalidate time below minimum', () => {
    // given
    component.ngOnInit();

    // when
    component.parentForm.patchValue({
      _senderId: 'sender-id',
      _time: 10,
      _includeExternal: true
    });

    // then
    expect(component.parentForm.valid).toBeFalsy();
    expect(component.parentForm.get('_senderId').value).toEqual('sender-id');
    expect(component.parentForm.get('_time').value).toEqual(10);
    expect(component.parentForm.get('_includeExternal').value).toBeTruthy();
  });

  it('should invalidate time above maximum', () => {
    // given
    component.ngOnInit();

    // when
    component.parentForm.patchValue({
      _senderId: 'sender-id',
      _time: 100,
      _includeExternal: true
    });

    // then
    expect(component.parentForm.valid).toBeFalsy();
    expect(component.parentForm.get('_senderId').value).toEqual('sender-id');
    expect(component.parentForm.get('_time').value).toEqual(100);
    expect(component.parentForm.get('_includeExternal').value).toBeTruthy();
  });

  it('should validate settings', () => {
    // given
    component.ngOnInit();

    // when
    component.parentForm.patchValue({
      _senderId: 'sender-id',
      _time: 30,
      _includeExternal: true
    });

    // then
    expect(component.parentForm.valid).toBeTruthy();
    expect(component.parentForm.get('_senderId').value).toEqual('sender-id');
    expect(component.parentForm.get('_time').value).toEqual(30);
    expect(component.parentForm.get('_includeExternal').value).toBeTruthy();
  });
});
