import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {User} from '@domain/user/user';
import {FacegameUser} from '@widgets/facegame/facegame-widget';
import {FacegameService} from '@widgets/facegame/facegame.service';
import {BehaviorSubject, of} from 'rxjs';
import {FacegameWidgetComponent} from './facegame-widget.component';

describe('FacegameAvatarTeaserComponent', () => {
  let component: FacegameWidgetComponent;
  let fixture: ComponentFixture<FacegameWidgetComponent>;
  let facegameService: jasmine.SpyObj<FacegameService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FacegameWidgetComponent],
      providers: [
        {
          provide: FacegameService,
          useValue: jasmine.createSpyObj('FacegameService', ['getAvatarTeasers', 'getRanking', 'startGame', 'getImageUrl', 'answer'])
        }
      ]
    }).overrideTemplate(FacegameWidgetComponent, '')
      .compileComponents();

    facegameService = TestBed.get(FacegameService);
    const user = {
      displayName: 'displayName'
    } as User;
    const userIds = new Set();
    userIds.add('a-a-a-a');
    userIds.add('a-b-b-b');
    userIds.add('a-a-c-c');
    facegameService.getAvatarTeasers.and.returnValue(of(userIds));
    facegameService.getRanking.and.returnValue(of([{user: user, points: 100}]));
    facegameService.startGame.and.returnValue(of({answers: [{displayName: 'a'} as FacegameUser,
        {displayName: 'b'} as FacegameUser, {displayName: 'c'} as FacegameUser, {displayName: 'd'} as FacegameUser]}));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacegameWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      tempId: 'temp-id',
      id: 'widget-id',
      settings: {
        _senderId: null,
        _time: 60,
        _includeExternal: false
      }
    };
    component.state$ = new BehaviorSubject({
      isRunning: false,
      isLoading: false,
      isNoMoreColleagues: false,
      game: FacegameWidgetComponent.INITIAL_GAME_STATE,
      avatarTeaserUserIds: new Set(),
      ranking: []
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get init game and get avatars and ranking on game init', () => {
    // given

    // when
    component.ngOnInit();

    // then
    expect(facegameService.getAvatarTeasers).toHaveBeenCalled();
    expect(facegameService.getRanking).toHaveBeenCalled();
  });

  it('should start game', () => {
    // given

    // when
    component.startGame();

    // then
    expect(component.state$.getValue().isRunning).toBeTruthy();
    expect(component.state$.getValue().isLoading).toBeFalsy();
    expect(component.state$.getValue().isNoMoreColleagues).toBeFalsy();
  });

  it('should get more answers after answering a question', () => {
    // given

    // when
    facegameService.answer.and.returnValue(of({
      points: 50, multiplier: 1, answers: [{displayName: 'e'}, {displayName: 'f'}, {displayName: 'g'},
        {displayName: 'h'}], lastAnswerCorrect: true
    }));
    component.answer(0);

    // then
    expect(facegameService.answer).toHaveBeenCalled();
    expect(component.state$.getValue().game.points).toEqual(50);
    expect(component.state$.getValue().game.multiplier).toEqual(1);
    expect(component.state$.getValue().game.lastAnswerCorrect).toBeTruthy();
  });

  it('should end game after time is up', () => {
    // given
    const game = FacegameWidgetComponent.INITIAL_GAME_STATE;
    game.endDate = new Date().getTime() - 1;
    game.totalMilliSeconds = 60000;
    component.state$ = new BehaviorSubject({
      isRunning: false,
      isLoading: false,
      isNoMoreColleagues: false,
      game: game,
      avatarTeaserUserIds: new Set(),
      ranking: []
    });

    // when
    component.checkGame();

    // then
    expect(component.state$.getValue().game.percentage).toEqual(0);
    expect(facegameService.getRanking).toHaveBeenCalled();
  });

  it('should end game with no possible answers left', fakeAsync(() => {
    // given
    const game = FacegameWidgetComponent.INITIAL_GAME_STATE;
    game.currentAnswer = undefined;
    game.answers = [{displayName: 'i'} as FacegameUser, {displayName: 'j'} as FacegameUser,
      {displayName: 'k'} as FacegameUser, {displayName: 'l'} as FacegameUser];
    component.state$ = new BehaviorSubject({
      isRunning: false,
      isLoading: false,
      isNoMoreColleagues: false,
      game: game,
      avatarTeaserUserIds: new Set(),
      ranking: []
    });

    // when
    facegameService.answer.and.returnValue(of({answers: []}));
    component.answer(0);
    tick(750);

    // then
    expect(component.state$.getValue().isRunning).toBeFalsy();
    expect(component.state$.getValue().isLoading).toBeFalsy();
    expect(component.state$.getValue().isNoMoreColleagues).toBeTruthy();
    expect(facegameService.getAvatarTeasers).toHaveBeenCalled();
    expect(facegameService.getRanking).toHaveBeenCalled();
  }));
});
