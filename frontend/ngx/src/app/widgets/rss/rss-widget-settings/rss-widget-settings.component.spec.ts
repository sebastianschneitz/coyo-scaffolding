import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {RssWidget} from '@widgets/rss/rss-widget';
import {RssWidgetSettingsComponent} from '@widgets/rss/rss-widget-settings/rss-widget-settings.component';
import {RssWidgetVerifyUrlResponse} from '@widgets/rss/rss-widget-verify-url-response.model';
import {RssWidgetService} from '@widgets/rss/rss-widget.service';
import {of} from 'rxjs';

describe('RssWidgetSettingsComponent', () => {
  let component: RssWidgetSettingsComponent;
  let fixture: ComponentFixture<RssWidgetSettingsComponent>;
  let rssWidgetService: jasmine.SpyObj<RssWidgetService>;

  let widget: RssWidget;
  let parentForm: FormGroup;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RssWidgetSettingsComponent],
      providers: [FormBuilder, {
        provide: RssWidgetService,
        useValue: jasmine.createSpyObj('rssWidgetService', ['verifyUrl'])
      }]
    }).overrideTemplate(RssWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    parentForm = new FormGroup({});
    widget = {
      id: 'widget-id',
      settings: {
        rssUrl: 'https://some.site/rss/feed',
        userName: 'John Doe',
        _encrypted_password: 'secret',
        maxCount: 0,
        displayImage: false
      }
    } as RssWidget;

    fixture = TestBed.createComponent(RssWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    component.parentForm = parentForm;
    rssWidgetService = TestBed.get(RssWidgetService);
    fixture.detectChanges();
  });

  it('should set default settings values', () => {
    // then
    expect(component.parentForm.valid).toBeTruthy();
    expect(component.parentForm.value.displayImage).toBeTruthy();
    expect(component.parentForm.value.maxCount).toEqual(5);
  });

  it('should verify Url', () => {
    // given
    rssWidgetService.verifyUrl.and.returnValue(of({valid: true} as RssWidgetVerifyUrlResponse));
    component.parentForm.markAsDirty();
    component.parentForm.get('rssUrl').setValue('https://some.site/rss/other_feed');
    fixture.detectChanges();

    // when
    component.verifyUrl();

    // then
    expect(rssWidgetService.verifyUrl).toHaveBeenCalledWith('https://some.site/rss/other_feed', 'John Doe', 'secret');
    expect(component.isLoading).toBeFalsy();
    expect(component.parentForm.valid).toBeTruthy();
  });

  it('should not verify Url', () => {
    // given
    rssWidgetService.verifyUrl.and.returnValue(
      of({valid: false, validationMessage: 'invalidUrl'} as RssWidgetVerifyUrlResponse));
    component.parentForm.markAsDirty();
    component.parentForm.get('rssUrl').setValue('https://some.site/rss/non_valid_url');
    fixture.detectChanges();

    // when
    component.verifyUrl();

    // then
    expect(rssWidgetService.verifyUrl).toHaveBeenCalledWith('https://some.site/rss/non_valid_url', 'John Doe', 'secret');
    expect(component.isLoading).toBeFalsy();
    expect(component.parentForm.valid).toBeFalsy();
    expect(component.parentForm.get('rssUrl').errors.invalidUrl).toBeTruthy();
  });
});
