/**
 * The entity model for a RSS feed entry
 */
// backend: RssEntryResponseBody
export interface RssEntry {
  title: string;
  description: string; // may be empty but never null/undefined
  publishedDate?: Date;
  feedUri?: string;
  imageUrl?: string;
}
