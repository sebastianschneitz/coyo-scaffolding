import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetLayoutSaveBarComponent} from './widget-layout-save-bar.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoWidgetLayoutSaveBar', downgradeComponent({
    component: WidgetLayoutSaveBarComponent
  }));
