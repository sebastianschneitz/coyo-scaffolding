import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {LocalStorageService} from '@core/storage/local-storage/local-storage.service';
import {Ng1WidgetLayoutService} from '@root/typings';
import {NG1_ROOTSCOPE, NG1_WIDGET_LAYOUT_SERVICE} from '@upgrade/upgrade.module';
import {IRootScopeService} from 'angular';
import {BehaviorSubject} from 'rxjs';

/**
 * The WidgetLayoutBarComponent displays a bar at the bottom of the screen when the global
 * edit mode is enabled. The bar contains buttons to save or discard changes to the layout as well
 * as a help-button.
 */
@Component({
  selector: 'coyo-widget-layout-save-bar',
  templateUrl: './widget-layout-save-bar.component.html',
  styleUrls: ['./widget-layout-save-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WidgetLayoutSaveBarComponent implements OnInit {
  globalEditMode: BehaviorSubject<boolean>;

  constructor(
    private localStorageService: LocalStorageService,
    @Inject(NG1_WIDGET_LAYOUT_SERVICE) private widgetLayoutService: Ng1WidgetLayoutService,
    @Inject(NG1_ROOTSCOPE) private rootScopeService: IRootScopeService) {
    this.globalEditMode = new BehaviorSubject<boolean>(this.rootScopeService['globalEditMode']);
  }

  /**
   * Returns the value of messagingSidebar.compact in the local storage
   */
  get compact(): boolean {
    return this.localStorageService.getValue('messagingSidebar.compact', false);
  }

  ngOnInit(): void {
    this.rootScopeService.$watch('globalEditMode', (v: boolean) => this.globalEditMode.next(v));
  }

  /**
   * Saves the current state of the global widget layout
   */
  save(): void {
    this.widgetLayoutService.save(this.rootScopeService, true);
  }

  /**
   * Discards any changes made to the global widget layout
   */
  cancel(): void {
    this.widgetLayoutService.cancel(this.rootScopeService, true);
  }
}
