import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a code widget.
 */
export interface CodeWidgetSettings extends WidgetSettings {
  _html_content: string;
  _js_content: string;
  _css_content: string;
}
