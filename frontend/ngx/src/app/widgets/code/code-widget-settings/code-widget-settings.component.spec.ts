import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {CodeWidgetSettingsComponent} from './code-widget-settings.component';

describe('CodeWidgetSettingsComponent', () => {
  let component: CodeWidgetSettingsComponent;
  let fixture: ComponentFixture<CodeWidgetSettingsComponent>;

  let widget: any;
  let parentForm: FormGroup;
  const onSubmit: Subject<any> = new Subject<any>();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CodeWidgetSettingsComponent]
    }).overrideTemplate(CodeWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    widget = {settings: {}};
  });

  beforeEach(() => {
    parentForm = new FormGroup({});
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    component.parentForm = parentForm;
    component.onSubmit = onSubmit;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
