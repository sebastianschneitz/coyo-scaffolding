import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {User} from '@domain/user/user';
import {InterestingColleaguesService} from '@widgets/interesting-colleagues/interesting-colleagues.service';

describe('InterestingColleaguesService', () => {
  let service: InterestingColleaguesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [InterestingColleaguesService]
    });

    service = TestBed.get(InterestingColleaguesService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should get interesting colleagues', () => {
    // given
    const page = {content: [{id: '3'}, {id: '4'}, {id: '5'}], last: true} as Page<User>;

    // when
    const result = service.getInterestingColleaguesList();

    let called = false;
    result.subscribe(res => {
      called = true;
      expect(res).toBe(page);
    });

    // then
    const request = httpMock
      .expectOne('/web/widgets/interestingcolleagues');
    request.flush(page);
    expect(called).toBeTruthy();
  });
});
