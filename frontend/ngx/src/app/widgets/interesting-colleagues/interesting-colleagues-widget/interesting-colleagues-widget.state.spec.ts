import {async, TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {InterestingColleaguesWidgetState} from '@widgets/interesting-colleagues/interesting-colleagues-widget/interesting-colleagues-widget.state';
import {
  Load,
  Reset
} from '@widgets/interesting-colleagues/interesting-colleagues-widget/interesting-colleagues.actions';
import {InterestingColleaguesService} from '@widgets/interesting-colleagues/interesting-colleagues.service';
import {of} from 'rxjs';

describe('InterestingColleaguesWidgetState', () => {
  let store: Store;
  let colleaguesService: jasmine.SpyObj<InterestingColleaguesService>;
  let users: any;
  const ID = 'id';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([InterestingColleaguesWidgetState])],
      providers: [{
        provide: InterestingColleaguesService,
        useValue: jasmine.createSpyObj('colleaguesService', ['getInterestingColleaguesList'])
      }]
    });

    store = TestBed.get(Store);
    colleaguesService = TestBed.get(InterestingColleaguesService);

    users = {content: [{id: 1}, {id: 2}], pages: 1, last: true};

    colleaguesService.getInterestingColleaguesList.and.returnValue(of(users));
  }));

  it('should load interesting colleagues', () => {
    // when
    store.dispatch(new Load(ID));
    // then
    const actual = store.selectSnapshot((state => state.interestingColleagues));
    expect(actual[ID]).toBeDefined();
    expect(actual[ID].users).toBeDefined();
    expect(actual[ID].users.length).toBe(2);
    expect(actual[ID].loading).toBeFalsy();
  });

  it('should reset the state', () => {
    // given
    store.dispatch(new Reset(ID));

    // then
    const actual = store.selectSnapshot((state => state.interestingColleagues));
    expect(actual[ID]).toBeUndefined();
  });
});
