import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {User} from '@domain/user/user';
import {Store} from '@ngxs/store';
import {WidgetVisibilityService} from '@widgets/api/widget-visibility/widget-visibility.service';
import {
  Load,
  Reset
} from '@widgets/interesting-colleagues/interesting-colleagues-widget/interesting-colleagues.actions';
import {InterestingColleaguesService} from '@widgets/interesting-colleagues/interesting-colleagues.service';
import * as _ from 'lodash';
import {of} from 'rxjs';
import {InterestingColleaguesWidgetComponent} from './interesting-colleagues-widget.component';

describe('InterestingColleaguesWidgetComponent', () => {
  let component: InterestingColleaguesWidgetComponent;
  let fixture: ComponentFixture<InterestingColleaguesWidgetComponent>;
  let widgetVisibilityService: jasmine.SpyObj<WidgetVisibilityService>;
  let store: jasmine.SpyObj<Store>;
  let colleaguesService: jasmine.SpyObj<InterestingColleaguesService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterestingColleaguesWidgetComponent],
      providers: [{
        provide: WidgetVisibilityService,
        useValue: jasmine.createSpyObj('widgetVisibilityService', ['setHidden'])
      }, {
        provide: Store,
        useValue: jasmine.createSpyObj('store', ['dispatch', 'select'])
      }, {
        provide: InterestingColleaguesService,
        useValue: jasmine.createSpyObj('colleaguesService', ['getInterestingColleaguesList'])
      }]
    }).overrideTemplate(InterestingColleaguesWidgetComponent, '')
      .compileComponents();

    colleaguesService = TestBed.get(InterestingColleaguesService);
    widgetVisibilityService = TestBed.get(WidgetVisibilityService);
    store = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestingColleaguesWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'id',
      tempId: 'tempId',
      settings: {
        _titles: ['title']
      }
    };
  });

  it('should init the widget with available colleagues', () => {
    // given
    const body = {loading: false, users: [{loginName: 'Dummy'}, {loginName: 'Freddy'}]};

    const state = {interestingColleagues: {id: body}};

    store.select.and.returnValue(of(body));

    // when
    fixture.detectChanges();
    component.state$.subscribe();

    // then
    expect(store.select).toHaveBeenCalled();
    expect(store.select.calls.mostRecent().args[0](state)).toBe(state.interestingColleagues.id);
    expect(store.dispatch).toHaveBeenCalledWith(new Load(component.widget.id));
    expect(widgetVisibilityService.setHidden).toHaveBeenCalledWith(component.widget.id, false);
  });

  it('should init and hide the widget without available colleagues', () => {
    // given
    const body = {loading: false, users: [] as User[]};

    const state = {interestingColleagues: {id: body}};

    store.select.and.returnValue(of(body));

    // when
    fixture.detectChanges();
    component.state$.subscribe();

    // then
    expect(store.select).toHaveBeenCalled();
    expect(store.select.calls.mostRecent().args[0](state)).toBe(state.interestingColleagues.id);
    expect(store.dispatch).toHaveBeenCalledWith(new Load(component.widget.id));
    expect(widgetVisibilityService.setHidden).toHaveBeenCalledWith(component.widget.id, true);
  });

  it('should reload colleagues', () => {
    // given

    // when
    component.refresh();

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new Load(component.widget.id));
  });

  it('should destroy the current state of the widget', () => {
    // given

    // when
    component.ngOnDestroy();

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new Reset(component.widget.id));
  });

  it('should set a new title for the widget', () => {
    // given
    const newTitle = 'my colleagues';
    _.set(component.widget, 'settings._titles[0]', newTitle);

    // when
    const result = component.getTitle();

    // then
    expect(result).toBe(newTitle);
  });
});
