import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngxs/store';
import {UserList} from '@shared/sender-ui/user-list/user-list';
import {WidgetComponent} from '@widgets/api/widget-component';
import {WidgetVisibilityService} from '@widgets/api/widget-visibility/widget-visibility.service';
import {InterestingColleaguesWidget} from '@widgets/interesting-colleagues/interesting-colleagues-widget';
import {
  Load,
  Reset
} from '@widgets/interesting-colleagues/interesting-colleagues-widget/interesting-colleagues.actions';
import * as _ from 'lodash';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'coyo-interesting-colleagues-widget',
  templateUrl: './interesting-colleagues-widget.component.html',
  styleUrls: ['./interesting-colleagues-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InterestingColleaguesWidgetComponent extends WidgetComponent<InterestingColleaguesWidget> implements OnInit, OnDestroy {

  state$: Observable<UserList>;
  private id: string;
  constructor(cd: ChangeDetectorRef,
              private widgetVisibilityService: WidgetVisibilityService,
              private store: Store) {
    super(cd);
  }

  ngOnInit(): void {
    this.id = this.widget.id || this.widget.tempId;
    this.state$ = this.store.select(state => state.interestingColleagues[this.id])
      .pipe(tap(res => {
        this.widgetVisibilityService.setHidden(this.widget.id, !res.loading && !res.users.length);
      }));
    this.store.dispatch(new Load(this.id));
  }

  refresh(): void {
    this.store.dispatch(new Load(this.widget.id));
  }

  ngOnDestroy(): void {
    this.store.dispatch(new Reset(this.widget.id));
  }

  getTitle(): string {
    return _.get(this.widget, 'settings._titles[0]');
  }
}
