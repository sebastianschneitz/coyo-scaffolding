import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Bookmark} from '@widgets/bookmarking/bookmark.model';

import {BookmarkingShowComponent} from './bookmarking-show.component';

describe('BookmarkingShowComponent', () => {
  let component: BookmarkingShowComponent;
  let fixture: ComponentFixture<BookmarkingShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkingShowComponent]
    }).overrideTemplate(BookmarkingShowComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkingShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open new window for absolute URL', () => {
    // given
    component.bookmark = {url: 'https://coyoapp.com'} as Bookmark;

    // when
    fixture.detectChanges();

    // then
    expect(component.target).toEqual('_blank');
  });

  it('should not open new window for relative URL', () => {
    // given
    component.bookmark = {url: '/foo/path'} as Bookmark;

    // when
    fixture.detectChanges();

    // then
    expect(component.target).toEqual('_self');
  });
});
