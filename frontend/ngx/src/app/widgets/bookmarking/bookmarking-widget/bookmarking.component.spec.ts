import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {SettingsService} from '@domain/settings/settings.service';
import {Bookmark} from '@widgets/bookmarking/bookmark.model';
import {BookmarkingWidget} from '@widgets/bookmarking/bookmarking-widget';

import {BookmarkingComponent} from './bookmarking.component';

describe('BookmarkingComponent', () => {
  let component: BookmarkingComponent;
  let fixture: ComponentFixture<BookmarkingComponent>;

  let widget: BookmarkingWidget;
  let parentForm: FormGroup;

  const bookmark1: Bookmark = {url: 'https://coyoapp.com', title: 'COYO'};
  const bookmark2: Bookmark = {url: 'https://abc.de', title: 'ABC'};
  const bookmark3: Bookmark = {url: 'https://a.de', title: 'A'};
  const bookmarkInvalid: Bookmark = {url: '', title: 'INVALID'};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: SettingsService,
        useValue: jasmine.createSpyObj('SettingsService', ['retrieveByKey'])
      }],
      declarations: [BookmarkingComponent]
    }).overrideTemplate(BookmarkingComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    widget = {settings: {}} as BookmarkingWidget;
    parentForm = new FormGroup({});

    fixture = TestBed.createComponent(BookmarkingComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize bookmarks list', () => {
    // given/when
    component.ngOnInit();

    // then
    expect(widget.settings._bookmarks).toEqual([]);
    expect(widget.settings._validBookmarkList).toBeFalsy();
  });

  it('should remove a bookmark', () => {
    // given
    widget.settings._bookmarks = [bookmark1, bookmark2];

    // when
    component.removeBookmark(1);

    // then
    expect(widget.settings._bookmarks).toEqual([bookmark1]);
    expect(widget.settings._validBookmarkList).toBeTruthy();
  });

  it('should change a bookmark', () => {
    // given
    widget.settings._bookmarks = [bookmark1, bookmark2];

    // when
    component.changeBookmark(bookmark3, 0);

    // then
    expect(widget.settings._bookmarks).toEqual([bookmark3, bookmark2]);
    expect(widget.settings._validBookmarkList).toBeTruthy();
  });

  it('should add a bookmark', () => {
    // given
    widget.settings._bookmarks = [bookmark1, bookmark2];

    // when
    component.addBookmark(bookmark3);

    // then
    expect(widget.settings._bookmarks).toEqual([bookmark1, bookmark2, bookmark3]);
    expect(widget.settings._validBookmarkList).toBeTruthy();
  });

  it('should add an invalid bookmark to the empty list', () => {
    // given
    component.ngOnInit();

    // when
    component.addBookmark(bookmarkInvalid);

    // then
    expect(widget.settings._bookmarks).toEqual([bookmarkInvalid]);
    expect(widget.settings._validBookmarkList).toBeFalsy();
  });

  it('should be able to drop and down the bookmarks', () => {
    // given
    widget.settings._bookmarks = [bookmark1, bookmark2];

    // when
    component.drop({previousIndex: 0, currentIndex: 1} as CdkDragDrop<any>);

    // then
    expect(widget.settings._bookmarks).toEqual([bookmark2, bookmark1]);
    expect(widget.settings._validBookmarkList).toBeTruthy();
  });
});
