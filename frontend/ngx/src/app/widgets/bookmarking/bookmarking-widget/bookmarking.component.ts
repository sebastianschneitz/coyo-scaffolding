import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {WidgetComponent} from '@widgets/api/widget-component';
import {Bookmark} from '@widgets/bookmarking/bookmark.model';
import {BookmarkingWidget} from '@widgets/bookmarking/bookmarking-widget';
import * as _ from 'lodash';

/**
 * Widget to display bookmarks.
 */
@Component({
  selector: 'coyo-bookmarking',
  templateUrl: './bookmarking.component.html',
  styleUrls: ['./bookmarking.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookmarkingComponent extends WidgetComponent<BookmarkingWidget> implements OnInit {

  constructor(cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit(): void {
    if (!this.bookmarks) {
      this.widget.settings._bookmarks = [];
      this.widget.settings._validBookmarkList = false;
    }
  }

  get bookmarks(): Bookmark[] {
    return this.widget.settings._bookmarks;
  }

  removeBookmark(idx: number): void {
    this.bookmarks.splice(idx, 1);
    this.validateBookmarkList();
  }

  changeBookmark(bookmark: Bookmark, idx: number): void {
    this.bookmarks.splice(idx, 1, bookmark);
    this.validateBookmarkList();
  }

  addBookmark(bookmark: Bookmark): void {
    this.bookmarks.push(bookmark);
    this.validateBookmarkList();
  }

  drop(event: CdkDragDrop<any>): void {
    moveItemInArray(this.bookmarks, event.previousIndex, event.currentIndex);
    this.validateBookmarkList();
  }

  private validateBookmarkList(): void {
    this.widget.settings._validBookmarkList = _.some(this.bookmarks, (bookmark: Bookmark) => !!bookmark.url);
  }
}
