/**
 * The entity model for a bookmark
 */
export interface Bookmark {
  url: string;
  title: string;
}
