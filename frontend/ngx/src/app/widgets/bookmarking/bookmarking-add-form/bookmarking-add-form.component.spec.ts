import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {SettingsService} from '@domain/settings/settings.service';
import {of} from 'rxjs';
import {BookmarkingAddFormComponent} from './bookmarking-add-form.component';

describe('BookmarkingAddFormComponent', () => {
  let component: BookmarkingAddFormComponent;
  let fixture: ComponentFixture<BookmarkingAddFormComponent>;

  let urlElement: jasmine.SpyObj<HTMLInputElement>;
  let titleElement: jasmine.SpyObj<HTMLInputElement>;

  let settingsService: jasmine.SpyObj<SettingsService>;
  const bookmark = {url: 'http://www.coyoapp.com', title: 'COYO'};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: SettingsService,
        useValue: jasmine.createSpyObj('SettingsService', ['retrieveByKey'])
      }],
      declarations: [BookmarkingAddFormComponent]
    }).overrideTemplate(BookmarkingAddFormComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    urlElement = jasmine.createSpyObj('urlInput', ['focus']);
    titleElement = jasmine.createSpyObj('titleInput', ['focus']);

    settingsService = TestBed.get(SettingsService);
    settingsService.retrieveByKey.and.returnValue(of('https?:\\/\\/\\S+'));

    fixture = TestBed.createComponent(BookmarkingAddFormComponent);
    component = fixture.componentInstance;
    detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set URL validator on init', () => {
    // given
    spyOn(component.url, 'setValidators');

    // when
    component.ngOnInit();

    // then
    expect(component.url.setValidators).toHaveBeenCalled();
  });

  it('should go to the title step', fakeAsync(() => {
    // given
    spyOn(component.bookmarkAdd, 'emit');
    component.bookmarkForm.setValue(bookmark);
    detectChanges();

    // when
    component.nextStep();

    // then
    expect(component.bookmarkAdd.emit).not.toHaveBeenCalled();
    tick();
    expect(titleElement.focus).toHaveBeenCalled();
  }));

  it('should add a bookmark', fakeAsync(() => {
    // given
    spyOn(component.bookmarkAdd, 'emit');
    component.bookmarkForm.setValue(bookmark);
    detectChanges();
    component.nextStep();
    detectChanges();

    // when
    component.nextStep();
    detectChanges();

    // then
    expect(component.bookmarkForm.value).toEqual({url: null, title: null});
    expect(component.bookmarkAdd.emit).toHaveBeenCalledWith(bookmark);
    tick();
    expect(urlElement.focus).toHaveBeenCalled();
  }));

  function detectChanges(): void {
    fixture.detectChanges();
    // elements are not static, so we have to set them again after each change detection
    component.urlInput = {nativeElement: urlElement};
    component.titleInput = {nativeElement: titleElement};
  }
});
