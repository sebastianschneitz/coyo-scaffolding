import {GoogleFileMetaData} from '@app/integration/gsuite/google-api/google-file-metadata';
import {Attachment} from '@domain/attachment/attachment';
import {StorageType} from '@domain/attachment/storage';
import {FilePreview} from '@domain/preview/file-preview';
import {Sender} from '@domain/sender/sender';

/**
 * Interface extending FilePreview and attachment to include fields used by the
 * latest-files widget
 */
export interface DocumentInfo extends FilePreview, Attachment {
  name: string | null;
  target: object | null;
  appRoot: string | null;
  contentType: string | null;
  created: Date | null;
  displayName: string | null;
  folder: string | null;
  id: string;
  modified: Date | null;
  senderId: string | null;
  storage: StorageType | null;
  author: Sender | null;
  attachment: boolean;
  canEdit: boolean;
  deleted: boolean;
  downloadUrl: string;
  exportLinks: GoogleFileMetaData['exportLinks'];
  externalFileUrl: string;
  fileId: string;
  groupId: string;
  length: number;
  modelId: string;
  previewAvailable: boolean;
  previewUrl: string;
  serialVersionUID: number;
  showSize: boolean;
}
