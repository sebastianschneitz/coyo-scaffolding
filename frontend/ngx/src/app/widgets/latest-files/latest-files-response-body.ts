import {App} from '@domain/apps/app';
import {DocumentInfo} from '@widgets/latest-files/document-info';

export interface LatestFilesResponseBody {
  app: App;
  documents: DocumentInfo[];
}
