import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {App} from '@domain/apps/app';
import {Sender} from '@domain/sender/sender';
import {Ng1FileAuthorService, Ng1fileDetailsModalService} from '@root/typings';
import {NG1_FILE_AUTHOR_SERVICE, NG1_FILE_DETAILS_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {DocumentInfo} from '@widgets/latest-files/document-info';
import {LatestFilesWidget} from '@widgets/latest-files/latest-files-widget';
import {LatestFilesService} from '@widgets/latest-files/latest-files.service';
import {Subject} from 'rxjs';
import {first} from 'rxjs/operators';
import {LatestFilesWidgetComponent} from './latest-files-widget.component';

describe('LatestFilesWidgetComponent', () => {
  let component: LatestFilesWidgetComponent;
  let fixture: ComponentFixture<LatestFilesWidgetComponent>;
  let widgetSettingsService: jasmine.SpyObj<WidgetSettingsService>;
  let latestFilesService: jasmine.SpyObj<LatestFilesService>;
  let ng1FileAuthorService: jasmine.SpyObj<Ng1FileAuthorService>;
  let ng1fileDetailsModalService: jasmine.SpyObj<Ng1fileDetailsModalService>;
  const app = {id: 'test-id', senderId: 'test-sender-id', settings: {showAuthors: true}} as App;
  const fileSubject = new Subject();
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [LatestFilesWidgetComponent],
        providers: [{
          provide: WidgetSettingsService,
          useValue: jasmine.createSpyObj('WidgetSettingsService', ['getChanges$'])
        },
          {
            provide: LatestFilesService,
            useValue: jasmine.createSpyObj('LatestFilesService', ['getLatestFiles'])
          },
          {
            provide: NG1_FILE_AUTHOR_SERVICE,
            useValue: jasmine.createSpyObj('Ng1FileAuthorService', ['loadFileAuthors'])
          },
          {
            provide: NG1_FILE_DETAILS_MODAL_SERVICE,
            useValue: jasmine.createSpyObj('Ng1fileDetailsModalService', ['open'])
          },
          ChangeDetectorRef]
      })
      .overrideTemplate(LatestFilesWidgetComponent, '')
      .compileComponents();
    widgetSettingsService = TestBed.get(WidgetSettingsService);
    latestFilesService = TestBed.get(LatestFilesService);
    latestFilesService.getLatestFiles.and.returnValue(fileSubject);
    ng1FileAuthorService = TestBed.get(NG1_FILE_AUTHOR_SERVICE);
    ng1fileDetailsModalService = TestBed.get(NG1_FILE_DETAILS_MODAL_SERVICE);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatestFilesWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {
        _titles: ['test-title'],
        _app: app,
        _fileCount: 5
      },
    } as unknown as LatestFilesWidget;
  });

  it('should create', () => {
    // given

    // when

    // then
    expect(component).toBeTruthy();
  });
  it('should correctly report not having any files', done => {
    // given
    ng1FileAuthorService.loadFileAuthors.and.returnValue(Promise.resolve({}));
    // when
    component.loadFiles();
    component.hasFiles$().pipe(first()).subscribe(hasFiles => {
      expect(hasFiles).toBeFalsy();
      done();
    });

    fileSubject.next({app, documents: []});
  });

  it('should correctly report having any files', done => {
    // given
    ng1FileAuthorService.loadFileAuthors.and.returnValue(Promise.resolve({'test-id': 'test-author' as any as Sender}));
    // when
    component.loadFiles();
    component.hasFiles$().pipe(first()).subscribe(hasFiles => {
      expect(hasFiles).toBeTruthy();
      done();
    });
    fileSubject.next({app, documents: [{id: 'test-id', author: null}]});

    // then

  });
  it('should correctly load files', done => {
    // given
    ng1FileAuthorService.loadFileAuthors.and.returnValue(Promise.resolve({'test-id': 'test-author' as any as Sender}));
    // when
    component.loadFiles();
    component.files$.pipe(first()).subscribe(files => {
      expect(files).toEqual([{id: 'test-id', author: 'test-author'}] as any as DocumentInfo[]);
      done();
    });

    fileSubject.next({app, documents: [{id: 'test-id', author: null}]});
  });
});
