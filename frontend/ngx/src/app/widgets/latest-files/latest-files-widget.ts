import {Widget} from '@domain/widget/widget';
import {LatestFilesWidgetSettings} from '@widgets/latest-files/latest-files-widget-settings';

export interface LatestFilesWidget extends Widget<LatestFilesWidgetSettings> {
}
