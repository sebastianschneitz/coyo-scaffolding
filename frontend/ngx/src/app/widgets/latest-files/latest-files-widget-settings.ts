import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export interface CompatibilityAppModel {
  id: string;
  senderId: string;
}
/**
 * The entity model for the settings of a latest-files widget
 */
export interface LatestFilesWidgetSettings extends WidgetSettings {
  _app: CompatibilityAppModel;
  _fileCount: number;
}
