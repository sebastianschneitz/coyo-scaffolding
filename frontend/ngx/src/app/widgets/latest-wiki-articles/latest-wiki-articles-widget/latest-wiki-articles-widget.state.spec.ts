import {async, TestBed} from '@angular/core/testing';
import {Target} from '@domain/sender/target';
import {TargetService} from '@domain/sender/target/target.service';
import {NgxsModule, Store} from '@ngxs/store';
import {LatestWikiArticle} from '@widgets/latest-wiki-articles/latest-wiki-article';
import {
  Load,
  Reset
} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget/latest-wiki-articles-widget.actions';
import {
  LatestWikiArticlesStatesModel,
  LatestWikiArticlesWidgetState
} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget/latest-wiki-articles-widget.state';
import {LatestWikiArticlesService} from '@widgets/latest-wiki-articles/latest-wiki-articles.service';
import {of} from 'rxjs';

describe('LatestWikiArticlesWidgetState', () => {
  let store: Store;
  let latestWikiArticlesService: jasmine.SpyObj<LatestWikiArticlesService>;
  let targetService: jasmine.SpyObj<TargetService>;
  const articles = [
    {senderTarget: {params: {id: 's1'}}, articleTarget: {params: {id: 'a1'}}},
    {senderTarget: {params: {id: 's2'}}, articleTarget: {params: {id: 'a2'}}},
    {senderTarget: {params: {id: 's3'}}, articleTarget: {params: {id: 'a3'}}}] as any as LatestWikiArticle[];
  const ID = 'id';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([LatestWikiArticlesWidgetState])],
      providers: [{
        provide: LatestWikiArticlesService,
        useValue: jasmine.createSpyObj('latestWikiArticlesService', ['getLatestWikiArticles'])
      },
        {
          provide: TargetService,
          useValue: jasmine.createSpyObj('targetService', ['getLinkTo'])
        }]
    });

    store = TestBed.get(Store);
    latestWikiArticlesService = TestBed.get(LatestWikiArticlesService);
    targetService = TestBed.get(TargetService);

  }));

  it('should init latest wiki articles state', () => {

    // given
    latestWikiArticlesService.getLatestWikiArticles.and.returnValue(of(articles));
    targetService.getLinkTo.and.callFake((target: Target) => target.params.id + '_link');

    // when
    store.dispatch(new Load(ID, {_appId: 'appId', _senderId: 'senderId', _articleCount: 8}));

    // then
    const actual: LatestWikiArticlesStatesModel = store.selectSnapshot((state => state.latestWikiArticles));
    expect(actual[ID]).toBeDefined();
    expect(actual[ID].articles).toEqual(articles);
    expect(actual[ID].links.articles).toEqual(Object.values(articles).reduce((prev: any, curr: LatestWikiArticle) => {
      prev[curr.articleTarget.params.id] = curr.articleTarget.params.id + '_link';
      return prev;
    }, {} as any));
    expect(actual[ID].links.senders).toEqual(Object.values(articles).reduce((prev: any, curr: LatestWikiArticle) => {
      prev[curr.senderTarget.params.id] = curr.senderTarget.params.id + '_link';
      return prev;
    }, {} as any));
    expect(actual[ID].loading).toBeFalsy();
  });

  it('should reset the latest wiki article state', () => {
    // given
    store.dispatch(new Reset(ID));

    // then
    const actual = store.selectSnapshot((state => state.latestWikiArticles));
    expect(actual[ID]).toBeUndefined();
  });
});
