import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngxs/store';
import {LatestWikiArticle} from '@widgets/latest-wiki-articles/latest-wiki-article';
import {LatestWikiArticlesWidget} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget';
import {Load} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget/latest-wiki-articles-widget.actions';
import {LatestWikiArticlesWidgetStateModel} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget/latest-wiki-articles-widget.state';
import * as _ from 'lodash';
import {of} from 'rxjs';
import {LatestWikiArticlesWidgetComponent} from './latest-wiki-articles-widget.component';

describe('LatestWikiArticlesWidgetComponent', () => {
  let component: LatestWikiArticlesWidgetComponent;
  let fixture: ComponentFixture<LatestWikiArticlesWidgetComponent>;
  let store: jasmine.SpyObj<Store>;
  const ID = 'widget-id';
  const testChange =
    (previousValue: any,
     currentValue: any) => {
      // given
      const previousSettings = _.merge(component.widget.settings, previousValue);
      component.widget.settings = previousSettings;

      // when
      fixture.detectChanges();
      component.ngOnChanges({
        widget: {
          previousValue,
          currentValue,
          firstChange: !previousValue,
          isFirstChange: null
        }
      });

      // then
      expect(store.dispatch).toHaveBeenCalledWith(new Load(ID, previousSettings));
      expect(store.dispatch).toHaveBeenCalledWith(new Load(ID, _.merge(previousSettings, currentValue)));
      expect(store.dispatch).toHaveBeenCalledTimes(2);
    };
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [LatestWikiArticlesWidgetComponent],
        providers: [ChangeDetectorRef, {
          provide: Store,
          useValue: jasmine.createSpyObj('store', ['select', 'dispatch'])
        }]
      }).overrideTemplate(LatestWikiArticlesWidgetComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(LatestWikiArticlesWidgetComponent);
    store = TestBed.get(Store);
    component = fixture.componentInstance;
    component.widget = {
      id: ID,
      settings: {
        _appId: 'appId',
        _senderId: 'senderId',
        _articleCount: 3
      },
    } as LatestWikiArticlesWidget;
  }));

  it('should create', () => {
    // given
    const state = {latestWikiArticles: {[ID]: 'state'}};
    const model: LatestWikiArticlesWidgetStateModel = {
      loading: false,
      hasArticles: true,
      articles: ['a' as any as LatestWikiArticle],
      links: {
        articles: {a: 'article_link_a'},
        senders: {a: 'sender_link_a'}
      }
    };
    let expectedStateSelectResult = null;
    store.select.and.callFake((callback: any) => {
      expectedStateSelectResult = callback(state);
      return of(model);
    });

    // when
    fixture.detectChanges();

    // then
    expect(expectedStateSelectResult).toEqual('state');
    expect(component).toBeTruthy();
    expect(store.select).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalledWith(new Load(ID, component.widget.settings));
  });

  it('should refresh when app id changes', () => {
    testChange({settings: {_appId: 'a'}}, {settings: {_appId: 'b'}});
  });

  it('should refresh when sender id changes', () => {
    testChange({settings: {_senderId: 'a'}}, {settings: {_senderId: 'b'}});
  });

  it('should refresh when article count changes', () => {
    testChange({settings: {_articleCount: '1'}}, {settings: {_articleCount: '2'}});
  });

  it('should refresh when no settings were set', () => {
    const settings = component.widget.settings;
    component.widget.settings = undefined;
    testChange({settings: undefined}, {settings});
  });

  it('should not refresh when no settings were altered', () => {
    // when
    fixture.detectChanges();
    component.ngOnChanges({
      widget: {
        previousValue: {settings: component.widget.settings},
        currentValue: {settings: component.widget.settings},
        firstChange: false,
        isFirstChange: null
      }
    });

    // then
    expect(store.dispatch).toHaveBeenCalledTimes(1);
    expect(store.dispatch).toHaveBeenCalledWith(new Load(ID, component.widget.settings));
  });

  it('should destroy', () => {
    // when
    fixture.detectChanges();
    component.ngOnDestroy();

    // then
    expect(store.dispatch).toHaveBeenCalled();
  });
});
