import {NgModule} from '@angular/core';
import {NgxsModule} from '@ngxs/store';
import {AppUiModule} from '@shared/app-ui/app-ui.module';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {HelpModule} from '@shared/help/help.module';
import {TimeModule} from '@shared/time/time.module';
import {UpgradeModule} from '@upgrade/upgrade.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {LATEST_WIKI_ARTICLES_WIDGET} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget-config';
/*tslint:disable:max-line-length*/
import {LatestWikiArticlesWidgetSettingsComponent} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget-settings/latest-wiki-articles-widget-settings.component';
import {LatestWikiArticlesWidgetComponent} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget/latest-wiki-articles-widget.component';
import {LatestWikiArticlesWidgetState} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget/latest-wiki-articles-widget.state';

export const ngxsModule = NgxsModule.forFeature([LatestWikiArticlesWidgetState]);
/**
 * Module providing the latest-wiki-articles widget.
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    CoyoFormsModule,
    AppUiModule,
    UpgradeModule,
    HelpModule,
    TimeModule,
    ngxsModule
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: LATEST_WIKI_ARTICLES_WIDGET, multi: true}
  ],
  declarations: [
    LatestWikiArticlesWidgetComponent,
    LatestWikiArticlesWidgetSettingsComponent
  ],
  entryComponents: [
    LatestWikiArticlesWidgetComponent,
    LatestWikiArticlesWidgetSettingsComponent
  ]
})
export class LatestWikiArticlesWidgetModule {
}
