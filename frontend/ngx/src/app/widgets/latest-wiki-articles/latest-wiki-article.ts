import {Target} from '@domain/sender/target';

/**
 * DTO for latest-wiki-article responses
 */
export interface LatestWikiArticle {
  title: string;
  senderName: string;
  created: Date;
  updated: Date;
  articleTarget: Target;
  senderTarget: Target;
}
