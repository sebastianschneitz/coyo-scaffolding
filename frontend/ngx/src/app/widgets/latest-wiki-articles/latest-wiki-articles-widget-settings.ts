import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a latest-wiki-articles widget
 */
export interface LatestWikiArticlesWidgetSettings extends WidgetSettings {
  _appId: string;
  _senderId: string;
  _articleCount: number;
}
