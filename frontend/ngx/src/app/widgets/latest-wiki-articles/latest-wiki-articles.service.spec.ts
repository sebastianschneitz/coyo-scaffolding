import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {LatestWikiArticlesService} from '@widgets/latest-wiki-articles/latest-wiki-articles.service';
import {NewColleaguesService} from '@widgets/new-colleagues/new-colleagues.service';

describe('LatestWikiArticlesService', () => {
  let service: LatestWikiArticlesService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [NewColleaguesService]
    });
    service = TestBed.get(LatestWikiArticlesService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should get latest wiki articles of a given app', () => {
    // given
    const response: any[] = [];
    const count = LatestWikiArticlesService.DEFAULT_COUNT + 1;
    const appId = 'app-id';
    // when
    const result = service.getLatestWikiArticles(count, appId);
    let called = false;
    result.subscribe(res => {
      called = true;
      expect(res).toBe(response);
    });
    // then
    const request = httpMock
      .expectOne(`/web/widgets/wiki/latest?appId=${appId}&count=${count}`);
    request.flush(response);
    expect(called).toBeTruthy();
  });

  it('should get latest wiki articles for all apps', () => {
    // given
    const response: any[] = [];
    const count = LatestWikiArticlesService.DEFAULT_COUNT + 1;
    const appId: any = undefined;
    // when
    const result = service.getLatestWikiArticles(count, appId);
    let called = false;
    result.subscribe(res => {
      called = true;
      expect(res).toBe(response);
    });
    // then
    const request = httpMock
      .expectOne(`/web/widgets/wiki/latest?count=${count}`);
    request.flush(response);
    expect(called).toBeTruthy();
  });

  it('should use the default number of articles', () => {
    // given
    const response: any[] = [];
    const count: any = undefined;
    const appId = 'app-id';
    // when
    const result = service.getLatestWikiArticles(count, appId);
    let called = false;
    result.subscribe(res => {
      called = true;
      expect(res).toBe(response);
    });
    // then
    const request = httpMock
      .expectOne(`/web/widgets/wiki/latest?appId=${appId}&count=${LatestWikiArticlesService.DEFAULT_COUNT}`);
    request.flush(response);
    expect(called).toBeTruthy();
  });
});
