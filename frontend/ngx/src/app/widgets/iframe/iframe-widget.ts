import {Widget} from '@domain/widget/widget';
import {IframeWidgetSettings} from '@widgets/iframe/iframe-widget-settings.model';

export interface IframeWidget extends Widget<IframeWidgetSettings> {
}
