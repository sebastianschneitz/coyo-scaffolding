import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnChanges, OnInit} from '@angular/core';
import {WidgetComponent} from '@widgets/api/widget-component';
import {IframeWidget} from '@widgets/iframe/iframe-widget';

/**
 * The iframe widget embeds web pages.
 */
@Component({
  selector: 'coyo-iframe-widget',
  templateUrl: './iframe-widget.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IframeWidgetComponent extends WidgetComponent<IframeWidget> implements OnInit, OnChanges {

  /**
   * If scrolling is activated in the widget.
   */
  scrolling: string;

  constructor(cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit(): void {
    this.setScrolling();
  }

  ngOnChanges(): void {
    this.setScrolling();
  }

  setScrolling(): void {
    this.scrolling = this.widget.settings.scrolling ? 'yes' : 'no';
  }
}
