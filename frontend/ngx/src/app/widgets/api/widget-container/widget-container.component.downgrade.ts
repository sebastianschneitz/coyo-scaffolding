import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetContainerComponent} from './widget-container.component';

getAngularJSGlobal()
  .module('coyo.widgets')
  .directive('coyoNgxWidget', downgradeComponent({
    component: WidgetContainerComponent
  }));
