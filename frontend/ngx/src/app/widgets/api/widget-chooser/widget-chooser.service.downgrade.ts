import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetChooserService} from './widget-chooser.service';

getAngularJSGlobal()
  .module('coyo.widgets.api')
  .factory('ngxWidgetChooser', downgradeInjectable(WidgetChooserService) as any);
