import {Overlay} from '@angular/cdk/overlay';
import {Injectable, NgZone} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatDialogSize} from '@coyo/ui';
import {filter, first} from 'rxjs/operators';
import {WidgetChooserComponent} from './widget-chooser.component';

/**
 * A service that opens a {@link WidgetChooserComponent} modal to select and configure widgets.
 */
@Injectable()
export class WidgetChooserService {

  constructor(private ngZone: NgZone,
              private dialog: MatDialog,
              private overlay: Overlay) {
  }

  /**
   * Opens a {@link WidgetChooserComponent} to select and configure widgets.
   *
   * @returns a promise containing the widget data
   */
  open(): Promise<any> {
    return this.ngZone.run(() => {
      const dialogRef = this.dialog.open(WidgetChooserComponent, {
        width: MatDialogSize.Medium,
        // This strategy needs to be used to make drag and drop work inside the dialog.
        // Issue link: https://github.com/angular/components/issues/15880
        scrollStrategy: this.overlay.scrollStrategies.noop(),
        data: {
          widget: {
            isNew: () => true, settings: {}
          }
        }
      });

      return dialogRef.afterClosed()
        .pipe(first())
        .pipe(filter(value => value))
        .toPromise();
    });
  }
}
