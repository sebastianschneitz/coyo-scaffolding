import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AuthService} from '@core/auth/auth.service';
import {Widget} from '@domain/widget/widget';
import {TranslateService} from '@ngx-translate/core';
import {Ng1ScrollBehaviourService} from '@root/typings';
import {UIRouter} from '@uirouter/core';
import {NG1_SCROLL_BEHAVIOR_SERVICE} from '@upgrade/upgrade.module';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {ButtonWidget} from '@widgets/button/button-widget';
import {ButtonWidgetSettingsComponent} from '@widgets/button/button-widget-settings/button-widget-settings.component';
import {ButtonWidgetComponent} from '@widgets/button/button-widget/button-widget.component';
import {of} from 'rxjs';
import {skip} from 'rxjs/operators';
import {WidgetCategory, WidgetConfig} from '../widget-config';
import {WidgetRegistryService} from '../widget-registry/widget-registry.service';
import {WidgetChooserComponent, WidgetGroup} from './widget-chooser.component';

describe('WidgetChooserComponent', () => {
  let component: WidgetChooserComponent;
  let fixture: ComponentFixture<WidgetChooserComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let widgetRegistry: jasmine.SpyObj<WidgetRegistryService>;
  let dialog: jasmine.SpyObj<MatDialogRef<WidgetChooserComponent>>;
  let translateService: jasmine.SpyObj<TranslateService>;
  let scrollBehaviourService: jasmine.SpyObj<Ng1ScrollBehaviourService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: AuthService,
        useValue: jasmine.createSpyObj('authService', ['getUser'])
      }, {
        provide: WidgetRegistryService,
        useValue: jasmine.createSpyObj('widgetRegistry', ['getEnabled'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['get'])
      }, {
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('dialog', ['close'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: {}
      }, {
        provide: UIRouter, useValue: {globals: {start$: of()}}
      }, {
        provide: NG1_SCROLL_BEHAVIOR_SERVICE, useValue: jasmine.createSpyObj('scrollBehaviourService',
          ['enableBodyScrolling', 'disableBodyScrolling', 'disableBodyScrollingOnXsScreen'])
      }],
      imports: [],
      declarations: [WidgetChooserComponent]
    }).overrideTemplate(WidgetChooserComponent, '<div></div>')
      .compileComponents();

    authService = TestBed.get(AuthService);
    widgetRegistry = TestBed.get(WidgetRegistryService);
    dialog = TestBed.get(MatDialogRef);
    translateService = TestBed.get(TranslateService);
    scrollBehaviourService = TestBed.get(NG1_SCROLL_BEHAVIOR_SERVICE);
  }));

  const buttonWidgetConfig: WidgetConfig<ButtonWidget> = {
    key: 'button',
    name: 'WIDGET.BUTTON.NAME',
    description: 'WIDGET.BUTTON.DESCRIPTION',
    icon: 'zmdi-mail-reply zmdi-hc-flip-horizontal',
    component: ButtonWidgetComponent,
    categories: WidgetCategory.STATIC,
    settings: {
      skipOnCreate: false,
      component: ButtonWidgetSettingsComponent
    }
  };

  const noSettingsButtonWidgetConfig: WidgetConfig<ButtonWidget> = {
    key: 'button-no-settings',
    name: 'WIDGET.BUTTON-NO-SETTINGS.NAME',
    description: 'WIDGET.BUTTON-NO-SETTINGS.DESCRIPTION',
    icon: 'zmdi-mail-reply zmdi-hc-flip-horizontal',
    component: ButtonWidgetComponent,
    categories: WidgetCategory.STATIC
  };

  const widgets: WidgetConfig<ButtonWidget>[] = [
    buttonWidgetConfig,
    noSettingsButtonWidgetConfig
  ];

  beforeEach(() => {
    authService.getUser.and.returnValue(of({
      moderatorMode: true
    }));
    widgetRegistry.getEnabled.and.returnValue(of(widgets));
    translateService.get.and.callFake((values: any) =>
      of(values.reduce((o: any, v: any) => {
        o[v] = v;
        return o;
      }, {})));

    fixture = TestBed.createComponent(WidgetChooserComponent);
    component = fixture.componentInstance;
    component.widget = {} as unknown as Widget<WidgetSettings>;
    fixture.detectChanges();

  });

  it('should initialize correctly', () => {
    // when
    component.allConfigs$.subscribe(values => {
      expect(values).toEqual([noSettingsButtonWidgetConfig, buttonWidgetConfig]);
    });

    // then
    expect(component).toBeTruthy();
  });

  it('should search for button widget', async(() => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      // skip first emitted value, as it is the inital load of all widgets
      component.tabConfigs$.pipe(skip(1)).subscribe(values => {
        const expected: WidgetGroup = ['ALL', [buttonWidgetConfig]];
        expect(values).toEqual([expected]);
      });

      component.searchInput.setValue('WIDGET.BUTTON.NAME', {
        emitEvent: true
      });
      fixture.detectChanges();
    });
  }));

  it('should rebuild form on back', () => {
    // given
    component.config = noSettingsButtonWidgetConfig;
    fixture.detectChanges();
    expect(component.config).toBe(noSettingsButtonWidgetConfig);

    // when
    component.onBack();

    // then
    expect(component.config).toBe(null);
    expect(component.widgetForm.value).toEqual({});
  });

  it('should set config when selecting', () => {
    // given

    // when
    component.select(buttonWidgetConfig);

    // then
    expect(component.config).toBe(buttonWidgetConfig);
  });

  it('should submit directly when component has no settings', fakeAsync(() => {
    // given
    component.onSubmitSubject.subscribe((value: WidgetConfig<Widget<WidgetSettings>>) => {
      expect(value).toBeUndefined();
    });

    // when
    component.select(noSettingsButtonWidgetConfig);
    tick();

    // then
    expect(component.config).toBe(noSettingsButtonWidgetConfig);
    expect(dialog.close).toHaveBeenCalled();
  }));
});
