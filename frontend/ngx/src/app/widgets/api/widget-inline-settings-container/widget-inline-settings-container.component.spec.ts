import {ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetInlineSettingsComponent} from '../widget-inline-settings-component';
import {WidgetInlineSettingsContainerComponent} from './widget-inline-settings-container.component';

class DummyWidgetInlineSettingsComponent extends WidgetInlineSettingsComponent<Widget<WidgetSettings>> {
}

describe('WidgetInlineSettingsContainerComponent', () => {
  let component: WidgetInlineSettingsContainerComponent;
  let fixture: ComponentFixture<WidgetInlineSettingsContainerComponent>;
  let componentFactoryResolver: jasmine.SpyObj<ComponentFactoryResolver>;
  let viewContainerRef: jasmine.SpyObj<ViewContainerRef>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WidgetInlineSettingsContainerComponent],
      providers: [{
        provide: ComponentFactoryResolver,
        useValue: jasmine.createSpyObj('ComponentFactoryResolver', ['resolveComponentFactory'])
      }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetInlineSettingsContainerComponent);
    component = fixture.componentInstance;
    componentFactoryResolver = fixture.debugElement.injector.get(ComponentFactoryResolver); // tslint:disable-line:deprecation
    viewContainerRef = fixture.debugElement.injector.get(ViewContainerRef); // tslint:disable-line:deprecation
  });

  it('should dynamically create a component', () => {
    spyOn(viewContainerRef, 'createComponent');

    // given
    const componentFactory = {};
    componentFactoryResolver.resolveComponentFactory.and.returnValue(componentFactory);
    const componentRef = {instance: {}};
    viewContainerRef.createComponent.and.returnValue(componentRef);

    component.component = DummyWidgetInlineSettingsComponent;
    component.widget = {};
    fixture.detectChanges();

    // when
    component.ngOnInit();

    // then
    expect(componentFactoryResolver.resolveComponentFactory).toHaveBeenCalledWith(component.component);
    expect(viewContainerRef.createComponent).toHaveBeenCalledWith(componentFactory);
    expect(component['componentRef'].instance.widget).toEqual(component.widget);
  });
});
