import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {Widget} from '@domain/widget/widget';
import {Ng1WidgetRegistry} from '@root/typings';
import {NG1_WIDGET_REGISTRY} from '@upgrade/upgrade.module';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {WIDGET_CONFIGS, WidgetConfig} from '../widget-config';

interface WidgetEnabledConfig {
  key: string;
  enabled: boolean;
  moderatorsOnly: boolean;
}

/**
 * Service to manage and retrieve provided instances of {@link WidgetConfig}.
 */
@Injectable()
export class WidgetRegistryService {

  private widgetConfigs: WidgetConfig<Widget<WidgetSettings>>[];
  private widgetConfigsCache$: Observable<WidgetEnabledConfig[]>;

  constructor(@Inject(NG1_WIDGET_REGISTRY) widgetRegistry: Ng1WidgetRegistry,
              @Inject(WIDGET_CONFIGS) widgetConfigs: WidgetConfig<Widget<WidgetSettings>>[],
              private http: HttpClient) {
    this.widgetConfigs = (widgetRegistry.getAll() as WidgetConfig<Widget<WidgetSettings>>[]).concat(widgetConfigs);
  }

  /**
   * Returns the configurations of all registered widgets (including disabled widgets).
   *
   * @returns an array of widget configurations
   */
  getAll(): WidgetConfig<Widget<WidgetSettings>>[] {
    return this.widgetConfigs;
  }

  /**
   * Returns the configuration of the (possibly disabled) widget with the given widget key.
   *
   * @param key the key of the widget
   * @returns the widget configuration or `undefined` if no configuration exists
   */
  get(key: string): WidgetConfig<Widget<WidgetSettings>> | undefined {
    return this.widgetConfigs.find(config => config.key === key);
  }

  /**
   * Returns the configurations of all registered and enabled widgets.
   *
   * @param includeRestricted include restricted widgets in the result list
   * @returns an array of widget configurations
   */
  getEnabled(includeRestricted: boolean = false): Observable<WidgetConfig<Widget<WidgetSettings>>[]> {
    const filter = (c1: any, c2: any) => c1.key === c2.key && (includeRestricted || !c2.moderatorsOnly);
    if (!this.widgetConfigsCache$) {
      this.widgetConfigsCache$ = this.http
        .get<WidgetEnabledConfig[]>('/web/widget-configurations')
        .pipe(shareReplay(1));
    }

    return this.widgetConfigsCache$.pipe(map(configs => configs.map(config =>
      this.widgetConfigs.find(widgetConfig => filter(widgetConfig, config)))
      .filter(config => config !== undefined)));
  }
}
