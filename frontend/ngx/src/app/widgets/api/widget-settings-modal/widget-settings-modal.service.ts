import {Overlay} from '@angular/cdk/overlay';
import {Injectable, NgZone} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import * as _ from 'lodash';
import {filter} from 'rxjs/operators';
import {WidgetRegistryService} from '../widget-registry/widget-registry.service';
import {WidgetSettingsModalComponent} from './widget-settings-modal.component';

/**
 * A service that opens a {@link WidgetSettingsModalComponent} modal to configure widgets.
 */
@Injectable()
export class WidgetSettingsModalService {

  constructor(private dialog: MatDialog,
              private widgetRegistry: WidgetRegistryService,
              private ngZone: NgZone, private overlay: Overlay) {
  }

  /**
   * Opens a {@link WidgetSettingsModalComponent} to select and configure widgets.
   *
   * @param widget the widget to open the settings for
   * @returns a promise containing the widget data
   */
  open(widget: any): Promise<any> {
    return this.ngZone.run(() => {
      const dialogRef = this.dialog.open(WidgetSettingsModalComponent, {
        // This strategy needs to be used to make drag and drop work inside the dialog.
        // Issue link: https://github.com/angular/components/issues/15880
        scrollStrategy: this.overlay.scrollStrategies.noop(),
        data: {
          config: this.widgetRegistry.get(widget.key),
          widget: _.cloneDeep(widget)
        }
      });

      return dialogRef.afterClosed()
        .pipe(filter(value => value))
        .toPromise();
    });
  }
}
