import {NoopScrollStrategy, Overlay, ScrollStrategyOptions} from '@angular/cdk/overlay';
import {async, inject, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {Subject} from 'rxjs';
import {WidgetRegistryService} from '../widget-registry/widget-registry.service';
import {WidgetSettingsModalComponent} from './widget-settings-modal.component';
import {WidgetSettingsModalService} from './widget-settings-modal.service';

describe('WidgetSettingsModalService', () => {
  let dialogService: jasmine.SpyObj<MatDialog>;
  let widgetRegistry: jasmine.SpyObj<WidgetRegistryService>;
  let scrollStategies: jasmine.SpyObj<ScrollStrategyOptions>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WidgetSettingsModalService, {
        provide: MatDialog,
        useValue: jasmine.createSpyObj('MatDialog', ['open'])
      }, {
        provide: WidgetRegistryService,
        useValue: jasmine.createSpyObj('WidgetRegistryService', ['get'])
      }, {
        provide: Overlay,
        useValue: {
          scrollStrategies: jasmine.createSpyObj('scrollStrategies', ['noop'])
        }
      }]
    });

    dialogService = TestBed.get(MatDialog);
    widgetRegistry = TestBed.get(WidgetRegistryService);
    scrollStategies = TestBed.get(Overlay).scrollStrategies;
  });

  it('should be created', inject([WidgetSettingsModalService], (service: WidgetSettingsModalService) => {
    expect(service).toBeTruthy();
  }));

  it('should open a modal', async((done: () => void) =>
    inject([WidgetSettingsModalService], (service: WidgetSettingsModalService) => {
      // given
      const widgetConfig = {key: 'widget', data: {}};
      const widget = {key: 'widget', name: 'WidgetComponent'};
      widgetRegistry.get.and.returnValue(widgetConfig);

      const noopScrollStrategy = new NoopScrollStrategy();

      const subject = new Subject<any>();
      const dialogRef = jasmine.createSpyObj('dialogRef', ['afterClosed']);
      dialogRef.afterClosed.and.returnValue(subject);
      dialogService.open.and.returnValue(dialogRef);
      scrollStategies.noop.and.returnValue(noopScrollStrategy);

      // when
      service.open(widget).then(result => {
        // then
        expect(result).toEqual('result');
        expect(dialogService.open).toHaveBeenCalledWith(WidgetSettingsModalComponent, {
          scrollStrategy: noopScrollStrategy,
          data: {
            config: widgetConfig,
            widget: widget,
          }
        });
        expect(scrollStategies.noop).toHaveBeenCalled();
        done();
      });
      subject.next('result');
    })));
});
