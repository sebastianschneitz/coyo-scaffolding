import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UIRouter} from '@uirouter/angular';
import {WidgetModal} from '../widget-modal';

/**
 * A modal dialog to configure widgets.
 *
 * This component should not be used directly to show a widget configuration dialog. The
 * {@link WidgetSettingsModalService#open} provides the canonical way to configure widgets.
 */
@Component({
  selector: 'coyo-widget-settings-modal',
  templateUrl: './widget-settings-modal.component.html',
  styleUrls: ['./widget-settings-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WidgetSettingsModalComponent extends WidgetModal {

  constructor(public dialogRef: MatDialogRef<WidgetSettingsModalComponent>, uiRouter: UIRouter,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    super(dialogRef, uiRouter);
    this.widget = data.widget;
    this.config = data.config;
  }
}
