import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * An event that is sent when widget settings are updated.
 */
export interface WidgetSettingsUpdatedEvent {
  widgetId?: string;
  tempId?: string;
  settings: WidgetSettings;
}
