import {Target} from '@domain/sender/target';

export interface WidgetWikiArticleResponse {
  title: string;
  articleTarget: Target;
  created?: Date;
  senderName: string;
  senderTarget: Target;
}
