import {Widget} from '@domain/widget/widget';
import {WikiArticleWidgetSettings} from '@widgets/wiki-article/wiki-article-widget-settings';

export interface WikiArticleWidget extends Widget<WikiArticleWidgetSettings> {
}
