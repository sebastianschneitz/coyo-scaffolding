import {async, TestBed} from '@angular/core/testing';
import {Target} from '@domain/sender/target';
import {NgxsModule, Store} from '@ngxs/store';
import {WikiArticleWidgetService} from '@widgets/wiki-article/wiki-article-widget/wiki-article-widget.service';
import {WikiArticleWidgetState} from '@widgets/wiki-article/wiki-article-widget/wiki-article-widget.state';
import {of} from 'rxjs';
import {Init} from './wiki-article-widget.actions';

describe('WikiArticleWidgetSettingsStore', () => {
  let store: Store;
  let wikiService: jasmine.SpyObj<WikiArticleWidgetService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([WikiArticleWidgetState])],
      providers: [{
        provide: WikiArticleWidgetService,
        useValue: jasmine.createSpyObj('wikiArticleWidgetService', ['getWikiArticle'])
      }]
    }).compileComponents();

    store = TestBed.get(Store);
    wikiService = TestBed.get(WikiArticleWidgetService);
  }));

  it('should init the widget', () => {
    // given
    const id = 'id';
    const articleId = 'article-id';
    const article = {
      title: 'title',
      articleTarget: {} as Target,
      senderName: 'sender-name',
      senderTarget: {
        name: 'sender-id',
        params: {}
      }
    };

    wikiService.getWikiArticle.and.returnValue(of(article));

    // when
    store.dispatch(new Init(id, articleId));

    // then
    const state = store.selectSnapshot(snapshot => snapshot.wikiArticleWidgetState);

    expect(state[id]).toBeDefined();
    expect(state[id].article).toBe(article);
    expect(state[id].sender.displayName).toBe(article.senderName);
    expect(state[id].sender.target).toBe(article.senderTarget);
    expect(state[id].loading).toBeFalsy();
  });
});
