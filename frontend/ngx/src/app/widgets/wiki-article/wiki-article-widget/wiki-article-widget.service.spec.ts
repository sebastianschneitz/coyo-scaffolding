import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {WidgetWikiArticleResponse} from '@widgets/wiki-article/widget-wiki-article-response';
import {WikiArticleWidgetService} from './wiki-article-widget.service';

describe('WikiArticleWidgetService', () => {
  let service: WikiArticleWidgetService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WikiArticleWidgetService],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.get(WikiArticleWidgetService);
    http = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should request a single wiki article', () => {
    // given
    const id = 'id';
    const wikiArticle = {
      title: 'title'
    } as WidgetWikiArticleResponse;

    // when
    const result = service.getWikiArticle(id);

    // then
    let called = false;
    result.subscribe(response => {
      called = true;
      expect(response).toBe(wikiArticle);
    });

    const req = http.expectOne(`/web/widgets/wikiarticle/article/${id}`);
    req.flush(wikiArticle);

    expect(called).toBeTruthy();
  });

  it('should request a wiki article page', () => {
    // given
    const id = 'id';
    const searchTerm = 'search';
    const wikiArticle = {
      title: 'title'
    } as WidgetWikiArticleResponse;

    const page = {
      content: [wikiArticle]
    } as Page<WidgetWikiArticleResponse>;

    // when
    const result = service.getWikiArticlePage(new Pageable(), searchTerm);

    // then
    let called = false;
    result.subscribe(response => {
      called = true;
      expect(response).toBe(page);
    });

    const req = http.expectOne('/web/widgets/wikiarticle?title=search&_page=0&_pageSize=20');
    req.flush(page);

    expect(called).toBeTruthy();
  });
});
