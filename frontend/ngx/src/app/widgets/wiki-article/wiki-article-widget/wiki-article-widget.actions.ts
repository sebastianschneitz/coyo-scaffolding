export class Init {
  static readonly type: string = '[WikiArticleWidget] Init';
  constructor(public id: string, public articleId: string) { }
}

export class SetLoading {
  static readonly type: string = '[WikiArticleWidgetSettings] Set loading';
  constructor(public id: string, public loading: boolean) { }
}
