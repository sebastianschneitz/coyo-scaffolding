import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {Page} from '@domain/pagination/page';
import {NgxsModule, Store} from '@ngxs/store';
import {WidgetWikiArticleResponse} from '@widgets/wiki-article/widget-wiki-article-response';
import {WikiArticleWidget} from '@widgets/wiki-article/wiki-article-widget';
import {
  LoadMore,
  Search,
  TriggerSearch
} from '@widgets/wiki-article/wiki-article-widget-settings/wiki-article-widget-settings.actions';

import {WikiArticleWidgetSettingsComponent} from './wiki-article-widget-settings.component';

describe('WikiArticleWidgetSettingsComponent', () => {
  let component: WikiArticleWidgetSettingsComponent;
  let fixture: ComponentFixture<WikiArticleWidgetSettingsComponent>;
  let store: Store;
  let dispatchSpy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WikiArticleWidgetSettingsComponent],
      imports: [NgxsModule.forRoot([])]
    }).overrideTemplate(WikiArticleWidgetSettingsComponent, '')
      .compileComponents();

    store = TestBed.get(Store);
    dispatchSpy = spyOn(store, 'dispatch');
    dispatchSpy.and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiArticleWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.parentForm = new FormGroup({});
    component.widget = {
      id: 'id',
      settings: {}
    } as WikiArticleWidget;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should debounce searching', fakeAsync(() => {
    // when
    store.dispatch(new TriggerSearch('a'));
    tick();
    store.dispatch(new TriggerSearch('ab'));

    // then
    expect(dispatchSpy).not.toHaveBeenCalledWith(new Search('a'));
    expect(dispatchSpy).not.toHaveBeenCalledWith(new Search('ab'));

    // when again
    tick(500);

    // then again
    expect(dispatchSpy).not.toHaveBeenCalledWith(new Search('a'));
    expect(dispatchSpy).toHaveBeenCalledWith(new Search('ab'));
  }));

  it('should cleanup properly', fakeAsync(() => {
    // when
    store.dispatch(new TriggerSearch('a'));

    // when again
    component.ngOnDestroy();
    tick(500);

    // then again
    expect(dispatchSpy).not.toHaveBeenCalledWith(new Search('a'));
  }));

  it('should load more on scroll if the end of the list is near', () => {
    // given

    // when
    component.onScroll({start: 0, end: 1}, {
      initialized: true,
      articles: [{id: 'id'}, {id: 'id'}, {id: 'id'}, {id: 'id'}, {id: 'id'}, {id: 'id'},
        {id: 'id'}, {id: 'id'}, {id: 'id'}] as unknown as WidgetWikiArticleResponse[],
      page: {} as Page<WidgetWikiArticleResponse>,
      searchTerm: '',
      loading: false
    });

    // then
    expect(dispatchSpy).toHaveBeenCalledWith(new LoadMore());
  });

  it('should load more on scroll if the end of the list is near', () => {
    // given

    // when
    component.onScroll({start: 0, end: 0}, {
      initialized: true,
      articles: [{id: 'id'}, {id: 'id'}, {id: 'id'}, {id: 'id'}, {id: 'id'}, {id: 'id'},
        {id: 'id'}, {id: 'id'}, {id: 'id'}] as unknown as WidgetWikiArticleResponse[],
      page: {} as Page<WidgetWikiArticleResponse>,
      searchTerm: '',
      loading: false
    });

    // then
    expect(dispatchSpy).not.toHaveBeenCalledWith(new LoadMore());
  });

  it('should load articles on open', () => {
    // given

    // when
    component.onOpen();

    // then
    expect(dispatchSpy).toHaveBeenCalledWith(new Search(''));
  });

  it('should trigger search request', () => {
    // given
    const searchTerm = 'test';

    // when
    component.onSearch({term: searchTerm});

    // then
    expect(dispatchSpy).toHaveBeenCalledWith(new TriggerSearch(searchTerm));
  });

  afterEach(() => {
    component.ngOnDestroy();
  });
});
