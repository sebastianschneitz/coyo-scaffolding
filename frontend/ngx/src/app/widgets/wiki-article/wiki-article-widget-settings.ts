import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a wiki article widget
 */
export interface WikiArticleWidgetSettings extends WidgetSettings {
  _articleId: string;
}
