import {SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TargetService} from '@domain/sender/target/target.service';
import {BlogArticleWidget, SingleBlogArticle} from '@widgets/blog-article/blog-article-widget';
import {BlogArticleWidgetService} from '@widgets/blog-article/blog-article-widget.service';
import {of} from 'rxjs';
import {skip} from 'rxjs/operators';

import {BlogArticleWidgetComponent} from './blog-article-widget.component';

describe('BlogArticleWidgetComponent', () => {
  let component: BlogArticleWidgetComponent;
  let fixture: ComponentFixture<BlogArticleWidgetComponent>;
  let blogArticleWidgetService: jasmine.SpyObj<BlogArticleWidgetService>;
  let targetService: jasmine.SpyObj<TargetService>;
  const singleBlogArticle: SingleBlogArticle = new SingleBlogArticle();

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        providers: [{
          provide: BlogArticleWidgetService,
          useValue: jasmine.createSpyObj('BlogArticleWidgetService', ['getArticleById'])
        }, {
          provide: TargetService,
          useValue: jasmine.createSpyObj('TargetService', ['getLinkTo'])
        }],
        declarations: [BlogArticleWidgetComponent]
      })
      .overrideTemplate(BlogArticleWidgetComponent, '')
      .compileComponents();

    targetService = TestBed.get(TargetService);
    blogArticleWidgetService = TestBed.get(BlogArticleWidgetService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogArticleWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {
        _articleId: 'any-article-id'
      },
      article: undefined,
      tempId: 'id'
    } as unknown as unknown as BlogArticleWidget;
    singleBlogArticle.id = 'any-article-id';
    singleBlogArticle.title = 'any-article-title';
    singleBlogArticle.teaserText = 'any-article-teaser-text';
    component.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve article when setting _articleId is given', () => {
    // given
    blogArticleWidgetService.getArticleById.and.returnValue(of(singleBlogArticle));

    // when
    component.ngOnInit();

    // then
    component.article$.subscribe();
    expect(blogArticleWidgetService.getArticleById).toHaveBeenCalledWith(singleBlogArticle.id);
  });

  it('should retrieve article when setting is changed', () => {
    // given
    const articleId = 'new-article-id';
    blogArticleWidgetService.getArticleById.and.returnValue(of(singleBlogArticle));

    // when
    component.ngOnInit();
    component.article$.pipe(skip(1)).subscribe();
    component.ngOnChanges({
      widget: {
        currentValue: {
          settings: {
            _articleId: articleId
          }
        }
      } as SimpleChange
    });

    // then
    expect(blogArticleWidgetService.getArticleById).toHaveBeenCalledWith(articleId);
  });
});
