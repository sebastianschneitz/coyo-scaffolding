import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {BlogArticleWidget, SingleBlogArticle} from '@widgets/blog-article/blog-article-widget';
import {BlogArticleWidgetService} from '@widgets/blog-article/blog-article-widget.service';
import * as _ from 'lodash';
import {of} from 'rxjs';
import {BlogArticleWidgetSettingsComponent} from './blog-article-widget-settings.component';

function generateArticles(amount: number): SingleBlogArticle[] {
  const articles: SingleBlogArticle[] = [];
  for (let i = 0; i < amount; i++) {
    articles.push(generateArticle(_.toString(i)));
  }
  return articles;
}

function generateArticle(nameId: string): SingleBlogArticle {
  const article: SingleBlogArticle = new SingleBlogArticle();
  article.id = nameId;
  article.title = nameId;
  return article;
}

describe('BlogArticleWidgetSettingsComponent', () => {
  let component: BlogArticleWidgetSettingsComponent;
  let fixture: ComponentFixture<BlogArticleWidgetSettingsComponent>;
  const blogArticleWidgetService = jasmine.createSpyObj('BlogArticleWidgetService', ['getArticles']);
  const cd = jasmine.createSpyObj('ChangeDetectorRef', ['detectChanges']);
  const singleBlogArticle: SingleBlogArticle = generateArticle('any-article-id');
  const singleBlogArticle1: SingleBlogArticle = generateArticle('any-article-id1');
  const singleBlogArticle2: SingleBlogArticle = generateArticle('any-article-id2');
  const articles: SingleBlogArticle[] = [singleBlogArticle, singleBlogArticle1, singleBlogArticle2];
  const page: Page<SingleBlogArticle> = {
    content: articles,
    first: true,
    last: true,
    number: 0,
    numberOfElements: 3,
    size: 3,
    sort: null,
    totalElements: 3,
    totalPages: 1,
    empty: false
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlogArticleWidgetSettingsComponent],
      providers: [{
        provide: BlogArticleWidgetService,
        useValue: blogArticleWidgetService
      }, {
        provide: ChangeDetectorRef,
        useValue: cd
      }]
    }).overrideTemplate(BlogArticleWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogArticleWidgetSettingsComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update _articleId in the widget settings with the id of the given article', () => {
    // given
    const blogArticle: SingleBlogArticle = generateArticle('any-other-article-id');
    const inputParentMock = jasmine.createSpyObj<HTMLElement>('HTMLElement', ['focus']);
    inputParentMock.focus.and.callFake(() => {
      return;
    });
    component.inputParent = {nativeElement: inputParentMock};
    component.widget = {
      id: 'widget-id',
      settings: {
        _articleId: 'any-article-id'
      },
      article: undefined,
      tempId: 'id'
    } as unknown as unknown as BlogArticleWidget;

    // when
    component.updateArticle(blogArticle);

    // then
    expect(component.widget.settings._articleId).toBe(blogArticle.id);
    expect(component.inputParent.nativeElement.focus).toHaveBeenCalled();
  });

  it('should calle next/first article page when component initialize', fakeAsync(() => {
    // given before each
    blogArticleWidgetService.getArticles.and.returnValue(of(page));

    // when
    component.ngOnInit();
    tick();

    // then
    expect(blogArticleWidgetService.getArticles)
      .toHaveBeenCalledWith(new Pageable(0, 20), '');
    expect(component.articles).toEqual(articles);
    expect(component.loading).toBeFalsy();
  }));

  it('should load articles on open article dropdown', fakeAsync(() => {
    // given before each
    blogArticleWidgetService.getArticles.and.returnValue(of(page));

    // when
    component.onOpen();
    tick();

    // then
    expect(blogArticleWidgetService.getArticles)
      .toHaveBeenCalledWith(new Pageable(0, 20, null), '');
    expect(component.articles).toEqual(articles);
    expect(component.loading).toBeFalsy();
  }));

  it('should call loadNextArticle when search field are cleared and search retrieve article page',
    fakeAsync(() => {
      // given before each
      component.articles = articles;
      const article: SingleBlogArticle = generateArticle('any-article-id-new');
      const resultArticles: SingleBlogArticle[] = [article];
      component.loading = false;
      page.content = resultArticles;
      blogArticleWidgetService.getArticles.and.returnValue(of(page));

      // when
      component.clear(true);
      tick();

      // then
      expect(blogArticleWidgetService.getArticles)
        .toHaveBeenCalledWith(new Pageable(0, 20, null), '');
      expect(component.articles).toEqual(resultArticles);
      expect(component.loading).toBeFalsy();
    }));

  it('should call loadNextArticles with searchTerm when something is enterd into search field ', fakeAsync(() => {
    // given before each
    component.articles = articles;
    component.term = 'any-article-title-searched';
    component.loading = false;
    const searchedArticle: SingleBlogArticle = generateArticle('any-article-id-searched');
    const resultArticles: SingleBlogArticle[] = [searchedArticle];
    page.content = resultArticles;
    blogArticleWidgetService.getArticles.and.returnValue(of(page));
    spyOn(component.termEvent, 'asObservable').and.returnValue(of('any-article-title-searched'));

    // when
    component.search({term: 'any-article-title-searched', items: component.articles});
    tick(300);

    // then
    expect(blogArticleWidgetService.getArticles)
      .toHaveBeenCalledWith(new Pageable(0, 20),
        'any-article-title-searched');
    expect(component.articles).toEqual(resultArticles);
    expect(component.loading).toBeFalsy();
  }));

  it('should not call loadNextArticles when bufferSize is greater than size of articles loaded', () => {
    // given before each
    component.preFetchedArticlesSize = 20;
    component.articles = articles;
    component.loading = false;

    // when
    component.onScroll({end: 10});

    // then
    expect(component.articles).toEqual(articles);
    expect(component.loading).toBeFalsy();
  });

  it('should call loadNextArticles when bufferSize is smaller than size of articles loaded ' +
    'and the end plus articles size greater than the bufferSize', () => {
    // given before each
    component.preFetchedArticlesSize = 2;
    component.articles = articles;
    component.loading = false;
    const manyArticles: SingleBlogArticle[] = generateArticles(40);
    const resultArticles = _.union(component.articles, manyArticles);
    page.content = manyArticles;
    blogArticleWidgetService.getArticles.and.returnValue(of(page));

    // when
    component.onScroll({end: 10});

    // then
    expect(component.articles).toEqual(resultArticles);
    expect(component.loading).toBeFalsy();
  });
});
