import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {DownloadWidget} from '@widgets/download/download-widget';
import {DownloadWidgetFile} from '@widgets/download/download-widget-file';
import {of} from 'rxjs';
import {DownloadWidgetSettingsComponent} from './download-widget-settings.component';

describe('DownloadWidgetSettingsComponent', () => {
  let component: DownloadWidgetSettingsComponent;
  let fixture: ComponentFixture<DownloadWidgetSettingsComponent>;
  let senderService: jasmine.SpyObj<SenderService>;

  const file1 = {
    id: '1'
  } as DownloadWidgetFile;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [DownloadWidgetSettingsComponent],
        providers: [{
          provide: SenderService, useValue: jasmine.createSpyObj('senderService', ['get', 'getCurrentIdOrSlug'])
        }]
      })
      .overrideTemplate(DownloadWidgetSettingsComponent, '')
      .compileComponents();

    senderService = TestBed.get(SenderService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {
        _files: [file1]
      }
    } as DownloadWidget;
    component.parentForm = new FormGroup({});
  });

  it('should create', () => {
    // given
    const slug = 'slud';
    const currentSender = {id: 'sender-id'} as Sender;

    senderService.getCurrentIdOrSlug.and.returnValue(slug);
    senderService.get.and.returnValue(of(currentSender));

    // when
    fixture.detectChanges();

    // then
    let called = false;
    component.sender$.subscribe(sender => {
      expect(sender).toBe(currentSender);
      called = true;
    });

    expect(called).toBeTruthy();
    expect(component).toBeTruthy();
    expect(senderService.get).toHaveBeenCalledWith(slug, {permissions: ['createFile']});
    expect(component.parentForm.getRawValue()).toEqual({_files: [file1]});
  });

  it('should remove unnecessary fields from settings', () => {
    // given
    const file = {id: 'id', senderId: 'senderId', title: 'title', name: 'name'};
    const titles = ['title-1'];
    const settings = {
      _titles: titles,
      _files: [{...file, unusedField: 'unused'} as DownloadWidgetFile]
    };

    // when
    const result = component.onBeforeSave(settings);

    // then
    let called = false;
    result.subscribe(resultSettings => {
      expect(resultSettings._files).toEqual([file]);
      expect(resultSettings._titles).toEqual(titles);
      called = true;
    });
    expect(called).toBeTruthy();
  });
});
