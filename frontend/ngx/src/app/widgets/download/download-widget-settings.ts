import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {DownloadWidgetFile} from '@widgets/download/download-widget-file';

/**
 * The entity model for the settings of a download widget
 */
export interface DownloadWidgetSettings extends WidgetSettings {
  _files: DownloadWidgetFile[];
  _titles: string[];
}
