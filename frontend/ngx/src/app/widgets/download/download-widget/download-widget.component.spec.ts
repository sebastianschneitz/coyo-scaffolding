import {ChangeDetectorRef, SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DocumentService} from '@domain/file/document/document.service';
import {DownloadWidget} from '@widgets/download/download-widget';
import {DownloadWidgetFile} from '@widgets/download/download-widget-file';
import {DownloadWidgetService} from '@widgets/download/download-widget/download-widget.service';
import {of} from 'rxjs';
import {first, skip} from 'rxjs/operators';
import {DownloadWidgetComponent} from './download-widget.component';

describe('DownloadWidgetComponent', () => {
  let component: DownloadWidgetComponent;
  let fixture: ComponentFixture<DownloadWidgetComponent>;
  let documentService: jasmine.SpyObj<DocumentService>;
  let downloadWidgetService: jasmine.SpyObj<DownloadWidgetService>;

  const file1 = {
    id: '1',
    title: 'az'
  } as DownloadWidgetFile;

  const file2 = {
    id: '2',
    title: 'a'
  } as DownloadWidgetFile;

  const file3 = {
    id: '3',
    name: 'ab'
  } as DownloadWidgetFile;

  const settingsFile1 = {
    id: '1',
    senderId: 'sender-id1'
  } as DownloadWidgetFile;

  const settingsFile2 = {
    id: '2',
    senderId: 'sender-id2'
  } as DownloadWidgetFile;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [DownloadWidgetComponent],
        providers: [{
          provide: DownloadWidgetService,
          useValue: jasmine.createSpyObj('downloadWidgetService', ['getAccessibleFiles'])
        }, {
          provide: DocumentService,
          useValue: jasmine.createSpyObj('documentService', ['getDownloadUrl'])
        }, ChangeDetectorRef]
      }).overrideTemplate(DownloadWidgetComponent, '')
      .compileComponents();

    documentService = TestBed.get(DocumentService);
    downloadWidgetService = TestBed.get(DownloadWidgetService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id'
    } as DownloadWidget;

  });

  it('should create', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    let called = false;
    component.state$.pipe(first()).subscribe(state => {
      expect(state.loading).toBeTruthy();
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should show accessible files in default mode', () => {
    // given
    downloadWidgetService.getAccessibleFiles.and.returnValue(of([file1, file2, file3]));

    // when
    fixture.detectChanges();

    // then
    let called = false;
    component.state$.pipe(skip(1)).subscribe(state => {
      expect(state.files).toEqual([file2, file3, file1]);
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should merge accessible files with setting on edit view when widget is already saved', () => {
    // given
    component.editMode = true;
    component.widget = {
      id: 'id',
      settings: {
        _files: [settingsFile1, settingsFile2]
      }
    } as DownloadWidget;

    downloadWidgetService.getAccessibleFiles.and.returnValue(of([file1, file2]));

    // when
    fixture.detectChanges();

    // then
    let called = false;
    component.state$.pipe(skip(1)).subscribe(state => {
      expect(state.files).toEqual([{...settingsFile2, ...file2}, {...settingsFile1, ...file1}]);
      called = true;
    });
    expect(called).toBeTruthy();
  });

  it('should use settings files if widget was not saved yet', () => {
    // given
    component.editMode = true;
    component.widget = {
      settings: {
        _files: [settingsFile1, settingsFile2]
      }
    } as DownloadWidget;

    // when
    fixture.detectChanges();

    // then
    let called = false;
    component.state$.pipe(skip(1)).subscribe(state => {
      expect(state.files).toEqual([settingsFile2, settingsFile1]);
      called = true;
    });
    expect(called).toBeTruthy();
    expect(downloadWidgetService.getAccessibleFiles).not.toHaveBeenCalled();
  });

  it('should reload files on widget changes', () => {
    // given
    downloadWidgetService.getAccessibleFiles.and.returnValues(of([]), of([file1, file2]));

    let called = false;

    fixture.detectChanges();

    component.state$.pipe(skip(2)).subscribe(state => {
      // then
      expect(state.files).toEqual([file2, file1]);
      called = true;
    });

    downloadWidgetService.getAccessibleFiles.calls.reset();

    // when
    component.ngOnChanges({
      widget: {
        currentValue: {
          id: 'test',
          settings: {
            _files: [file1, file2]
          }
        },
        previousValue: {
          id: 'test',
          settings: {
            _files: []
          }
        }
      } as SimpleChange
    });

    // then
    expect(called).toBeTruthy();
    expect(downloadWidgetService.getAccessibleFiles).toHaveBeenCalledTimes(1);
  });

  it('should reload files on edit mode changes', () => {
    // given
    downloadWidgetService.getAccessibleFiles.and.returnValues(of([]), of([file1, file2]));

    let called = false;

    fixture.detectChanges();

    component.state$.pipe(skip(2)).subscribe(state => {
      // then
      expect(state.files).toEqual([file2, file1]);
      called = true;
    });

    downloadWidgetService.getAccessibleFiles.calls.reset();

    // when
    component.ngOnChanges({editMode: {currentValue: false} as SimpleChange});

    // then
    expect(called).toBeTruthy();
    expect(downloadWidgetService.getAccessibleFiles).toHaveBeenCalledTimes(1);
  });

  it('should show file title in favor of file name', () => {
    // given
    const file = {
      title: 'title',
      name: 'name'
    } as DownloadWidgetFile;

    // when
    const result = component.getName(file);

    // then
    expect(result).toBe(file.title);
  });

  it('should show file name if title is not given', () => {
    // given
    const file = {
      name: 'name'
    } as DownloadWidgetFile;

    // when
    const result = component.getName(file);

    // then
    expect(result).toBe(file.name);
  });

  it('should return a download link based on the file id and sender', () => {
    // given
    const file = {
      id: 'id',
      senderId: 'sender-id'
    } as DownloadWidgetFile;

    const link = 'document-link';

    documentService.getDownloadUrl.and.returnValue(link);

    // when
    const result = component.getLink(file);

    // then
    expect(result).toBe(link);
    expect(documentService.getDownloadUrl).toHaveBeenCalledWith(file);
  });

  it('should set the title on input changes', () => {
    // given
    const newTitle = 'new title';

    const file = {
      title: 'title'
    } as DownloadWidgetFile;

    // when
    component.onTitleChange(newTitle, file);

    // then
    expect(file.title).toBe(newTitle);
  });

  it('should delete the title if its empty', () => {
    // given
    const file = {
      title: 'title'
    } as DownloadWidgetFile;

    // when
    component.onTitleChange('', file);

    // then
    expect(file.title).toBeUndefined();
  });
});
