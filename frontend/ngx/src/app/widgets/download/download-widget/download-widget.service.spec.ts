import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {DownloadWidgetFile} from '@widgets/download/download-widget-file';

import {DownloadWidgetService} from './download-widget.service';

describe('DownloadWidgetService', () => {
  let service: DownloadWidgetService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DownloadWidgetService]
    });

    service = TestBed.get(DownloadWidgetService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get accessible files', () => {
    // given
    const widgetId = 'widget-id';
    const file = {id: '1'} as DownloadWidgetFile;

    // when
    const result = service.getAccessibleFiles(widgetId);

    // then
    let called = false;
    result.subscribe(values => {
      expect(values).toEqual([file]);
      called = true;
    });

    const req = httpMock.expectOne(`/web/widgets/download/${widgetId}/files`);
    req.flush({accessibleFiles: [file]});

    expect(called).toBeTruthy();
    httpMock.verify();
  });
});
