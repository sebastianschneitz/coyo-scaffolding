/**
 * File abstraction for the download widget
 */
export interface DownloadWidgetFile {
  id: string;
  senderId: string;
  name: string;
  title?: string;
}
