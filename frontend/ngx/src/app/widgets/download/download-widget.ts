import {Widget} from '@domain/widget/widget';
import {DownloadWidgetSettings} from '@widgets/download/download-widget-settings';

export interface DownloadWidget extends Widget<DownloadWidgetSettings> {
}
