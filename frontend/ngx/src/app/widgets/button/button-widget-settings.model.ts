import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a button widget
 */
export interface ButtonWidgetSettings extends WidgetSettings {
  text: string;
  _url: string;
  _linkTarget: string;
  _button: {
    btnClass: string;
  };
}
