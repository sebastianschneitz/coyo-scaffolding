import {Widget} from '@domain/widget/widget';
import {ButtonWidgetSettings} from './button-widget-settings.model';

export interface ButtonWidget extends Widget<ButtonWidgetSettings> {
}
