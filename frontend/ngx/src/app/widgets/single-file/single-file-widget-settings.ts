import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export interface SingleFileWidgetSettings extends WidgetSettings {
  _fileId: string;
  _senderId: string;
  _hidePreview: boolean;
  title: string;
  _hideDate: boolean;
  _hideSender: boolean;
  _hideDownload: boolean;
}
