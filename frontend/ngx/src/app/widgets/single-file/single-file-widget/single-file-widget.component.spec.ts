import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DocumentService} from '@domain/file/document/document.service';
import {File} from '@domain/file/file.ts';
import {FileService} from '@domain/file/file/file.service';
import {FilePreview} from '@domain/preview/file-preview';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {Ng1fileDetailsModalService} from '@root/typings';
import {NG1_FILE_DETAILS_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {of, throwError} from 'rxjs';
import {SingleFileWidgetComponent} from './single-file-widget.component';
import SpyObj = jasmine.SpyObj;

describe('SingleFileWidgetComponent', () => {
  let component: SingleFileWidgetComponent;
  let fixture: ComponentFixture<SingleFileWidgetComponent>;
  let documentService: SpyObj<DocumentService>;
  let fileService: SpyObj<FileService>;
  let senderService: SpyObj<SenderService>;
  let fileDetailsModalService: jasmine.SpyObj<Ng1fileDetailsModalService>;

  const file: File = {
    id: 'file-id',
    created: 234,
    modified: 123213,
    senderId: 'sender-id',
    name: 'ladida',
    displayName: 'ladida',
    folder: 'folder',
    appRoot: 'appRoot',
    uploadFailed: false,
    contentType: 'image/png',
    extension: undefined,
    storage: 'LOCAL_BLOB_STORAGE'
  };

  const sender: Sender = {
    id: 'sender-id',
    created: 123,
    modified: 1234,
    displayName: 'Robert Lang',
    displayNameInitials: 'RL',
    color: '',
    slug: 'rl',
    typeName: '',
    target: {name: '', params: {}},
    public: true,
    _permissions: {},
    imageUrls: {avatar: ''}
  };
  const typeName = 'TYPE_NAME';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SingleFileWidgetComponent],
      providers: [{
        provide: DocumentService,
        useValue: jasmine.createSpyObj('documentService', ['getDownloadUrl'])
      }, {
        provide: FileService,
        useValue: jasmine.createSpyObj('fileService', ['getFile'])
      }, {
        provide: SenderService,
        useValue: jasmine.createSpyObj('senderService', ['get'])
      }, {
        provide: NG1_FILE_DETAILS_MODAL_SERVICE,
        useValue: jasmine.createSpyObj('fileDetailsModalService', ['open'])
      }]
    }).overrideTemplate(SingleFileWidgetComponent, '')
      .compileComponents();

    documentService = TestBed.get(DocumentService);
    fileService = TestBed.get(FileService);
    fileService.getFile.and.returnValue(of(file));
    senderService = TestBed.get(SenderService);
    senderService.get.and.returnValue(of(sender));
    fileDetailsModalService = TestBed.get(NG1_FILE_DETAILS_MODAL_SERVICE);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleFileWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'some-id',
      tempId: '',
      settings: {
        title: 'title',
        _fileId: 'file-id',
        _senderId: 'sender-id',
        _hideDate: false,
        _hideDownload: false,
        _hidePreview: false,
        _hideSender: false
      }
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve the file on init', done => {
    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(state => {
      expect(fileService.getFile)
        .toHaveBeenCalledWith(component.widget.settings._fileId, component.widget.settings._senderId);
      expect(state.file).toEqual(file);
      done();
    });
  });

  it('should retrieve the sender on init', done => {
    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(state => {
      expect(senderService.get).toHaveBeenCalledWith(component.widget.settings._senderId);
      expect(state.sender).toEqual(sender);
      done();
    });
  });

  it('should set the file preview on init', done => {
    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(state => {
      expect(state.filePreview).toBeDefined();
      expect(state.filePreview.id).toEqual(file.id);
      done();
    });
  });

  it('should get sender and file again when settings changing', done => {
    // given
    fixture.detectChanges();

    // when
    component.ngOnChanges();

    // then
    component.state$.subscribe(() => {
      expect(fileService.getFile).toHaveBeenCalled();
      expect(senderService.get).toHaveBeenCalled();
      done();
    });
  });

  it('should open file details modal', () => {
    // given
    fixture.detectChanges();

    // when
    component.showFileDetails({} as FilePreview);

    // then
    expect(fileDetailsModalService.open).toHaveBeenCalled();
  });

  it('should return download url', () => {
    // given
    documentService.getDownloadUrl.and.returnValue('url');
    fixture.detectChanges();

    // when
    const url = component.getDownloadUrl(file, sender);

    // then
    expect(documentService.getDownloadUrl).toHaveBeenCalled();
    expect(url).toEqual('url');
  });

  it('should not update state when file not found', done => {
    // given
    fileService.getFile.and.returnValue(throwError({status: 404}));

    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(state => {
      expect(state.file).toBeUndefined();
      expect(state.sender).toEqual(sender);
      expect(state.filePreview).toBeUndefined();
      done();
    });
  });
});
