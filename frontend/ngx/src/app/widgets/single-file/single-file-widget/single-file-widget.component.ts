import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnChanges, OnInit} from '@angular/core';
import {Document} from '@domain/file/document';
import {DocumentService} from '@domain/file/document/document.service';
import {File} from '@domain/file/file.ts';
import {FileService} from '@domain/file/file/file.service';
import {FilePreview} from '@domain/preview/file-preview';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {Ng1fileDetailsModalService} from '@root/typings';
import {NG1_FILE_DETAILS_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {WidgetComponent} from '@widgets/api/widget-component';
import {SingleFileWidget} from '@widgets/single-file/single-file-widget';
import {forkJoin, Observable, of, ReplaySubject} from 'rxjs';
import {catchError, map, switchMap, tap} from 'rxjs/operators';

interface SingleFileWidgetComponentState {
  file: File;
  sender: Sender;
  filePreview?: FilePreview;
}

@Component({
  selector: 'coyo-single-file-widget',
  templateUrl: './single-file-widget.component.html',
  styleUrls: ['./single-file-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SingleFileWidgetComponent extends WidgetComponent<SingleFileWidget> implements OnInit, OnChanges {

  constructor(cd: ChangeDetectorRef,
              private documentService: DocumentService,
              @Inject(NG1_FILE_DETAILS_MODAL_SERVICE) private fileDetailsModalService: Ng1fileDetailsModalService,
              private fileService: FileService,
              private senderService: SenderService) {
    super(cd);
  }

  state$: Observable<SingleFileWidgetComponentState>;
  previewUrl: string = '/web/senders/{{groupId}}/documents/{{id}}';

  private changeSubject$: ReplaySubject<any> = new ReplaySubject();

  ngOnInit(): void {
    this.state$ = this.changeSubject$
      .pipe(switchMap(() => forkJoin([
        this.fileService.getFile(this.widget.settings._fileId, this.widget.settings._senderId)
          .pipe(catchError(() => of(undefined))),
        this.senderService.get(this.widget.settings._senderId)
      ])))
      .pipe(
        map(([file, sender]) => ({file, sender})),
        tap((state: SingleFileWidgetComponentState) => {
          if (state.file) {
            state.filePreview = this.toFilePreview(state.file);
          }
        }));

    this.changeSubject$.next();
  }

  ngOnChanges(): void {
    this.changeSubject$.next();
  }

  getDownloadUrl(file: File, sender: Sender): string {
    return this.documentService.getDownloadUrl({id: file.id, senderId: sender.id} as Document);
  }

  showFileDetails(filePreview: FilePreview): void {
    this.fileDetailsModalService.open([filePreview]);
  }

  private toFilePreview(file: File): FilePreview {
    return {
      serialVersionUID: null,
      id: file.id,
      name: file.name,
      contentType: file.contentType,
      previewUrl: this.previewUrl,
      length: 1,
      created: new Date(file.created),
      modified: new Date(file.modified),
      storage: 'LOCAL_BLOB_STORAGE',
      downloadUrl: null,
      fileId: file.id,
      modelId: null,
      deleted: false,
      attachment: false,
      groupId: file.senderId,
      senderId: file.senderId
    };
  }
}
