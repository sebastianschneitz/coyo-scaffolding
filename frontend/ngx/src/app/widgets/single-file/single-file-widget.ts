import {Widget} from '@domain/widget/widget';
import {SingleFileWidgetSettings} from '@widgets/single-file/single-file-widget-settings';

export interface SingleFileWidget extends Widget<SingleFileWidgetSettings> {
}
