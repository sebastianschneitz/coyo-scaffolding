import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'coyo-single-file-widget-skeleton',
  template: '<div></div>',
  styleUrls: ['./single-file-widget-skeleton.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SingleFileWidgetSkeletonComponent {
}
