import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {DividerWidgetSettingsComponent} from './divider-widget-settings.component';

describe('DividerWidgetSettingsComponent', () => {
  let component: DividerWidgetSettingsComponent;
  let fixture: ComponentFixture<DividerWidgetSettingsComponent>;

  let parentForm: FormGroup;
  let widget: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DividerWidgetSettingsComponent ]
    }).overrideTemplate(DividerWidgetSettingsComponent, '')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DividerWidgetSettingsComponent);
    component = fixture.componentInstance;
    widget = {settings: {}};
    parentForm = new FormGroup({});
    component.widget = widget;
    component.parentForm = parentForm;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
