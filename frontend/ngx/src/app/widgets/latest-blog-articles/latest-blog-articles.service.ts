import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {LatestBlogArticle} from '@widgets/latest-blog-articles/latest-blog-article';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
/**
 * Blog article widget service
 * Fetches a list of latest blog articles
 */
export class LatestBlogArticlesService {
  static readonly DEFAULT_COUNT: number = 5;
  private readonly url: string = '/web/widgets/blog/latest';

  constructor(@Inject(HttpClient) private http: HttpClient) {
  }

  getLatestBlogArticles(
    sourceSelection: 'ALL' | 'SUBSCRIBED' | 'SELECTED',
    count: number = LatestBlogArticlesService.DEFAULT_COUNT,
    appIds?: string[]): Observable<LatestBlogArticle[]> {
    return this.http.post<LatestBlogArticle[]>(this.url,
      {appIds, count, sourceSelection});
  }
}
