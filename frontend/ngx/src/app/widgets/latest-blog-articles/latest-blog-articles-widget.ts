import {Widget} from '@domain/widget/widget';
import {LatestBlogArticlesWidgetSettings} from '@widgets/latest-blog-articles/latest-blog-articles-widget-settings';

export interface LatestBlogArticlesWidget extends Widget<LatestBlogArticlesWidgetSettings> {
}
