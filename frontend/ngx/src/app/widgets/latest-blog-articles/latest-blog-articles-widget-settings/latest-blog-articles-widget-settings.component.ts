import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {App} from '@domain/apps/app';
import {AppService} from '@domain/apps/app.service';
import {CoyoValidators} from '@shared/forms/validators/validators';
import {WidgetSettingsComponent} from '@widgets/api/widget-settings-component';
import {LatestBlogArticlesWidget} from '@widgets/latest-blog-articles/latest-blog-articles-widget';
import {
  CompatibilityAppModel,
  LatestBlogArticlesWidgetSettings
} from '@widgets/latest-blog-articles/latest-blog-articles-widget-settings';
import * as _ from 'lodash';
import {forkJoin, Observable, of} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

/**
 * The latest-blog-articles widget settings component.
 */
@Component({
  selector: 'coyo-latest-blog-articles-widget-settings',
  templateUrl: './latest-blog-articles-widget-settings.component.html',
  styleUrls: ['./latest-blog-articles-widget-settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LatestBlogArticlesWidgetSettingsComponent extends WidgetSettingsComponent<LatestBlogArticlesWidget>
  implements OnInit {

  isSourceSelection$: Observable<boolean>;

  constructor(private appService: AppService) {
    super();
  }

  ngOnInit(): void {
    const selectedApps = _.get(this.widget.settings, '_app', null);
    const sourceSelection = _.get(this.widget.settings, '_sourceSelection', 'ALL');
    const articleCount = _.get(this.widget.settings, '_articleCount', 5);
    const showTeaserImage = _.get(this.widget.settings, '_showTeaserImage', false);
    this.parentForm.addControl('apps', new FormControl([]));
    this.parentForm.addControl('showTeaserImage', new FormControl(showTeaserImage));
    this.parentForm.addControl('sourceSelection', new FormControl(sourceSelection));
    this.parentForm.addControl('articleCount', new FormControl(articleCount,
      [Validators.required,
        Validators.min(1),
        Validators.max(20),
        CoyoValidators.isWholeNumber]));
    this.setupSourceSelectionObservable(sourceSelection);
    this.loadSelectedApps(selectedApps);
  }

  /**
   * Transforms the form data to settings data
   * @param settings The settings stored in the form
   * @returns An observable emitting the transformed save data
   */
  onBeforeSave(settings?: any): Observable<LatestBlogArticlesWidgetSettings> {
    const selectedApps = settings.apps as App[];
    let apps: CompatibilityAppModel[] = [];
    if (selectedApps && selectedApps.length > 0) {
      /*tslint:disable:arrow-return-shorthand*/
      apps = selectedApps.map(app => {
        return {id: app.id, senderId: app.senderId};
      });
    } else {
      settings.sourceSelection = 'ALL';
    }
    return of({
      _app: apps,
      _articleCount: settings.articleCount,
      _sourceSelection: settings.sourceSelection,
      _showTeaserImage: settings.showTeaserImage
    });
  }

  private setupSourceSelectionObservable(currentValue: string): void {
    this.isSourceSelection$ = this.parentForm
      .get('sourceSelection')
      .valueChanges
      .pipe(map(value => value === 'SELECTED'), startWith(currentValue === 'SELECTED'));
  }

  private loadSelectedApps(selectedApps: CompatibilityAppModel[]): void {
    if (!!selectedApps && !!selectedApps.length) {
      const appResults: Observable<App>[] = selectedApps.map((app: CompatibilityAppModel) =>
        this.appService.get(app.id, {context: {senderId: app.senderId}}));
      forkJoin(appResults).subscribe(apps => {
        this.parentForm.patchValue({apps});
      });
    }
  }
}
