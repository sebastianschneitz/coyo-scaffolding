import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export interface CompatibilityAppModel {
  id: string;
  senderId: string;
}

/**
 * The entity model for the settings of a latest-blog-articles widget
 */
export interface LatestBlogArticlesWidgetSettings extends WidgetSettings {
  _sourceSelection: 'ALL' | 'SUBSCRIBED' | 'SELECTED';
  _articleCount: number;
  _showTeaserImage: boolean;
  _app: CompatibilityAppModel[];
}
