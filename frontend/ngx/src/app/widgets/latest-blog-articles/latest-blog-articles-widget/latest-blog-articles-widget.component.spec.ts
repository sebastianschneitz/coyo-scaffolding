import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngxs/store';
import {LatestBlogArticle} from '@widgets/latest-blog-articles/latest-blog-article';
import {LatestBlogArticlesWidget} from '@widgets/latest-blog-articles/latest-blog-articles-widget';
import {Load} from '@widgets/latest-blog-articles/latest-blog-articles-widget/latest-blog-articles-widget.actions';
import {LatestBlogArticlesWidgetStateModel} from '@widgets/latest-blog-articles/latest-blog-articles-widget/latest-blog-articles-widget.state';
import * as _ from 'lodash';
import {of} from 'rxjs';
import {LatestBlogArticlesWidgetComponent} from './latest-blog-articles-widget.component';

describe('LatestBlogArticlesWidgetComponent', () => {
  let component: LatestBlogArticlesWidgetComponent;
  let fixture: ComponentFixture<LatestBlogArticlesWidgetComponent>;
  let store: jasmine.SpyObj<Store>;
  const ID = 'widget-id';
  const testChange =
    (previousValue: any,
     currentValue: any) => {
      // given
      const previousSettings = _.merge(component.widget.settings, previousValue);
      component.widget.settings = previousSettings;

      // when
      fixture.detectChanges();
      component.ngOnChanges({
        widget: {
          previousValue,
          currentValue,
          firstChange: !previousValue,
          isFirstChange: null
        }
      });

      // then
      expect(store.dispatch).toHaveBeenCalledWith(new Load(ID, previousSettings));
      expect(store.dispatch).toHaveBeenCalledWith(new Load(ID, _.merge(previousSettings, currentValue)));
      expect(store.dispatch).toHaveBeenCalledTimes(2);
    };
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [LatestBlogArticlesWidgetComponent],
        providers: [ChangeDetectorRef, {
          provide: Store,
          useValue: jasmine.createSpyObj('store', ['select', 'dispatch'])
        }]
      }).overrideTemplate(LatestBlogArticlesWidgetComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(LatestBlogArticlesWidgetComponent);
    store = TestBed.get(Store);
    component = fixture.componentInstance;
    component.widget = {
      id: ID,
      tempId: 'temp' + ID,
      settings: {
        _app: null,
        _sourceSelection: 'ALL',
        _showTeaserImage: false,
        _articleCount: 3
      },
    } as LatestBlogArticlesWidget;
  }));

  it('should create', () => {
    // given
    const state = {latestBlogArticles: {[ID]: 'state'}};
    const model: LatestBlogArticlesWidgetStateModel = {
      loading: false,
      hasArticles: true,
      articles: ['a' as any as LatestBlogArticle],
      links: {
        articles: {a: 'article_link_a'},
        senders: {a: 'sender_link_a'}
      }
    };
    let expectedStateSelectResult = null;
    store.select.and.callFake((callback: any) => {
      expectedStateSelectResult = callback(state);
      return of(model);
    });

    // when
    fixture.detectChanges();

    // then
    expect(expectedStateSelectResult).toEqual('state');
    expect(component).toBeTruthy();
    expect(store.select).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalledWith(new Load(ID, component.widget.settings));
  });

  it('should refresh when app id changes', () => {
    testChange({settings: {_app: {appId: 'a'}}}, {settings: {_app: {appId: 'b'}}});
  });

  it('should refresh when sender id changes', () => {
    testChange({settings: {_app: {senderId: 'a'}}}, {settings: {_app: {senderId: 'b'}}});
  });

  it('should refresh when article count changes', () => {
    testChange({settings: {_articleCount: 4}}, {settings: {_articleCount: 5}});
  });

  it('should refresh when no settings were set', () => {
    const settings = component.widget.settings;
    component.widget.settings = undefined;
    testChange({settings: undefined}, {settings});
  });

  it('should not refresh when no settings were altered', () => {
    // when
    fixture.detectChanges();
    component.ngOnChanges({
      widget: {
        previousValue: {settings: component.widget.settings},
        currentValue: {settings: component.widget.settings},
        firstChange: false,
        isFirstChange: null
      }
    });

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new Load(ID, component.widget.settings));
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

  it('should destroy', () => {
    // when
    fixture.detectChanges();
    component.ngOnDestroy();

    // then
    expect(store.dispatch).toHaveBeenCalled();
  });
});
