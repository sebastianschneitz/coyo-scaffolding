import {async, TestBed} from '@angular/core/testing';
import {Target} from '@domain/sender/target';
import {TargetService} from '@domain/sender/target/target.service';
import {NgxsModule, Store} from '@ngxs/store';
import {LatestBlogArticle} from '@widgets/latest-blog-articles/latest-blog-article';
import {
  Load,
  Reset
} from '@widgets/latest-blog-articles/latest-blog-articles-widget/latest-blog-articles-widget.actions';
import {
  LatestBlogArticlesWidgetState,
  LatestBlogArticlesWidgetStateModel
} from '@widgets/latest-blog-articles/latest-blog-articles-widget/latest-blog-articles-widget.state';
import {LatestBlogArticlesService} from '@widgets/latest-blog-articles/latest-blog-articles.service';
import {of} from 'rxjs';

describe('LatestBlogArticlesWidgetState', () => {
  let store: Store;
  let latestBlogArticlesService: jasmine.SpyObj<LatestBlogArticlesService>;
  let targetService: jasmine.SpyObj<TargetService>;
  const articles = [
    {senderTarget: {params: {id: 's1'}}, articleTarget: {params: {id: 'a1'}}},
    {senderTarget: {params: {id: 's2'}}, articleTarget: {params: {id: 'a2'}}},
    {senderTarget: {params: {id: 's3'}}, articleTarget: {params: {id: 'a3'}}}] as any as LatestBlogArticle[];
  const ID = 'id';
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([LatestBlogArticlesWidgetState])],
      providers: [{
        provide: LatestBlogArticlesService,
        useValue: jasmine.createSpyObj('latestBlogArticlesService', ['getLatestBlogArticles'])
      },
        {
          provide: TargetService,
          useValue: jasmine.createSpyObj('targetService', ['getLinkTo'])
        }]
    });
    store = TestBed.get(Store);
    latestBlogArticlesService = TestBed.get(LatestBlogArticlesService);
    targetService = TestBed.get(TargetService);
  }));

  it('should init latest blog articles state', () => {
    // given
    latestBlogArticlesService.getLatestBlogArticles.and.returnValue(of(articles));
    targetService.getLinkTo.and.callFake((target: Target) => target.params.id + '_link');
    // when
    store.dispatch(new Load(ID, {
      _app: [{id: 'app', senderId: 'senderId'}],
      _articleCount: 1,
      _showTeaserImage: true,
      _sourceSelection: 'ALL'
    }));
    // then
    const actual: LatestBlogArticlesWidgetStateModel = store.selectSnapshot((state => state.latestBlogArticles));
    expect(actual[ID]).toBeDefined();
    expect(actual[ID].articles).toEqual(articles);
    expect(actual[ID].links.articles).toEqual(Object.values(articles).reduce((prev: any, curr: LatestBlogArticle) => {
      prev[curr.articleTarget.params.id] = curr.articleTarget.params.id + '_link';
      return prev;
    }, {} as any));
    expect(actual[ID].links.senders).toEqual(Object.values(articles).reduce((prev: any, curr: LatestBlogArticle) => {
      prev[curr.senderTarget.params.id] = curr.senderTarget.params.id + '_link';
      return prev;
    }, {} as any));
    expect(actual[ID].loading).toBeFalsy();
  });
  it('should reset the latest blog article state', () => {
    // given
    store.dispatch(new Reset(ID));
    // then
    const actual = store.selectSnapshot((state => state.latestBlogArticles));
    expect(actual[ID]).toBeUndefined();
  });
});
