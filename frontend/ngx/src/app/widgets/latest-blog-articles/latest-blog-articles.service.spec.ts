import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {LatestBlogArticlesService} from '@widgets/latest-blog-articles/latest-blog-articles.service';
import {NewColleaguesService} from '@widgets/new-colleagues/new-colleagues.service';

describe('LatestBlogArticlesService', () => {
  let service: LatestBlogArticlesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [NewColleaguesService]
    });
    service = TestBed.get(LatestBlogArticlesService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should get latest blog articles of a given app', () => {
    // given
    const response: any[] = [];
    const count = LatestBlogArticlesService.DEFAULT_COUNT + 1;
    const appId = 'app-id';
    const selection = 'ALL';
    // when
    const result = service.getLatestBlogArticles(selection, count, [appId]);
    let called = false;
    result.subscribe(res => {
      called = true;
      expect(res).toBe(response);
    });
    // then

    const request = httpMock
      .expectOne(`/web/widgets/blog/latest`);
    expect(request.request.body).toEqual({appIds: [appId], count, sourceSelection: selection});
    request.flush(response);
    expect(called).toBeTruthy();
  });

  it('should get latest blog articles for multiple apps', () => {
    // given
    const response: any[] = [];
    const count = LatestBlogArticlesService.DEFAULT_COUNT + 1;
    const appIds = ['app-id1', 'app-id2', 'app-id3', 'app-id4'];
    const selection = 'ALL';
    // when
    const result = service.getLatestBlogArticles(selection, count, appIds);
    let called = false;
    result.subscribe(res => {
      called = true;
      expect(res).toBe(response);
    });
    // then
    const request = httpMock
      .expectOne(`/web/widgets/blog/latest`);
    expect(request.request.body).toEqual({appIds: appIds, count, sourceSelection: selection});
    request.flush(response);
    expect(called).toBeTruthy();
  });

  it('should get latest blog articles for all apps', () => {
    // given
    const response: any[] = [];
    const count = LatestBlogArticlesService.DEFAULT_COUNT + 1;
    const selection = 'ALL';
    // when
    const result = service.getLatestBlogArticles(selection, count);
    let called = false;
    result.subscribe(res => {
      called = true;
      expect(res).toBe(response);
    });
    // then
    const request = httpMock
      .expectOne(`/web/widgets/blog/latest`);
    expect(request.request.body).toEqual({appIds: undefined, count, sourceSelection: selection});
    request.flush(response);
    expect(called).toBeTruthy();
  });

  it('should use the default number of articles', () => {
    // given
    const response: any[] = [];
    const count: any = undefined;
    const appId = 'app-id';
    const selection = 'SELECTED';
    // when
    const result = service.getLatestBlogArticles(selection, count, [appId]);
    let called = false;
    result.subscribe(res => {
      called = true;
      expect(res).toBe(response);
    });
    // then
    const request = httpMock
      .expectOne(`/web/widgets/blog/latest`);
    expect(request.request.body).toEqual({
      appIds: [appId],
      count: LatestBlogArticlesService.DEFAULT_COUNT,
      sourceSelection: selection
    });
    request.flush(response);
    expect(called).toBeTruthy();
  });
});
