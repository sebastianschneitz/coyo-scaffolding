import {Target} from '@domain/sender/target';

/**
 * DTO for latest-blog-article responses
 */
export interface LatestBlogArticle {
  title: string;
  senderName: string;
  created: Date;
  updated: Date;
  articleTarget: Target;
  senderTarget: Target;
  teaserImageWideFileId: string;
  teaserImageWideSenderId: string;
}
