import {SuggestPagesWidgetSettings} from '@widgets/suggest-pages/suggest-pages-widget-settings';

export class Load {

  static readonly type: string = '[SuggestPagesWidget] Load';

  constructor(public settings: SuggestPagesWidgetSettings, public id: string) {
  }
}
