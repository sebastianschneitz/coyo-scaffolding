import {Widget} from '@domain/widget/widget';
import {SuggestPagesWidgetSettings} from '@widgets/suggest-pages/suggest-pages-widget-settings';

export interface SuggestPagesWidget extends Widget<SuggestPagesWidgetSettings> {
}
