import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export interface UpcomingEventsWidgetSettings extends WidgetSettings {
  _fetchUpcomingEvents: 'SELECTED' | 'SUBSCRIBED' | 'ALL';
  _displayOngoingEvents: boolean;
  _numberOfDisplayedEvents: number;
  _sender: string;
  _titles: string[];
}
