import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {UpcomingEventsWidgetSettings} from '@widgets/upcoming-events/upcoming-events-widget-settings.model';
import {UpcomingEventsWidgetSettingsComponent} from '@widgets/upcoming-events/upcoming-events-widget-settings/upcoming-events-widget-settings.component';
import {of} from 'rxjs';

describe('UpcomingEventsWidgetSettingsComponent', () => {
  let component: UpcomingEventsWidgetSettingsComponent;
  let fixture: ComponentFixture<UpcomingEventsWidgetSettingsComponent>;
  let senderService: jasmine.SpyObj<SenderService>;
  let sender: Sender;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [UpcomingEventsWidgetSettingsComponent],
      providers: [{
        provide: SenderService,
        useValue: jasmine.createSpyObj('SenderService', ['get'])
      }]
    }).overrideTemplate(UpcomingEventsWidgetSettingsComponent, '')
      .compileComponents();

    sender = {id: 'test-event-sender'} as Sender;
    senderService = TestBed.get(SenderService);
    senderService.get.and.returnValue(of(sender));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingEventsWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.parentForm = new FormGroup({});
    component.widget = {
      id: 'widget-id',
      tempId: 'temp-id',
      settings: {
        _fetchUpcomingEvents: 'SELECTED',
        _displayOngoingEvents: true,
        _numberOfDisplayedEvents: 5,
        _sender: 'test-event-sender',
        _titles: ['title']
      }
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create controls with default values', () => {
    // given
    component.widget.settings = {} as UpcomingEventsWidgetSettings;

    // when
    fixture.detectChanges();

    // then
    expect(component.parentForm.value._fetchUpcomingEvents).toEqual('ALL');
    expect(component.parentForm.value._numberOfDisplayedEvents).toEqual(5);
    expect(component.parentForm.value._displayOngoingEvents).toEqual(false);
    expect(component.parentForm.value._selectedSender).toEqual(null);
    expect(senderService.get).not.toHaveBeenCalled();
  });

  it('should create controls with given values', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component.parentForm.value._fetchUpcomingEvents).toEqual('SELECTED');
    expect(component.parentForm.value._numberOfDisplayedEvents).toEqual(5);
    expect(component.parentForm.value._displayOngoingEvents).toEqual(true);
    expect(component.parentForm.value._selectedSender).toEqual(sender);
    expect(senderService.get).toHaveBeenCalledWith('test-event-sender');
  });

  it('should transform settings onBeforeSave', done => {
    // given
    fixture.detectChanges();

    // when
    component.onBeforeSave({
      _fetchUpcomingEvents: 'SELECTED',
      _displayOngoingEvents: true,
      _numberOfDisplayedEvents: 5,
      _sender: 'test-event-sender-old',
      _selectedSender: {id: 'test-event-sender'} as Sender,
      _titles: ['title']
    }).subscribe(settings => {
      // then
      expect(settings['_selectedSender']).toBeUndefined();
      expect(settings._sender).toEqual('test-event-sender');
      done();
    });
  });
});
