import {ToArrayPipe} from './as-array.pipe';

describe('ToArrayPipe', () => {
  let pipe: ToArrayPipe;

  beforeEach(() => {
    pipe = new ToArrayPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('convert a number to an array', () => {
    expect(pipe.transform(5)).toEqual([0, 1, 2, 3, 4]);
  });
});
