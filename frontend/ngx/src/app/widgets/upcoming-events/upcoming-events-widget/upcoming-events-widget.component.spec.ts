import {SimpleChanges} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EventService} from '@domain/event/event.service';
import {of} from 'rxjs';
import {UpcomingEventsWidgetComponent} from './upcoming-events-widget.component';
import SpyObj = jasmine.SpyObj;

describe('UpcomingEventsWidgetComponent', () => {
  let component: UpcomingEventsWidgetComponent;
  let fixture: ComponentFixture<UpcomingEventsWidgetComponent>;
  let eventService: SpyObj<EventService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpcomingEventsWidgetComponent],
      providers: [{
        provide: EventService,
        useValue: jasmine.createSpyObj('EventService', ['getUpcomingEvents'])
      }]
    }).overrideTemplate(UpcomingEventsWidgetComponent, '')
      .compileComponents();

    eventService = TestBed.get(EventService);
    eventService.getUpcomingEvents.and.returnValue(of([{}, {}, {}]));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingEventsWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      tempId: 'temp-id',
      settings: {
        _fetchUpcomingEvents: 'ALL',
        _displayOngoingEvents: false,
        _numberOfDisplayedEvents: 5,
        _sender: null,
        _titles: ['title']
      }
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load events onInit', done => {
    // when
    fixture.detectChanges();

    // then
    component.events$.subscribe(events => {
      expect(events.length).toEqual(3);
      expect(eventService.getUpcomingEvents).toHaveBeenCalledWith(
        component.widget.id,
        component.widget.settings._numberOfDisplayedEvents,
        component.widget.settings._displayOngoingEvents,
        component.widget.settings._fetchUpcomingEvents,
        component.widget.settings._sender);
      done();
    });
  });

  it('should reload events onChanges', done => {
    // given
    const previousValue = {
      settings: component.widget.settings
    };
    const currentValue = {
      settings: {
        _fetchUpcomingEvents: 'SELECTED',
        _displayOngoingEvents: true,
        _numberOfDisplayedEvents: 3,
        _sender: 'test-event-sender',
        _titles: ['title']
      }
    };

    fixture.detectChanges();

    // when
    component.ngOnChanges({
      widget: {
        previousValue,
        currentValue
      }
    } as unknown as SimpleChanges);

    // then
    component.events$.subscribe(events => {
      expect(events.length).toEqual(3);
      expect(eventService.getUpcomingEvents).toHaveBeenCalledWith(
        component.widget.id,
        currentValue.settings._numberOfDisplayedEvents,
        currentValue.settings._displayOngoingEvents,
        currentValue.settings._fetchUpcomingEvents,
        currentValue.settings._sender);
      done();
    });
  });

  it('should not reload events onChanges of the title', done => {
    // given
    const previousValue = {
      settings: component.widget.settings
    };
    const currentValue = {
      settings: {
        ...previousValue.settings,
        _titles: ['newTitle']
      }
    };

    fixture.detectChanges();

    // when
    component.ngOnChanges({
      widget: {
        previousValue,
        currentValue
      }
    } as unknown as SimpleChanges);

    // then
    component.events$.subscribe(events => {
      expect(events.length).toEqual(3);
      expect(eventService.getUpcomingEvents).toHaveBeenCalledWith(
        component.widget.id,
        currentValue.settings._numberOfDisplayedEvents,
        currentValue.settings._displayOngoingEvents,
        currentValue.settings._fetchUpcomingEvents,
        currentValue.settings._sender);
      done();
    });
  });
});
