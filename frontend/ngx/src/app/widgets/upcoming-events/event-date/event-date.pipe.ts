import {Pipe, PipeTransform} from '@angular/core';
import {SenderEvent} from '@domain/event/SenderEvent';
import * as moment from 'moment';

/**
 * Transforms an event to its correct event date.
 */
@Pipe({
  name: 'eventDate'
})
export class EventDatePipe implements PipeTransform {

  transform(event: SenderEvent, format: string = 'dd. D.M.YY'): string {
    const start = moment(event.startDate).format(format);
    const end = moment(event.endDate).format(format);
    return start === end ? start : `${start} – ${end}`;
  }
}
