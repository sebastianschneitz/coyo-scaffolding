import {Pipe, PipeTransform} from '@angular/core';
import {SenderEvent} from '@domain/event/SenderEvent';
import * as moment from 'moment';

/**
 * Transforms an event to its correct event time.
 */
@Pipe({
  name: 'eventTime'
})
export class EventTimePipe implements PipeTransform {

  transform(event: SenderEvent, format: string = 'h:mm A'): string {
    const start = moment(event.startDate);
    const end = moment(event.endDate);
    const sameDay = start.isSame(end, 'day');

    const startStr = start.format(format);
    const endStr = end.format(format);
    return startStr === endStr && sameDay ? startStr : `${startStr} – ${endStr}`;
  }
}
