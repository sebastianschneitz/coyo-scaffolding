import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {UpgradeModule} from '@upgrade/upgrade.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {IMAGE_WIDGET} from '@widgets/image/image-widget-config';
import {ImageWidgetSettingsComponent} from '@widgets/image/image-widget-settings/image-widget-settings.component';
import {ImageWidgetComponent} from '@widgets/image/image-widget/image-widget.component';

/**
 * Module providing the image widget.
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    UpgradeModule
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: IMAGE_WIDGET, multi: true},

  ],
  declarations: [
    ImageWidgetComponent,
    ImageWidgetSettingsComponent
  ],
  entryComponents: [
    ImageWidgetComponent,
    ImageWidgetSettingsComponent
  ]
})
export class ImageWidgetModule {
}
