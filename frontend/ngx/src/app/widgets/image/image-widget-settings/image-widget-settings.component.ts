import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '@core/auth/auth.service';
import {App} from '@domain/apps/app';
import {Document} from '@domain/file/document';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {CropSettings, Ng1FileLibraryModalOptions, Ng1FileLibraryModalService} from '@root/typings';
import {NG1_FILE_LIBRARY_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {WidgetInlineSettingsComponent} from '@widgets/api/widget-inline-settings-component';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {ImageWidget} from '@widgets/image/image-widget';
import * as _ from 'lodash';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {defaultIfEmpty, map, switchMap} from 'rxjs/operators';

/**
 * The image widget state.
 */
interface ImageWidgetState {
  sender: Sender;
  app: App;
}

/**
 * The image widget settings component.
 */
@Component({
  selector: 'coyo-image-widget-settings',
  templateUrl: './image-widget-settings.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageWidgetSettingsComponent extends WidgetInlineSettingsComponent<ImageWidget>
  implements OnInit, OnDestroy {

  constructor(private authService: AuthService,
              private senderService: SenderService,
              private widgetSettingsService: WidgetSettingsService,
              @Inject(NG1_FILE_LIBRARY_MODAL_SERVICE) private fileLibraryModalService: Ng1FileLibraryModalService) {
    super();
    this.state$ = this.stateSubject$.asObservable();
  }

  state$: Observable<ImageWidgetState>;

  private stateSubject$: BehaviorSubject<ImageWidgetState> = new BehaviorSubject<ImageWidgetState>({
    sender: null,
    app: null
  });

  ngOnInit(): void {
    this.initSender();
    this.state$.subscribe(currentState => {
      if (currentState.sender && !this.imageSettingsAvailable()) {
        this.selectImage();
      }
    });
  }

  ngOnDestroy(): void {
    this.stateSubject$.complete();
  }

  /**
   * Opens the file library for image selection.
   */
  selectImage(): void {
    const currentState: ImageWidgetState = this.stateSubject$.getValue();
    this.openFileLibrary(currentState.sender, currentState.app)
      .subscribe((image: Document) => {
        if (image) {
          const newImageSettings = {
            id: image.id,
            senderId: image.senderId
          };
          _.set(this.widget, 'settings._image', newImageSettings);
          this.widgetSettingsService.propagateChanges(this.widget, {_image: newImageSettings});
        }
      });
  }

  private initSender(): void {
    const senderId = this.senderService.getCurrentIdOrSlug();
    if (senderId) {
      this.senderService.get(senderId, {permissions: ['createFile']})
        .pipe(
          switchMap(sender => this.senderService.getCurrentApp(sender.id)
            .pipe(map(app => [sender, app]))
            .pipe(defaultIfEmpty([sender, null as App]))
          )
        )
        .subscribe((result: [Sender, App]) => this.updateState(result[0], result[1]));
    } else {
      this.authService.getUser().subscribe(user => this.updateState(user, null));
    }
  }

  private openFileLibrary(sender: Sender, app?: App): Observable<Document | Document[]> {
    const options: Ng1FileLibraryModalOptions = {
      uploadMultiple: false,
      selectMode: 'single',
      filterContentType: 'image',
      initialFolder: app ? {id: app.rootFolderId} : null
    };
    const cropSettings: CropSettings = {
      cropImage: true
    };
    return from(this.fileLibraryModalService.open(sender, options, cropSettings));
  }

  private updateState(sender: Sender, app: App): void {
    this.stateSubject$.next({sender, app});
  }

  private imageSettingsAvailable(): boolean {
    return !_.isNil(_.get(this.widget, 'settings._image'));
  }

}
