import {ChangeDetectorRef, SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Document} from '@domain/file/document';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {ImageWidget} from '@widgets/image/image-widget';
import {ImageWidgetComponent} from './image-widget.component';

describe('ImageWidgetComponent', () => {
  let component: ImageWidgetComponent;
  let fixture: ComponentFixture<ImageWidgetComponent>;
  let widgetSettingsService: jasmine.SpyObj<WidgetSettingsService>;
  const imageData = {_image: {id: 'document-id', senderId: 'sender-id'} as Document};

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ImageWidgetComponent],
        providers: [{
          provide: WidgetSettingsService,
          useValue: jasmine.createSpyObj('WidgetSettingsService', ['getChanges$'])
        }, ChangeDetectorRef]
      })
      .overrideTemplate(ImageWidgetComponent, '')
      .compileComponents();

    widgetSettingsService = TestBed.get(WidgetSettingsService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id'
    } as ImageWidget;
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init with object data', () => {
    // given
    component.widget = {
      id: 'widget-id',
      settings: {
        _image: imageData._image
      }
    } as ImageWidget;

    // when
    fixture.detectChanges();

    // then
    expect(component.image).toBe(imageData._image);
  });

  it('should react on changes', () => {
    // when
    component.ngOnChanges({
      widget: {
        currentValue: {
          settings: imageData
        }
      } as SimpleChange
    });

    // then
    expect(component.image).toBe(imageData._image);
  });

});
