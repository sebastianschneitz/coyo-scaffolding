import {Document} from '@domain/file/document';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of an image widget
 */
export interface ImageWidgetSettings extends WidgetSettings {
  _image: Document;
}
