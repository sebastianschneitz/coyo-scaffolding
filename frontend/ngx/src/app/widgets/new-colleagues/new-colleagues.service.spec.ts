import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {User} from '@domain/user/user';

import {NewColleaguesService} from '@widgets/new-colleagues/new-colleagues.service';

describe('NewColleaguesService', () => {
  let service: NewColleaguesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [NewColleaguesService]
    });

    service = TestBed.get(NewColleaguesService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should get new colleagues', () => {
    // given
    const page = {content: [{id: '3'}, {id: '4'}, {id: '5'}], last: true} as Page<User>;

    // when
    const result = service.getNewColleaguesList(new Pageable(0, 3));

    let called = false;
    result.subscribe(res => {
      called = true;
      expect(res).toBe(page);
    });

    // then
    const request = httpMock
      .expectOne('/web/widgets/newcolleagues?_page=0&_pageSize=3');
    request.flush(page);
    expect(called).toBeTruthy();
  });
});
