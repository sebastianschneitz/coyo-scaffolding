import {Widget} from '@domain/widget/widget';
import {UserProfileWidgetSettings} from '@widgets/user-profile/user-profile-widget-settings';

export interface UserProfileWidget extends Widget<UserProfileWidgetSettings> {
}
