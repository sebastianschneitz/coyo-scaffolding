import {CleanPhoneNumberPipe} from './clean-phone-number.pipe';

describe('CleanPhoneNumberPipe', () => {
  it('should strip formatting signs from telephone number', () => {
    // given
    const pipe = new CleanPhoneNumberPipe();
    const originalNumber = '+49 40 609 4000-70';
    const expectedResult = '+4940609400070';

    // when
    const transformResult = pipe.transform(originalNumber);

    // then
    expect(transformResult).toEqual(expectedResult);
  });
});
