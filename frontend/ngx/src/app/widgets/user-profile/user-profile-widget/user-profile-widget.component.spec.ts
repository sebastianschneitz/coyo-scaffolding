import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserService} from '@domain/user/user.service';
import {UserProfileWidget} from '@widgets/user-profile/user-profile-widget';
import {UserProfileWidgetSettings} from '@widgets/user-profile/user-profile-widget-settings';

import {UserProfileWidgetComponent} from './user-profile-widget.component';

describe('UserProfileWidgetComponent', () => {
  let component: UserProfileWidgetComponent;
  let fixture: ComponentFixture<UserProfileWidgetComponent>;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserProfileWidgetComponent],
      providers: [{
        provide: UserService,
        useValue: jasmine.createSpyObj('userService', ['get'])
      }]
    })
      .overrideTemplate(UserProfileWidgetComponent, '')
      .compileComponents();

    userService = TestBed.get(UserService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      settings: {} as UserProfileWidgetSettings
    } as UserProfileWidget;
    fixture.detectChanges();
  });

  it('should set user on init', () => {
    expect(userService.get).toHaveBeenCalled();
  });

  it('should set user on change', () => {
    // given
    const anotherUserId = 'another-user';
    component.widget.settings._selectedUser = anotherUserId;

    // when
    component.ngOnChanges({
      widget: {} as any
    });

    // then
    expect(userService.get).toHaveBeenCalledTimes(2);
    expect(userService.get).toHaveBeenCalledWith(anotherUserId);
  });

  it('should not set user on no widget change', () => {
    // when
    component.ngOnChanges({});

    // then
    expect(userService.get).toHaveBeenCalledTimes(1);
  });
});
