import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {UserProfileWidget} from '@widgets/user-profile/user-profile-widget';
import {UserProfileWidgetSettings} from '@widgets/user-profile/user-profile-widget-settings';

import {UserProfileWidgetSettingsComponent} from './user-profile-widget-settings.component';

describe('UserProfileWidgetSettingsComponent', () => {
  let component: UserProfileWidgetSettingsComponent;
  let fixture: ComponentFixture<UserProfileWidgetSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserProfileWidgetSettingsComponent]
    })
      .overrideTemplate(UserProfileWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.parentForm = new FormGroup({});
    component.widget = {
      settings: {} as UserProfileWidgetSettings
    } as UserProfileWidget;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
