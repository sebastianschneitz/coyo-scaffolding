import {ChangeDetectorRef, SimpleChange} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {TeaserWidget} from '@widgets/teaser/teaser-widget';
import {SwiperLoaderService} from '@widgets/teaser/teaser-widget/swiper-loader.service';
import * as _ from 'lodash';
import {Subject} from 'rxjs';
import {TeaserWidgetComponent} from './teaser-widget.component';

describe('TeaserWidgetComponent', () => {
  let component: TeaserWidgetComponent;
  let fixture: ComponentFixture<TeaserWidgetComponent>;
  let widgetSettingsService: jasmine.SpyObj<WidgetSettingsService>;
  const widgetChangesSubject$: Subject<WidgetSettings> = new Subject();
  let swiperLoader: jasmine.SpyObj<SwiperLoaderService>;
  let swiperObj: jasmine.SpyObj<any>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [TeaserWidgetComponent],
        providers: [{
          provide: SwiperLoaderService,
          useValue: jasmine.createSpyObj('swiperLoader', ['createInstance'])
        }, {
          provide: WidgetSettingsService,
          useValue: jasmine.createSpyObj('WidgetSettingsService', ['getChanges$'])
        }, {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        }, ChangeDetectorRef]
      })
      .overrideTemplate(TeaserWidgetComponent, '<div #swiperContainer></div>')
      .compileComponents();
    widgetSettingsService = TestBed.get(WidgetSettingsService);
    swiperLoader = TestBed.get(SwiperLoaderService);
    swiperObj = {
      destroy: jasmine.createSpy(),
      update: jasmine.createSpy(),
      params: {}
    };
    swiperLoader.createInstance.and.returnValue(swiperObj);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeaserWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {
        _autoplayDelay: 5,
        slides: [{}, {}]
      }
    } as TeaserWidget;
  });

  it('should create', fakeAsync(() => {
    // given

    // when
    fixture.detectChanges();
    tick();

    // then
    expect(component).toBeTruthy();
  }));

  it('should initialize with options', fakeAsync(() => {
    // given
    const expectedOptions = {
      // Optional parameters
      direction: 'horizontal',
      loop: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    };

    // when
    fixture.detectChanges();
    tick();

    // then
    expect(swiperLoader.createInstance).toHaveBeenCalled();
    const options = _.omit(swiperLoader.createInstance.calls.argsFor(0)[1], 'on'); // omit functions, they can't be compared
    expect(options).toEqual(expectedOptions);
  }));

  it('should not autoplay with only one slide', fakeAsync(() => {
    // given
    component.widget.settings.slides = [{} as any];
    const expectedOptions = {
      // Optional parameters
      direction: 'horizontal',
      loop: false
    };

    // when
    fixture.detectChanges();
    tick();

    // then
    expect(swiperLoader.createInstance).toHaveBeenCalled();
    const options = _.omit(swiperLoader.createInstance.calls.argsFor(0)[1], 'on'); // omit functions, they can't be compared
    expect(options).toEqual(expectedOptions);
  }));

  it('should update swiper if settings change, with autoplay', fakeAsync(() => {
    // when
    fixture.detectChanges();
    tick();
    component.ngOnChanges({
      widget: {
        isFirstChange: () => false,
      } as SimpleChange
    });
    tick();

    // then
    expect(swiperObj.destroy).toHaveBeenCalled();
    expect(component.swiper.params.autoplay).toEqual({
      delay: 5000,
      disableOnInteraction: false
    });
  }));

  it('should update swiper if settings change, one slide without autoplay', fakeAsync(() => {
    // given
    fixture.detectChanges();
    tick();
    component.widget.settings.slides = [{} as any];

    // when
    component.ngOnChanges({
      widget: {
        isFirstChange: () => false,
      } as SimpleChange
    });
    tick();

    // then
    expect(swiperObj.destroy).toHaveBeenCalled();
    expect(component.swiper.params.autoplay).toBeUndefined();
  }));
});
