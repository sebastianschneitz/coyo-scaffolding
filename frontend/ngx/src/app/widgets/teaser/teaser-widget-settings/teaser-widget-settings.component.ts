import {CdkDragSortEvent} from '@angular/cdk/drag-drop';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatDialogSize} from '@coyo/ui';
import {TranslateService} from '@ngx-translate/core';
import {WidgetSettingsComponent} from '@widgets/api/widget-settings-component';
import {NewSlideDialogComponent} from '@widgets/teaser/new-slide-dialog/new-slide-dialog.component';
import {TeaserWidget} from '@widgets/teaser/teaser-widget';
import * as _ from 'lodash';

/**
 * The teaser widget settings component.
 */
@Component({
  selector: 'coyo-teaser-widget-settings',
  templateUrl: './teaser-widget-settings.component.html',
  styleUrls: ['./teaser-widget-settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeaserWidgetSettingsComponent extends WidgetSettingsComponent<TeaserWidget>
  implements OnInit {

  constructor(private dialog: MatDialog,
              private cd: ChangeDetectorRef,
              private dialogRef: MatDialogRef<TeaserWidgetSettingsComponent>,
              private translateService: TranslateService) {
    super();
  }

  ngOnInit(): void {
    _.defaults(this.widget.settings, {
      _size: 'sm',
      _autoplayDelay: 5,
      slides: [
        {
          _image: 'assets/images/widgets/teaser/dummy-content/dreaming_of_clouds.jpg',
          _url: '/pages/company-news/apps/timeline/timeline',
          headline: this.translateService.instant('WIDGET.TEASER.SETTINGS.SLIDES.LABEL.DEFAULT.ONE.HEADLINE'),
          subheadline: this.translateService.instant('WIDGET.TEASER.SETTINGS.SLIDES.LABEL.DEFAULT.ONE.SUBHEADLINE'),
          _newTab: false
        },
        {
          _image: 'assets/images/widgets/teaser/dummy-content/favorit_act.jpg',
          _url: '/pages/company-news/apps/timeline/timeline',
          headline: this.translateService.instant('WIDGET.TEASER.SETTINGS.SLIDES.LABEL.DEFAULT.TWO.HEADLINE'),
          subheadline: this.translateService.instant('WIDGET.TEASER.SETTINGS.SLIDES.LABEL.DEFAULT.TWO.SUBHEADLINE'),
          _newTab: false
        },
        {
          _image: 'assets/images/widgets/teaser/dummy-content/new_client.jpg',
          _url: '/pages/company-news/apps/timeline/timeline',
          headline: this.translateService.instant('WIDGET.TEASER.SETTINGS.SLIDES.LABEL.DEFAULT.THREE.HEADLINE'),
          subheadline: this.translateService.instant('WIDGET.TEASER.SETTINGS.SLIDES.LABEL.DEFAULT.THREE.SUBHEADLINE'),
          _newTab: false
        }
      ]
    });

    this.parentForm.addControl('_size', new FormControl(this.widget.settings._size));
    this.parentForm.addControl('_autoplayDelay', new FormControl(this.widget.settings._autoplayDelay, [Validators.required, Validators.min(1)]));
    this.parentForm.addControl('slides', new FormArray([]));

    this.widget.settings.slides.forEach(slide => {
      (this.parentForm.get('slides') as FormArray).push(new FormGroup({
        _image: new FormControl(slide._image),
        _url: new FormControl(slide._url),
        headline: new FormControl(slide.headline, [Validators.required]),
        subheadline: new FormControl(slide.subheadline),
        _newTab: new FormControl(slide._newTab)
      }));
    });
  }

  addSlide(): void {
    this.hideDialog();
    const matDialogRef = this.dialog.open(NewSlideDialogComponent, {
      width: MatDialogSize.Medium,
      data: {}
    });
    matDialogRef.afterClosed().subscribe(newSlide => {
      if (newSlide) {
        (this.parentForm.get('slides') as FormArray).push(new FormGroup({
          _image: new FormControl(newSlide._image),
          _url: new FormControl(newSlide._url),
          headline: new FormControl(newSlide.headline),
          subheadline: new FormControl(newSlide.subheadline),
          _newTab: new FormControl(newSlide._newTab)
        }));
      }
      this.showDialog();
      this.cd.markForCheck();
    });
  }

  editSlide(index: number): void {
    this.hideDialog();
    const matDialogRef = this.dialog.open(NewSlideDialogComponent, {
      width: MatDialogSize.Medium,
      hasBackdrop: false,
      data: (this.parentForm.get('slides') as FormArray).at(index).value
    });
    matDialogRef.afterClosed().subscribe(newSlide => {
      if (newSlide) {
        (this.parentForm.get('slides') as FormArray).at(index).patchValue(newSlide);
        this.cd.markForCheck();
      }
      this.showDialog();
    });
  }

  deleteSlide(index: number): void {
    (this.parentForm.get('slides') as FormArray).removeAt(index);
  }

  getSlides(): FormArray {
    return this.parentForm.get('slides') as FormArray;
  }

  dropped($event: CdkDragSortEvent): void {
    const control = this.getSlides().at($event.previousIndex);
    this.getSlides().removeAt($event.previousIndex);
    this.getSlides().insert($event.currentIndex, control);
  }

  private hideDialog(): void {
    this.dialogRef.addPanelClass('ng-hide');
  }

  private showDialog(): void {
    this.dialogRef.removePanelClass('ng-hide');
  }
}
