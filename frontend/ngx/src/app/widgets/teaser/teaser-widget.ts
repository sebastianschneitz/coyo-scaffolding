import {Widget} from '@domain/widget/widget';
import {TeaserWidgetSettings} from '@widgets/teaser/teaser-widget-settings';

export interface TeaserWidget extends Widget<TeaserWidgetSettings> {

}
