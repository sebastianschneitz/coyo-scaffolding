import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UrlService} from '@core/http/url/url.service';
import {SenderService} from '@domain/sender/sender/sender.service';
import {NewSlideDialogComponent} from './new-slide-dialog.component';

describe('NewSlideDialogComponent', () => {
  let component: NewSlideDialogComponent;
  let fixture: ComponentFixture<NewSlideDialogComponent>;
  let backendUrlService: jasmine.SpyObj<UrlService>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<NewSlideDialogComponent>>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [NewSlideDialogComponent],
      providers: [{
        provide: MAT_DIALOG_DATA,
        useValue: {}
      }, {
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('ref', ['close'])
      }, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['getBackendUrl'])
      }, {
        provide: SenderService,
        useValue: jasmine.createSpyObj('senderService', ['get', 'getCurrentIdOrSlug'])
      }]
    })
      .overrideTemplate(NewSlideDialogComponent, '')
      .compileComponents();

    backendUrlService = TestBed.get(UrlService);
    dialogRef = TestBed.get(MatDialogRef);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSlideDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set image field when file changes', fakeAsync(() => {
    // given
    const senderId = 'sender-id';
    const fileId = 'file-id';
    const modified = Date.now();
    backendUrlService.getBackendUrl.and.returnValue('');

    // when
    component.form.patchValue({
      file: {
        senderId: senderId,
        id: fileId,
        modified: modified
      }
    });
    fixture.detectChanges();
    tick();

    // then
    expect(component.form.getRawValue()._image)
      .toBe(`/web/senders/${senderId}/documents/${fileId}?imageSize=ORIGINAL&modified=${modified}`);
  }));

  it('should submit and exclude file form value', fakeAsync(() => {
    // given
    const senderId = 'sender-id';
    const fileId = 'file-id';
    const modified = Date.now();
    backendUrlService.getBackendUrl.and.returnValue('');

    // when
    component.form.patchValue({
      file: {
        senderId: senderId,
        id: fileId,
        modified: modified
      }
    });
    fixture.detectChanges();
    tick();
    component.submit();

    // then
    expect(dialogRef.close.calls.argsFor(0)).toEqual([{
      headline: null, subheadline: null, _url: null,
      _image: `/web/senders/${senderId}/documents/${fileId}?imageSize=ORIGINAL&modified=${modified}`,
      _newTab: null
    }]);
  }));
});
