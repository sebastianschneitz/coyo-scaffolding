import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a teaser widget
 */
export interface TeaserWidgetSettings extends WidgetSettings {
  _autoplayDelay: number;
  _size: 'xs' | 'sm' | 'md' | 'lg';
  slides: TeaserWidgetSlide[];
}

export interface TeaserWidgetSlide {
  _url: string;
  _image: string;
  _newTab: boolean;
  headline: string;
  subheadline: string;
}
