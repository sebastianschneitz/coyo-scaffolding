import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import {Hashtag} from '../hashtag';
import {HashtagWidgetComponent} from './hashtag-widget.component';
import {HashtagWidgetService} from './hashtag-widget.service';

describe('HashtagWidgetComponent', () => {
  let component: HashtagWidgetComponent;
  let fixture: ComponentFixture<HashtagWidgetComponent>;
  let hashtagWidgetService: jasmine.SpyObj<any>;
  let stateService: jasmine.SpyObj<IStateService>;

  let model: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HashtagWidgetComponent],
      providers: [{
        provide: HashtagWidgetService,
        useValue: jasmine.createSpyObj('hashtagWidgetService', ['getTrendingHashtags'])
      }, {
        provide: NG1_STATE_SERVICE,
        useValue: jasmine.createSpyObj('stateService', ['go'])
      }]
    }).overrideTemplate(HashtagWidgetComponent, '')
      .compileComponents();

    hashtagWidgetService = TestBed.get(HashtagWidgetService);
    stateService = TestBed.get(NG1_STATE_SERVICE);
  }));

  beforeEach(() => {
    model = {settings: {_period: [{id: 'ONE_DAY'}]}};
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HashtagWidgetComponent);
    component = fixture.componentInstance;
    component.widget = model;
    fixture.detectChanges();
    hashtagWidgetService.getTrendingHashtags.and.returnValue([]);
  });

  it('should use the configured period', () => {
    model.settings['_period'] = 'TWO_MONTHS';
    fixture.detectChanges();

    expect(model.settings['_period']).toBe('TWO_MONTHS');
  });

  it('should call getTrendingHashtags from HashtagWidgetService', () => {
    component.getTrendingHashtags();

    expect(hashtagWidgetService.getTrendingHashtags).toHaveBeenCalled();
  });

  it('should call go from StateService', () => {
    const state = 'main.search';
    const hashtag = {tag: '#hashtag'} as Hashtag;
    component.openHashtag(hashtag);

    expect(stateService.go).toHaveBeenCalledWith(state, {term: hashtag.tag});
  });

  it('should have a default period value if not initialized', () => {
    component.widget.settings = undefined;

    component.getTrendingHashtags();

    expect(hashtagWidgetService.getTrendingHashtags).toHaveBeenCalledWith(''); // '' is the value for EVER, the new default value
  });
});
