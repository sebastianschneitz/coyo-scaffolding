/**
 * A single hashtag in the context of the hashtag widget.
 */
export interface Hashtag {
  tag: string;
  count?: number;
  opacity?: number;
  fontWeight?: number;
}
