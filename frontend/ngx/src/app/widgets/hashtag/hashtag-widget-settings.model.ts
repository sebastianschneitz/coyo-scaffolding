import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a hashtag widget
 */
export interface HashtagWidgetSettings extends WidgetSettings {
  _period: {
    id: string;
    text: string;
  };
}
