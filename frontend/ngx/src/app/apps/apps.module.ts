import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

/**
 * Module for Apps
 */
@NgModule({
  imports: [
    CommonModule
  ]
})
export class AppsModule {}
