import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {MessageFormComponent} from '@app/messaging/message-form/message-form.component';

getAngularJSGlobal()
  .module('coyo.messaging')
  .directive('coyoMessageForm', downgradeComponent({
    component: MessageFormComponent,
    propagateDigest: false
  }));
