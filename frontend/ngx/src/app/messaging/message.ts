import {Attachment} from '@domain/attachment/attachment';
import {FileItem} from 'ng2-file-upload';

/**
 * A chat message
 */
export interface Message {
  message: string;
  attachments: (FileItem | Attachment)[];
}
