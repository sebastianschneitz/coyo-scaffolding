import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {NotificationItemSkeletonComponent} from '@app/notifications/notification-item-skeleton/notification-item-skeleton.component';

getAngularJSGlobal()
  .module('coyo.notifications')
  .directive('coyoNotificationItemSkeleton', downgradeComponent({
    component: NotificationItemSkeletonComponent,
    propagateDigest: false
  }));
