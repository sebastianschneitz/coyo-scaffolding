import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

/**
 * Skeleton component for notification items
 */
@Component({
  selector: 'coyo-notification-item-skeleton',
  template: '<div class="{{className}}"></div>',
  styleUrls: ['./notification-item-skeleton.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationItemSkeletonComponent {
  /**
   * Defines which skeleton to load.
   */
  @Input() className: string;
}
