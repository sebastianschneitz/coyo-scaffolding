import {NgModule} from '@angular/core';
import {NotificationItemSkeletonComponent} from './notification-item-skeleton/notification-item-skeleton.component';
import './notification-item-skeleton/notification-item-skeleton.component.downgrade';

/**
 * Feature module for all notification related stuff
 */
@NgModule({
  imports: [],
  declarations: [
    NotificationItemSkeletonComponent
  ],
  exports: [],
  entryComponents: [
    NotificationItemSkeletonComponent
  ],
  providers: []
})

export class NotificationsModule {
}
