import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';

getAngularJSGlobal()
    .module('coyo.domain')
    .factory('ngxIntegrationApiService', downgradeInjectable(IntegrationApiService));
