import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {GDrivePickerService} from './g-drive-picker.service';

getAngularJSGlobal()
  .module('coyo.domain')
  .factory('ngxGDrivePickerService', downgradeInjectable(GDrivePickerService));
