/**
 * Represents a Google suite file type.
 */
export interface GDrivePickerFile {
    id: string;
    mimeType: string;
    url: string;
    displayName: string;
}
