/**
 * Model for passing the needed configuration settings to the file picker.
 */
export interface PickerSettings {
  multipleSelect: boolean;
  view: 'DOCS' | 'DOCS_IMAGES' | 'DOCS_VIDEOS';
  uploadView: boolean;
  recentFilesView: boolean;
}
