import {NgModule} from '@angular/core';
import './g-drive-picker/g-drive-picker.service.downgrade';
import './google-api/google-api.service.downgrade';

/**
 * Module for providing the functionality around the G Suite integration.
 */
@NgModule({})
export class GsuiteModule {}
