/**
 * Interface for google drive api permission
 */
export interface GoogleApiDrivePermission {
  kind: 'drive#permission';
  id: string;
  type: string;
  emailAddress: string;
  domain: string;
  role: string;
  allowFileDiscovery: boolean;
  displayName: string;
  photoLink: string;
  expirationTime: string;
  teamDrivePermissionDetails: [
      {
        teamDrivePermissionType: string;
        role: string;
        inheritedFrom: string;
        inherited: boolean;
      }
      ];
  permissionDetails: [
      {
        permissionType: string;
        role: string;
        inheritedFrom: string;
        inherited: boolean;
      }
      ];
  deleted: boolean;
}
