import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {GoogleApiService} from './google-api.service';

getAngularJSGlobal()
  .module('coyo.domain')
  .factory('ngxGoogleApiService', downgradeInjectable(GoogleApiService));
