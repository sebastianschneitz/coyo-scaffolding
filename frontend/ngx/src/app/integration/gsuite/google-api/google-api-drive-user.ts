/**
 * Interface for google drive api user
 */
export interface GoogleApiDriveUser {
  kind: 'drive#user';
  displayName: string;
  photoLink: string;
  me: boolean;
  permissionId: string;
  emailAddress: string;
}
