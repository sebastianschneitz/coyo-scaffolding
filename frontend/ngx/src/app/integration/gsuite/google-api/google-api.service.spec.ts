import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {fakeAsync, getTestBed, inject, TestBed, tick} from '@angular/core/testing';
import {AccessToken} from '@app/integration/gsuite/google-api/access-token';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {SettingsService} from '@domain/settings/settings.service';
import {Observable, of, throwError} from 'rxjs';
import {GoogleApiService} from './google-api.service';

describe('GoogleApiService', () => {
  let injector: TestBed;
  let settingsService: jasmine.SpyObj<SettingsService>;
  let http: jasmine.SpyObj<HttpClient>;
  let integrationApiService: jasmine.SpyObj<IntegrationApiService>;
  const gapiClient = jasmine.createSpyObj('gapi.client', ['setToken', 'init', 'request']);

  const ssoTokenResponse: AccessToken = {
    token: 'any-valid-access-token',
    expiresIn: 5000
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GoogleApiService, {
        provide: SettingsService,
        useValue: jasmine.createSpyObj('settingsService', ['retrieveByKey'])
      }, {
        provide: IntegrationApiService,
        useValue: jasmine.createSpyObj('integrationApiService', ['getToken', 'updateAndGetActiveState'])
      }]
    });

    injector = getTestBed();
    settingsService = injector.get(SettingsService);
    integrationApiService = injector.get(IntegrationApiService);
  });

  describe('GoogleApiService', () => {
    beforeEach(() => {
      settingsService.retrieveByKey.and.returnValue(Promise.resolve(''));
    });

    it('should be created', inject([GoogleApiService], (service: GoogleApiService) => {
      expect(service).toBeTruthy();
    }));

    it('should not update the dom tree if Google api is not activated', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        settingsService.retrieveByKey.and.returnValue(Promise.resolve('NONE'));

        // when
        service.initGoogleApi();
        tick();

        // then
        expect(document.querySelectorAll('[src="https://apis.google.com/js/api.js"]').length).toEqual(0);
      })
    ));

    it('should update the dom tree if Google api is activated', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        settingsService.retrieveByKey.and.returnValue(Promise.resolve('G_SUITE'));

        // when
        service.initGoogleApi();
        tick();

        // then
        expect(document.querySelectorAll('[src="https://apis.google.com/js/api.js"]').length).toEqual(1);
      })
    ));

    it('should get bearer token with header name', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        const token = 'access-token';
        integrationApiService.getToken.and.returnValue(of({token: token} as AccessToken));

        // when
        const actualResult: Observable<string> = service.getBearerTokenOnly();
        tick();

        // then
        actualResult.subscribe(accessTokenWithHeader => {
          expect(accessTokenWithHeader).toEqual('Bearer ' + token);
        });
      })
    ));

  });

  describe('GoogleApiService with active integration', () => {
    beforeEach(() => {
      integrationApiService.updateAndGetActiveState.and.returnValue(of(true));
    });

    it('should return true',
      inject([GoogleApiService], (service: GoogleApiService) => {
        // when
        const actualResult: Observable<boolean> = service.isGoogleApiActive();

        // then
        actualResult.subscribe(isGoogleApiActive => {
          expect(isGoogleApiActive).toBeTruthy();
        });
      })
    );
  });

  describe('GoogleApiService with inactive integration', () => {
    beforeEach(() => {
      integrationApiService.updateAndGetActiveState.and.returnValue(of(false));
    });

    it('should return false',
      inject([GoogleApiService], (service: GoogleApiService) => {
        // when
        const actualResult: Observable<boolean> = service.isGoogleApiActive();

        // then
        actualResult.subscribe(isGoogleApiActive => {
          expect(isGoogleApiActive).toBeFalsy();
        });
      })
    );
  });

  describe('extended api request', () => {
    const httpRequestHandling = (endpoint: string) => {
      if (endpoint.startsWith('https://www.googleapis.com')) {
        return of('[discoveryDocs]');
      }
    };

    beforeEach(() => {
      window['gapi'] = {
        load(type: string, callback: Function): void {
          callback();
        },
        client: gapiClient
      };
      http = injector.get(HttpClient);
      gapiClient.init.and.returnValue(Promise.resolve());
      integrationApiService.getToken.and.returnValue(of(ssoTokenResponse));
    });

    it('should provide file content request url', inject([GoogleApiService], (service: GoogleApiService) => {
      // given
      const sampleFileId = 'file123';
      const appendAccessTokenParam = spyOn(service, 'appendAccessTokenParam').and.returnValue(of('http://some.url?access_token=WITH_TOKEN'));

      // when
      const result = service.getFileContentRequestUrl(sampleFileId);

      // then
      expect(appendAccessTokenParam).toHaveBeenCalledWith(`https://www.googleapis.com/drive/v3/files/${sampleFileId}?alt=media`);
      result.subscribe(url => expect(url).toBe('http://some.url?access_token=WITH_TOKEN'));
    }));

    it('should provide file metadata', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        window['gapi'] = {
          load(type: string, callback: Function): void {
            expect(type).toEqual('client');
            callback();
          },
          client: gapiClient
        };

        tick();
        spyOn(http, 'get').and.callFake(httpRequestHandling);
        gapiClient.request.and.returnValue(Promise.resolve({result: {result: 'success'}}));

        // when
        service.getFileMetadata('id')
          .then((testResult: any) => expect(testResult.result).toEqual('success'));
        tick();

        // then
        expect(integrationApiService.getToken).toHaveBeenCalled();
        expect(http.get).toHaveBeenCalledWith('https://www.googleapis.com/discovery/v1/apis/drive/v3/rest');
        expect(gapi.client.setToken).toHaveBeenCalledWith({access_token: ssoTokenResponse.token});
        expect(gapi.client.init).toHaveBeenCalledWith({discoveryDocs: ['[discoveryDocs]']});
        expect(gapi.client.request).toHaveBeenCalledWith({path: 'https://www.googleapis.com/drive/v3/files/id?supportsTeamDrives=true&fields=*'});
      })
    ));

    it('should call the Google Discovery Documents only one time', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        let metadataRequestCount = 0;
        window['gapi'] = {
          load(type: string, callback: Function): void {
            expect(type).toEqual('client');
            callback();
          },
          client: gapiClient
        };
        http = injector.get(HttpClient);

        spyOn(http, 'get').and.callFake((endpoint: string) => {
          if (endpoint === 'https://www.googleapis.com/discovery/v1/apis/drive/v3/rest') {
            metadataRequestCount++;
          }
          if (endpoint.startsWith('https://www.googleapis.com')) {
            return of('[discoveryDocs]');
          }
        });

        tick();
        gapiClient.request.and.returnValue(Promise.resolve({result: {result: 'success'}}));

        // when
        service.getFileMetadata('id').then();
        service.getFileMetadata('id').then();
        tick();

        // then
        expect(metadataRequestCount).toBe(1);
      })
    ));

    it('should handle errors in doRequest', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        spyOn(http, 'get').and.callFake(httpRequestHandling);
        gapiClient.request.and.returnValue(() => Promise.reject());

        // when
        service.getFileMetadata('id').then(() => {
          // then
          throw new Error('an error is expected.');
        }).catch(() => {
          // expected behavior.
        });
      })
    ));

    it('should handle errors in getting the token', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        spyOn(http, 'get').and.returnValue(throwError('test'));

        // when
        service.getFileMetadata('id').then(() => {
          // then
          throw new Error('an error is expected.');
        }).catch(() => {
          // expected behavior.
        });
      })
    ));

    it('should provide the visibility of a file having the type domain', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        spyOn(http, 'get').and.callFake(httpRequestHandling);
        gapiClient.request.and.returnValue(Promise.resolve({result: {permissions: [{type: 'undefined'}, {type: 'domain'}]}}));

        // when
        service.isFilePublicVisible('id')
        // then
          .then((testResult: boolean) => expect(testResult).toEqual(true));
        tick();

        expect(http.get).toHaveBeenCalledWith('https://www.googleapis.com/discovery/v1/apis/drive/v3/rest');
        expect(gapi.client.request).toHaveBeenCalledWith({path: 'https://www.googleapis.com/drive/v3/files/id/permissions?supportsTeamDrives=true'});
      })
    ));

    it('should provide the visibility of a file having the type anyone', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        spyOn(http, 'get').and.callFake(httpRequestHandling);
        gapiClient.request.and.returnValue(Promise.resolve({result: {permissions: [{type: 'undefined'}, {type: 'anyone'}]}}));

        // when
        service.isFilePublicVisible('id')
        // then
          .then((testResult: boolean) => expect(testResult).toEqual(true));
      })
    ));

    it('should provide the visibility of a file that is public invisible', fakeAsync(
      inject([GoogleApiService], (service: GoogleApiService) => {
        // given
        spyOn(http, 'get').and.callFake(httpRequestHandling);
        gapiClient.request.and.returnValue(Promise.resolve({result: {permissions: [{type: 'undefined'}, {type: 'hidden'}]}}));

        // when
        service.isFilePublicVisible('id')
        // then
          .then((testResult: boolean) => expect(testResult).toEqual(false));
      })
    ));
  });
});
