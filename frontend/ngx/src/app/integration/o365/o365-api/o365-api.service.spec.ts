import {fakeAsync, TestBed} from '@angular/core/testing';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {SharePointApiService} from '@app/integration/o365/o365-api/share-point-api.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {BehaviorSubject, of} from 'rxjs';
import {GraphApiService} from './graph-api.service';
import {O365ApiService} from './o365-api.service';

describe('O365ApiService', () => {
  let service: O365ApiService;
  let integrationApiService: jasmine.SpyObj<IntegrationApiService>;
  let graphApiService: jasmine.SpyObj<GraphApiService>;
  let sharePointService: jasmine.SpyObj<SharePointApiService>;
  let activeStateObservable: BehaviorSubject<boolean>;
  let jwtHelper: jasmine.SpyObj<JwtHelperService>;

  sharePointService = jasmine.createSpyObj('SharePointApiService', ['get', 'getResultsWithCount']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: IntegrationApiService,
        useValue: jasmine.createSpyObj('IntegrationApiService', ['updateAndGetActiveState', 'getToken'])
      },
        {
          provide: GraphApiService,
          useValue: jasmine.createSpyObj('GraphApiService', ['get'])
        },
        {
          provide: JwtHelperService,
          useValue: jasmine.createSpyObj('JwtHelperService', ['decodeToken'])
        },
        {
          provide: SharePointApiService,
          useValue: sharePointService
        }]
    });
    activeStateObservable = new BehaviorSubject(true);
    integrationApiService = TestBed.get(IntegrationApiService);
    integrationApiService.updateAndGetActiveState.and.returnValue(activeStateObservable);
    jwtHelper = TestBed.get(JwtHelperService);
    graphApiService = TestBed.get(GraphApiService);
    graphApiService.get.and.callFake((sharePointUrl: string) => {
      switch (sharePointUrl) {
        case '/sites/root?$select=webUrl':
          return of({webUrl: sharePointUrl});
        case '/me/mailFolders/Inbox/messages?$filter=isRead eq false&$count=true':
          return of(true);
        default :
          return undefined;
      }
    });

    service = TestBed.get(O365ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update active state', fakeAsync(() => {
    // given
    const activeState = jasmine.createSpy();

    // when
    service.isApiActive().subscribe(activeState);
    activeStateObservable.next(false);

    // then
    expect(activeState).toHaveBeenCalledTimes(2);
    expect(activeState).toHaveBeenCalledWith(true);
    expect(activeState).toHaveBeenCalledWith(false);
  }));

  it('should get sharePoint site info', fakeAsync(() => {
    // given
    // when
    service.getDefaultSite();
    // then
    expect(graphApiService.get).toHaveBeenCalled();
  }));

  it('should fetch a drive item', fakeAsync(() => {
    // given
    const driveId = 'DRIVE_ID';
    const driveItemId = 'ITEM_ID';

    // when
    service.getDriveItem(driveId, driveItemId);

    // then
    expect(graphApiService.get).toHaveBeenCalledWith('/drives/DRIVE_ID/items/ITEM_ID');
  }));

  it('should return claims', () => {
    // given
    integrationApiService.getToken.and.returnValue(of({token: 'token'}));
    jwtHelper.decodeToken.and.returnValue({scp: 'testClaims'});

    // when
    service.getTokenClaims().subscribe(result => {
      // then
      expect(jwtHelper.decodeToken).toHaveBeenCalledWith('token');
      expect(result.scp).toEqual('testClaims');
    });
  });

  it('should be called unread outlook emails with correct params', () => {
    // given
    const url = '/me/mailFolders/Inbox/messages?$filter=isRead eq false&$count=true';
    const apiVersion = 'v1.0';
    const completeResult = true;
    // when
    service.hasUnreadOutlookEmails();

    expect(graphApiService.get).toHaveBeenCalledWith(url, apiVersion, completeResult);
  });

  it('should return unread outlook emails', done => {
    // when
    service.hasUnreadOutlookEmails().subscribe(result => {

      // then
      expect(result).toBeDefined();
      done();
    });
  });
  it('should get share point url', () => {
    // given

    // when
    service.getSharePointUrl().subscribe(resp => {
      // then
      expect(graphApiService.get).toHaveBeenCalled();
      expect(graphApiService.get).toHaveBeenCalledWith('/sites/root?$select=webUrl');
      expect(resp).toEqual('/sites/root?$select=webUrl');
    });
  });
});
