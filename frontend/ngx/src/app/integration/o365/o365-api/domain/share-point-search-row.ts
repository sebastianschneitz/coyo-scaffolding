export interface SharePointSearchRow {
  Cells: [{
    Key: string;
    Value: string;
  }];
}
