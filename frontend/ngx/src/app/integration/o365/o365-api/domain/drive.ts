export interface Drive {
  id: string;
  name: string;
  lastModifiedDateTime: string;
}
