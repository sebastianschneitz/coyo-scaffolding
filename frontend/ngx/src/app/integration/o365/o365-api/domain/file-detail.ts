export interface FileDetail {
  name: string;
  mimeType: string;
  webUrl: string;
  size: number;
}
