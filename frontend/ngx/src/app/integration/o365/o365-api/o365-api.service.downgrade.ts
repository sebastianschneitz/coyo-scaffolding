import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {O365ApiService} from '@app/integration/o365/o365-api/o365-api.service';

getAngularJSGlobal()
  .module('coyo.domain')
  .factory('ngxO365ApiService', downgradeInjectable(O365ApiService));
