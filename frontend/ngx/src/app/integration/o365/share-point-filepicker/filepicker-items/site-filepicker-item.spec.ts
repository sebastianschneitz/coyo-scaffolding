import {fakeAsync} from '@angular/core/testing';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {Site} from '@app/integration/o365/o365-api/domain/site';
import {DriveItemFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-item-filepicker-item';
import {SiteFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/site-filepicker-item';
import {of} from 'rxjs';

describe('SiteFilepickerItem', () => {
  let site: Site;
  let fetchDrives: jasmine.Spy;
  let filePickerItem: FilepickerItem;

  beforeEach(() => {
    site = {id: '1', displayName: 'site', lastModifiedDateTime: ''};
    fetchDrives = jasmine.createSpy();
    filePickerItem = new SiteFilepickerItem(site, fetchDrives);
  });

  it('should initialize correctly', () => {
    expect(filePickerItem.name).toBe(site.displayName);
    expect(filePickerItem.isFolder).toBe(true);
  });

  it('should get drives as children', fakeAsync(() => {
    // given
    const drives: DriveItemFilepickerItem[] = [];
    fetchDrives.and.returnValue(of(drives));

    // when/then
    filePickerItem.getChildren().subscribe(
      items => {
        expect(fetchDrives).toHaveBeenCalledWith(site.id);
        expect(items).toBe(drives);
      },
      error => fail()
    );
  }));
});
