import {fakeAsync} from '@angular/core/testing';
import {DriveItem} from '@app/integration/o365/o365-api/domain/drive-item';
import {DriveItemFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-item-filepicker-item';
import {of} from 'rxjs';

describe('DriveItemFilepickerItem', () => {
  let driveItem: DriveItem;
  let fetchChildDriveItems: jasmine.Spy;

  beforeEach(() => {
    driveItem = {id: '1', name: 'driveItem', parentReference: {driveId: 'drive'}, lastModifiedDateTime: ''};
    fetchChildDriveItems = jasmine.createSpy();
  });

  it('should initialize correctly', () => {
    // given/when
    const filePickerItem = new DriveItemFilepickerItem(driveItem, fetchChildDriveItems);

    // then
    expect(filePickerItem.name).toBe(driveItem.name);
  });

  it('should initialize as file', () => {
    // given/when
    driveItem.file = {mimeType: 'mimeType'};
    const filePickerItem = new DriveItemFilepickerItem(driveItem, fetchChildDriveItems);

    // then
    expect(filePickerItem.isFolder).toBe(false);
  });

  it('should initialize as folder', () => {
    // given/when
    driveItem.folder = {childCount: 0};
    const filePickerItem = new DriveItemFilepickerItem(driveItem, fetchChildDriveItems);

    // then
    expect(filePickerItem.isFolder).toBe(true);
  });

  it('should get the items inside the folder as children', fakeAsync(() => {
    // given
    const driveItems: DriveItemFilepickerItem[] = [];
    fetchChildDriveItems.and.returnValue(of(driveItems));
    const filePickerItem = new DriveItemFilepickerItem(driveItem, fetchChildDriveItems);

    // when/then
    filePickerItem.getChildren().subscribe(
      items => {
        expect(fetchChildDriveItems).toHaveBeenCalledWith(driveItem.parentReference.driveId, driveItem.id);
        expect(items).toBe(driveItems);
      },
      error => fail()
    );
  }));
});
