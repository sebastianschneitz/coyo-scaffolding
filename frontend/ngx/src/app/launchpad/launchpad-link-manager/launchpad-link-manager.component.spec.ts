import {FocusTrapFactory} from '@angular/cdk/a11y';
import {OverlayRef} from '@angular/cdk/overlay';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {LaunchpadLinkIconSrcPipe} from '@app/launchpad/launchpad-link-icon-src/launchpad-link-icon-src.pipe';
import {UrlService} from '@core/http/url/url.service';
import {LaunchpadCategory} from '@domain/launchpad/launchpad-category';
import {LaunchpadLink} from '@domain/launchpad/launchpad-link';
import {LaunchpadLinkService} from '@domain/launchpad/launchpad-link.service';
import {WebPreviewService} from '@domain/preview/web-preview/web-preview.service';
import {Ng1CsrfService} from '@root/typings';
import {NG1_CSRF_SERVICE} from '@upgrade/upgrade.module';
import {FileItem} from 'ng2-file-upload';
import {EMPTY, of} from 'rxjs';
import {CATEGORIES, LaunchpadLinkManagerComponent} from './launchpad-link-manager.component';

describe('LaunchpadLinkManagerComponent', () => {
  let component: LaunchpadLinkManagerComponent;
  let fixture: ComponentFixture<LaunchpadLinkManagerComponent>;
  let launchpadLinkService: jasmine.SpyObj<LaunchpadLinkService>;
  let launchpadLinkIconSrcPipe: jasmine.SpyObj<LaunchpadLinkIconSrcPipe>;
  let urlService: jasmine.SpyObj<UrlService>;
  let csrfService: jasmine.SpyObj<Ng1CsrfService>;
  let overlayRef: jasmine.SpyObj<OverlayRef>;
  let focusTrapFactory: jasmine.SpyObj<FocusTrapFactory>;
  let webPreviewService: jasmine.SpyObj<WebPreviewService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LaunchpadLinkManagerComponent],
      providers: [FormBuilder, {
        provide: OverlayRef,
        useValue: jasmine.createSpyObj('OverlayRef', ['keydownEvents', 'dispose'])
      }, {
        provide: FocusTrapFactory,
        useValue: jasmine.createSpyObj('FocusTrapFactory', ['create'])
      }, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['getBackendUrl'])
      }, {
        provide: LaunchpadLinkService,
        useValue: jasmine.createSpyObj('LaunchpadLinkService', ['post', 'put'])
      }, {
        provide: LaunchpadLinkIconSrcPipe,
        useValue: jasmine.createSpyObj('LaunchpadLinkIconSrcPipe', ['transform'])
      }, {
        provide: WebPreviewService,
        useValue: jasmine.createSpyObj('WebPreviewService', ['generateWebPreview'])
      }, {
        provide: NG1_CSRF_SERVICE,
        useValue: jasmine.createSpyObj('csrfService', ['getToken'])
      }, {
        provide: CATEGORIES,
        useValue: []
      }]
    }).overrideTemplate(LaunchpadLinkManagerComponent, '')
      .compileComponents();

    launchpadLinkService = TestBed.get(LaunchpadLinkService);
    launchpadLinkService.post.and.returnValue(of({}));
    launchpadLinkService.put.and.returnValue(of({}));
    launchpadLinkIconSrcPipe = TestBed.get(LaunchpadLinkIconSrcPipe);
    launchpadLinkIconSrcPipe.transform.and.returnValue('iconSrc');
    urlService = TestBed.get(UrlService);
    urlService.getBackendUrl.and.returnValue('http://localhost:8080');
    csrfService = TestBed.get(NG1_CSRF_SERVICE);
    csrfService.getToken.and.returnValue(Promise.resolve('csrf-token'));
    overlayRef = TestBed.get(OverlayRef);
    overlayRef.keydownEvents.and.returnValue(of({}));
    focusTrapFactory = TestBed.get(FocusTrapFactory);
    focusTrapFactory.create.and.returnValue(jasmine.createSpyObj('FocusTrap', ['focusInitialElementWhenReady']));
    webPreviewService = TestBed.get(WebPreviewService);
    webPreviewService.generateWebPreview.and.returnValue(of({}));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchpadLinkManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set up the uploader', fakeAsync(() => {
    // given
    component.category = {
      name: 'cat1'
    } as LaunchpadCategory;
    component.link = {
      url: 'URL',
      name: 'link1'
    } as LaunchpadLink;

    // when
    component.ngOnInit();
    tick();

    // then
    expect(component.iconSrc$.getValue()).toEqual('iconSrc');
    expect(component.linkForm.getRawValue()).toEqual(jasmine.objectContaining({
      url: 'URL',
      name: 'link1',
      category: component.category
    }));

    expect(csrfService.getToken).toHaveBeenCalled();
    expect(component.uploader).toBeDefined();
    expect(component.uploader.onSuccessItem).toBeDefined();
  }));

  it('should process file', fakeAsync(() => {
    // given
    const fileItem = {
      isUploading: false,
      isUploaded: false,
      upload: () => {}
    } as FileItem;
    spyOn(fileItem, 'upload');
    component.ngOnInit();
    tick();
    component.uploader.queue = [fileItem];

    // when
    component.uploadIcon();
    fixture.detectChanges();

    // then
    expect(fileItem.upload).toHaveBeenCalled();
  }));

  it('should remove uploaded icon', fakeAsync(() => {
    // given
    const fileItem = {
      isUploaded: true,
      remove: () => {}
    } as FileItem;
    spyOn(fileItem, 'remove');
    component.link = {} as LaunchpadLink;
    component.ngOnInit();
    tick();
    component.uploader.queue = [fileItem];

    // when
    component.removeIcon();
    fixture.detectChanges();

    // then
    expect(fileItem.remove).toHaveBeenCalled();
    expect(component.iconSrc$.getValue()).toBeNull();
    expect(component.linkForm.getRawValue()).toEqual(jasmine.objectContaining({
      newIconUid: '',
      newIconContentType: ''
    }));
  }));

  it('should not save the form while saving', () => {
    // given
    component.saving$.next(true);

    // when
    component.save();

    // then
    expect(launchpadLinkService.post).not.toHaveBeenCalled();
  });

  it('should save the form and add a link', () => {
    testSave(null);
  });

  it('should save the form and edit a link', () => {
    testSave({id: 'linkId'} as LaunchpadLink);
  });

  function testSave(link: LaunchpadLink): void {
    // given
    const category = {id: 'catId'} as LaunchpadCategory;
    component.link = link;
    component.linkForm.setValue({
      url: 'url',
      name: 'name',
      file: null,
      newIconUid: 'newIconUid',
      newIconContentType: 'newIconContentType',
      category: category
    });

    // when
    spyOn(component, 'close');
    component.save();

    // then
    const body = {
      url: 'url',
      name: 'name',
      newIconUid: 'newIconUid',
      newIconContentType: 'newIconContentType'
    };
    const context = {
      categoryId: 'catId'
    };

    if (link) {
      expect(launchpadLinkService.put).toHaveBeenCalledWith('linkId', body, {context});
      expect(component.close).toHaveBeenCalledWith([category, {} as LaunchpadLink]);
    } else {
      expect(launchpadLinkService.post).toHaveBeenCalledWith(body, {context});
      expect(component.close).toHaveBeenCalledWith([category, {} as LaunchpadLink]);
    }
  }

  it('should fetch url data (force)', () => {
    // given
    const inputUrl = 'www.url1';
    component.linkForm.patchValue({
      url: inputUrl,
      name: 'name1'
    });
    webPreviewService.generateWebPreview.and.returnValue(of({
      title: 'title',
      imageBlobUid: 'blobUid',
      contentType: 'cntType',
      imageUrl: 'imgUrl'
    }));

    // when
    component.fetchUrlMetaData(true);

    // then
    expect(webPreviewService.generateWebPreview).toHaveBeenCalledWith('https://' + inputUrl);
    expect(component.linkForm.get('name').value).toEqual('title');
  });

  it('should fetch url data (not force)', () => {
    // given
    const inputUrl = 'https://url1';
    component.linkForm.patchValue({
      url: inputUrl,
      newIconUid: 'iconUid',
      newIconContentType: 'iconCntType'
    });
    webPreviewService.generateWebPreview.and.returnValue(of({
      title: 'title',
      imageBlobUid: 'blobUid',
      contentType: 'cntType',
      imageUrl: 'imgUrl'
    }));

    // when
    component.fetchUrlMetaData(false);

    // then
    expect(webPreviewService.generateWebPreview).toHaveBeenCalledWith(inputUrl);
    expect(component.linkForm.get('newIconUid').value).toEqual('iconUid');
    expect(component.linkForm.get('newIconContentType').value).toEqual('iconCntType');
  });

  it('should not fetch url data (input empty)', () => {
    // given
    // when
    component.fetchUrlMetaData(true);

    // then
    expect(webPreviewService.generateWebPreview).not.toHaveBeenCalled();
  });

  it('should not fetch url data (no urlData)', () => {
    // given
    webPreviewService.generateWebPreview.and.returnValue(EMPTY);
    const inputUrl = 'https://url1';
    component.linkForm.patchValue({
      url: inputUrl
    });
    const currentStatus = component.linkForm.getRawValue();
    // when
    component.fetchUrlMetaData(true);

    // then
    expect(component.linkForm.getRawValue()).toEqual(currentStatus);
  });
});
