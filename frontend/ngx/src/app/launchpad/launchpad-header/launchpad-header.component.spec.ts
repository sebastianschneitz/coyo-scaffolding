import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {of} from 'rxjs';
import {LaunchpadHeaderComponent} from './launchpad-header.component';

describe('LaunchpadHeaderComponent', () => {
  let component: LaunchpadHeaderComponent;
  let fixture: ComponentFixture<LaunchpadHeaderComponent>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LaunchpadHeaderComponent],
      providers: [{
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('WindowSizeService', ['observeScreenChange'])
      }]
    }).overrideTemplate(LaunchpadHeaderComponent, '')
      .compileComponents();

    windowSizeService = TestBed.get(WindowSizeService);
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.LG));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchpadHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
