import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {BootstrapComponent} from '@app/app.bootstrap.component';

getAngularJSGlobal()
  .module('coyo.app')
  .directive('coyoBootstrap', downgradeComponent({
    component: BootstrapComponent
  }));
