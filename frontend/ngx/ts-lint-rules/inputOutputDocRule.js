"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Lint = require("tslint");
var Rule = /** @class */ (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        var walker = new InputOutputDocStylesWalker(sourceFile, this.getOptions());
        return this.applyWithWalker(walker);
    };
    Rule.FAILURE_STRING = 'component styles forbidden :(';
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var InputOutputDocStylesWalker = /** @class */ (function (_super) {
    __extends(InputOutputDocStylesWalker, _super);
    function InputOutputDocStylesWalker(sourceFile, options) {
        return _super.call(this, sourceFile, options) || this;
    }
    InputOutputDocStylesWalker.prototype.visitPropertyDeclaration = function (node) {
        if (node.decorators && node.decorators.length > 0) {
            var decorator = node.decorators[0].expression.getText();
            var hasDoc = node.jsDoc && node.jsDoc.length;
            if ((decorator.startsWith('Input(') || decorator.startsWith('Output(')) && !hasDoc) {
                this.addFailureAtNode(node, 'Missing documentation for input/output parameter');
            }
        }
    };
    return InputOutputDocStylesWalker;
}(Lint.RuleWalker));
