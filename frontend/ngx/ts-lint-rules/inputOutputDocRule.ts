import * as ts from 'typescript';
import * as Lint from 'tslint';
import {IOptions} from 'tslint';

export class Rule extends Lint.Rules.AbstractRule {
  public static FAILURE_STRING = 'component styles forbidden :(';

  public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
    const walker = new InputOutputDocStylesWalker(sourceFile, this.getOptions());
    return this.applyWithWalker(walker);
  }
}

class InputOutputDocStylesWalker extends Lint.RuleWalker {

  constructor(sourceFile: ts.SourceFile, options: IOptions) {
    super(sourceFile, options);
  }

  visitPropertyDeclaration(node: ts.PropertyDeclaration) {
    if (node.decorators && node.decorators.length > 0) {
      const decorator = node.decorators[0].expression.getText();
      const hasDoc = (<any>node).jsDoc && (<any>node).jsDoc.length;
      if ((decorator.startsWith('Input(') || decorator.startsWith('Output(')) && !hasDoc) {
        this.addFailureAtNode(node, 'Missing documentation for input/output parameter')
      }
    }
  }
}
