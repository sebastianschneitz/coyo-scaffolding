import {Tree} from '@angular-devkit/schematics';
import {SchematicTestRunner} from '@angular-devkit/schematics/testing';
import * as path from 'path';


const collectionPath = path.join(__dirname, '../collection.json');


describe('widget', () => {
    it('works', () => {
        const runner = new SchematicTestRunner('schematics', collectionPath);
        const tree = runner.runSchematic('widget', {name: 'foo_bar'}, Tree.empty());

        const expectedFiles = [
            '/widgets/foo-bar/foo-bar-widget.ts',
            '/widgets/foo-bar/foo-bar-widget-config.ts',
            '/widgets/foo-bar/foo-bar-widget-settings.ts',
            '/widgets/foo-bar/foo-bar-widget.module.ts',
            '/widgets/foo-bar/de.foo-bar-widget.messages.ts',
            '/widgets/foo-bar/en.foo-bar-widget.messages.ts',
            '/widgets/foo-bar/foo-bar-widget-settings/foo-bar-widget-settings.component.ts',
            '/widgets/foo-bar/foo-bar-widget-settings/foo-bar-widget-settings.component.spec.ts',
            '/widgets/foo-bar/foo-bar-widget-settings/foo-bar-widget-settings.component.html',
            '/widgets/foo-bar/foo-bar-widget/foo-bar-widget.component.ts',
            '/widgets/foo-bar/foo-bar-widget/foo-bar-widget.component.spec.ts',
            '/widgets/foo-bar/foo-bar-widget/foo-bar-widget.component.html',
            '/widgets/foo-bar/foo-bar-widget/foo-bar-widget.component.global.scss'
        ];
        for (const file of expectedFiles) {
            expect(tree.files).toContain(file);
        }
        expect(tree.files.length).toEqual(expectedFiles.length);
    });
});
