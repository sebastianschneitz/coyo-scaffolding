import {camelize, capitalize, classify, dasherize, underscore} from '@angular-devkit/core/src/utils/strings';
import {
  apply,
  MergeStrategy,
  mergeWith,
  move,
  renameTemplateFiles,
  Rule,
  SchematicContext,
  template,
  Tree,
  url
} from '@angular-devkit/schematics';

export function fullcaps(str: string): string {
  return underscore(str).toUpperCase();
}

export function widget(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const stringUtils = {classify, dasherize, camelize, underscore, capitalize, fullcaps};
    const templateSource = apply(url('./files'), [
        renameTemplateFiles,
        template({..._options, ...stringUtils}),
        move(`./src/app/widgets/${dasherize(_options.name)}`)
    ]);
    const rule = mergeWith(templateSource, MergeStrategy.Overwrite);
    return rule(tree, _context);
  };
}
