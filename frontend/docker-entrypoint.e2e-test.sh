#!/bin/bash

set -e;

cd /coyo

for i in $(seq 1 5); do ./gradlew coyo-frontend:ngx:yarn_install && s=0 && break || s=$? && echo \"Retrying...\"; sleep 1; done; (exit $s)

./gradlew -PbuildDirBase=/build \
  --project-cache-dir=/.coyogradle \
  --stacktrace \
  --no-daemon \
  coyo-frontend:e2eTest \
  -x coyo-frontend:ngx:yarn_install
