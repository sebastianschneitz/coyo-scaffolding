// ***********************************************************
// Custom commands related to user management
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Command for creating a user via UI
     * @param group
     * @param role
     */
    createUser(group, role): Cypress.Chainable<void>;
  }
}