// ***********************************************************
// Custom commands related to users
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Command for creating a user via API
     * @param {boolean} superadmin defines if the user is a superadmin or not
     *
     */
    apiCreateUser(superadmin: boolean): Cypress.Chainable<any>;

  }
}
