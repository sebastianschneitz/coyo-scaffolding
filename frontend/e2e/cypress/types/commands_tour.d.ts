// ***********************************************************
// Custom commands related to tour
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Command for click through each tour via UI
     * @param {string} target can be 'timeline', 'pages', 'workspaces', 'events', 'search', 'colleagues', or 'profile'
     *
     */
    tour(target: string): Cypress.Chainable<void>;
  }
}