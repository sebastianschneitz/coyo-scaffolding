// ***********************************************************
// Custom commands related to coyo chat
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {
    /**
     * Command for expanding chat sidebar
     */
    expandChatSidebar(): Chainable<any>;

    /**
     * Command for collapsing chat sidebar
     */
    collapseChatSidebar(): Chainable<any>;

    /**
     * Command for editing the status of the user
     * @param {string} status can be 'online', 'away', 'busy', 'gone' or 'offline'
     * @param {string} message is the status message
     *
     */

    changePresenceStatus(status: string, message: string): Chainable<any>;

    /**
     * Command for create a new single message channel and send a message
     * @param {string} user is the name of the user for the new message channel
     * @param {string} message is the chat message
     *
     */

    newSingleMessageChannel(user: string, message: string): Chainable<any>;

    /**
     * Command for create a new group message channel with two users and send a message
     * @param {string} user1 is the name of the first user for the new message channel
     * @param {string} user2 is the name of the second user for the new message channel
     * @param {string} channelName is the name for the new group message channel
     * @param {string} message is the chat message
     *
     */

    newGroupMessageChannel(user1: string, user2: string, channelName: string, message: string): Chainable<any>;

    /**
     * Command to check the behavior of emoji in the chat
     * @param {string} user is the name of the user for the new message channel
     * * @param {string} message is the chat message
     *
     */

    newSingleMessageChannelWithEmoji(user: string, message: string): Chainable<any>;
  }
}