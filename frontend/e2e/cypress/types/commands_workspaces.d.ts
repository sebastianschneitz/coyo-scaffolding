// ***********************************************************
// Custom commands related to coyo workspaces
// ***********************************************************

declare namespace Cypress {

    interface Chainable<Subject = any> {

        /**
         * Command for creating a workspace
         * @param {string} workspaceName is a custom name for the workspace
         * @param {string} category can be 'Ideas', 'Projects', 'Products', or 'Partners'
         * @param {string} visibility can be 'public', 'protected' or 'private'
         *
         */

        createWorkspace(workspaceName: string, category: string, visibility: string): Chainable<any>;

        /**
         * Command for creating a page via API
         * @param {string} name describes the name of the workspace
         * @param {string} visibility can be 'PUBLIC' or 'PRIVATE'
         *
         */

        apiCreateWorkspace(name: string, visibility: string): Chainable<any>;
    }
}