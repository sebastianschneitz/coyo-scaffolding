// ***********************************************************
// Custom commands related to coyo timeline
// ***********************************************************

declare namespace Cypress {

    interface Chainable<Subject = any> {

        /**
         * Creates a new timeline post and returns it.
         *
         * @param message The timeline post message.
         * @param isRestricted Is a restricted post or not.
         * @example
         *    cy.createTimelinePost('Post message');
         */
        apiCreateTimelinePost(message: string, isRestricted: boolean): Cypress.Chainable<any>;

        /**
         * Shows the new timeline items if the load button is visible.
         *
         * @example
         *    cy.showNewTimelinePosts();
         */
        apiShowNewTimelinePosts(): Cypress.Chainable<any>;
    }
}
