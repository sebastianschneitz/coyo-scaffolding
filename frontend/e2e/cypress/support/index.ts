// ***********************************************************
// This support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

import './commands-api';
import './commands-auth';
import './commands-comment';
import './commands-events';
import './commands-widgets';
import './commands-user';
import './commands-page';
import './commands-timeline';
import './commands-apps';
import './commands-apps-blog';
import './commands-workspaces';
import './commands-chat';
import './commands-login';
import './commands-tour';
import './commands-user-management';
import './commands-general';

import * as faker from 'faker';

Cypress['faker'] = faker;
