// ***********************************************************
// Custom commands related to authentication
//
// Commands defined in this file must not utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// ***********************************************************

/**
 * Retrieves a new CSRF token and returns it. The token is also provided as
 * a context variable named `csrf`.
 *
 * @example
 *    cy.csrf();
 */
Cypress.Commands.add('csrf', function () {
    return cy.request({
        method: 'GET',
        url: Cypress.env('backendUrl') + '/web/csrf',
    }).then((response) => cy.wrap(response.body.token).as('csrf'));
});

/**
 * Perform request using сыка token of the current user
 * @param method can be any http method (GET, POST, PUT, DELETE)
 * @param path is a url path
 * @param body optional and contains a body of the request
 */
// Request authentication via API
Cypress.Commands.add('requestAuth', function (method, path, body) {
    return cy.request({
        method: method,
        url: Cypress.env('backendUrl') + path,
        headers: method === 'GET' ? {
            'Accept': 'application/json',
            'X-Coyo-Current-User': this.user.id
        } : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': this.csrf,
            'X-Coyo-Current-User': this.user.id
        },
        body: body
    });
});

/**
 * Get bearer token via API
 * @param {string} username can be any available username
 * @param {string} password is the corresponding password
 */
// Get bearer token via API
Cypress.Commands.add('apiBearerToken', function (username, password) {
    cy.getClientSecret().then((credentials) => cy.request({
        auth: {
            user: credentials.clientId,
            pass: credentials.clientSecret
        },
        method: 'POST',
        url: Cypress.env('backendUrl') + '/api/oauth/token?grant_type=password&username=' + username + '&password='
            + password
    })).then((response) => cy.wrap(response.body.access_token).as('bearerToken'));
});


/**
 * Command for logging in via API
 * @param {string} username can be any available username
 * @param {string} password is the corresponding password
 * If left empty the given user in the cypress.json will taken, usually Robert Lang
 *
 */

// Login via API
Cypress.Commands.add('apiLogin', function (username, password) {
    return cy.csrf().then((csrf) => cy.request({
        method: 'POST',
        url: Cypress.env('backendUrl') + '/web/auth/login',
        form: true,
        headers: {
            'X-CSRF-TOKEN': csrf
        },
        body: {
            username: username || Cypress.env('username'),
            password: password || Cypress.env('password')
        }
    }).then((response) => {
        const user = JSON.parse(response.body);
        localStorage.setItem('ngStorage-backendUrl', '"' + Cypress.env('backendUrl') + '"');
        localStorage.setItem('ngStorage-isAuthenticated', 'true');
        localStorage.setItem('ngStorage-userId', '"' + user.id + '"');

        return cy.wrap(user).as('user').then(() =>
            cy.csrf().then(() => user)
        );
    }));
});

/**
 * Command for logging user out via API
 *
 */

Cypress.Commands.add('apiLogout', function () {
    cy.csrf().then((csrf) => cy.request({
        method: 'POST',
        url: Cypress.env('backendUrl') + '/web/auth/logout',
        headers: {
            'X-CSRF-TOKEN': csrf
        }
    }));
    cy.visit('/');
    cy.clearCookies();
});
