// ***********************************************************
// Custom commands related to events
// ***********************************************************

/**
 * Command for creating a event via UI
 * @param eventName string, the event will be created with this name
 * @param host string, the host of the event, if set up the event will be created with a host different from the current user
 * @param isPrivate boolean, if set up and true, the event will be created as private with additional step to invite attendees. If not/false, public event will be created
 * @param invitee string, if set up for private event, the event will be created with inviting this user to an event. Will be ignored if set up for public event
 */

Cypress.Commands.add('createEvent', function (eventName: string, isPrivate=false, host: string, invitee: string) {
    // Create an event
    cy.server();
    cy.route('POST', '/web/events').as('postEvents');
    cy.route('GET', '/web/senders/search/managed**').as('getSenders');


    cy.get('[data-test=navigation-events]')
        .click();
    cy.url()
        .should('contain', Cypress.config().baseUrl + '/events');
    cy.get('[data-test=create-event-menu]')
        .click();

    let eventType: string;
    switch (isPrivate) {
        case true:
            eventType = 'Private event';
            break;
        case false:
            eventType = 'Public event';
            break;
        default:
            eventType = 'Public event';
    }

    cy.get('.cdk-overlay-pane').get('button').contains(eventType).click();
    cy.url()
        .should('contain', Cypress.config().baseUrl + '/events/create');

    cy.get('[data-test=event-create-input-event-name]')
        .click()
        .type(eventName).blur();

    if (host) {
        cy.get('[data-test=select-sender-component]').click();
        cy.wait('@getSenders').its('status').should('eq', 200);
        cy.get('.ng-dropdown-panel-items').within(() => {
            cy.wait(1000); //Temporal solution until Cypress bug https://github.com/cypress-io/cypress/issues/5743 is fixed
            cy.contains('[data-test="sender-option-name"]', host).should('be.visible')
                .click();
        });
    }

    cy.get('[data-test=event-create-checkbox-full-day]').click();

    cy.get('[data-test=event-create-advanced-settings]').within(() => {
        cy.get('[data-test=event-create-show-participants-checkbox]').click();
        cy.get('[data-test=event-create-definite-answer-checkbox]').click();
        cy.get('[data-test=event-create-limited-participants-checkbox]').click();
        cy.get('[data-test=event-create-limited-participants-input]').click()
            .clear()
            .type('10').blur();
    });

    cy.get('[data-test=event-create-submit-button]')
        .click();

    if (isPrivate === true) {
        if (invitee) {
            cy.get('[data-test=button-user-chooser]').click();
            cy.get('[data-test=list-user-chooser-user]').get('[data-test=user-chooser-element]').contains(invitee).scrollIntoView()
                .click();
            cy.get('[data-test=button-user-chooser-submit]').click();
        }
        cy.get('[data-test="button-event-create-event"]').click();
    }
});
