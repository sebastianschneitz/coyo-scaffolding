// ***********************************************************
// Custom commands related to the login
// ***********************************************************

/**
 * Logs in via UI using the given credentials and returns the current user. The
 * user is also provided as a context variable named `user`.
 *
 * @param {string} [username] The username to use. Defaults to `Cypress.env('username')`.
 * @param {string} [password] The password to use. Defaults to `Cypress.env('password')`.
 * @example
 *    cy.login();
 *    cy.login('username@example.com', 'secret');
 */// Successful login
Cypress.Commands.add('login', function (username, password) {
  cy.server();
  cy.route('GET', '/web/setup/check').as('getSetupCheck');

  cy.visit('/');
  cy.wait('@getSetupCheck');
  cy.url()
      .should('eq', Cypress.config().baseUrl + '/f/login');
  // Login with given user
  cy.get('[data-test=input-username]')
      .type(username);
  cy.get('[data-test=input-password]')
      .type(password);
  cy.get('[data-test=button-submit]')
      .click();
});

// Reset the password
Cypress.Commands.add('resetPassword', function (user) {
  cy.server();
  cy.route('GET', '/web/setup/check').as('getSetupCheck');

  cy.visit('/');
  cy.wait('@getSetupCheck');
  cy.url()
      .should('eq', Cypress.config().baseUrl + '/f/login');

  // Reset forgotten password
  cy.get('[data-test="button-password-reset"]')
      .click();
  cy.url()
      .should('eq', Cypress.config().baseUrl + '/f/reset');
  cy.get('[data-test=input-username]')
      .type(user);
  cy.get('[data-test=button-submit]')
      .click();
  cy.get('[data-test=text-mail-sent]')
      .should('contain', 'A link to reset your password');
  
  // Go back to login afterwards
  cy.get('[data-test=button-back-to-login]')
      .click();
  cy.url()
      .should('eq', Cypress.config().baseUrl + '/f/login');
});

// Login with SAML

// Login with Office 365

// Login with G Suite
