// ***********************************************************
// Custom commands related to the timeline
// ***********************************************************

Cypress.Commands.add('apiCreateTimelinePost', function (message: string, isRestricted: boolean) {
  return cy.requestAuth('POST', '/web/timeline-items', {
    attachments: [],
    authorId: this.user.id,
    data: {message: message},
    fileLibraryAttachments: [],
    recipientIds: [this.user.id],
    restricted: isRestricted,
    stickyExpiry: null,
    type: 'post',
    webPreviews: {}
  }).its('body');
});

Cypress.Commands.add('apiShowNewTimelinePosts', function() {
  return cy.requestAuth('GET', '/web/timeline-items/new?contextId=' + this.user.id + '&type=personal');
});