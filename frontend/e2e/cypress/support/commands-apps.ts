// ***********************************************************
// Custom commands related to apps
//
// Commands defined in this file may utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// Ideally, these commands utilize only variables defined in
// authentication commands at `./commands-auth.ts`,
// ***********************************************************
/// <reference types="Cypress" />
/// <reference types="../types" />

Cypress.Commands.add('createFormApp', function (name: string, senderId: string, enableNotifications: boolean): any {
  const notificationLevel = enableNotifications ? 'ADMIN' : 'NONE';
  return cy.requestAuth('POST', '/web/senders/' + senderId + '/apps?_permissions=manage', {
    active: true,
    defaultLanguage: null,
    key: 'form',
    name: name,
    senderId: senderId,
    settings: {notification: notificationLevel, title: name},
    translations: {}
  }).its('body');
});

Cypress.Commands.add('createFormAppEntry', function (value: string, fieldId: string, senderId: string, appId: string): any {
  return cy.requestAuth('POST', '/web/senders/' + senderId + '/apps/' + appId + '/form/entries', {
    appId: appId,
    appKey: 'form',
    senderId: senderId,
    values: [{fieldId: fieldId, value: value}]
  }).its('body');
});

Cypress.Commands.add('addFieldToFormApp', function (name: string, senderId: string, appId: string, type: string): any {
  return cy.requestAuth('POST', '/web/senders/' + senderId + '/apps/' + appId + '/form/fields', {
    appId: appId,
    appKey: 'form',
    key: type,
    name: name,
    senderId: senderId,
    settings: {}
  }).its('body');
});

/**
 * Command for activating and deactivating apps for pages and workspaces
 * @param {string} app can be 'blog', 'content', 'events', 'file-library', 'form', 'forum', 'list', 'task', 'timeline', 'wiki', or 'championship'
 * @param {string} senderType should be 'page' or 'workspace'
 * @param {boolean} enabled describes if the app is enabled or disabled
 *
 */

Cypress.Commands.add('apiAppConfiguration', function (app, senderType, enabled) {
  return cy.apiBearerToken('robert.lang@coyo4.com', 'demo').then((bearerToken) => cy.request({
    url: Cypress.env('backendUrl') + '/api/apps/configurations/' + app,
    method: 'PUT',
    auth: {
      bearer: bearerToken
    },
    body: {
      senderType : senderType,
      enabled : enabled
    }
  }));
});
