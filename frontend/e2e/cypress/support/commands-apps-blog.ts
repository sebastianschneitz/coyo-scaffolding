// ***********************************************************
// Custom commands related to the blog app
// ***********************************************************


/**
 * Command for creating a blog app
 * @param {string} name defines the name of the blog app
 * @param {boolean} active defines if the app is active or not
 * @param {string} authors defines who can write blog articles and can be 'admins', 'everyone', or 'custom'
 * @param {string} publishers defines who can edit and publish articles and can be 'admins', 'everyone', or 'custom'
 * @param {boolean} comments defines if user can comment blog articles
 * @param {boolean} share defines if the articles get automatically shared
 *
 */

Cypress.Commands.add('createBlogApp', function (name, active, authors, publishers, comments, share) {
  cy.server();
  cy.route('GET', '/web/apps/configurations**').as('getAppsModal');
  cy.route('POST', '/web/senders/**').as('postBlogCreation');
  cy.get('[data-test=button-add-app]')
      .click();
  cy.wait('@getAppsModal');
  cy.get('[data-test=button-app-chooser]').contains('Blog')
      .click();
  // Set blog name
  cy.get('[data-test=input-app-name]')
      .clear()
      .type(name);
  // Set if blog is active
  cy.get('[data-test=radio-app-active] > [data-test=checkbox]').then((checkboxValues) => {
    let checkboxChecked = checkboxValues[0].attributes[2].value;
    let checkboxCheckedBool = (checkboxChecked === 'true');
    if (active !== checkboxCheckedBool) {
      cy.get('[data-test=radio-app-active] > [data-test=checkbox]')
          .click();
    }
  });
  // Set blog authors
  cy.get('[data-test=radio-blog-author-' + authors + ']')
      .check();
  // Set blog publishers
  cy.get('[data-test=radio-blog-publishers-' + publishers + ']')
      .check();
  // Set if comments are possible
  cy.get('[data-test=radio-blog-comments] > [data-test=checkbox]').then((checkboxValues) => {
    let checkboxChecked = checkboxValues[0].attributes[2].value;
    let checkboxCheckedBool = (checkboxChecked === 'true');
    if (comments !== checkboxCheckedBool) {
      cy.get('[data-test=radio-blog-comments] > [data-test=checkbox]')
          .click();
    }
  });
  // Set if blog will be shared automatically
  cy.get('[data-test=radio-blog-share] > [data-test=checkbox]').then((checkboxValues) => {
    let checkboxChecked = checkboxValues[0].attributes[2].value;
    let checkboxCheckedBool = (checkboxChecked === 'true');
    if (share !== checkboxCheckedBool) {
      cy.get('[data-test=radio-blog-share] > [data-test=checkbox]')
          .click();
    }
  });
  // Save blog app
  cy.get('[data-test="button-app-chooser-save submit"]')
      .click();
  cy.wait('@postBlogCreation');
});

/**
 * Command for creating a blog article
 * @param {string} title defines the blog title
 * @param {string} text defines the blog text
 * @param {string} teaserText define the blog teaser text
 * @param {boolean} displayTeaser asd Display the teaser before the blog article.
 * @param {string} status asd 'draft','published', or 'published-at'
 * @param {string} author 'user' 'pageworkspace'
 *
 */

Cypress.Commands.add('createBlogArticle', function (title, text, teaserText, displayTeaser, status, author) {
  cy.server();
  cy.route('GET', '/web/senders/**/blog/articles/publishAsSender').as('getBlogCreation');
  cy.get('[data-test=button-create-blog-article]').eq(2)
      .click();
  cy.wait('@getBlogCreation');
  // Set blog title
  cy.get('[data-test=input-blog-title]')
      .type(title);
  // Set blog text
  cy.get('.fr-element') // So far not possible to edit the selector since this is the Froala RTE
      .type(text);
  // Change to teaser tab
  cy.get('[data-test=tab-blog-teaser]')
      .click();
  // Set teaser text
  cy.get('[data-test=textarea-teaser-text]')
      .type(teaserText);
  // Set if main teaser image is displayed before the blog article
  cy.get('[data-test=checkbox-blog-display-teaser] > [data-test=checkbox]').then((checkboxValues) => {
    let checkboxChecked = checkboxValues[0].attributes[2].value;
    let checkboxCheckedBool = (checkboxChecked === 'true');
    if (displayTeaser !== checkboxCheckedBool) {
      cy.get('[data-test=checkbox-blog-display-teaser] > [data-test=checkbox]')
          .click();
    }
  });
  // Change to publish tab
  cy.get('[data-test=tab-blog-publish]')
      .click();
  // Set blog status
  cy.get('[data-test=radio-blog-status-' + status + ']')
      .check();
  // Set author
  cy.get('[data-test=radio-blog-author-' + author + ']')
      .check();
  // Save the blog article
  cy.get('[data-test=submit]')
      .click();
  cy.wrap({
    title: title,
    teaserText: teaserText,
    text: text
  });
});

/**
 * Command for creating a blog app via api
 * @param {string} sender defines the sender id for the page or workspace the app should be created for
 * @param {string} name defines the name of the blog app
 * @param {boolean} active defines if the app is active or not
 *
 */

Cypress.Commands.add('apiCreateBlogApp', function (sender, name, active) {
  return cy.apiBearerToken('robert.lang@coyo4.com', 'demo').then((bearerToken) => cy.request({
    url: Cypress.env('backendUrl') + '/api/senders/' + sender + '/apps',
    method: 'POST',
    auth: {
      bearer: bearerToken
    },
    body: {
      key: 'blog',
      name: name || 'Blog',
      active: active || true,
      settings: {}
    }
  }));
});
