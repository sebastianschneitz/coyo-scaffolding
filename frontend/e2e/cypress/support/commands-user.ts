// ***********************************************************
// Custom commands related to users
//
// Commands defined in this file may utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// Ideally, these commands utilize only variables defined in
// authentication commands at `./commands-auth.ts`,
// ***********************************************************

/**
 * Command for creating a user via API
 * @param {boolean} superadmin defines if the user is a superadmin or not
 *
 */

Cypress.Commands.add('apiCreateUser', function (superadmin) {
    const firstName = Cypress.faker.name.firstName();
    const lastName = Cypress.faker.name.lastName();
    const email = firstName + '.' + lastName + '@coyoapp.com';
    return cy.apiBearerToken('robert.lang@coyo4.com', 'demo').then((bearerToken) => cy.request({
        auth: {
            bearer: bearerToken
        },
        method: 'POST',
        url: Cypress.env('backendUrl') + '/api/users',
        body: {
            firstname: firstName,
            lastname: lastName,
            email: email,
            active: true,
            superadmin: superadmin,
            password: 'Test1234'
        }
    }));
});