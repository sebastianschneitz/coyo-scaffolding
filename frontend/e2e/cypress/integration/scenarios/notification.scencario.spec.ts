describe('Notifications', function () {

    describe('Form app', function () {

        const textFieldName = Cypress.faker.random.word();
        const textFieldValue = Cypress.faker.random.word();

        beforeEach(function () {
            Cypress.Cookies.defaults({
                whitelist: 'COYOSESSION'
            });
            cy.clearLocalStorage().apiLogin('rl', 'demo');
            cy.apiCreatePage(Cypress.faker.random.word(), 'PUBLIC').as('page')
                .then(page => cy.createFormApp(Cypress.faker.random.word(), page.id, true).as('formApp')
                    .then(app => cy.addFieldToFormApp(textFieldName, page.id, app.id, 'text').as('formAppField')));
            //cy.visit('/home/timeline');
            cy.apiLogout();
        });

        afterEach(function () {
            cy.apiLogout();
        });

        it('should show notification and open details view when clicking on notification', function () {
            cy.clearLocalStorage().apiLogin('nf', 'demo').apiDismissTour();
            cy.createFormAppEntry(textFieldValue, this.formAppField.id, this.formApp.senderId, this.formApp.id);
            cy.apiLogout();
            cy.clearLocalStorage();
            cy.apiLogin('rl', 'demo').apiDismissTour().then(function () {
                cy.visit('/home');
                cy.get('[data-test=notification-toggle]').click();
                cy.get(':nth-child(2) > [data-test=notification-tabs]').click();
                cy.get(':nth-child(1) > [data-test=notification-item]').contains(this.formApp.name);
                cy.get(':nth-child(1) > [data-test=notification-item]').contains(this.page.name);
                cy.get('#notification-panel-activity > ul > :nth-child(1)').click();
                cy.get('[data-test=text-value]').contains(textFieldValue);
                cy.get(`[data-test="${textFieldName}"]`).contains(textFieldName);
            });
        });
    });
});
