describe('API user creation', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.apiLogin('rl', 'demo');
  });

  afterEach(function () {
    cy.apiLogout();
  });

  it('should create a random user and check if he/she has been created', function () {
    cy.apiCreateUser(true).then((credentials) => {
      cy.server();
      cy.route('GET', '/web/users**').as('getUserList');
      cy.visit('/admin/user-management/users');
      cy.wait('@getUserList');
      cy.get('[data-test=user-search-filter]')
          .clear()
          .type(`${credentials.body.firstname} ${credentials.body.lastname}{enter}`).then(() => {
        cy.get('[data-test=panel-list-user-management]')
            .should('contain', credentials.body.loginName);
      });
    });
  });
});
