describe('Chat sidebar', function () {

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin('rl', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.apiLogout();
    });

    it('should expand and collapse the sidebar', function () {
        cy.expandChatSidebar();
        cy.collapseChatSidebar();
    });

    it('should change the status', function () {
        cy.changePresenceStatus('busy', Cypress.faker.lorem.words());
    });

    it('should create a new single message channel', function () {
        cy.newSingleMessageChannel('Nancy Fork', Cypress.faker.lorem.sentence());
    });

    it('should create a new group message channel', function () {
        cy.newGroupMessageChannel('Nancy Fork', 'Ian Bold', Cypress.faker.lorem.words(), Cypress.faker.lorem.sentence());
    });

    it('should place and display emojies', function () {
        cy.newSingleMessageChannelWithEmoji('Nancy Fork', Cypress.faker.lorem.sentence());
    });
});
