describe('Event creation', function () {

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin('rl', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.apiLogout();
    });

    it('should create a private Event with inviting a user at the creation', function () {
        cy.server();
        cy.route('POST', '/web/events').as('postEvents');

        const eventName = 'qa-event with inviting the user ' + Date.now();
        const invitee: string = 'Ian Bold';
        const eventNameHyphens = eventName.replace(/\s/g, "-");

        cy.createEvent(eventName, true, null, invitee);
        cy.wait('@postEvents').its('status').should('eq', 201);

        cy.url().should('contain', Cypress.config().baseUrl + `/events/${eventNameHyphens}/timeline`);
        cy.get('[data-test=headline-event-title]')
            .should('have.text', eventName);
        cy.get('[data-test=events-show-participants-tab]').click();
        cy.get('[data-test=user-card-panel]').contains(invitee).within((panel) => {
            cy.get('[data-test=user-card-heading-text]').should('contain', invitee).and('contain', 'Member');
        })
    });

    it('should create a public Event without inviting a user at the creation', function () {
        cy.server();
        cy.route('POST', '/web/events').as('postEvents');
        const eventName = 'qa-event without inviting the user ' + Date.now();
        const eventNameHyphens = eventName.replace(/\s/g, "-");

        cy.createEvent(eventName);

        cy.wait('@postEvents').its('status').should('eq', 201);
        cy.url().should('contain', Cypress.config().baseUrl + `/events/${eventNameHyphens}/timeline`);
        cy.get('[data-test=headline-event-title]')
            .should('have.text', eventName);
        //TODO add other verifications that event contains relevant information
    });

    it('should create a public Event with different from the current user host', function () {
        cy.server();
        cy.route('POST', '/web/events').as('postEvents');

        const eventName = 'qa-event with different from the current user host ' + Date.now();
        const host: string = 'Company News';
        const eventNameHyphens = eventName.replace(/\s+/g, "-");

        cy.createEvent(eventName, false, host);

        cy.wait('@postEvents').its('status').should('eq', 201);
        cy.url().should('contain', Cypress.config().baseUrl + `/events/${eventNameHyphens}/timeline`);
        cy.get('[data-test=headline-event-title]')
            .should('have.text', eventName);
        cy.get('[data-test=events-show-sender-link]').should('contain', host);
        //TODO add other verifications that event contains relevant information
    });
});
