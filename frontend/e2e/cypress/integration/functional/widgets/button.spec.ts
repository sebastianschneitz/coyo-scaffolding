
describe('Button widget', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.clearLocalStorage().apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour();

    cy.apiCreateLandingPage('Button widget', true).as('landingPage')
        .then(lp => cy.visit('/home/' + lp.slug));
  });

  afterEach(function () {
    cy.apiLogout();
  });

  it('should be created', function () {
    cy.visit('/home/' + this.landingPage.slug);

    // edit view & add widget
    cy.get('[data-test=navigation-dropdown]').click();
    cy.get('[data-test=nav-view]').click();
    cy.get('[data-test=widget-add]').click();

    cy.get('.modal').within(function () {
      cy.get('.nav-item:nth-child(2)').click();
      cy.get('[data-test=widget-button]').click();

      // configure widget & submit
      cy.get('[data-test=form-text]').type('Click Me!');
      cy.get('[data-test=form-style-btn-primary]').click();
      cy.get('[data-test=form-url]').type('http://example.com/');
      cy.get('[data-test=form-external] > label').click();
      cy.get('[data-test=widget-submit]').click();
    });

    // save view & wait for UI
    cy.get('[data-test=view-save]').click();
    cy.wait(100);

    cy.get('.content').within(() => {
      cy.contains('Click Me!')
        .should('have.class', 'btn-primary')
        .and('have.attr', 'href', 'http://example.com/');
    });
  });
});
