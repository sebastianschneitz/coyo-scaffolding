describe('Timeline item footer actions', () => {

    let timelinePostMessage = '';
    const otherSender = 'Company News';

    beforeEach(() => {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        timelinePostMessage = 'Qa timeline-post for actions test ' + Date.now();
        cy.clearLocalStorage().apiLogin('rl', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.apiCreateTimelinePost(timelinePostMessage, false).apiShowNewTimelinePosts();
        cy.visit('/home/timeline');

        cy.server();
        cy.route('GET', '/web/timeline-items/new?**').as('getNewTimelineItems');
        cy.route('GET', '/web/users/*/subscriptions**').as('getSubscriptions');
        cy.route('DELETE', '/web/users/*/subscriptions?**').as('deleteSubscription');
        cy.route('GET', '/web/senders/search/actable-senders?**').as('getActableSenders');
        cy.route('GET', '/web/like-targets/timeline-item**').as('getLikeTimelineItem');
        cy.route('POST', '/web/like-targets/timeline-item/*/likes/*').as('postLikeTimelineItem');
        cy.route('POST', '/web/shares/timeline-item').as('postShareTimelineItem');
        cy.route('GET', '/web/timeline-items/shares/count?**').as('getCountSharesTimelineItem');

    });

    afterEach(() => {
        cy.apiLogout();
    });

    it('should be possible to like a non-restricted timeline entry', () => {
        cy.get('[data-test="timeline-stream"]')
            .contains('[data-test="timeline-item"]', timelinePostMessage).as('post').within(() => {
            cy.wait('@getLikeTimelineItem').its('status').should('eq', 200);
            cy.get('[data-test=like-button]').should('be.visible')
                .click();
            cy.wait('@postLikeTimelineItem').its('status').should('eq', 201);
            cy.get('@post').find('[data-test=like-button]')
                .should('have.attr', 'aria-checked', 'true')
                .get('[data-test="info-container"]').should('be.visible')
                .get('[data-test="like-info-button"]')
                .should('be.visible')
                .should('contain', 'You');
        });
    });

    it('should be possible to unlike a timeline entry', () => {
        cy.route('DELETE', '/web/like-targets/timeline-item/**').as('deleteLikeTimelineItem');

        cy.get('[data-test="timeline-stream"]').contains('[data-test="timeline-item"]', timelinePostMessage).as('post').within(() => {
            cy.wait('@getLikeTimelineItem').its('status').should('eq', 200);
            cy.get('[data-test=like-button]').should('be.visible')
                .click();
            cy.wait('@postLikeTimelineItem').its('status').should('eq', 201);
            cy.get('[data-test=like-button]').click();
            cy.wait('@deleteLikeTimelineItem').its('status').should('eq', 200);
            cy.get('@post').find('[data-test=like-button]')
                .should('have.attr', 'aria-checked', 'false');
            cy.get('[data-test="info-container"]').should('not.exist');
        });
    });

    it('should share a timeline post', () => {
        const message = Cypress.faker.random.word();
        cy.get('[data-test=timeline-stream]').contains('[data-test=timeline-item]', timelinePostMessage).within(() => {
            cy.get('[data-test=share-button]').click();
        })
            .get('[data-test=mat_dialog-content]').within(() => {
            cy.get('[data-test=share-with-sender-select] input').click().type(otherSender);
        })
            .get('.ng-dropdown-panel').within(() => {
            cy.contains('.ng-option', otherSender).click();
        })
            .get('[data-test=mat_dialog-content]').within(() => {
            cy.get('[data-test=share-message]').type(message);
        })
            .get('[data-test=share-submit]').click();
        cy.wait('@postShareTimelineItem').its('status').should('eq', 201);
        cy.wait('@getCountSharesTimelineItem').its('status').should('eq', 200);
        cy.get('[data-test=timeline-stream]')
            .contains('[data-test=timeline-item]', timelinePostMessage).within(() => {
            cy.get('[data-test=coyo-timeline-share]');
            cy.get('[data-test=timeline-share-message]').should('have.text', `${message}`)
            cy.get('[data-test=timeline-share-header-meta]').should('be.visible'); //TODO add verification to contain `You shared this post with ${otherSender}`
            cy.get('[data-test=info-container]').should('be.visible');
            cy.get('[data-test=shares-info-button]').should('be.visible');
        });
    });

    xit('should subscribe user to a timeline post automatically on creation', () => { //TODO in cypress sometimes doesn't see it is subscribed even though it is. Maybe adding some another wait
        cy.wait('@getSubscriptions').its('status').should('eq', 200);
        cy.get('[data-test=timeline-stream]')
            .contains('[data-test=timeline-item]', timelinePostMessage).within(() => {
            cy.wait('@getSubscriptions').its('status').should('eq', 200);
            cy.get('[data-test=subscribe-button]')
                .should('have.attr', 'aria-checked', 'true');
        });
    });

    xit('should unsubscribe timeline post', () => { //TODO in cypress sometimes doesn't see it is subscribed even though it is. Maybe adding some another wait
        cy.wait('@getSubscriptions').its('status').should('eq', 200);
        cy.wait('@getSubscriptions').its('status').should('eq', 200);
        cy.get('[data-test=timeline-stream]')
            .contains('[data-test=timeline-item]', timelinePostMessage).within(() => {
            cy.get('[data-test=subscribe-button]')
                .should('have.attr', 'aria-checked', 'true')
                .should('have.class', 'active');
            cy.get('[data-test=subscribe-button]').click();
            cy.wait('@deleteSubscription').its('status').should('eq', 204);
            cy.wait('@getSubscriptions').its('status').should('eq', 200);
            cy.get('[data-test=subscribe-button]')
                .should('have.attr', 'aria-checked', 'false')
        });
    });

    it('should change author for timeline post comment', () => {
        const timelinePostCommentMessage = Cypress.faker.random.word();
        cy.get('[data-test=timeline-stream]')
            .contains('[data-test=timeline-item]', timelinePostMessage).within(() => {
            cy.get('[data-test=functional-user-chooser]').click()
                .get('.ng-dropdown-panel-items').within(() => { //library component - impossible to add data attribute
                cy.contains('.ng-option', otherSender).scrollIntoView().click();   //library component - impossible to add data attribute
            })
                .get('[data-test=timeline-comment-message-edit-mode]').type(timelinePostCommentMessage)
                .get('[data-test=comment-form-submit]').click()
                .get('[data-test=comment-author]').should('contain', otherSender);
        });
    });

    it('should like as another sender', () => {
        const likeButton = '[data-test=like-button]';
        cy.get('[data-test=timeline-stream]')
            .contains('[data-test=timeline-item]', timelinePostMessage).within(() => {
            cy.get('[data-test=functional-user-chooser]').click()
                .get('.ng-dropdown-panel-items').within(() => { //library component - impossible to add data attribute
                cy.contains('.ng-option', otherSender).scrollIntoView().click();
            })
                .get(likeButton).click()
                .get('[data-test=like-button]')
                .get('[data-test=info-container]').should('be.visible')
                .get('[data-test=like-info-button]').should('be.visible')
                .get('[data-test=functional-user-chooser]').click()
                .get('.ng-dropdown-panel-items').within(() => {
                cy.contains('.ng-option', 'Robert Lang').click();
            })
                .get(likeButton)
                .should('have.attr', 'aria-checked', 'false')
                .get('[data-test="like-info-button"]').should('contain', otherSender);
        });
    });

});
