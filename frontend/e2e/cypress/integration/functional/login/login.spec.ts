describe('Login functionality', function () {

  beforeEach(function () {
    // Whitelist cookies to prevent logout
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
  });

  // Open COYO and reset password
  it('should send the link to reset the password', function () {
    cy.resetPassword('rl');
  });

  // Open COYO and login with wrong password
  it('should not login with a wrong password', function () {
    cy.login('rl', 'password');
    // Check for failure
    cy.get('[data-test=text-login-error]')
      .should('contain', 'Authentication Failed');
  });

  // Open COYO and login
  it('should login', function () {
    cy.login('rl', 'demo');
    // Check for successful login
    cy.url()
      .should('eq', Cypress.config().baseUrl + '/home/timeline');

    cy.apiLogout();
  });
});
