describe('Blog creation', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour()
        .then(() => {
          cy.visit('/');
        });
  });

  afterEach(function () {
    cy.apiLogout();
  });

  it('should create a workspace with an active blog app with auto share', function () {
    cy.apiCreateWorkspace(Cypress.faker.lorem.words(), 'PUBLIC').then((workspaceObject) => {
      cy.visit('/workspaces/' + workspaceObject.body.displayName.replace(/ /gi, '-'));
      cy.createBlogApp(Cypress.faker.lorem.words(), true, 'everyone', 'everyone', true, true);
      cy.get('[data-test=panel-blog-app]')
          .should('be.visible');
    });
  });

  it('should create a blog article', function () {
    cy.apiCreateWorkspace(Cypress.faker.lorem.words(), 'PUBLIC').then((workspaceObject) => {
      cy.apiCreateBlogApp(workspaceObject.body.id, 'Blog-' + Cypress.faker.lorem.word(), true).then((blogObject) => {
        cy.visit('/workspaces/' + workspaceObject.body.slug + '/apps/blog/' + blogObject.body.slug);
        cy.createBlogArticle(Cypress.faker.lorem.words(), Cypress.faker.lorem.sentences(),
            Cypress.faker.lorem.sentence(),
            true, 'published', 'user').then((values) => {
          cy.get('[data-test=text-blog-article-title]')
              .should('contain', values.title);
          cy.get('[data-test=text-blog-article-teaser]')
              .should('contain', values.teaserText);
          cy.get('[data-test=text-blog-article-text]')
              .should('contain', values.text);
        });
      });
    });
  });
});
