describe('App creation', function () {

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin('rl', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.apiLogout();
    });

    it('should enable all apps', function () {
        const appsToEnable = ['blog', 'content', 'events', 'file-library', 'form', 'forum', 'list', 'task', 'timeline',
            'wiki', 'championship'];
        appsToEnable.forEach(function (app) {
            cy.apiAppConfiguration(app, 'page', true)
                .its('status').should('eq', 200);
            cy.wait(3000); // Wait for the request to be done, otherwise it will throw an authorization error
        });
    });

    it('should create a page via API and create a blog app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Blog');
            cy.get('[data-test=panel-blog-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a Content app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Content');
            cy.get('[data-test=panel-content-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a Events app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Events');
            cy.get('[data-test=panel-events-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a Documents app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Documents');
            cy.get('[data-test=panel-documents-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a Form app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Form');
            cy.get('[data-test=panel-form-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a Forum app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Forum');
            cy.get('[data-test=panel-forum-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a List app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('List');
            cy.get('[data-test=panel-list-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a Tasks app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Tasks');
            cy.get('[data-test=panel-tasks-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a Timeline app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Timeline');
            cy.get('[data-test=panel-timeline-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a Wiki app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Wiki');
            cy.get('[data-test=panel-wiki-app]')
                .should('be.visible');
        });
    });

    it('should create a page via API and create a Championship app', function () {
        cy.apiCreatePage(Cypress.faker.lorem.words(), 'PUBLIC').then((pageObject) => {
            cy.visit('/pages/' + pageObject.slug);
            cy.createApp('Championship');
            cy.get('[data-test=panel-championship-app]')
                .should('be.visible');
        });
    });
});
