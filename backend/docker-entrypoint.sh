#!/bin/bash

set -e;

cat /etc/hosts | sed "s|localhost|localhost $(hostname)|g" > /etc/hosts

SKIP_WAIT_FOR_SERVICES="${SKIP_WAIT_FOR_SERVICES}"
if [ -z "${SKIP_WAIT_FOR_SERVICES}" ]; then
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "${COYO_DB_HOST:-coyo-db}:${COYO_DB_PORT:-5432}"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "${COYO_ES_HOST:-coyo-es}:${COYO_ES_PORT:-9300}"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "${COYO_MQ_HOST:-coyo-mq}:${COYO_MQ_PORT:-5672}"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "${COYO_STOMP_HOST:-coyo-stomp}:${COYO_STOMP_PORT:-61613}"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "${COYO_REDIS_HOST:-coyo-redis}:${COYO_REDIS_PORT:-6379}"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "${COYO_MONGODB_HOST:-coyo-mongo}:${COYO_MONGODB_PORT:-27017}"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "${COYO_TIKA_HOST:-coyo-tika}:${COYO_TIKA_PORT:-9998}"
fi

cd /coyo
bash -C '/coyo/start.sh';'bash'
